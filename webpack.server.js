const path = require('path');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: './server/index.js',

  target: 'node',

  externals: [nodeExternals()],

  output: {
    path: path.resolve('server-build'),
    filename: 'index.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader'
      }, {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
        ],
      },
      { test: /\.(scss|css)$/, loader: "ignore-loader" },
      {
        test: /\.(woff|woff2|eot|ttf|otf|jpe?g|png|gif|svg)$/,
        loader: "file-loader",
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
   // Options similar to the same options in webpackOptions.output
   // both options are optional
   filename: "style.css",
   chunkFilename: "[name].css"
 })
]
};