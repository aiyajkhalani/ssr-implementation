export const dashboardTabs = {
  userProfile: {
    title: "User Profile",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "profile"
  },
  buyItNow: {
    title: "Buy it Now",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "sales-engines?buyItNow=true"
  },
  boats: {
    title: "Manage Boats",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-boats"
  },
  brokers: {
    title: "Brokers",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "brokers"
  },
  boatRents: {
    title: "Manage Boat Rent",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-boat-rents"
  },
  branch: {
    title: "Branch",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-branches"
  },
  services: {
    title: "Services",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "add-boat-service"
  },
  marinaAndStorage: {
    title: "Marina & Storage",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-marina-storage"
  },
  auctionRooms: {
    title: "Manage Auction Rooms",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-auction-rooms"
  },
  boatShows: {
    title: "Boat Shows",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-boat-shows"
  },
  contactLeads: {
    title: "Contact Leads",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "contact-leads"
  },
  agreementsAndFiles: {
    title: "Agreements & Files",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "agreement-file"
  },
  salesEngine: {
    title: "Sales Engine",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "sales-engines"
  },
  salesEngineArchive: {
    title: "Sales Engine Archive",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "sales-engine-archives"
  },
  wishlists: {
    title: "Manage Wishlist",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: ""
  },
  articles: {
    title: "Manage Articles",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "manage-articles"
  },
  advertisements: {
    title: "Advertisements",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: ""
  },
  changePassword: {
    title: "Change Password",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "change-password"
  },
  logOut: {
    title: "Logout",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    url: "logout"
  }
};

export const userRoles = {
  MEMBER: "member",
  BOAT_OWNER: "boat-owner",
  BROKER_AND_DEALER: "broker-and-dealer",
  BOAT_MANUFACTURER: "boat-manufacturer",
  YACHT_SHIPPER: "yacht-shipper",
  SURVEYOR: "surveyor",
  RENT_AND_CHARTER: "rent-and-charter",
  SERVICE_AND_MAINTENANCE: "service-and-maintenance",
  MARINA_AND_STORAGE: "marina-and-storage",
  AGENT: "agent"
};

export const userRoleTypes = {
  BUYER: "buyer",
  SELLER: "seller",
  ADDITIONAL_MARKET: "additional-market",
  OTHER: "other",
  AGENT: "agent"
};

export const salesEngineAccessibleTypes = [userRoleTypes.BUYER, userRoleTypes.SELLER, userRoleTypes.OTHER, userRoleTypes.AGENT]

export const boatInformation = {
  TITLE: "Boat information",
  BOAT_TYPE: "Boat Type",
  BOAT_NAME: "Boat Name",
  BOAT_PARKING: "Boat Parking",
  TRAILER: "Trailer",
  YEARS_OF_BUILT: "Years Of Built",
  MANUFACTURED_BY: "Manufactured By",
  HULL_MATERIAL: "Hull Material",
  HULL_COLOR: "Hull Color",
  HOURS_USED: "Hours Used"
};

export const mechanicalSystem = {
  TITLE: "Mechanical System",
  WATER_MAKER: "WATER MAKER",
  BOW_THRUSTER: "BOW THRUSTER",
  STEERING_SYSTEM: "STEERING SYSTEM",
  STABILIZER_SYSTEM: "STABILIZER SYSTEM",
  FUEL_TYPE: "FUEL TYPE",
  OIL_WATER_SEPARATOR: "OIL/WATER SEPARATOR"
};

export const electricalSystem = {
  TITLE: "Electrical System",
  OUTPUT: "OUTPUT",
  GENERATORS: "GENERATORS",
  EMERGENCY_GENERATORS: "EMERGENCY GENERATORS",
  TYPES_OF_BATTERIES: "TYPES OF BATTERIES",
  NO_OF_BATTERIES: "NO OF BATTERIES",
  CONTENT: "CONTENT"
};

export const boatDimensions = {
  TITLE: "Dimensions",
  DECK_NUMBER: "DECK NUMBER",
  HEIGHT: "HEIGHT",
  LENGTH: "LENGTH",
  WIDTH: "WIDTH",
  WEIGHT: "WEIGHT",
  BEAM: "BEAM",
  DRAFT: "DRAFT",
  DISPLACEMENT: "DISPLACEMENT",
  MAIN_SALOON: "MAIN SALOON",
  MAIN_SALOON_CONVERTIBLE: "MAIN SALOON CONVERTIBLE",
  NO_OF_HEADS: "NO OF HEADS",
  NO_OF_CREW_CABIN: "NO OF CREW CABIN",
  NO_OF_CREW_BIRTHS: "NO OF CREW BIRTHS",
  NO_OF_CREW_HEADS: "NO OF CREW HEADS"
};

export const getListOfYears = () => {
  let listOfYears = [];
  const year = new Date().getFullYear();
  for (let i = 1; i <= 30; i++) {
    listOfYears = listOfYears.concat(year - i);
  }
  listOfYears.reverse();
  for (let i = 0; i <= 10; i++) {
    listOfYears = listOfYears.concat(year + i);
  }

  return listOfYears;
};

export const auctionCarouselOptions = ["None", "Atria", "Callisto", "Dione"];

export const pagination = {
  PAGE_COUNT: 1,
  PAGE_RECORD_LIMIT: 20,
};

export const icons = {
  fridge: "adam-refrigeratorr",
  water_ski: "adam-jetski-facing-right",
  ipod_docking_system: "adam-audio-system",
  automatic_pilot: "adam-user",
  satellite_communication: "adam-radar",
  radar: "adam-radar",
  data_interface: "adam-cog-4",
  tv: "adam-tv",
  jacuzzi: "adam-bathtub",
  microwave: "adam-microwave-3",
  stove: " adam-stove",
  rest_room: "adam-room-service",
  freezer: "adam-refrigeratorr",
  bbq: " adam-bbq",
  black_water: " adam-water",
  shower: "adam-shower",
  dish_washer: "adam-dishwasher",
  washing_machine: "adam-washing-machine1",
  unser: "adam-lightbulb",
  helipad: "adam-parking",
  bimini: "adam-sailingboat",
  raft: "adam-wood-raft",
  table: " adam-table",
  windlass: "adam-ship-window",
  tabs: "adam-tabs",
  bow: " adam-bowboat",
  platform: "adam-kitchen",
  swimming: "adam-swimming-pool",
  sundeck: "adam-sunbed1",
  radar: "adam-radar",
  compass: "adam-compass",
  gps: "adam-gps",
  instruments: "adam-navigation-main",
  navigation: "adam-big-lighthouse",
  communication: "adam-communication",
  // rent_per_hour: require("../../assets/images/rent/Vector Smart Object copy 4-2@3x.png"),
  // private_trip: require("../../assets/images/rent/Vector Smart Object copy 4@3x.png"),
  // shared_trip: require("../../assets/images/rent/Vector Smart Object copy 4-1@3x.png"),
  // favicon: require("../../assets/images/logo/favicon.png"),
  fishing_boat: "adam-fishing-boat1",
  motor_yacht: "adam-motoryacht",
  sailing_boat: "adam-sailingboat",
  power_boat: "adam-powerboat",
  bow_boat: "adam-bowboat",
  super_yacht: "adam-superyacht",
  canone: "adam-small-canoe",
  kayak: "adam-kayak",
  jet_ski: "adam-jetski",
  cuddy_boat: "adam-cuddyboat",
  wedding_party: "adam-party",
  deep_sea_fishing: "adam-fish-facing-right",
  sea_tour: "adam-ocean-waves",
  private_party: "adam-mask",
  diving_trip: "adam-diving-helmet",
  night_trip: "adam-star-inside-circle",
  birthday_party: "adam-party-hat",
  cruise_tour: "adam-cruise-ship",
  yacht_charter: "yacht-charter",
  event: "adam-fireworks",
  bbq_grill: "adam-bbq",
  outside_shower: "adam-shower1",
  navigation_lights: "adam-long-lighthouse",
  navigation_instruments: "adam-compass",
  painting_and_fiberglass: "adam-hair-brush",
  moorings: "adam-coins",
  marina_management: "adam-management",
  marina_product: "adam-profits",
  mobile_marina: "adam-remote-control",
  provisioning_and_delivery: "adam-delivery",
  woodworking: "adam-wood-raft",
  bottoms_painiting: "adam-photos",
  blister_repairing: "adam-service-2",
  king_starboard_structures: "adam-wood-board",
  full_boat_survey: "adam-measure",
  buffing_and_waxing: "adam-settings",
  scuba_diving_equipments: "adam-swimming-glasses",
  tender_engine: "adam-ship-engine",
  swimming_ladder: "adam-swimming-pool",
  life_raft: "adam-wood-raft",
  secondary_radar: "adam-radar",
  tender: "adam-service-2",
  kiteboard: "adam-wood-board",
  wi_fi: "adam-wifi2",
  kids_meal: "adam-food",

};

export const iconImages = {
  water_marker: require("../../assets/images/boatInner/Water Marker.png"),
  ice_box: require("../../assets/images/boatInner/ICE Box.png"),
  under_water_lights: require("../../assets/images/boatInner/Under Water Lights.png"),
  bimini_top: require("../../assets/images/boatInner/Bamini Top.png"),
  boat_covers: require("../../assets/images/boatInner/Boat Cover.png"),
  cockpit_table: require("../../assets/images/boatInner/Cockpit Table.png"),
  trip_tabs: require("../../assets/images/boatInner/Trip Tab.png"),
  bow_thruster: require("../../assets/images/boatInner/Bow Thruster.png"),
  gangway: require("../../assets/images/boatInner/gangway.png"),
  stern_thruster: require("../../assets/images/boatInner/Stern Thruster.png"),
  davit: require("../../assets/images/boatInner/Davit.png"),
  teak_flooring: require("../../assets/images/boatInner/Teak Flooring.png"),
  echo_sounder: require("../../assets/images/boatInner/Echo Sounder.png"),
  speed_log: require("../../assets/images/boatInner/Speed Log.png"),
  weather_station: require("../../assets/images/boatInner/Weather Station.png"),
  satellite_tv_system: require("../../assets/images/boatInner/Setellite TV.png"),
  radar_primary: require("../../assets/images/boatInner/Radar Primary.png"),
  radar_secondary: require("../../assets/images/boatInner/Radar Secondary.png"),
}

export const lookupTypes = {
  YOU_ARE_AN: "You are An",
  SWP: "SWP",
  TRIP_TYPE: "Trip Type"
};

export const resultMessage = {
  search:
    "We couldn't find anything matching your search. Try searching other keywords.",
  default: "We couldn't find anything."
};

export const rentBoatTrip = {
  SHARED_TRIP: "Shared Trip",
  PRIVATE_TRIP: "Private Trip",
  RENT_PER_HOUR: "Rent Per Hour"
};

export const totalRecords = {
  limit: 100
};

export const moduleCategory = {
  RENT: "rent",
  MARINA_STORAGE: "marinaStorage",
  BOAT_SERVICE: "boatService",
  BOAT_SHOW: "boatShow",
  HOME: "home",
  BOAT_MEDIA: "boatMedia"
};

export const advertisementCategory = {
  RENT: "rent",
  MARINA_STORAGE: "marina & storage",
  BOAT_SERVICE: "boat service",
  BOAT_SHOW: "boatShow",
  HOME: "home"
};

export const dimension = {
  sharedTrip: {
    width: 500,
    height: 340,
    divide: 3
  },
  privateTrip: {
    width: 380,
    height: 280,
    divide: 4
  },
  popularTrip: {
    width: 420,
    height: 307,
    divide: 5
  },
  perHourTrip: {
    width: 380,
    height: 340,
    divide: 4
  },
  recommended: {
    divide: 3,
    margin: 16,
    marginHeight: 0,
    width: 500,
    height: 340,
    respectRatioWidth: 500,
    respectRatioHeight: 340
  },
  sellAround: {
    divide: 5,
    margin: 16,
    marginHeight: 5,
    width: 295,
    height: 320
  },
  marinaStorageVideo: {
    divide: 3,
    margin: 18,
    marginHeight: 0,
    width: 500,
    height: 340
  },
  rentVideo: {
    divide: 3,
    margin: 30,
    marginHeight: 0,
    width: 500,
    height: 380
  },
  boatShowVideo: {
    divide: 3,
    margin: 18,
    marginHeight: 0,
    width: 500,
    height: 340
  },
  boatServiceVideo: {
    divide: 3,
    margin: 30,
    marginHeight: 0,
    width: 500,
    height: 340
  },
  homePageVideo: {
    divide: 3,
    margin: 30,
    marginHeight: 0,
    width: 500,
    height: 340
  },
  marinaRegister: {
    divide: 1,
    margin: 0,
    marginHeight: 0,
    width: 1600,
    height: 250
  },
  marinaRegisterDouble: {
    divide: 2,
    margin: 15,
    marginHeight: 0,
    width: 1600,
    height: 300,

  },
  marinaMostPopular: {
    divide: 4,
    margin: 16,
    marginHeight: 0,
    width: 380,
    height: 280,
    respectRatioWidth: 380,
    respectRatioHeight: 280
  },
  topRatedMarina: {
    divide: 5,
    margin: 30,
    marginHeight: 0,
    width: 280,
    height: 360,
    respectRatioWidth: 280,
    respectRatioHeight: 360
  },
  servicesMarinaStorage: {
    divide: 4,
    margin: 30,
    marginHeight: 0,
    width: 400,
    height: 130,
    respectRatioWidth: 130,
    respectRatioHeight: 90
  },
  marinaMoreServices: {
    divide: 6,
    margin: 30
  },
  yachtServices: {
    divide: 6,
    margin: 23
  },
  marinaMoreServicesView: {
    divide: 4,
    margin: 16,
    marginHeight: 0,
    width: 380,
    height: 280,
    respectRatioWidth: 380,
    respectRatioHeight: 280
  },
  auctionRoom: {
    divide: 4,
    margin: 16,
    marginHeight: 0,
    width: 360,
    height: 290,
    respectRatioWidth: 360,
    respectRatioHeight: 290
  },
  homeRecommended: {
    divide: 4,
    margin: 16,
    marginHeight: 0,
    width: 380,
    height: 280,
    respectRatioWidth: 380,
    respectRatioHeight: 280
  },
  homeMostPopular: {
    divide: 5,
    margin: 12,
    marginHeight: 0,
    width: 275,
    height: 350,
    respectRatioWidth: 275,
    respectRatioHeight: 350,
  },
  boatMediaArticle: {
    divide: 3,
    width: 360,
    height: 180,
    respectRatioHeight: 180,
    respectRatioWidth: 360,
    margin: 16,
    marginHeight: 0,
  },
  boatMediaPost: {
    divide: 10,
    width: 100,
    height: 100,
    respectRatioHeight: 100,
    respectRatioWidth: 100,
    margin: 16,
    marginHeight: 0,
  },
  boatMediaVideo: {
    divide: 3,
    width: 500,
    height: 280,
    respectRatioHeight: 280,
    respectRatioWidth: 500,
    margin: 56,
    marginHeight: 0,
  },
  boatMediaMainDiv: {
    divide: 3,
    width: 500,
    height: 280,
    respectRatioHeight: 280,
    respectRatioWidth: 500,
    margin: 32,
    marginHeight: 0,
  },
  boatMediaCommonVideo: {
    divide: 3,
    width: 500,
    height: 340,
    respectRatioHeight: 340,
    respectRatioWidth: 500,
    margin: 55,
    marginHeight: 0,
  },
  userProfileListing: {
    divide: 4,
    width: 295,
    height: 220,
    respectRatioHeight: 220,
    respectRatioWidth: 295,
    margin: 10,
    marginHeight: 0,
  },
  showAllGrid: {
    divide: 4,
    margin: 30,
    marginHeight: 0,
    width: 380,
    height: 280,
    respectRatioWidth: 380,
    respectRatioHeight: 280
  },
  boatServiceReview: {
    divide: 4,
    margin: 16,
    marginHeight: 0,
    width: 380,
    height: 280,
    respectRatioWidth: 380,
    respectRatioHeight: 280
  },
  boatShowItems: {
    divide: 4,
    width: 275,
    height: 310,
    respectRatioHeight: 310,
    respectRatioWidth: 275,
    margin: 16,
    marginHeight: 0,
  },
};

export const defaultImage =
  "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/boat10.jpeg";

export const sharedAndPrivateTripTypes = {
  DEEP_SEA_FISHING: "Deep see finishing",
  SEE_TOUR: "See tour",
  PRIVATE_PARTY: " Private party",
  WEDDING_PARTY: "Wedding party",
  DRIVING_TRIP: " Diving trip",
  OVER_NIGHT_TRIP: " Over night trip",
  BIRTHDAY_PARTY: "Birthday party",
  CRUISE_TRIP: "Cruise trip",
  EVENT_TRIP: "Event Trip",
  SUPER_YACHT_CHARTER: "Super yacht charter",
  DINNER_CRUISE: "Dinner Cruise",
  DINNING_AND_BAR_CRUISE: "Dinning and Bar Cruise"
};

export const timePeriod = ["Hours", "Days"];

export const defaultLayout = require("../../assets/images/default/no_image_png_935205.png");

export const defaultBoatHomeImage = require("../../assets/images/default/Coaker’s-Walk.jpg");

export const preferenceOptions = [
  { id: "Individual", value: "Individual" },
  { id: "Organization", value: "Organization" }
];

export const infoList = [
  "Buy from anywhere in the world",
  "Buy using AdamSea's Sales Engine platform",
  "Inspect your yacht before you buy it",
  "Negotiate prices online direct with the seller",
  "Access to our worldwide shipping partners",
  "Use our Secure 'Escrow' Account",
  "Assign your agent as a purchase assistant",
  "Add articles on AdamSea"
];

export const mediaCategory = {
  marinaStorage: {
    type: "marinaStorage",
    fieldName: "marinaStorageBanner"
  },
  boatShow: {
    type: "boatShow",
    fieldName: "boatShowBanner",
    isBanner: true
  },
  boatService: {
    type: "boatService",
    fieldName: "boatServiceBanner"
  },
  home: {
    type: "home",
    fieldName: "homeBanner"
  },
  rent: {
    type: "rent",
    fieldName: "rentBanner"
  }
}

export const carouselArrow = {
  ratio: 24
}

export const latLng = { lat: 23.0225, lng: 72.5714 };

export const salesEngineSteps = (skipSurvey = false) => {

  return skipSurvey ?
    [
      { step: 1, name: "My Boat", isActivated: true },
      { step: 2, name: "Payment", isActivated: true },
      { step: 3, name: "Shipment", isActivated: true },
    ]
    :
    [
      { step: 1, name: "My Boat", isActivated: true },
      { step: 2, name: "Survey My Boat", isActivated: true },
      { step: 3, name: "Survey Report", isActivated: true },
      { step: 4, name: "Payment", isActivated: true },
      { step: 5, name: "Shipment", isActivated: true },
    ]
}

export const salesEngineStatus = {
  myBoat: "My Boat",
  negotiation: "Negotiation",
  surveyorSkipped: "Surveyor Skipped",
  surveyMyBoat: "Survey My Boat",
  surveyorRequested: "Surveyor Requested",
  surveyorAccepted: "Surveyor Accepted",
  surveyorPaymentPending: "Surveyor Payment Pending",
  surveyorPaymentCompleted: "Surveyor Payment Completed",
  surveyorReportUpload: "Surveyor Report Uploaded",
  agreementPending: "Agreement Pending",
  buyerAcceptAgreement: "Buyer Accept Agreement",
  sellerAcceptAgreement: "Seller Accept Agreement",
  agreement: "Agreement Completed",
  paymentPending: "Payment Pending",
  paymentCompleted: "Payment Completed",
  shipperAccepted: "Shipper Accepted",
  buyerAddedShipmentLocationPending: "Buyer Added Shipment Location Pending",
  buyerAddedShipmentLocationCompleted: "Buyer Added Shipment Location Completed",
  shipperPaymentPending: "Shipper Payment Pending",
  shipperPaymentCompleted: "Shipper Payment Completed",

  shipmentStarted: "Shipment Started",
  shipmentCompleted: "Shipment Completed",

  shipperAddedDocument: "Shipper Uploaded Document",

  inspectionPayment: "Inspection Payment",
  negotiationPending: "Negotiation Pending",
  certifyMyBoat: "Certify My Boat",
  report: "Report",

  shipment: "Shipment",
  completed: "Completed",
  stopProcess: "Process stopped",
  // auditor : "Auditor",
  auditorConfirm: "Auditor Confirm",
  auditorNotConfirm: "Auditor Not Confirm",
  // accounter : "Accounter",
  accounterConfirm: "Accounter Confirm",
  accounterNotConfirm: "Accounter Not Confirm",
  lostDeal: "Lost Deal",
}

export const stepOneStatus = [
  salesEngineStatus.myBoat,
  salesEngineStatus.negotiation
]

export const stepTwoStatus = [
  salesEngineStatus.surveyMyBoat,
  salesEngineStatus.surveyorRequested,
  salesEngineStatus.surveyorAccepted,
  salesEngineStatus.surveyorPaymentPending,
  salesEngineStatus.surveyorPaymentCompleted,
]

export const stepThreeStatus = [
  salesEngineStatus.surveyorReportUpload,
]

export const stepFourStatus = [
  salesEngineStatus.surveyorSkipped,
  salesEngineStatus.agreementPending,
  salesEngineStatus.buyerAcceptAgreement,
  salesEngineStatus.sellerAcceptAgreement,
  salesEngineStatus.agreement,
  salesEngineStatus.paymentPending,
]

export const stepFiveStatus = [
  salesEngineStatus.paymentCompleted,
  salesEngineStatus.shipperAccepted,
  salesEngineStatus.buyerAddedShipmentLocationCompleted,
  salesEngineStatus.buyerAddedShipmentLocationPending,
  salesEngineStatus.shipperPaymentPending,
  salesEngineStatus.shipperPaymentCompleted,
]

export const marketTypes = {
  BOAT: "BOAT",
  RENT_BOAT: "RENT BOAT",
  YACHT_SERVICE: "YACHT SERVICE",
  MARINA_AND_STORAGE: "MARINA AND STORAGE",
}

export const paymentOptions = [
  { name: "Bank To Bank Transfer" },
  { name: "Credit Card" },
]

export const salesEngineAgreementType = {
  agreementContract: "agreementContract",
  skipSurveySection: "skipSurveySection",
  newBoatBuying: "newBoatBuying",
  usedBoatBuying: "usedBoatBuying",
  bankPaymentDetail: "bankPaymentDetail",
  escapeInspection: "escapeInspection",
  receivedItemNotice: "receivedItemNotice",
  transactionGeneralRules: "transactionGeneralRules",
  escrowPaymentDetails: "escrowPaymentDetails",
  advertiseAgreement: "advertiseAgreement",
}

export const boatServiceInner = [
  { image: require("../../assets/images/boatInner/2.jpg") },
  { image: require("../../assets/images/boatInner/4.jpg") }
];

export const showAll = {
  recommended: "recommended",
  mostPopular: "mostPopular",
  privateTrip: "privateTrip",
  sharedTrip: "sharedTrip",
  tripsPerHour: "tripsPerHour",
}

export const showAllMarina = {
  recommended: "recommended",
  mostPopular: "mostPopular",
  topRated: "topRated",
}

export const footerLinks = {
  Service_Agreement: "service-agreement-282",
  Dispute_Rules: "dispute-rules-780",
  Privacy_Policy: "privacy-policy-518",
  Product_Listing_Policy: "product-listing-policy-342",
  Terms_Of_Use: "terms-of-use-731",
};

export const arrows = {
  left: require("../../assets/images/Arrow/left-arrow.png"),
  right: require("../../assets/images/Arrow/right-arrow.png"),
}

export const rentIcon = {
  clock: require("../../assets/images/rent/clock.png"),
  user: require("../../assets/images/rent/user-3.png"),
  measure: require("../../assets/images/rent/measure-3.png"),
}

export const bellIcon = require("../../assets/images/header/bell.png");


export const showAllService = {
  recentlyAdded: "recentlyAdded",
}


export const profileFields = {
  member: 17,
  "boat-owner": 25,
  "broker-and-dealer": 26,
  "marina-and-storage": 18,
  "rent-and-charter":  18,
  "boat-manufacturer": 27,
  "yacht-shipper": 26,
  surveyor: 27,
  "service-and-maintenance": 18,
  agent: 18,
}