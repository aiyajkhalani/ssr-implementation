import { icons } from "./enums/enums";

export const getLookUpImages = data => {
  const image = {
    marinaProvide: require("../assets/images/marinaStorageInner/marina.jpeg"),
    storageProvide: require("../assets/images/marinaStorageInner/storage.jpeg"),
    both: require("../assets/images/marinaStorageInner/marina&storage1.jpeg")
  };

  switch (data.alias) {
    case "Marina Provide":
      return image.marinaProvide;
      break;
    case "Boat Storage Provide":
      return image.storageProvide;
      break;
    case "Both":
      return image.both;
      break;
    default:
      break;
  }
};

