import React from "react";
import uuid from "uuid/v4";
import moment from "moment";
import Datetime from "react-datetime";

export function renderOptions(options) {
  let optionArray = [];
  for (let i = 1; i <= options; i++) {
    optionArray.push(i);
  }
  return optionArray.map(item => {
    return (
      <option key={uuid()} value={item}>
        {item}
      </option>
    );
  });
}
export function renderOptionsWithKey(options) {
  return (
    options &&
    options.map(item => {
      return (
        <option key={uuid()} value={item.id}>
          {item.alias}
        </option>
      );
    })
  );
}

export function dateStringFormate(date) {
  return moment(date).toISOString();
}
export function dateStringFormateByHour(date) {
  return moment(date).format("hh:mm a");
}
export const dimensionAspectRatio = (width, height) => {
  return width / height;
};
let yesterday = Datetime.moment().subtract(1, "day");

export const valid = function (current) {
  return current.isAfter(yesterday);
};
export const priceFormat = function (price) {
  return price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
};
export const percentageFormate = function (data) {
  return parseFloat(data).toFixed(2)
}

export function prepareGalleryData(items) {
  return items && items.length && items.map(item => {
    return {
      src: item
    };
  })
}

export function typeWiseTripLabel(type) {
  return type === "Shared Trip"
    ? "Person"
    : type === "Private Trip"
      ? "Trip"
      : "Hour";
}

export function commonBoatType(alias) {
  return alias === "USED" ? (
    <span className="new-build new-build-bg-home">USED</span>
  ) : (
      <span className="brand-new-tag">BRAND NEW</span>
    );
}

export function commonBoatTypeProfile(value) {
  return value && value.boatStatus && value.boatStatus.alias === "USED" ? (
    <span className="new-build new-build-bg-home">USED</span>
  ) : (
      <span className="brand-new-tag">BRAND NEW</span>
    );
}

export function commonBoatShowAllType(data) {
  return data && data.boatStatus && data.boatStatus.alias === "USED" ? (
    <span className="new-build new-build-bg-home">USED</span>
  ) : (
      <span className="brand-new-tag">BRAND NEW</span>
    );
}

export function commonMarinaType(provider) {
  return provider && provider.alias === "Marina" ? (
    <span className="new-build new-build-bg-home small-screen-marina-badge">
      MARINA
    </span>
  ) : provider && provider.alias === "Marina & Storage" ? (
    <span className="marina-new-tag small-screen-marina-badge">
      MARINA & STORAGE
    </span>
  ) : (
        <span className="shared-trip-tag small-screen-marina-badge">
          STORAGE
    </span>
      )
}

export function commonRentType(value) {
  return value && value.trip && value.trip.alias === "Rent Per Hour" ? (
    <div className="rent-per-hour-tag position-absolute boat-grid-badge">
      Rent per hour
    </div>
  ) : value && value.trip && value.trip.alias === "Shared Trip" ? (
    <div className="shared-trip-tag position-absolute small-shared boat-grid-badge">
      Shared Trip
    </div>
  ) : (
        <div className="private-trip-tag position-absolute boat-grid-badge">
          private trip
    </div>
      );
}

export function commonRecommendedRentType(trip) {
  return trip && trip.alias === "Rent Per Hour" ? (
    <div className="rent-per-hour-tag position-absolute boat-grid-badge">
      Rent per hour
    </div>
  ) : trip && trip.alias === "Shared Trip" ? (
    <div className="shared-trip-tag position-absolute small-shared boat-grid-badge">
      Shared Trip
    </div>
  ) : (
        <div className="private-trip-tag position-absolute boat-grid-badge">
          private trip
    </div>
      );
}

export function rentTypeAddress(trip, address, tripAddress) {
  return trip && trip.alias === "Rent Per Hour" ? (
    <div>{address}</div>
  ) : (
      <div>{tripAddress}</div>
    );
}


export function rentTypeName(trip, boatName, tripType, boatLength) {
  return trip && trip.alias === "Rent Per Hour" ? (
    <div>{boatName}</div>
  ) : (
      <div>{boatLength} Ft / {tripType && tripType.name}</div>
    );
}

export function reviewType(user) {
  return user && user.role && user.role.role === "RENT & CHARTER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : user && user.role && user.role.role === "MEMBER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Good quality </span>
    </div>
  ) : user && user.role && user.role.role === "BOAT OWNER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication  </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Organization </span>
      <span className="review-quality">Service</span>
      <span className="review-quality">Good value </span>
    </div>
  ) : user && user.role && user.role.role === "BROKER & DEALER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication</span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Services quality</span>
    </div>
  ) : user && user.role && user.role.role === "SERVICE & MAINTENANCE" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : user && user.role && user.role.role === "BOAT MANUFACTURER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication  </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Organization </span>
      <span className="review-quality">Service</span>
      <span className="review-quality">Good value </span>
    </div>
  ) : user && user.role && user.role.role === "SURVEYOR" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication </span>
      <span className="review-quality">Recommended </span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Survey quality</span>
    </div>
  ) : user && user.role && user.role.role === "YACHT SHIPPER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication </span>
      <span className="review-quality">Recommended </span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Survey quality</span>
    </div>
  ) : user && user.role && user.role.role === "MARINA & STORAGE" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : null
}

export function reviewTypeProfile(seller) {
  return seller && seller.role && seller.role.role === "RENT & CHARTER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : seller && seller.role && seller.role.role === "MEMBER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication</span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Good quality </span>
    </div>
  ) : seller && seller.role && seller.role.role === "BOAT OWNER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication  </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Organization </span>
      <span className="review-quality">Service</span>
      <span className="review-quality">Good value </span>
    </div>
  ) : seller && seller.role && seller.role.role === "BROKER & DEALER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication</span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Services quality</span>
    </div>
  ) : seller && seller.role && seller.role.role === "SERVICE & MAINTENANCE" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : seller && seller.role && seller.role.role === "BOAT MANUFACTURER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication  </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Organization </span>
      <span className="review-quality">Service</span>
      <span className="review-quality">Good value </span>
    </div>
  ) : seller && seller.role && seller.role.role === "SURVEYOR" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication </span>
      <span className="review-quality">Recommended </span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Survey quality</span>
    </div>
  ) : seller && seller.role && seller.role.role === "YACHT SHIPPER" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Communication </span>
      <span className="review-quality">Recommended </span>
      <span className="review-quality">Good value</span>
      <span className="review-quality">Survey quality</span>
    </div>
  ) : seller && seller.role && seller.role.role === "MARINA & STORAGE" ? (
    <div className="ratingQuality width-100 d-flex justify-content-around flex-column align-items-center">
      <span className="review-quality">Organization </span>
      <span className="review-quality">Recommended</span>
      <span className="review-quality">Value for money</span>
      <span className="review-quality">Services</span>
      <span className="review-quality">Safety</span>
    </div>
  ) : null
}


