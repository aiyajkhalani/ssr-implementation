import React from 'react'

const UserContext = React.createContext({
    currentUser: {
        firstName: 'Adam-Sea User',
        country: ''
    }
})

export const UserProvider = UserContext.Provider
export const UserConsumer = UserContext.Consumer
export default UserContext