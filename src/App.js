import React from 'react';

import { AppRoutes } from "./routes/routes";

class App extends React.Component {

  render() {
    return (
      <>
        <AppRoutes />
      </>
    )
  }
}

export default App;
