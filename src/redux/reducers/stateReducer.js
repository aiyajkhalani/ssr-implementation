import {
    GET_ALL_STATES_SUCCESS
} from '../actionTypes'


const InitialState = {
    states: {}
}

export const stateReducer = (state = InitialState, action) => {
    switch (action.type) {

        case GET_ALL_STATES_SUCCESS:
            return {
                ...state,
                states: action.payload
            }


        default:
            return state
    }
}