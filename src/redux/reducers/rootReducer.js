import { combineReducers } from 'redux';
import { loginReducer } from './loginReducer';
import { errorReducer } from './errorReducers';
import { dashboardReducer } from './dashboardReducer';
import { stateReducer } from './stateReducer'
import { boatReducer } from './boatReducer'
import { marinaAndStorageReducer } from './marinaAndStorageReducer'
import { branchReducer } from './branchReducer'
import { boatRentReducer } from './boatRentReducer'
import { articleReducer } from './articleReducer'
import { rentReducer } from "./rentReducer"
import { boatServiceReducer } from './boatServiceReducer'
import { boatShowReducer } from "./boatShowReducer";
import { videoReducer } from "./VideoReducer"
import { reviewReducer } from "./ReviewReducer"
import { advertisementReducer } from "./advertisementReducer"
import { salesEngineReducer } from "./salesEngineReducer"
import { mediaReviewsReducer } from "./mediaReviewsReducer"
import { mediaArticleReducer } from "./mediaArticleReducer"
import { userGuideReducer } from "./userGuideReducer"
import { userFaqReducer } from "./userFaqReducer"
import { pageInfoByTypeReducer } from "./pageInfoByTypeReducer"
import { bannerReducer } from "./bannerReducer"


export default combineReducers({
    loginReducer,
    dashboardReducer,
    stateReducer,
    boatReducer,
    marinaAndStorageReducer,
    branchReducer,
    boatRentReducer,
    articleReducer,
    rentReducer,
    boatServiceReducer,
    boatShowReducer,
    videoReducer,
    reviewReducer,
    advertisementReducer,
    errorReducer,
    salesEngineReducer,
    mediaReviewsReducer,
    mediaArticleReducer,
    userGuideReducer,
    userFaqReducer,
    pageInfoByTypeReducer,
    bannerReducer
});
