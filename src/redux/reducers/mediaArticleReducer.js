import {
  GET_ALL_MEDIA_ARTICLES_SUCCESS,
  GET_ALL_MEDIA_ARTICLES_FAILURE
} from "../actionTypes";

const InitialState = {
  articles: []
};

export const mediaArticleReducer = (state = InitialState, action) => {
  switch (action.type) {
    case GET_ALL_MEDIA_ARTICLES_SUCCESS:
      return {
        ...state,
        articles: action.payload.items
      };

    case GET_ALL_MEDIA_ARTICLES_FAILURE:
      return {
        ...state,
        articles: []
      };

    default:
      return state;
  }
};
