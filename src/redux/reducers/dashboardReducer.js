import {
  SEND_CONFIRM_MAIL_LINK,
  SEND_CONFIRM_MAIL_LINK_SUCCESS,
  CLEAR_EMAIL_FLAGS,
  GET_HOME_VIDEOS,
  GET_HOME_VIDEOS_SUCCESS,
  GET_HOME_VIDEOS_FAILURE,
  CLEAR_DASHBOARD_FLAG,
  GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_SUCCESS,  
  GET_MODULE_WISE_BANNERS,
  GET_MODULE_WISE_BANNERS_SUCCESS,
  GET_MODULE_WISE_BANNERS_FAILURE,
  GET_DASHBOARD_COUNT,
  GET_DASHBOARD_COUNT_SUCCESS,
  GET_DASHBOARD_COUNT_FAILURE,
  GET_EXPERIENCES,
  GET_EXPERIENCES_SUCCESS,
  GET_EXPERIENCES_FAILURE
} from "../actionTypes";

const InitialState = {
  isMailSent: false,
  cityLists: [],
  statusSuccess: false,
  statusError: false,
  success: false,
  error: false,
  experience: [],
};

export const dashboardReducer = (state = InitialState, action) => {
  switch (action.type) {
    case SEND_CONFIRM_MAIL_LINK:
      return {
        ...state
      };

    case SEND_CONFIRM_MAIL_LINK_SUCCESS:
      return {
        ...state,
        isMailSent: true
      };

    case CLEAR_EMAIL_FLAGS:
      return {
        ...state,
        isMailSent: false
      };

    case GET_HOME_VIDEOS:
      return {
        ...state,
        success: false,
        error: false
      };

    case GET_HOME_VIDEOS_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        videos: action.payload.data.getVideoList
      };

    case GET_HOME_VIDEOS_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    case CLEAR_DASHBOARD_FLAG:
      return {
        ...state,
        success: false,
        error: false,
        statusSuccess: false,
        statusError: false
      };

    case GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_SUCCESS:
      return {
        ...state,
        cityLists: action.payload
      };

    case GET_MODULE_WISE_BANNERS:
      const fieldName = action.payload.fieldName;
      return {
        ...state,
        [fieldName]: []
      };

    case GET_MODULE_WISE_BANNERS_SUCCESS:
      const fieldNameSuccess = action.fieldName;
      return {
        ...state,
        [fieldNameSuccess]: action.payload.data.getBannerByModule
      };

    case GET_MODULE_WISE_BANNERS_FAILURE:
      const fieldNameFail = action.fieldName;
      return {
        ...state,
        [fieldNameFail]: []
      };
    case GET_DASHBOARD_COUNT:
      return {
        ...state,
        dashboardCount: {}
      };

    case GET_DASHBOARD_COUNT_SUCCESS:
      return {
        ...state,
        dashboardCount: action.payload
      };

    case GET_DASHBOARD_COUNT_FAILURE:
      return {
        ...state,
        dashboardCount: {}
      };

      case GET_EXPERIENCES: 
      return {
        ...state,
        success: false,
        error: true
      }

    case GET_EXPERIENCES_SUCCESS: 
    
      return {
        ...state,
        success: true,
        error: false,
        experience: action.payload.data.getExperiences
      }

    case GET_EXPERIENCES_FAILURE: 
      return {
        ...state,
        success: false,
        error: true,
      }

    default:
      return state;
  }
};
