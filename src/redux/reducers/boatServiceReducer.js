import {
  ADD_BOAT_SERVICE_SUCCESS,
  ADD_BOAT_SERVICE_FAILURE,
  CLEAR_ADD_BOAT_SERVICE_FLAGS,
  GET_TYPE_WISE_BOAT_SERVICE,
  GET_TYPE_WISE_BOAT_SERVICE_SUCCESS,
  GET_TYPE_WISE_BOAT_SERVICE_FAILURE,
  GET_RECENTLY_ADDEDD_SERVICE,
  GET_RECENTLY_ADDEDD_SERVICE_SUCCESS,
  GET_RECENTLY_ADDEDD_SERVICE_FAILURE,
  CLEAR_SERVICE_FLAG,
  GET_USER_BOAT_SERVICE,
  GET_USER_BOAT_SERVICE_SUCCESS,
  GET_USER_BOAT_SERVICE_FAILURE,
  CLEAR_EDIT_SERVICE,
  GET_MOST_VIEWED_BOAT_SERVICES,
  GET_MOST_VIEWED_BOAT_SERVICES_SUCCESS,
  GET_MOST_VIEWED_BOAT_SERVICES_FAILURE,
  SEARCH_YACHT_SERVICE,
  SEARCH_YACHT_SERVICE_SUCCESS,
  SEARCH_YACHT_SERVICE_FAILURE,
  CLEAR_SEARCH_YACHT_SERVICE_FLAG,
  GET_ALL_BOAT_SERVICE_TYPES,
  GET_ALL_BOAT_SERVICE_TYPES_SUCCESS,
  GET_ALL_BOAT_SERVICE_TYPES_FAILURE,
  EDIT_YACHT_SERVICE,
  EDIT_YACHT_SERVICE_SUCCESS,
  EDIT_YACHT_SERVICE_FAILURE
} from "../actionTypes";
import { renderSelectOptions } from "../../helpers/string";

const InitialState = {
  editYachtService: {},

  isAddSuccess: false,
  isAddError: false,
  providedServices: [],
  getSuccess: false,
  getError: false,
  typeWiseServices: [],
  totalTypeWiseServices: null,

  recentlyAddedService: [],
  totalRecentlyAddedService: null,

  success: false,
  error: false,
  getOneSuccess: false,
  getOneError: false,
  userService: [],
  isLoading: false,
  singleSuccess: false,
  singleError: false,

  searchedYachtServices: [],
  totalSearchedYachtServices: null,
  isYachtSearched: false
};

export const boatServiceReducer = (state = InitialState, action) => {
  switch (action.type) {
    case ADD_BOAT_SERVICE_SUCCESS:
      return {
        ...state,
        isAddSuccess: true
      };

    case ADD_BOAT_SERVICE_FAILURE:
      return {
        ...state,
        isAddError: true
      };

    case CLEAR_ADD_BOAT_SERVICE_FLAGS:
      return {
        ...state,
        isAddSuccess: false,
        isAddError: false
      };

    case GET_TYPE_WISE_BOAT_SERVICE:
      return {
        ...state,
        getSuccess: false,
        getError: false
      };
    case GET_TYPE_WISE_BOAT_SERVICE_SUCCESS:
      return {
        ...state,
        getSuccess: true,
        getError: false,
        typeWiseServices:
          action.payload.data.getYachtServiceByServiceType.items,
        totalTypeWiseServices:
          action.payload.data.getYachtServiceByServiceType.total
      };
    case GET_TYPE_WISE_BOAT_SERVICE_FAILURE:
      return {
        ...state,
        getSuccess: false,
        getError: true
      };
    case GET_RECENTLY_ADDEDD_SERVICE:
      return {
        ...state,
        success: false,
        error: false
      };
    case GET_RECENTLY_ADDEDD_SERVICE_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        recentlyAddedService:
          action.payload.data.getAllYachtServicesByCountry.items,
        totalRecentlyAddedService:
          action.payload.data.getAllYachtServicesByCountry.total
      };
    case GET_RECENTLY_ADDEDD_SERVICE_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };
    case CLEAR_SERVICE_FLAG:
      return {
        ...state,
        isAddSuccess: false,
        isAddError: false,
        getSuccess: false,
        getError: false,
        success: false,
        error: false,
        getOneSuccess: false,
        getOneError: false
      };
    case GET_USER_BOAT_SERVICE:
      return {
        ...state,
        getOneSuccess: false,
        isLoading: true,
        getOneError: false
      };
    case GET_USER_BOAT_SERVICE_SUCCESS:

      const data = action.payload.data.yachtServiceByUser[0]
      const service = { ...data, serviceProvide: renderSelectOptions(data.serviceProvide, "name", "id") }

      return {
        ...state,
        getOneSuccess: true,
        getOneError: false,
        isLoading: false,
        userService: service
      };
    case GET_USER_BOAT_SERVICE_FAILURE:
      return {
        ...state,
        getOneSuccess: false,
        isLoading: false,
        getOneError: true
      };
    case GET_MOST_VIEWED_BOAT_SERVICES:
      return {
        ...state,
        geMostViewedSuccess: false,
        isLoading: true,
        geMostViewedError: false
      };
    case GET_MOST_VIEWED_BOAT_SERVICES_SUCCESS:
      return {
        ...state,
        geMostViewedSuccess: true,
        geMostViewedError: false,
        isLoading: false,
        mostViewedBoatServices:
          action.payload.data.mostViewedYachtServices.items,
        total:
          action.payload.data.mostViewedYachtServices.total
      };
    case GET_MOST_VIEWED_BOAT_SERVICES_FAILURE:
      return {
        ...state,
        geMostViewedSuccess: false,
        isLoading: false,
        geMostViewedError: true
      };
    case CLEAR_EDIT_SERVICE:
      return {
        ...state,
        userService: {}
      };

    case SEARCH_YACHT_SERVICE:
      return {
        ...state,
        isYachtSearched: false,
        searchedYachtServices: [],
        totalSearchedYachtServices: null
      };

    case SEARCH_YACHT_SERVICE_SUCCESS:
      return {
        ...state,
        isYachtSearched: true,
        searchedYachtServices: action.payload.items,
        totalSearchedYachtServices: action.payload.total
      };

    case SEARCH_YACHT_SERVICE_FAILURE:
      return {
        ...state,
        isYachtSearched: false,
        searchedYachtServices: []        
      };

    case CLEAR_SEARCH_YACHT_SERVICE_FLAG:
      return {
        ...state,
        isYachtSearched: false,
        singleSuccess: false,
        singleError: false,
      };

    case GET_ALL_BOAT_SERVICE_TYPES: {
      return {
        ...state,
        providedServices: []
      }
    }

    case GET_ALL_BOAT_SERVICE_TYPES_SUCCESS: {
      return {
        ...state,
        providedServices: action.payload.items
      }
    }

    case GET_ALL_BOAT_SERVICE_TYPES_FAILURE: {
      return {
        ...state,
        providedServices: []
      }
    }

    case EDIT_YACHT_SERVICE: {
      return {
        ...state,
        isLoading: true,
        singleSuccess: false,
        singleError: false,
        editYachtService: {}
      }
    }

    case EDIT_YACHT_SERVICE_SUCCESS: {
      return {
        singleSuccess: false,
        isLoading: false,
        singleError: true,
        editYachtService: action.payload.data.EditYachtService
      }
    }

    case EDIT_YACHT_SERVICE_FAILURE: {
      return {
        singleSuccess: false,
        isLoading: false,
        singleError: true,
        editYachtService: {}
      }
    }

    default:
      return state;
  }
};
