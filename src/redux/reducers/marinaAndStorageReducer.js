import {
  CLEAR_MARINA_FLAG,
  ADD_MARINA_AND_STORAGE,
  ADD_MARINA_AND_STORAGE_SUCCESS,
  ADD_MARINA_AND_STORAGE_FAILURE,
  GET_ALL_MARINA,
  GET_ALL_MARINA_SUCCESS,
  GET_ALL_MARINA_FAILURE,
  GET_TYPE_WISE_LOOKUP_SUCCESS,
  GET_TYPE_WISE_LOOKUP_FAILURE,
  GET_TYPE_WISE_LOOKUP,
  GET_EXPLORE_MARINA,
  GET_EXPLORE_MARINA_SUCCESS,
  GET_EXPLORE_MARINA_FAILURE,
  GET_USER_MARINA_STORAGE,
  GET_USER_MARINA_STORAGE_SUCCESS,
  GET_USER_MARINA_STORAGE_FAILURE,
  GET_RECENTLY_ADDED_MARINA_STORAGE,
  GET_RECENTLY_ADDED_MARINA_STORAGE_SUCCESS,
  GET_RECENTLY_ADDED_MARINA_STORAGE_FAILURE,
  GET_SINGLE_MARINA_STORAGE,
  GET_SINGLE_MARINA_STORAGE_SUCCESS,
  GET_SINGLE_MARINA_STORAGE_FAILURE,
  UPDATE_MARINA_STORAGE,
  UPDATE_MARINA_STORAGE_SUCCESS,
  UPDATE_MARINA_STORAGE_FAILURE,
  CLEAR_EDIT_MARINA_FLAG,
  DELETE_MARINA_STORAGE,
  DELETE_MARINA_STORAGE_SUCCESS,
  DELETE_MARINA_STORAGE_FAILURE,
  GET_MOST_VIEWED_MARINA_STORAGE,
  GET_MOST_VIEWED_MARINA_STORAGE_SUCCESS,
  GET_MOST_VIEWED_MARINA_STORAGE_FAILURE,
  SEARCH_MARINA_AND_STORAGE,
  SEARCH_MARINA_AND_STORAGE_SUCCESS,
  SEARCH_MARINA_AND_STORAGE_FAILURE,
  CLEAR_SEARCH_MARINA_AND_STORAGE_FLAG,
  GET_MORE_SERVICE,
  GET_MORE_SERVICE_SUCCESS,
  GET_MORE_SERVICE_FAILURE,
  GET_TOP_RATED_MARINA_STORAGE,
  GET_TOP_RATED_MARINA_STORAGE_SUCCESS,
  GET_TOP_RATED_MARINA_STORAGE_FAILURE,
  GET_MARINA_STORAGE_All_SERVICES,
  GET_MARINA_STORAGE_All_SERVICES_SUCCESS,
  GET_MARINA_STORAGE_All_SERVICES_FAILURE,
  GET_MARINA_BY_TYPE,
  GET_MARINA_BY_TYPE_SUCCESS,
  GET_MARINA_BY_TYPE_FAILURE,
} from "../actionTypes";
import { lookupTypes } from "../../util/enums/enums";

const InitialState = {
  success: false,
  error: false,
  createError: false,
  createSuccess: false,
  marinaStorage: [],
  totalMarinaStorage: null,
  getSuccess: false,
  getError: false,
  marinaTypeWiseLookUps: [],
  exploreMarina: [],
  totalExploreMarina: null,
  getRecentlySuccess: false,
  getRecentlyError: false,
  recentMarinaStorage: [],
  singleSuccess: false,
  singleError: false,
  isLoading: false,
  updateSuccess: false,
  updateError: false,
  mostViewedSuccess: false,
  mostViewedFailure: false,

  searchedMarinaStorage: [],
  totalSearchedMarinaStorage: null,
  isMarinaStorageSearched: false,

  moreMarinaService: [],
  topRatedMarinaStorage: [],
  topRatedMarinaTotal: 0,
  marinaServiceList: [],
  mostViewedMarinaStorage: [],
  marinaServiceListTotal: 0,
  marinaByType: []
};

export const marinaAndStorageReducer = (state = InitialState, action) => {
  switch (action.type) {

    // START

    case GET_TYPE_WISE_LOOKUP:
      return {
        ...state,
        success: false,
        error: false
      };

    case GET_TYPE_WISE_LOOKUP_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        marinaTypeWiseLookUps:
          action.value === lookupTypes.YOU_ARE_AN ? action.payload : []
      };

    case GET_TYPE_WISE_LOOKUP_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    // END

    // START GET EXPLORE MARINA

    case GET_EXPLORE_MARINA:
      return {
        ...state,
        success: false,
        error: false
      };

    case GET_EXPLORE_MARINA_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        exploreMarina: action.payload.data.getMarinasByType.items,
        totalExploreMarina: action.payload.data.getMarinasByType.total
      };

    case GET_EXPLORE_MARINA_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    // END GET MARINA EXPLORE

    case ADD_MARINA_AND_STORAGE:
      return {
        ...state,
        createSuccess: false,
        createError: false
      };

    case ADD_MARINA_AND_STORAGE_SUCCESS:
      return {
        ...state,
        createSuccess: true,
        createError: false
      };

    case ADD_MARINA_AND_STORAGE_FAILURE:
      return {
        ...state,
        createSuccess: false,
        createError: true
      };
    case CLEAR_MARINA_FLAG:
      return {
        ...state,
        isLoading: false,
        createSuccess: false,
        createError: false,

        success: false,
        error: false,

        getRecentlySuccess: false,
        getRecentlyError: false,

        singleSuccess: false,
        singleError: false,

        updateSuccess: false,
        updateError: false,

        deleteSuccess: false,
        deleteError: false,

        mostViewedSuccess: false,
        mostViewedFailure: false
      };
    case GET_ALL_MARINA:
      return {
        ...state,
        success: false,
        error: false
      };
    case GET_ALL_MARINA_SUCCESS:
      return {
        ...state,
        success: true,
        error: false
      };
    case GET_ALL_MARINA_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };
    case GET_USER_MARINA_STORAGE:
      return {
        ...state,
        getSuccess: false,
        getError: false
      };
    case GET_USER_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        getSuccess: true,
        getError: false,
        marinaStorage: action.payload.data.marinaStorageByUser.items,
        totalMarinaStorage: action.payload.data.marinaStorageByUser.total
      };
    case GET_USER_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        getSuccess: false,
        getError: true
      };
    case GET_RECENTLY_ADDED_MARINA_STORAGE:
      return {
        ...state,
        getRecentlySuccess: false,
        getRecentlyError: false
      };
    case GET_RECENTLY_ADDED_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        getRecentlySuccess: true,
        getRecentlyError: false,
        recentMarinaStorage:
          action.payload.data.getAllMarinaStorageByCountry.items,
        totalRecentMarinaCount:
          action.payload.data.getAllMarinaStorageByCountry.total
      };
    case GET_RECENTLY_ADDED_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        getRecentlySuccess: false,
        getRecentlyError: true
      };
    case GET_SINGLE_MARINA_STORAGE:
      return {
        ...state,
        isLoading: true,
        singleSuccess: false,
        singleError: false,
        editMarina: {}
      };
    case GET_SINGLE_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        singleSuccess: true,
        isLoading: false,
        singleError: false,
        editMarina: action.payload.data.editMarina
      };
    case GET_SINGLE_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        singleSuccess: false,
        isLoading: false,
        singleError: true,
        editMarina: {}
      };
    case UPDATE_MARINA_STORAGE:
      return {
        ...state,
        isLoading: true,
        updateSuccess: false,
        updateError: false
      };
    case UPDATE_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        updateSuccess: true,
        updateError: false
      };
    case UPDATE_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        updateSuccess: false,
        updateError: true,
        isLoading: false
      };
    case DELETE_MARINA_STORAGE:
      return {
        ...state,
        isLoading: true,
        deleteSuccess: false,
        deleteError: false
      };
    case DELETE_MARINA_STORAGE_SUCCESS:
      const { marinaStorage } = state
      return {
        ...state,
        isLoading: false,
        deleteSuccess: true,
        deleteError: false,
        marinaStorage: marinaStorage.filter(item => item.id !== action.payload.id)
      };
    case DELETE_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        deleteSuccess: false,
        deleteError: true,
        isLoading: false
      };
    case CLEAR_EDIT_MARINA_FLAG:
      return {
        ...state,
        editMarina: {}
      };
    case GET_MOST_VIEWED_MARINA_STORAGE:
      return {
        ...state,
        mostViewedSuccess: false,
        mostViewedFailure: false
      };
    case GET_MOST_VIEWED_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        mostViewedSuccess: true,
        mostViewedFailure: false,
        mostViewedMarinaStorage:
          action.payload,
        mostViewedMarinaStorageTotal:
          action.total
      };
    case GET_MOST_VIEWED_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        mostViewedSuccess: false,
        mostViewedFailure: true
      };

    case SEARCH_MARINA_AND_STORAGE:
      return {
        ...state,
        isMarinaStorageSearched: false,
        searchedMarinaStorage: [],
        totalSearchedMarinaStorage: null
      };

    case SEARCH_MARINA_AND_STORAGE_SUCCESS:
      return {
        ...state,
        isMarinaStorageSearched: true,
        searchedMarinaStorage: action.payload.items,
        totalSearchedMarinaStorage: action.payload.total
      };

    case SEARCH_MARINA_AND_STORAGE_FAILURE:
      return {
        ...state,
        isMarinaStorageSearched: false,
        searchedMarinaStorage: []
      };

    case CLEAR_SEARCH_MARINA_AND_STORAGE_FLAG:
      return {
        ...state,
        isMarinaStorageSearched: false,
      };
    case GET_MORE_SERVICE:
      return {
        ...state,
        moreMarinaService: []
      };
    case GET_MORE_SERVICE_SUCCESS:
      return {
        ...state,
        moreMarinaService: action.payload
      };
    case GET_MORE_SERVICE_FAILURE:
      return {
        ...state,
        moreMarinaService: []
      };

    case GET_TOP_RATED_MARINA_STORAGE:
      return {
        ...state,
        topRatedMarinaStorage: [],
        topRatedMarinaTotal: 0
      };
    case GET_TOP_RATED_MARINA_STORAGE_SUCCESS:
      return {
        ...state,
        topRatedMarinaStorage: action.payload.items,
        topRatedMarinaTotal: action.payload.total
      };
    case GET_TOP_RATED_MARINA_STORAGE_FAILURE:
      return {
        ...state,
        topRatedMarinaStorage: [],
        topRatedMarinaTotal: 0
      };

    /** Listing of services  */
    case GET_MARINA_STORAGE_All_SERVICES:
      return {
        ...state,
        marinaServiceList: [],
        marinaServiceListTotal: 0
      };
    case GET_MARINA_STORAGE_All_SERVICES_SUCCESS:
      return {
        ...state,
        marinaServiceList: action.payload ? action.payload.items : [],
        marinaServiceListTotal: action.payload ? action.payload.total : 0
      };
    case GET_MARINA_STORAGE_All_SERVICES_FAILURE:
      return {
        ...state,
        marinaServiceList: [],
        marinaServiceListTotal: 0
      };

    case GET_MARINA_BY_TYPE:
      return {
        ...state,
        success: false,
        error: false,
        marinaByType: [],
      }

    case GET_MARINA_BY_TYPE_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        marinaByType: action.payload.items
      }

    case GET_MARINA_BY_TYPE_FAILURE:
      return {
        ...state,
        success: false,
        error: false
      }


    default:
      return state;
  }
};
