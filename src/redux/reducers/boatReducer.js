import {
  GET_ALL_BOAT_LOOKUPS,
  GET_ALL_BOAT_LOOKUPS_SUCCESS,
  GET_ALL_BOAT_LOOKUPS_FAILURE,
  GET_ALL_BOATS_BY_USER,
  GET_ALL_BOATS_BY_USER_SUCCESS,
  GET_ALL_BOATS_BY_USER_FAILURE,
  SEARCH_BOAT,
  SEARCH_BOAT_SUCCESS,
  SEARCH_BOAT_FAILURE,
  GLOBAL_BOAT_SEARCH,
  GLOBAL_BOAT_SEARCH_SUCCESS,
  GLOBAL_BOAT_SEARCH_FAILURE,
  CLEAR_SEARCH_BOAT_FLAG,
  GET_BOAT_TYPE,
  GET_BOAT_TYPE_SUCCESS,
  GET_BOAT_TYPE_FAILURE,
  RECENT_SEARCH_SUCCESS,
  RECENT_SEARCH_FAILURE,
  MULTI_SEARCH,
  MULTI_SEARCH_SUCCESS,
  MULTI_SEARCH_FAILURE,
  CLEAR_MULTI_SEARCH,
  GET_SINGLE_BOAT_SUCCESS,
  GET_SINGLE_BOAT_FAILURE,
  TOGGLE_BOAT_STATUS_SUCCESS,
  TOGGLE_BOAT_STATUS_FAILURE,
  DELETE_BOAT_SUCCESS,
  CLEAR_BOAT_DELETE_FLAG,
  GET_BOAT_BY_TYPE_SUCCESS,
  GET_LATEST_BOATS_SUCCESS,
  GET_POPULAR_BOATS_SUCCESS,
  GET_TOP_RATED_BOATS_SUCCESS,
  UPDATE_BOAT_SUCCESS,
  ADD_BOAT_SUCCESS,
  CLEAR_BOAT_ADD_FLAG,
  CLEAR_BOAT_UPDATE_FLAG,
  UPDATE_BOAT_FAILURE,
  ADD_BOAT_FAILURE,
  CLEAR_EXISTING_BOAT,
  CLEAR_SEARCHED_RESULTS,
  GET_CITY_WISE_BOATS_SUCCESS,
  CLEAR_CITY_WISE_BOATS,
  GET_CATEGORY_WISE_BOATS_SUCCESS,
  CLEAR_CATEGOEY_WISE_BOATS,
  CREATE_AUCTION_ROOM,
  CREATE_AUCTION_ROOM_SUCCESS,
  CREATE_AUCTION_ROOM_FAILURE,
  CLEAR_AUCTION_FLAG,
  GET_USER_AUCTION_ROOMS,
  GET_USER_AUCTION_ROOMS_FAILURE,
  GET_USER_AUCTION_ROOMS_SUCCESS,
  GET_ALL_AUCTION_BID,
  GET_ALL_AUCTION_BID_SUCCESS,
  GET_ALL_AUCTION_BID_FAILURE,
  CLEAR_BID_FLAG,
  GET_ALL_AUCTION_ROOMS,
  GET_ALL_AUCTION_ROOMS_SUCCESS,
  GET_ALL_AUCTION_ROOMS_FAILURE,
  GET_BOAT_BY_TYPE,
  CREATE_AUCTION_BID,
  CREATE_AUCTION_BID_SUCCESS,
  CREATE_AUCTION_BID_FAILURE,
  CLEAR_FLAG_AUCTION_BID,
  GET_BOAT_SHIPPERS,
  GET_BOAT_SHIPPERS_SUCCESS,
  GET_BOAT_SHIPPERS_FAILURE,
  BOAT_BY_CITIES,
  BOAT_BY_CITIES_SUCCESS,
  BOAT_BY_CITIES_FAILURE,
  BOAT_BY_CITIES_CLEAR,
  GET_BOAT_TYPE_FOR_MANUFACTURE,
  GET_BOAT_TYPE_FOR_MANUFACTURE_SUCCESS,
  GET_BOAT_TYPE_FOR_MANUFACTURE_FAILURE,
  GET_SINGLE_BOAT,
  GET_BOAT_SEARCH_MINIMUM_VALUE,
  GET_BOAT_SEARCH_MINIMUM_VALUE_SUCCESS,
  GET_BOAT_SEARCH_MINIMUM_VALUE_FAILURE,
} from "../actionTypes";
import _ from "lodash";

const InitialState = {
  success: false,
  error: false,
  boat: {},
  userBoats: [],
  totalUserBoats: null,
  auctionSuccess: false,
  auctionError: false,

  searchedBoats: [],
  searchedBoatsCount: null,

  boatTypeSuccess: false,
  boatTypes: [],
  boatTypeError: false,
  isSearched: false,

  boatTypesForManufacture: [],

  recentSearchResults: [],
  multipleSearchResults: {},

  isBoatCreated: false,
  isBoatDeleted: false,
  isBoatUpdated: false,
  isBoatCreateAndUpdateError: false,

  featureBoats: [],
  totalFeatureBoats: null,
  bestDealBoats: [],
  totalBestDealBoats: null,
  mustBuyBoats: [],
  totalMustBuyBoats: null,

  latestBoats: [],
  totalLatestBoats: null,
  popularBoats: [],
  totalPopularBoats: null,
  topRatedBoats: [],
  totalTopRatedBoats: null,
  cityWiseBoats: [],
  totalCityWiseBoats: null,
  totalCategoryWiseBoats: null,

  boatShipperLoad: false,
  boatShipperSuccess: false,
  boatShipperError: false,
  boatShipperList: [],

  boatByCityLoad: false,
  boatByCitySuccess: false,
  boatByCityError: {},
  boatByCityData: [],

  boatSearchValues: {}
};

export const boatReducer = (state = InitialState, action) => {
  switch (action.type) {
    case GET_ALL_BOAT_LOOKUPS:
      return {
        ...state,
        success: false,
        error: false,
        boatLookups: []
      };

    case GET_ALL_BOAT_LOOKUPS_SUCCESS:
      const boatLookUps = action.data.data.getBoatLookUps;

      let filteredBoatLookUps = {
        boatStatus: [],
        boatParking: [],
        hullMaterial: [],
        fuelType: [],
        engineDrives: [],
        engineStroke: [],
        boatType: [],
        mainSaloonConvertible: [],
        mainSaloon: [],
        amenitiesItems: [],
        accessoriesItems: [],
        navigationEq: [],
        manufacturerBoatStatus: [],
      };

      boatLookUps.forEach(item => {
        switch (item.typeName) {
          case "Boat Status":
            filteredBoatLookUps.boatStatus.push(item);
            break;
          case "Boat Parking":
            filteredBoatLookUps.boatParking.push(item);
            break;
          case "Hull Material":
            filteredBoatLookUps.hullMaterial.push(item);
            break;
          case "Fuel Type":
            filteredBoatLookUps.fuelType.push(item);
            break;
          case "Engine Drives":
            filteredBoatLookUps.engineDrives.push(item);
            break;
          case "Engine Stroke":
            filteredBoatLookUps.engineStroke.push(item);
            break;
          case "Main Saloon":
            filteredBoatLookUps.mainSaloon.push(item);
            break;
          case "Main Saloon Convertible":
            filteredBoatLookUps.mainSaloonConvertible.push(item);
            break;
          case "Boat Type":
            filteredBoatLookUps.boatType.push(item);
            break;
          case "Amenities":
            filteredBoatLookUps.amenitiesItems.push(item);
            break;
          case "Accessories":
            filteredBoatLookUps.accessoriesItems.push(item);
            break;
          case "Navigation Equipment":
            filteredBoatLookUps.navigationEq.push(item);
            break;
          case "Boat Status For Manufacturer":
            filteredBoatLookUps.manufacturerBoatStatus.push(item);
            break;
          default:
            break;
        }
      });
      return {
        ...state,
        success: true,
        error: false,
        boatLookups: filteredBoatLookUps
      };

    case GET_ALL_BOAT_LOOKUPS_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    case GET_ALL_BOATS_BY_USER:
      return {
        ...state,
        success: false,
        error: false
      };

    case GET_ALL_BOATS_BY_USER_SUCCESS:
      return {
        ...state,
        userBoats: action.payload.items,
        totalUserBoats: action.payload.total
      };

    case GET_ALL_BOATS_BY_USER_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    case SEARCH_BOAT_SUCCESS:
      return {
        ...state,
        isSearched: true,
        searchedBoats: action.payload.items,
        searchedBoatsCount: action.payload.total
      };

    case SEARCH_BOAT_FAILURE:
      return {
        ...state,
        isSearched: false
      };

    case GLOBAL_BOAT_SEARCH_SUCCESS:
      return {
        ...state,
        isSearched: true,
        searchedBoats: action.payload
      };

    case GLOBAL_BOAT_SEARCH_FAILURE:
      return {
        ...state,
        isSearched: false
      };

    case CLEAR_SEARCH_BOAT_FLAG:
      return {
        ...state,
        isSearched: false
      };

    case GET_BOAT_TYPE:
      return {
        ...state,
        boatTypeSuccess: false,
        boatTypes: [],
        boatTypeError: false
      };
    case GET_BOAT_TYPE_SUCCESS: {
      return {
        ...state,
        boatTypeSuccess: true,
        boatTypes: action.payload,
        boatTypeError: false
      };
    }
    case GET_BOAT_TYPE_FAILURE:
      return {
        ...state,
        boatTypeSuccess: false,
        boatTypesForManufacture: action.payload,
        boatTypeError: true
      };

    case GET_BOAT_TYPE_FOR_MANUFACTURE:
      return {
        ...state,
        boatTypeSuccess: false,
        boatTypesForManufacture: [],
        boatTypeError: false
      };
    case GET_BOAT_TYPE_FOR_MANUFACTURE_SUCCESS: {
      return {
        ...state,
        boatTypeSuccess: true,
        boatTypesForManufacture: action.payload,
        boatTypeError: false
      };
    }
    case GET_BOAT_TYPE_FOR_MANUFACTURE_FAILURE:
      return {
        ...state,
        boatTypeSuccess: false,
        boatTypeError: true
      };

    case RECENT_SEARCH_SUCCESS:
      return {
        ...state,
        recentSearchResults: action.payload
      };

    case RECENT_SEARCH_FAILURE:
      return {
        ...state,
        recentSearchResults: []
      };

    case MULTI_SEARCH:
      return {
        ...state,
        multipleSearchResults: {}
      };

    case MULTI_SEARCH_SUCCESS:
      return {
        ...state,
        multipleSearchResults: action.payload
      };

    case MULTI_SEARCH_FAILURE:
      return {
        ...state,
        multipleSearchResults: {}
      };

    case CLEAR_MULTI_SEARCH:
      return {
        ...state,
        multipleSearchResults: {}
      };

    case GET_SINGLE_BOAT:
      return {
        ...state,
        boat: {},
      };

    case GET_SINGLE_BOAT_SUCCESS:
      return {
        ...state,
        boat: action.payload,
        createBidSuccess: false
      };

    case GET_SINGLE_BOAT_FAILURE:
      return {
        ...state,
        boat: {},
        createBidSuccess: false
      };

    case TOGGLE_BOAT_STATUS_SUCCESS:
      return {
        ...state
      };

    case ADD_BOAT_SUCCESS:
      return {
        ...state,
        isBoatCreated: true
      };

    case ADD_BOAT_FAILURE:
      return {
        ...state,
        isBoatCreateAndUpdateError: true
      };

    case CLEAR_BOAT_ADD_FLAG:
      return {
        ...state,
        isBoatCreated: false,
        isBoatCreateAndUpdateError: false
      };

    case DELETE_BOAT_SUCCESS:
      const { userBoats } = state;
      return {
        ...state,
        isBoatDeleted: true,
        userBoats: userBoats.filter(boat => boat.id !== action.payload.id)
      };

    case CLEAR_BOAT_DELETE_FLAG:
      return {
        ...state,
        isBoatDeleted: false
      };

    case UPDATE_BOAT_SUCCESS:
      return {
        ...state,
        isBoatUpdated: true
      };

    case UPDATE_BOAT_FAILURE:
      return {
        ...state,
        isBoatCreateAndUpdateError: true
      };

    case CLEAR_BOAT_UPDATE_FLAG:
      return {
        ...state,
        isBoatUpdated: false,
        isBoatCreateAndUpdateError: false
      };

    case CLEAR_EXISTING_BOAT:
      return {
        ...state,
        boat: {}
      };
    case GET_BOAT_BY_TYPE:
      return {
        ...state,
        featureBoats: [],
      };

    // GET BOAT BY TYPE
    case GET_BOAT_BY_TYPE_SUCCESS:
      switch (action.fieldName) {
        case "featureStatus":
          return {
            ...state,
            featureBoats: action.payload.items,
            totalFeatureBoats: action.payload.total
          };
        case "bestDealStatus":
          return {
            ...state,
            bestDealBoats: action.payload.items,
            totalBestDealBoats: action.payload.total
          };
        case "mustBuyStatus":
          return {
            ...state,
            mustBuyBoats: action.payload.items,
            totalMustBuyBoats: action.payload.total
          };

        default:
          return {
            ...state
          };
      }

    case GET_LATEST_BOATS_SUCCESS:
      return {
        ...state,
        latestBoats: action.payload.items,
        totalLatestBoats: action.payload.total
      };

    case GET_POPULAR_BOATS_SUCCESS:
      return {
        ...state,
        popularBoats: action.payload.items,
        totalPopularBoats: action.payload.total
      };

    case GET_TOP_RATED_BOATS_SUCCESS:
      return {
        ...state,
        topRatedBoats: action.payload.items,
        totalTopRatedBoats: action.payload.total
      };

    case CLEAR_SEARCHED_RESULTS:
      return {
        ...state,
        searchedBoats: [],
        multipleSearchResults: {}
      };

    case GET_CITY_WISE_BOATS_SUCCESS:
      return {
        ...state,
        cityWiseBoats: action.payload.items,
        totalCityWiseBoats: action.payload.total
      };

    case CLEAR_CITY_WISE_BOATS:
      return {
        ...state,
        cityWiseBoats: [],
        totalCityWiseBoats: 0
      };

    case GET_CATEGORY_WISE_BOATS_SUCCESS:
      return {
        ...state,
        categoryWiseBoat: action.payload.items,
        totalCategoryWiseBoats: action.payload.total
      };
    case CLEAR_CATEGOEY_WISE_BOATS:
      return {
        ...state,
        categoryWiseBoat: [],
        totalCategoryWiseBoats: 0
      };

    case CREATE_AUCTION_ROOM:
      return {
        ...state
      };
    case CREATE_AUCTION_ROOM_SUCCESS:
      return {
        ...state,
        auctionSuccess: true,
        auctionError: false
      };
    case CREATE_AUCTION_ROOM_FAILURE:
      return {
        ...state,
        auctionSuccess: false,
        auctionError: true
      };

    case CLEAR_AUCTION_FLAG:
      return {
        ...state,
        auctionSuccess: false,
        auctionError: false,
        getAuctionSuccess: false,
        getAuctionError: false
      };
    case GET_USER_AUCTION_ROOMS:
      return {
        ...state,
        getAuctionSuccess: false,
        getAuctionError: false
      };
    case GET_USER_AUCTION_ROOMS_SUCCESS:
      return {
        ...state,
        getAuctionSuccess: true,
        getAuctionError: false,
        userAuctions: action.payload.data.getAuctionRoomsByUser.items
      };
    case GET_USER_AUCTION_ROOMS_FAILURE:
      return {
        ...state,
        getAuctionSuccess: false,
        getAuctionError: true
      };
    case GET_ALL_AUCTION_BID:
      return {
        ...state,
        allBids: [],
        getBidSuccess: false,
        getBidFailure: false
      };
    case GET_ALL_AUCTION_BID_SUCCESS:

      return {
        ...state,
        allBids: action.payload,
        getBidSuccess: true,
        getBidFailure: false
      };
    case GET_ALL_AUCTION_BID_FAILURE:
      return {
        ...state,
        allBids: [],
        getBidSuccess: false,
        getBidFailure: true
      };
    case CLEAR_BID_FLAG:
      return {
        ...state,
        getBidSuccess: false,
        getBidFailure: false
      };
    case GET_ALL_AUCTION_ROOMS:
      return {
        ...state,
        getAuctionSuccess: false,
        getAuctionError: true,
        allAuctionRooms: []
      };
    case GET_ALL_AUCTION_ROOMS_SUCCESS:
      return {
        ...state,
        getAuctionSuccess: false,
        getAuctionError: true,
        allAuctionRooms: action.payload.data.getAllAuctionRoom.items,
        allAuctionRoomCounts: action.payload.data.getAllAuctionRoom.total
      };
    case GET_ALL_AUCTION_ROOMS_FAILURE:
      return {
        ...state,
        getAuctionSuccess: false,
        getAuctionError: true
      };

    case CREATE_AUCTION_BID:
      return {
        ...state,
        createBidSuccess: false,
        createBidFailure: false
      };
    case CREATE_AUCTION_BID_SUCCESS:
      return {
        ...state,
        createBidSuccess: true,
        createBidFailure: false
      };
    case CREATE_AUCTION_BID_FAILURE:
      return {
        ...state,
        createBidSuccess: false,
        createBidFailure: true
      };
    case CLEAR_FLAG_AUCTION_BID:
      return {
        ...state,
        createBidSuccess: false,
      };
    case GET_BOAT_SHIPPERS:
      return {
        ...state,
        boatShipperLoad: true,
        boatShipperSuccess: false,
        boatShipperError: false,
        boatShipperList: []
      };

    case GET_BOAT_SHIPPERS_SUCCESS:
      return {
        ...state,
        boatShipperLoad: false,
        boatShipperSuccess: true,
        boatShipperError: false,
        boatShipperList: action.data.data.getAllShipment
      };

    case GET_BOAT_SHIPPERS_FAILURE:
      return {
        ...state,
        boatShipperLoad: false,
        boatShipperSuccess: false,
        boatShipperError: true,
        boatShipperList: []
      };

    case BOAT_BY_CITIES:
      return {
        ...state,
        boatByCityLoad: true,
      };

    case BOAT_BY_CITIES_SUCCESS:
      return {
        ...state,
        boatByCityLoad: false,
        boatByCitySuccess: true,
        boatByCityData: action.data.boatByCities
      };

    case BOAT_BY_CITIES_FAILURE:
      return {
        ...state,
        boatByCityLoad: false,
        boatByCitySuccess: false,
        boatByCityError: action.error
      };

    case BOAT_BY_CITIES_CLEAR:
      return {
        ...state,
        boatByCityLoad: false,
        boatByCitySuccess: false,
        boatByCityData: [],
        boatByCityError: {}
      };

    case GET_BOAT_SEARCH_MINIMUM_VALUE:
      return {
        ...state,
        boatSearchValues: {}
      };

    case GET_BOAT_SEARCH_MINIMUM_VALUE_SUCCESS:
      return {
        ...state,
        boatSearchValues: action.payload[0]
      };

    case GET_BOAT_SEARCH_MINIMUM_VALUE_FAILURE:
      return {
        ...state,
      };

    default:
      return state;
  }
};
