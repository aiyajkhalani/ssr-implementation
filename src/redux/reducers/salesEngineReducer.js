import {
    GET_SALES_ENGINE_BY_BUYER, GET_SALES_ENGINE_BY_BUYER_SUCCESS, GET_SALES_ENGINE_BY_BUYER_FAILURE, GET_SALES_ENGINE_BY_SELLER, GET_SALES_ENGINE_BY_SELLER_SUCCESS, GET_SALES_ENGINE_BY_SELLER_FAILURE, GET_SALES_ENGINE_BY_SURVEYOR, GET_SALES_ENGINE_BY_SURVEYOR_SUCCESS, GET_SALES_ENGINE_BY_SURVEYOR_FAILURE, GET_SALES_ENGINE_BY_SHIPPER, GET_SALES_ENGINE_BY_SHIPPER_SUCCESS, GET_SALES_ENGINE_BY_SHIPPER_FAILURE, GET_SINGLE_SALES_ENGINE, GET_SINGLE_SALES_ENGINE_SUCCESS, GET_SINGLE_SALES_ENGINE_FAILURE, CLEAR_SALES_ENGINES, CREATE_SALES_ENGINE, CREATE_SALES_ENGINE_SUCCESS, CREATE_SALES_ENGINE_FAILURE, CLEAR_CREATE_SALES_ENGINE_FLAG, GET_ALL_SURVEYORS_SUCCESS, GET_ALL_SURVEYORS, GET_ALL_SURVEYORS_FAILURE, ASSIGN_SURVEYOR, ASSIGN_SURVEYOR_SUCCESS, ASSIGN_SURVEYOR_FAILURE, PAYMENT_REQUEST, PAYMENT_REQUEST_SUCCESS, PAYMENT_REQUEST_FAILURE, GET_AGREEMENTS_CONTENTS, GET_AGREEMENTS_CONTENTS_SUCCESS, GET_AGREEMENTS_CONTENTS_FAILURE, CHECK_AGREEMENT, CHECK_AGREEMENT_SUCCESS, CHECK_AGREEMENT_FAILURE, SALES_ENGINE_ASSIGN_SHIPPER_REQUEST, SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_SUCCESS, SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_FAILURE, CLEAR_GET_SINGLE_SALES_ENGINE_FLAG, SALES_ENGINE_SURVEYOR_ACCEPT_BUYER, SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_SUCCESS, SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_FAILURE, SALES_ENGINE_SURVEYOR_DECLINE_BUYER, SALES_ENGINE_SURVEYOR_DECLINE_BUYER_SUCCESS, SALES_ENGINE_SURVEYOR_DECLINE_BUYER_FAILURE, CREATE_SURVEYOR_REPORT, CREATE_SURVEYOR_REPORT_SUCCESS, CREATE_SURVEYOR_REPORT_FAILURE, SURVEYOR_REPORT_SUBMIT, SURVEYOR_REPORT_SUBMIT_SUCCESS, SURVEYOR_REPORT_SUBMIT_FAILURE, CLEAR_REPORT_FLAG, GET_SURVEYOR_REPORT_SUCCESS, GET_SURVEYOR_REPORT_FAILURE, GET_SURVEYOR_REPORT,
    GET_SALES_ENGINE_BY_BOAT,
    GET_SALES_ENGINE_BY_BOAT_SUCCESS,
    GET_SALES_ENGINE_BY_BOAT_FAILURE,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST_SUCCESS,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST_FAILURE,
    SHIPPER_DECLINE_SHIPMENT_REQUEST,
    SHIPPER_DECLINE_SHIPMENT_REQUEST_SUCCESS,
    SHIPPER_DECLINE_SHIPMENT_REQUEST_FAILURE,
    ADD_SHIPMENT_PROPOSAL,
    ADD_SHIPMENT_PROPOSAL_SUCCESS,
    ADD_SHIPMENT_PROPOSAL_FAILURE,
    GET_SINGLE_SHIPMENT_PROPOSAL,
    GET_SINGLE_SHIPMENT_PROPOSAL_SUCCESS,
    GET_SINGLE_SHIPMENT_PROPOSAL_FAILURE,
    UPDATE_SHIPMENT_PROPOSAL,
    UPDATE_SHIPMENT_PROPOSAL_SUCCESS,
    UPDATE_SHIPMENT_PROPOSAL_FAILURE,
    GET_ALL_SHIPMENT_PROPOSAL,
    GET_ALL_SHIPMENT_PROPOSAL_SUCCESS,
    GET_ALL_SHIPMENT_PROPOSAL_FAILURE,
    CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG,
    ADD_NEGOTIABLE_PRICE,
    ADD_NEGOTIABLE_PRICE_SUCCESS,
    ADD_NEGOTIABLE_PRICE_FAILURE,
    CLEAR_SALES_ENGINE_MY_BOAT_FLAG,
    ADD_SURVEY_OPTION,
    ADD_SURVEY_OPTION_SUCCESS,
    ADD_SURVEY_OPTION_FAILURE,
    CLEAR_SALES_ENGINE_SURVEYOR_DASHBOARD_FLAG,
    CLEAR_PAYMENT_FLAG,
    SKIP_SHIPMENT,
    SKIP_SHIPMENT_SUCCESS,
    SKIP_SHIPMENT_FAILURE,
    DECLINE_SURVEYOR,
    DECLINE_SURVEYOR_SUCCESS,
    DECLINE_SURVEYOR_FAILURE,
    CLEAR_SALES_ENGINE_SHIPMENT_FLAG,
    GET_AGENTS_BY_COUNTRY,
    GET_AGENTS_BY_COUNTRY_SUCCESS,
    GET_AGENTS_BY_COUNTRY_FAILURE,
    ASSIGN_AGENT,
    ASSIGN_AGENT_SUCCESS,
    ASSIGN_AGENT_FAILURE,
    CLEAR_AGENT_FLAG,
    GET_SALES_ENGINE_BY_AGENT,
    GET_SALES_ENGINE_BY_AGENT_SUCCESS,
    GET_SALES_ENGINE_BY_AGENT_FAILURE,
    GET_DOCUMENT_LINKS,
    GET_DOCUMENT_LINKS_SUCCESS,
    GET_DOCUMENT_LINKS_FAILURE,
    CLEAR_SALES_ENGINE_PAYMENT_FLAG,
    ADD_BOAT_SHIPMENT_LOCATION,
    ADD_BOAT_SHIPMENT_LOCATION_SUCCESS,
    ADD_BOAT_SHIPMENT_LOCATION_FAILURE,
    GET_COST_ESTIMATE,
    GET_COST_ESTIMATE_SUCCESS,
    GET_COST_ESTIMATE_FAILURE,
    START_SHIPMENT,
    START_SHIPMENT_SUCCESS,
    START_SHIPMENT_FAILURE,
    COMPLETE_SHIPMENT,
    COMPLETE_SHIPMENT_SUCCESS,
    COMPLETE_SHIPMENT_FAILURE,
    ADD_SHIPPER_DOCUMENTS,
    ADD_SHIPPER_DOCUMENTS_SUCCESS,
    ADD_SHIPPER_DOCUMENTS_FAILURE,
    CLEAR_SHIPPER_DOCUMENT_FLAG
} from "../actionTypes";


const initialState = {
    salesEngine: {},
    getSalesEngineSuccess: false,
    getSalesEngineError: false,

    salesEngines: [],
    createSuccess: false,
    createError: false,

    salesEnginesBoats: [],

    nearestSurveyors: [],

    requestSurveyorSuccess: false,
    requestSurveyorError: false,
    declineSurveyorSuccess: false,
    declineSurveyorError: false,

    agreementsContents: [],
    isAgreementChecked: false,

    paymentSuccess: false,

    surveyorAcceptSuccess: false,
    surveyorAcceptError: false,
    surveyorDeclineSuccess: false,
    surveyorDeclineError: false,

    surveyorReport: {},

    createNegotiablePriceSuccess: false,
    createNegotiablePriceError: false,

    addSurveyOptionSuccess: false,
    addSurveyOptionError: false,

    skipShipmentSuccess: false,
    skipShipmentError: false,
    shipmentLocationAddedSuccess: false,
    shipmentLocationAddedError: false,

    shipperAcceptShipperRequestSuccess: false,
    shipperAcceptShipperRequestError: false,
    shipperDeclineShipperRequestSuccess: false,
    shipperDeclineShipperRequestError: false,
    shipmentProposals: [],
    addShipmentProposalSuccess: false,
    addShipmentProposalError: false,
    agents: [],
    salesEngineByAgent: [],


    costEstimateSuccess: false,
    costEstimateError: false,
    costEstimation: {}
}

export const salesEngineReducer = (state = initialState, action) => {
    const { salesEngines } = state

    switch (action.type) {

        //BUYER
        case GET_SALES_ENGINE_BY_BUYER:
            return {
                ...state,
                salesEngines: []
            }

        case GET_SALES_ENGINE_BY_BUYER_SUCCESS:
            return {
                ...state,
                salesEngines: action.payload
            }

        case GET_SALES_ENGINE_BY_BUYER_FAILURE:
            return {
                ...state,
                salesEngines: []
            }

        //SELLER
        case GET_SALES_ENGINE_BY_SELLER:
            return {
                ...state,
                salesEnginesBoats: []
            }

        case GET_SALES_ENGINE_BY_SELLER_SUCCESS:
            return {
                ...state,
                salesEnginesBoats: action.payload.items
            }

        case GET_SALES_ENGINE_BY_SELLER_FAILURE:
            return {
                ...state,
                salesEnginesBoats: []
            }

        case GET_SALES_ENGINE_BY_BOAT:
            return {
                ...state,
                salesEngines: []
            }

        case GET_SALES_ENGINE_BY_BOAT_SUCCESS:
            return {
                ...state,
                salesEngines: action.payload.items
            }

        case GET_SALES_ENGINE_BY_BOAT_FAILURE:
            return {
                ...state,
                salesEngines: []
            }

        //SURVEYOR
        case GET_SALES_ENGINE_BY_SURVEYOR:
            return {
                ...state,
                salesEngines: []
            }

        case GET_SALES_ENGINE_BY_SURVEYOR_SUCCESS:
            return {
                ...state,
                salesEngines: action.payload.items
            }

        case GET_SALES_ENGINE_BY_SURVEYOR_FAILURE:
            return {
                ...state,
                salesEngines: []
            }

        //SHIPPER
        case GET_SALES_ENGINE_BY_SHIPPER:
            return {
                ...state,
                salesEngines: []
            }

        case GET_SALES_ENGINE_BY_SHIPPER_SUCCESS:
            return {
                ...state,
                salesEngines: action.payload.items
            }

        case GET_SALES_ENGINE_BY_SHIPPER_FAILURE:
            return {
                ...state,
                salesEngines: []
            }

        //GET SINGLE SALES ENGINE
        case GET_SINGLE_SALES_ENGINE:
            return {
                ...state,
                getSalesEngineSuccess: false,
                getSalesEngineError: false,
                salesEngine: {}
            }

        case GET_SINGLE_SALES_ENGINE_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                getSalesEngineSuccess: true,
            }

        case GET_SINGLE_SALES_ENGINE_FAILURE:
            return {
                ...state,
                getSalesEngineError: true,
            }

        // CREATE SALES ENGINE
        case CREATE_SALES_ENGINE:
            return {
                ...state,
                salesEngine: {},
                createSuccess: false,
                createError: false
            }

        case CREATE_SALES_ENGINE_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                createSuccess: true,
            }

        case CREATE_SALES_ENGINE_FAILURE:

            return {
                ...state,
                salesEngine: {},
                createError: true,
                errors: action.errors && action.errors.length && action.errors[0].message
            }

        //GET NEAREST SURVEYORS
        case GET_ALL_SURVEYORS:
            return {
                ...state,
                nearestSurveyors: [],
            }
        case GET_ALL_SURVEYORS_SUCCESS:
            return {
                ...state,
                nearestSurveyors: action.payload,
            }
        case GET_ALL_SURVEYORS_FAILURE:
            return {
                ...state,
                nearestSurveyors: [],
            }

        //ASSIGN SURVEYOR
        case ASSIGN_SURVEYOR:
            return {
                ...state,
                requestSurveyorSuccess: false,
                requestSurveyorError: false
            }

        case ASSIGN_SURVEYOR_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                requestSurveyorSuccess: true,
            }

        case ASSIGN_SURVEYOR_FAILURE:
            return {
                ...state,
                requestSurveyorError: true
            }

        //DECLINE SURVEYOR
        case DECLINE_SURVEYOR:
            return {
                ...state,
                declineSurveyorSuccess: false,
                declineSurveyorError: false
            }

        case DECLINE_SURVEYOR_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                declineSurveyorSuccess: true,
            }

        case DECLINE_SURVEYOR_FAILURE:
            return {
                ...state,
                declineSurveyorError: true
            }

        // PAYMENT REQUEST
        case PAYMENT_REQUEST:
            return {
                ...state,
                paymentSuccess: false,
            }

        case PAYMENT_REQUEST_SUCCESS:
            return {
                ...state,
                paymentSuccess: true,
            }
        case PAYMENT_REQUEST_FAILURE:
            return {
                ...state,
                paymentSuccess: false,
            }

        // GET ALL SHIPMENT
        case SALES_ENGINE_ASSIGN_SHIPPER_REQUEST:
            return {
                ...state,
            }

        case SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
            }
        case SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_FAILURE:
            return {
                ...state,
            }

        // GET ALL AGREEMENTS CONTENTS
        case GET_AGREEMENTS_CONTENTS:
            return {
                ...state,
                agreementsContents: []
            }

        case GET_AGREEMENTS_CONTENTS_SUCCESS:
            return {
                ...state,
                agreementsContents: action.payload
            }

        case GET_AGREEMENTS_CONTENTS_FAILURE:
            return {
                ...state,
                agreementsContents: []
            }

        // CHECK AGREEMENT
        case CHECK_AGREEMENT:
            return {
                ...state,
                isAgreementChecked: false
            }

        case CHECK_AGREEMENT_SUCCESS:
            return {
                ...state,
                isAgreementChecked: true,
                salesEngine: action.payload
            }

        case CHECK_AGREEMENT_FAILURE:
            return {
                ...state,
                isAgreementChecked: false
            }

        // SALES_ENGINE_SURVEYOR_ACCEPT_BUYER
        case SALES_ENGINE_SURVEYOR_ACCEPT_BUYER:
            return {
                ...state,
                surveyorAcceptSuccess: false,
                surveyorAcceptError: false
            }

        case SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                surveyorAcceptSuccess: true,
            }

        case SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_FAILURE:
            return {
                ...state,
                surveyorAcceptError: true,
            }

        // SALES_ENGINE_SURVEYOR_DECLINE_BUYER
        case SALES_ENGINE_SURVEYOR_DECLINE_BUYER:
            return {
                ...state,
                surveyorDeclineSuccess: false,
                surveyorDeclineError: false
            }

        case SALES_ENGINE_SURVEYOR_DECLINE_BUYER_SUCCESS:
            return {
                ...state,
                surveyorDeclineSuccess: true,
            }

        case SALES_ENGINE_SURVEYOR_DECLINE_BUYER_FAILURE:
            return {
                ...state,
                surveyorDeclineError: true,
            }

        case CREATE_SURVEYOR_REPORT:
            return {
                ...state,
                createReportSuccess: false,
                createReportFailure: false,
            }
        case CREATE_SURVEYOR_REPORT_SUCCESS:
            return {
                ...state,
                createReportSuccess: true,
                createReportFailure: false,
            }
        case CREATE_SURVEYOR_REPORT_FAILURE:
            return {
                ...state,
                createReportSuccess: false,
                createReportFailure: true,
            }

        case SURVEYOR_REPORT_SUBMIT:
            return {
                ...state,
                submitReportSuccess: false,
                submitReportError: false
            }
        case SURVEYOR_REPORT_SUBMIT_SUCCESS:
            return {
                ...state,
                submitReportSuccess: true,
                submitReportError: false
            }
        case SURVEYOR_REPORT_SUBMIT_FAILURE:
            return {
                ...state,
                submitReportSuccess: false,
                submitReportError: true
            }

        case GET_SURVEYOR_REPORT:
            return {
                ...state,
                getReportSuccess: false,
                getReportError: false,
                surveyorReport: {}
            }

        case GET_SURVEYOR_REPORT_SUCCESS:
            return {
                ...state,
                getReportSuccess: true,
                getReportError: false,
                surveyorReport: action.payload.data.getSurveyorReportBySalesEngine
            }

        case GET_SURVEYOR_REPORT_FAILURE:
            return {
                ...state,
                getReportSuccess: false,
                getReportError: true,
            }


        // SHIPPER ACCEPT/DECLINE SHIPMENT REQUEST
        case SHIPPER_ACCEPT_SHIPMENT_REQUEST:
            return {
                ...state,
                shipperAcceptShipperRequestSuccess: false,
                shipperAcceptShipperRequestError: false,
            }
        case SHIPPER_ACCEPT_SHIPMENT_REQUEST_SUCCESS:
            return {
                ...state,
                shipperAcceptShipperRequestSuccess: true,
                salesEngines: salesEngines.map(item => {
                    if (item.id === action.payload.id) {
                        return {
                            ...item,
                            shipperRequested: action.payload.shipperRequested
                        }
                    }
                    return item
                })
            }
        case SHIPPER_ACCEPT_SHIPMENT_REQUEST_FAILURE:
            return {
                ...state,
                shipperAcceptShipperRequestError: true,
            }

        case SHIPPER_DECLINE_SHIPMENT_REQUEST:
            return {
                ...state,
                shipperDeclineShipperRequestSuccess: false,
                shipperDeclineShipperRequestError: false,
            }
        case SHIPPER_DECLINE_SHIPMENT_REQUEST_SUCCESS:
            return {
                ...state,
                shipperDeclineShipperRequestSuccess: true
            }
        case SHIPPER_DECLINE_SHIPMENT_REQUEST_FAILURE:
            return {
                ...state,
                shipperDeclineShipperRequestError: true,
            }

        // SHIPMENT PROPOSAL
        case ADD_SHIPMENT_PROPOSAL:
            return {
                ...state,
                addShipmentProposalSuccess: false,
                addShipmentProposalError: false
            }
        case ADD_SHIPMENT_PROPOSAL_SUCCESS:
            return {
                ...state,
                addShipmentProposalSuccess: true
            }
        case ADD_SHIPMENT_PROPOSAL_FAILURE:
            return {
                ...state,
                salesEngine: action.payload,
                addShipmentProposalError: true
            }

        case GET_SINGLE_SHIPMENT_PROPOSAL:
            return {
                ...state
            }
        case GET_SINGLE_SHIPMENT_PROPOSAL_SUCCESS:
            return {
                ...state
            }
        case GET_SINGLE_SHIPMENT_PROPOSAL_FAILURE:
            return {
                ...state
            }

        case UPDATE_SHIPMENT_PROPOSAL:
            return {
                ...state
            }
        case UPDATE_SHIPMENT_PROPOSAL_SUCCESS:
            return {
                ...state
            }
        case UPDATE_SHIPMENT_PROPOSAL_FAILURE:
            return {
                ...state
            }

        case GET_ALL_SHIPMENT_PROPOSAL:
            return {
                ...state,
                shipmentProposals: []
            }
        case GET_ALL_SHIPMENT_PROPOSAL_SUCCESS:
            return {
                ...state,
                shipmentProposals: action.payload
            }
        case GET_ALL_SHIPMENT_PROPOSAL_FAILURE:
            return {
                ...state,
                shipmentProposals: []
            }

        // ADD NEGOTIABLE PRICE
        case ADD_NEGOTIABLE_PRICE:
            return {
                ...state,
                createNegotiablePriceSuccess: false,
                createNegotiablePriceError: false
            }

        case ADD_NEGOTIABLE_PRICE_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                createNegotiablePriceSuccess: true,
            }

        case ADD_NEGOTIABLE_PRICE_FAILURE:
            return {
                ...state,
                createNegotiablePriceError: true
            }

        // ADD SURVEY OPTION
        case ADD_SURVEY_OPTION:
            return {
                ...state,
                addSurveyOptionSuccess: false,
                addSurveyOptionError: false,
            }

        case ADD_SURVEY_OPTION_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                addSurveyOptionSuccess: true,
            }

        case ADD_SURVEY_OPTION_FAILURE:
            return {
                ...state,
                addSurveyOptionError: true,
            }

        // ADD SURVEY OPTION
        case SKIP_SHIPMENT:
            return {
                ...state,
                skipShipmentSuccess: false,
                skipShipmentError: false,
            }

        case SKIP_SHIPMENT_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                skipShipmentSuccess: true,
            }

        case SKIP_SHIPMENT_FAILURE:
            return {
                ...state,
                skipShipmentError: true,
            }

        // get all agents
        case GET_AGENTS_BY_COUNTRY:
            return {
                ...state,
                agentSuccess: false,
                agentError: false,
            }

        case GET_AGENTS_BY_COUNTRY_SUCCESS:
            return {
                ...state,
                agentSuccess: true,
                agentError: false,
                agents: action.payload.getAgentUserByCountry
            }

        case GET_AGENTS_BY_COUNTRY_FAILURE:
            return {
                ...state,
                agentSuccess: false,
                agentError: true,
            }

        // assign agent
        case ASSIGN_AGENT:
            return {
                ...state,
                assignAgentSuccess: false,
                assignAgentError: false,
            }

        case ASSIGN_AGENT_SUCCESS:
            return {
                ...state,
                assignAgentSuccess: true,
                assignAgentError: false,
                salesEngine: action.payload.assignAgent
            }

        case ASSIGN_AGENT_FAILURE:
            return {
                ...state,
                assignAgentSuccess: false,
                assignAgentError: true,
            }

        //get sales engine by agent
        case GET_SALES_ENGINE_BY_AGENT:
            return {
                ...state,
                getByAgentSuccess: false,
                getByAgentError: false,
            }
        case GET_SALES_ENGINE_BY_AGENT_SUCCESS:
            return {
                ...state,
                getByAgentSuccess: true,
                getByAgentError: false,
                salesEngineByAgent: action.payload.getSalesEngineByAgent.items
            }
        case GET_SALES_ENGINE_BY_AGENT_FAILURE:
            return {
                ...state,
                getByAgentSuccess: false,
                getByAgentError: true,
            }

        // GET DOCUMENTS LINKS
        case GET_DOCUMENT_LINKS:
            return {
                ...state,
            }

        case GET_DOCUMENT_LINKS_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload
            }

        case GET_DOCUMENT_LINKS_FAILURE:
            return {
                ...state,
            }

        // GET DOCUMENTS LINKS
        case ADD_BOAT_SHIPMENT_LOCATION:
            return {
                ...state,
                shipmentLocationAddedSuccess: false,
                shipmentLocationAddedError: false,
            }

        case ADD_BOAT_SHIPMENT_LOCATION_SUCCESS:
            return {
                ...state,
                salesEngine: action.payload,
                shipmentLocationAddedSuccess: true,
            }

        case ADD_BOAT_SHIPMENT_LOCATION_FAILURE:
            return {
                ...state,
                shipmentLocationAddedError: true,
            }

        case GET_COST_ESTIMATE:
            return {
                ...state,
                costEstimateSuccess: false,
                costEstimateError: false
            }

        case GET_COST_ESTIMATE_SUCCESS:
            return {
                ...state,
                costEstimateSuccess: true,
                costEstimation: action.payload
            }

        case GET_COST_ESTIMATE_FAILURE:
            return {
                ...state,
                costEstimateError: true,
            }

        // CLEAR ALL FLAGS

        case CLEAR_CREATE_SALES_ENGINE_FLAG:
            return {
                ...state,
                createSuccess: false,
                createError: false
            }

        case CLEAR_GET_SINGLE_SALES_ENGINE_FLAG:
            return {
                ...state,
                getSalesEngineSuccess: false,
                getSalesEngineError: false,
            }

        case CLEAR_SALES_ENGINE_MY_BOAT_FLAG:
            return {
                ...state,
                createNegotiablePriceSuccess: false,
                createNegotiablePriceError: false,
                addSurveyOptionSuccess: false,
                addSurveyOptionError: false,
            }

        case CLEAR_AGENT_FLAG:
            return {
                ...state,
                assignAgentSuccess: false,
                assignAgentError: false,
                agentSuccess: false,
                agentError: false,
            }

        case CLEAR_SALES_ENGINE_PAYMENT_FLAG:
            return {
                ...state,
                isAgreementChecked: false,
            }

        case CLEAR_SALES_ENGINE_SHIPMENT_FLAG:
            return {
                ...state,
                skipShipmentSuccess: false,
                skipShipmentError: false,
                shipmentLocationAddedSuccess: false,
                shipmentLocationAddedError: false,
            }

        case CLEAR_PAYMENT_FLAG:
            return {
                ...state,
                paymentSuccess: false,
            }


        // CLEAR ALL DASHBOARD FLAGS

        case CLEAR_SALES_ENGINES:
            return {
                ...state,
                salesEngines: []
            }

        case CLEAR_SALES_ENGINE_SURVEYOR_DASHBOARD_FLAG:
            return {
                ...state,
                surveyorAcceptSuccess: false,
                surveyorAcceptError: false,
                surveyorDeclineSuccess: false,
                surveyorDeclineError: false,
            }

        case CLEAR_REPORT_FLAG:
            return {
                ...state,
                submitReportSuccess: false,
                submitReportError: false,
                createReportSuccess: false,
                createReportFailure: false,
                getReportSuccess: false,
                getReportError: false,
            }

        case CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG:
            return {
                ...state,
                shipperAcceptShipperRequestSuccess: false,
                shipperAcceptShipperRequestError: false,
                shipperDeclineShipperRequestSuccess: false,
                shipperDeclineShipperRequestError: false,
                addShipmentProposalSuccess: false,
                addShipmentProposalError: false
            }

        case START_SHIPMENT:
            return {
                ...state,
                startShipmentSuccess: false,
                startShipmentFailure: false,
            }
        case START_SHIPMENT_SUCCESS:
            return {
                ...state,
                startShipmentSuccess: true,
                startShipmentFailure: false,
                salesEngine: action.payload.shipmentStart
            }
        case START_SHIPMENT_FAILURE:
            return {
                ...state,
                startShipmentSuccess: false,
                startShipmentFailure: true,
            }

        case COMPLETE_SHIPMENT:
            return {
                ...state,
                completeShipmentSuccess: false,
                completeShipmentFailure: false,
            }
        case COMPLETE_SHIPMENT_SUCCESS:
            return {
                ...state,
                completeShipmentSuccess: true,
                completeShipmentFailure: false,
                salesEngine: action.payload.shipmentComplete
            }
        case COMPLETE_SHIPMENT_FAILURE:
            return {
                ...state,
                completeShipmentSuccess: false,
                completeShipmentFailure: true,
            }

        case ADD_SHIPPER_DOCUMENTS:
            return {
                ...state,
                shipperDocumentSuccess: false,
                shipperDocumentError: false,
            }
        case ADD_SHIPPER_DOCUMENTS_SUCCESS:
            return {
                ...state,
                shipperDocumentSuccess: true,
                shipperDocumentError: false,
            }
        case ADD_SHIPPER_DOCUMENTS_FAILURE:
            return {
                ...state,
                shipperDocumentSuccess: false,
                shipperDocumentError: true,
            }

        case CLEAR_SHIPPER_DOCUMENT_FLAG:
            return {
                shipperDocumentSuccess: false,
                shipperDocumentError: false,
            }
        default:
            return state
    }
}