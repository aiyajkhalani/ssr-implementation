import { CREATE_REVIEW, CREATE_REVIEW_SUCCESS, CREATE_REVIEW_FAILURE, GET_REVIEW_BY_MODULE_ID, GET_REVIEW_BY_MODULE_ID_SUCCESS, GET_REVIEW_BY_MODULE_ID_FAILURE, CLEAR_REVIEW_FLAG } from "../actionTypes";

const InitialState = {
    createSuccess: false,
	createError: false,
	reviews: []
}

export const reviewReducer = (state = InitialState, action) => {
    switch(action.type) {
        case CREATE_REVIEW:
			return {
				...state,
				isReview:false,
                createSuccess: false,
				createError: false,
			};
		case CREATE_REVIEW_SUCCESS:
			return {
				...state,
				reviews:action.payload.data.createReview,
				createSuccess: true,
				isReview:true,
				createError: false,
			};
		case CREATE_REVIEW_FAILURE:
			return {
				...state,
				createSuccess: false,
				createError: true,
				isReview:false,
			};
		case GET_REVIEW_BY_MODULE_ID:
			return {
				...state,
				success: false,
				error: false,
			};
		case CLEAR_REVIEW_FLAG:
			return {
				...state,
				isReview:false,
			};
		case GET_REVIEW_BY_MODULE_ID_SUCCESS:
			return {
				...state,
				success: true,
				error: false,
				reviews: action.payload.data.getReviewsByModuleId,
			};
		case GET_REVIEW_BY_MODULE_ID_FAILURE:
			return {
				...state,
				success: false,
				error: true,
			};
        default:
			return state;
    }
}