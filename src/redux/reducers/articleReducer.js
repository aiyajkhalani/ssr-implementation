import { CREATE_ARTICLE, CREATE_ARTICLE_SUCCESS, CREATE_ARTICLE_FAILURE, CLEAR_ARTICLE_FLAG, GET_USER_ALL_ARTICLES, GET_USER_ALL_ARTICLES_SUCCESS, GET_USER_ALL_ARTICLES_FAILURE } from '../actionTypes';

const InitialState = {
	createSuccess: false,
	createError: false,
	articles: [],
	totalArticles: null
};

export const articleReducer = (state = InitialState, action) => {
	switch (action.type) {
		case CREATE_ARTICLE:
			return {
				...state,
			};
		case CREATE_ARTICLE_SUCCESS:
			return {
				...state,
				createSuccess: true,
				createError: false,
			};
		case CREATE_ARTICLE_FAILURE:
			return {
				...state,
				createSuccess: false,
				createError: true,
			};
		case CLEAR_ARTICLE_FLAG:
			return {
				...state,
				createSuccess: false,
				createError: false,
			};
		case GET_USER_ALL_ARTICLES:
			return {
				...state,
				success: false,
				error: false,
			};
		case GET_USER_ALL_ARTICLES_SUCCESS:
			return {
				...state,
				success: true,
				error: false,
				articles: action.payload.data.getAllArticlesByUser.items,
				totalArticles: action.payload.data.getAllArticlesByUser.total
			};
		case GET_USER_ALL_ARTICLES_FAILURE:
			return {
				...state,
				success: false,
				error: true,
			};

		default:
			return state;
	}
};
