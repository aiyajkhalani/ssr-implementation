import {
  CREATE_RENT_BOAT,
  CREATE_RENT_BOAT_SUCCESS,
  CREATE_RENT_BOAT_FAILURE,
  GET_BOAT_RENT_LOOKUPS,
  GET_BOAT_RENT_LOOKUPS_SUCCESS,
  GET_BOAT_RENT_LOOKUPS_FAILURE,
  GET_USER_BOAT_RENTS,
  GET_USER_BOAT_RENTS_SUCCESS,
  GET_USER_BOAT_RENTS_FAILURE,
  DELETE_BOAT_RENT,
  DELETE_BOAT_RENT_SUCCESS,
  DELETE_BOAT_RENT_FAILURE,
  CLEAR_BOAT_RENT_FLAG,
  GET_RENT_CATEGORY_WISE_BOATS_SUCCESS,
  GET_RENT_CATEGORY_WISE_BOATS,
  GET_RENT_CATEGORY_WISE_BOATS_FAILURE,
  GET_RENTS_INNER_BOAT_FAILURE,
  GET_RENTS_INNER_BOAT_SUCCESS,
  GET_RENTS_INNER_BOAT,
  GET_ALL_BOAT_RENT_TYPES,
  GET_ALL_BOAT_RENT_TYPES_SUCCESS,
  GET_ALL_BOAT_RENT_TYPES_FAILURE,
  GET_ALL_RENT_TYPES,
  GET_ALL_RENT_TYPES_SUCCESS,
  GET_ALL_RENT_TYPES_FAILURE,
  CLEAR_DELETE_BOAT_RENT_FLAG,
  UPDATE_RENT_BOAT,
  UPDATE_RENT_BOAT_SUCCESS,
  UPDATE_RENT_BOAT_FAILURE,
  GET_RENT_TRIP_CITY_WISE,
  GET_RENT_TRIP_CITY_WISE_SUCCESS,
  GET_RENT_TRIP_CITY_WISE_FAILURE
} from "../actionTypes";

const InitialState = {
  success: false,
  error: false,
  boatRents: [],
  totalBoatRents: null,
  boatRentLookUps: [],
  boatRentTypes: [],
  deleteSuccess: false,

  boatRentInnerData: {},
  rentTripCityWise: [],
  validationError: false
};

export const boatRentReducer = (state = InitialState, action) => {
  switch (action.type) {
    case CREATE_RENT_BOAT:
      return {
        ...state,
        success: false,
        error: false,
        validationError: false
      };

    case CREATE_RENT_BOAT_SUCCESS:
      return {
        ...state,
        createSuccess: true,
        error: false,
      };

    case CREATE_RENT_BOAT_FAILURE:
      return {
        ...state,
        success: false,
        error: true,
        validationError: true
      };

    case CLEAR_BOAT_RENT_FLAG:
      return {
        ...state,
        createSuccess: false,
        deleteSuccess: false,
        success: false,
        error: false
      };
    case GET_BOAT_RENT_LOOKUPS:
      return {
        ...state,
        success: false,
        error: false,
        boatRentLookUps: [],
        lookUpSuccess: false
      };
    case GET_BOAT_RENT_LOOKUPS_SUCCESS:
      const boatRentLookUps = action.payload.data.getBoatRentLookUps;

      let filteredBoatRentLookUps = {
        trip: [],
        tripType: [],
        decksAndEnvironment: [],
        tripDeposit: []
      };

      boatRentLookUps.forEach(item => {
        switch (item.typeName) {
          case "Trip":
            filteredBoatRentLookUps.trip.push(item);
            break;

          case "Deck & entertainment":
            filteredBoatRentLookUps.decksAndEnvironment.push(item);
            break;

          case "Trip Type":
            filteredBoatRentLookUps.tripType.push(item);
            break;

          case "Trip Deposit":
            filteredBoatRentLookUps.tripDeposit.push(item);
            break;

          default:
            break;
        }
      });

      return {
        ...state,
        success: true,
        error: false,
        boatRentLookUps: filteredBoatRentLookUps,
        lookUpSuccess: true
      };
    case GET_BOAT_RENT_LOOKUPS_FAILURE:
      return {
        ...state,
        success: false,
        error: true,
        lookUpSuccess: false
      };

    case GET_USER_BOAT_RENTS:
      return {
        ...state,
        success: false,
        error: false
      };
    case GET_USER_BOAT_RENTS_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        updatedSuccess: false,
        boatRents: action.payload.data.rentBoatByUser.items,
        totalBoatRents: action.payload.data.rentBoatByUser.total
      };
    case GET_USER_BOAT_RENTS_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };
    case GET_ALL_RENT_TYPES:
      return {
        ...state,
        success: false,
        error: false
      };
    case GET_ALL_RENT_TYPES_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        boatRentTypes: action.payload.data.getAllRentTypes.items,
        totalBoatRents: action.payload.data.getAllRentTypes.total
      };
    case GET_ALL_RENT_TYPES_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    case DELETE_BOAT_RENT:
      return {
        ...state,
        deleteSuccess: false,
        error: false
      };
    case DELETE_BOAT_RENT_SUCCESS:
      const { boatRents } = state;

      return {
        ...state,
        boatRents: boatRents.filter(item => item.id !== action.payload.id),
        deleteSuccess: true,
        error: false
      };
    case DELETE_BOAT_RENT_FAILURE:
      return {
        ...state,
        deleteSuccess: false,
        error: true
      };

    case CLEAR_DELETE_BOAT_RENT_FLAG:
      return {
        ...state,
        deleteSuccess: false,
      };

    case GET_RENT_CATEGORY_WISE_BOATS:
      return {
        ...state,
        categoryWiseBoat: [],
        totalCategoryWiseBoats: 0
      };
    case GET_RENT_CATEGORY_WISE_BOATS_SUCCESS:
      return {
        ...state,
        rentCategoryWiseBoat: action.payload.items,
        totalCategoryWiseBoats: action.payload.total
      };
    case GET_RENT_CATEGORY_WISE_BOATS_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    // Boat Rent Inner
    case GET_RENTS_INNER_BOAT:
      return {
        ...state,
        boatRentInnerData: {}
      };
    case GET_RENTS_INNER_BOAT_SUCCESS:
      return {
        ...state,
        boatRentInnerData: action.payload.data.editRentBoat
      };
    case GET_RENTS_INNER_BOAT_FAILURE:
      return {
        ...state,
        boatRentInnerData: {}
      };

    //boat rent trip types

    case GET_ALL_BOAT_RENT_TYPES:
      return {
        ...state,
        getTypesSuccess: false,
        getTypesError: false
      }
    case GET_ALL_BOAT_RENT_TYPES_SUCCESS:
      return {
        ...state,
        getTypesSuccess: true,
        getTypesError: false
      }
    case GET_ALL_BOAT_RENT_TYPES_FAILURE:
      return {
        ...state,
        getTypesSuccess: false,
        getTypesError: true
      }

    case UPDATE_RENT_BOAT:
      return {
        ...state,
        updatedSuccess: false,
        error: false,
        validationError: false
      };

    case UPDATE_RENT_BOAT_SUCCESS:
      return {
        ...state,
        updatedSuccess: true,
        error: false
      };

    case UPDATE_RENT_BOAT_FAILURE:
      return {
        ...state,
        updatedSuccess: false,
        error: true,
        validationError: true
      };

    case GET_RENT_TRIP_CITY_WISE:
      return {
        ...state,
        success: false,
        error: false,
        rentTripCityWise: [],
      }

    case GET_RENT_TRIP_CITY_WISE_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        rentTripCityWise: action.payload.items
      }

    case GET_RENT_TRIP_CITY_WISE_FAILURE:
      return {
        ...state,
        success: false,
        error: false
      }


    default:
      return state;
  }
};
