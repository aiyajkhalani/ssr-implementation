import {
  GET_RENT_BOAT_BY_TRIP,
  GET_RENT_BOAT_BY_TRIP_SUCCESS,
  GET_RENT_BOAT_BY_TRIP_FAILURE,
  GET_RENT_BOAT_MOST_POPULAR_FAILURE,
  GET_RENT_BOAT_MOST_POPULAR_SUCCESS,
  GET_RENT_BOAT_MOST_POPULAR,
  GET_TYPE_WISE_LOOKUP_SUCCESS,
  GET_RECOMMENDED_TRIPS_SUCCESS,
  GET_RENT_BOAT_TRIP_CITIES,
  GET_RENT_BOAT_TRIP_CITIES_SUCCESS,
  GET_RENT_BOAT_TRIP_CITIES_FAILURE,
  GET_RENT_CITY_WISE_BOATS,
  GET_RENT_CITY_WISE_BOATS_SUCCESS,
  GET_RENT_CITY_WISE_BOATS_FAILURE,
  SEARCH_BOAT_RENT,
  SEARCH_BOAT_RENT_SUCCESS,
  SEARCH_BOAT_RENT_FAILURE,
  CLEAR_SEARCH_BOAT_RENT_FLAG,
  
} from "../actionTypes";
import { lookupTypes, rentBoatTrip } from "../../util/enums/enums";

const InitialState = {
  success: false,
  error: false,
  rentSharedTrips: [],
  privateTrips: [],
  recommendedTripsPerHour: [],
  tripTypes: [],
  recommendedTrips: [],
  totalRecommendedTrips: null,
  rentBoatTripCities: [],
  //used to store user search boat type(Private,Rent,Share)
  boatType: {},

  searchedBoatRent: [],
  totalSearchedBoatRent: null,
  isBoatRentSearched: false,
  
  rentMostPopularTrips: []
};

export const rentReducer = (state = InitialState, action) => {
  switch (action.type) {
    // get boat rent by trip start

    case GET_RENT_BOAT_BY_TRIP:
      return {
        ...state,
        success: false,
        error: false
      };

    case GET_RENT_BOAT_BY_TRIP_SUCCESS:
      // Below condition change as per proper data @ghanshyam
      const actionType =
        action.payload.data &&
        action.payload.data.getRentBoatsByTrip &&
        action.payload.data.getRentBoatsByTrip.items &&
        action.payload.data.getRentBoatsByTrip.items.length &&
        action.payload.data.getRentBoatsByTrip.items[0].trip.alias;
      switch (actionType) {
        case rentBoatTrip.SHARED_TRIP:
          return {
            ...state,
            success: true,
            error: false,
            rentSharedTrips: action.payload.data.getRentBoatsByTrip.items,
            rentSharedTripsTotal: action.payload.data.getRentBoatsByTrip.total
          };

        case rentBoatTrip.PRIVATE_TRIP:
          return {
            ...state,
            success: true,
            error: false,
            privateTrips: action.payload.data.getRentBoatsByTrip.items,
            privateTripsTotal: action.payload.data.getRentBoatsByTrip.total
          };

        case rentBoatTrip.RENT_PER_HOUR:
          return {
            ...state,
            success: true,
            error: false,
            recommendedTripsPerHour:
              action.payload.data.getRentBoatsByTrip.items,
            tripsPerHourTotal:
              action.payload.data.getRentBoatsByTrip.total
          };

        default:
          return {
            ...state
          };
      }

    case SEARCH_BOAT_RENT:
      return {
        ...state,
        searchedBoatRent: [],
        totalSearchedBoatRent: null,
        isBoatRentSearched: false
      }

    case SEARCH_BOAT_RENT_SUCCESS:
      return {
        ...state,
        searchedBoatRent: action.payload.items,
        totalSearchedBoatRent: action.payload.total,
        isBoatRentSearched: true
      }

    case SEARCH_BOAT_RENT_FAILURE:
      return {
        ...state,
        searchedBoatRent: [],
        isBoatRentSearched: false
      }

    case CLEAR_SEARCH_BOAT_RENT_FLAG:
      return {
        ...state,
        isBoatRentSearched: false
      };

    case GET_RENT_BOAT_BY_TRIP_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    // end
    // get boat rent most popular

    case GET_RENT_BOAT_MOST_POPULAR:
      return {
        ...state,
        isMostPopularSuccess: false,
        isMostPopularError: false
      };

    case GET_RENT_BOAT_MOST_POPULAR_SUCCESS:
      return {
        ...state,
        isMostPopularSuccess: true,
        isMostPopularError: false,
        rentMostPopularTrips: action.payload.data.getMostPopularBoatRents.items,
        rentMostPopularTripsTotal: action.payload.data.getMostPopularBoatRents.total
      };

    case GET_RENT_BOAT_MOST_POPULAR_FAILURE:
      return {
        ...state,
        isMostPopularSuccess: false,
        isMostPopularError: true
      };

    case GET_TYPE_WISE_LOOKUP_SUCCESS:
      return {
        ...state,
        tripTypes: action.value === lookupTypes.TRIP_TYPE ? action.payload : []
      };

    case GET_RECOMMENDED_TRIPS_SUCCESS:
      return {
        ...state,
        recommendedTrips: action.payload.items,
        totalRecommendedTrips: action.payload.total
      };

    case GET_RENT_BOAT_TRIP_CITIES:
      return {
        ...state,
        rentBoatTripCities: []
      };

    case GET_RENT_BOAT_TRIP_CITIES_SUCCESS:
      return {
        ...state,
        rentBoatTripCities: action.payload
      };

    case GET_RENT_BOAT_TRIP_CITIES_FAILURE:
      return {
        ...state,
        rentBoatTripCities: []
      };

    case GET_RENT_CITY_WISE_BOATS:
      return {
        ...state,
        cityWiseBoats: []
      };

    case GET_RENT_CITY_WISE_BOATS_SUCCESS:
      return {
        ...state,
        cityWiseBoats: action.payload.items,
        totalCityWiseBoats: action.payload.total
      };

    case GET_RENT_CITY_WISE_BOATS_FAILURE:
      return {
        ...state,
        cityWiseBoats: []
      };

    

    default:
      return state;
  }
};
