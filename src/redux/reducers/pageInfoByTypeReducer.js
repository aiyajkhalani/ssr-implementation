import {
  GET_ALL_PAGE_INFORMATION_BY_TYPE,
  GET_ALL_PAGE_INFORMATION_BY_TYPE_SUCCESS,
  GET_ALL_PAGE_INFORMATION_BY_TYPE_FAILURE,
  CLEAR_PAGE_INFO_BY_TYPE_FLAG
} from "../actionTypes";

const InitialState = {
  pageInfoByType: [],
  success: false,
  error: false
};

export const pageInfoByTypeReducer = (state = InitialState, action) => {
  switch (action.type) {
    case GET_ALL_PAGE_INFORMATION_BY_TYPE:
      return {
        ...state,
        success: false,
        error: false
      };
    case GET_ALL_PAGE_INFORMATION_BY_TYPE_SUCCESS:
      return {
        ...state,
        success: true,
        error: false,
        pageInfoByType: action.payload.data.getPagesByTitle
      };
    case GET_ALL_PAGE_INFORMATION_BY_TYPE_FAILURE:
      return {
        ...state,
        success: false,
        error: true
      };

    case CLEAR_PAGE_INFO_BY_TYPE_FLAG:
      return {
        ...state,
        success: false,
        error: false
      };
    default:
      return state;
  }
};
