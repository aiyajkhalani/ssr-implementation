import { GET_ALL_USER_FAQ, GET_ALL_USER_FAQ_SUCCESS, GET_ALL_USER_FAQ_FAILURE } from '../actionTypes';

const InitialState = {
    userfaqs: [],
    success:false,
    error:false
};

export const userFaqReducer = (state = InitialState, action) => {
	switch (action.type) {
		
		case GET_ALL_USER_FAQ:
			return {
				...state,
				success: false,
				error: false,
			};
		case GET_ALL_USER_FAQ_SUCCESS:
			return {
				...state,
				success: true,
				error: false,
				userfaqs: action.payload.data.getAllFaqList.items,
			};
		case GET_ALL_USER_FAQ_FAILURE:
			return {
				...state,
				success: false,
				error: true,
			};

		default:
			return state;
	}
};
