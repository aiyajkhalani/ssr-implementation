import {
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAILURE,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  CLEAR_AUTH_FLAG,
  USER_LOGOUT_SUCCESS,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE,
  CLEAR_UPDATE_FLAG,
  GET_USER_ROLES_SUCCESS,
  GET_USER_BY_ID_SUCCESS,
  USER_EMAIL_VERIFY_SUCCESS,
  CLEAR_USER_VERIFY_FLAGS,
  SET_CURRENT_LOCATION,
  FORGOT_PASSWORD_MAIL_FAILURE,
  FORGOT_PASSWORD_MAIL_SUCCESS,
  CLEAR_FORGOT_PASSWORD_MAIL_FLAG,
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_SUCCESS,
  CLEAR_RESET_PASSWORD_FLAG,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD,
  CLEAR_PASSWORD_FLAG,
  GET_ALL_PAGE_INFO_BY_TYPE,
  GET_ALL_PAGE_INFO_BY_TYPE_SUCCESS,
  GET_ALL_PAGE_INFO_BY_TYPE_FAILURE,
  CHANGE_USER_ACCOUNT_STATUS,
  CHANGE_USER_ACCOUNT_STATUS_SUCCESS,
  CHANGE_USER_ACCOUNT_STATUS_FAILURE,
  GET_USER_LOCATION,
  GET_USER_LOCATION_SUCCESS,
  GET_USER_LOCATION_FAILURE
} from "../actionTypes";

const InitialState = {
  isAuthorized: false,
  isAuthenticated: false,
  currentUser: {},
  isError: false,
  errorMessage: "",
  isUpdate: false,
  isUpdateError: false,
  roles: [],
  isMailVerify: false,
  currentLocation: "",
  forgotPasswordMailSent: false,
  isPasswordReset: false,
  isPasswordResetError: false,
  forgotPassword: false,
  forgotPasswordError: false,
  infoList: []
};

export const loginReducer = (state = InitialState, action) => {
  const { currentUser } = state

  switch (action.type) {
    case USER_REGISTER_SUCCESS:
      return {
        ...state,
        isAuthorized: true,
        isAuthenticated: true,
        currentUser: action.payload
      };

    case USER_REGISTER_FAILURE:
      return {
        ...state,
        isError: true,
        // errorMessage: action.res.errors[0].message.split(',')
      };

    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        isAuthorized: true,
        isAuthenticated: true,
        currentUser: action.payload
      };

    case USER_LOGIN_FAILURE:
      return {
        ...state,
        isError: true
      };

    case CLEAR_AUTH_FLAG:
      return {
        ...state,
        isAuthorized: false,
        isError: false,
        isPasswordReset: false,
        errorMessage: ""
      };

    case USER_LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
        currentUser: {}
      };

    case FORGOT_PASSWORD_MAIL_SUCCESS:
      return {
        ...state,
        forgotPasswordMailSent: true
      };

    case FORGOT_PASSWORD_MAIL_FAILURE:
      return {
        ...state,
        isError: true,
        forgotPasswordMailSent: false
      };

    case CLEAR_FORGOT_PASSWORD_MAIL_FLAG:
      return {
        ...state,
        forgotPasswordMailSent: false
      };

    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isPasswordReset: true
      };

    case RESET_PASSWORD_FAILURE:
      return {
        ...state,
        isPasswordResetError: true,
        isError: true,
      };

    case CLEAR_RESET_PASSWORD_FLAG:
      return {
        ...state,
        isPasswordReset: false,
        isPasswordResetError: false
      };

    case USER_UPDATE_SUCCESS:
      return {
        ...state,
        isUpdate: true,
        currentUser: action.payload
      };

    case USER_UPDATE_FAILURE:
      return {
        ...state,
        isUpdateError: true
      };

    case CLEAR_UPDATE_FLAG:
      return {
        ...state,
        isUpdate: false,
        isUpdateError: false
      };

    case GET_USER_ROLES_SUCCESS:
      return {
        ...state,
        roles: action.payload
      };

    case GET_ALL_PAGE_INFO_BY_TYPE:
      return {
        ...state,
        infoList: []
      };

    case GET_ALL_PAGE_INFO_BY_TYPE_SUCCESS:
      return {
        ...state,
        infoList: action.payload
      };

    case GET_ALL_PAGE_INFO_BY_TYPE_FAILURE:
      return {
        ...state,
        infoList: [],
      };

    case GET_USER_BY_ID_SUCCESS:
      return {
        ...state,
        currentUser: action.payload
      };

    case USER_EMAIL_VERIFY_SUCCESS:
      return {
        ...state,
        isMailVerify: true,
        isAuthenticated: true,
        currentUser: action.payload
      };

    case CLEAR_USER_VERIFY_FLAGS:
      return {
        ...state,
        isMailVerify: false
      };

    case SET_CURRENT_LOCATION:
      return {
        ...state,
        currentLocation: action.payload
      };

    case FORGOT_PASSWORD:
      return {
        ...state,
        forgotPasswordSuccess: false,
        forgotPasswordError: false
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        forgotPasswordSuccess: true,
        forgotPasswordError: false
      };

    case FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        forgotPasswordSuccess: false,
        forgotPasswordError: true
      };
    case CLEAR_PASSWORD_FLAG:
      return {
        ...state,
        forgotPasswordSuccess: false,
        forgotPasswordError: false,
      };

    case CHANGE_USER_ACCOUNT_STATUS:
      return {
        ...state,
        statusSuccess: false,
        statusError: false
      };

    case CHANGE_USER_ACCOUNT_STATUS_SUCCESS:
      return {
        ...state,
        currentUser: { ...currentUser, accountActivated: !currentUser.accountActivated },
        statusSuccess: true,
        statusError: false
      };

    case CHANGE_USER_ACCOUNT_STATUS_FAILURE:
      return {
        ...state,
        statusSuccess: false,
        statusError: true
      };

    case GET_USER_LOCATION:
      return {
        ...state,
        currentLocation: ""
      };

    case GET_USER_LOCATION_SUCCESS:
      return {
        ...state,
        currentLocation: action.payload.country
      };

    case GET_USER_LOCATION_FAILURE:
      return {
        ...state,
      };


    default:
      return state;
  }
};
