import {
  CREATE_RENT_BOAT,
  CLEAR_BOAT_RENT_FLAG,
  GET_BOAT_RENT_LOOKUPS,
  GET_USER_BOAT_RENTS,
  DELETE_BOAT_RENT,
  GET_RENTS_INNER_BOAT,
  GET_ALL_BOAT_RENT_TYPES,
  INCREASE_BOAT_RENT_VIEW_COUNT,
  GET_ALL_RENT_TYPES,
  CLEAR_DELETE_BOAT_RENT_FLAG,
  CLEAR_SEARCH_BOAT_RENT_FLAG,
  UPDATE_RENT_BOAT,
  GET_RENT_TRIP_CITY_WISE
} from "../actionTypes";

export function createBoatRent(data) {
  return {
    type: CREATE_RENT_BOAT,
    payload: data
  };
}

export function clearBoatRentFlag() {
  return {
    type: CLEAR_BOAT_RENT_FLAG
  };
}

export function clearSearchBoatRentFlag() {
  return {
    type: CLEAR_SEARCH_BOAT_RENT_FLAG
  };
}

export function getBoatRentLookUps() {
  return {
    type: GET_BOAT_RENT_LOOKUPS
  };
}

export function getAllBoatRentOfUser(data) {
  return {
    type: GET_USER_BOAT_RENTS,
    payload: data
  };
}

export function getRentInnerBoat(data) {
  return {
    type: GET_RENTS_INNER_BOAT,
    payload: data
  };
}

export function getAllRentTypes(data) {
  return {
    type: GET_ALL_RENT_TYPES,
    payload: data
  }
}

export function deleteBoatRent(data) {
  return {
    type: DELETE_BOAT_RENT,
    payload: data
  };
}

export function getAllBoatRentTripTypes(data) {
  return {
    type: GET_ALL_BOAT_RENT_TYPES,
    payload: data
  };
}

export const increaseBoatRentViewCount = data => ({
  type: INCREASE_BOAT_RENT_VIEW_COUNT,
  payload: data
});

export const clearBoatRentDeleteFlag = () => ({
  type: CLEAR_DELETE_BOAT_RENT_FLAG
});

export function updateBoatRent(data) {
  return {
    type: UPDATE_RENT_BOAT,
    payload: data
  };
}

export const getRentTripCityWise = data => ({
  type: GET_RENT_TRIP_CITY_WISE,
  payload: data
})