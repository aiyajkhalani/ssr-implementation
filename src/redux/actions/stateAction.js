import { GET_ALL_STATES } from '../actionTypes'

export const getAllStates = () => ({
    type: GET_ALL_STATES,
})
