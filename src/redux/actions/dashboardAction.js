import {
    SEND_CONFIRM_MAIL_LINK,
    CLEAR_EMAIL_FLAGS, GET_HOME_VIDEOS,
    GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS,
    CHANGE_USER_ACCOUNT_STATUS,
    GET_MODULE_WISE_BANNERS,
    GET_DASHBOARD_COUNT,
    GET_EXPERIENCES
} from '../actionTypes'

export const sendConfirmationLink = (data) => ({
    type: SEND_CONFIRM_MAIL_LINK,
    payload: data
})

export const clearEmailFlag = () => ({
    type: CLEAR_EMAIL_FLAGS,
})

export const getHomeVideos = (data) => ({
    type: GET_HOME_VIDEOS,
    payload: data

})

export const getGlobalMinimumPriceBoats = (data) => ({
    type: GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS,
    payload: data
})

export const getModuleWiseBanners = data => ({
    type: GET_MODULE_WISE_BANNERS,
    payload: data
  });

  export const getDashBoardCount = data => ({
    type: GET_DASHBOARD_COUNT,
    payload: data
})

export const getExperiences = data => ({
    type: GET_EXPERIENCES,
    payload: data
  })