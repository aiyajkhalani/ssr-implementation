import {
  GET_ALL_PAGE_INFORMATION_BY_TYPE,
  CLEAR_PAGE_INFO_BY_TYPE_FLAG
} from "../actionTypes";

export const getAllPageInfoByType = data => ({
  type: GET_ALL_PAGE_INFORMATION_BY_TYPE,
  payload: data
});
export const clearPageInfoByTypeFlag = () => ({
  type: CLEAR_PAGE_INFO_BY_TYPE_FLAG
});
