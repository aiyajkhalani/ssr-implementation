import { GET_CATEGORY_WISE_VIDEOS } from "../actionTypes";

export const getCategoryWiseVideos = (data) => ({
    type: GET_CATEGORY_WISE_VIDEOS,
    payload:data
})