import { GET_CATEGORY_WISE_ADVERTISEMENTS } from "../actionTypes";

export const getCategoryWiseAdvertisements = data => ({
	type: GET_CATEGORY_WISE_ADVERTISEMENTS,
	payload: data,
});