import { GET_ALL_USER_GUIDE } from "../actionTypes";

export const getAllUserGuide = data => ({
	type: GET_ALL_USER_GUIDE,
	payload: data,
});