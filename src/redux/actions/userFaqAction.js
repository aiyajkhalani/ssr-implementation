import { GET_ALL_USER_FAQ } from "../actionTypes";

export const getAllUserFaq = data => ({
	type: GET_ALL_USER_FAQ,
	payload: data,
});