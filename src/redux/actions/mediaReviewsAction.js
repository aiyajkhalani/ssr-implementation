import { GET_ALL_REVIEWS } from "../actionTypes";

export const getAllReviews = () => ({
    type: GET_ALL_REVIEWS,
})
