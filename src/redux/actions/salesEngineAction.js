import {
    GET_SALES_ENGINE_BY_BUYER,
    GET_SALES_ENGINE_BY_SELLER,
    GET_SALES_ENGINE_BY_SURVEYOR,
    GET_SALES_ENGINE_BY_SHIPPER,
    GET_SINGLE_SALES_ENGINE,
    CLEAR_SALES_ENGINES,
    CREATE_SALES_ENGINE,
    CLEAR_CREATE_SALES_ENGINE_FLAG,
    GET_ALL_SURVEYORS,
    ASSIGN_SURVEYOR,
    PAYMENT_REQUEST,
    SALES_ENGINE_ASSIGN_SHIPPER_REQUEST,
    GET_AGREEMENTS_CONTENTS,
    CHECK_AGREEMENT,
    CLEAR_GET_SINGLE_SALES_ENGINE_FLAG,
    SALES_ENGINE_SURVEYOR_ACCEPT_BUYER,
    SALES_ENGINE_SURVEYOR_DECLINE_BUYER,
    CREATE_SURVEYOR_REPORT,
    SURVEYOR_REPORT_SUBMIT,
    CLEAR_REPORT_FLAG,
    GET_SURVEYOR_REPORT,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST,
    SHIPPER_DECLINE_SHIPMENT_REQUEST,
    GET_SALES_ENGINE_BY_BOAT,
    ADD_SHIPMENT_PROPOSAL,
    GET_SINGLE_SHIPMENT_PROPOSAL,
    UPDATE_SHIPMENT_PROPOSAL,
    GET_ALL_SHIPMENT_PROPOSAL,
    CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG,
    ADD_NEGOTIABLE_PRICE,
    ADD_SURVEY_OPTION,
    CLEAR_PAYMENT_FLAG,
    SKIP_SHIPMENT,
    DECLINE_SURVEYOR,
    GET_AGENTS_BY_COUNTRY,
    ASSIGN_AGENT,
    CLEAR_AGENT_FLAG,
    CLEAR_SALES_ENGINE_SURVEYOR_DASHBOARD_FLAG,
    GET_SALES_ENGINE_BY_AGENT,
    GET_DOCUMENT_LINKS,
    CLEAR_SALES_ENGINE_PAYMENT_FLAG,
    ADD_BOAT_SHIPMENT_LOCATION,
    GET_COST_ESTIMATE,
    START_SHIPMENT,
    COMPLETE_SHIPMENT,
    ADD_SHIPPER_DOCUMENTS,
    CLEAR_SHIPPER_DOCUMENT_FLAG
} from "../actionTypes";


// SALES ENGINE PROCESS

export const getSingleSalesEngine = data => ({
    type: GET_SINGLE_SALES_ENGINE,
    payload: data
});

export const addNegotiablePrice = data => ({
    type: ADD_NEGOTIABLE_PRICE,
    payload: data
})

export const addSurveyOption = data => ({
    type: ADD_SURVEY_OPTION,
    payload: data
})

export const clearSalesEngines = () => ({
    type: CLEAR_SALES_ENGINES,
});

export const createSalesEngine = data => ({
    type: CREATE_SALES_ENGINE,
    payload: data
});

export const clearCreateSalesEngineFlag = () => ({
    type: CLEAR_CREATE_SALES_ENGINE_FLAG,
});

export const clearGetSingleSalesEngineFlag = () => ({
    type: CLEAR_GET_SINGLE_SALES_ENGINE_FLAG,
});

export const getNearestSurveyors = data => ({
    type: GET_ALL_SURVEYORS,
    payload: data
});

export const requestSurveyor = data => ({
    type: ASSIGN_SURVEYOR,
    payload: data
});

export const declineSurveyor = data => ({
    type: DECLINE_SURVEYOR,
    payload: data
});

export const paymentRequest = (data) => ({
    type: PAYMENT_REQUEST,
    payload: data
});
export const salesEngineAssignShipper = (data) => ({
    type: SALES_ENGINE_ASSIGN_SHIPPER_REQUEST,
    payload: data
});
export const getAgreementContents = (data) => ({
    type: GET_AGREEMENTS_CONTENTS,
    payload: data
});

export const checkAgreement = (data) => ({
    type: CHECK_AGREEMENT,
    payload: data
});

export const getAllShipmentProposal = (data) => ({
    type: GET_ALL_SHIPMENT_PROPOSAL,
    payload: data
})

export const clearPaymentFlags = (data) => ({
    type: CLEAR_PAYMENT_FLAG,
    payload: data
})

export const skipShipment = (data) => ({
    type: SKIP_SHIPMENT,
    payload: data
})

export const getDocumentLinks = (data) => ({
    type: GET_DOCUMENT_LINKS,
    payload: data
})


// BUYER DASHBOARD

export const getSalesEngineByBuyer = data => ({
    type: GET_SALES_ENGINE_BY_BUYER,
    payload: data
});


// SELLER DASHBOARD

export const getSalesEngineBySeller = data => ({
    type: GET_SALES_ENGINE_BY_SELLER,
    payload: data
});

export const getSalesEngineByBoat = data => ({
    type: GET_SALES_ENGINE_BY_BOAT,
    payload: data
});


// SURVEYOR DASHBOARD

export const getSalesEngineBySurveyor = data => ({
    type: GET_SALES_ENGINE_BY_SURVEYOR,
    payload: data
});

export const surveyorAcceptBuyer = (data) => ({
    type: SALES_ENGINE_SURVEYOR_ACCEPT_BUYER,
    payload: data
})

export const surveyorDeclineBuyer = (data) => ({
    type: SALES_ENGINE_SURVEYOR_DECLINE_BUYER,
    payload: data
})

export const createSurveyorReport = (data) => ({
    type: CREATE_SURVEYOR_REPORT,
    payload: data
})

export const submitSurveyorReport = (data) => ({
    type: SURVEYOR_REPORT_SUBMIT,
    payload: data
})

export const getSurveyorReport = (data) => ({
    type: GET_SURVEYOR_REPORT,
    payload: data
})

export const clearReportFlag = () => ({
    type: CLEAR_REPORT_FLAG
})


// SHIPPER DASHBOARD

export const getSalesEngineByShipper = data => ({
    type: GET_SALES_ENGINE_BY_SHIPPER,
    payload: data
})

export const shipperAcceptShipmentRequest = (data) => ({
    type: SHIPPER_ACCEPT_SHIPMENT_REQUEST,
    payload: data
})

export const shipperDeclineShipmentRequest = (data) => ({
    type: SHIPPER_DECLINE_SHIPMENT_REQUEST,
    payload: data
})

export const addShipmentProposal = (data) => ({
    type: ADD_SHIPMENT_PROPOSAL,
    payload: data
})

export const getSingleShipmentProposal = (data) => ({
    type: GET_SINGLE_SHIPMENT_PROPOSAL,
    payload: data
})

export const updateShipmentProposal = (data) => ({
    type: UPDATE_SHIPMENT_PROPOSAL,
    payload: data
})

export const clearSalesEngineShipperDashboardFlags = (data) => ({
    type: CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG,
    payload: data
})

export const getAllAgents = (data) => ({
    type: GET_AGENTS_BY_COUNTRY,
    payload: data
})

export const assignAgent = (data) => ({
    type: ASSIGN_AGENT,
    payload: data
})

export const clearAgentFlag = () => ({
    type: CLEAR_AGENT_FLAG,
})

export const clearSalesEngineSurveyorDashboardFlags = () => ({
    type: CLEAR_SALES_ENGINE_SURVEYOR_DASHBOARD_FLAG
})

export const clearSalesEnginePaymentFlags = () => ({
    type: CLEAR_SALES_ENGINE_PAYMENT_FLAG
})

export const getSalesEngineByAgent = data => ({
    type: GET_SALES_ENGINE_BY_AGENT,
    payload: data
})

export const addBoatShipmentLocation = data => ({
    type: ADD_BOAT_SHIPMENT_LOCATION,
    payload: data
})

export const getCostEstimate = () => ({
    type: GET_COST_ESTIMATE
})

export const startShipment = data => ({
    type: START_SHIPMENT,
    payload: data
})

export const completeShipment = data => ({
    type: COMPLETE_SHIPMENT,
    payload: data
})

export const addShipperDocuments = data => ({
    type: ADD_SHIPPER_DOCUMENTS,
    payload: data
})

export const clearShipperDocument = () => ({
    type: CLEAR_SHIPPER_DOCUMENT_FLAG,
})