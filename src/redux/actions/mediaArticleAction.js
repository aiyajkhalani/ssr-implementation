import { GET_ALL_MEDIA_ARTICLES } from "../actionTypes";

export const getAllMediaArticles = (data) => ({
    type: GET_ALL_MEDIA_ARTICLES,
    data
})
