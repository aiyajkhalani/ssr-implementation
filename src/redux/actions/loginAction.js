import {
    USER_REGISTER,
    USER_LOGIN,
    CLEAR_AUTH_FLAG,
    USER_LOGOUT,
    USER_UPDATE,
    GET_USER_ROLES,
    USER_EMAIL_VERIFY,
    GET_USER_BY_ID,
    SET_CURRENT_LOCATION,
    FORGOT_PASSWORD_MAIL,
    RESET_PASSWORD,
    FORGOT_PASSWORD,
    CLEAR_PASSWORD_FLAG,
    GET_ALL_PAGE_INFO_BY_TYPE,
    CHANGE_USER_ACCOUNT_STATUS,
    GET_USER_LOCATION
} from '../actionTypes'

export const register = (data) => ({
    type: USER_REGISTER,
    payload: data
})

export const login = (data) => ({
    type: USER_LOGIN,
    payload: data
})

export const logout = () => ({
    type: USER_LOGOUT,
})

export const forgetPasswordMail = (data) => ({
    type: FORGOT_PASSWORD_MAIL,
    payload: data
})

export const resetPassword = (data) => ({
    type: RESET_PASSWORD,
    payload: data
})

export const clearAuthorizationFlag = () => ({
    type: CLEAR_AUTH_FLAG,
})

export const updateUser = (data) => ({
    type: USER_UPDATE,
    payload: data
})

export const getUserRoles = () => ({
    type: GET_USER_ROLES,
})

export const getAllPageInformationByType = (data) => ({
    type: GET_ALL_PAGE_INFO_BY_TYPE,
    payload: data
})


export const verifyUserEmail = (data) => ({
    type: USER_EMAIL_VERIFY,
    payload: data
})

export const getUserById = (data) => ({
    type: GET_USER_BY_ID,
    payload: data
})

export const setCurrentLocation = (data) => ({
    type: SET_CURRENT_LOCATION,
    payload: data
})

export const forgotPassword = (data) => ({
    type: FORGOT_PASSWORD,
    payload: data
})

export const clearForgotPassword = () => ({
    type: CLEAR_PASSWORD_FLAG
})

export const changeUserStatus = (data) => ({
    type: CHANGE_USER_ACCOUNT_STATUS,
    payload: data
})

export const getUserLocation = (data) => ({
    type: GET_USER_LOCATION,
    payload: data
})