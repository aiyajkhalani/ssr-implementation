import { CREATE_ARTICLE, CLEAR_ARTICLE_FLAG, GET_USER_ALL_ARTICLES } from '../actionTypes';

export const clearArticleFlag = () => ({
	type: CLEAR_ARTICLE_FLAG,
});

export const createArticle = data => ({
	type: CREATE_ARTICLE,
	payload: data,
});

export const getUserAllArticle = data => ({
	type: GET_USER_ALL_ARTICLES,
	payload: data,
});
