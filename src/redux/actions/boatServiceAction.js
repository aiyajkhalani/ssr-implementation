import {
  ADD_BOAT_SERVICE,
  GET_TYPE_WISE_BOAT_SERVICE,
  GET_RECENTLY_ADDEDD_SERVICE,
  CLEAR_SERVICE_FLAG,
  GET_USER_BOAT_SERVICE,
  CLEAR_EDIT_SERVICE,
  GET_MOST_VIEWED_BOAT_SERVICES,
  SEARCH_YACHT_SERVICE,
  GET_ALL_BOAT_SERVICE_TYPES,
  EDIT_YACHT_SERVICE
} from "../actionTypes";

export const addBoatService = data => ({
  type: ADD_BOAT_SERVICE,
  payload: data
});

export const getTypeWiseService = data => ({
  type: GET_TYPE_WISE_BOAT_SERVICE,
  payload: data
});

export const getRecentlyAddedServices = data => ({
  type: GET_RECENTLY_ADDEDD_SERVICE,
  payload: data
});

export const clearServiceFlag = () => ({
  type: CLEAR_SERVICE_FLAG
});

export const getUserBoatService = () => ({
  type: GET_USER_BOAT_SERVICE
});

export const clearEditFlag = () => ({
  type: CLEAR_EDIT_SERVICE
});

export const getMostViewedBoatServices = data => ({
  type: GET_MOST_VIEWED_BOAT_SERVICES,
  payload: data
});

export const searchYachtService = data => ({
  type: SEARCH_YACHT_SERVICE,
  payload: data
});

export const getAllBoatServiceTypes = () => ({
  type: GET_ALL_BOAT_SERVICE_TYPES,
});

export const editYachtService = data => ({
  type: EDIT_YACHT_SERVICE,
  payload: data
})
