import { put, takeLatest, all } from 'redux-saga/effects';
import { GET_ALL_STATES, GET_ALL_STATES_SUCCESS, GET_ALL_STATES_FAILURE } from '../actionTypes'
import { graphqlClient } from '../../helpers/graphqlClient'
import { getAllStates } from '../../graphql/stateSchema'


function getStatesApi() {
    return graphqlClient.query({
        query: getAllStates,
    })
        .then(res => res)
        .catch(error => error)
}

function* getStates() {
    try {
        const res = yield getStatesApi()
        yield put({ type: GET_ALL_STATES_SUCCESS, payload: res.data })
    } catch (error) {
        yield put({ type: GET_ALL_STATES_FAILURE, error })
    }
}

function* getStatesSaga() {
    yield takeLatest(GET_ALL_STATES, getStates)
}

export default function* dashboardSaga() {
    yield all([
        getStatesSaga(),
    ]);
}