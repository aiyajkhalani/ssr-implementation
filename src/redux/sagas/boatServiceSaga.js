import { put, takeLatest, all } from "redux-saga/effects";
import { graphqlClient } from "../../helpers/graphqlClient";
import {
  ADD_BOAT_SERVICE,
  ADD_BOAT_SERVICE_SUCCESS,
  ADD_BOAT_SERVICE_FAILURE,
  CLEAR_ADD_BOAT_SERVICE_FLAGS,
  GET_TYPE_WISE_BOAT_SERVICE_SUCCESS,
  GET_TYPE_WISE_BOAT_SERVICE_FAILURE,
  GET_TYPE_WISE_BOAT_SERVICE,
  GET_RECENTLY_ADDEDD_SERVICE,
  GET_RECENTLY_ADDEDD_SERVICE_FAILURE,
  GET_RECENTLY_ADDEDD_SERVICE_SUCCESS,
  GET_USER_BOAT_SERVICE,
  GET_USER_BOAT_SERVICE_FAILURE,
  GET_USER_BOAT_SERVICE_SUCCESS,
  GET_MOST_VIEWED_BOAT_SERVICES,
  GET_MOST_VIEWED_BOAT_SERVICES_SUCCESS,
  GET_MOST_VIEWED_BOAT_SERVICES_FAILURE,
  SEARCH_YACHT_SERVICE,
  SEARCH_YACHT_SERVICE_SUCCESS,
  SEARCH_YACHT_SERVICE_FAILURE,
  CLEAR_SEARCH_YACHT_SERVICE_FLAG,
  GET_ALL_BOAT_SERVICE_TYPES,
  GET_ALL_BOAT_SERVICE_TYPES_SUCCESS,
  GET_ALL_BOAT_SERVICE_TYPES_FAILURE,
  EDIT_YACHT_SERVICE_SUCCESS,
  EDIT_YACHT_SERVICE_FAILURE,
  EDIT_YACHT_SERVICE,
  ERROR_MESSAGE_SHOW
} from "../actionTypes";
import {
  createBoatService,
  typeWiseService,
  getRecentlyAddedService,
  getUserBoatService,
  getMostViewedBoatServices,
  searchYachtService,
  getAllBoatServiceTypes,
  EditYachtService
} from "../../graphql/boatServiceSchema";

function addBoatServiceApi(input) {
  return graphqlClient
    .mutate({
      mutation: createBoatService,
      variables: { input }
    })
    .then(res => {
      return res
    })
    .catch(err => {
      return err.networkError.result.errors;
    });
}

function* addBoatService(action) {
  try {
    const res = yield addBoatServiceApi(action.payload);
    yield put({
      type: ADD_BOAT_SERVICE_SUCCESS,
      payload: res.data.createOrUpdateYachtService
    });
    yield put({ type: CLEAR_ADD_BOAT_SERVICE_FLAGS });
  } catch (error) {
    yield put({ type: ADD_BOAT_SERVICE_FAILURE, error });
		yield put({ type: ERROR_MESSAGE_SHOW, payload: error });
  }
}

function* addBoatServiceSaga() {
  yield takeLatest(ADD_BOAT_SERVICE, addBoatService);
}

//get type wise service

function getTypeWiseServiceApi(input) {
  return graphqlClient
    .query({
      query: typeWiseService,
      variables: {
        page: input.page,
        limit: input.limit,
        typeId: input.typeId,
        country: input.country
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function* getTypeWiseBoatData(action) {
  try {
    const data = yield getTypeWiseServiceApi(action.payload);
    yield put({ type: GET_TYPE_WISE_BOAT_SERVICE_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_TYPE_WISE_BOAT_SERVICE_FAILURE, error });
  }
}

function* getTypeWiseService() {
  yield takeLatest(GET_TYPE_WISE_BOAT_SERVICE, getTypeWiseBoatData);
}

//get recently added service

function getRecentlyAddedServiceApi(input) {
  return graphqlClient
    .query({
      query: getRecentlyAddedService,
      variables: {
        page: input.page,
        limit: input.limit,
        country: input.country
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => { throw err });
}

function* getRecentlyAddedServiceData(action) {
  try {
    const data = yield getRecentlyAddedServiceApi(action.payload);
    yield put({ type: GET_RECENTLY_ADDEDD_SERVICE_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_RECENTLY_ADDEDD_SERVICE_FAILURE, error });
  }
}

function* getRecentlyAddedServiceSaga() {
  yield takeLatest(GET_RECENTLY_ADDEDD_SERVICE, getRecentlyAddedServiceData);
}

//get user boat service

function getUserBoatServiceApi(input) {
  return graphqlClient
    .query({
      query: getUserBoatService,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => err);
}

function* getUserBoatServiceData(action) {
  try {
    const data = yield getUserBoatServiceApi(action.payload);
    yield put({ type: GET_USER_BOAT_SERVICE_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_USER_BOAT_SERVICE_FAILURE, error });
  }
}

function* getUserBoatServiceSaga() {
  yield takeLatest(GET_USER_BOAT_SERVICE, getUserBoatServiceData);
}

//most viewed boat services

function mostViewedBoatServiceAPi(input) {
  return graphqlClient
    .query({
      query: getMostViewedBoatServices,
      variables: {
        page: input.page,
        limit: input.limit,
        country: input.country
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => { throw err });
}

function* mostViewedBoatServiceData(action) {
  try {
    const data = yield mostViewedBoatServiceAPi(action.payload);
    yield put({ type: GET_MOST_VIEWED_BOAT_SERVICES_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_MOST_VIEWED_BOAT_SERVICES_FAILURE, error });
  }
}

function* mostViewedBoatServiceSaga() {
  yield takeLatest(GET_MOST_VIEWED_BOAT_SERVICES, mostViewedBoatServiceData);
}

// SEARCH YACHT SERVICE

function searchYachtServicesApi(input) {
  return graphqlClient
    .query({
      query: searchYachtService,
      variables: { input },
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => err);
}

function* searchYachtServices(action) {
  try {
    const res = yield searchYachtServicesApi(action.payload);
    yield put({
      type: SEARCH_YACHT_SERVICE_SUCCESS,
      payload: res.data.searchYachtService
    });
    yield put({ type: CLEAR_SEARCH_YACHT_SERVICE_FLAG })
  } catch (error) {
    yield put({ type: SEARCH_YACHT_SERVICE_FAILURE, error });
  }
}

function* searchYachtServicesSaga() {
  yield takeLatest(SEARCH_YACHT_SERVICE, searchYachtServices);
}

// Get All Boat-Service Types

function getAllBoatServiceApi() {

  return graphqlClient
    .query({
      query: getAllBoatServiceTypes,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => err)
}

function* getAllBoatService() {
  try {
    const res = yield getAllBoatServiceApi();
    yield put({ type: GET_ALL_BOAT_SERVICE_TYPES_SUCCESS, payload: res.data.getAllBoatServiceTypes });
  } catch (error) {
    yield put({ type: GET_ALL_BOAT_SERVICE_TYPES_FAILURE, error });
  }
}

function* getAllBoatServiceSaga() {
  yield takeLatest(GET_ALL_BOAT_SERVICE_TYPES, getAllBoatService);
}

function editYachtServiceApi(data) {
  return graphqlClient
    .query({
      query: EditYachtService,
      variables: { id: data },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function* editYachtServiceData(action) {
  try {
    const data = yield editYachtServiceApi(action.payload);    
    yield put({ type: EDIT_YACHT_SERVICE_SUCCESS, payload: data });
  } catch (e) {
		yield put({ type: ERROR_MESSAGE_SHOW, payload: e });
    yield put({ type: EDIT_YACHT_SERVICE_FAILURE, e });
  }
}

function* editYachtServiceSaga() {
  yield takeLatest(EDIT_YACHT_SERVICE, editYachtServiceData);
}


export default function* boatServiceSaga() {
  yield all([
    addBoatServiceSaga(),
    getTypeWiseService(),
    getRecentlyAddedServiceSaga(),
    getUserBoatServiceSaga(),
    mostViewedBoatServiceSaga(),
    searchYachtServicesSaga(),
    getAllBoatServiceSaga(),
    editYachtServiceSaga()
  ]);
}
