import { put, takeLatest, all, takeEvery } from "redux-saga/effects";
import {
  GET_ALL_BOAT_LOOKUPS,
  GET_ALL_BOAT_LOOKUPS_SUCCESS,
  GET_ALL_BOAT_LOOKUPS_FAILURE,
  ADD_BOAT,
  ADD_BOAT_FAILURE,
  ADD_BOAT_SUCCESS,
  GET_ALL_BOATS_BY_USER,
  GET_ALL_BOATS_BY_USER_SUCCESS,
  GET_ALL_BOATS_BY_USER_FAILURE,
  SEARCH_BOAT,
  SEARCH_BOAT_SUCCESS,
  SEARCH_BOAT_FAILURE,
  GLOBAL_BOAT_SEARCH,
  GLOBAL_BOAT_SEARCH_FAILURE,
  GLOBAL_BOAT_SEARCH_SUCCESS,
  GET_BOAT_TYPE_SUCCESS,
  GET_BOAT_TYPE_FAILURE,
  GET_BOAT_TYPE,
  RECENT_SEARCH,
  RECENT_SEARCH_SUCCESS,
  RECENT_SEARCH_FAILURE,
  MULTI_SEARCH,
  MULTI_SEARCH_SUCCESS,
  MULTI_SEARCH_FAILURE,
  GET_SINGLE_BOAT,
  GET_SINGLE_BOAT_SUCCESS,
  GET_SINGLE_BOAT_FAILURE,
  TOGGLE_BOAT_STATUS,
  TOGGLE_BOAT_STATUS_SUCCESS,
  TOGGLE_BOAT_STATUS_FAILURE,
  DELETE_BOAT,
  DELETE_BOAT_SUCCESS,
  DELETE_BOAT_FAILURE,
  CLEAR_BOAT_DELETE_FLAG,
  GET_BOAT_BY_TYPE,
  GET_BOAT_BY_TYPE_SUCCESS,
  GET_BOAT_BY_TYPE_FAILURE,
  GET_LATEST_BOATS,
  GET_LATEST_BOATS_SUCCESS,
  GET_LATEST_BOATS_FAILURE,
  GET_POPULAR_BOATS,
  GET_POPULAR_BOATS_SUCCESS,
  GET_POPULAR_BOATS_FAILURE,
  GET_TOP_RATED_BOATS,
  GET_TOP_RATED_BOATS_SUCCESS,
  GET_TOP_RATED_BOATS_FAILURE,
  UPDATE_BOAT,
  UPDATE_BOAT_SUCCESS,
  UPDATE_BOAT_FAILURE,
  CLEAR_BOAT_ADD_FLAG,
  CLEAR_BOAT_UPDATE_FLAG,
  GET_CITY_WISE_BOATS,
  GET_CITY_WISE_BOATS_SUCCESS,
  GET_CITY_WISE_BOATS_FAILURE,
  GET_CATEGORY_WISE_BOATS,
  GET_CATEGORY_WISE_BOATS_SUCCESS,
  GET_CATEGORY_WISE_BOATS_FAILURE,
  CREATE_AUCTION_ROOM_SUCCESS,
  CREATE_AUCTION_ROOM_FAILURE,
  CREATE_AUCTION_ROOM,
  GET_USER_AUCTION_ROOMS,
  GET_USER_AUCTION_ROOMS_SUCCESS,
  GET_USER_AUCTION_ROOMS_FAILURE,
  GET_ALL_AUCTION_BID,
  GET_ALL_AUCTION_BID_SUCCESS,
  GET_ALL_AUCTION_BID_FAILURE,
  GET_ALL_AUCTION_ROOMS,
  GET_ALL_AUCTION_ROOMS_SUCCESS,
  GET_ALL_AUCTION_ROOMS_FAILURE,
  INCREASE_BOAT_VIEW_COUNT,
  INCREASE_BOAT_VIEW_COUNT_SUCCESS,
  INCREASE_BOAT_VIEW_COUNT_FAILURE,
  CREATE_AUCTION_BID,
  CREATE_AUCTION_BID_SUCCESS,
  CREATE_AUCTION_BID_FAILURE,
  GET_BOAT_SHIPPERS,
  GET_BOAT_SHIPPERS_SUCCESS,
  GET_BOAT_SHIPPERS_FAILURE,
  ERROR_MESSAGE_SHOW,
  BOAT_BY_CITIES,
  BOAT_BY_CITIES_SUCCESS,
  BOAT_BY_CITIES_FAILURE,
  GET_BOAT_TYPE_FOR_MANUFACTURE_SUCCESS,
  GET_BOAT_TYPE_FOR_MANUFACTURE_FAILURE,
  GET_BOAT_TYPE_FOR_MANUFACTURE,
  GET_BOAT_SEARCH_MINIMUM_VALUE,
  GET_BOAT_SEARCH_MINIMUM_VALUE_SUCCESS,
  GET_BOAT_SEARCH_MINIMUM_VALUE_FAILURE,
} from "../actionTypes";
import { graphqlClient } from "../../helpers/graphqlClient";
import {
  getBoatLookUps,
  addBoat,
  searchBoat,
  globalBoatSearch,
  getAllBoatByUser,
  getBoatTypes,
  getRecentSearch,
  multiSearch,
  editBoat,
  boatChangeStatus,
  deleteBoat,
  getBoatsByType,
  getLatestBoats,
  getMostPopularBoats,
  getTopRatedBoats,
  updateBoat,
  getCityWiseBoats,
  categoryWiseBoats,
  createAuctionRoom,
  getUserAuctionRooms,
  getAuctionAllBids,
  getAllAuctionRooms,
  increaseBoatViewCount,
  createAuctionBidQuery,
  getBoatShippers,
  boatByCitiesSchema,
  getBoatTypesForManufacture,
  getBoatsSearchValues,
} from "../../graphql/boatSchema";
import { createAuctionBid } from "../actions";

function getBoatLookUpCall() {
  return graphqlClient
    .query({
      query: getBoatLookUps
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      throw error;
    });
}

function getBoatShipperApi() {
  return graphqlClient
    .query({
      query: getBoatShippers,
      variables: {
        isAdmin: true
      },
    })
    .then(res => {
      return res;
    })
    .catch(error => error);
}

function boatByCitiesApi(data) {
  return graphqlClient
    .query({
      query: boatByCitiesSchema,
      variables: {
        id: data.id
      },
    })
    .then(res => {
      return res;
    })
    .catch(error => error);
}

function saveBoatCall(input) {
  return graphqlClient.mutate({
    mutation: addBoat,
    variables: {
      boat: input
    }
  }).then(res => {
    return res;
  })
    .catch(e => {
      throw e.networkError.result.errors;
    });
}

function getAllBoatsApi(input) {
  return graphqlClient
    .query({
      query: getAllBoatByUser,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function searchBoatApi(input) {
  return graphqlClient
    .mutate({
      mutation: searchBoat,
      variables: { input }
    })
    .then(res => res)
    .catch(error => error);
}

function getBoatTypesApi() {
  return graphqlClient
    .query({
      query: getBoatTypes
    })
    .then(res => res)
    .catch(error => error);
}

function getBoatTypesForManufactureApi() {
  return graphqlClient
    .query({
      query: getBoatTypesForManufacture
    })
    .then(res => res)
    .catch(error => error);
}

function globalBoatSearchApi(input) {
  return graphqlClient
    .query({
      query: globalBoatSearch,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function recentSearchApi() {
  return graphqlClient
    .query({
      query: getRecentSearch,
      fetchPolicy: 'no-cache'
    })
    .then(res => res)
    .catch(error => error);
}

function multipleSearchApi(input) {
  return graphqlClient
    .query({
      query: multiSearch,
      variables: input,
      fetchPolicy: 'no-cache'
    })
    .then(res => res)
    .catch(error => error);
}

function getSingleBoatApi(input) {
  return graphqlClient
    .query({
      query: editBoat,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function deleteSingleBoatApi(input) {
  return graphqlClient
    .mutate({
      mutation: deleteBoat,
      variables: { id: input }
    })
    .then(res => res)
    .catch(error => error);
}

function updateExistingBoatApi(input) {
  return graphqlClient
    .mutate({
      mutation: updateBoat,
      variables: { input }
    })
    .then(res => res)
    .catch(e => { throw e.networkError.result.errors; });
}

function toggleBoatStatusApi(input) {
  return graphqlClient
    .mutate({
      mutation: boatChangeStatus,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function getBoatByTypeApi(input) {
  return graphqlClient
    .query({
      query: getBoatsByType,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getLatestBoatApi(input) {
  return graphqlClient
    .query({
      query: getLatestBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getPopularBoatApi(input) {
  return graphqlClient
    .query({
      query: getMostPopularBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getTopRatedBoatApi(input) {
  return graphqlClient
    .query({
      query: getTopRatedBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getCityWiseBoatApi(input) {
  return graphqlClient
    .query({
      query: getCityWiseBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getCategoryWiseBoatApi(input) {
  return graphqlClient
    .query({
      query: categoryWiseBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function createAuctionRoomApi(auctionRoom) {
  return graphqlClient
    .mutate({
      mutation: createAuctionRoom,
      variables: { auctionRoom: auctionRoom }
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}
function getUserAuctionRoomApi(data) {
  return graphqlClient
    .query({
      query: getUserAuctionRooms,
      variables: { page: data.page, limit: data.limit },
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => {
      throw err;
    });
}
function getAllAuctionBidsApi(data) {
  return graphqlClient
    .query({
      query: getAuctionAllBids,
      variables: { page: data.page, limit: data.limit, auctionId: data.auctionId },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}
function getAllActionRoomsApi(data) {
  return graphqlClient
    .query({
      query: getAllAuctionRooms,
      variables: { page: data.page, limit: data.limit, isAdmin: false },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function increaseBoatsViewCountApi(input) {
  return graphqlClient
    .mutate({
      mutation: increaseBoatViewCount,
      variables: input,
    })
    .then(res => res)
    .catch(err => err)
}

function boatSearchMinimumValuesApi() {
  return graphqlClient
    .query({
      query: getBoatsSearchValues,
    })
    .then(res => res)
    .catch(err => err)
}

function createAuctionBidApi(auctionBid) {

  return graphqlClient
    .mutate({
      mutation: createAuctionBidQuery,
      variables: { input: auctionBid }
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function* createAuctionData(action) {
  try {
    const data = yield createAuctionRoomApi(action.payload);
    yield put({ type: CREATE_AUCTION_ROOM_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: CREATE_AUCTION_ROOM_FAILURE, e });
  }
}

function* getBoatTypeData(action) {
  try {
    const res = yield getBoatTypesApi(action.payload);
    yield put({ type: GET_BOAT_TYPE_SUCCESS, payload: res.data.getAllBoatTypes });
  } catch (error) {
    yield put({ type: GET_BOAT_TYPE_FAILURE, error });
  }
}

function* getBoatTypesForManufactureData(action) {
  try {
    const res = yield getBoatTypesForManufactureApi(action.payload);
    yield put({ type: GET_BOAT_TYPE_FOR_MANUFACTURE_SUCCESS, payload: res.data.getAllBoatTypes });
  } catch (error) {
    yield put({ type: GET_BOAT_TYPE_FOR_MANUFACTURE_FAILURE, error });
  }
}

function* getBoatLookUpData(action) {
  try {
    const data = yield getBoatLookUpCall(action.payload);
    yield put({ type: GET_ALL_BOAT_LOOKUPS_SUCCESS, data: data });
  } catch (error) {
    yield put({ type: GET_ALL_BOAT_LOOKUPS_FAILURE, error });
  }
}

function* getBoatShipperData(action) {
  try {
    const data = yield getBoatShipperApi(action.payload);
    yield put({ type: GET_BOAT_SHIPPERS_SUCCESS, data: data });
  } catch (error) {
    yield put({ type: GET_BOAT_SHIPPERS_FAILURE, error });
  }
}

function* getBoatByCity(action) {
  try {
    const data = yield boatByCitiesApi(action.payload);
    yield put({ type: BOAT_BY_CITIES_SUCCESS, data: data.data });
  } catch (error) {
    yield put({ type: BOAT_BY_CITIES_FAILURE, error });
  }
}

function* saveBoatData(action) {
  try {
    const data = yield saveBoatCall(action.payload);
    yield put({ type: ADD_BOAT_SUCCESS, data: data });
    yield put({ type: CLEAR_BOAT_ADD_FLAG });
  } catch (error) {
    yield put({ type: ERROR_MESSAGE_SHOW, payload: error })
    yield put({ type: ADD_BOAT_FAILURE, error });
  }
}

function* getAllBoatsData(action) {
  try {
    const res = yield getAllBoatsApi(action.payload);
    yield put({
      type: GET_ALL_BOATS_BY_USER_SUCCESS,
      payload: res.data.getAllBoatByUser
    });
  } catch (error) {
    yield put({ type: GET_ALL_BOATS_BY_USER_FAILURE, error });
  }
}

function* globalBoatSearchData(action) {
  try {
    const res = yield globalBoatSearchApi(action.payload);
    yield put({
      type: GLOBAL_BOAT_SEARCH_SUCCESS,
      payload: res.data.searchBoatByString
    });
  } catch (error) {
    yield put({ type: GLOBAL_BOAT_SEARCH_FAILURE, error });
  }
}

function* searchBoatData(action) {
  try {
    const res = yield searchBoatApi(action.payload);
    yield put({ type: SEARCH_BOAT_SUCCESS, payload: res.data.searchBoat });
  } catch (error) {
    yield put({ type: SEARCH_BOAT_FAILURE, error });
  }
}

function* recentSearch() {
  try {
    const res = yield recentSearchApi();
    yield put({
      type: RECENT_SEARCH_SUCCESS,
      payload: res.data.getRecentSearch
    });
  } catch (error) {
    yield put({ type: RECENT_SEARCH_FAILURE, error });
  }
}

function* multipleSearch(action) {
  try {
    const res = yield multipleSearchApi(action.payload);
    yield put({ type: MULTI_SEARCH_SUCCESS, payload: res.data.multiSearch });
  } catch (error) {
    yield put({ type: MULTI_SEARCH_FAILURE, error });
  }
}

function* getSingleBoat(action) {
  try {
    const res = yield getSingleBoatApi(action.payload);
    yield put({ type: GET_SINGLE_BOAT_SUCCESS, payload: res.data.editBoat });
  } catch (error) {
    yield put({ type: GET_SINGLE_BOAT_FAILURE, error });
  }
}

function* deleteSingleBoat(action) {
  try {
    const res = yield deleteSingleBoatApi(action.payload);
    yield put({ type: DELETE_BOAT_SUCCESS, payload: res.data.deleteBoat });
    yield put({ type: CLEAR_BOAT_DELETE_FLAG });
  } catch (error) {
    yield put({ type: DELETE_BOAT_FAILURE, error });
  }
}

function* updateExistingBoat(action) {
  try {
    const res = yield updateExistingBoatApi(action.payload);
    yield put({ type: UPDATE_BOAT_SUCCESS, payload: res.data.updateBoat });
    yield put({ type: CLEAR_BOAT_UPDATE_FLAG });
  } catch (error) {
    yield put({ type: UPDATE_BOAT_FAILURE, error });
    yield put({ type: ERROR_MESSAGE_SHOW, payload: error });
  }
}

function* toggleBoatStatus(action) {
  try {
    const res = yield toggleBoatStatusApi(action.payload);
    yield put({
      type: TOGGLE_BOAT_STATUS_SUCCESS,
      payload: res.data.boatChangeStatus
    });
  } catch (error) {
    yield put({ type: TOGGLE_BOAT_STATUS_FAILURE, error });
  }
}

function* getBoatByType(action) {
  try {
    const res = yield getBoatByTypeApi(action.payload);
    yield put({
      type: GET_BOAT_BY_TYPE_SUCCESS,
      payload: res.data.getBoatsByType,
      fieldName: action.payload.input.fieldName
    });
  } catch (error) {
    yield put({ type: GET_BOAT_BY_TYPE_FAILURE, error });
  }
}

function* getLatestBoat(action) {
  try {
    const res = yield getLatestBoatApi(action.payload);
    yield put({
      type: GET_LATEST_BOATS_SUCCESS,
      payload: res.data.getLatestBoats
    });
  } catch (error) {
    yield put({ type: GET_LATEST_BOATS_FAILURE, error });
  }
}

function* getPopularBoat(action) {
  try {
    const res = yield getPopularBoatApi(action.payload);
    yield put({
      type: GET_POPULAR_BOATS_SUCCESS,
      payload: res.data.getMostPopularBoats
    });
  } catch (error) {
    yield put({ type: GET_POPULAR_BOATS_FAILURE, error });
  }
}

function* getTopRatedBoat(action) {
  try {
    const res = yield getTopRatedBoatApi(action.payload);
    yield put({
      type: GET_TOP_RATED_BOATS_SUCCESS,
      payload: res.data.getTopRatedBoats
    });
  } catch (error) {
    yield put({ type: GET_TOP_RATED_BOATS_FAILURE, error });
  }
}

function* getCityWiseBoat(action) {
  try {
    const res = yield getCityWiseBoatApi(action.payload);
    yield put({
      type: GET_CITY_WISE_BOATS_SUCCESS,
      payload: res.data.getCityWiseBoats
    });
  } catch (error) {
    yield put({ type: GET_CITY_WISE_BOATS_FAILURE, error });
  }
}
function* getCategoryWiseBoat(action) {
  try {
    const res = yield getCategoryWiseBoatApi(action.payload);
    yield put({
      type: GET_CATEGORY_WISE_BOATS_SUCCESS,
      payload: res.data.categoryWiseBoats
    });
  } catch (error) {
    yield put({ type: GET_CATEGORY_WISE_BOATS_FAILURE, error });
  }
}
function* getUserAuctionRoomData(action) {
  try {
    const res = yield getUserAuctionRoomApi(action.payload);
    yield put({
      type: GET_USER_AUCTION_ROOMS_SUCCESS,
      payload: res
    });
  } catch (error) {
    yield put({ type: GET_USER_AUCTION_ROOMS_FAILURE, error });
  }
}
function* getAllAuctionBidData(action) {
  try {
    const res = yield getAllAuctionBidsApi(action.payload);
    yield put({
      type: GET_ALL_AUCTION_BID_SUCCESS,
      payload: res.data.getAuctionBids.items
    });
  } catch (error) {
    yield put({ type: GET_ALL_AUCTION_BID_FAILURE, error });
  }
}
function* getAllAuctionRoomData(action) {
  try {
    const res = yield getAllActionRoomsApi(action.payload);
    yield put({
      type: GET_ALL_AUCTION_ROOMS_SUCCESS,
      payload: res
    });
  } catch (error) {
    yield put({ type: GET_ALL_AUCTION_ROOMS_FAILURE, error });
  }
}
function* createAuctionBidData(action) {

  try {
    const res = yield createAuctionBidApi(action.payload);
    yield put({
      type: CREATE_AUCTION_BID_SUCCESS,
      payload: res
    });
  } catch (error) {
    yield put({ type: CREATE_AUCTION_BID_FAILURE, error });
  }
}

function* increaseBoatsViewCount(action) {
  try {
    const res = yield increaseBoatsViewCountApi(action.payload);
    yield put({ type: INCREASE_BOAT_VIEW_COUNT_SUCCESS, payload: res.data.increaseUserCount });
  } catch (error) {
    yield put({ type: INCREASE_BOAT_VIEW_COUNT_FAILURE, error });
  }
}

function* boatSearchMinimumValues(action) {
  try {
    const res = yield boatSearchMinimumValuesApi(action.payload);
    yield put({ type: GET_BOAT_SEARCH_MINIMUM_VALUE_SUCCESS, payload: res.data.getBoatsSearchValues });
  } catch (error) {
    yield put({ type: GET_BOAT_SEARCH_MINIMUM_VALUE_FAILURE, error });
  }
}

function* getAllBoatLookUps() {
  yield takeLatest(GET_ALL_BOAT_LOOKUPS, getBoatLookUpData);
}

function* getBoatShipperSaga() {
  yield takeLatest(GET_BOAT_SHIPPERS, getBoatShipperData);
}

function* getBoatByCitiesSaga() {
  yield takeLatest(BOAT_BY_CITIES, getBoatByCity);
}

function* getBoatTypeSaga() {
  yield takeLatest(GET_BOAT_TYPE, getBoatTypeData);
}

function* getBoatTypesForManufactureSaga() {
  yield takeLatest(GET_BOAT_TYPE_FOR_MANUFACTURE, getBoatTypesForManufactureData);
}

function* saveBoatSaga() {
  yield takeLatest(ADD_BOAT, saveBoatData);
}

function* getAllBoatsSaga() {
  yield takeLatest(GET_ALL_BOATS_BY_USER, getAllBoatsData);
}

function* globalBoatSearchSaga() {
  yield takeLatest(GLOBAL_BOAT_SEARCH, globalBoatSearchData);
}

function* searchBoatSaga() {
  yield takeLatest(SEARCH_BOAT, searchBoatData);
}

function* recentSearchSaga() {
  yield takeLatest(RECENT_SEARCH, recentSearch);
}

function* multipleSearchSaga() {
  yield takeLatest(MULTI_SEARCH, multipleSearch);
}

function* getSingleBoatSaga() {
  yield takeLatest(GET_SINGLE_BOAT, getSingleBoat);
}

function* deleteSingleBoatSaga() {
  yield takeLatest(DELETE_BOAT, deleteSingleBoat);
}

function* updateExistingBoatSaga() {
  yield takeLatest(UPDATE_BOAT, updateExistingBoat);
}

function* toggleBoatStatusSaga() {
  yield takeLatest(TOGGLE_BOAT_STATUS, toggleBoatStatus);
}

function* getBoatByTypeSaga() {
  yield takeEvery(GET_BOAT_BY_TYPE, getBoatByType);
}

function* getLatestBoatSaga() {
  yield takeLatest(GET_LATEST_BOATS, getLatestBoat);
}

function* getPopularBoatSaga() {
  yield takeLatest(GET_POPULAR_BOATS, getPopularBoat);
}

function* getTopRatedBoatSaga() {
  yield takeLatest(GET_TOP_RATED_BOATS, getTopRatedBoat);
}

function* getCityWiseBoatSaga() {
  yield takeLatest(GET_CITY_WISE_BOATS, getCityWiseBoat);
}
function* getCategoryWiseBoatSaga() {
  yield takeLatest(GET_CATEGORY_WISE_BOATS, getCategoryWiseBoat);
}

function* createAuctionRoomSaga() {
  yield takeLatest(CREATE_AUCTION_ROOM, createAuctionData);
}

function* getUserAuctionRoomsSaga() {
  yield takeLatest(GET_USER_AUCTION_ROOMS, getUserAuctionRoomData);
}
function* getAllBidOfAuctionSaga() {
  yield takeLatest(GET_ALL_AUCTION_BID, getAllAuctionBidData);
}
function* getAllAuctionRoomSaga() {
  yield takeLatest(GET_ALL_AUCTION_ROOMS, getAllAuctionRoomData);
}

function* increaseBoatsViewCountSaga() {
  yield takeLatest(INCREASE_BOAT_VIEW_COUNT, increaseBoatsViewCount);
}

function* createAuctionBidSaga() {
  yield takeLatest(CREATE_AUCTION_BID, createAuctionBidData);
}

function* boatSearchMinimumValuesSaga() {
  yield takeLatest(GET_BOAT_SEARCH_MINIMUM_VALUE, boatSearchMinimumValues);
}


export default function* boatSaga() {
  yield all([
    getAllBoatLookUps(),
    saveBoatSaga(),
    getAllBoatsSaga(),
    searchBoatSaga(),
    globalBoatSearchSaga(),
    getBoatTypeSaga(),
    recentSearchSaga(),
    multipleSearchSaga(),
    getSingleBoatSaga(),
    deleteSingleBoatSaga(),
    updateExistingBoatSaga(),
    toggleBoatStatusSaga(),
    getBoatByTypeSaga(),
    getLatestBoatSaga(),
    getPopularBoatSaga(),
    getTopRatedBoatSaga(),
    getCityWiseBoatSaga(),
    getCategoryWiseBoatSaga(),
    createAuctionRoomSaga(),
    getUserAuctionRoomsSaga(),
    getAllBidOfAuctionSaga(),
    getAllAuctionRoomSaga(),
    increaseBoatsViewCountSaga(),
    createAuctionBidSaga(),
    getBoatShipperSaga(),
    getBoatByCitiesSaga(),
    getBoatTypesForManufactureSaga(),
    boatSearchMinimumValuesSaga(),
  ]);
}
