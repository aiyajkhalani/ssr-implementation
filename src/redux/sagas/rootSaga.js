import { all, fork } from "redux-saga/effects";
import loginSaga from "./loginSaga";
import dashboardSaga from "./dashboardSaga";
import stateSaga from "./stateSaga";
import marinaAndStorageSaga from "./marinaAndStorageSaga";
import boatSaga from "./boatSaga";
import branchSaga from "./branchSaga";
import boatRentSaga from "./boatRentSaga";
import articleSaga from "./articleSaga";
import rentSaga from "./rentSaga";
import boatShowSaga from "./boatShowSaga";
import boatServiceSaga from "./boatServiceSaga";
import videoSaga from "./VideoSaga";
import reviewSaga from "./ReviewSaga";
import advertisementSaga from "./advertisementSaga";
import salesEngineSaga from "./salesEngineSaga";
import mediaReviewSaga from "./mediaReviewsSaga";
import mediaArticlesSaga from "./mediaArticleSaga";
import userGuideSaga from "./userGuideSaga";
import userFaqSaga from "./userFaqSaga";
import pageInfoByTypeSaga from "./pageInfoByTypeSaga";
import { getHomeBannerSaga } from "./bannerSaga";


export default function* rootSaga() {
    yield all([
        fork(loginSaga),
        fork(dashboardSaga),
        fork(stateSaga),
        fork(boatSaga),
        fork(marinaAndStorageSaga),
        fork(branchSaga),
        fork(boatRentSaga),
        fork(articleSaga),
        fork(rentSaga),
        fork(boatShowSaga),
        fork(boatServiceSaga),
        fork(videoSaga),
        fork(reviewSaga),
        fork(advertisementSaga),
        fork(salesEngineSaga),
        fork(mediaReviewSaga),
        fork(mediaArticlesSaga),
        fork(userGuideSaga),
        fork(userFaqSaga),
        fork(pageInfoByTypeSaga),
        fork(getHomeBannerSaga)
    ]);
}
