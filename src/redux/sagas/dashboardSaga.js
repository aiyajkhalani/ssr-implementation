import { put, takeLatest, all } from "redux-saga/effects";
import {
  SEND_CONFIRM_MAIL_LINK,
  SEND_CONFIRM_MAIL_LINK_SUCCESS,
  SEND_CONFIRM_MAIL_LINK_FAILURE,
  GET_HOME_VIDEOS,
  GET_HOME_VIDEOS_SUCCESS,
  GET_HOME_VIDEOS_FAILURE,
  GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS,
  GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_SUCCESS,
  GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_FAILURE,
  GET_MODULE_WISE_BANNERS_SUCCESS,
  GET_MODULE_WISE_BANNERS,
  GET_MODULE_WISE_BANNERS_FAILURE,
  GET_DASHBOARD_COUNT,
  GET_DASHBOARD_COUNT_SUCCESS,
  GET_DASHBOARD_COUNT_FAILURE,
  GET_EXPERIENCES_SUCCESS,
  GET_EXPERIENCES_FAILURE,
  GET_EXPERIENCES
} from "../actionTypes";
import { graphqlClient } from "../../helpers/graphqlClient";
import {
  sendEmailLink,
  getFeaturedHomeVideos,
  getGlobalMinimumPriceBoats,
  getExperiences
} from "../../graphql/dashboardSchema";
import {
  getBannerByModuleQuery,
  getUserDashBoardCountQuery
} from "../../graphql/boatSchema";

function sendConfirmationMailApi(input) {
  return graphqlClient
    .mutate({
      mutation: sendEmailLink,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function getFeaturedBoatApi(type) {
  const data = {
    type: type.payload,
    metatype: "video",
    status: true
  };
  return graphqlClient
    .query({
      query: getFeaturedHomeVideos,
      variables: {
        input: data
      }
    })
    .then(res => res)
    .catch(error => error);
}

function getMinimumPriceBoatsApi(input) {
  return graphqlClient
    .query({
      query: getGlobalMinimumPriceBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}


function getRentExperienceApi(moduleName) {
  return graphqlClient
    .query({
      query: getExperiences,
      variables: { moduleName  }, 
      fetchPolicy: "no-cache"
    })
    .then (res => res)
    .catch(error => error)
}

function* getRentExperienceData(action){
  try {
    const data = yield getRentExperienceApi(action.payload)
    yield put ({
      type: GET_EXPERIENCES_SUCCESS,
      payload: data
    })
  } catch (error) {
    yield put ({ type: GET_EXPERIENCES_FAILURE, error})
  }
}


function* sendConfirmationMail(action) {
  try {
    yield sendConfirmationMailApi(action.payload);
    yield put({ type: SEND_CONFIRM_MAIL_LINK_SUCCESS });
  } catch (error) {
    yield put({ type: SEND_CONFIRM_MAIL_LINK_FAILURE, error });
  }
}

function* getFeaturedVideosData(action) {
  try {
    const data = yield getFeaturedBoatApi(action);
    yield put({ type: GET_HOME_VIDEOS_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_HOME_VIDEOS_FAILURE, error });
  }
}

function* getMinimumPriceBoats(action) {
  try {
    const res = yield getMinimumPriceBoatsApi(action.payload);
    yield put({
      type: GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_SUCCESS,
      payload: res.data.getGlobalMinimumPriceBoats
    });
  } catch (error) {
    yield put({ type: GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS_FAILURE, error });
  }
}

function* getRentExperienceSaga() {
  yield takeLatest(GET_EXPERIENCES, getRentExperienceData)
}

function* sendConfirmationMailSaga() {
  yield takeLatest(SEND_CONFIRM_MAIL_LINK, sendConfirmationMail);
}

function* getFeaturedVideosSaga() {
  yield takeLatest(GET_HOME_VIDEOS, getFeaturedVideosData);
}

function* getMinimumPriceBoatsSaga() {
  yield takeLatest(GET_ALL_GLOBAL_MINIMUM_PRICE_BOATS, getMinimumPriceBoats);
}

function* getModuleWiseBannerSaga() {
  yield takeLatest(GET_MODULE_WISE_BANNERS, getModuleWiseBannerData);
}
function* getModuleWiseBannerData(action) {
  try {
    const res = yield getModuleWiseBannerApi(action.payload);
    yield put({
      type: GET_MODULE_WISE_BANNERS_SUCCESS,
      payload: res,
      fieldName: action.payload.fieldName
    });
  } catch (error) {
    yield put({
      type: GET_MODULE_WISE_BANNERS_FAILURE,
      error,
      fieldName: action.payload.fieldName
    });
  }
}
function getModuleWiseBannerApi(input) {
//   const isBanner = input.hasOwnProperty("isBanner");
  return graphqlClient
    .query({
      query: getBannerByModuleQuery,
      variables: { input: { type: input.type, isBanner: true } },
      fieldName: input.fieldName,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => {
      throw err;
    });
}
function* getDashBoardCountSaga() {
  yield takeLatest(GET_DASHBOARD_COUNT, getDashBoardCountData);
}
function* getDashBoardCountData(action) {
  try {
    const res = yield getDashBoardCountApi(action.payload);
    yield put({
      type: GET_DASHBOARD_COUNT_SUCCESS,
      payload: res.data.getUserDashBoardCount
    });
  } catch (error) {
    yield put({ type: GET_DASHBOARD_COUNT_FAILURE, error });
  }
}
function getDashBoardCountApi(input) {
  return graphqlClient
    .query({
      query: getUserDashBoardCountQuery,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(err => {
      throw err;
    });
}

export default function* dashboardSaga() {
  yield all([
    sendConfirmationMailSaga(),
    getFeaturedVideosSaga(),
    getMinimumPriceBoatsSaga(),
    getModuleWiseBannerSaga(),
    getDashBoardCountSaga(),
    getRentExperienceSaga()
  ]);
}
