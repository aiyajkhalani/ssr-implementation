import { put, takeLatest, all, takeEvery } from "redux-saga/effects";
import {
  GET_RENT_BOAT_BY_TRIP,
  GET_RENT_BOAT_BY_TRIP_SUCCESS,
  GET_RENT_BOAT_BY_TRIP_FAILURE,
  GET_RENT_BOAT_MOST_POPULAR_FAILURE,
  GET_RENT_BOAT_MOST_POPULAR_SUCCESS,
  GET_RENT_BOAT_MOST_POPULAR,
  GET_RECOMMENDED_TRIPS,
  GET_RECOMMENDED_TRIPS_SUCCESS,
  GET_RECOMMENDED_TRIPS_FAILURE,
  GET_RENT_BOAT_TRIP_CITIES,
  GET_RENT_BOAT_TRIP_CITIES_SUCCESS,
  GET_RENT_BOAT_TRIP_CITIES_FAILURE,
  GET_RENT_CITY_WISE_BOATS,
  GET_RENT_CITY_WISE_BOATS_SUCCESS,
  GET_RENT_CITY_WISE_BOATS_FAILURE,
  SEARCH_BOAT_RENT,
  SEARCH_BOAT_RENT_SUCCESS,
  SEARCH_BOAT_RENT_FAILURE,
} from "../actionTypes";
import { graphqlClient } from "../../helpers/graphqlClient";
import {
  getRentBoatsByTrip,
  getMostPopularBoatRents,
  recommendedBoatRents,
  getBoatRentTripCities,
  getRentCityWiseBoats,
  searchBoatRent,
  getExperiences
} from "../../graphql/rentSchema";

function searchBoatRentApi(input) {
  return graphqlClient
    .mutate({
      mutation: searchBoatRent,
      variables: {
        input: input
      }
    })
    .then(res => res)
    .catch(err => err);
}


function* searchBoatRentData(action) {
  try {
    const res = yield searchBoatRentApi(action.payload);
    yield put({ type: SEARCH_BOAT_RENT_SUCCESS, payload: res.data.searchBoatRent });
  } catch (e) {
    yield put({ type: SEARCH_BOAT_RENT_FAILURE, e });
  }
}

function* searchBoatRentSaga() {
  yield takeLatest(SEARCH_BOAT_RENT, searchBoatRentData);
}


function getRentBoatsByTripApi(data) {
  return graphqlClient
    .query({
      query: getRentBoatsByTrip,
      variables: data
    })
    .then(res => res)
    .catch(err => err);
}

function getRentBoatMostPopularApi(data) {
  return graphqlClient
    .query({
      query: getMostPopularBoatRents,
      variables: {
        page: data.page,
        limit: data.limit,
        country: data.country
      }
    })
    .then(res => res)
    .catch(err => err);
}

function getRecommendedTripApi(input) {
  return graphqlClient
    .query({
      query: recommendedBoatRents,
      variables: input
    })
    .then(res => res)
    .catch(err => err);
}

function getRentBoatTripCitiesApi(input) {
  return graphqlClient
    .query({
      query: getBoatRentTripCities,
      variables: 5
    })
    .then(res => res)
    .catch(err => err);
}



function* getRentBoatsByTripData(action) {
  try {
    const data = yield getRentBoatsByTripApi(action.payload);
    yield put({ type: GET_RENT_BOAT_BY_TRIP_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_RENT_BOAT_BY_TRIP_FAILURE, e });
  }
}

function* getRentBoatMostPopularData(action) {
  try {
    const data = yield getRentBoatMostPopularApi(action.payload);
    yield put({ type: GET_RENT_BOAT_MOST_POPULAR_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_RENT_BOAT_MOST_POPULAR_FAILURE, e });
  }
}

function* getRecommendedTrip(action) {
  try {
    const res = yield getRecommendedTripApi(action.payload);
    yield put({
      type: GET_RECOMMENDED_TRIPS_SUCCESS,
      payload: res.data.recommendedBoatRents
    });
  } catch (e) {
    yield put({ type: GET_RECOMMENDED_TRIPS_FAILURE, e });
  }
}

function* getRentBoatTripCities(action) {
  try {
    const res = yield getRentBoatTripCitiesApi(action.payload);
    yield put({ type: GET_RENT_BOAT_TRIP_CITIES_SUCCESS, payload: res.data.getBoatRentTripCities });
  } catch (error) {
    yield put({ type: GET_RENT_BOAT_TRIP_CITIES_FAILURE, error });
  }
}

function* getRentCityWiseBoatSaga() {
  yield takeLatest(GET_RENT_CITY_WISE_BOATS, getCityWiseBoat);
}

function* getCityWiseBoat(action) {
  try {
    const res = yield getCityWiseBoatApi(action.payload);
    yield put({
      type: GET_RENT_CITY_WISE_BOATS_SUCCESS,
      payload: res.data.getBoatRentTripCityWise
    });
  } catch (error) {
    yield put({ type: GET_RENT_CITY_WISE_BOATS_FAILURE, error });
  }
}

function getCityWiseBoatApi(input) {
  return graphqlClient
    .query({
      query: getRentCityWiseBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}



function* getRentBoatsByTripSaga() {
  yield takeEvery(GET_RENT_BOAT_BY_TRIP, getRentBoatsByTripData);
}

function* getRentBoatMostPopularSaga() {
  yield takeLatest(GET_RENT_BOAT_MOST_POPULAR, getRentBoatMostPopularData);
}

function* getRecommendedTripSaga() {
  yield takeLatest(GET_RECOMMENDED_TRIPS, getRecommendedTrip);
}

function* getRentBoatTripCitiesSaga() {
  yield takeLatest(GET_RENT_BOAT_TRIP_CITIES, getRentBoatTripCities);
}

export default function* rentSaga() {
  yield all([
    getRentBoatsByTripSaga(),
    getRentBoatMostPopularSaga(),
    getRecommendedTripSaga(),
    getRentBoatTripCitiesSaga(),
    getRentCityWiseBoatSaga(),
    searchBoatRentSaga(),

  ]);
}
