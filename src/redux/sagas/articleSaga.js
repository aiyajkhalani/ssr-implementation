import { CREATE_ARTICLE, CREATE_ARTICLE_SUCCESS, CREATE_ARTICLE_FAILURE, GET_USER_ALL_ARTICLES, GET_USER_ALL_ARTICLES_FAILURE, GET_USER_ALL_ARTICLES_SUCCESS, ERROR_MESSAGE_SHOW } from '../actionTypes';
import { createArticle, getAllArticles } from '../../graphql/articleSchema';
import { put, takeLatest, all } from 'redux-saga/effects';
import { graphqlClient } from '../../helpers/graphqlClient';

function createArticleApi(data) {
    const article = {...data}
	return graphqlClient
		.mutate({
			mutation: createArticle,
			variables: { input: article },
		})
		.then(res => {
			return res;
		})
		.catch(err => {			
			throw err.networkError.result.errors;
		});
}

function* createArticleData(action) {
	try {
		const data = yield createArticleApi(action.payload);
		yield put({ type: CREATE_ARTICLE_SUCCESS, payload: data });
	} catch (e) {
		yield put({ type: ERROR_MESSAGE_SHOW, payload: e });
		yield put({ type: CREATE_ARTICLE_FAILURE, e });
	}
}

function* createArticleSaga() {
	yield takeLatest(CREATE_ARTICLE, createArticleData);
}

function getAllArticlesApi(data) {
	return graphqlClient
		.query({
			query: getAllArticles,
			fetchPolicy: 'no-cache',
			variables: { page: data.page, limit: data.limit },
			fetchPolicy: 'no-cache'
		})
		.then(res => {
			return res;
		})
		.catch(e => {
			throw e;
		});
}

function* getAllArticlesData(action) {
	try {
		const data = yield getAllArticlesApi(action.payload);
		yield put({ type: GET_USER_ALL_ARTICLES_SUCCESS, payload: data });
	} catch (e) {
		yield put({ type: GET_USER_ALL_ARTICLES_FAILURE, e });
	}
}

function* getAllArticlesSaga() {
	yield takeLatest(GET_USER_ALL_ARTICLES, getAllArticlesData);
}

export default function* articleSaga() {
	yield all([
		createArticleSaga(),
		getAllArticlesSaga()
	]);
}
