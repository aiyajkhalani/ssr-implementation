import { put, takeLatest, all } from "redux-saga/effects";
import {
  USER_REGISTER,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAILURE,
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
  USER_UPDATE,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE,
  CLEAR_UPDATE_FLAG,
  GET_USER_ROLES,
  GET_USER_ROLES_SUCCESS,
  GET_USER_ROLES_FAILURE,
  USER_EMAIL_VERIFY,
  USER_EMAIL_VERIFY_SUCCESS,
  USER_EMAIL_VERIFY_FAILURE,
  GET_USER_BY_ID,
  GET_USER_BY_ID_SUCCESS,
  GET_USER_BY_ID_FAILURE,
  CLEAR_USER_VERIFY_FLAGS,
  FORGOT_PASSWORD_MAIL,
  FORGOT_PASSWORD_MAIL_SUCCESS,
  FORGOT_PASSWORD_MAIL_FAILURE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  CLEAR_FORGOT_PASSWORD_MAIL_FLAG,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_SUCCESS,
  ERROR_MESSAGE_SHOW,
  GET_ALL_PAGE_INFO_BY_TYPE_FAILURE,
  GET_ALL_PAGE_INFO_BY_TYPE,
  GET_ALL_PAGE_INFO_BY_TYPE_SUCCESS,
  CHANGE_USER_ACCOUNT_STATUS,
  CHANGE_USER_ACCOUNT_STATUS_SUCCESS,
  CHANGE_USER_ACCOUNT_STATUS_FAILURE,
  GET_USER_LOCATION,
  GET_USER_LOCATION_SUCCESS,
  GET_USER_LOCATION_FAILURE
} from "../actionTypes";
import { graphqlClient } from "../../helpers/graphqlClient";
import {
  registerUser,
  loginUser,
  updateUser,
  getAllRoles,
  activateUser,
  editUser,
  forgotPasswordMail,
  changePassword,
  getAllPageInformationByType,
  resetPasswordQuery,
  changeUserAccountStatus,
  getUserLocation,
  forgotPassword
} from "../../graphql/userSchema";

function registerApi(input) {
  return graphqlClient
    .mutate({
      mutation: registerUser,
      variables: { input }
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      return error.networkError.result;
    });
}

function loginApi(input) {
  return graphqlClient
    .mutate({
      mutation: loginUser,
      variables: { input }
    })
    .then(res => res)
    .catch(error => error.networkError.result);
}

function updateApi(input) {
  return graphqlClient
    .mutate({
      mutation: updateUser,
      variables: { input }
    })
    .then(res => res)
    .catch(error => error);
}

function forgetPasswordMailApi(input) {
  return graphqlClient
    .mutate({
      mutation: forgotPasswordMail,
      variables: input
    })
    .then(res => res)
    .catch(error => error.networkError.result);
}

function resetPasswordApi(input) {
  return graphqlClient
    .mutate({
      mutation: resetPasswordQuery,
      variables: input
    })
    .then(res => res)
    .catch(error => error.networkError.result);
}

function userRolesApi() {
  return graphqlClient
    .query({
      query: getAllRoles
    })
    .then(res => res)
    .catch(error => error);
}

function getAllPageInformationByTypeApi(data) {
  return graphqlClient
    .query({
      query: getAllPageInformationByType,
      variables: data.payload
    })
    .then(res => res)
    .catch(error => error);
}


function verifyUserEmailApi(input) {
  return graphqlClient
    .mutate({
      mutation: activateUser,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function getUserByIdApi(input) {
  return graphqlClient
    .query({
      query: editUser,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function forgotPasswordApi(input) {
  console.log({ input })
  return graphqlClient
    .mutate({
      mutation: forgotPassword,
      variables: {
        password: input.password
      }
    })
    .then(res => res)
    .catch(error => error);
}

function changeUserAccountStatusApi(input) {
  return graphqlClient
    .mutate({
      mutation: changeUserAccountStatus,
      variables: input
    })
    .then(res => res)
    .catch(error => error);
}

function userLocationApi(input) {
  return graphqlClient
    .query({
      query: getUserLocation,
      variables: { ip: input.ip },
      fetchPolicy: 'no-cache'
    })
    .then(res => res)
    .catch(error => error);
}


function* userRegister(action) {
  try {
    const res = yield registerApi(action.payload);

    if (res.errors && res.errors.length) {
      yield put({ type: USER_REGISTER_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      localStorage.setItem("token", res.data.createUser.token);
      localStorage.setItem("isAuthenticated", true);
      yield put({ type: USER_REGISTER_SUCCESS, payload: res.data.createUser });
    }
  } catch (error) {
    yield put({ type: USER_REGISTER_FAILURE, error });
  }
}

function* userLogin(action) {
  try {
    const res = yield loginApi(action.payload);
    if (res.errors && res.errors.length) {
      yield put({ type: USER_LOGIN_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      localStorage.setItem("token", res.data.loginUser.token);
      localStorage.setItem("userId", res.data.loginUser.id);
      localStorage.setItem("isAuthenticated", true);
      yield put({ type: USER_LOGIN_SUCCESS, payload: res.data.loginUser });
    }

  } catch (error) {
    yield put({ type: USER_LOGIN_FAILURE, error });
  }
}

function* userLogout() {
  try {
    localStorage.clear();
    yield put({ type: USER_LOGOUT_SUCCESS });
  } catch (error) {
    yield put({ type: USER_LOGOUT_FAILURE, error });
  }
}

function* forgetPasswordMail(action) {
  try {
    const res = yield forgetPasswordMailApi(action.payload);
    if (res.errors && res.errors.length) {
      yield put({ type: FORGOT_PASSWORD_MAIL_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      yield put({
        type: FORGOT_PASSWORD_MAIL_SUCCESS,
        payload: res.data.forgetPasswordMail
      });
      yield put({ type: CLEAR_FORGOT_PASSWORD_MAIL_FLAG });
    }

  } catch (error) {
    yield put({ type: FORGOT_PASSWORD_MAIL_FAILURE, error });
  }
}

function* resetPassword(action) {
  try {
    const res = yield resetPasswordApi(action.payload);
    if (res.errors && res.errors.length) {
      yield put({ type: RESET_PASSWORD_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      yield put({
        type: RESET_PASSWORD_SUCCESS,
        payload: res.data.resetPassword
      });
    }

  } catch (error) {
    yield put({ type: RESET_PASSWORD_FAILURE, error });
  }
}

function* userUpdate(action) {
  try {
    const res = yield updateApi(action.payload);
    yield put({ type: USER_UPDATE_SUCCESS, payload: res.data.updateProfile });
    yield put({ type: CLEAR_UPDATE_FLAG });
  } catch (error) {
    yield put({ type: USER_UPDATE_FAILURE, error });
    yield put({ type: CLEAR_UPDATE_FLAG });
  }
}

function* getUserRoles() {
  try {
    const res = yield userRolesApi();
    yield put({ type: GET_USER_ROLES_SUCCESS, payload: res.data.getAllRoles });
  } catch (error) {
    yield put({ type: GET_USER_ROLES_FAILURE, error });
  }
}

function* getUserPageInformationByType(data) {
  try {
    const res = yield getAllPageInformationByTypeApi(data);
    yield put({ type: GET_ALL_PAGE_INFO_BY_TYPE_SUCCESS, payload: res.data.getAllPageInformationByType });
  } catch (error) {
    yield put({ type: GET_ALL_PAGE_INFO_BY_TYPE_FAILURE, error });
  }
}



function* verifyUserEmail(action) {
  try {
    const res = yield verifyUserEmailApi(action.payload);
    // const actionForUser = { payload: { id: res.data.activateUser.id } };
    // yield getUserById(actionForUser);
    // localStorage.setItem("token", res.data.activateUser.token);
    localStorage.setItem("isAuthenticated", true);
    yield put({
      type: USER_EMAIL_VERIFY_SUCCESS,
      payload: res.data.activateUser
    });
    yield put({ type: CLEAR_USER_VERIFY_FLAGS });
  } catch (error) {
    yield put({ type: USER_EMAIL_VERIFY_FAILURE, error });
  }
}

function* getUserById(action) {
  try {
    const res = yield getUserByIdApi(action.payload);
    localStorage.setItem("token", res.data.editUser.token);
    localStorage.setItem("isAuthenticated", true);
    yield put({ type: GET_USER_BY_ID_SUCCESS, payload: res.data.editUser });
  } catch (error) {
    yield put({ type: GET_USER_BY_ID_FAILURE, error });
  }
}

function* forgotPasswordData(action) {
  try {
    const res = yield forgotPasswordApi(action.payload);
    yield put({ type: FORGOT_PASSWORD_SUCCESS, payload: res.data });
  } catch (error) {
    yield put({ type: FORGOT_PASSWORD_FAILURE, error });
  }
}

function* changeUserAccountStatusData(action) {
  try {
    const res = yield changeUserAccountStatusApi(action.payload);
    yield put({ type: CHANGE_USER_ACCOUNT_STATUS_SUCCESS, payload: res });
  } catch (error) {
    yield put({ type: CHANGE_USER_ACCOUNT_STATUS_FAILURE, error });
  }
}

function* userLocation(action) {
  try {
    const res = yield userLocationApi(action.payload);
    yield put({ type: GET_USER_LOCATION_SUCCESS, payload: res.data.getUserLocation });
  } catch (error) {
    yield put({ type: GET_USER_LOCATION_FAILURE, error });
  }
}



function* registerUserSaga() {
  yield takeLatest(USER_REGISTER, userRegister);
}

function* loginUserSaga() {
  yield takeLatest(USER_LOGIN, userLogin);
}

function* logoutUserSaga() {
  yield takeLatest(USER_LOGOUT, userLogout);
}

function* forgetPasswordMailSaga() {
  yield takeLatest(FORGOT_PASSWORD_MAIL, forgetPasswordMail);
}

function* resetPasswordSaga() {
  yield takeLatest(RESET_PASSWORD, resetPassword);
}

function* updateUserSaga() {
  yield takeLatest(USER_UPDATE, userUpdate);
}

function* getUserRolesSaga() {
  yield takeLatest(GET_USER_ROLES, getUserRoles);
}

function* getAllPageInformationByTypeSaga() {
  yield takeLatest(GET_ALL_PAGE_INFO_BY_TYPE, getUserPageInformationByType);
}

function* verifyUserEmailSaga() {
  yield takeLatest(USER_EMAIL_VERIFY, verifyUserEmail);
}

function* getUserByIdSaga() {
  yield takeLatest(GET_USER_BY_ID, getUserById);
}
function* forgotPasswordSaga() {
  yield takeLatest(FORGOT_PASSWORD, forgotPasswordData);
}

function* changeUserStatusSaga() {
  yield takeLatest(CHANGE_USER_ACCOUNT_STATUS, changeUserAccountStatusData);
}

function* userLocationSaga() {
  yield takeLatest(GET_USER_LOCATION, userLocation);
}

export default function* loginSaga() {
  yield all([
    registerUserSaga(),
    loginUserSaga(),
    logoutUserSaga(),
    forgetPasswordMailSaga(),
    resetPasswordSaga(),
    updateUserSaga(),
    getUserRolesSaga(),
    verifyUserEmailSaga(),
    getUserByIdSaga(),
    forgotPasswordSaga(),
    getAllPageInformationByTypeSaga(),
    changeUserStatusSaga(),
    userLocationSaga(),
  ]);
}
