import { GET_ALL_PAGE_INFORMATION_BY_TYPE, GET_ALL_PAGE_INFORMATION_BY_TYPE_FAILURE, GET_ALL_PAGE_INFORMATION_BY_TYPE_SUCCESS } from '../actionTypes';
import { put, takeLatest, all } from 'redux-saga/effects';
import { graphqlClient } from '../../helpers/graphqlClient';
import { getAllPageInformationByType } from '../../graphql/pageInfoByTypeSchema';


function getAllPageInformationApi(data) {
	return graphqlClient
		.query({
			query: getAllPageInformationByType,
			fetchPolicy: 'no-cache',
			variables: { title: data.title },
		})
		.then(res => {
			return res;
		})
		.catch(e => {
			throw e;
		});
}

function* getPageInfoByTypeData(action) {
	try {
		const data = yield getAllPageInformationApi(action.payload);
		yield put({ type: GET_ALL_PAGE_INFORMATION_BY_TYPE_SUCCESS, payload: data });
	} catch (e) {
		yield put({ type: GET_ALL_PAGE_INFORMATION_BY_TYPE_FAILURE, e });
	}
}

function* getPageInfoByTypeSaga() {
	yield takeLatest(GET_ALL_PAGE_INFORMATION_BY_TYPE, getPageInfoByTypeData);
}

export default function* pageInfoByTypeSaga() {
	yield all([
		getPageInfoByTypeSaga()
	]);
}
