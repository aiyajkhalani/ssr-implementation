import {  GET_ALL_USER_FAQ, GET_ALL_USER_FAQ_FAILURE, GET_ALL_USER_FAQ_SUCCESS } from '../actionTypes';
import {  getAllUserFaq } from '../../graphql/userFaqSchema';
import { put, takeLatest, all } from 'redux-saga/effects';
import { graphqlClient } from '../../helpers/graphqlClient';


function getAllUserFaqApi(data) {
	return graphqlClient
		.query({
			query: getAllUserFaq,
			fetchPolicy: 'no-cache',
			variables: { page: data.page, limit: data.limit },
			fetchPolicy: 'no-cache'
		})
		.then(res => {
			return res;
		})
		.catch(e => {
			throw e;
		});
}

function* getAllUserFaqData(action) {
	try {
		const data = yield getAllUserFaqApi(action.payload);
		yield put({ type: GET_ALL_USER_FAQ_SUCCESS, payload: data });
	} catch (e) {
		yield put({ type: GET_ALL_USER_FAQ_FAILURE, e });
	}
}

function* getAllUserFaqSaga() {
	yield takeLatest(GET_ALL_USER_FAQ, getAllUserFaqData);
}

export default function* userFaqSaga() {
	yield all([
		getAllUserFaqSaga()
	]);
}
