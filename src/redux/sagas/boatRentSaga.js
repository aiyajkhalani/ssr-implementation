import { put, takeLatest, all } from "redux-saga/effects";
import {
  CREATE_RENT_BOAT,
  CREATE_RENT_BOAT_SUCCESS,
  CREATE_RENT_BOAT_FAILURE,
  GET_BOAT_RENT_LOOKUPS_FAILURE,
  GET_BOAT_RENT_LOOKUPS_SUCCESS,
  GET_BOAT_RENT_LOOKUPS,
  GET_USER_BOAT_RENTS,
  GET_USER_BOAT_RENTS_SUCCESS,
  GET_USER_BOAT_RENTS_FAILURE,
  DELETE_BOAT_RENT,
  DELETE_BOAT_RENT_SUCCESS,
  DELETE_BOAT_RENT_FAILURE,
  GET_RENT_CATEGORY_WISE_BOATS,
  GET_RENT_CATEGORY_WISE_BOATS_SUCCESS,
  GET_RENT_CATEGORY_WISE_BOATS_FAILURE,
  GET_RENTS_INNER_BOAT_FAILURE,
  GET_RENTS_INNER_BOAT_SUCCESS,
  GET_RENTS_INNER_BOAT,
  INCREASE_BOAT_RENT_VIEW_COUNT,
  INCREASE_BOAT_RENT_VIEW_COUNT_FAILURE,
  INCREASE_BOAT_RENT_VIEW_COUNT_SUCCESS,
  GET_ALL_BOAT_RENT_TYPES_SUCCESS,
  GET_ALL_BOAT_RENT_TYPES_FAILURE,
  GET_ALL_BOAT_RENT_TYPES,
  GET_ALL_RENT_TYPES_SUCCESS,
  GET_ALL_RENT_TYPES_FAILURE,
  GET_ALL_RENT_TYPES,
  UPDATE_RENT_BOAT,
  UPDATE_RENT_BOAT_SUCCESS,
  UPDATE_RENT_BOAT_FAILURE,
  ERROR_MESSAGE_SHOW,
  GET_RENT_TRIP_CITY_WISE_SUCCESS,
  GET_RENT_TRIP_CITY_WISE_FAILURE,
  GET_RENT_TRIP_CITY_WISE,
} from '../actionTypes';
import { graphqlClient } from '../../helpers/graphqlClient';
import { createRentBoat, getBoatRentLookUps, getUserBoatRent, deleteBoatRent, rentCategoryWiseBoats, getRentBoatInnerQuery, increaseBoatRentViewCount, getAllRentTypes, updateRentBoat, getSimilarBoatRents } from '../../graphql/boatRentSchema';
import { getAllRentTripType } from "../../graphql/rentSchema";


function createRentBoatApi(data) {
  const input = data.payload;
  return graphqlClient
    .mutate({
      mutation: createRentBoat,
      variables: {
        input: input
      }
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      return err.networkError.result
    });
}
function updateRentBoatApi(data) {
  const input = data.payload;
  return graphqlClient
    .mutate({
      mutation: updateRentBoat,
      variables: {
        input: input
      }
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      return err.networkError.result;
    });
}

function getBoatRentLookUpApi(action) {
  return graphqlClient
    .query({
      query: getBoatRentLookUps,
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function getUserBoatRentsApi(data) {
  return graphqlClient
    .query({
      query: getUserBoatRent,
      variables: { page: data.page, limit: data.limit },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function getAllRentTypesApi(data) {
  return graphqlClient
    .query({
      query: getAllRentTypes,
      variables: data,
      fetchPolicy: 'no-cache'
    })
    .then(res => res)
    .catch(error => { throw error })
}

function deleteBoatRentApi(id) {
  return graphqlClient
    .mutate({
      mutation: deleteBoatRent,
      variables: { id }
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      throw error;
    });
}

function getRentCategoryWiseBoatApi(input) {
  return graphqlClient
    .query({
      query: rentCategoryWiseBoats,
      variables: input,
      fetchPolicy: "no-cache"
    })
    .then(res => res)
    .catch(error => error);
}

function getRentBoatInnerApi(data) {
  return graphqlClient
    .query({
      query: getRentBoatInnerQuery,
      variables: data,
      fetchPolicy: 'no-cache'
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function increaseBoatRentsViewCountApi(input) {
  return graphqlClient
    .mutate({
      mutation: increaseBoatRentViewCount,
      variables: input,
    })
    .then(res => res)
    .catch(error => error)
}

function getRentTripCityWiseApi(data) {  
  return graphqlClient
    .query({
      query: getSimilarBoatRents,
      variables: {
        id: data.id
      },
    })
    .then(res => res)
    .catch(error => error);
}

function* getRentTripCityWiseData(action) {
  try {
    const res = yield getRentTripCityWiseApi(action.payload);
    yield put({ type: GET_RENT_TRIP_CITY_WISE_SUCCESS, payload: res.data.getSimilarBoatRents });
  } catch (error) {
    yield put({ type: GET_RENT_TRIP_CITY_WISE_FAILURE, error });
  }
}

function* createRentBoatData(action) {
  try {
    const res = yield createRentBoatApi(action);

    if (res.errors && res.errors.length) {
      yield put({ type: CREATE_RENT_BOAT_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      yield put({ type: CREATE_RENT_BOAT_SUCCESS, payload: res });
    }
  } catch (e) {
    yield put({ type: CREATE_RENT_BOAT_FAILURE, e });
  }
}

function* updateRentBoatData(action) {
  try {
    const res = yield updateRentBoatApi(action);

    if (res.errors && res.errors.length) {
      yield put({ type: UPDATE_RENT_BOAT_FAILURE, res });
      yield put({ type: ERROR_MESSAGE_SHOW, payload: res.errors });
    } else {
      yield put({ type: UPDATE_RENT_BOAT_SUCCESS, payload: res });
    }

  } catch (e) {
    yield put({ type: UPDATE_RENT_BOAT_FAILURE, e });
  }
}

function* getBoatRentLookUpsData(action) {
  try {
    const data = yield getBoatRentLookUpApi(action);
    yield put({ type: GET_BOAT_RENT_LOOKUPS_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_BOAT_RENT_LOOKUPS_FAILURE, e });
  }
}

function* getUserBoatRentsData(action) {
  try {
    const data = yield getUserBoatRentsApi(action.payload);
    yield put({ type: GET_USER_BOAT_RENTS_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_USER_BOAT_RENTS_FAILURE, e });
  }
}

function* getAllRentTypesData(action) {
  try {
    const data = yield getAllRentTypesApi(action.payload);
    yield put({ type: GET_ALL_RENT_TYPES_SUCCESS, payload: data })
  } catch (e) {
    yield put({ type: GET_ALL_RENT_TYPES_FAILURE, e })
  }
}


function* deleteBoatRentData(action) {
  try {
    const res = yield deleteBoatRentApi(action.payload);
    yield put({ type: DELETE_BOAT_RENT_SUCCESS, payload: res.data.deleteRentBoat });
  } catch (e) {
    yield put({ type: DELETE_BOAT_RENT_FAILURE, e });
  }
}

function* getRentCategoryWiseBoat(action) {
  try {
    const res = yield getRentCategoryWiseBoatApi(action.payload);
    yield put({
      type: GET_RENT_CATEGORY_WISE_BOATS_SUCCESS,
      payload: res.data.getRentBoatsByTripType
    });
  } catch (error) {
    yield put({ type: GET_RENT_CATEGORY_WISE_BOATS_FAILURE, error });
  }
}

function* getRentBoatInnerData(action) {
  try {
    const data = yield getRentBoatInnerApi(action.payload);
    yield put({ type: GET_RENTS_INNER_BOAT_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_RENTS_INNER_BOAT_FAILURE, e });
  }
}

function* increaseBoatRentsViewCount(action) {
  try {
    const res = yield increaseBoatRentsViewCountApi(action.payload);
    yield put({ type: INCREASE_BOAT_RENT_VIEW_COUNT_SUCCESS, payload: res.data.boatRentIncreaseCount });
  } catch (e) {
    yield put({ type: INCREASE_BOAT_RENT_VIEW_COUNT_FAILURE, e });
  }
}


function* createRentBoatSaga() {
  yield takeLatest(CREATE_RENT_BOAT, createRentBoatData);
}

function* getBoatRentLookUpsSaga() {
  yield takeLatest(GET_BOAT_RENT_LOOKUPS, getBoatRentLookUpsData);
}

function* getUserBoatRentSaga() {
  yield takeLatest(GET_USER_BOAT_RENTS, getUserBoatRentsData);
}

function* getAllRentTypesSaga() {
  yield takeLatest(GET_ALL_RENT_TYPES, getAllRentTypesData)
}

function* deleteBoatRentSaga() {
  yield takeLatest(DELETE_BOAT_RENT, deleteBoatRentData);
}
function* getRentCategoryWiseBoatSaga() {
  yield takeLatest(GET_RENT_CATEGORY_WISE_BOATS, getRentCategoryWiseBoat);
}

function* getRentBoatInner() {
  yield takeLatest(GET_RENTS_INNER_BOAT, getRentBoatInnerData);
}

function* increaseBoatRentsViewCountSaga() {
  yield takeLatest(INCREASE_BOAT_RENT_VIEW_COUNT, increaseBoatRentsViewCount);
}

function* getRentTripCityWiseSaga() {
  yield takeLatest(GET_RENT_TRIP_CITY_WISE, getRentTripCityWiseData);
}


//get all boat rent types
function getRentBoatTypeApi(data) {
  return graphqlClient
    .query({
      query: getAllRentTripType,
      variables: {
        page: data.page,
        limit: data.limit
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      throw err;
    });
}

function* getRentBoatTypesData(action) {
  try {
    const data = yield getRentBoatTypeApi(action.payload);
    yield put({ type: GET_ALL_BOAT_RENT_TYPES_SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: GET_ALL_BOAT_RENT_TYPES_FAILURE, e });
  }
}

function* getRentBoatTypesSaga() {
  yield takeLatest(GET_ALL_BOAT_RENT_TYPES, getRentBoatTypesData);
}

function* updateRentBoatSaga() {
  yield takeLatest(UPDATE_RENT_BOAT, updateRentBoatData);
}

export default function* boatRentSaga() {
  yield all([
    createRentBoatSaga(), getBoatRentLookUpsSaga(), getUserBoatRentSaga(), deleteBoatRentSaga(), getAllRentTypesSaga(),
    getRentCategoryWiseBoatSaga(), getRentBoatInner(), increaseBoatRentsViewCountSaga(), getRentBoatTypesSaga(), getRentBoatTypesSaga(), 
    updateRentBoatSaga(), getRentTripCityWiseSaga()]);
}
