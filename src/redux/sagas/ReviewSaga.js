import { put, takeLatest, all } from 'redux-saga/effects';
import { graphqlClient } from '../../helpers/graphqlClient';
import { CREATE_REVIEW_SUCCESS, 
    CREATE_REVIEW_FAILURE, 
    CREATE_REVIEW, 
    GET_REVIEW_BY_MODULE_ID_SUCCESS, 
    GET_REVIEW_BY_MODULE_ID_FAILURE, 
    GET_REVIEW_BY_MODULE_ID } from '../actionTypes';
import { createReview, getReviewsByModuleId } from '../../graphql/ReviewSchema';

function crateArticleApi (data) {
    const userId = localStorage.getItem('userId');
    // const userId = loginReducer.currentUser.getItem('userId');
    const review = {...data, userId: userId}
    return graphqlClient
        .mutate ({
            mutation: createReview,
            variables: { input: data }
        })
        .then (res => {
            return res
        })
        .catch (err => {
            return err
        })
}

function* createReviewData(action) {
    try {
        const data = yield crateArticleApi(action.payload)
        yield put ({ type: CREATE_REVIEW_SUCCESS, payload: data})
    } catch (e) {
        yield put ({ type: CREATE_REVIEW_FAILURE, e})
    }
}

function* createReviewSaga() {
    yield takeLatest(CREATE_REVIEW, createReviewData)
}

function getReviewsByModuleIdApi(data) {
	return graphqlClient
		.query({
			query: getReviewsByModuleId,
			variables: { moduleId: data.moduleId },
			fetchPolicy: 'no-cache'
		})
		.then(res => {
			return res;
		})
		.catch(e => {
			throw e;
		});
}

function* getReviewsByModuleIdData(action) {
	try {
		const data = yield getReviewsByModuleIdApi(action.payload);
		yield put({ type: GET_REVIEW_BY_MODULE_ID_SUCCESS, payload: data });
	} catch (e) {
		yield put({ type: GET_REVIEW_BY_MODULE_ID_FAILURE, e });
	}
}

function* getReviewsByModuleIdSaga() {
	yield takeLatest(GET_REVIEW_BY_MODULE_ID, getReviewsByModuleIdData);
}

export default function* reviewSaga() {
    yield all([
        createReviewSaga(),
        getReviewsByModuleIdSaga()
    ])
    
}