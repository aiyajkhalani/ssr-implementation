import { put, takeLatest, all } from 'redux-saga/effects';
import { graphqlClient } from '../../helpers/graphqlClient'
import {
    GET_SALES_ENGINE_BY_BUYER,
    GET_SALES_ENGINE_BY_BUYER_SUCCESS,
    GET_SALES_ENGINE_BY_BUYER_FAILURE,
    GET_SALES_ENGINE_BY_SELLER,
    GET_SALES_ENGINE_BY_SELLER_SUCCESS,
    GET_SALES_ENGINE_BY_SELLER_FAILURE,
    GET_SALES_ENGINE_BY_SURVEYOR,
    GET_SALES_ENGINE_BY_SURVEYOR_SUCCESS,
    GET_SALES_ENGINE_BY_SHIPPER,
    GET_SALES_ENGINE_BY_SURVEYOR_FAILURE,
    GET_SALES_ENGINE_BY_SHIPPER_SUCCESS,
    GET_SALES_ENGINE_BY_SHIPPER_FAILURE,
    GET_SINGLE_SALES_ENGINE,
    GET_SINGLE_SALES_ENGINE_FAILURE,
    GET_SINGLE_SALES_ENGINE_SUCCESS,
    CREATE_SALES_ENGINE,
    CREATE_SALES_ENGINE_SUCCESS,
    CREATE_SALES_ENGINE_FAILURE,
    GET_ALL_SURVEYORS,
    GET_ALL_SURVEYORS_FAILURE,
    GET_ALL_SURVEYORS_SUCCESS,
    ASSIGN_SURVEYOR,
    ASSIGN_SURVEYOR_SUCCESS,
    ASSIGN_SURVEYOR_FAILURE,
    PAYMENT_REQUEST,
    PAYMENT_REQUEST_SUCCESS,
    PAYMENT_REQUEST_FAILURE,
    SALES_ENGINE_ASSIGN_SHIPPER_REQUEST,
    SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_FAILURE,
    SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_SUCCESS,
    GET_AGREEMENTS_CONTENTS,
    GET_AGREEMENTS_CONTENTS_SUCCESS,
    GET_AGREEMENTS_CONTENTS_FAILURE,
    CHECK_AGREEMENT,
    CHECK_AGREEMENT_SUCCESS,
    CHECK_AGREEMENT_FAILURE,
    SALES_ENGINE_SURVEYOR_ACCEPT_BUYER,
    SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_SUCCESS,
    SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_FAILURE,
    SALES_ENGINE_SURVEYOR_DECLINE_BUYER,
    SALES_ENGINE_SURVEYOR_DECLINE_BUYER_SUCCESS,
    SALES_ENGINE_SURVEYOR_DECLINE_BUYER_FAILURE,
    CREATE_SURVEYOR_REPORT,
    CREATE_SURVEYOR_REPORT_SUCCESS,
    CREATE_SURVEYOR_REPORT_FAILURE,
    SURVEYOR_REPORT_SUBMIT,
    SURVEYOR_REPORT_SUBMIT_SUCCESS,
    SURVEYOR_REPORT_SUBMIT_FAILURE,
    GET_SURVEYOR_REPORT_FAILURE,
    GET_SURVEYOR_REPORT_SUCCESS,
    GET_SURVEYOR_REPORT,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST_SUCCESS,
    SHIPPER_ACCEPT_SHIPMENT_REQUEST_FAILURE,
    SHIPPER_DECLINE_SHIPMENT_REQUEST,
    SHIPPER_DECLINE_SHIPMENT_REQUEST_SUCCESS,
    SHIPPER_DECLINE_SHIPMENT_REQUEST_FAILURE,
    GET_SALES_ENGINE_BY_BOAT,
    GET_SALES_ENGINE_BY_BOAT_SUCCESS,
    GET_SALES_ENGINE_BY_BOAT_FAILURE,
    CLEAR_GET_SINGLE_SALES_ENGINE_FLAG,
    ADD_SHIPMENT_PROPOSAL,
    ADD_SHIPMENT_PROPOSAL_SUCCESS,
    ADD_SHIPMENT_PROPOSAL_FAILURE,
    GET_ALL_SHIPMENT_PROPOSAL,
    GET_ALL_SHIPMENT_PROPOSAL_SUCCESS,
    GET_ALL_SHIPMENT_PROPOSAL_FAILURE,
    ADD_NEGOTIABLE_PRICE_SUCCESS,
    ADD_NEGOTIABLE_PRICE_FAILURE,
    ADD_NEGOTIABLE_PRICE,
    CLEAR_SALES_ENGINE_MY_BOAT_FLAG,
    ADD_SURVEY_OPTION,
    ADD_SURVEY_OPTION_SUCCESS,
    ADD_SURVEY_OPTION_FAILURE,
    CLEAR_SALES_ENGINE_SURVEYOR_DASHBOARD_FLAG,
    CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG,
    CLEAR_PAYMENT_FLAG,
    SKIP_SHIPMENT,
    SKIP_SHIPMENT_SUCCESS,
    SKIP_SHIPMENT_FAILURE,
    DECLINE_SURVEYOR,
    DECLINE_SURVEYOR_SUCCESS,
    DECLINE_SURVEYOR_FAILURE,
    CLEAR_SALES_ENGINE_SHIPMENT_FLAG,
    GET_AGENTS_BY_COUNTRY,
    GET_AGENTS_BY_COUNTRY_SUCCESS,
    GET_AGENTS_BY_COUNTRY_FAILURE,
    ASSIGN_AGENT,
    ASSIGN_AGENT_SUCCESS,
    ASSIGN_AGENT_FAILURE,
    GET_SALES_ENGINE_BY_AGENT,
    GET_SALES_ENGINE_BY_AGENT_SUCCESS,
    GET_SALES_ENGINE_BY_AGENT_FAILURE,
    GET_DOCUMENT_LINKS,
    GET_DOCUMENT_LINKS_SUCCESS,
    GET_DOCUMENT_LINKS_FAILURE,
    ADD_BOAT_SHIPMENT_LOCATION,
    ADD_BOAT_SHIPMENT_LOCATION_SUCCESS,
    ADD_BOAT_SHIPMENT_LOCATION_FAILURE,
    GET_COST_ESTIMATE,
    GET_COST_ESTIMATE_SUCCESS,
    GET_COST_ESTIMATE_FAILURE,
    START_SHIPMENT,
    START_SHIPMENT_FAILURE,
    START_SHIPMENT_SUCCESS,
    COMPLETE_SHIPMENT_FAILURE,
    COMPLETE_SHIPMENT,
    COMPLETE_SHIPMENT_SUCCESS,
    ADD_SHIPPER_DOCUMENTS,
    ADD_SHIPPER_DOCUMENTS_FAILURE,
    ADD_SHIPPER_DOCUMENTS_SUCCESS,
} from '../actionTypes';
import {
    getSalesEngineByBuyer,
    getSalesEngineBySeller,
    getSalesEngineBySurveyor,
    getSalesEngineByShipper,
    singleSalesEngine,
    createSalesEngine,
    getNearestSurveyors,
    paymentQuery,
    requestSurveyor,
    getAgreementContents,
    checkAgreement,
    salesEngineAssignShipperQuery,
    salesEngineSurveyorAcceptBuyerRequest,
    salesEngineSurveyorDeclineBuyerRequest,
    createSurveyorReport,
    surveyorReportSubmit,
    getSurveyorReport,
    shipperAcceptShipmentRequest,
    shipperDeclineShipmentRequest,
    getSalesEngineByBoat,
    salesEngineShipperSubmitPrice,
    salesEngineShipperList,
    sellerAddNegotiableBoatPrice,
    skipSurveyor,
    skipShipper,
    declineSurveyor,
    getAgentUsersByCountry,
    assignAgent,
    getSalesEngineByAgent,
    generateAgreementAndSurvey,
    addBoatShipmentLocation,
    getCostEstimate,
    startShipment,
    shipmentComplete,
    createShipperDocument
} from '../../graphql/salesEngineSchema';

function getBuyerSalesEnginesApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineByBuyer,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getSellerSalesEnginesApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineBySeller,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getBoatsSalesEnginesApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineByBoat,
            variables: { boatId: input },
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getSurveyorSalesEnginesApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineBySurveyor,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getShipperSalesEnginesApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineByShipper,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getSingleSalesEngineApi(input) {
    return graphqlClient
        .query({
            query: singleSalesEngine,
            variables: { id: input },
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function addSalesEngineApi(input) {
    return graphqlClient
        .mutate({
            mutation: createSalesEngine,
            variables: { input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}
function paymentApi(input) {

    return graphqlClient
        .mutate({
            mutation: paymentQuery,
            variables: { input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}
function salesEngineAssignShipperApi(input) {

    return graphqlClient
        .mutate({
            mutation: salesEngineAssignShipperQuery,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getAllNearestSurveyorsApi(input) {
    return graphqlClient
        .query({
            query: getNearestSurveyors,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function assignSalesEngineSurveyorApi(input) {
    return graphqlClient
        .mutate({
            mutation: requestSurveyor,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function declineSalesEngineSurveyorApi(input) {
    return graphqlClient
        .mutate({
            mutation: declineSurveyor,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getAllAgreementsContentsApi(input) {
    return graphqlClient
        .query({
            query: getAgreementContents,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function checkAgreementsApi(input) {
    return graphqlClient
        .mutate({
            mutation: checkAgreement,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function salesEngineSurveyorAcceptBuyerApi(input) {
    return graphqlClient
        .mutate({
            mutation: salesEngineSurveyorAcceptBuyerRequest,
            variables: { id: input.id, surveyorId: input.surveyorId },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function salesEngineSurveyorDeclineBuyerApi(input) {
    return graphqlClient
        .mutate({
            mutation: salesEngineSurveyorDeclineBuyerRequest,
            variables: { id: input.id, surveyorId: input.surveyorId },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}
function salesEngineSurveyorCreateReportApi(input) {
    return graphqlClient
        .mutate({
            mutation: createSurveyorReport,
            variables: { input: input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}
function surveyorSubmitReportApi(input) {
    return graphqlClient
        .mutate({
            mutation: surveyorReportSubmit,
            variables: { id: input.id, surveyorId: input.surveyorId },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}
function getSurveyorReportBySalesEngineApi(input) {
    return graphqlClient
        .query({
            query: getSurveyorReport,
            variables: { surveyorId: input.surveyorId, salesEngineId: input.salesEngineId },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function salesEngineShipperAcceptShipmentRequestApi(input) {
    return graphqlClient
        .mutate({
            mutation: shipperAcceptShipmentRequest,
            variables: { id: input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function salesEngineShipperDeclineShipmentRequestApi(input) {
    return graphqlClient
        .mutate({
            mutation: shipperDeclineShipmentRequest,
            variables: { id: input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function addShipmentProposalApi(input) {
    return graphqlClient
        .mutate({
            mutation: salesEngineShipperSubmitPrice,
            variables: { input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function salesEngineGetAllShipmentProposalApi(input) {
    return graphqlClient
        .query({
            query: salesEngineShipperList,
            variables: { salesEngineId: input },
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function addNegotiablePriceApi(input) {
    return graphqlClient
        .mutate({
            mutation: sellerAddNegotiableBoatPrice,
            variables: { input },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function addSurveyOptionApi(input) {
    return graphqlClient
        .mutate({
            mutation: skipSurveyor,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function skipShipmentProcessApi(input) {
    return graphqlClient
        .mutate({
            mutation: skipShipper,
            variables: input,
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function agentByCountryApi(input) {
    return graphqlClient
        .query({
            query: getAgentUsersByCountry,
            variables: { country: input.country },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function assignAgentApi(input) {
    return graphqlClient
        .mutate({
            mutation: assignAgent,
            variables: { agentId: input.agentId, id: input.id },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getSalesEngineByAgentApi(input) {
    return graphqlClient
        .query({
            query: getSalesEngineByAgent,
            variables: { page: input.page, limit: input.limit },
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getSalesEngineDocumentLinksApi(input) {
    return graphqlClient
        .query({
            query: generateAgreementAndSurvey,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function boatShipmentLocationApi(input) {
    return graphqlClient
        .mutate({
            mutation: addBoatShipmentLocation,
            variables: input,
            fetchPolicy: 'no-cache',
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function getCostEstimateApi(input) {
    return graphqlClient
        .query({
            query: getCostEstimate,
            fetchPolicy: "no-cache"
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function startShipmentApi(input) {
    return graphqlClient
        .mutate({
            mutation: startShipment,
            variables: { id: input.id }
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function completeShipmentApi(input) {
    return graphqlClient
        .mutate({
            mutation: shipmentComplete,
            variables: { id: input.id }
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}

function createShipperDocumentApi(input) {
    return graphqlClient
        .mutate({
            mutation: createShipperDocument,
            variables: { input }
        })
        .then(res => res)
        .catch(error => {
            return (error.networkError) ? error.networkError.result : error
        })
}


function* getBuyerSalesEngines(action) {
    const res = yield getBuyerSalesEnginesApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_BUYER_SUCCESS, payload: res.data.getSalesEngineByBuyer.items })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_BUYER_FAILURE, errors: res.errors })
    }
}

function* getSellerSalesEngines(action) {
    const res = yield getSellerSalesEnginesApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_SELLER_SUCCESS, payload: res.data.getSalesEngineBySeller })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_SELLER_FAILURE, errors: res.errors })
    }
}

function* getBoatsSalesEngines(action) {
    const res = yield getBoatsSalesEnginesApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_BOAT_SUCCESS, payload: res.data.getSalesEngineByBoat })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_BOAT_FAILURE, errors: res.errors })
    }
}

function* getSurveyorSalesEngines(action) {
    const res = yield getSurveyorSalesEnginesApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_SURVEYOR_SUCCESS, payload: res.data.getSalesEngineBySurveyor })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_SURVEYOR_FAILURE, errors: res.errors })
    }
}

function* getShipperSalesEngines(action) {
    const res = yield getShipperSalesEnginesApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_SHIPPER_SUCCESS, payload: res.data.getSalesEngineByShipper })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_SHIPPER_FAILURE, errors: res.errors })
    }
}

function* getSingleSalesEngine(action) {
    const res = yield getSingleSalesEngineApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SINGLE_SALES_ENGINE_SUCCESS, payload: res.data.singleSalesEngine })
    } else {
        yield put({ type: GET_SINGLE_SALES_ENGINE_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_GET_SINGLE_SALES_ENGINE_FLAG })
}

function* addSalesEngine(action) {
    const res = yield addSalesEngineApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: CREATE_SALES_ENGINE_SUCCESS, payload: res.data.createSalesEngine })
    } else {
        yield put({ type: CREATE_SALES_ENGINE_FAILURE, errors: res.errors })
    }
}

function* getAllNearestSurveyors(action) {
    const res = yield getAllNearestSurveyorsApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_ALL_SURVEYORS_SUCCESS, payload: res.data.salesEngineNearestSurveyors })
    } else {
        yield put({ type: GET_ALL_SURVEYORS_FAILURE, errors: res.errors })
    }
}

function* assignSalesEngineSurveyor(action) {
    const res = yield assignSalesEngineSurveyorApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ASSIGN_SURVEYOR_SUCCESS, payload: res.data.salesEngineBuyerRequestSurveyor })
    } else {
        yield put({ type: ASSIGN_SURVEYOR_FAILURE, errors: res.errors })
    }
}

function* declineSalesEngineSurveyor(action) {
    const res = yield declineSalesEngineSurveyorApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: DECLINE_SURVEYOR_SUCCESS, payload: res.data.buyerDeclineSurveyor })
    } else {
        yield put({ type: DECLINE_SURVEYOR_FAILURE, errors: res.errors })
    }
}

function* paymentData(action) {

    const res = yield paymentApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: PAYMENT_REQUEST_SUCCESS, payload: res.data.payment })
    } else {
        yield put({ type: PAYMENT_REQUEST_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_PAYMENT_FLAG })
}

function* salesEngineAssignShipperData(action) {
    const res = yield salesEngineAssignShipperApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_SUCCESS, payload: res.data.salesEngineAssignShipper })
    } else {
        yield put({ type: SALES_ENGINE_ASSIGN_SHIPPER_REQUEST_FAILURE, errors: res.errors })
    }
}

function* getAllAgreementsContents(action) {
    const res = yield getAllAgreementsContentsApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_AGREEMENTS_CONTENTS_SUCCESS, payload: res.data.salesEngineAgreementContent })
    } else {
        yield put({ type: GET_AGREEMENTS_CONTENTS_FAILURE, errors: res.errors })
    }
}

function* checkAgreements(action) {
    const res = yield checkAgreementsApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: CHECK_AGREEMENT_SUCCESS, payload: res.data.salesEngineAgreementCheck })
    } else {
        yield put({ type: CHECK_AGREEMENT_FAILURE, errors: res.errors })
    }
}

function* salesEngineSurveyorAcceptBuyerData(action) {
    const res = yield salesEngineSurveyorAcceptBuyerApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_SUCCESS, payload: res.data.salesEngineSurveyorAcceptBuyerRequest })
    } else {
        yield put({ type: SALES_ENGINE_SURVEYOR_ACCEPT_BUYER_FAILURE, errors: res.errors })
    }
}

function* salesEngineSurveyorDeclineBuyerData(action) {
    const res = yield salesEngineSurveyorDeclineBuyerApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SALES_ENGINE_SURVEYOR_DECLINE_BUYER_SUCCESS, payload: res.data.salesEngineSurveyorDeclineBuyerRequest })
    } else {
        yield put({ type: SALES_ENGINE_SURVEYOR_DECLINE_BUYER_FAILURE, errors: res.errors })
    }
}

function* salesEngineSurveyorCreateReportData(action) {
    const res = yield salesEngineSurveyorCreateReportApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: CREATE_SURVEYOR_REPORT_SUCCESS, payload: res })
    } else {
        yield put({ type: CREATE_SURVEYOR_REPORT_FAILURE, errors: res.errors })
    }
}

function* surveyorSubmitReportData(action) {
    const res = yield surveyorSubmitReportApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SURVEYOR_REPORT_SUBMIT_SUCCESS, payload: res })
    } else {
        yield put({ type: SURVEYOR_REPORT_SUBMIT_FAILURE, errors: res.errors })
    }
}

function* getSurveyorReportBySalesEngineData(action) {
    const res = yield getSurveyorReportBySalesEngineApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SURVEYOR_REPORT_SUCCESS, payload: res })
    } else {
        yield put({ type: GET_SURVEYOR_REPORT_FAILURE, errors: res.errors })
    }
}

function* salesEngineShipperAcceptShipmentRequest(action) {
    const res = yield salesEngineShipperAcceptShipmentRequestApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SHIPPER_ACCEPT_SHIPMENT_REQUEST_SUCCESS, payload: res.data.salesEngineShipperAcceptShipmentRequest })
    } else {
        yield put({ type: SHIPPER_ACCEPT_SHIPMENT_REQUEST_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG })
}

function* salesEngineShipperDeclineShipmentRequest(action) {
    const res = yield salesEngineShipperDeclineShipmentRequestApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SHIPPER_DECLINE_SHIPMENT_REQUEST_SUCCESS, payload: res.data.salesEngineShipperDeclineShipmentRequest })
    } else {
        yield put({ type: SHIPPER_DECLINE_SHIPMENT_REQUEST_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_SHIPPER_DASHBOARD_FLAG })
}

function* addShipmentProposal(action) {
    const res = yield addShipmentProposalApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ADD_SHIPMENT_PROPOSAL_SUCCESS, payload: res.data.salesEngineShipperSubmitPrice })
    } else {
        yield put({ type: ADD_SHIPMENT_PROPOSAL_FAILURE, errors: res.errors })
    }
}

function* salesEngineGetAllShipmentProposal(action) {
    const res = yield salesEngineGetAllShipmentProposalApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_ALL_SHIPMENT_PROPOSAL_SUCCESS, payload: res.data.salesEngineShipperList })
    } else {
        yield put({ type: GET_ALL_SHIPMENT_PROPOSAL_FAILURE, errors: res.errors })
    }
}

function* addNegotiablePriceData(action) {
    const res = yield addNegotiablePriceApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ADD_NEGOTIABLE_PRICE_SUCCESS, payload: res.data.sellerAddNegotiableBoatPrice })
    } else {
        yield put({ type: ADD_NEGOTIABLE_PRICE_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_MY_BOAT_FLAG })
}

function* addSurveyOption(action) {
    const res = yield addSurveyOptionApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ADD_SURVEY_OPTION_SUCCESS, payload: res.data.skipSurveyor })
    } else {
        yield put({ type: ADD_SURVEY_OPTION_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_MY_BOAT_FLAG })
}

function* skipShipmentProcess(action) {
    const res = yield skipShipmentProcessApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: SKIP_SHIPMENT_SUCCESS, payload: res.data.skipShipper })
    } else {
        yield put({ type: SKIP_SHIPMENT_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_SHIPMENT_FLAG })
}

function* agentByCountryData(action) {
    const res = yield agentByCountryApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_AGENTS_BY_COUNTRY_SUCCESS, payload: res.data })
    } else {
        yield put({ type: GET_AGENTS_BY_COUNTRY_FAILURE, errors: res.errors })
    }
}

function* assignAgentData(action) {
    const res = yield assignAgentApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ASSIGN_AGENT_SUCCESS, payload: res.data })
    } else {
        yield put({ type: ASSIGN_AGENT_FAILURE, errors: res.errors })
    }
}

function* getSalesEngineByAgentData(action) {
    const res = yield getSalesEngineByAgentApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_SALES_ENGINE_BY_AGENT_SUCCESS, payload: res.data })
    } else {
        yield put({ type: GET_SALES_ENGINE_BY_AGENT_FAILURE, errors: res.errors })
    }
}

function* getSalesEngineDocumentLinks(action) {
    const res = yield getSalesEngineDocumentLinksApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: GET_DOCUMENT_LINKS_SUCCESS, payload: res.data.generateAgreementAndSurvey })
    } else {
        yield put({ type: GET_DOCUMENT_LINKS_FAILURE, errors: res.errors })
    }
}

function* boatShipmentLocation(action) {
    const res = yield boatShipmentLocationApi(action.payload)

    if (res && res.data && !res.hasOwnProperty('errors')) {
        yield put({ type: ADD_BOAT_SHIPMENT_LOCATION_SUCCESS, payload: res.data.addBoatShipmentLocation })
    } else {
        yield put({ type: ADD_BOAT_SHIPMENT_LOCATION_FAILURE, errors: res.errors })
    }
    yield put({ type: CLEAR_SALES_ENGINE_SHIPMENT_FLAG })
}

function* getCostEstimateData() {
    try {
        const res = yield getCostEstimateApi()
        yield put({ type: GET_COST_ESTIMATE_SUCCESS, payload: res.data.getCostEstimate })
    } catch (error) {
        yield put({ type: GET_COST_ESTIMATE_FAILURE, error })
    }
}

function* startShipmentData(action) {
    try {
        const res = yield startShipmentApi(action.payload)
        yield put({ type: START_SHIPMENT_SUCCESS, payload: res.data })
    } catch (error) {
        yield put({ type: START_SHIPMENT_FAILURE, error })
    }
}

function* completeShipmentData(action) {
    try {
        const res = yield completeShipmentApi(action.payload)
        yield put({ type: COMPLETE_SHIPMENT_SUCCESS, payload: res.data })
    } catch (error) {
        yield put({ type: COMPLETE_SHIPMENT_FAILURE, error })
    }
}

function* createShipperDocumentData(action) {
    try {
        const res = yield createShipperDocumentApi(action.payload)
        yield put({ type: ADD_SHIPPER_DOCUMENTS_SUCCESS, payload: res.data })
    } catch (error) {
        yield put({ type: ADD_SHIPPER_DOCUMENTS_FAILURE, error })
    }
}


function* getBuyerSalesEnginesSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_BUYER, getBuyerSalesEngines)
}

function* getSellerSalesEnginesSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_SELLER, getSellerSalesEngines)
}

function* getBoatsSalesEnginesSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_BOAT, getBoatsSalesEngines)
}

function* getSurveyorSalesEnginesSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_SURVEYOR, getSurveyorSalesEngines)
}

function* getShipperSalesEnginesSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_SHIPPER, getShipperSalesEngines)
}

function* getSingleSalesEngineSaga() {
    yield takeLatest(GET_SINGLE_SALES_ENGINE, getSingleSalesEngine)
}

function* addSalesEngineSaga() {
    yield takeLatest(CREATE_SALES_ENGINE, addSalesEngine)
}

function* getAllNearestSurveyorsSaga() {
    yield takeLatest(GET_ALL_SURVEYORS, getAllNearestSurveyors)
}

function* assignSalesEngineSurveyorSaga() {
    yield takeLatest(ASSIGN_SURVEYOR, assignSalesEngineSurveyor)
}

function* declineSalesEngineSurveyorSaga() {
    yield takeLatest(DECLINE_SURVEYOR, declineSalesEngineSurveyor)
}

function* paymentSaga() {
    yield takeLatest(PAYMENT_REQUEST, paymentData)
}

function* salesEngineAssignShipperSaga() {
    yield takeLatest(SALES_ENGINE_ASSIGN_SHIPPER_REQUEST, salesEngineAssignShipperData)
}
function* getAllAgreementsContentsSaga() {
    yield takeLatest(GET_AGREEMENTS_CONTENTS, getAllAgreementsContents)
}

function* checkAgreementsSaga() {
    yield takeLatest(CHECK_AGREEMENT, checkAgreements)
}

function* salesEngineSurveyorAcceptBuyerSaga() {
    yield takeLatest(SALES_ENGINE_SURVEYOR_ACCEPT_BUYER, salesEngineSurveyorAcceptBuyerData)
}

function* salesEngineSurveyorDeclineBuyerSaga() {
    yield takeLatest(SALES_ENGINE_SURVEYOR_DECLINE_BUYER, salesEngineSurveyorDeclineBuyerData)
}
function* salesEngineSurveyorCreateReportSaga() {
    yield takeLatest(CREATE_SURVEYOR_REPORT, salesEngineSurveyorCreateReportData)
}
function* surveyorSubmitReportSaga() {
    yield takeLatest(SURVEYOR_REPORT_SUBMIT, surveyorSubmitReportData)
}
function* getSurveyorReportBySalesEngineSaga() {
    yield takeLatest(GET_SURVEYOR_REPORT, getSurveyorReportBySalesEngineData)
}

function* salesEngineShipperAcceptShipmentRequestSaga() {
    yield takeLatest(SHIPPER_ACCEPT_SHIPMENT_REQUEST, salesEngineShipperAcceptShipmentRequest)
}

function* salesEngineShipperDeclineShipmentRequestSaga() {
    yield takeLatest(SHIPPER_DECLINE_SHIPMENT_REQUEST, salesEngineShipperDeclineShipmentRequest)
}

function* addShipmentProposalSaga() {
    yield takeLatest(ADD_SHIPMENT_PROPOSAL, addShipmentProposal)
}

function* salesEngineGetAllShipmentProposalSaga() {
    yield takeLatest(GET_ALL_SHIPMENT_PROPOSAL, salesEngineGetAllShipmentProposal)
}

function* addNegotiablePriceSaga() {
    yield takeLatest(ADD_NEGOTIABLE_PRICE, addNegotiablePriceData)
}

function* addSurveyOptionSaga() {
    yield takeLatest(ADD_SURVEY_OPTION, addSurveyOption)
}

function* skipShipmentProcessSaga() {
    yield takeLatest(SKIP_SHIPMENT, skipShipmentProcess)
}

function* agentByCountrySaga() {
    yield takeLatest(GET_AGENTS_BY_COUNTRY, agentByCountryData)
}

function* assignAgentSaga() {
    yield takeLatest(ASSIGN_AGENT, assignAgentData)
}

function* getSalesEngineByAgentSaga() {
    yield takeLatest(GET_SALES_ENGINE_BY_AGENT, getSalesEngineByAgentData)
}

function* getSalesEngineDocumentLinksSaga() {
    yield takeLatest(GET_DOCUMENT_LINKS, getSalesEngineDocumentLinks)
}

function* boatShipmentLocationSaga() {
    yield takeLatest(ADD_BOAT_SHIPMENT_LOCATION, boatShipmentLocation)
}

function* getCostEstimateSaga() {
    yield takeLatest(GET_COST_ESTIMATE, getCostEstimateData)
}

function* startShipmentSaga() {
    yield takeLatest(START_SHIPMENT, startShipmentData)
}

function* completeShipmentSaga() {
    yield takeLatest(COMPLETE_SHIPMENT, completeShipmentData)
}

function* createShipperDocumentSaga() {
    yield takeLatest(ADD_SHIPPER_DOCUMENTS, createShipperDocumentData)
}

export default function* salesEngineSaga() {
    yield all([
        getBuyerSalesEnginesSaga(),
        getSellerSalesEnginesSaga(),
        getBoatsSalesEnginesSaga(),
        getSurveyorSalesEnginesSaga(),
        getShipperSalesEnginesSaga(),
        getSingleSalesEngineSaga(),
        addSalesEngineSaga(),
        getAllNearestSurveyorsSaga(),
        assignSalesEngineSurveyorSaga(),
        declineSalesEngineSurveyorSaga(),
        paymentSaga(),
        salesEngineAssignShipperSaga(),
        getAllAgreementsContentsSaga(),
        checkAgreementsSaga(),
        salesEngineSurveyorAcceptBuyerSaga(),
        salesEngineSurveyorDeclineBuyerSaga(),
        salesEngineSurveyorCreateReportSaga(),
        surveyorSubmitReportSaga(),
        getSurveyorReportBySalesEngineSaga(),
        salesEngineShipperAcceptShipmentRequestSaga(),
        salesEngineShipperDeclineShipmentRequestSaga(),
        addShipmentProposalSaga(),
        salesEngineGetAllShipmentProposalSaga(),
        addNegotiablePriceSaga(),
        addSurveyOptionSaga(),
        skipShipmentProcessSaga(),
        agentByCountrySaga(),
        assignAgentSaga(),
        getSalesEngineByAgentSaga(),
        getSalesEngineDocumentLinksSaga(),
        boatShipmentLocationSaga(),
        getCostEstimateSaga(),
        startShipmentSaga(),
        completeShipmentSaga(),
        createShipperDocumentSaga(),
    ]);
}