import { put, takeLatest, all } from "redux-saga/effects";
import {
  GET_ALL_MEDIA_ARTICLES_SUCCESS,
  GET_ALL_MEDIA_ARTICLES_FAILURE,
  GET_ALL_MEDIA_ARTICLES
} from "../actionTypes";
import { graphqlClient } from "../../helpers/graphqlClient";
import { getAllMediaArticles } from "../../graphql/mediaArticleSchema";

function getAllMediaArticlesApi(data) {
  return graphqlClient
    .query({
      query: getAllMediaArticles,
      variables: {
        page: data.page,
        limit: data.limit,
        isApproved: data.isApproved
      }
    })
    .then(res => res)
    .catch(error => {
      if (error.networkError) {
        return error.networkError.result;
      }
    });
}

function* getAllMediaArticle(data) {
  const res = yield getAllMediaArticlesApi(data);
  if (res && res.data && !res.hasOwnProperty("errors")) {
    yield put({
      type: GET_ALL_MEDIA_ARTICLES_SUCCESS,
      payload: res.data.getAllArticles
    });
  } else {
    yield put({ type: GET_ALL_MEDIA_ARTICLES_FAILURE, error: res.errors });
  }
}

function* getAllMediaArticlesSaga() {
  yield takeLatest(GET_ALL_MEDIA_ARTICLES, getAllMediaArticle);
}

export default function* mediaArticlesSaga() {
  yield all([getAllMediaArticlesSaga()]);
}
