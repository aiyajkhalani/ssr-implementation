import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import configureStore from "./redux/store";
import { graphqlClient } from "./helpers/graphqlClient";
import App from "./App";

import "./index.css";
import "./font.scss"
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter } from 'react-router-dom';


const { store, persistor } = configureStore();


const app = (
    <ApolloProvider client={graphqlClient}>
        <Provider store={store}>
            <PersistGate loading={<div>Loading ...</div>} persistor={persistor}>
                <BrowserRouter>
                <App />
                </BrowserRouter>
            </PersistGate>
        </Provider>
    </ApolloProvider>
);
const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate
if (typeof document !== 'undefined') {
    renderMethod(app, document.getElementById('root'));
}

if (process.env.NODE_ENV === 'development') {
    if (module.hot) {
        module.hot.accept();
    }

    if (!window.store) {
        window.store = store;
    }
}