import { gql } from "apollo-boost";

export const sendEmailLink = gql`
  mutation sendEmailLink($email: String!) {
    sendEmailLink(email: $email) {
      message
      statusCode
    }
  }
`;

export const getFeaturedHomeVideos = gql`
  query getVideoList($input: MediaInput!) {
    getVideoList(input: $input) {
      id
      type
      metatype
      url
      title
      description
      mediaOrder
      thumbnail
      status
    }
  }
`;

export const getGlobalMinimumPriceBoats = gql`
  query getGlobalMinimumPriceBoats($limit: Float) {
    getGlobalMinimumPriceBoats(limit: $limit) {
      _id
      minimumPrice
      countryImage
      cityName
      country
      count
    }
  }
`;


export const getExperiences = gql`
  query getExperiences ($moduleName: String!) {
    getExperiences (moduleName: $moduleName){
    id
    userId {
      firstName
      lastName
      image
      reviews{
        reviews{
            rating
            reviews
            createdAt
        }
    }
    }
    moduleName
    images
    description
    name
    destination
    rating
    experienceRef
  }
}
`
