import { gql } from "apollo-boost";
import { GeometricLocation, AdminUser } from "./schemaTypes/schemaTypes";

export const registerUser = gql`
  mutation createUser($input: UserCreateInput!) {
    createUser(user: $input) {
      id
      firstName
      middleName
      lastName
      email
      mobileNumber
      country
      address1
      address2
      street
      city
      zip
      postBox
      state
      holderName
      accountNumber
      bankName
      transitCode
      companyName
      geometricLocation ${GeometricLocation}
      preference
      provider {
        id
      }
      language
      title
      incorporationCertificate
      businessRegistrationNumber
      companyLogo
      secondContactFullName
      secondContactEmail
      companyWebsite
      mobileNumber
      accountActivated
      branchName
      instituteNumber
      branchAddress
      swiftCode
      addressOfAccountHolder
      role {
        id
        role
        aliasName
        type
      }
      token
      isActivated
      image
      createdAt
    }
  }
`;
export const loginUser = gql`
  mutation loginUser($input: LoginType!) {
    loginUser(input: $input) {
      id
      firstName
      middleName
      lastName
      email
      mobileNumber
      country
      address1
      address2
      language
      street
      city
      zip
      postBox
      state
      holderName
      accountNumber
      companyWebsite
      companyLogo
      companyName
      bankName
      transitCode
      branchName
      instituteNumber
      branchAddress
      swiftCode
      companyName
      title
      geometricLocation ${GeometricLocation}      
      preference
      incorporationCertificate
      businessRegistrationNumber
      companyLogo
      secondContactFullName
      secondContactEmail
      companyWebsite
      mobileNumber
      commercialLicence
      governmentId
      provider {
        id
      }
      officeAddress
      officeLocation ${GeometricLocation} 
      officeCity
      officeState
      officeCountry
      officeRoute
      officePlaceName
      officePostalCode
      accountActivated
      addressOfAccountHolder
      role {
        id
        role
        aliasName
        type
      }
      token
      isActivated
      image
      createdAt
    }
  }
`;

export const activateUser = gql`
  mutation activateUser($token: String!) {
    activateUser(token: $token) 
    ${AdminUser}
  }
`;

export const editUser = gql`
  query editUser($id: String!) {
    editUser(id: $id) {
      id
      firstName
      middleName
      middleName
      lastName
      email
      mobileNumber
      country
      address1
      address2
      street
      city
      zip
      postBox
      title
      state
      holderName
      accountNumber
      bankName
      transitCode
      branchName
      language
      instituteNumber
      branchAddress
      swiftCode
      addressOfAccountHolder
      token
      isActivated
      companyName
      commercialLicence
      governmentId
      geometricLocation ${GeometricLocation}      
      preference
      provider {
        id
      }
      incorporationCertificate
      businessRegistrationNumber
      companyLogo
      secondContactFullName
      secondContactEmail
      companyWebsite
      mobileNumber
      accountActivated
      image
    }
  }
`;

export const updateUser = gql`
  mutation updateProfile($input: ProfileInput!) {
    updateProfile(user: $input) {
      id
      firstName
      middleName
      lastName
      email
      mobileNumber
      country
      address1
      address2
      street
      city
      zip
      postBox
      state
      holderName
      title
      accountNumber
      companyWebsite
      companyLogo
      language
      companyName
      bankName
      transitCode
      branchName
      instituteNumber
      branchAddress
      swiftCode
      addressOfAccountHolder
      companyName
      geometricLocation ${GeometricLocation}      
      preference
      incorporationCertificate
      businessRegistrationNumber
      companyLogo
      secondContactFullName
      secondContactEmail
      companyWebsite
      mobileNumber
      accountActivated
      officeAddress
      officeLocation ${GeometricLocation}
      officeCity
      officeState
      officeCountry
      officeRoute
      officePlaceName
      officePostalCode
      commercialLicence
      governmentId
      provider {
        id
      }
      role {
        id
        role
        aliasName
        type
      }
      token
      isActivated
      image
    }
  }
`;

export const getAllRoles = gql`
  {
    getAllRoles {
      id
      role
      type
      aliasName
    }
  }
`;


export const getAllPageInformationByType = gql`
query getAllPageInformationByType($input: PageGetInput!) {
  getAllPageInformationByType(input: $input) {
  	id
    uiType
    module
    header
    content
  }
}
`;



export const forgotPassword = gql`
  mutation forgotPassword($password: String!) {
    forgotPassword(password: $password) {
      message
      statusCode
      id
    }
  }
`;

export const forgotPasswordMail = gql`
  mutation forgotPasswordMail($email: String!) {
    forgotPasswordMail(email: $email) {
      message
      statusCode
      id
    }
  }
`;
export const changePassword = gql`
  mutation forgotPassword($oldPassword: String!, $password: String!) {
    forgotPassword(oldPassword: $oldPassword, password: $password) {
      id
    }
  }
`;
export const resetPasswordQuery = gql`
  mutation resetPassword($oldPassword: String!, $password: String!) {
    resetPassword(oldPassword: $oldPassword, password: $password) {
      id
    }
  }
`;

export const changeUserAccountStatus = gql`
  mutation changeUserStatus($value: Boolean!, $column: String!, $id: String!) {
    changeUserStatus(value: $value, column: $column, id: $id) {
      message
      statusCode
    }
  }
`;

export const getUserLocation = gql`
  query getUserLocation ($ip: String!) {
    getUserLocation(ip: $ip) {
      city
      country
    }
  }
`
