import gql from "graphql-tag";
import { SalesEngine, Surveyor, creditPayment, SalesEngineAgreement, shipMent, SalesEngineAssignShipper, TypeCrud, SurveyorReport, Boat, AdminUser } from "./schemaTypes/schemaTypes";


// SALES ENGINE PROCESS
export const singleSalesEngine = gql`
    query singleSalesEngine($id: String!) {
        singleSalesEngine(id: $id)
            ${SalesEngine}
    }
`
export const sellerAddNegotiableBoatPrice = gql`
    mutation sellerAddNegotiableBoatPrice($input: SellerNegotiableBoatPriceInput!){
        sellerAddNegotiableBoatPrice(input: $input)
            ${SalesEngine}
        
    }`

export const skipSurveyor = gql`
    mutation skipSurveyor($id: String!, $isSurveyorSkip: Boolean!){
        skipSurveyor(id: $id, isSurveyorSkip: $isSurveyorSkip)
            ${SalesEngine}
    }`

export const createSalesEngine = gql`
    mutation createSalesEngine($input: SalesEngineInput!) {
        createSalesEngine(input: $input)
            ${SalesEngine}
    }
`

export const getNearestSurveyors = gql`
    query salesEngineNearestSurveyors($country: String!, $latitude: Float!, $longitude: Float!) {
        salesEngineNearestSurveyors(country: $country, latitude: $latitude, longitude: $longitude)
            ${Surveyor}
    }
`

export const requestSurveyor = gql`
    mutation salesEngineBuyerRequestSurveyor($id: String!, $surveyorId: String!) {
        salesEngineBuyerRequestSurveyor(id: $id, surveyorId: $surveyorId)
            ${SalesEngine}
    }
`

export const declineSurveyor = gql`
    mutation buyerDeclineSurveyor($id: String!, $surveyorId: String!) {
        buyerDeclineSurveyor(id: $id, surveyorId: $surveyorId)
            ${SalesEngine}
    }
`

export const paymentQuery = gql`
    mutation payment($input: PaymentInput!) {
        payment(input: $input)
            ${creditPayment}
    }
`


export const getAgreementContents = gql`
    query salesEngineAgreementContent($id: String!, $type: salesEngineAgreementTypeArg!) {
        salesEngineAgreementContent(id: $id, type: $type) 
            ${SalesEngineAgreement}
    }
`

export const generateAgreementAndSurvey = gql`
    query generateAgreementAndSurvey($id: String!, $type: String!) {
        generateAgreementAndSurvey(id: $id, type: $type) 
            ${SalesEngine}
    }
`

export const checkAgreement = gql`
    mutation salesEngineAgreementCheck($id: String!, $buyerId: String) {
        salesEngineAgreementCheck(id: $id, buyerId: $buyerId)
            ${SalesEngine}
    }
`

export const salesEngineAssignShipperQuery = gql`
    mutation salesEngineAssignShipper($id: String!, $shipperId: String!) {
        salesEngineAssignShipper(id: $id, shipperId: $shipperId) 
            ${SalesEngine}
    }
`

export const skipShipper = gql`
    mutation skipShipper($id: String!, $isShipperSkip: Boolean!) {
        skipShipper(id: $id, isShipperSkip: $isShipperSkip) 
            ${SalesEngine}
    }
`

export const salesEngineShipperList = gql`
    query salesEngineShipperList($salesEngineId: String!) {
        salesEngineShipperList(salesEngineId: $salesEngineId) {
            price
            proposalDocument
            shipper ${AdminUser}
            declineSalesEngineRequest
        }
    }
`

export const getAgentUsersByCountry = gql`
    query getAgentUserByCountry($country: String!) {
        getAgentUserByCountry(country: $country) 
            ${AdminUser}
    }
`;

export const assignAgent = gql`
    mutation assignAgent($agentId: String!, $id: String!) {
        assignAgent(agentId: $agentId, id: $id) 
            ${SalesEngine}
    }
`;

export const addBoatShipmentLocation = gql`
    mutation addBoatShipmentLocation($input: ShipmentLocationInput!, $id: String!) {
        addBoatShipmentLocation(input: $input, id: $id) 
            ${SalesEngine}
    }
`;

export const getCostEstimate = gql`
{
    getCostEstimate{
        id
        adminFee
        payPalFee
        payPalFixedPrice
        payPalFeeOther
        estimatedCostNote
        bankAccountDetail
        sitePayPalEmail
        payPalUserName
        payPalPassword
        payPalSignature
        surveyorReportSubmissionDays
        stopSalesEngineDays
        daysOfAutoDeleteBoat
        daysCounterForInspection
        daysCounterInactiveBuyers
        nexmoSmsKey
        nexmoSmsSecret
        nexmoSmsForm
        daysCounterSurveyorRespond
        createdAt
    }
}`

// BUYER DASHBOARD
export const getSalesEngineByBuyer = gql`
    query getSalesEngineByBuyer($isBuyItNow: Boolean! = false, $page: Int, $limit: Int) {
        getSalesEngineByBuyer(page: $page, limit: $limit, isBuyItNow: $isBuyItNow) {
            items  {
                id
                boat {
                    boatType ${TypeCrud}
                    adId
                }
                seller {
                    id
                    firstName
                    lastName
                }
                createdAt
                buySellProcess
            }
            total
        }
    }
`

// SELLER DASHBOARD
export const getSalesEngineBySeller = gql`
    query getSalesEngineBySeller($page: Int, $limit: Int) {
        getSalesEngineBySeller(page: $page, limit: $limit) {
            items {
                sellerBoatInquiry 
                boat {
                    id
                    adId
                    boatName
                }
            }
            total
        }
    }
`

export const getSalesEngineByBoat = gql`
    query getSalesEngineByBoat($boatId: String!, $page: Int, $limit: Int) {
        getSalesEngineByBoat(boatId: $boatId, page: $page, limit: $limit) {
            items ${SalesEngine}
            total
        }
    }
`

// SURVEYOR DASHBOARD
export const getSalesEngineBySurveyor = gql`
    query getSalesEngineBySurveyor($page: Int, $limit: Int) {
        getSalesEngineBySurveyor(page: $page, limit: $limit) {
            items ${SalesEngine}
            total
        }
    }
`
export const salesEngineSurveyorAcceptBuyerRequest = gql`
    mutation salesEngineSurveyorAcceptBuyerRequest($id: String!, $surveyorId: String!) {
        salesEngineSurveyorAcceptBuyerRequest(id: $id, surveyorId: $surveyorId)
            ${SalesEngine}
    }
`
export const salesEngineSurveyorDeclineBuyerRequest = gql`
    mutation salesEngineSurveyorDeclineBuyerRequest($id: String!, $surveyorId: String!) {
        salesEngineSurveyorDeclineBuyerRequest(id: $id, surveyorId: $surveyorId)
            ${SalesEngine}
}
`

export const createSurveyorReport = gql`
mutation createSurveyorReport($input: SurveyorReportInput!) {
    createSurveyorReport(input: $input){
        id
        comment
        report
        boatVerifications
        otherDocument
        video
        images
        surveyorId{
            id
            firstName
            lastName
        }
    }
}
`;

export const surveyorReportSubmit = gql`
mutation surveyorReportSubmit($id: String!, $surveyorId: String!) {
    surveyorReportSubmit(id: $id, surveyorId: $surveyorId) {
        message
        statusCode
    }
}
`;

export const getSurveyorReport = gql`
query getSurveyorReportBySalesEngine($surveyorId: String!, $salesEngineId: String!){
    getSurveyorReportBySalesEngine(surveyorId: $surveyorId, salesEngineId: $salesEngineId) {
        id
        comment
        report
        boatVerifications
        otherDocument
        video
        images
        surveyorId{
            id
            firstName
            lastName
        }
    }
}
`;


// SHIPPER DASHBOARD 
export const getSalesEngineByShipper = gql`
    query getSalesEngineByShipper($page: Int, $limit: Int) {
        getSalesEngineByShipper(page: $page, limit: $limit) {
            items  ${SalesEngine}
            total
        }
    }
`

export const shipperAcceptShipmentRequest = gql`
    mutation salesEngineShipperAcceptShipmentRequest($id: String!) {
        salesEngineShipperAcceptShipmentRequest(id: $id) {
            id
            shipperRequested {
                price
                proposalDocument
                declineSalesEngineRequest
            }
        }
    }
`

export const shipperDeclineShipmentRequest = gql`
    mutation salesEngineShipperDeclineShipmentRequest($id: String!) {
        salesEngineShipperDeclineShipmentRequest(id: $id) {
            id
            shipperRequested {
                price
                proposalDocument
                declineSalesEngineRequest
            }
        }
    }
`

export const salesEngineShipperSubmitPrice = gql`
    mutation salesEngineShipperSubmitPrice($input: ShipperSubmitPriceInput!) {
        salesEngineShipperSubmitPrice(input: $input)
            ${SalesEngine}
    }
`

// AGENT DASHBOARD
export const getSalesEngineByAgent = gql`
    query getSalesEngineByAgent($limit: Int, $page: Int) {
        getSalesEngineByAgent(limit: $limit, page:$page) {
            items
            ${SalesEngine}
    }
}`;

//start shipment
export const startShipment = gql`
    mutation shipmentStart($id: String!) {
        shipmentStart(id: $id) 
        ${SalesEngine}
    }
`;

//complete shipment
export const shipmentComplete = gql`
    mutation shipmentComplete($id: String!) {
        shipmentComplete(id: $id) 
        ${SalesEngine}
    }
`;

export const createShipperDocument = gql`
mutation createShipperDocument($input: ShipperDocumentInput!) {
    createShipperDocument(input: $input) {
        id
        shipper {
            id
            firstName
            lastName
        }
        salesEngine {
            id
        }
        billOfLading
        certificateOfOrigin
        insuranceCertificate
        receivedForShipment
        invoiceOfShipmentPayment
        otherDocument
    }
}
`;