import gql from 'graphql-tag';

export const getAllUserFaq = gql`
query getAllFaqList($page: Int, $limit: Int) {
  getAllFaqList(page: $page, limit: $limit) {
    items {
      status
      id
      description
      question
      __typename
    }
    __typename
  }
}`;