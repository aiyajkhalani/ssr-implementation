import { gql } from "apollo-boost";

export const getAllStates = gql`
{
  getAllStates{
    items{
      id
      name
      stateCode
    }
    total
  }
}
`
