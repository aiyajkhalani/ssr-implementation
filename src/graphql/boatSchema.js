import { gql } from "apollo-boost";
import { Boat, Yacht, Marina, BoatRent, LookUp, RatingReview, SuccessResponse, GeometricLocation, Role, AdminUser, SellerUser } from "./schemaTypes/schemaTypes";


export const boatByCitiesSchema = gql`
  query getBoatByCities($id: String!) {
    boatByCities(id: $id) 
      ${Boat}
  }
`;

export const getBoatShippers = gql`
  query getAllShipment($isAdmin: Boolean) {
    getAllShipment(isAdmin: $isAdmin) {
      id
      companyName
      companyLogo
      firstName
      lastName
      mobileNumber
      email
      image
      country
    }
  }
`;

export const getBoatLookUps = gql`
  {
    getBoatLookUps {
      typeName
      lookUp ${LookUp}
    }
  }
`;

export const getBoatTypes = gql`
query getAllBoatTypes($isAdmin: Boolean = true) {
  getAllBoatTypes(isAdmin: $isAdmin) {
      id
      name
      icon
      createdAt
      updatedAt
    }
  }
`;

export const getBoatTypesForManufacture = gql`
query getAllBoatTypes($isAdmin: Boolean = false) {
  getAllBoatTypes(isAdmin: $isAdmin) {
      id
      name
      icon
      createdAt
      updatedAt
    }
  }
`;

export const getBoatsByType = gql`
  query getBoatsByType($input: BoatTypes!, $page: Int, $limit: Int) {
    getBoatsByType(input: $input, page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;

export const getLatestBoats = gql`
  query getLatestBoats($country: String!, $page: Int, $limit: Int) {
    getLatestBoats(country: $country, page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;
export const getMostPopularBoats = gql`
  query getMostPopularBoats($country: String!, $page: Int, $limit: Int) {
    getMostPopularBoats(country: $country, page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;

export const getTopRatedBoats = gql`
  query getTopRatedBoats($country: String!, $page: Int, $limit: Int) {
    getTopRatedBoats(country: $country, page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;

export const getAllBoatByCountry = gql`
  query getAllBoatByCountry($country: String!, $page: Float, $limit: Float) {
    getAllBoatByCountry(country: $country, page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;

export const getAllBoatByUser = gql`
  query getAllBoatByUser($page: Int, $limit: Int) {
    getAllBoatByUser(page: $page, limit: $limit) {
      items ${Boat}
      total
    }
  }
`;

export const addBoat = gql`
  mutation createBoat($boat: BoatInput!) {
    createBoat(boat: $boat) {
      id
    }
  }
`;

export const searchBoat = gql`
  mutation searchBoat($input: BoatSearchInput!) {
    searchBoat(input: $input) {
      items ${Boat}
      total
    }
  }
`;

export const globalBoatSearch = gql`
  query searchBoatByString($country: String!, $query: String!) {
    searchBoatByString(country: $country, query: $query)
      ${Boat}
  }
`;

export const getBoatsSearchValues = gql`
  {
    getBoatsSearchValues {
      minPrice
      maxPrice
      minYearBuild
      maxYearBuild
      minLengthInFt
      maxLengthInFt
    }
  }
`;

export const multiSearch = gql`
  query multiSearch($country: String!, $query: String!, $isAdmin: Boolean = false) {
    multiSearch(country: $country, query: $query, isAdmin: $isAdmin) {

      boats ${Boat}

      usedBoats ${Boat}

      newBoats ${Boat}

      rentBoats ${BoatRent}

      yachtService ${Yacht}

      marinaStorage ${Marina}
    }
  }
`;

export const getRecentSearch = gql`
  {
    getRecentSearch {
      id
      query
    
      result {

        boats ${Boat}

        usedBoats ${Boat}

        newBoats ${Boat}
        
        rentBoats ${BoatRent}
  
        yachtService ${Yacht}

        marinaStorage ${Marina}
      }
    }
  }
`;

export const editBoat = gql`
  query editBoat($id: String!) {
    editBoat(boatId: $id) {
      id
      geometricLocation ${GeometricLocation}
      adStatus
      likeCount
      auctionRoom {
        boat {
          id
          boatName
          price
        }
        id
        startTime
        endTime
        startPrice
        minimumRaisedAmount
        auctionCurrentBid
        auctionStaringBid
        status
        bidDuration
        createdAt
      }
      featureStatus
      bestDealStatus
      mustBuyStatus
      status
      accidentDescribe
      boatReview
      placeName
      country
      address
      postalCode
      city
      complianceLabelNumber
      hullSerialNumber
      vehicleIdentificationNumber
      licenceNo
      attachId
      userCount
      ownershipAttachment
      projectDuration
      expectedCompletion
      isTaxEnabled
      tax
      listedBy
      boatType {
        id
        name
        icon
        createdAt
        updatedAt
      }
      boatStatus {
        id
        alias
      }
      boatName
      boatParking {
        id
        alias
      }
      trailer
      yearBuilt
      manufacturedBy
      hullMaterial {
        id
        alias
      }
      hullColor
      usedHours
      noOfEngines
      modelYear
      fuelType {
        id
        alias
      }
      fuelCapacity
      holdingCapacity
      freshWater
      engineManufacturer
      engineModel
      engineHp
      engineDrives {
        id
        alias
      }
      engineStroke {
        id
        alias
      }
      waterMarker
      bowThruster
      steeringSystem
      stabilizerSystem
      oilWaterSeparator
      fireBilgePump
      output
      generators
      emergencyGenerator
      batteriesCount
      batteryType
      decks
      heightInFt
      lengthInFt
      widthInFt
      weightInKg
      beam
      draft
      displacement
      mainSaloon {
        id
        alias
      }
      mainSaloonConvertible {
        id
        alias
      }
      numberOfHeads
      crewCabinCount
      crewBerthCount
      crewHeadsCount
      amenities {
        id
        alias
      }
      accessories {
        id
        alias
      }
      navigationEquipments {
        id
        alias
      }
      usage
      accidentHistory
      repairHistory
      lastMaintenance
      price
      description
      images
      layout
      video
      seller ${SellerUser}
      reviews ${RatingReview}
      adId
    }
  }
`;

export const boatChangeStatus = gql`
  mutation boatChangeStatus(
    $id: String!
    $columnName: String!
    $value: Boolean!
  ) {
    boatChangeStatus(id: $id, columnName: $columnName, value: $value) 
      ${SuccessResponse}
  }
`;

export const deleteBoat = gql`
  mutation deleteBoat($id: String!) {
    deleteBoat(id: $id) {
      id
      boatName
    }
  }
`;

export const updateBoat = gql`
  mutation updateBoat($input: BoatInput!) {
    updateBoat(boat: $input) {
      id
      boatName
    }
  }
`;

export const getCityWiseBoats = gql`
  query getCityWiseBoats(
    $page: Int
    $limit: Int
    $city: String!
    $country: String!
  ) {
    getCityWiseBoats(
      page: $page
      limit: $limit
      city: $city
      country: $country
    ) {
      items ${Boat}
      total
    }
  }
`;
export const categoryWiseBoats = gql`
  query categoryWiseBoats(
    $page: Int
    $limit: Int
    $categoryId: String!
    $country: String!
  ) {
    categoryWiseBoats(
      page: $page
      limit: $limit
      categoryId: $categoryId
      country: $country
    ) {
      items ${Boat}
      total
    }
  }
`;

export const createAuctionRoom = gql`
  mutation createAuctionRoom($auctionRoom: AuctionRoomInput!) {
    createAuctionRoom(auctionRoom: $auctionRoom) {
      id
      startTime
      endTime
      startPrice
      minimumRaisedAmount
    }
  }
`;

export const getUserAuctionRooms = gql`
  query getAuctionRoomsByUser($page: Int!, $limit: Int!) {
    getAuctionRoomsByUser(page: $page, limit: $limit) {
      items {
        boat {
          id
          boatName
          price
        }
        id
        startTime
        endTime
        startPrice
        minimumRaisedAmount
        status
        createdAt
      }
      total
    }
  }
`;

export const getAuctionAllBids = gql`
  query getAuctionBids($page: Int!, $limit: Int!, $auctionId: String!) {
    getAuctionBids(page: $page, limit: $limit, auctionId: $auctionId) {
      items {
        auctionRoom {
          #boat {
            #id
            #boatName
            #price
          #}
          id
          startTime
          endTime
          startPrice
          minimumRaisedAmount
          status
          createdAt
        }
        id
        price
        status
        user {
          id
          firstName
          lastName
        }
        createdAt
      }
      total
    }
  }
`;

export const getAllAuctionRooms = gql`
query getAllAuctionRoom($page: Int!, $limit: Int!) {
  getAllAuctionRoom(page: $page, limit: $limit) {
    items {
      boat {
        id
        yearBuilt
        country
        city
        boatType{
          name
        }
        lengthInFt
        boatName
        price
        images
        seller {
          firstName
          lastName
        }
      }
      id
      startPrice
      startTime
      endTime
      minimumRaisedAmount
      status
      createdAt
    }
    total
  }
}
`;

export const increaseBoatViewCount = gql`
  mutation boatIncreaseCount($boatId: String!) {
    boatIncreaseCount(boatId: $boatId) 
      ${SuccessResponse}
  }
`
export const getBannerByModuleQuery = gql`
query getBannerByModule(
    $input : GetMediaInput!
  ) {
    getBannerByModule(
        input : $input
    ){
        id
        type
        metatype
        url
        title
        mediaOrder
        thumbnail
        isBanner
    }
}`

export const getUserDashBoardCountQuery = gql`
query getUserDashBoardCount($role: String!) {
    getUserDashBoardCount(role: $role) 
      {
        auctionRoom
        boatShow
        article
        manageHomeAdvertisement
        boat
        broker
        surveyor
        boatRent
        marina
        salesEngine
        salesEngineCount
        salesEngineArchiveCount
        salesEngineBuyItNowCount
      }
  }
`

export const createAuctionBidQuery = gql`
  mutation createAuctionBid($input: AuctionBidInput!) {
    createAuctionBid(input: $input) {
      id
      price
      status
    }
  }
`;