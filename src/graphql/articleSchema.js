import gql from 'graphql-tag';

export const createArticle = gql`
	mutation createArticle($input: ArticleInput!) {
		createArticle(input: $input) {
			id
			title
			titleSlug
			description
			file
			articleViewCount
			articleApproved
			status
			createdAt
			updatedAt
		}
	}
`;


export const getAllArticles = gql`
	query getAllArticlesByUser($page: Int!, $limit: Int!) {
		getAllArticlesByUser(page: $page, limit: $limit) {
			items {
				id
				title
				description
				titleSlug
				file
				articleViewCount
				articleApproved
				status
				createdAt
				updatedAt
			}
			total
		}
	}
`;
