import gql from "graphql-tag";
import { AdminUser } from "./schemaTypes/schemaTypes";

export const getAllReviews = gql`
{
    getAllReviews{
        moduleId
        id
        userId {
          city
          country
          firstName
          lastName
          id
          image
        }
        rating
        reviews
        reviewStatus
        createdAt
        isAbusive
  }
}
`;
