import gql from "graphql-tag";
import {
  GeometricLocation,
  RatingReview,
  RentType,
  LookUp,
  Boat,
  BoatRent,
  SellerUser
} from "./schemaTypes/schemaTypes";

export const searchBoatRent = gql`
  mutation searchBoatRent($input: BoatRentSearchInput!) {
    searchBoatRent(input: $input) {
      items ${BoatRent}
      total
    }
  }
`;

export const getRentBoatsByTrip = gql`
  query getRentBoatsByTrip(
    $page: Int
    $limit: Int
    $trip: String!
    $country: String!
  ) {
    getRentBoatsByTrip(
      page: $page
      limit: $limit
      trip: $trip
      country: $country
    ) {
      items {
        id
        boatName
        price
        city
        country
        images
        rating
        geometricLocation ${GeometricLocation}
        tripType ${RentType}
        trip {
          id
          alias
        }
        address
        tripAddress
        noOfUnit
        boatLength
      }
      total
    }
  }
`;

export const getMostPopularBoatRents = gql`
  query getMostPopularBoatRents(
    $page: Int
    $limit: Int
    $country: String!
  ) {
    getMostPopularBoatRents(page: $page, limit: $limit, country: $country) {
      items {
        address
        tripAddress
        adStatus
        id
        userCount
        city
        country
        placeName
        postalCode
        route
        state
        boatName
        images
        price
        rating
        captainName
        createdAt
        trip ${LookUp}
        geometricLocation ${GeometricLocation}
        tripType{
          id
          name
        }
        boatLength
      }
      total
    }
  }
`;

export const recommendedBoatRents = gql`
  query recommendedBoatRents($country: String!, $page: Int, $limit: Int) {
    recommendedBoatRents(country: $country, page: $page, limit: $limit) {
      items {
        id
        adId
        address
        city
        country
        geometricLocation ${GeometricLocation}
        placeName
        postalCode
        route
        state

        adStatus
        boatRentStatus

        boatName
        captainName
        tripAddress
        tripCity
        tripCountry
        tripPlaceName
        tripPostalCode
        tripRoute
        tripState
        tripDuration
        whatToBring
        aboutUs
        descriptionOfOurTrip
        policy
        deposit {
          id
          alias
        }
        images
        youtubeLinkVideo
        boatLayout

        userCount
        tripLatitude
        tripLongitude
        boatLength
        bedroomAndCabins
        maximumGuest
        bathrooms
        seatsOnBoat
        price
        deckAndEntertainment{
          id
          alias
        }

        trip ${LookUp}
        tripType ${RentType}

        reviews ${RatingReview}

        user {
          id
          firstName
        }
        rating
        startTime
        endTime
        createdAt
      }
      total
    }
  }
`;

export const getAllRentTripType = gql`
  query getAllRentTypes(
    $page: Float!
    $limit: Int!
    $isAdmin: Boolean = false
  ) {
    getAllRentTypes(page: $page, limit: $limit, isAdmin: $isAdmin) {
      items {
        id
        name
        icon
        tripId
        createdAt
        updatedAt
      }
      total
    }
  }
`;

export const getBoatRentTripCities = gql`
  query getBoatRentTripCities($limit: Float) {
    getBoatRentTripCities(limit: $limit) {
      cityName
      countryImage
      minimumPrice
    }
  }
`;
export const getRentCityWiseBoats = gql`
  query getBoatRentTripCityWise(
   
    $city: String!
    $country: String!
  ) {
    getBoatRentTripCityWise(
    
      city: $city
      country: $country
    ) {
      items {
        id
        adId
        address
        city
        country
        geometricLocation ${GeometricLocation}
        placeName
        postalCode
        route
        state

        adStatus
        boatRentStatus

        boatName
        captainName
        tripAddress
        tripCity
        tripCountry
        tripPlaceName
        tripPostalCode
        tripRoute
        tripState
        tripDuration
        whatToBring
        aboutUs
        descriptionOfOurTrip
        policy
        deposit {
          id
          alias
        }
        images
        youtubeLinkVideo
        boatLayout

        userCount
        tripLatitude
        tripLongitude
        boatLength
        bedroomAndCabins
        maximumGuest
        bathrooms
        seatsOnBoat
        price
        deckAndEntertainment{
          id
          alias
        }

        trip ${LookUp}
        tripType ${RentType}

        reviews ${RatingReview}

        user {
          id
          firstName
        }
        rating
        startTime
        endTime
        createdAt
      }
      total
    }
  }
`;
