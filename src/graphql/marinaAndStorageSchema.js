import gql from "graphql-tag";
import { GeometricLocation, TypeCrud, SellerUser, AdminUser, LookUp, Marina, Role } from "./schemaTypes/schemaTypes";

export const addMarinaAndStorage = gql`
  mutation createMarina($input: MarinaInput!) {
    createMarina(input: $input) {
      id
      address
      city
      country
      geometricLocation ${GeometricLocation}
      postalCode
      route
      noneMembersRate
      membershipBenefits
      memberEvents
      promotionRunning
      exclusiveFacilities
      membersReview
      newsEvents
      berth
      facilities
      storage
      membersRate
      salesmanFullName
      salesmanContact
      contactPersonImage
      youtubeLinkVideo
      images
      userStatus
      accountStatus
      createdAt
    }
  }
`;

export const getAllMarinaStorage = gql`
  {
    getMarinaList {
      id
      address
      city
      country
      geometricLocation ${GeometricLocation}
      postalCode
      route
      provider ${LookUp}
      noneMembersRate
      membershipBenefits
      memberEvents
      promotionRunning
      exclusiveFacilities
      membersReview
      newsEvents
      berth
      facilities
      storage
      membersRate
      serviceProvide ${TypeCrud}
      serviceDescription
      salesmanFullName
      salesmanContact
      contactPersonImage
      youtubeLinkVideo
      images
      userStatus
      accountStatus
      createdAt
      rating
    }
  }
`;

export const getTypeWiseLookup = gql`
  query getTypeWiseLookup($type: String!) {
    getTypeWiseLookup(type: $type) {
      id
      alias
      typeId {
        id
        type
      }
      lookUpName
    }
  }
`;

export const getMarinasByType = gql`
  query getMarinasByType(
    $page: Float!
    $limit: Int!
    $country: String!
    $typeId: String!
  ) {
    getMarinasByType(
      page: $page
      limit: $limit
      country: $country
      typeId: $typeId
    ) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        membersRate
        salesmanFullName
        salesmanContact
        contactPersonImage
        youtubeLinkVideo
        images
        userStatus
        accountStatus
        createdAt
        rating
      }
      total
    }
  }
`;
export const marinaStorageByUser = gql`
  query marinaStorageByUser($page: Int, $limit: Int) {
    marinaStorageByUser(page: $page, limit: $limit) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        serviceProvide ${TypeCrud}
        membersRate
        salesmanFullName
        salesmanContact
        contactPersonImage
        youtubeLinkVideo
        images
        userStatus
        accountStatus
        createdAt
        adId
        rating
      }
      total
    }
  }
`;
export const getRecentlyAddedMarinaStorage = gql`
  query getAllMarinaStorageByCountry(
    $page: Int!
    $limit: Int!
    $country: String!
  ) {
    getAllMarinaStorageByCountry(
      page: $page
      limit: $limit
      country: $country
    ) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        membersRate
        salesmanFullName
        salesmanContact
        placeName
        serviceDescription
        contactPersonImage
        youtubeLinkVideo
        images
        userStatus
        accountStatus
        createdAt
        adId
        rating
        user{
          image,
          firstName,
          lastName,
          companyLogo
        }
      }

      total
    }
  }
`;

export const getSingleMarinaStorage = gql`
  query editMarina($id: String!) {
    editMarina(id: $id) {
      id
      address
      city
      country
      geometricLocation ${GeometricLocation}
      postalCode
      route
      provider ${LookUp}
      noneMembersRate
      membershipBenefits
      memberEvents
      promotionRunning
      exclusiveFacilities
      membersReview
      userCount
      newsEvents
      berth
      facilities
      serviceProvide ${TypeCrud}
      serviceDescription
      storage
      membersRate
      salesmanFullName
      salesmanContact
      contactPersonImage
      youtubeLinkVideo
      images
      userStatus
      accountStatus
      createdAt
      adId
      user {
        createdAt
    address
    city
    country
    placeName
    postalCode
    route
    state
    like
    title
    firstName
    middleName
    lastName
    secondContactFullName
    secondContactEmail
    companyWebsite
    mobileNumber
    address1
    address2
    street
    zip
    postBox
    preference
    language
    companyName
    holderName
    bankName
    branchName
    branchAddress
    addressOfAccountHolder
    accountNumber
    transitCode
    instituteNumber
    companyLogo
    incorporationCertificate
    businessRegistrationNumber
    swiftCode
    token
    id
    email
    language
    geometricLocation ${GeometricLocation}

    isActivated
    accountActivated
    
    provider ${LookUp}
    
    role ${Role} 
    image
    governmentId
    commercialLicence
    documentVerification {
        commercialLicenceVerified
        emailVerified
        mobileVerified
        governmentIdVerified
    }
    reviews{
        reviews{
            userId ${AdminUser}
            rating
            reviews
            createdAt
        }
        ratings{
            averageRating
            count
        }
    }
    relatedItems {
        id
        boatName
        rating
        placeName
        manufacturedBy
        description
        price
        video
        state
        address
        country
        city
        images
        yearBuilt
        lengthInFt
        
    }
      }
      reviews {
        reviews {
          moduleId
          id
          userId {
            id
            firstName
          }
          rating
          reviews
          reviewStatus
          createdAt
        }
        ratings {
          _id
          averageRating
          count
        }
      }
    }
  }
`;

export const updateMarinaStorage = gql`
  mutation updateMarina($input: MarinaInput!) {
    updateMarina(input: $input) {
      id
    }
  }
`;

export const deleteMarinaStorage = gql`
  mutation deleteMarina($id: String!) {
    deleteMarina(id: $id) {
      id
    }
  }
`;

export const getMostViewedMarinaStorage = gql`
  query getMostViewedMarinasList(
    $page: Int!
    $limit: Int!
    $country: String!
  ) {
    getMostViewedMarinasList(page: $page, limit: $limit, country: $country) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        membersRate
        salesmanFullName
        salesmanContact
        contactPersonImage
        youtubeLinkVideo
        images
        adId
        rating
        user ${AdminUser}
      }
      total
    }
  }
`;

export const searchMarinaStorage = gql`
  mutation searchMarinaStorage($input: MarinaSearchInput!) {
    searchMarinaStorage(input: $input) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        serviceProvide ${TypeCrud}
        membersRate
        salesmanFullName
        salesmanContact
        contactPersonImage
        youtubeLinkVideo
        images
        userStatus
        accountStatus
        createdAt
        adId
        createdAt
        rating
      }
      total
    }
  }
`;

export const increaseMarinaViewCount = gql`
  mutation marinaIncreaseCount($marinaId: String!) {
    marinaIncreaseCount(marinaId: $marinaId) {
        id
        message
        statusCode
    }
  }
`
export const getMoreServicesQuery = gql`
  query getAllMarinaTypes($isAdmin: Boolean = false) {
    getAllMarinaTypes(isAdmin: $isAdmin) {
      items{ 
          id
          name
          icon
          createdAt
          updatedAt
      }
   }
  }
`;

export const getTopRatedMarinaStorageQuery = gql`
  query getTopRatedMarinaList(
    $page: Int!
    $limit: Int!
    $country: String!
  ) {
    getTopRatedMarinaList(page: $page, limit: $limit, country: $country) {
      items {
        id
        address
        city
        country
        geometricLocation ${GeometricLocation}
        postalCode
        route
        provider ${LookUp}
        noneMembersRate
        membershipBenefits
        memberEvents
        promotionRunning
        exclusiveFacilities
        membersReview
        newsEvents
        berth
        facilities
        storage
        membersRate
        salesmanFullName
        salesmanContact
        contactPersonImage
        youtubeLinkVideo
        images
        adId
        userCount
        rating
        user ${AdminUser}
      }
      total
    }
  }`
export const getMarinaStorageServiceListQuery = gql`
  query marinaStorageByService(
    $page: Int!
    $limit: Int!
    $serviceId: String!
  ) {
    marinaStorageByService(page: $page, limit: $limit, serviceId: $serviceId) {
      items {
        id
        images
        city
        geometricLocation ${GeometricLocation}
        country
        rating
        reviews {
          reviews {
            moduleId
            id
            userId {
              id
              firstName
            }
            rating
            reviews
            reviewStatus
            createdAt
          }
          ratings {
            _id
            averageRating
            count
          }
        }
        serviceProvide ${TypeCrud}
        user{
          image,
          firstName,
          lastName
        }
        membersRate
        facilities
        userCount
      }
      total
    }
  }`

export const getSimilarMarinaList = gql`
  query getSimilarMarinaList($id: String!){
    getSimilarMarinaList(id: $id){
      items ${Marina}
      total
    }
  }`