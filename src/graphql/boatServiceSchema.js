import { gql } from "apollo-boost";
import { TypeCrud, Yacht, GeometricLocation } from "./schemaTypes/schemaTypes";

export const createBoatService = gql`
  mutation createOrUpdateYachtService($input: YachtInput!) {
    createOrUpdateYachtService(input: $input) {
      items ${Yacht}
      total
    }
  }
`;

export const typeWiseService = gql`
  query getYachtServiceByServiceType($page: Int!, $limit: Int!, $typeId: String!, $country: String!) {
    getYachtServiceByServiceType(page: $page, limit: $limit, typeId: $typeId, country: $country) {
      items ${Yacht}
      total
    }
  }
`;

export const getRecentlyAddedService = gql`
  query getAllYachtServicesByCountry($page: Int!, $limit: Int!, $country: String!) {
    getAllYachtServicesByCountry(page: $page, limit: $limit, country: $country) {
      items ${Yacht}
      total
    }
  }
`;

export const getUserBoatService = gql`
  {
    yachtServiceByUser {
      address
      placeName
      postalCode
      route
      state
      city
      country
      id
      service
      reviews
      featuresAndAdvantages
      teamCommitment
      qualityOfService
      salesManFullName
      contactOfSalesMan
      uploadSalesManPhoto
      youtubeUrl
      adId
      userStatus
      accountStatus
      rating
      images
      geometricLocation ${GeometricLocation}
      serviceProvide ${TypeCrud}
      createdAt
    }
  }
`;

export const getMostViewedBoatServices = gql`
  query mostViewedYachtServices($page: Int!,$limit: Int!,$country: String!) {
    mostViewedYachtServices(page: $page, limit: $limit, country: $country) {
      items ${Yacht}
      total
    }
  }
`;

export const searchYachtService = gql`
  query searchYachtService($input: YachtSearchInput!) {
    searchYachtService(input: $input) {
      items ${Yacht}
      total
    }
  }
`;

export const getAllBoatServiceTypes = gql`{
    getAllBoatServiceTypes {
        items ${TypeCrud}
        total
    }
}    
`;

export const EditYachtService = gql`
  query EditYachtService($id: String!){
    EditYachtService(id: $id)
      ${Yacht}
    
  }`
