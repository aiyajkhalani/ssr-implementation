import gql from 'graphql-tag'

export const categoryWiseVideos = gql`
    query categoryWiseAd($input: GetMediaInput!) {
        getAllVideosList(input: $input){
            id
            url
            title
            type
            metatype
            thumbnail
            mediaOrder
            description
            createdAt
            isBanner
            status
        }
    }`


 