import gql from 'graphql-tag'

export const getBannerByModuleQuery = gql`
query getBannerByModule(
    $input : GetMediaInput!
  ) {
    getBannerByModule(
        input : $input
    ){
        id
        type
        metatype
        url
        title
        mediaOrder
        thumbnail
        isBanner
    }
}`