import { Trip } from "../../components";

export const LookUp = `{
    id
    alias
    lookUpName
  
  }`;

export const TypeCrud = `{
    id
    name
    icon
    # createdAt 
    # updatedAt
}`;

export const Role = `{
    id
    role
    type
    aliasName
}`;

export const SuccessResponse = `{
    id
    message
    statusCode
}`;

export const GeometricLocation = `{
    type
    coordinates
}`;

export const AdminUser = `{
    address
    city
    country
    placeName
    postalCode
    route
    state
    like
    title
    firstName
    middleName
    lastName
    secondContactFullName
    secondContactEmail
    companyWebsite
    mobileNumber
    address1
    address2
    street
    zip
    postBox
    preference
    language
    companyName
    holderName
    bankName
    branchName
    branchAddress
    addressOfAccountHolder
    accountNumber
    transitCode
    instituteNumber
    companyLogo
    incorporationCertificate
    businessRegistrationNumber
    swiftCode
    token
    id
    email
    relatedItems {
        id
        boatName
        rating
        placeName
        manufacturedBy
        description
        price
        video
        state
        address
        country
        city
        images
        yearBuilt
        lengthInFt
        geometricLocation ${GeometricLocation}
        boatStatus ${LookUp}
        boatType
        {
            id
            name
        }
    }
    geometricLocation ${GeometricLocation}

    isActivated
    accountActivated
    
    provider ${LookUp}
    
    role ${Role} 
    image
    governmentId
    commercialLicence
    
    documentVerification {
        commercialLicenceVerified
        emailVerified
        mobileVerified
        governmentIdVerified
    }
    reviews{
        reviews{
            rating
            reviews
            createdAt
        }
        ratings{
            averageRating
            count
        }
    }


}`;

export const RatingReview = `{
    ratings {
        _id
        averageRating
        count
    }
    reviews {
        id
        moduleId
        reviews
        rating
        reviewStatus
        userId ${AdminUser}
        createdAt
    }
}`;

export const SellerUser = `{
    createdAt
    address
    city
    country
    placeName
    postalCode
    route
    state
    like
    title
    firstName
    middleName
    lastName
    secondContactFullName
    secondContactEmail
    companyWebsite
    mobileNumber
    address1
    address2
    street
    zip
    postBox
    preference
    language
    companyName
    holderName
    bankName
    branchName
    branchAddress
    addressOfAccountHolder
    accountNumber
    transitCode
    instituteNumber
    companyLogo
    incorporationCertificate
    businessRegistrationNumber
    swiftCode
    token
    id
    email
    language
    geometricLocation ${GeometricLocation}

    isActivated
    accountActivated
    
    provider ${LookUp}
    
    role ${Role} 
    image
    governmentId
    commercialLicence
    documentVerification {
        commercialLicenceVerified
        emailVerified
        mobileVerified
        governmentIdVerified
    }
    reviews{
        reviews{
            userId ${AdminUser}
            rating
            reviews
            createdAt
        }
        ratings{
            averageRating
            count
        }
    }
    relatedItems {
        id
        boatName
        rating
        placeName
        manufacturedBy
        description
        price
        video
        state
        address
        country
        city
        images
        yearBuilt
        lengthInFt
        geometricLocation ${GeometricLocation}
        boatStatus ${LookUp}
        boatType
        {
            id
            name
        }
    }
}`;

export const Boat = `{
    id
    adId
    geometricLocation ${GeometricLocation}    
    placeName
   # auctionRoom{
       # startTime
       # endTime
        # bidDuration
        # auctionStaringBid
        # auctionCurrentBid
        # inspectionCost
    # }
    city
    country
    address
    postalCode
    adStatus
    featureStatus
    bestDealStatus
    mustBuyStatus
    status
    boatName
    listedBy
    price
    yearBuilt
    description
    projectDuration
    expectedCompletion
    manufacturedBy
    trailer
    engineManufacturer
    engineHp
    images
    layout
    video
    isTaxEnabled
    tax
    boatType ${TypeCrud}
    boatStatus ${LookUp}
    boatParking ${LookUp}
    seller ${AdminUser}
    rating
    createdAt
    lengthInFt

}`;
export const RecentServicesInner = `{
    address
    city
    country
    geometricLocation ${GeometricLocation}
    placeName
    postalCode
    route
    state
    id
    like
    title
    provider ${LookUp}
    latitude
    longitude
    firstName
    middleName
    lastName
    secondContactFullName
    secondContactEmail
    companyWebsite
    mobileNumber
    companyLogo
    companyName
}`;

export const Yacht = `{
        
    address
    placeName
    postalCode
    route
    state
    
    city
    country
    id
    service
    reviews
    featuresAndAdvantages
    teamCommitment
    qualityOfService
    salesManFullName
    contactOfSalesMan
    uploadSalesManPhoto
    youtubeUrl
    adId

    userStatus
    accountStatus
    
    userCount
    rating
    
    images

    geometricLocation ${GeometricLocation}
    
    ratedReviews ${RatingReview}

    serviceProvide ${TypeCrud}

    user ${RecentServicesInner}

    createdAt
  }`;

export const Marina = `{
        
    address
    placeName
    postalCode
    route
    state
    _id
    noneMembersRate
    membersRate
    youtubeLinkVideo

    city
    country
    id
    serviceDescription
    membershipBenefits
    memberEvents
    promotionRunning
    exclusiveFacilities
    membersReview
    newsEvents
    berth
    facilities
    storage
    salesmanFullName
    salesmanContact
    contactPersonImage
    adId
    
    geometricLocation ${GeometricLocation}
    rating
    userCount
    
    userStatus
    accountStatus
    adStatus

    images
    
    reviews ${RatingReview}
    
    provider ${LookUp}
    serviceProvide ${TypeCrud}

    user ${AdminUser}

    createdAt
    
  }`;

export const RentType = `{
    id
    name
    icon
    tripId {
        id
        alias
    }
    createdAt
    updatedAt
}`;

export const BoatRent = `{
    address
    placeName
    postalCode
    route
    state
    _id
    tripAddress
    tripCity
    tripCountry
    tripPlaceName
    tripPostalCode
    tripRoute
    tripState
    
    city
    country
    id
    boatName
    captainName
    tripDuration
    whatToBring
    aboutUs
    descriptionOfOurTrip
    images
    youtubeLinkVideo
    boatLayout
    model
    manufacturer
    yearOfManufacturer
    engine
    policy
    adId
    
    geometricLocation ${GeometricLocation}
    rating
    userCount
    tripLatitude
    tripLongitude
    boatLength
    bedroomAndCabins
    maximumGuest
    bathrooms
    seatsOnBoat
    price
    noOfUnit
    
    adStatus
    boatRentStatus 

    reviews ${RatingReview}
    
    deposit ${LookUp}
    trip ${LookUp}

    tripType ${RentType}
    deckAndEntertainment ${LookUp}
    
    user ${AdminUser}
    
    startTime
    endTime
    createdAt
}`;

export const BoatShow = `{
    address
    placeName
    postalCode
    route
    state

    adId
    city
    country
    id
    title
    showLink
    showDescription
    showLogo
    
    geometricLocation ${GeometricLocation}
    
    user ${AdminUser}

    createdAt
    startDate
    endDate
    
}`;

export const Broker = `{
    id
    country
    companyName
    firstName
    lastName
    contactNumber
    email
    position
    companyLogo
    createdAt
    user {
        id
        role{
            id
        }
        firstName
        lastName
    }
}`;

export const RoleStatus = `{
    beforeStatus
    currentStatus
}`;

export const SalesEngineStatusTracking = `{
    buyer ${RoleStatus}
    seller ${RoleStatus}
    surveyor ${RoleStatus}
    shipper ${RoleStatus}
}`;

export const Branch = `{
    id
    address
    city
    country
    placeName
    route
    state
    postalCode
    officeLocation ${GeometricLocation}
    
    contactName
    mobileNumber
    emailAddress
    position
    
    branchStatus
    branchVerificationStatus
    
    user ${AdminUser}
    pricePerFt
    createdAt
}`;

export const SalesEngine = `{
    id
    boat ${Boat}
    
    buyer ${AdminUser}
    seller ${AdminUser}
    agent ${AdminUser}
    broker ${Broker}

    buySellProcess
    documentUploadedShippers

    requestedSurveyor
    approvedSurveyors
    surveyor ${Branch}

    shipperRequested {
        price
        proposalDocument
        shipper ${AdminUser}
    }
    shipper ${AdminUser}

    createdAt

    isSurveyorSkip
    isShipperSkip
    surveyorAccepted
    selectedShipper
    surveyorReport
    buyerAgreement
    sellerAgreement
    surveyorPayment
    shipperPayment
    auditorAccepted
    accountantAccepted

    surveyDocument
    agreementDocument

    negotiatedBoatPrice
    agreement {
        buyerId
        sellerId
    }
    shipmentLocation {
        address
        city
        country
        geometricLocation ${GeometricLocation}
        placeName
        postalCode
        route
        state
    }
    status ${SalesEngineStatusTracking}
}`;

export const creditPayment = `{
    cardHolderName
    payWith
    expirationMonth
    expirationYear
    cardNumber
    securityCode
    type
    user {
      firstName
      lastName
    }
}`;

export const Surveyor = `{
    address
    city
    country
    placeName
    postalCode
    route
    state
    id
    contactName
    emailAddress
    position
    distance
    
    distanceCalculates
    pricePerFt

    branchStatus
    branchVerificationStatus
    
    mobileNumber

    geometricLocation ${GeometricLocation}
    officeLocation ${GeometricLocation}
    
    user ${AdminUser}
    createdAt
}`;

export const SalesEngineAgreement = `{
    id
    title
    content
    createdAt
}`;

export const SalesEngineAssignShipper = `{
    id
    shipper{
        address
        city
        country
        placeName
        postalCode
        firstName
        lastName
        mobileNumber
        address
    }
    selectedShipper
    status{
        buyer{
            beforeStatus
            currentStatus
        }
        seller{
            beforeStatus
            currentStatus
        }
        surveyor{
            beforeStatus
            currentStatus
        }
        shipper{
            beforeStatus
            currentStatus
        }   
    }
}`;

export const SurveyorReport = `{
    id
    comment
    report
    boatVerifications
    otherDocument
    video
    images
    salesEngineId ${SalesEngine}
    surveyorId ${AdminUser}
}`;
