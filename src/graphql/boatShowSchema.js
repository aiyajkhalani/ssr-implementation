import gql from "graphql-tag";
import { BoatShow, GeometricLocation, AdminUser } from "./schemaTypes/schemaTypes";

export const createBoatShow = gql`
  mutation createBoatShow($input: BoatShowInput!) {
    createBoatShow(input: $input) {
      address
      placeName
      postalCode
      route
      state
      adId
      city
      country
      id
      title
      showLink
      showDescription
      showLogo
      status
      geometricLocation ${GeometricLocation}
      createdAt
      startDate
      endDate
    }
  }
`;

export const getUserBoatShows = gql`
  query getAllBoatShowByUser($page: Int, $limit: Int) {
    getAllBoatShowByUser(page: $page, limit: $limit) {
      items ${BoatShow}
      total
    }
  }
`;

export const getAllBoatShows = gql`
  query getAllBoatShow($page: Int!, $limit: Int!, $country: String!) {
    getAllBoatShow(page: $page, limit: $limit, country: $country) {
      items {
        id
        geometricLocation ${GeometricLocation}
        country
        city
        createdAt
        title
        startDate
        endDate
        showLink
        showDescription
        showLogo
        user ${AdminUser}
      }
      total
    }
  }
`;


export const searchBoatShow = gql`
query searchBoatShow($city: String!, $country: String!, $query: String!) {
  searchBoatShow(city: $city, country: $country, query: $query) {
    items {
        id
        address
        country
        city
        geometricLocation ${GeometricLocation}
        createdAt
        title
        startDate
        endDate
        showLink
        showDescription
        user { 
          id
        }
        showLogo
    }
    total
  }
}`;

export const updateBoatShow = gql`
  mutation updateBoatShow($input: BoatShowInput!) {
    updateBoatShow(input: $input) {
      address
      placeName
      postalCode
      route
      state
      adId
      city
      country
      id
      title
      showLink
      showDescription
      showLogo
      status
      geometricLocation ${GeometricLocation}
      createdAt
      startDate
      endDate
    }
  }
`;

export const getBoatShowById = gql`
  query editBoatShow($id: String!) {
    editBoatShow(id: $id) 
      ${BoatShow}
  }
`;

export const deleteBoatShow = gql`
  mutation deleteBoatShow($id: String!) {
    deleteBoatShow(id: $id) {
      id
    }
  }
`;
