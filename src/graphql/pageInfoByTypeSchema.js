import { gql } from "apollo-boost";

export const getAllPageInformationByType = gql`
  query getPagesByTitle($title: String!) {
    getPagesByTitle(title: $title) {
      id
      title
      pageContent
      titleSlug
    }
  }
`;
