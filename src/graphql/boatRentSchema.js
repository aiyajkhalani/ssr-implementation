import gql from "graphql-tag";
import { GeometricLocation, RatingReview, RentType, SellerUser, LookUp, BoatRent, AdminUser, Role } from "./schemaTypes/schemaTypes";

export const createRentBoat = gql`
  mutation createRentBoat($input: BoatRentInput!) {
    createRentBoat(input: $input) {
      id
      adId
      address
      city
      country
      geometricLocation ${GeometricLocation}
      placeName
      postalCode
      route
      state
      noOfUnit
      tripAddress
      tripCity
      tripCountry
      tripLatitude
      tripLongitude
      tripPlaceName
      tripPostalCode
      tripRoute
      tripState
      boatName
      captainName
      tripDuration
      startTime
      endTime
      boatLength
      bedroomAndCabins
      maximumGuest
      bathrooms
      seatsOnBoat
      whatToBring
      aboutUs
      price
      descriptionOfOurTrip
      policy
      images
      youtubeLinkVideo
      boatLayout
    }
  }
`;

export const getBoatRentLookUps = gql`
  {
    getBoatRentLookUps {
      typeName
      lookUp {
        id
        alias
        typeId {
          id
          type
        }
      }
    }
  }
`;

export const getAllRentTypes = gql`
  query getAllRentTypes($page: Int, $limit: Int, $isAdmin: Boolean = false){
    getAllRentTypes(page: $page, limit: $limit, isAdmin: $isAdmin) {
      items {
        id
        name
        icon
        tripId{
          id
          alias
        }
        createdAt
        updatedAt
      }
      total
    }
  }`

export const getUserBoatRent = gql`
  query rentBoatByUser($page: Int!, $limit: Int!) {
    rentBoatByUser(page: $page, limit: $limit) {
      items {
        id
        adId
        address
        city
        country
        geometricLocation ${GeometricLocation}
        placeName
        postalCode
        route
        state
        tripAddress
        tripCity
        tripCountry
        tripLatitude
        tripLongitude
        noOfUnit
        tripPlaceName
        tripPostalCode
        tripRoute
        tripState
        boatName
        captainName
        trip {
          id
          alias
        }
        tripType ${RentType}
        tripDuration
        startTime
        startDate
        endDate
        endTime
        engine
        boatLength
        bedroomAndCabins
        yearOfManufacturer
        maximumGuest
        manufacturer
        model
        bathrooms
        seatsOnBoat
        deckAndEntertainment {
          id
          alias
        }
        durationUnit
        whatToBring
        aboutUs
        price
        descriptionOfOurTrip
        policy
        deposit {
          id
          alias
        }
        images
        youtubeLinkVideo
        boatLayout
        rating
        user {
          id
          firstName
          lastName
          role {
            id
            role
          }
        }
      }
      total
    }
  }
`;

export const deleteBoatRent = gql`
  mutation deleteRentBoat($id: String!) {
    deleteRentBoat(id: $id) {
      id
      adId
      address
      city
      country
      geometricLocation ${GeometricLocation}
      placeName
      postalCode
      route
      state
      tripAddress
      tripCity
      tripCountry
      tripLatitude
      tripLongitude
      tripPlaceName
      tripPostalCode
      tripRoute
      tripState
      boatName
      captainName
      tripDuration
      startTime
      endTime
      boatLength
      bedroomAndCabins
      maximumGuest
      bathrooms
      seatsOnBoat
      whatToBring
      aboutUs
      price
      descriptionOfOurTrip
      policy
      images
      youtubeLinkVideo
      boatLayout
      rating
    }
  }
`;
export const rentCategoryWiseBoats = gql`
  query getRentBoatsByTripType(
    $page: Int
    $limit: Int
    $tripTypeId: String!
    $country: String!
  ) {
    getRentBoatsByTripType(
      page: $page
      limit: $limit
      tripTypeId: $tripTypeId
      country: $country
    ) {
      items {
        id
        adId
        address
        city
        country
        geometricLocation ${GeometricLocation}
        placeName
        postalCode
        route
        state
        tripAddress
        tripCity
        tripCountry
        tripLatitude
        tripLongitude
        noOfUnit
        tripPlaceName
        tripPostalCode
        tripRoute
        tripState
        boatName
        captainName
        trip {
          id
          alias
        }
        tripType ${RentType}
        tripDuration
        startTime
        startDate
        endDate
        endTime
        engine
        boatLength
        bedroomAndCabins
        yearOfManufacturer
        maximumGuest
        manufacturer
        model
        bathrooms
        seatsOnBoat
        deckAndEntertainment {
          id
          alias
        }
        durationUnit
        whatToBring
        aboutUs
        price
        descriptionOfOurTrip
        policy
        deposit {
          id
          alias
        }
        images
        youtubeLinkVideo
        boatLayout
        rating
        user {
          id
          firstName
          lastName
          role {
            id
            role
          }
        }
      }
      total
    }
  }
`;

export const getRentBoatInnerQuery = gql`
  query editRentBoat($id: String!) {
    editRentBoat(id: $id) {
      id
      adStatus
      rating
      userCount
      address
      city
      country
      geometricLocation ${GeometricLocation}
      placeName
      postalCode
      route
      state
      boatName
      captainName
      trip{
        id
        alias
      }
      deckAndEntertainment{
        id
        alias
      }
      tripAddress
      tripCity
      tripCountry
      tripLatitude
      tripLongitude
      tripPlaceName
      tripPostalCode
      tripRoute
      tripState
      tripDuration
      startTime
      endTime
      boatLength
      bedroomAndCabins
      maximumGuest
      bathrooms
      seatsOnBoat
      whatToBring
      aboutUs
      price
      descriptionOfOurTrip
      deposit{
        id 
        alias
      }
      images
      youtubeLinkVideo
      boatLayout
      boatRentStatus
      noOfUnit
      model
      manufacturer
      yearOfManufacturer
      engine
      policy
      user {
        createdAt
        address
        city
        country
        placeName
        postalCode
        route
        state
        like
        title
        firstName
        middleName
        lastName
        secondContactFullName
        secondContactEmail
        companyWebsite
        mobileNumber
        address1
        address2
        street
        zip
        postBox
        preference
        language
        companyName
        holderName
        bankName
        branchName
        branchAddress
        addressOfAccountHolder
        accountNumber
        transitCode
        instituteNumber
        companyLogo
        incorporationCertificate
        businessRegistrationNumber
        swiftCode
        token
        id
        email
        language
        geometricLocation ${GeometricLocation}
    
        isActivated
        accountActivated
        
        provider ${LookUp}
        
        role ${Role} 
        image
        governmentId
        commercialLicence
        documentVerification {
            commercialLicenceVerified
            emailVerified
            mobileVerified
            governmentIdVerified
        }
        reviews{
            reviews{
                userId ${AdminUser}
                rating
                reviews
                createdAt
            }
            ratings{
                averageRating
                count
            }
        }
        relatedItems {
            id
            boatName
            rating
            placeName
            manufacturedBy
            description
            price
            video
            state
            address
            country
            city
            images
            yearBuilt
            lengthInFt
            geometricLocation ${GeometricLocation}
        }
    }
      tripType ${RentType}
      adId
      reviews ${RatingReview}
      createdAt
      startDate
      endDate
    }
  }
`;

export const increaseBoatRentViewCount = gql`
  mutation boatRentIncreaseCount($boatRentId: String!) {
    boatRentIncreaseCount(boatRentId: $boatRentId) {
        id
        message
        statusCode
    }
  }
`
export const updateRentBoat = gql`
  mutation updateRentBoat($input: BoatRentInput!) {
    updateRentBoat(input: $input) {
      id
      adId
      address
      city
      country
      geometricLocation ${GeometricLocation}
      placeName
      postalCode
      route
      state
      tripAddress
      tripCity
      tripCountry
      tripLatitude
      tripLongitude
      tripPlaceName
      tripPostalCode
      tripRoute
      tripState
      boatName
      captainName
      tripDuration
      startTime
      endTime
      boatLength
      bedroomAndCabins
      maximumGuest
      bathrooms
      seatsOnBoat
      whatToBring
      noOfUnit
      aboutUs
      price
      descriptionOfOurTrip
      policy
      images
      youtubeLinkVideo
      boatLayout
    }
  }
`;

export const getSimilarBoatRents = gql`
  query getSimilarBoatRents($id: String!){
    getSimilarBoatRents(id: $id){
      items ${BoatRent}
      total
    }
  }`