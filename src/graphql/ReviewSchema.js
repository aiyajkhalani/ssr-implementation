import gql from "graphql-tag";

export const createReview = gql`
  mutation createReview($input: ReviewInput!) {
    createReview(input: $input) {
      id
      moduleId
      rating
      reviews
    }
  }
`;

export const getReviewsByModuleId = gql`
    query getReviewsByModuleId($moduleId: String!){
    getReviewsByModuleId(moduleId: $moduleId){
        reviews{
            moduleId
            id
            userId{
                id
                like
                firstName
                middleName
                lastName
                mobileNumber
                country
                address1
                address2
                street
                city
                state
                zip
                postBox
                companyName
                holderName
                bankName
                branchName
                branchAddress
                addressOfAccountHolder
                accountNumber
                transitCode
                instituteNumber
                file
                swiftCode
                token
                isActivated
                accountActivated
                email
            }
            rating
            reviews
            createdAt
        }
        ratings
    }}`;
