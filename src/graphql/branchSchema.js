import { gql } from "apollo-boost";
import { GeometricLocation } from "./schemaTypes/schemaTypes";

const Branch = `{
    id
    address
    city
    country
    placeName
    route
    state
    postalCode
    officeLocation ${GeometricLocation}
    
    contactName
    mobileNumber
    emailAddress
    position
    pricePerFt

    branchStatus
    branchVerificationStatus

    user {
      firstName
      lastName
      email
    }
    createdAt
}`

export const createBranch = gql`
  mutation createBranch($input: SurveyorInput!) {
    createBranch(input: $input) {
        id
  }
}`

export const editBranch = gql`
  query editBranch($id: String!) {
    editBranch(id: $id) ${Branch}
}`

export const updateBranch = gql`
  mutation updateBranch($input: SurveyorInput!) {
    updateBranch(input: $input) {
        id
  }
}`

export const deleteBranch = gql`
  mutation deleteBranch($id: String!) {
    deleteBranch(id: $id) {
        id
  }
}
`

export const changeBranchStatus = gql`
  mutation changeBranchStatus($id: String!, $value: Boolean!, $columnName: String!) {
    changeBranchStatus(id: $id, value: $value, columnName: $columnName) ${Branch}
}
`

export const getAllBranch = gql`
{
  branchesByUser{
    items ${Branch}
    total
  }
}
`