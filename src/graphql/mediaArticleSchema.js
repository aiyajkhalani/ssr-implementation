import gql from "graphql-tag";
import { AdminUser } from "./schemaTypes/schemaTypes";

export const getAllMediaArticles = gql`
query getAllArticles($page: Int, $limit: Int, $isApproved: Boolean = false) {
    getAllArticles(page: $page, limit: $limit, isApproved: $isApproved) {
    items {      
      id
      user {
        city
        country
        firstName
        middleName
        lastName
        id
        image
      }
      title
      titleSlug
      description
      file
      articleViewCount
      articleApproved
      status
      adId
      createdAt
      updatedAt
    }
    total
  }
}
`;
