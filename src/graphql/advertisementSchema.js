import gql from "graphql-tag";

export const categoryWiseAd = gql`
  query categoryWiseAd($category: String!) {
    categoryWiseAd(category: $category) {
      id
      title
      description
      link
      linkTitle
      imageUrl
    }
  }
`;
