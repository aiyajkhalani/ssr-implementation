import { getGeoLocation } from "./geoLocation";

export const getMarinaStorageMarker = marinas => {
  if (marinas && marinas.length) {
    return marinas.map(marina => {
      return {
        image: marina.images && marina.images.length && marina.images[0],
        country: marina.country,
        city: marina.city,
        rating: marina.rating,
        title: marina.FOS,
        icon: {
          url: require("../assets/images/marker/place (1).png"),
        },
        position: {
          lat: marina.geometricLocation.coordinates[1], lng: marina.geometricLocation.coordinates[0]
        }
      };
    });
  }
};

export const getSingleMarinaMarker = (marina) => {
  if (marina  && marina.geometricLocation && marina.geometricLocation.coordinates) {
    const marker = {
      image: null,
      country: marina.country,
      city: marina.city,
      rating: marina.rating,
      title: marina.FOS,
      icon: {
        url: require("../assets/images/marker/place (1).png"),
      },
      position: {
        lat: marina.geometricLocation.coordinates[1], lng: marina.geometricLocation.coordinates[0]
      }
    }
    return [marker]
  }
}

export const getSingleServiceMarker = (marina) => {
  if (marina  && marina.geometricLocation && marina.geometricLocation.coordinates) {
    const marker = {
      image: null,
      country: marina.country,
      city: marina.city,
      rating: marina.rating,
      title: marina.FOS,
      icon: {
        url: require("../assets/images/marker/place (2).png"),
      },
      position: {
        lat: marina.geometricLocation.coordinates[1], lng: marina.geometricLocation.coordinates[0]
      }
    }
    return [marker]
  }
}



export const getMarinaType = data => {
  switch (data.alias) {
    case "Marina Provide":
      return data.id;

    case "Boat Storage Provide":
      return data.id;

    case "Both":
      return data.id;

    default:
      break;
  }
};

export const getLocation = async () => {
  await getGeoLocation(fetchCountry, true);
};

export const fetchCountry = response => {
  const { country } = response;
  return country
};
