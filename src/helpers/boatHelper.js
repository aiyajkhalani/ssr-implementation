import { readableString } from "./string";
import { userRoles } from "../util/enums/enums";
import { InfoNotify } from "./notification";

export const viewBoatHandler = (boat) => {
    if (boat) {
        const { id, _original, yearBuilt, boatType: { name }, manufacturedBy, engineHp } = boat
        const userId = id || _original.id
        const boatName = `${yearBuilt}-${name}-${manufacturedBy}-${engineHp}`
        if (userId) {
            window && window.open(`/boat-inner/${userId}/${readableString(boatName)}`, '_blank')
        }
    }
}

export const getSingleBoatMarker = (boat) => {
    if (boat && boat.boatName && boat.geometricLocation && boat.geometricLocation.coordinates) {
        const marker = {
            image: null,
            country: boat.country,
            city: boat.city,
            rating: boat.rating,
            price: boat.price,
            title: boat.boatName,
            icon: {
                url: require("../assets/images/marker/place (3).png"),
            },
            position: {
                lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
            }
        }
        return [marker]
    }
}


export const getRentMarker = (boat) => {
    if (boat && boat.boatName && boat.geometricLocation && boat.geometricLocation.coordinates) {
        const marker = {
            image: null,
            country: boat.country,
            city: boat.city,
            rating: boat.rating,
            price: boat.price,
            title: boat.boatName,
            icon: {
                url: require("../assets/images/marker/place.png"),
            },
            position: {
                lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
            }
        }
        return [marker]
    }
}

export const getBoatMarkers = (boats) => {
    if (boats && boats.length) {
        return boats.map(boat => {
            if (boat.geometricLocation && boat.boatName) {
                return {
                    image: boat.images && boat.images.length && boat.images[0],
                    country: boat.country,
                    city: boat.city,
                    rating: boat.rating,
                    price: boat.price,
                    title: boat.boatName,
                    icon: {
                        url: require("../assets/images/marker/place (3).png"),
                    },
                    position: {
                        lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
                    }
                }
            }
        })
    }
}

export const getSurveyorMarkersWithBoat = (branches, boat) => {
    let branchesMarker, boatMarker
    if (branches && branches.length) {
        branchesMarker = branches.map(branch => {
            if (branch.officeLocation) {
                return {
                    image: null,
                    country: branch.country,
                    city: branch.city,
                    rating: null,
                    price: branch.pricePerFt,
                    title: branch.contactName,
                    icon: {
                        url: require("../assets/images/marker/place (3).png"),
                    },
                    position: {
                        lat: branch.officeLocation.coordinates[1], lng: branch.officeLocation.coordinates[0]
                    }
                }
            }
        })
    }
    if (boat && boat.boatName && boat.geometricLocation && boat.geometricLocation.coordinates) {
        boatMarker = {
            image: null,
            country: boat.country,
            city: boat.city,
            rating: boat.rating,
            price: boat.price,
            title: boat.boatName,
            icon: {
                url: require("../assets/images/marker/place.png"),
            },
            position: {
                lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
            }
        }
    }

    return [...branchesMarker, boatMarker]
}

export const getRentShowMarkers = (boats) => {
    if (boats && boats.length) {
        return boats.map(boat => {
            if (boat.geometricLocation && boat.boatName) {
                return {
                    image: boat.images && boat.images.length && boat.images[0],
                    country: boat.country,
                    city: boat.city,
                    rating: boat.rating,
                    price: boat.price,
                    title: boat.boatName,
                    icon: {
                        url: require("../assets/images/marker/place.png"),
                    },
                    position: {
                        lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
                    }
                }
            }
        })
    }
}

export const getBoatShowMarker = boatShows => {
    if (boatShows && boatShows.length) {
        return boatShows.map(boatShow => {
            return {
                image: boatShow.showLogo,
                country: boatShow.country,
                city: boatShow.city,
                title: boatShow.title,
                icon: {
                    url: require("../assets/images/marker/place (2).png"),
                },
                position: {
                    lat: boatShow.geometricLocation.coordinates[1], lng: boatShow.geometricLocation.coordinates[0]
                }
            };
        });
    }
};

export const getBoatServiceMarkers = (boats) => {
    if (boats && boats.length) {
        return boats.map(boat => {
            if (boat.geometricLocation) {
                return {
                    image: boat.images && boat.images.length && boat.images[0],
                    country: boat.country,
                    city: boat.city,
                    rating: boat.rating,
                    title: boat.service,
                    icon: {
                        url: require("../assets/images/marker/place (2).png"),
                    },
                    position: {
                        lat: boat.geometricLocation.coordinates[1], lng: boat.geometricLocation.coordinates[0]
                    }
                }
            }
        })
    }
}

export const viewRentBoatHandler = (boat) => {
    if (boat) {
        const { id, boatName, _original, name } = boat
        const userId = id || _original.id
        const boatFieldName = boatName ? boatName : name
        if (userId) {
            window && window.open(`/rent-inner/${userId}/${readableString(boatFieldName)}`, '_blank')

        }
    }
}

export const viewServiceBoatHandler = (id) => {

    id && window && window.open(`/boat-service-inner/${id}`, '_blank')
}

export const viewMarinaDetails = (id) => {

    id && window && window.open(`/marina-storage-inner/${id}`, '_blank')
};

export const isBuyerEligible = (buyer) => {

    let isBuyer, isVerified = false

    if (buyer) {
        if (buyer.role && buyer.role.aliasName) {
            isBuyer = (buyer.role.aliasName === userRoles.MEMBER)

            !isBuyer && InfoNotify("Only buyer will able to buy a boat.")

            if (isBuyer) {
                isVerified = (buyer.isActivated && buyer.accountActivated)

                !isVerified && InfoNotify("You are either not verified or your account not activated")
            }
        }
    } else {
        InfoNotify("Please login as member to buy a boat.")
    }

    return isBuyer && isVerified
}