import S3 from 'aws-s3';

const config = {
    bucketName: process.env.REACT_APP_BUCKET_NAME,
    dirName: process.env.REACT_APP_DIR_NAME,
    region: process.env.REACT_APP_REGION,
    accessKeyId: process.env.REACT_APP_ACCESS_KEY_ID,
    secretAccessKey: process.env.REACT_APP_SECRET_ACCESS_KEY,
}

const S3Client = new S3(config)
const S3ClientVideo = new S3({...config, dirName: process.env.REACT_APP_DIR_NAME_VIDEO, })


export const uploadToS3 = (selectedFile) => {

    return S3Client
        .uploadFile(selectedFile, selectedFile.name)
        .then((data) => data)
        .catch((err) => err)
}

export function uploadToS3Video(selectedFile) {

    return S3ClientVideo
        .uploadFile(selectedFile)
        .then((data) => data)
        .catch((err) => err)
}


export const deleteFromS3 = (selectedFile) => {

    return S3Client
        .deleteFile(selectedFile)
        .then(response => response)
        .catch(err => err)
}

export const handleSingleFileDelete = async (value, name, setValue) => {
    value && setValue(name, "")
}

export const handleSingleFileUpload = async (file, name, setValue) => {
    if (file.length) {
        let res = await uploadToS3(file[file.length - 1])
        
        if (res && res.location) {
            setValue(name, res.location)
        }
    }
}