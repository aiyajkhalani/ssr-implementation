import { toast } from 'react-toastify'

export const SuccessNotify = (message) => {
    toast.success(message)
}

export const ErrorNotify = (message) => {
        toast.error(message)
}

export const InfoNotify = (message) => {
    toast.info(message)
}

export const WarnNotify = (message) => {
    toast.warn(message)
}
