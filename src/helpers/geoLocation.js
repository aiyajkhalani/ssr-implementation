import { SuccessNotify, ErrorNotify } from './notification';
import { toast } from 'react-toastify'


const showError = (error) => {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            // ErrorNotify("User denied the request for Geolocation.")
            // ErrorNotify("Location Denied")
            toast.error("Location Denied", {               
                className: 'location-popup',
              });
            break;
        case error.POSITION_UNAVAILABLE:
            ErrorNotify("Location information is unavailable.")
            break;
        case error.TIMEOUT:
            ErrorNotify("The request to get user location timed out.")
            break;
        case error.UNKNOWN_ERROR:
            ErrorNotify("Geolocation failed due to unknown error.")
            break;

        default:
            break;
    }
}

export const getGeoLocation = (getValue, fetchCountry = false) => {
    if (!navigator.geolocation) {
        ErrorNotify('Geolocation is not supported by your browser')
    } else {
        navigator.geolocation.getCurrentPosition(async (position) => await showPosition(position, getValue, fetchCountry), showError)
    }

}

const showPosition = (position, getValue, fetchCountry) => {
    const latitude = position.coords.latitude
    const longitude = position.coords.longitude
    let country

    if (fetchCountry) {

        return new Promise((resolve, reject) => {
            return fetch(`https://maps.googleapis.com/maps/api/geocode/json?key=${process.env.REACT_APP_MAP_API_KEY}&latlng=${latitude},${longitude}`)
                .then(res => res.json())
                .then(data => {
                    const address_components = data.results[0].address_components
                    address_components.map(component => {
                        const addressType = component.types[0]

                        if (addressType === 'country') {
                            country = component.long_name

                            // SuccessNotify(`Your country ${country} is attached.`)
                            resolve(getValue({ lat: latitude, lng: longitude, country }))
                        }
                    })
                })
        })
    }

    return getValue({ lat: latitude, lng: longitude })
}

export const getPlaceInformation = (place) => {

    let result = {}

    result.placeName = place.name
    result.address = place.formatted_address
    result.latitude = place.geometry.location.lat()
    result.longitude = place.geometry.location.lng()

    if (place && place.address_components && place.address_components.length) {

        place.address_components.map(component => {

            const addressType = component.types[0]

            switch (addressType) {
                case 'street_number':
                    result.streetNumber = component.long_name
                    break;
                case 'route':
                    result.route = component.short_name
                    break;
                case 'locality':
                    result.city = component.long_name
                    break;
                case 'administrative_area_level_1':
                    result.state = component.long_name
                    break;
                case 'country':
                    result.country = component.long_name
                    break;
                case 'postal_code':
                    result.postalCode = component.long_name
                    break;

                default:
                    break;
            }
        })
    }

    return { ...result }
}
