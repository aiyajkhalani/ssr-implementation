import randomize from "randomatic";
import moment from "moment";
import { stepOneStatus, stepTwoStatus, stepThreeStatus, stepFourStatus, stepFiveStatus } from "../util/enums/enums";

export const lowerHypenCase = name => {
  if (name) {
    return name
      .toLowerCase()
      .split(" ")
      .join("-");
  }
};

export const snakeCase = name => {
  if (name) {
    return name
      .toLowerCase()
      .split(" ")
      .join("_");
  }
};

export const readableString = name => {
  if (name) {
    return name.replace(/\s+$/, '')
      .toLowerCase()
      .split(" ")
      .map(w => w[0] && w[0].toUpperCase() + w.substr(1).toLowerCase())
      .join("-");
  }
};

export const TitleCase = name => {
  return (
    name &&
    name
      .toLowerCase()
      .split(" ")
      .map(w => w[0].toUpperCase() + w.slice(1))
      .join(" ")
  );
};

export const verifiedCheck = status => {
  return status ? "Verified" : "Unverified";
};


export const randomAdId = (prefix = "") => {
  return prefix + randomize("A0", 16 - prefix.length);
};

export const requireMessage = (label = "Field") => {
  return `${label} is required.`
};

export const displayDefaultValue = (value) => {
  return value || '-'
}

export const displayDefaultNumericValue = (value) => {
  return value || 0
}

export const displayDefaultReview = (value) => {
  return value || {
    averageRating: 0,
    count: 0
  }
}

export const displayDefaultImage = (value) => {
  return value || require('../assets/images/default/no_image_png_935205.png')
}

export const formattedDate = (date) => {
  return date ? moment(date).format("Do MMMM, YYYY") : null
}

export const sliderImageHelper = (images = []) => {
  return images.map(item => {
    return {
      original: item,
      thumbnail: item,
    }
  })
}

export const renderSelectOptions = (data = [], labelKey, valueKey, isLookUp = false) => {
  return data && data.length > 0 && data.map(item => {
    return isLookUp ? { label: item.lookUp[labelKey], value: item.lookUp[valueKey] } : { label: item[labelKey], value: item[valueKey] }
  })
}

export const getIds = (data = []) => {
  return data && data.length > 0 ? data.map(item => item.value) : []
}

export const salesEngineStatusCheck = (salesEngine, skipSurveyor) => {

  const currentStatus = salesEngine && salesEngine.buySellProcess[salesEngine.buySellProcess.length - 1]

  const stepsArray = skipSurveyor ?
    [
      stepOneStatus,
      stepFourStatus,
      stepFiveStatus,
    ] : [
      stepOneStatus,
      stepTwoStatus,
      stepThreeStatus,
      stepFourStatus,
      stepFiveStatus,
    ]

  return currentStatus && stepsArray.reduce((accum, step, index) => {
    return step.includes(currentStatus) ? index + 1 : accum
  }, 1)
}