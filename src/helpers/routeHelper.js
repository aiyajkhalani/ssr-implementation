export const cancelHandler = (history) => {
    history.goBack()
};

export const objectToQuerystring = (obj) => {

    return Object.keys(obj).reduce((str, key, i) => {
        let delimiter, val

        delimiter = (i === 0) ? '?' : '&'

        key = encodeURIComponent(key)
        val = obj[key] && encodeURIComponent(obj[key])

        return [str, delimiter, key, '=', val].join('')
    }, '')
}