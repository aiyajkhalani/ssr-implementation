import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import { Card, Grid, Button } from "@material-ui/core";
import * as Yup from "yup";

export const confirmSubmitHandler = (
  fn,
  value,
  message = "Are you sure to perform this action?"
) => {
  confirmAlert({
    title: "Alert!",
    message: message,
    buttons: [
      {
        label: "Yes",
        onClick: () => fn(value)
      },
      {
        label: "No",
        onClick: () => null
      }
    ]
  });
};

export const bidPopup = (fn,auctionRoomId) => {
  const initValues = {
    price: ""
  };
  confirmAlert({
    customUI: ({ onClose }) => {
      return (
        <div className="auction-custom-ui w-500">
          <Formik
            initialValues={initValues}
            onSubmit={values => {
              values.auctionRoom = auctionRoomId
              fn(values);
              onClose();
            }}
            validationSchema={Yup.object().shape({
              price: Yup.string().required("Price field is required.")
            })}
            render={({ setFieldValue, values, handleSubmit }) => (
              <Form>
                <Card className="map-rent p-4">
                  <h3>Bid Details</h3>
                  <Grid container>
                    <Grid item sm={12} className="mb-3">
                      <label className="mb-1 font-16 font-weight-500">Price :</label>
                      <input
                        name="price"
                        value={values.price}
                        type="number"
                        className="form-control"
                        onChange={e => setFieldValue("price", +e.target.value)}
                      ></input>
                      <ErrorMessage
                        className="invalid-feedback ant-typography-danger d-block"
                        name={`price`}
                        component="span"
                      />
                    </Grid>

                    <Grid item sm={12}>

                      <div className="d-flex justify-content-end">
                        <Button
                          type="button"
                          className="button btn btn-primary w-auto addBoatService-btn primary-button"
                          onClick={handleSubmit}>
                          Save
                        </Button>
                        <Button
                          type="button"
                          className="button btn profile-button btn-dark w-auto"
                          onClick={onClose}
                          >
                          Cancel
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Card>
              </Form>
            )}
          />
        </div>
      );
    }
  });
};
