import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import { Card, Grid, Button } from "@material-ui/core";
import * as Yup from "yup";
import Datetime from "react-datetime";
import * as moment from "moment";
import { valid, dateStringFormate } from "../util/utilFunctions";
import { createAuctionRoom } from "../redux/actions";
import { requireMessage } from "./string";

export const confirmSubmitHandler = (
  fn,
  value,
  message = "Are you sure want to do this ?"
) => {
  confirmAlert({
    title: "Alert",
    message: message,
    buttons: [
      {
        label: "Yes",
        onClick: () => fn(value)
      },
      {
        label: "No",
        onClick: () => null
      }
    ]
  });
};

export const createAuctionRoomPopup = (fn, value) => {
  const initValues = {
    startTime: "",
    endTime: "",
    startPrice: "",
    minimumRaisedAmount: ""
  };
  confirmAlert({
    customUI: ({ onClose }) => {
      return (
        <div className="auction-custom-ui w-500">
          <Formik
            initialValues={initValues}
            onSubmit={values => {
              values.startPrice = parseInt(values.startPrice);
              values.minimumRaisedAmount = parseInt(values.minimumRaisedAmount);
              values.boat = value;
              fn(values);
              onClose();
            }}
            validationSchema={Yup.object().shape({
              startTime: Yup.date().required(requireMessage('Start Time')),
							endTime: Yup.date().min(Yup.ref('startTime'), 'End date must be later than start date.').required(requireMessage('End Time')),
              startPrice: Yup.string().required(
                "Start Price field is required."
              ),
              minimumRaisedAmount: Yup.string().required(
                "Amount field is required."
              )
            })}
            render={({
              props,
              errors,
              status,
              touched,
              setFieldValue,
              values,
              handleSubmit
            }) => (
                <Form>
                  <Card className="map-rent p-4">
                    <h3>Auction Details</h3>
                    <Grid container>
                      <Grid item sm={12} className="mb-2">
                        <label className="mb-1 font-16 font-weight-500">Start Time</label>
                        <Datetime
                          name="startTime"
                          isValidDate={valid}
                          value={moment(values.startTime)}
                          closeOnSelect
                          onChange={value => {
                            setFieldValue("startTime", dateStringFormate(value));
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="startTime"
                          className="error-message"
                        />
                      </Grid>
                      <Grid item sm={12} className="mb-2">
                        <label className="mb-1 font-16 font-weight-500">End Time</label>
                        <Datetime
                          name="startTime"
                          isValidDate={valid}
                          value={moment(values.endTime)}
                          closeOnSelect
                          onChange={value => {
                            setFieldValue("endTime", dateStringFormate(value));
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="endTime"
                          className="error-message"
                        />
                      </Grid>
                      <Grid item sm={12} className="mb-2">
                        <label className="mb-1 font-16 font-weight-500">Price</label>
                        <input
                          name="startPrice"
                          value={values.startPrice}
                          type="number"
                          className="form-control"
                          onChange={e =>
                            setFieldValue("startPrice", e.target.value)
                          }
                        ></input>
                        <ErrorMessage
                          component="div"
                          name="startPrice"
                          className="error-message"
                        />
                      </Grid>
                      <Grid item sm={12} className="mb-3">
                        <label className="mb-1 font-16 font-weight-500">Minimum Amount Raised</label>
                        <input
                          name="minimumRaisedAmount"
                          value={values.minimumRaisedAmount}
                          type="number"
                          className="form-control"
                          onChange={e =>
                            setFieldValue("minimumRaisedAmount", e.target.value)
                          }
                        ></input>
                        <ErrorMessage
                          component="div"
                          name="minimumRaisedAmount"
                          className="error-message"
                        />
                      </Grid>

                      <Grid item sm={12}>
                        {/* <button onClick={onClose}>Cancel</button>
                        <button type="button" onClick={handleSubmit}>
                          Save
                      </button> */}

                        <div className="d-flex justify-content-end">
                          <Button
                            type="button"
                            className="button btn btn-primary w-auto addBoatService-btn primary-button"
                            onClick={handleSubmit}
                          >
                            Save
                        </Button>
                          <Button
                            type="button"
                            className="button btn profile-button btn-dark w-auto"
                            onClick={onClose}
                          >
                            Cancel
                        </Button>
                        </div>
                      </Grid>
                    </Grid>
                  </Card>
                </Form>
              )}
          />
        </div>
      );
    }
  });
};
