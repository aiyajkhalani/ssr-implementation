import React from "react";
import { MenuItem } from "@material-ui/core";
import uuid from "uuid/v4";
import { AiOutlineClose } from "react-icons/ai";
import { IoMdCheckmark } from "react-icons/io";
import { userRoles, profileFields } from "../util/enums/enums";
import { percentageFormate } from "../util/utilFunctions";

export const renderMenuItems = (values = []) => {
  if (values && values.length) {
    return values.map(
      name =>
        name &&
        name.lookUp && (
          <option key={uuid()} value={name.lookUp.id}>
            {name.lookUp.alias}
          </option>
        )
    );
  }
};


export const renderMenuItemsRentCard = (values = []) => {
  if (values && values.length) {
    return values.map(
      name =>
        name && (
          <MenuItem key={uuid()} value={name.id}>
            {name.alias}
          </MenuItem>
        )
    );
  }
};

export const renderErrorMessage = (message, className, isError) => {
  return (
    <div className={className}>
      <span className="mr-2 register-password-hint-icon">
        {isError ? <AiOutlineClose /> : <IoMdCheckmark />}
      </span>
      <span className="register-password-hint-text">{message}</span>
    </div>
  );
}

export const renderBoatTypes = (boatTypes = []) => {
  if (boatTypes && boatTypes.length) {
    return boatTypes.map(
      type =>
        (
          <MenuItem key={uuid()} value={type.id}>
            {type.name}
          </MenuItem>
        )
    );
  }
};


export const getBoatPassengerText = (selectedTrip) => {
  if (selectedTrip) {
    switch (selectedTrip.lookUp.alias) {
      case "Private Trip":
        return "Boat capacity";
      case "Rent Per Hour":
        return "No of unit";
      case "Shared Trip":
        return "Passengers";
      default:
        return "Boat capacity";
    }
  }
}


export const getSelectedTrip = (tripList, boatType) => {
  return tripList && tripList.length ? tripList.find((tripType) => {
    return tripType.lookUp.id === boatType;
  }) : null;
}

export const renderRentBoatTypes = (boatRentTypes = [], selectedTrip) => {
  if (boatRentTypes && selectedTrip && boatRentTypes.length) {
    return boatRentTypes.map(
      (type) => {
        return type.tripId.map(tripId => {
          if (tripId.alias === selectedTrip.lookUp.alias) {
            return <MenuItem key={uuid()} value={type.id}>
              {type.name}
            </MenuItem>
          }
        });
      }
    );
  }
};

export const renderYachtServicesMenuItems = (values = []) => {
  if (values && values.length) {

    return values.map(item => {
      return (<MenuItem key={item.id} value={item.id}>
        {item.name}
      </MenuItem>)
    });
  }
};


// NOT Review (WIP) @ghanshyam
export const getHourFromDifferent = (endTime, startTime) => {
  let diff = (Date.parse(new Date(endTime)) - Date.parse(new Date(startTime))) / 1000;

  const timeLeft = {
    years: 0,
    days: 0,
    hours: 0,
    min: 0,
    sec: 0,
    millisec: 0,
  };

  // calculate time difference between now and expected date
  if (diff >= (365.25 * 86400)) { // 365.25 * 24 * 60 * 60
    timeLeft.years = Math.floor(diff / (365.25 * 86400));
    diff -= timeLeft.years * 365.25 * 86400;
  }
  if (diff >= 86400) { // 24 * 60 * 60
    timeLeft.days = Math.floor(diff / 86400);
    diff -= timeLeft.days * 86400;
  }
  if (diff >= 3600) { // 60 * 60
    timeLeft.hours = Math.floor(diff / 3600);
    diff -= timeLeft.hours * 3600;
  }
  if (diff >= 60) {
    timeLeft.min = Math.floor(diff / 60);
    diff -= timeLeft.min * 60;
  }
  timeLeft.sec = diff;
  return timeLeft;

  // return moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")
}

export const renderMarinaServicesMenuItems = (values = []) => {
  if (values && values.length) {
    return values.map(item =>
      <MenuItem key={item.id} value={item.id}>
        {item.name}
      </MenuItem>
    );
  }
};

export const getUserProfileCount = (user) => {
  const role = user.role.aliasName;

  switch (role) {

    case userRoles.MEMBER:
      const total = profileFields[role]
      return getUserProfilePercentage(user, total)

    case userRoles.BOAT_OWNER:
      const boatOwnerTotal = profileFields[role]
      return getUserProfilePercentage(user, boatOwnerTotal)

    case userRoles.BROKER_AND_DEALER:
      const brokerTotal = profileFields[role]
      return getUserProfilePercentage(user, brokerTotal)

    case userRoles.MARINA_AND_STORAGE:
      const marinaTotal = profileFields[role]
      return getUserProfilePercentage(user, marinaTotal)

    case userRoles.RENT_AND_CHARTER:
      const rentTotal = profileFields[role]
      return getUserProfilePercentage(user, rentTotal)

    case userRoles.BOAT_MANUFACTURER:
      const manufacturerTotal = profileFields[role]
      return getUserProfilePercentage(user, manufacturerTotal)

    case userRoles.YACHT_SHIPPER:
      const shipperTotal = profileFields[role]
      return getUserProfilePercentage(user, shipperTotal)

    case userRoles.SURVEYOR:
      const surveyorTotal = profileFields[role]
      return getUserProfilePercentage(user, surveyorTotal)

    case userRoles.SERVICE_AND_MAINTENANCE:
      const serviceTotal = profileFields[role]
      return getUserProfilePercentage(user, serviceTotal)

    case userRoles.AGENT:
      const agentTotal = profileFields[role]
      return getUserProfilePercentage(user, agentTotal)

    default:
      break;
  }
}

const countFields = (user) => {
  let count = 0;
  for (const field in user) {
    if (field === "firstName" && user[field] !== null && user[field] !== "") { count++ };
    if (field === "lastName" && user[field] !== null && user[field] !== "") { count++ };
    if (field === "email" && user[field] !== null && user[field] !== "") { count++ };
    if (field === "mobileNumber" && user[field] !== null && user[field] !== "") { count++ };
    if (field === "country" && user[field] !== null && user[field] !== "") { count++ };
    if (field === "governmentId" && user[field] !== null && user[field] !== "") { count++ };

    if (user.role.aliasName === userRoles.MEMBER ||
      user.role.aliasName === userRoles.BOAT_OWNER ||
      user.role.aliasName === userRoles.BROKER_AND_DEALER ||
      user.role.aliasName === userRoles.MARINA_AND_STORAGE ||
      user.role.aliasName === userRoles.RENT_AND_CHARTER ||
      user.role.aliasName === userRoles.YACHT_SHIPPER ||
      user.role.aliasName === userRoles.BOAT_MANUFACTURER ||
      user.role.aliasName === userRoles.SERVICE_AND_MAINTENANCE ||
      user.role.aliasName === userRoles.AGENT) {
      if (field === "address1" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "address2" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "street" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "city" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "zip" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "postBox" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "state" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "image" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.MEMBER) {
      if (field === "preference" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "language" && user[field] !== null && user[field] !== "") { count++ };
    }
    if (user.role.aliasName === userRoles.BOAT_OWNER) {
      if (field === "middleName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.BOAT_OWNER ||
      user.role.aliasName === userRoles.BROKER_AND_DEALER ||
      user.role.aliasName === userRoles.BOAT_MANUFACTURER ||
      user.role.aliasName === userRoles.YACHT_SHIPPER ||
      user.role.aliasName === userRoles.SURVEYOR) {
      if (field === "holderName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "bankName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "branchName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "branchAddress" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "addressOfAccountHolder" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "accountNumber" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "transitCode" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "instituteNumber" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.BROKER_AND_DEALER) {
      if (field === "companyName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.MARINA_AND_STORAGE ||
      user.role.aliasName === userRoles.BOAT_MANUFACTURER ||
      user.role.aliasName === userRoles.SURVEYOR) {
      if (field === "companyName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyWebsite" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyLogo" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.RENT_AND_CHARTER) {
      if (field === "companyName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyWebsite" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "provider" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.BOAT_MANUFACTURER) {
      if (field === "swiftCode" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.YACHT_SHIPPER) {
      if (field === "companyName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyLogo" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "swiftCode" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.SURVEYOR) {
      if (field === "officeAddress" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officeCity" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officeState" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officeCountry" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officeRoute" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officePlaceName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "officePostalCode" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "swiftCode" && user[field] !== null && user[field] !== "") { count++ };
    }

    if (user.role.aliasName === userRoles.SERVICE_AND_MAINTENANCE ||
      user.role.aliasName === userRoles.AGENT) {
      if (field === "companyName" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyLogo" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "companyWebsite" && user[field] !== null && user[field] !== "") { count++ };
      if (field === "commercialLicence" && user[field] !== null && user[field] !== "") { count++ };
    }

  }

  return count
}

export const getUserProfilePercentage = (user, total) => {
  const fields = countFields(user)
  const count = (fields * 100) / total
  return percentageFormate(count);
}

export const cityCountryNameFormatter = (city, country) => {
  return `${city && city}${(city && country) && `,`} ${country && `${country}`}`
}