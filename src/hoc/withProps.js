import React, { Component } from 'react';
import { connect } from 'react-redux';

import { UserProvider } from '../UserContext'
import { getGeoLocation } from '../helpers/geoLocation'
import publicIp from "public-ip";
import {
    globalBoatSearch,
    setCurrentLocation,
    getRecentSearch,
    multiSearch,
    clearMultiSearch,
    logout,
    changeUserStatus,
    getUserLocation
} from '../redux/actions'
import { getClientIp } from '../helpers/jsxHelper';

export default function (ComposedComponent) {
    class WithProps extends Component {

        constructor(props) {
            super(props)
            this.state = {
                country: props.currentLocation
            }
        }

        static getDerivedStateFromProps(nextProps, prevState) {
            if (nextProps.currentLocation !== prevState.country) {
                return {
                    country: nextProps.currentLocation
                }
            }
            return null
        }

       async  componentDidMount() {
            const { country } = this.state
            const { getUserLocation } = this.props

            
            if (!country) {
                const ip = await publicIp.v4()
                // getGeoLocation(this.getValue, true)

                getUserLocation({ ip })
            }
        }

        getValue = (response) => {
            const { country } = response
            const { setCurrentLocation } = this.props

            setCurrentLocation(country)
        }

        render() {
            const { country } = this.state
            const {
                roles,
                currentUser,
                history,
                match,
                globalBoatSearch,
                getRecentSearch,
                multiSearch,
                recentSearchResults,
                multipleSearchResults,
                clearMultiSearch,
                logout,
                changeUserStatus
            } = this.props

            return (
                <>
                    <UserProvider value={{
                        roles, currentUser, history, match, globalBoatSearch, country, getRecentSearch,
                        multiSearch, recentSearchResults, multipleSearchResults, clearMultiSearch, logout, changeUserStatus
                    }}>
                        <ComposedComponent {...this.props} />
                    </UserProvider>
                </>
            )

        }
    }

    const mapStateToProps = state => ({
        roles: state.loginReducer.roles,
        currentUser: state.loginReducer.currentUser,
        currentLocation: state.loginReducer.currentLocation,
        recentSearchResults: state.boatReducer.recentSearchResults,
        multipleSearchResults: state.boatReducer.multipleSearchResults,
    })

    const mapDispatchToProps = dispatch => ({
        globalBoatSearch: data => dispatch(globalBoatSearch(data)),
        setCurrentLocation: data => dispatch(setCurrentLocation(data)),
        getRecentSearch: () => dispatch(getRecentSearch()),
        multiSearch: data => dispatch(multiSearch(data)),
        clearMultiSearch: () => dispatch(clearMultiSearch()),
        logout: () => dispatch(logout()),
        changeUserStatus: data => dispatch(changeUserStatus(data)),
        getUserLocation: data => dispatch(getUserLocation(data)),
    })


    return connect(mapStateToProps, mapDispatchToProps)(WithProps)
}