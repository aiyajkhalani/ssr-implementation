import React, { Component } from 'react';

export default function (ComposedComponent) {
    class Authentication extends Component {

        componentDidMount() {
            this.isAuthCheck(this.props)
        }

        componentDidUpdate(nextProps) {
            this.isAuthCheck(nextProps)
        }

        isAuthCheck = (props) => {

            const isAuth = localStorage.getItem("isAuthenticated")

            if (!isAuth) {
                props.history.push('/login')
            }
            return
        }

        render() {
            const isAuth = localStorage.getItem("isAuthenticated")

            return (
                <>
                    {isAuth && <ComposedComponent {...this.props} />}
                </>
            )

        }
    }

    return Authentication
}