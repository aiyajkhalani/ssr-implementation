import React, { Component } from 'react'
import { connect } from 'react-redux'

export default function (ComposedComponent) {
    class WithVerification extends Component {

        render() {
            const { currentUser } = this.props
            const { isActivated } = currentUser

            return (
                <>
                    {isActivated ? <ComposedComponent {...this.props} />
                        :
                        <ConfirmationEmail open={!isActivated} />
                    }
                </>
            )
        }
    }

    const mapStateToProps = state => ({
        currentUser: state.loginReducer.currentUser,
    })

    return connect(mapStateToProps)(WithVerification)
}