import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import uuid from "uuid/v4";

const ProductCarousal = (props) => {
    
   return (
            <Carousel autoPlay>
            {props.photo.map((item)=>{
                return (
                    <div key={uuid()}>
                        <img src={item.image} alt={item.alt} />
                    </div>)
            })}
            </Carousel>)
    }

export default ProductCarousal