import React from "react";
import { SellAroundStyle } from "../styleComponent/styleComponent";
import { priceFormat } from "../../util/utilFunctions";

export const CityWiseComponent = React.memo(({ value , minimumPrice, width, height, city, img, getCityWiseBoats, className }) => {
    return (
        <SellAroundStyle
            className={value && value.length > 4 ? `mr-0` : `mr-20`}
            width={width}
            onClick={() => getCityWiseBoats()}
            height={height}
            img={img} >

            <div className="card-content-position cursor-pointer">
                <h1 className="mb-0 country-name">
                    {city}
                </h1>
                <h3 className="price1">Price start from ₹{minimumPrice && priceFormat(minimumPrice)}</h3>
            </div>
        </SellAroundStyle>
    )
})