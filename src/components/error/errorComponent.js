import React, { Component } from "react";
import { SnackbarContent } from "@material-ui/core";
import ErrorIcon from '@material-ui/icons/Error';
class ErrorComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { errors } = this.props;
    const errorsArray = typeof errors !== "string" && errors && errors.length && errors[0].message.split(",");

    return (
      <>
        {errors && (
          <>
            {typeof errors === "string" && 
             <p className="alert-danger" key={errors}><ErrorIcon /> {errors}</p>}
            {errorsArray && errorsArray.length &&
              <div className="alert alert-danger f-14 mb-0" role="alert" >
                {errorsArray.map((error, index) => <p className="alert-danger mb-0" key={index}><ErrorIcon /> {error}</p>)}
              </div>
            }
          </>
        )}
      </>
    );
  }
}
export default ErrorComponent;