import React, { Component } from 'react';
import Popover from "react-tiny-popover";

import { icons } from '../../util/enums/enums';
import IconButton from "@material-ui/core/IconButton";

import {
    FacebookShareButton,
    LinkedinShareButton,
    TwitterShareButton,
    PinterestShareButton,
    // GooglePlusShareButton,
    FacebookIcon,
    // GooglePlusIcon,
    TwitterIcon,
    LinkedinIcon,
    PinterestIcon,
} from 'react-share';

class SharePopup extends Component {
    render() {
        const { handleClick, index, selectedIndex, screenScroll, useOwnIcon } = this.props;

        return (
            <Popover
                disableReposition={true}
                isOpen={index === selectedIndex}
                position={"bottom"}
                onClickOutside={handleClick}
                content={
                    <div className="socialPopupBoxBg"  onMouseLeave={() => handleClick()}>
                        <div>
                            <FacebookShareButton url="https://www.facebook.com/" quote="Upcoming Events" style={{ cursor: 'pointer', marginRight: 5 }}>
                                <FacebookIcon size={32} round={true} />
                            </FacebookShareButton>
                            <TwitterShareButton url="https://www.twitter.com/" title={'share title'} style={{ cursor: 'pointer', marginRight: 5 }}>
                                <TwitterIcon size={32} round={true} />
                            </TwitterShareButton>
                            <PinterestShareButton url="https://in.pinterest.com/" description={'share description'} style={{ cursor: 'pointer', marginRight: 5 }}>
                                <PinterestIcon size={32} round={true} />
                            </PinterestShareButton>
                        </div>
                        <div className="mt-4 d-flex align-center">
                            {/* <GooglePlusShareButton url="https://plus.google.com/" style={{ cursor: 'pointer', marginRight: 5 }}>
                                <GooglePlusIcon size={32} round={true} />
                            </GooglePlusShareButton> */}
                            <LinkedinShareButton url="https://www.linkedin.com/" style={{ cursor: 'pointer', marginRight: 5 }}>
                                <LinkedinIcon size={32} round={true} />
                            </LinkedinShareButton>
                            <img src={icons['favicon']} />
                        </div>
                    </div>
                }
            >
                {
                    useOwnIcon ? (
                        <div onClick={handleClick} />
                    ) : (
                        <div className="share-button" onClick={handleClick}>
                            <IconButton
                                className="p-0"
                                aria-label="more"
                                aria-controls="long-menu"
                                aria-haspopup="true"

                            >
                                <i className="adam-share"></i>
                            </IconButton>
                        </div>
                    )
                }
                
            </Popover>
        );
    }
}

export default SharePopup;