import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Backdrop, Fade, Button, Link, TextField } from '@material-ui/core';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        margin: theme.spacing(1),
    },
    link: {
        margin: theme.spacing(3),
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export const ConfirmationEmail = (props) => {
    const classes = useStyles();

    const sendEmailLinkHandler = async (values) => {
        const { sendLink } = props
        await sendLink(values)
    }

    const { open, email, close } = props

    

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={close}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2 id="transition-modal-title">Confirm Your Mail</h2>
                        <p>We'll send an email to the address below. Tap the link in the email to confirm it's you.</p>
                        <Formik
                            initialValues={{ email: (email) || '' }}
                            validationSchema={Yup.object().shape({
                                email: Yup.string()
                                    .email('Email is invalid')
                                    .required('Email is required'),
                            })}
                            onSubmit={(values) => sendEmailLinkHandler(values)}
                            render={({ setFieldValue }) => (
                                <Form>
                                    <TextField
                                        disabled
                                        name="email"
                                        autoFocus
                                        fullWidth
                                        id="email"
                                        label="Email"
                                        defaultValue={email}
                                        onChange={(e) => setFieldValue('email', e.target.value)}
                                    />
                                    <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                    <Button variant="contained" type="submit" color="secondary" className={classes.button}>
                                        Send Confirmation Link
                                    </Button>
                                    <Link onClick={props.close} color="inherit" className={classes.link}>
                                        {"i'll do this later"}
                                    </Link>
                                </Form>
                            )}
                        />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}
