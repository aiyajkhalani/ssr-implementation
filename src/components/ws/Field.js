import React from "react";
import ImageUploader from "react-images-upload";
import CloseIcon from '@material-ui/icons/Close';
import { Card } from "react-bootstrap";
import { Button, Box } from "@material-ui/core";
import PhoneInput from 'react-phone-input-2';

import {
  handleSingleFileUpload,
  handleSingleFileDelete
} from "../../helpers/s3FileUpload";

import 'react-phone-input-2/lib/style.css';

export const Field = props => {
  const renderField = () => {
    switch (props.type) {
      case "text":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <input
              type="text"
              className="form-control"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></input>
          </div>
        );

      case "textarea":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <textarea
              class="form-control"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></textarea>
          </div>
        );

      case "number":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <input
              type="number"
              min="0"
              className="form-control"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></input>
          </div>
        );

      case "mobile-number":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <PhoneInput
              className="form-control w-100"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
            />
          </div>
        );

      case "url":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <input
              type="url"
              className="form-control"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></input>
          </div>
        );

      case "document":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <input
              type="file"
              accept=".xlsx,.xls/*,.doc, .docx,.ppt, .pptx,.txt,.pdf"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></input>
          </div>
        )

      case "single-document":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            {props.value &&
              <Box>
                {props.value}
                <CloseIcon
                  className="remove-doc"
                  onClick={() => handleSingleFileDelete(props.value, props.name, props.onChangeText)} />
              </Box>
            }
            <ImageUploader
              withIcon
              buttonText={props.buttonText}
              accept="/*"
              imgExtension={['.docx', '.doc', '.pdf', '.txt', '.xlsx']}
              onChange={file =>
                handleSingleFileUpload(
                  file,
                  props.name,
                  props.onChangeText
                )
              }
              maxFileSize={5242880}
            />
          </div>
        )

      case "single-image":
        return (
          <>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}

              </label>
            }

            <div className="d-flex flex-wrap justify-content-center single-upload-image-profile">
              {props.value ? (
                <div className="addBoat-container mr-2 mb-2 upload-img-circle">
                  <img
                    src={props.value}
                    alt={props.label}
                    className={props.isNotCircle && "square-img"}
                  />
                  <span
                    onClick={() =>
                      handleSingleFileDelete(
                        props.value,
                        props.name,
                        props.onChangeText
                      )
                    }
                  >
                    ×
                  </span>
                </div>
              ) : (
                  <div className="addBoatShow-imgUploader upload-img-circle single-image">
                    <img
                      src={require('./Images/placeholder.png')}
                      alt={props.label}
                      className={props.isNotCircle && "square-img"}
                    />
                    <ImageUploader
                      withIcon={false}
                      withLabel={false}
                      buttonText={""}
                      buttonStyles={{
                        'background-image': `url(${require('./Images/camera.png')})`,
                      }}
                      onChange={file =>
                        handleSingleFileUpload(
                          file,
                          props.name,
                          props.onChangeText
                        )
                      }
                      maxFileSize={5242880}
                    />
                  </div>
                )}
            </div>
          </>
        );


      case "shipment-document":
        return (
          <>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}

              </label>
            }

            <div className="d-flex flex-wrap justify-content-center single-upload-image-profile">
              {props.value ? (
                <Box>
                {props.value}
                <CloseIcon
                  className="remove-doc"
                  onClick={() => handleSingleFileDelete(props.value, props.name, props.onChangeText)} />
              </Box>
              ) : (
                  <div className="addBoatShow-imgUploader upload-img-circle single-image">
                    <img
                      src={require('./Images/placeholder.png')}
                      alt={props.label}
                      className="shipment-document-uploader"
                    />
                    <ImageUploader
                      
                    withIcon={false}
                    withLabel={false}
                    buttonText={""}
                    buttonStyles={{
                      'background-image': `url(${require('./Images/camera.png')})`,
                    }}
                      accept={".docx, .doc, .pdf, .txt, .xlsx"}
                      imgExtension={['.docx', '.doc', '.pdf', '.txt', '.xlsx']}
                      onChange={file =>
                        handleSingleFileUpload(
                          file,
                          props.name,
                          props.onChangeText
                        )
                      }
                      maxFileSize={5242880}
                    />
                  </div>
                )}
            </div>
          </>
        );

      case "bootstrap-single-image":
        return (
          <>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }

            <div className="d-flex flex-wrap justify-content-center">
              {props.value ?
                (
                  <div className="addBoat-container mr-2 mb-2">
                    <img src={props.value} alt={props.label} height="100px" width="100px" />
                    <span onClick={() => handleSingleFileDelete(props.value, props.name, props.onChangeText)} > × </span>
                  </div>
                )
                :
                (
                  <div className="addBoatShow-imgUploader">
                    <input type="file" id={props.id} name={props.name} accept="image/png, image/jpeg"
                      onChange={e => handleSingleFileUpload(e.target.files, props.name, props.onChangeText)} />
                  </div>
                )}
            </div>
          </>
        );
      case "bootstrap-single-image-register":
        return (



          <>
            {props.value ?
              (
                <div className="addBoat-container mr-2 mb-2">
                  <img src={props.value} alt={props.label} height="100px" width="100px" />
                  <span onClick={() => handleSingleFileDelete(props.value, props.name, props.onChangeText)} > × </span>
                </div>
              )
              :
              (

                <div className="register-img-uploader">
                  <input type="file" id={props.id} name={props.name} accept="image/png, image/jpeg"
                    onChange={e => handleSingleFileUpload(e.target.files, props.name, props.onChangeText)} />
                  <Button variant="contained" className="attach-btn">Attach Company Logo</Button>
                </div>
              )}

          </>

        );
      

      case "email":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <input
              type="email"
              className="form-control"
              placeholder={props.placeholder}
              id={props.id}
              name={props.name}
              onChange={props.onChangeText}
              value={props.value}
              disabled={props.disabled}
            ></input>
          </div>
        );

      case "radio":
        return (
          <div>
            {props.label &&
              <label className={props.required && "required"} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <div>
              <Card className={props.flex ? `d-flex flex-row justify-content-evenly profile-radio-container` : 'pl-8'}>
                {props.options &&
                  props.options.length > 0 &&
                  props.options.map(item => (
                    <div class="form-check" key={item.id}>
                      <input
                        checked={props.value === item.value ? true : false}
                        class="form-check-input"
                        type="radio"
                        name={props.name}
                        id={item.id}
                        value={item.id}
                        onChange={props.onChangeText}
                      />
                      <label class="form-check-label" for={item.id}>
                        {item.value}
                      </label>
                    </div>
                  ))}
              </Card>
            </div>
          </div>
        );

      case "select":
        return (
          <div>
            {props.label &&
              <label className={`${props.required && "required"} ${props.labelClass && props.labelClass}`} htmlFor={props.id}>
                {props.label}
              </label>
            }
            <select
              class="custom-select custom-select-sm"
              value={props.value}
              onChange={props.onChangeText}
            >
              <option selected>{props.placeholder}</option>
              {props.options &&
                props.options.length > 0 &&
                props.options.map(item => (
                  <option
                    key={item.id}
                    value={item.id}
                    selected={props.id === item.id}
                  >
                    {item.value}
                  </option>
                ))}
            </select>
          </div>
        );

      default:
        break;
    }
  };

  return <>{renderField()}</>;
};
