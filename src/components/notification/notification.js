import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export const Notification = (props) => <ToastContainer autoClose={2000} hideProgressBar />


