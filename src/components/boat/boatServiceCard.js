import React from "react";
import { Card, CardContent } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import "./boatCard.scss";
import { SectionWithShowMapStyle } from "../styleComponent/styleComponent";

export const BoatServiceMapCard = props => {
    const { value, onClick, index } = props;

    return (
        <Card className="boat-card mb-15" onClick={onClick}>
            <div className="row max-height w-100 m-0">
                <div className="col-xl-5 p-0">
                    <SectionWithShowMapStyle img={value.images && value.images.length && encodeURI(value.images[0])} />
                </div>
            </div>
            <div className="col-xl-7">
                <div className="boat-card-details">
                    {/* <span className="boat-status-ab">{boatStatus && boatStatus.alias}</span> */}
                    <CardContent className="boat-card-content h-100 pb-4 pt-0">
                        <div className="d-flex flex-column h-100 justify-content-evenly">
                            <div className="price-with-rating">
                                <div className="new-boat-label">
                                    <h6 className="mb-0 home-boat-price"> {value.city},{value.country || value.serviceCountry}</h6>
                                </div>
                            </div>
                            <h5 className="place-city boat-name">{value.service || value.serviceDesc}</h5>


                            <div className="d-flex position-absolute bottom-0">
                                <div className="mr-1">
                                    <Rating
                                        className="rating-clr"
                                        initialRating={value.rating}
                                        fullSymbol={<StarIcon />}
                                        emptySymbol={<StarBorderIcon />}
                                        placeholderSymbol={<StarBorderIcon />}
                                        readonly
                                    />
                                </div>
                                <span className="boat-card-rating-count"> ({value.rating}) </span>
                            </div>
                        </div>
                    </CardContent>
                </div>
            </div>
        </Card>
    );
};
