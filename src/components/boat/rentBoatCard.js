import React, { useState } from "react";
import { Card, CardContent } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { SectionWithShowMapStyle } from "../styleComponent/styleComponent";

import "./boatCard.scss";
import { Icons } from "../icons";
import { priceFormat } from "../../util/utilFunctions";
import SharePopup from "../share/SharePopup";

export const RentBoatCard = props => {

  const [selectedIndex, setSelectedIndex] = useState(null);

  const { boat, onClick, index } = props;

  const handleClick = index => {
    setSelectedIndex(selectedIndex !== index ? index : null);
  };

  const { boatName, images, country, rating, price, tripType, trip,noOfUnit,tripTime,maximumGuest,boatLength } = boat;
  return (
    <Card className="boat-card boat-card-rent-with-map mb-15" >
      <div className="row max-height w-100 m-0">
        <div className="col-xl-5 p-0">
          <div className="boat-card-image-height">
          <div className="position-relative">
              <div className="card-action rent-card-cation-with-map" >
                <div class="share-icon">
                  <div class="heart-button">
                    <i class="adam-heart-1"></i>
                  </div>
                    <SharePopup handleClick={()=> handleClick(index)} index={index} selectedIndex={selectedIndex} />


                </div>
              </div>
            </div>
            <SectionWithShowMapStyle img={images && images.length && encodeURI(images[0])} onClick={onClick} />
          </div>
        </div>
        <div className="col-xl-7" onClick={onClick}>
          <div className="boat-card-details">
            <CardContent className="boat-card-content h-100 p-0 d-flex flex-column h-100 justify-content-evenly">
              <div className="fix-h-rating">
              <div className="new-build">
                {trip && trip.alias}
              </div>
              <div className="d-flex align-items-center rating-rent-with-map">
                <div className="mr-1">
                  <Rating
                    className="rating-clr"
                    initialRating={rating}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="rating-count-rent mt-1">
                  {" "}
                  ({rating || `0`}){" "}
                </span>
              </div>
              </div>
                <div className="type-list">
              {tripType && tripType.length > 0 && tripType.map(type => {
                    return <div className="rent-trip-type type-name-rent">{type.name}</div>;
                  })}
                  </div>
                
            

              <div className="country-div-height">
                <h6 className="image-location boat-title mb-0 rent-boat-name-color">
                  {boatName} &nbsp;
                </h6>
                <h6 className="image-location font-weight-none boat-country mb-2">
                  {country}
                </h6>
              </div>
              <div className="d-flex align-items-center rent-card-price-with-map">

                <h6 className="mt-2 boat-price-rent">
                  INR {priceFormat(price)}
                  {<span className="per-person boat-price-rent"> Person</span>}
                </h6>
              </div>
              {trip && trip.alias === "Rent Per Hour" ? (
                    <div className="alignCenter mr-3 ft-type">
                      <img src={require("../../assets/images/rent/pieces.png")} className="piece-img" />
                      <span className="pieces-text">{ noOfUnit || `0`} Pieces Available</span>
                    </div>
                  ) : (
                      <div className="directionRow directionRow-custom ft-type">
                        <div className="alignCenter mr-3">
                          <Icons data="adam-clock-2 location-icon-rent" />
                          <span className="small-text">{tripTime || `0`} Hrs</span>
                        </div>
                        <div className="alignCenter mr-3">
                          <Icons data="adam-user-group location-icon-rent" />
                          <span className="small-text">{maximumGuest || `0`}</span>
                        </div>
                        <div className="alignCenter mr-3">
                          <Icons data="adam-dimension-1 location-icon-rent" />
                          <span className="small-text">{boatLength || `0`} ft</span>
                        </div>
                      </div>
                    )}
            </CardContent>
          </div>
        </div>
      </div>
    </Card>
  );
};
