import React, { useState } from "react";
import { Card, CardContent } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { SectionWithShowMapStyle } from "../styleComponent/styleComponent";
import { priceFormat, commonBoatShowAllType } from "../../util/utilFunctions";
import SharePopup from "../share/SharePopup";
import { cityCountryNameFormatter } from "../../helpers/jsxHelper";

import "./boatCard.scss";

export const BoatCard = props => {

  const [selectedIndex, setSelectedIndex] = useState(null);
  const { boat, onClick, index } = props;
  const {
    boatName,
    price,
    country,
    city,
    images,
    rating,
    manufacturedBy,
  } = boat;

  const handleClick = index => {
    setSelectedIndex(selectedIndex !== index ? index : null);
  };

  return (
    <Card className="boat-card mb-15" >
      <div className="row max-height w-100 m-0">
        <div className="col-xl-5 p-0">
          <div className="boat-card-image-height">
            <div className="position-relative">
              <div className="card-action" >

                {commonBoatShowAllType(boat)}

                <div class="share-icon">
                  <div class="heart-button">
                    <i class="adam-heart-1"></i>
                  </div>
                  <SharePopup handleClick={() => handleClick(index)} index={index} selectedIndex={selectedIndex} />


                </div>
              </div>
            </div>
            <SectionWithShowMapStyle img={images && images.length && encodeURI(images[0])} onClick={onClick} />
          </div>
        </div>
        <div className="col-xl-7" onClick={onClick}>
          <div className="boat-card-details">

            <CardContent className="boat-card-content h-100 pb-4 pt-0">
              <div className="d-flex flex-column h-100 justify-content-evenly">

                <h5 className="place-city boat-name">{boat.yearBuilt} {boat && boat.boatType && boat.boatType.name} / {boat.lengthInFt} ft</h5>

                <h6 className="image-location home-boat-name-font boat-title mb-0">
                  {boatName}
                </h6>
                <h5 className="image-location font-weight-none boat-country mt-0">
                  {cityCountryNameFormatter(city, country)}
                </h5>
                <h6 className="image-location boat-owner mb-0">
                  by {manufacturedBy}
                </h6>

                <div className="d-flex position-absolute bottom-0">
                  <div className="mr-1">
                    <Rating
                      className="rating-clr"
                      initialRating={rating}
                      fullSymbol={<StarIcon />}
                      emptySymbol={<StarBorderIcon />}
                      placeholderSymbol={<StarBorderIcon />}
                      readonly
                    />
                  </div>
                  <span className="boat-card-rating-count"> ({rating}) </span>
                </div>
              </div>
              <span className="price">₹ {priceFormat(price)}</span>
            </CardContent>
          </div>
        </div>
      </div>
    </Card>
  );
};
