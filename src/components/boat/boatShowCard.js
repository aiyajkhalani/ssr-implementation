import React from "react";
import { Card, CardContent } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import "./boatCard.scss";
import moment from "moment";
export const BoatShowCard = props => {
    const { boat, onClick } = props;
    const {
        title,
        city,
        showLogo,
        country,
        showDescription,
        startDate,
        endDate,
    } = boat;
    return (
        <Card className="boat-card mb-15" onClick={onClick}>
            <div className="row max-height w-100 m-0">
                <div className="col-xl-5 p-0">
                    <div className="boat-card-image-height">
                        <img
                            src={showLogo}
                            alt={title}
                            className="h-100"
                        />
                    </div>
                </div>
                <div className="col-xl-7">
                    <div className="boat-card-details">
                        <CardContent className="boat-card-content h-100 pb-4 pt-0">
                            <div className="d-flex flex-column h-100 justify-content-evenly">
                                <h6 className="image-location home-boat-name-font boat-title mb-0">
                                    {title}
                                </h6>
                                <div class="margin-bottom-boat-show d-flex align-items-center">
                                    <i class="adam-navigation-main location-icon boat-show-icon"></i><span class="boat-show-text">{city}</span></div>
                                <div class="margin-bottom-boat-show">
                                    <div class="d-flex flex-column">
                                        <span class="boat-show-text d-flex align-items-center">
                                            <i class="adam-calendar1 location-icon boat-show-icon">
                                            </i> {moment(startDate).format('DD-MM-YYYY')}  -  {moment(endDate).format('DD-MM-YYYY')} </span>
                                    </div>
                                </div>
                                <div>
                                    <div class="d-flex flex-column"><span class="boat-show-text d-flex align-items-center"><i class="adam-clock-2 location-icon boat-show-icon"></i> 12 : 00 AM  -  12 : 00 AM</span></div>
                                </div>
                            </div>
                        </CardContent>
                    </div>
                </div>
            </div>
        </Card>
    );
};