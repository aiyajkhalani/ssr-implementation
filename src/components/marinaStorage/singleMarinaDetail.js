import React, { PureComponent, Fragment } from "react";
import Truncate from "react-truncate";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import { Grid } from "@material-ui/core";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { MostPopularStyle } from "../styleComponent/styleComponent";
import SharePopup from "../share/SharePopup";
import { commonMarinaType } from "../../util/utilFunctions";
import { viewMarinaDetails } from "../../helpers/boatHelper";
import { cityCountryNameFormatter } from "../../helpers/jsxHelper";
import { dimension } from "../../util/enums/enums";
import { getHeightRatio, getRatio } from "../../helpers/ratio";

export class SingleMarinaDetail extends PureComponent {

  state = {
    selectedIndex: null,
    height: 300,
    width: 400
  }

  componentDidMount() {
    const width = getRatio(dimension, "marinaMostPopular", ".section-grid");
    const height = getHeightRatio(
      dimension,
      "marinaMostPopular",
      ".section-grid"
    );
    if (width && height) {
      this.setState({ width, height });
    }
  }
  handleClick = index => {
    const { selectedIndex } = this.state;
    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {

    const { selectedIndex, width, height } = this.state
    const { index, value } = this.props
    const {
      images, city, country, rating, membersReview, user, id, provider
    } = value;

    return (
      <Fragment>
        <Grid
          item
          xs={3}
          className={index > 3 ? 'most-popular-content mb-0' : 'most-popular-content'}

        >
          <div className="storage-around cursor-pointer marina-ads-sections">
          <div className="position-relative ">

            <MostPopularStyle
              bgHeight={height}
              bgWidth={width}
              onClick={() => viewMarinaDetails(id)}
              img={images && images.length && encodeURI(images[0])}
            >
            </MostPopularStyle>
            <div className="card-action card-action-marina justify-content-between">
              <div onClick={() => viewMarinaDetails(id)}>{commonMarinaType(provider)}</div>
              <div class="share-icon">
                <div class="heart-button">
                  <i class="adam-heart-1"></i>
                </div>
                <SharePopup
                  handleClick={() => this.handleClick(index)}
                  index={index}
                  selectedIndex={selectedIndex}
                />
              </div>
            </div>
          </div>

          <div className="spaceBetween mb-1 mt-3" onClick={() => viewMarinaDetails(id)}>
            <div className="most-popular-place-name">
              {user && user.companyLogo &&
                <div className="marina-icon">
                  <img
                    src={user && user.companyLogo}
                    alt="company logo"
                  />
                </div>
              }
              <h6 className="place-city ">
                {cityCountryNameFormatter(city, country)}
              </h6>
              <div className="d-flex align-center">
                <div>
                  <Rating
                    className="rating-clr"
                    initialRating={rating}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="ml-2 rating-text f-14">({rating})</span>
              </div>
            </div>

            <span className="most-popular-detail">
              <Truncate lines={2} ellipsis={<span>..</span>}>
                {membersReview}
              </Truncate>
            </span>

            <span
              className="view-marina"

            >
              {user && user.companyName}
            </span>

          </div>
          </div>
        </Grid>

      </Fragment>
    );
  }
}
