import React, { Component, Fragment } from "react";
import { Grid } from "@material-ui/core";
import { SingleMarinaDetail } from "./singleMarinaDetail";
import { ShowAllLink } from "../helper/showAllLink";



class MarinaMostPopular extends Component {
  
  render() {
    const {
      mostPopular,
      limit,
      showType,
      showAllText
    } = this.props;

    return (
      <>

        <Grid className="most-popular-main cursor-pointer marina-ads-sections" container spacing={2}>
          {mostPopular &&
            mostPopular.map((item, index) => {
              return (
                <Fragment key={item.id}>
                  {index < limit && (
                    <SingleMarinaDetail
                      value={item}
                      index={index}
                    />
                  )}
                </Fragment>
              );
            })}
        </Grid>
        <>
        <ShowAllLink 
          data={mostPopular} 
          itemsLength={limit} 
          showAllText={showAllText} 
          url={`/show-all-marina-and-storage/${showType}`} />
        </>
      </>
    );
  }
}

export default MarinaMostPopular;
