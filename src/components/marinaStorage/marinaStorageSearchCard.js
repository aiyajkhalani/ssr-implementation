import React from "react";
import {
  Select,
  FormControlLabel,
  Radio,
  RadioGroup,
  Button
} from "@material-ui/core";
import { Formik, Form, ErrorMessage } from "formik";
import { Link } from "react-router-dom";

import SearchComplete from "../map/SearchComplete";
import { renderMarinaServicesMenuItems } from "../../helpers/jsxHelper";
import "../../containers/marinaStorage/marinaStorage.scss";
import CommonBanner from "../mainBanner/commonBanner";

export const StyledRadio = props => {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<span className="icon-test  checked-icon" />}
      icon={<span className="icon-test" />}
      {...props}
    />
  );
};

export const MarinaStorageSearchCard = ({ types, services, search, marinaBanner }) => {

  const setPlaceHandler = ({ country, city }, setFieldValue) => {
    setFieldValue("country", country);
    setFieldValue("city", city);
  };

  return (
    <Formik
      initialValues={{
        country: "",
        city: "",
        serviceProvide: "",
        provider: ""
      }}
      onSubmit={values => {
        search(values);
      }}
      render={({ values, setFieldValue, handleSubmit }) => (
        <Form className="bg-transparent-form" onSubmit={e => e.preventDefault()}>
          <div className="bg-blue marina-storage-bg">
            <CommonBanner data={marinaBanner} />
            <div className="w-100 h-100 marina-storage-card-position">
              <div className="card">

                <RadioGroup
                  name="provider"
                  className="storage-radio-group"
                  value={values.provider}
                  onChange={e => {
                    setFieldValue("provider", e.target.value);
                  }}
                >
                  {types &&
                    types.length > 0 &&
                    types.map(type => (
                      <FormControlLabel
                        key={type.id}
                        value={type.id}
                        control={<StyledRadio />}
                        label={type.alias}
                      />
                    ))}
                  <ErrorMessage
                    component="div"
                    name={"provider"}
                    className="error-message"
                  />

                  <div className="search w-100">
                    <SearchComplete
                      placeHolder="Where would you like to search?"
                      className="form-control font-14"
                      getPlaceName={place =>
                        setPlaceHandler(place, setFieldValue)
                      }
                    />
                    <ErrorMessage
                      component="div"
                      name={"city" || "country"}
                      className="error-message"
                    />
                  </div>

                  <div className="search w-100 mt-0">
                    <Select
                      className="form-control font-14 select-div-padding border-none"
                      fullWidth
                      value={
                        values.serviceProvide === ""
                          ? "placeholder"
                          : values.serviceProvide
                      }
                      variant="outlined"
                      renderValue={selected => {
                        if (selected === "placeholder") {
                          return "Service you looking for?";
                        } else {
                          const selectedService = services.find(
                            item => item.id === selected
                          );
                          return selectedService.name;
                        }
                      }}
                      onChange={e =>
                        setFieldValue("serviceProvide", e.target.value)
                      }
                    >
                      {renderMarinaServicesMenuItems(services)}
                    </Select>
                    <ErrorMessage
                      component="div"
                      name="serviceProvide"
                      className="error-message"
                    ></ErrorMessage>
                  </div>

                  <div className="notify">
                    <Link href="#" className="marina-notify">
                      <u >Notify Me</u>
                    </Link>
                    <Button className="search-button-width" variant="contained" onClick={handleSubmit}>
                      Search Now
                    </Button>
                  </div>

                </RadioGroup>
              </div>
            </div>
          </div>
        </Form>
      )}
    />
  );
};
