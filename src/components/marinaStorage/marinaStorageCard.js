import React, { useState } from "react";
import { Card, CardContent } from "@material-ui/core";
import Truncate from "react-truncate";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import "../boat/boatCard.scss";
import { SectionWithShowMapStyle } from "../styleComponent/styleComponent";
import SharePopup from "../share/SharePopup";
import { cityCountryNameFormatter } from "../../helpers/jsxHelper";

export const MarinaStorageCard = props => {

  const { marinaStorage: { placeName, city, country, facilities, rating, images, id }, viewMarina, index } = props;
  const [selectedIndex, setSelectedIndex] = useState(null);

  const handleClick = index => {
    setSelectedIndex(selectedIndex !== index ? index : null);
  };
  return (
    <Card className="boat-card mb-15 card-marin-with-map cursor-pointer ">
      <div className="row max-height w-100 m-0">
        <div className="col-xl-5 p-0">
          <SectionWithShowMapStyle img={images && images.length && encodeURI(images[0])} onClick={() => viewMarina(id)} />
          <div className="card-action marina-card-cation-with-map" >
            <div class="share-icon">
              <div class="heart-button">
                <i class="adam-heart-1"></i>
              </div>
              <SharePopup handleClick={() => handleClick(index)} index={index} selectedIndex={selectedIndex} />
            </div>
          </div>
        </div>
        <div className="col-xl-7" onClick={() => viewMarina(id)}>
          <div className="boat-card-details">
            <CardContent className="boat-card-content">
              <h5 className="place-name">{placeName}</h5>
              <h6 className="place-city">
                {cityCountryNameFormatter(city, country)}
              </h6>
              <span className="recommended-detail ">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {facilities}
                </Truncate>
              </span>
              <div className="d-flex align-center marina-content-ab">
                <div>
                  <Rating
                    className="rating-clr"
                    initialRating={rating}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="ml-2 rating-text f-14">({rating})</span>
              </div>
            </CardContent>
          </div>
        </div>
      </div>
    </Card>
  );
};
