import React, { PureComponent, Fragment } from "react";
import Truncate from "react-truncate";

import { MostPopularContentStyle, MostPopularStyle } from "../styleComponent/styleComponent";
import SharePopup from "../share/SharePopup";
import { commonMarinaType } from "../../util/utilFunctions";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Rating from "react-rating";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { viewMarinaDetails } from "../../helpers/boatHelper";

export class SingleTopRatedMarina extends PureComponent {

  state = {
    selectedIndex: null,
  }

  handleClick = index => {
    const { selectedIndex } = this.state;
    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {

    const { selectedIndex } = this.state
    const { index, value, width, height } = this.props
    const {
      images,
      userCount,
      facilities,
      id,
      user: { firstName, lastName },
      provider
    } = value;

    return (
      <Fragment>
        <MostPopularContentStyle
          bgWidth={width}
          className="most-popular-content cursor-pointer position-relative"
        >
          <MostPopularStyle
            bgHeight={height}
            bgWidth={width}
            onClick={() => viewMarinaDetails(id)}
            img={images && images.length && encodeURI(images[0])}
          >
            <div className="inner-banner-views-div d-flex font13 pl-0">
              <div className="d-flex justify-content-center align-items-center ">
                <img
                  src={require("../../assets/images/rentInner/view.png")}
                  className="marina-views-img mr-2"
                  alt=""
                />
              </div>
              <div className="banner-count-text">{userCount}</div>
            </div>
          </MostPopularStyle>
          <div className="marina-storage-layout">
            <div className="card-action justify-content-between">
              <div onClick={() => viewMarinaDetails(id)}>{commonMarinaType(provider)}</div>
              <div class="share-icon">
                <div class="heart-button">
                  <i class="adam-heart-1"></i>
                </div>
                <SharePopup
                  handleClick={() => this.handleClick(index)}
                  index={index}
                  selectedIndex={selectedIndex}
                />
              </div>
            </div>
          </div>
          <div className="top-rated-info" onClick={() => viewMarinaDetails(id)}>


            <div className="top-rated-detail f-15">
              <Truncate lines={2} ellipsis={<span>..</span>}>
                {facilities}
              </Truncate>
            </div>
            <div className="d-flex align-center mt-1">
              <div className="mr-1">
                <Rating
                  className="rating-clr"
                  initialRating="3"
                  fullSymbol={<StarIcon />}
                  emptySymbol={<StarBorderIcon />}
                  placeholderSymbol={<StarBorderIcon />}
                  readonly
                />
              </div>
              <span className="rating-count">(3)</span>
            </div>
            <div className="user-info">
              <div className="user-name f-14 top-rated-section-user">
                {images && images.length ? (
                  <img src={images[0]} alt={firstName} className="marina-user-rated" />
                ) : (
                    <AccountCircleIcon />
                  )}
                By {firstName} {lastName}
              </div>

            </div>
          </div>
        </MostPopularContentStyle>
      </Fragment>
    );
  }
}