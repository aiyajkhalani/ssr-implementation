import React, { Component } from "react";
import { connect } from "react-redux";

import {  pagination } from "../../util/enums/enums";
import { Layout } from "../layout/layout";
import { getMarinaAndStorageAllServices } from "../../redux/actions";
import { PaginationBar } from "../pagination/PaginationBar";
import BoatListingsWithMap from "../gridComponents/BoatListingsWithMap";
import UserContext from "../../UserContext";

import "./marinaStorage.scss";

class MarinServicesView extends Component {
  state = {
    serviceId: null,
    showMap:false,
    country:"India",
  };
  static contextType = UserContext;

  static getDerivedStateFromProps(nextProps) {
    const { match } = nextProps;
    const { params } = match && match;

    if (params && params.hasOwnProperty("serviceId") && params.serviceId) {
      return {
        serviceId: params.serviceId
      };
    }

    return null;
  }
 
  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };
  componentDidMount() {
    const { serviceId } = this.state;
    const { getMarinaAndStorageAllServices } = this.props;
    
    getMarinaAndStorageAllServices({
      serviceId,
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    });
   
  }

  render() {
    const {   showMap } = this.state;
    const { country } = this.context;
    const { marinaServiceList, marinaServiceListTotal } = this.props;
    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} overflow-hidden w-100`}>

          <BoatListingsWithMap
            isMarinaStorage
            boatsTypeCount={marinaServiceListTotal}
            boatsType="Marina Service Boats"
            isShowAuction
            showMap={showMap}
            toggleMapHandler={this.toggleMapHandler}
            boats={marinaServiceList}
          />

          {marinaServiceListTotal > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination mt-50">
            <PaginationBar
              action={getMarinaAndStorageAllServices}
              value={{ country }}
              totalRecords={marinaServiceListTotal}
            />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  marinaServiceList:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaServiceList,
  marinaServiceListTotal:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaServiceListTotal
});
const mapDispatchToProps = dispatch => ({
  getMarinaAndStorageAllServices: data =>
    dispatch(getMarinaAndStorageAllServices(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(MarinServicesView);
