import React, { useState, useEffect, useContext } from "react";
import OutsideClickHandler from "react-outside-click-handler";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { Link } from "react-router-dom";
import Slide from "@material-ui/core/Slide";
import PropTypes from "prop-types";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import moment from "moment";
import {
  InputBase,
  Grid,
} from "@material-ui/core";
import { IoIosArrowDown } from "react-icons/io";
import { MdLocationOn } from "react-icons/md";

import CountrySelect from "../staticComponent/headerCountrySelect";
import RegisterUser from "../popUp/RegisterUser";
import { UserConsumer } from "../../UserContext";
import UserContext from "../../UserContext";
import SearchDropdown from "./searchDropdown";
import { bellIcon, salesEngineAccessibleTypes } from "../../util/enums/enums";

import "./header.scss";
import "../home/icon.scss";

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  logoDesktop: {
    display: "block",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  desktopMenu: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  desktopSearch: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "block"
    }
  },
  title: {
    [theme.breakpoints.up("sm")]: {
      display: "block",
      padding: "10px 0 0 0"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1.8),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "auto"
    },
    border: "1px solid #0d0799",
    color: "#0d0799",
    background: "white",
    fontWeight: "500"
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  displayFlex: {
    display: "flex",
    padding: "1rem .5rem .5rem 0",
    borderBottom: "solid 3px transparent"
  },
  displayFlexCenter: {
    display: "flex",
    padding: "1rem .5rem .5rem .5rem",
    borderBottom: "solid 3px transparent",
    justifyContent: "center"
  },
  alignRight: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  headerPadding: {
    padding: "6px 15px 0",
    display: "flex",
    minHeight: "unset"
  },
  textStyle: {
    color: "#484848",
    textDecoration: "none",
    fontSize: "0.8125rem"
  },
  textStyleBlack: {
    color: "#000",
    textDecoration: "none"
  },
  textFieldPadding: {
    padding: "4px 16px"
  },
  overflowUnset: {
    overflow: "unset"
  },
  textPrimary: {
    color: "#0d0799"
  },
  width100: {
    width: "100%"
  },
  root: {
    padding: "2px 4px",
    display: "flex",
    width: "600px",
    border: "1px solid #EBEBEB !important",
    borderRadius: "4px !important",
    boxShadow: "0 2px 4px rgba(0,0,0,0.1) !important",
    justifyContent: "center"
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  }
}));

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });
  return (
    <Slide direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func
};

const Header = props => {
  const {
    country,
    getRecentSearch,
    multiSearch,
    clearMultiSearch,
    match,
    logout,
    currentUser,
    changeUserStatus
  } = useContext(UserContext);

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [indexHeader, setIndexHeader] = React.useState("z-header-fix");
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const isAuth = localStorage.getItem("isAuthenticated");
  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }
  function handleMenuClose() {
    setAnchorEl(null);
    handleMobileMenuClose();
  }
  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }
  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    ></Menu>
  );
  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <Link to="/" className={classes.textStyle}>
          Register
        </Link>
      </MenuItem>
      <MenuItem>
        <Link to="/" className={classes.textStyle}>
          Sign Up
        </Link>
      </MenuItem>
      <MenuItem>
        <Link to="/" className={classes.textStyle}>
          Help
        </Link>
      </MenuItem>
      <CountrySelect />
      <MenuItem>
        <Link to="/rent" className={classes.textStyle}>
          Charter & Rent
        </Link>
      </MenuItem>
      <MenuItem>
        <Link to="/boat-service" className={classes.textStyle}>
          Service & Maintenance
        </Link>
      </MenuItem>
      <MenuItem>
        <Link to="/marina-storage" className={classes.textStyle}>
          Marina & Storage
        </Link>
      </MenuItem>
      <MenuItem>
        <Link to="/boat-show" className={classes.textStyle}>
          Boat Show
        </Link>
      </MenuItem>
    </Menu>
  );

  const [query, setQuery] = useState("");
  const [queryError, setQueryError] = useState("");

  const [open, isOpen] = useState(false);

  function isShowPopUp(isLoginOpen) {
    setIsLoginOpen(isLoginOpen);
  }
  function setSticky(value) {
    value
      ? setIndexHeader("z-header-fix-sticky")
      : setIndexHeader("z-header-fix");
  }
  const [isLoginOpen, setIsLoginOpen] = React.useState(false);

  const searchHandler = async () => {
    const payload = { query: query, country: country };
    setQueryError("");

    if (query && query.length >= 3) {
      isOpen(true);
      await multiSearch(payload);
      getRecentSearch();
    } else if (query.length === 0) {
      setQueryError("type something to search");
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      const payload = { query: query, country: country };

      setQueryError("");
      if (query && query.length >= 3) {
        isOpen(true);
        multiSearch(payload);
        getRecentSearch();
      } else if (query.length === 0) {
        isOpen(false);
      } else {
        setQueryError("searching would begin with atleast 3 letters!");
      }
    }, 500);

    return () => clearTimeout(timer);
  }, [query, country, multiSearch, getRecentSearch]);

  const dashboardSetLogo = () => {
    const {
      role
    } = currentUser;

    if (role) {
      switch (role.aliasName) {
        case "rent-and-charter":
          return require("../../assets/images/logo/rent.png");

        case "marina-and-storage":
          return require("../../assets/images/logo/marina.png");

        case "service-and-maintenance":
          return require("../../assets/images/logo/service.png");

        default:
          return require("../../assets/images/logo/home.png");
      }
    }
    return ""
  };

  const setLogo = () => {
    const { path } = match && match;

    if (path) {
      switch (path) {
        /** Rent Path */
        case "/rent":
        case "/rent-category-wise-boats/:tripTypeId/:country":
        case "/rent-city-wise-boats/:city/:country":
        case "/show-all-rent/:type":
        case "/add-rent-boat":
        case "/rent-inner/:id/:name":
        case "/search-rent-boats":
          return {
            key: require("../../assets/images/logo/rent.png"),
            className: "rent-logo-fix"
          };

        /** Boat Service Path */
        case "/boat-service":
        case "/boat-service-inner/:id":
        case "/view-boat-service":
        case "/add-rent-boat":
        case "/show-all-boat-service-gallery":
        case "/view-boat-service/:typeId":
        case "/search-boat-services":
          // return require("../../assets/images/logo/service.png");
          return {
            key: require("../../assets/images/logo/service.png"),
            className: "service-logo-fix"
          };

        /** Marina Storage Path */
        case "/marina-storage":
        case "/marina-storage-view":
        case "/marina-storage-service-view/:serviceId":
        case "/marina-storage-inner/:id":
        case "/search-marina-storage":
        case "/show-all-marina-and-storage/:type":
          // return require("../../assets/images/logo/marina.png");
          return {
            key: require("../../assets/images/logo/marina.png"),
            className: "marina-logo-fix"
          };

        case "/boat-show":
        case "/show-all-boat-show":
        case "/add-boat-show":
        case "/search-boat-show":
          // return require("../../assets/images/logo/boatshow.png");
          return {
            key: require("../../assets/images/logo/boatshow.png"),
            className: "boatshow-logo-fix"
          };

        case "/dashboard":
        case "/profile":
        case "/manage-marina-storage":
        case "/manage-articles":
        case "/manage-boat-shows":
        case "/change-password":
          return {
            key: dashboardSetLogo(),
            className: "dashboard-type-logo"
          }

        default:
          // Temporary need to change using api. @ghanshaym
          // return "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/Logo.png.png"
          // return require("../../assets/images/logo/home.png");
          return {
            key: require("../../assets/images/logo/home.png"),
            className: "home-logo-fix"
          };
      }
    }
  };

  const openSearchedResults = () => {
    if (query && query.length >= 3) {
      isOpen(true);
    }
  };

  return (
    <>
      <div
        className={`${classes.grow} d-sm-menu-none ${props.customClassName} ${indexHeader}`}
      >
        <AppBar
          className={`header__bg header-responsive ${currentUser && currentUser.id ? "login-user": "logout-user"} ${open &&
            "header-bg-index "}`}
        >
          <Toolbar className={`${classes.headerPadding} pt-0 flex-column`}>
            <Grid container className="mt-10">
              <Grid item sm={3}>
                <div className={`${classes.title} header-logo`}>
                  <Link to="/">
                    <img
                      src={setLogo().key}
                      alt="Logo"
                      className={`${classes.logoMobile} ${setLogo().className}`}
                    />
                  </Link>
                  <Link to="/" className="desktop-logo-size">
                    <img
                      src={setLogo()}
                      alt="Logo"
                      className={classes.logoDesktop}
                    />
                  </Link>
                </div>
              </Grid>

              <Grid
                item
                sm={6}
                className="justify-center header-search-main-div"
              >
                <OutsideClickHandler
                  display="contents"
                  onOutsideClick={() => isOpen(false)}
                >
                  <div className="width-100">
                    <div className="main-home-search">
                      <InputBase
                        placeholder="Search anything..."
                        inputProps={{ "aria-label": "search google maps" }}
                        className="search-input"
                        value={query}
                        onChange={e => setQuery(e.target.value)}
                        onClick={openSearchedResults}
                      />
                      <div
                        className="btn btn-primary btn-s mr--3 w-auto search-responsive"
                        onClick={searchHandler}
                      >
                        <i className="adam-search1"></i>
                      </div>
                    </div>
                    <span className="error-message position-absolute">
                      {!open && queryError}
                    </span>
                  </div>

                  {open && (
                    <SearchDropdown
                      query={query}
                      setRecentQuery={query => setQuery(query)}
                    />
                  )}
                </OutsideClickHandler>
              </Grid>

              <Grid item sm={3}>
                <div
                  className={`${classes.desktopMenu} desktopMenu-pt-0 float-right`}
                >
                  {isAuth && (
                    <MenuItem className="menu__help font-12 HeaderMenu-hover header-btn">
                      <Link
                        to="/"
                        className={`${classes.textStyleBlack} paddingRight-1`}
                      >
                        <IconButton className="help-button header-icons">
                          <i className="adam-chat-4"></i>
                        </IconButton>
                        <span className="col-gray link-hover-text">Live Support</span>
                      </Link>
                    </MenuItem>
                  )}
                  <MenuItem className="menu__help font-12 HeaderMenu-hover header-btn">
                    <Link
                      to="/help"
                      className={`${classes.textStyleBlack} paddingRight-1`}
                    >
                      <IconButton className="help-button header-icons">
                        <i className="adam-help"></i>
                      </IconButton>
                      <span className="col-gray link-hover-text">Help</span>
                    </Link>
                  </MenuItem>
                  <MenuItem className="menu__help font-12 currency-bg HeaderMenu-hover header-btn">
                    <Link to="/" className={classes.textStyleBlack}>
                      <div
                        className="btn btn-xs btn-light btn-outline-lights dropdown-toggle text-decoration-none header-small-font currencyBox"
                        to="#"
                        data-toggle="modal"
                        data-target="#currency-modal"
                      >
                        Currency
                      </div>
                    </Link>
                  </MenuItem>
                </div>
              </Grid>
            </Grid>

            <Grid container className="menu-marginTop">
              <div
                className={`d-flex manage-header ${currentUser &&
                  currentUser.id &&
                  "mb-2"}`}
              >
                {currentUser && currentUser.id ? (
                  <div className="d-flex pt-2">
                    <div className="header-location-img d-flex mt-1">
                      <MdLocationOn />
                    </div>
                    <div>
                      <div className="d-flex flex-column">
                        <span className="text-black ml-1 header-location-country gray-dark">
                          {country}
                        </span>
                        <span className="header-user-time text-black  header-location-time dark-silver">
                          {moment(new Date()).format("D MMM ddd hh:mm A")}
                        </span>
                      </div>
                    </div>
                  </div>
                ) : (
                    <div
                      to="/"
                      className={`${classes.textStyle} menu-hoverText location-header d-flex`}
                    >
                      <div className="header-icons cursor-pointer">
                        <i className="adam-navigation-main pr-5"></i>
                      </div>
                      Your Location
                    <span className="ml-1 f-500">{country}</span>
                    </div>
                  )}

                <div className="mange-right size-color">
                  {!isAuth && (
                    <div
                      className={`${classes.overflowUnset} header-btn dropdownButtonWidth appbarRegisterButton HeaderMenu-hover p-0`}
                    >
                      <div className="position-relative">
                        <RegisterUser setSticky={value => setSticky(value)} />
                      </div>
                    </div>
                  )}
                  {isAuth && (
                    <div className="menu__persons HeaderMenu-hover header-btn mr-3 cursor-pointer">
                      <div
                        onClick={() =>
                          isLoginOpen
                            ? setIsLoginOpen(false)
                            : isShowPopUp(true)
                        }
                      >
                        <UserConsumer>
                          {({ currentUser }) => (
                            <div className="d-flex align-items-center">
                              <div className="mr-2 header-user-image-div">
                                <img
                                  src={
                                    currentUser && currentUser.image
                                      ? currentUser.image
                                      : require("../../assets/images/header/user.png")
                                  }
                                  className="h-100 width-100"
                                />
                              </div>
                              <div className="ml-1 d-flex flex-column mr-2">
                                <span className="header-user-name gray-dark">
                                  {currentUser.firstName}
                                </span>
                                <span className="header-user-role dark-silver">
                                  {currentUser &&
                                    currentUser.role &&
                                    currentUser.role.role.toLowerCase()}
                                </span>

                              </div>
                              <div>
                                <IoIosArrowDown />
                              </div>
                            </div>
                          )}
                        </UserConsumer>
                      </div>
                    </div>
                  )}

                  {isAuth && (
                    <>
                      <div className="HeaderMenu-hover header-btn heart-1-li header-cart-icon cursor-pointer ml-2">
                        <Link to="/" className={`${classes.textStyle} d-flex`}>
                          <i className="adam-heart-1"></i>
                          <span className="cart-no">0</span>
                        </Link>
                      </div>
                      <div className="HeaderMenu-hover header-btn heart-1-li header-cart-icon cursor-pointer ml-3">
                        <Link to="/" className={`${classes.textStyle} d-flex`}>
                          <img
                            src={bellIcon}
                            className="width-height-icon-bell"
                          />
                          <span className="cart-no">1</span>
                        </Link>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </Grid>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
      </div>
      <OutsideClickHandler
        onOutsideClick={() => {
          isShowPopUp(false);
        }}
      >
        {isLoginOpen && (
          <div
            className="login-user-div p-4"
            onMouseLeave={e => {
              isShowPopUp(false);
            }}
          >
            {/* <h6>Your Market Title</h6> */}
            {isAuth && (
              <UserConsumer>
                {({ currentUser }) => (
                  <div className="d-flex align-items-center justify-content-between">
                    <div className=" d-flex flex-wrap align-items-center">
                      <span className="font-weight-500 mr-2 small-screen-login-user-title header-dropdown-user-name">
                        {currentUser.firstName}
                      </span>
                      <span className="login-user-div-text small-screen-login-user-title header-dropdown-user-role">
                        (
                      {currentUser &&
                          currentUser.role &&
                          currentUser.role.role.toLowerCase()}
                        )
                    </span>
                    </div>
                    <div className=" d-flex flex-wrap">
                      <span className=" font-weight-500 login-user-div-text small-screen-login-user-title header-dropdown-user-role mr-1">
                        Since
                      </span>
                      <span className=" font-weight-500 login-user-div-text small-screen-login-user-title header-dropdown-user-role">
                      {moment(currentUser && currentUser.createdAt).format("DD-MM-YYYY")} 
                      </span>
                    </div>
                  </div>
                )}
              </UserConsumer>
            )}
            <hr />
            <div className="mt-3 mb-3 d-flex flex-column">
              <span className="login-user-div-text font-weight-500 small-screen-login-user-info">
                My Account
              </span>
              <div className="pl-2 d-flex flex-column mt-1">
                <Link to="/dashboard" className="header-dropdown-link">
                  <span className="login-user-div-text login-user-div-text-link small-screen-login-user-info">
                    Dashboard
                  </span>
                </Link>
                {currentUser && currentUser.role && salesEngineAccessibleTypes.includes(currentUser.role.type) &&
                  <Link to="/sales-engines" className="header-dropdown-link">
                    <span className="login-user-div-text login-user-div-text-link small-screen-login-user-info">
                      Sales Engine
                    </span>
                  </Link>
                }
                <Link to="#" className="header-dropdown-link">
                  <span className="login-user-div-text login-user-div-text-link small-screen-login-user-info">
                    Account Setting
                  </span>
                </Link>
                <Link to="/change-password" className="header-dropdown-link">
                  <span className="login-user-div-text login-user-div-text-link small-screen-login-user-info">
                    Change Password
                  </span>
                </Link>
                <Link to="/logout" className="header-dropdown-link">
                  <span
                    className="login-user-div-text login-user-div-text-link cursor-pointer header-logout small-screen-login-user-info"
                    onClick={logout}
                  >
                    Logout
                  </span>
                </Link>
              </div>
            </div>
            <hr />
            {currentUser && currentUser.id && (
              <>
                <div className="mt-2 header-dropdown-link">
                  <span className="wrap-text login-user-notification-text small-screen-login-user-info">
                    This will manage your account status, if you turn off your account, your posted Ads will not
                    be display in your market and your profile will be hidden to be seen by your contact.
                  </span>
                </div>
                <div className="login-user-div-switch mt-4 d-flex align-items-center">
                  <div class="switchtoggle">
                    <input
                      type="checkbox"
                      checked={currentUser.accountActivated}
                      id="switch"
                      onClick={() =>
                        changeUserStatus({
                          id: currentUser.id,
                          column: "accountActivated",
                          value: currentUser.accountActivated
                        })
                      }
                    />
                    <label for="switch">Toggle</label>
                  </div>
                  <div className="pb-1">
                    <Link to="#" className="header-dropdown-link">
                      <span className="login-user-div-text login-user-div-text-link small-screen-login-user-info ml-2">
                        Turn off My Account
                      </span>
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
        )}
      </OutsideClickHandler>
    </>
  );
};

export default Header;
