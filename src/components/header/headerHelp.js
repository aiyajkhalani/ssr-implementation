import React from "react";
import "../footer/footer.scss";
import { Grid } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import { Link } from "react-router-dom";
import { Layout } from "../layout/layout";

class Help extends React.Component {

  render() {
    const { history } = this.props;
    return (
      <>
        <Layout>
          <div className="contactFooter-bg help-padding">
            <Container maxWidth="lg" className="header-title p-0">
              <div className="h-100 contact-title">
                <div className="help-title padding-title">
                  <h3 className="mb-0">Hello..!</h3>
                  <h5>What can we help you with?</h5>
                </div>
              </div>
            </Container>
            {/* </div> */}

            <div>
              <Container maxWidth="lg">
                <Grid container spacing={3}>
                  <Grid item xs className="border-help-section margin-help-section" >
                    <div className="p-10 position-relative">
                      <Grid container spacing={3}>
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/fraud-protection.png")}
                              alt="fraud-protection"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>Fraud Protection</h5>
                            <div className="d-flex flex-column help-description">
                              <span>Be safe</span>
                              <span>General guidelines and precautions</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs className="border-help-section margin-help-section">
                    <div className="p-10 position-relative " onClick={() => history && history.push("/user-guide") }>
                      <Grid container spacing={3} className="cursor-pointer">
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/user-guide.png")}
                              alt="User Guide"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>User Guide</h5>
                            <div className="d-flex flex-column help-description">
                              <span>Processes explained</span>
                              <span>Clear your doubts</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs className="border-help-section margin-help-section">
                    <div className="p-10 position-relative">
                      <Grid container spacing={3}>
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/downloads.png")}
                              alt="Downloads"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>Downloads</h5>
                            <div className="d-flex flex-column help-description">
                              <span>Resources</span>
                              <span>Guidelines & FAQs</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item xs className="border-help-section margin-help-section">
                    <div className="p-10 position-relative">
                      <Grid container spacing={3}>
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/buying-process.png")}
                              alt="Buying Process"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>Buying Process</h5>
                            <div className="d-flex flex-column help-description">
                              <span>Step by step instructions</span>
                              <span>Detailed explanations</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs className="border-help-section margin-help-section">
                    <div className="p-10 position-relative">
                      <Grid container spacing={3}>
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/selling-process.png")}
                              alt="Selling"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>Selling process</h5>
                            <div className="d-flex flex-column help-description">
                              <span>Selling process simplified</span>
                              <span>Steps explained</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs className="border-help-section margin-help-section">
                    <div className="p-10 position-relative" onClick={() => history && history.push("/faq") }>
                      <Grid container spacing={3} className="cursor-pointer">
                        <Grid item sm={3}>
                          <div className="helper-img">
                            <img
                              src={require("../../assets/images/help/faqs.png")}
                              alt="FAQ"
                              className="help-image-height width-100"
                            />
                          </div>
                        </Grid>
                        <Grid item sm={9}>
                          <div>
                            <h5>FAQs</h5>
                            <div className="d-flex flex-column help-description">
                              <span>All your quesitons answered</span>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                </Grid>
              </Container>
            </div>
          </div>
        </Layout>
      </>
    );
  }
}

export default Help;
