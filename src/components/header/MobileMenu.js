import React, { useState } from "react";
import OutsideClickHandler from 'react-outside-click-handler';
import { Grid, Button, Typography, Box, List, ListItem, IconButton } from "@material-ui/core";
import './header.scss';
import MoreIcon from '@material-ui/icons/MoreVert';
import RegisterUser from "../popUp/RegisterUser";
import { Link } from "react-router-dom";


 
function MobileMenu(props) {
    function isShowPopUp(isOpen) {
        setIsOpen(isOpen)
    }
    const [isOpen, setIsOpen] = React.useState(false);
  return (
      
       <OutsideClickHandler
      
      onOutsideClick={() => {
        isShowPopUp(false)
      }}
    >
   
    <IconButton
              aria-label="show more"
              aria-haspopup="true"
              onClick={() => isOpen ? setIsOpen(false) : isShowPopUp(true) }
              color="inherit"
              className="mobile__menu-color d-sm-icon-none"
            >
              <MoreIcon />
            </IconButton>
   
      { isOpen &&  
      <div className="mobile-login-dropdown">
          <List>
                <ListItem><Link to="#">Charter & Rent</Link></ListItem>
                <ListItem><Link to="#">Service & Maintenance</Link></ListItem>
                <ListItem><Link to="#">Marina & Storage</Link></ListItem>
                <ListItem><Link to="#">Boat Show</Link></ListItem>
                <ListItem className="register-component"><RegisterUser/></ListItem>
          </List>
      </div>
      }
    </OutsideClickHandler>
  );
}
export default MobileMenu