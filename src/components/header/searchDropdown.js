import React, { useContext, useState, useEffect } from "react";
import uuid from "uuid/v4";

import UserContext from "../../UserContext";

import { viewBoatHandler, viewRentBoatHandler, viewMarinaDetails } from "../../helpers/boatHelper";
import { marketTypes } from "../../util/enums/enums";
import "./header.scss";
import "../../styles/headerResponsive.scss";

const SearchDropdown = props => {
  const { query, setRecentQuery } = props;

  const { history, recentSearchResults, multipleSearchResults } = useContext(
    UserContext
  );

  function isShowPopUp(isOpen) {
    setIsOpen(isOpen);
  }

  const [isResult, setIsResult] = useState(false);
  const [displayedBoats, setDisplayedBoats] = useState(null);
  const [selectedMarketType, setSelectedMarketType] = useState(marketTypes.BOAT);
  const [isOpen, setIsOpen] = React.useState(true);

  useEffect(() => {
    if (multipleSearchResults) {
      const {
        boats,
        marinaStorage,
        rentBoats,
        yachtService
      } = multipleSearchResults;
      if (
        (boats && boats.length) ||
        (marinaStorage && marinaStorage.length) ||
        (rentBoats && rentBoats.length) ||
        (yachtService && yachtService.length)
      ) {
        setIsResult(true);
        setDisplayedBoats(boats);
      } else if (recentSearchResults) {
        setIsResult(false);
      }
    }
  }, [multipleSearchResults, recentSearchResults]);

  // See All Results
  const showAllResultHandler = e => {
    e.preventDefault();
    history.push({
      pathname: "/search-result",
    });
  };

  const setQueryResultHandler = (item) => {

    setRecentQuery(item.query)

    displayBoatHandler(item.results)
  }

  const typeWiseResultHandler = (type) => {

    setSelectedMarketType(type)

    displayBoatHandler(multipleSearchResults, type)
  }

  const displayBoatHandler = (data = { boats: [], marinaStorage: [], rentBoats: [], yachtService: [] },
    type = selectedMarketType) => {
    let results = []

    const { boats, marinaStorage, rentBoats, yachtService } = data;

    switch (type) {
      case marketTypes.BOAT:
        results = (boats && boats.length > 0) ? results.concat(boats) : results
        break
      case marketTypes.RENT_BOAT:
        results = (rentBoats && rentBoats.length > 0) ? results.concat(rentBoats) : []
        break
      case marketTypes.YACHT_SERVICE:
        results = (yachtService && yachtService.length > 0) ? results.concat(yachtService) : []
        break
      case marketTypes.MARINA_AND_STORAGE:
        results = (marinaStorage && marinaStorage.length > 0) ? results.concat(marinaStorage) : []
        break

      default:
        break
    }

    setDisplayedBoats(results)
    return
  }

  const viewHandler = (item) => {

    switch (selectedMarketType) {
      case marketTypes.BOAT:
        viewBoatHandler(item, history)
        break
      case marketTypes.RENT_BOAT:
        viewRentBoatHandler(item, history)
        break
      case marketTypes.MARINA_AND_STORAGE:
        viewMarinaDetails(item.id, history)
        break

      default:
        break
    }
  }

  const buttonClassHandler = type => type === selectedMarketType ? "btn-primary" : "btn-outline-primary"

  return (
    (isOpen &&  (
    <div className="dropdownContainer search-boat-header main-search-dropdown" 
    onMouseLeave={e => {
      isShowPopUp(false);
    }}
    >
      {isResult && (
        <div className="col-12">
          <h5 className="title-text mt-2 main-search-dropdown-title">Explore Adamsea</h5>
          <div className="d-flex flex-row flex-wrap">
            <button type="button"
              className={`btn my-2 mr-2 explore-boat-options-dropdown ${buttonClassHandler(marketTypes.BOAT)}`}
              onClick={() => typeWiseResultHandler(marketTypes.BOAT)}>
              Home
            </button>
            <button type="button"
              className={`btn my-2 mr-2 explore-boat-options-dropdown ${buttonClassHandler(marketTypes.RENT_BOAT)}`}
              onClick={() => typeWiseResultHandler(marketTypes.RENT_BOAT)}>
              Rent
            </button>
            <button type="button"
              className={`btn my-2 mr-2 explore-boat-options-dropdown ${buttonClassHandler(marketTypes.YACHT_SERVICE)}`}
              onClick={() => typeWiseResultHandler(marketTypes.YACHT_SERVICE)}>
              Boat Service
            </button>
            <button type="button"
              className={`btn my-2 mr-2 explore-boat-options-dropdown ${buttonClassHandler(marketTypes.MARINA_AND_STORAGE)}`}
              onClick={() => typeWiseResultHandler(marketTypes.MARINA_AND_STORAGE)}>
              Marina & Storage
            </button>
          </div>

          <>
            <h4 className="title-text mt-2 mb-2 f-16 main-search-dropdown-result-title main-search-dropdown-result-margin">Results</h4>
            <div>
              {displayedBoats && displayedBoats.length > 0
                ?
                <div>
                  <div className="header-search-div">
                    {displayedBoats.slice(0, 5).map(boat => (
                      <div
                        className="cityTitle d-flex flex-row mb-3 search-dropdown-div-margin"
                        key={boat.id}
                        onClick={() => viewHandler(boat)}
                      >
                        <div className="header-search-dropdown-image">
                          <img height="55px" width="55px" src={boat.images ? boat.images[0] : boat.image[0] || boat.image} alt="icon"
                            className="mr-2 header-search-dropdown" />
                        </div>
                        <div className="d-flex flex-column justify-content-center">
                          <span className="mt-0 home-boat-name-font f-15 gray-dark search-dropdown-boat-name">
                            {boat.boatName}
                          </span>
                          <span className="mt-0 home-boat-name-font font13 dark-silver search-dropdown-boat-country">
                            {boat.city || boat.tripCity} | {boat.country || boat.tripCountry} | {selectedMarketType}
                          </span>
                        </div>
                      </div>
                    ))}
                  </div>
                  <a href="#" onClick={showAllResultHandler} className="search-dropdown-see-all-btn">
                    see all
                </a>
                </div>
                : <span className="image-location mt-0 home-boat-name-font main-search-dropdown-result-title">No Results</span>
              }
            </div>
          </>
        </div>
      )
      }

      <div className="result-list search-dropdown-result-list small-screen-pl-15">
        <h5 className="mt-2 title-text f-16 main-search-dropdown-result-title main-search-dropdown-result-title-margin">Recent Search</h5>

        {recentSearchResults && recentSearchResults.length > 0 &&
          recentSearchResults.map(item => (
            <p
              className="cityTitle font-14 dark-silver mb-2 search-dropdown-recent-search"
              key={uuid()}
              onClick={() => setQueryResultHandler(item)}
            >
              {item.query}
            </p>
          ))}
      </div>

    </div >
      )
    )
  );
};

export default SearchDropdown;
