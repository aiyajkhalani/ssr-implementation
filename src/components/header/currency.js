const currency = [
  {
    value: "USD",
    label: "United States Dollar (USD)"
  },
  {
    value: "GBP",
    label: "British Pound (GBP)"
  },
  {
    value: "AFN",
    label: "Afghanistan Afghani (AFN)"
  },
  {
    value: "ALL",
    label: "Albanian Lek (ALL)"
  },
  {
    value: "DZD",
    label: "Algerian Dinar (DZD)"
  },
  {
    value: "ADP",
    label: "Andorran Peseta (ADP)"
  },
  {
    value: "AOA",
    label: "Angolan Kwanza (AOA)"
  },
  {
    value: "XCD",
    label: "Anguillan Dollar (XCD)"
  },
  {
    value: "XCD",
    label: "Antiguan & Barbudan Dollar (XCD)"
  },
  {
    value: "ARS",
    label: "Argentine Peso (ARS)"
  },
  {
    value: "AMD",
    label: "Armenian Dram (AMD)"
  },
  {
    value: "AWG",
    label: "Aruban Guilder (AWG)"
  },
  {
    value: "AUD",
    label: "Australian Dollar (AUD)"
  },
  {
    value: "AZN",
    label: "Azerbaijani New Manat (AZN)"
  },
  {
    value: "BSD",
    label: "Bahamas Dollar (BSD)"
  },
  {
    value: "BHD",
    label: "Bahraini Dinar (BHD)"
  },
  {
    value: "BDT",
    label: "Bangladesh Taka (BDT)"
  },
  {
    value: "BBD",
    label: "Barbadian Dollar (BBD)"
  },
  {
    value: "BYN",
    label: "Belarusian Ruble (BYN)"
  },
  {
    value: "BZD",
    label: "Belizean Dollar (BZD)"
  },
  {
    value: "XOF",
    label: "Benin Franc (XOF)"
  },
  {
    value: "BMD",
    label: "Bermudian Dollar (BMD)"
  },
  {
    value: "BTN",
    label: "Bhutan Ngultrum (BTN)"
  },
  {
    value: "BOB",
    label: "Bolivian Boliviano (BOB)"
  },
  {
    value: "BAM",
    label: "Bosnia & Herzegovinan Marka (BAM)"
  },
  {
    value: "BWP",
    label: "Botswanan Pula (BWP)"
  },
  {
    value: "BRL",
    label: "Brazilian Real (BRL)"
  },
  {
    value: "GBP",
    label: "British Indian Ocean Pound (GBP)"
  },
  {
    value: "GBP",
    label: "British Pound (GBP)"
  },
  {
    value: "BND",
    label: "Brunei Dollar (BND)"
  },
  {
    value: "BGN",
    label: "Bulgarian Lev (BGN)"
  },
  {
    value: "XOF",
    label: "Burkina Faso Franc (XOF)"
  },
  {
    value: "BIF",
    label: "Burundi Franc (BIF)"
  },
  {
    value: "KHR",
    label: "Cambodian Riel (KHR)"
  },
  {
    value: "XAF",
    label: "Cameroon Franc (XAF)"
  },
  {
    value: "CAD",
    label: "Canadian Dollar (CAD)"
  },
  {
    value: "EUR",
    label: "Canary Islands Euro (EUR)"
  },
  {
    value: "GBP",
    label: "Canton & Enderbury Is. Pound (GBP)"
  },
  {
    value: "CVE",
    label: "Cape Verde Escudo (CVE)"
  },
  {
    value: "KYD",
    label: "Cayman Islands Dollar (KYD)"
  },
  {
    value: "XAF",
    label: "Central African Republic Franc (XAF)"
  },
  {
    value: "XAF",
    label: "CFA Franc BEAC (XAF)"
  },
  {
    value: "XAF",
    label: "Chad Franc (XAF)"
  },
  {
    value: "CLP",
    label: "Chilean Peso (CLP)"
  },
  {
    value: "CNY",
    label: "Chinese Renminbi (CNY)"
  },
  {
    value: "AUD",
    label: "Christmas Island Dollar (AUD)"
  },
  {
    value: "AUD",
    label: "Cocos (Keeling) Is. Dollar (AUD)"
  },
  {
    value: "COP",
    label: "Colombian Peso (COP)"
  },
  {
    value: "KMF",
    label: "Comoran Franc (KMF)"
  },
  {
    value: "XAF",
    label: "Congo Republic Franc (XAF)"
  },
  {
    value: "CDF",
    label: "Congolese Franc (CDF)"
  },
  {
    value: "NZD",
    label: "Cook Islands Dollar (NZD)"
  },
  {
    value: "CRC",
    label: "Costa Rican Colon (CRC)"
  },
  {
    value: "HRK",
    label: "Croatian Kuna (HRK)"
  },
  {
    value: "CUP",
    label: "Cuban Peso (CUP)"
  },
  {
    value: "CZK",
    label: "Czech Koruna (CZK)"
  },
  {
    value: "DKK",
    label: "Danish Krone (DKK)"
  },
  {
    value: "DJF",
    label: "Djiboutian Franc (DJF)"
  },
  {
    value: "XCD",
    label: "Dominica Dollar (XCD)"
  },
  {
    value: "DOP",
    label: "Dominican Republic Peso (DOP)"
  },
  {
    value: "XCD",
    label: "East Caribbean Dollar (XCD)"
  },
  {
    value: "USD",
    label: "East Timor Dollar (USD)"
  },
  {
    value: "ECS",
    label: "Ecuadorean Sucre (ECS)"
  },
  {
    value: "EGP",
    label: "Egyptian Pound (EGP)"
  },
  {
    value: "SVC",
    label: "El Salvador Colon (SVC)"
  },
  {
    value: "XAF",
    label: "Equatorial Guinea Franc (XAF)"
  },
  {
    value: "ERN",
    label: "Eritrean Nakfa (ERN)"
  },
  {
    value: "ETB",
    label: "Ethiopian Birr (ETB)"
  },
  {
    value: "EUR",
    label: "Euro (EUR)"
  },
  {
    value: "FKP",
    label: "Falkland Islands Pound (FKP)"
  },
  {
    value: "DKK",
    label: "Faroe Islands Krone (DKK)"
  },
  {
    value: "FJD",
    label: "Fijian Dollar (FJD)"
  },
  {
    value: "XPF",
    label: "French Polynesian Franc (XPF)"
  },
  {
    value: "XAF",
    label: "Gabon Franc (XAF)"
  },
  {
    value: "GMD",
    label: "Gambian Dalasi (GMD)"
  },
  {
    value: "GEL",
    label: "Georgian Lari (GEL)"
  },
  {
    value: "GIP",
    label: "Gibraltar Pound (GIP)"
  },
  {
    value: "DKK",
    label: "Greenland Krone (DKK)"
  },
  {
    value: "XCD",
    label: "Grenadan Dollar (XCD)"
  },
  {
    value: "USD",
    label: "Guam Dollar (USD)"
  },
  {
    value: "GTQ",
    label: "Guatemalan Quetzal (GTQ)"
  },
  {
    value: "GBP",
    label: "Guernsey Pound (GBP)"
  },
  {
    value: "XOF",
    label: "Guinea-Bissau Franc (XOF)"
  },
  {
    value: "GNF",
    label: "Guinean Franc (GNF)"
  },
  {
    value: "GYD",
    label: "Guyanese Dollar (GYD)"
  },
  {
    value: "HTG",
    label: "Haitian Gourde (HTG)"
  },
  {
    value: "HNL",
    label: "Honduras Lempira (HNL)"
  },
  {
    value: "HKD",
    label: "Hong Kong Dollar (HKD)"
  },
  {
    value: "HUF",
    label: "Hungarian Forint (HUF)"
  },
  {
    value: "ISK",
    label: "Iceland Krona (ISK)"
  },
  {
    value: "INR",
    label: "Indian Rupee (INR)"
  },
  {
    value: "IDR",
    label: "Indonesian Rupiah (IDR)"
  },
  {
    value: "IRR",
    label: "Iranian Rial (IRR)"
  },
  {
    value: "IQD",
    label: "Iraqi Dinar (IQD)"
  },
  {
    value: "IEP",
    label: "Irish Punt (IEP)"
  },
  {
    value: "GBP",
    label: "Isle of Man Pound (GBP)"
  },
  {
    value: "ILS",
    label: "Israeli Shekel (ILS)"
  },
  {
    value: "XOF",
    label: "Ivory Coast Franc (XOF)"
  },
  {
    value: "JMD",
    label: "Jamaican Dollar (JMD)"
  },
  {
    value: "JPY",
    label: "Japanese Yen (JPY)"
  },
  {
    value: "JEP",
    label: "Jersey Pound (JEP)"
  },
  {
    value: "USD",
    label: "Johnston Atoll Dollar (USD)"
  },
  {
    value: "JOD",
    label: "Jordanian Dinar (JOD)"
  },
  {
    value: "KZT",
    label: "Kazakhstan Tenge (KZT)"
  },
  {
    value: "KES",
    label: "Kenyan Shilling (KES)"
  },
  {
    value: "AUD",
    label: "Kiribati Dollar (AUD)"
  },
  {
    value: "KWD",
    label: "Kuwaiti Dinar (KWD)"
  },
  {
    value: "KGS",
    label: "Kyrgyzstan Som (KGS)"
  },
  {
    value: "LAK",
    label: "Laotian Kip (LAK)"
  },
  {
    value: "LBP",
    label: "Lebanese Pound (LBP)"
  },
  {
    value: "LSL",
    label: "Lesotho Loti (LSL)"
  },
  {
    value: "LRD",
    label: "Liberian Dollar (LRD)"
  },
  {
    value: "LYD",
    label: "Libyan Dinar (LYD)"
  },
  {
    value: "CHF",
    label: "Liechtenstein Franc (CHF)"
  },
  {
    value: "MOP",
    label: "Macau Pataca (MOP)"
  },
  {
    value: "MKD",
    label: "Macedonian Denar (MKD)"
  },
  {
    value: "MGA",
    label: "Madagascar Ariary (MGA)"
  },
  {
    value: "MWK",
    label: "Malawian Kwacha (MWK)"
  },
  {
    value: "MYR",
    label: "Malaysian Ringgit (MYR)"
  },
  {
    value: "MVR",
    label: "Maldives Rufiyaa (MVR)"
  },
  {
    value: "XOF",
    label: "Mali Republic Franc (XOF)"
  },
  {
    value: "EUR",
    label: "Martinique Euro (EUR)"
  },
  {
    value: "MRO",
    label: "Mauritanian Ouguiya (MRO)"
  },
  {
    value: "MUR",
    label: "Mauritian Rupee (MUR)"
  },
  {
    value: "EUR",
    label: "Mayotte Euro (EUR)"
  },
  {
    value: "MXN",
    label: "Mexican Peso (MXN)"
  },
  {
    value: "USD",
    label: "Micronesian Dollar (USD)"
  },
  {
    value: "USD",
    label: "Midway Islands Dollar (USD)"
  },
  {
    value: "MDL",
    label: "Moldovan Leu (MDL)"
  },
  {
    value: "EUR",
    label: "Monaco Euro (EUR)"
  },
  {
    value: "MNT",
    label: "Mongolian Tugrik (MNT)"
  },
  {
    value: "EUR",
    label: "Montenegro Euro (EUR)"
  },
  {
    value: "XCD",
    label: "Montserrat Dollar (XCD)"
  },
  {
    value: "MAD",
    label: "Moroccan Dirham (MAD)"
  },
  {
    value: "MZN",
    label: "Mozambique New Metical (MZN)"
  },
  {
    value: "MMK",
    label: "Myanmar Kyat (MMK)"
  },
  {
    value: "NAD",
    label: "Namibian Dollar (NAD)"
  },
  {
    value: "AUD",
    label: "Nauru Dollar (AUD)"
  },
  {
    value: "NPR",
    label: "Nepalese Rupee (NPR)"
  },
  {
    value: "ANG",
    label: "Netherlands Antillean Guilder (ANG)"
  },
  {
    value: "GHS",
    label: "New Ghana Cedi (GHS)"
  },
  {
    value: "SDG",
    label: "New Sudanese Pound (SDG)"
  },
  {
    value: "NZD",
    label: "New Zealand Dollar (NZD)"
  },
  {
    value: "NIO",
    label: "Nicaraguan Cordoba (NIO)"
  },
  {
    value: "XOF",
    label: "Niger Republic Franc (XOF)"
  },
  {
    value: "NGN",
    label: "Nigerian Naira (NGN)"
  },
  {
    value: "NZD",
    label: "Niue Dollar (NZD)"
  },
  {
    value: "AUD",
    label: "Norfolk Island Dollar (AUD)"
  },
  {
    value: "KPW",
    label: "North Korean Won (KPW)"
  },
  {
    value: "USD",
    label: "Northern Mariana Is. Dollar (USD)"
  },
  {
    value: "NOK",
    label: "Norwegian Krone (NOK)"
  },
  {
    value: "CNH",
    label: "Offshore Chinese Renminbi (CNH)"
  },
  {
    value: "OMR",
    label: "Omani Rial (OMR)"
  },
  {
    value: "PKR",
    label: "Pakistani Rupee (PKR)"
  },
  {
    value: "USD",
    label: "Palau Dollar (USD)"
  },
  {
    value: "PAB",
    label: "Panamanian Balboa (PAB)"
  },
  {
    value: "PGK",
    label: "Papua New Guinea Kina (PGK)"
  },
  {
    value: "PYG",
    label: "Paraguay Guarani (PYG)"
  },
  {
    value: "PEN",
    label: "Peruvian New Sol (PEN)"
  },
  {
    value: "PHP",
    label: "Philippine Peso (PHP)"
  },
  {
    value: "NZD",
    label: "Pitcairn Islands Dollar (NZD)"
  },
  {
    value: "PLN",
    label: "Polish Zloty (PLN)"
  },
  {
    value: "USD",
    label: "Puerto Rican Dollar (USD)"
  },
  {
    value: "QAR",
    label: "Qatari Rial (QAR)"
  },
  {
    value: "RON",
    label: "Romanian Leu (RON)"
  },
  {
    value: "RUB",
    label: "Russian Ruble (RUB)"
  },
  {
    value: "RWF",
    label: "Rwandan Franc (RWF)"
  },
  {
    value: "EUR",
    label: "Saint Pierre & Miquelon Euro (EUR)"
  },
  {
    value: "EUR",
    label: "San Marino Euro (EUR)"
  },
  {
    value: "STD",
    label: "Sao Tome & Principe Dobra (STD)"
  },
  {
    value: "SAR",
    label: "Saudi Riyal (SAR)"
  },
  {
    value: "XOF",
    label: "Senegalese Franc (XOF)"
  },
  {
    value: "RSD",
    label: "Serbian Dinar (RSD)"
  },
  {
    value: "SCR",
    label: "Seychelles Rupee (SCR)"
  },
  {
    value: "SLL",
    label: "Sierra Leone Leone (SLL)"
  },
  {
    value: "SGD",
    label: "Singapore Dollar (SGD)"
  },
  {
    value: "SBD",
    label: "Solomon Islands Dollar (SBD)"
  },
  {
    value: "SOS",
    label: "Somali Shilling (SOS)"
  },
  {
    value: "ZAR",
    label: "South African Rand (ZAR)"
  },
  {
    value: "KRW",
    label: "South Korean Won (KRW)"
  },
  {
    value: "LKR",
    label: "Sri Lankan Rupee (LKR)"
  },
  {
    value: "SHP",
    label: "St. Helena Pound (SHP)"
  },
  {
    value: "XCD",
    label: "St. Kitts & Nevis Dollar (XCD)"
  },
  {
    value: "XCD",
    label: "St. Lucia Dollar (XCD)"
  },
  {
    value: "XCD",
    label: "St. Vincent Dollar (XCD)"
  },
  {
    value: "SDD",
    label: "Sudanese Dinar (SDD)"
  },
  {
    value: "SRD",
    label: "Suriname Dollar (SRD)"
  },
  {
    value: "SZL",
    label: "Swaziland Lilangeni (SZL)"
  },
  {
    value: "SEK",
    label: "Swedish Krona (SEK)"
  },
  {
    value: "CHF",
    label: "Swiss Franc (CHF)"
  },
  {
    value: "SYP",
    label: "Syrian Pound (SYP)"
  },
  {
    value: "TWD",
    label: "Taiwanese Dollar (TWD)"
  },
  {
    value: "TJS",
    label: "Tajikistani Somoni (TJS)"
  },
  {
    value: "TZS",
    label: "Tanzanian Shilling (TZS)"
  },
  {
    value: "THB",
    label: "Thai Baht (THB)"
  },
  {
    value: "THO",
    label: "Thai Baht Onshore (THO)"
  },
  {
    value: "XOF",
    label: "Togo Republic Franc (XOF)"
  },
  {
    value: "NZD",
    label: "Tokelau Dollar (NZD)"
  },
  {
    value: "TOP",
    label: "Tonga Paanga (TOP)"
  },
  {
    value: "TTD",
    label: "Trinidad & Tobago Dollar (TTD)"
  },
  {
    value: "TND",
    label: "Tunisian Dinar (TND)"
  },
  {
    value: "TRY",
    label: "Turkish New Lira (TRY)"
  },
  {
    value: "TRL",
    label: "Turkish Old Lira (TRL)"
  },
  {
    value: "TMT",
    label: "Turkmenistan Manat (TMT)"
  },
  {
    value: "USD",
    label: "Turks & Caicos Dollar (USD)"
  },
  {
    value: "AUD",
    label: "Tuvalu Dollar (AUD)"
  },
  {
    value: "AED",
    label: "UAE Dirham (AED)"
  },
  {
    value: "UGX",
    label: "Ugandan Shilling (UGX)"
  },
  {
    value: "UAH",
    label: "Ukrainian Hryvnia (UAH)"
  },
  {
    value: "UYU",
    label: "Uruguayan Peso (UYU)"
  },
  {
    value: "UZS",
    label: "Uzbekistani Soum (UZS)"
  },
  {
    value: "VUV",
    label: "Vanuatu Vatu (VUV)"
  },
  {
    value: "EUR",
    label: "Vatican Euro (EUR)"
  },
  {
    value: "VEE",
    label: "Venezuela Bolivar Essentials (VEE)"
  },
  {
    value: "VEF",
    label: "Venezuelan Bolivar Fuerte (VEF)"
  },
  {
    value: "VND",
    label: "Vietnamese Dong (VND)"
  },
  {
    value: "WST",
    label: "West Samoan Tala (WST)"
  },
  {
    value: "YER",
    label: "Yemeni Rial (YER)"
  },
  {
    value: "ZMK",
    label: "Zambian Kwacha (ZMK)"
  },
  {
    value: "ZWL",
    label: "Zimbabwean Dollar (ZWL)"
  },
];

export {currency};