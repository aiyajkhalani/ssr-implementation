import React, { useContext, useState } from 'react';
import ReactTable from 'react-table';

import UserContext from '../../UserContext';

import '../../styles/common.scss';
import { Modal, Button } from 'react-bootstrap';

export const TableCard = props => {

    const { title, button, columns, data, noButton, width } = props
    const { history } = useContext(UserContext)

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const redirectHandler = () => {
        if (!noButton && history && button.url) {
            history.push(`/${button.url}`)
        }
    }

    return (
        <>
            <div className="row m-0">
                <div className="col-12">
                    <div className="card">
                        <div className="card-header article-header sales-engin-header">
                            <h4 className="card-title">{title}</h4>
                            {!noButton && <button
                                type="button"
                                class="btn btn-primary primary-button"
                                onClick={redirectHandler}
                            >
                                {button.name}
                            </button>}
                        </div>
                        <div className="cursor-pointer mt-3 mb-0 d-flex justify-content-end pr-4" onClick={handleShow}>
                            <span className="navy-blue"> Do you need help!</span>
                        </div>
                        <Modal show={show} onHide={handleClose} className="sales-engine-header-help-dialog">
                            <Modal.Header closeButton>
                                <Modal.Title>Help</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                 eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
                                 ad minim veniam, quis nostrud exercitation ullamco </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose} className="sales-engine-header-help-dialog-btn">
                                    {'Close'}
                                </Button>
                               
                            </Modal.Footer>
                        </Modal>
                        <div className="card-body mt-0">
                            <div className="table-responsive">
                                {data && (
                                    <ReactTable
                                        columns={columns}
                                        data={data}
                                        defaultFilterMethod={(filter, row) =>
                                            String(row[filter.id]).includes(filter.value)
                                        }
                                        className="-striped -highlight"
                                        style={{
                                            width: width
                                          }}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

TableCard.defaultProps = {
    title: "",
    button: { name: "", url: "" },
    columns: [],
    data: [],
    noButton: false
};