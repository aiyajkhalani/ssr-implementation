export const customerService ={
    title: 'CUSTOMER SERVICE',
    data:  [
        {
            linkName: "Call Me"
        },
        {
            linkName: "Feedback"
        },
        {
            linkName: "Advertise with us"
        },
        {
            linkName: "Partner Access"
        },
        {
            linkName: "News & Multimedia"
        },
        {
            linkName: "Partner Relation"
        },
        {
            linkName: "Careers"
        },
        {
            linkName: "Suggestions"
        },
        {
            linkName: "Customer Service"
        },
        {
            linkName: "Submit Complaint"
        },
        {
            linkName: "Contact Us"
        },
    ]
}
export const certifyBoat ={
    title: 'CERTIFY YOUR BOAT',
    data:  [
        {
            linkName: "What Is Certification"
        },
        {
            linkName: "Why Do I Need It"
        },
        {
            linkName: "Types Of Inspection"
        },
        {
            linkName: "Inspection Points"
        },
        {
            linkName: "Inspect My Boat"
        },
        {
            linkName: "Sample"
        },
        {
            linkName: "Rate"
        },
        {
            linkName: "Be Aware"
        },
        {
            linkName: "How We Do It"
        },
        {
            linkName: "Who Will Do It"
        },
    ]
}
export const boatForSale ={
    title: 'BOAT FOR SALE',
    data:  [
        {
            linkName: "Recently Added Boats"
        },
        {
            linkName: "Listed By Broker & Dealer"
        },
        {
            linkName: "Listed By Boat Builder"
        },
        {
            linkName: "Must Buy"
        },
        {
            linkName: "Best Buy"
        },
        {
            linkName: "Most Viewed"
        },
        {
            linkName: "Auction Rooms"
        },
        {
            linkName: "All Boats List"
        },
        {
            linkName: "Top 20 Used Boats"
        },
    ]
}
export const partnership ={
    title: 'PARTNERSHIP',
    data:  [
        {
            linkName: "Be a Partner"
        },
        {
            linkName: "Shipper"
        },
        {
            linkName: "Escrow"
        },
        {
            linkName: "Manufacturer"
        },
        {
            linkName: "Trading With Us"
        },
        {
            linkName: "Surveyor"
        },
    ]
}
export const adamsea ={
    title: 'AdamSea',
    data:  [
        {
            linkName: "Our Market"
        },
        {
            linkName: "Policy"
        },
        {
            linkName: "Leadership"
        },
        {
            linkName: "What We Do"
        },
        {
            linkName: "Our Process"
        },
        {
            linkName: "Integrity & Compliance"
        },
        {
            linkName: "Social Media"
        },
        {
            linkName: "About Us"
        },
    ]
}
export const resources ={
    title: 'TIPS & RESOURCES',
    data:  [
        {
            linkName: "Buying Guide"
        },
        {
            linkName: "Selling Tips"
        },
        {
            linkName: "Buying Checklist"
        },
        {
            linkName: "Reviews"
        },
        {
            linkName: "Buying Tips"
        },
        {
            linkName: "Boat Selection"
        },
        {
            linkName: "Buying Process"
        },
    ]
}
export const sellBoat ={
    title: 'SELL YOUR BOAT',
    data:  [
        {
            linkName: "Single Seller"
        },
        {
            linkName: "Broker"
        },
        {
            linkName: "Boat builder & Manufacture"
        },
        {
            linkName: "Selling Process"
        },
    ]
}
export const boatShow ={
    title: 'WORLD BOAT SHOW',
    data:  [
        {
            linkName: "Boats Show List"
        },
        {
            linkName: "Add Boat Show"
        },
    ]
}
export const newBuild ={
    title: 'NEW BUILD',
    data:  [
        {
            linkName: "Buy New Boat"
        },
        {
            linkName: "Choose New Boat"
        },
    ]
}