import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import uuid from 'uuid/v1'

class FooterLink extends React.Component {
    render() {
        const { List: footerLinkData } = this.props

        return (
            <List component="nav" aria-label="main mailbox folders" className="footer-icon footer-div-align">
                <h2>{footerLinkData.title}</h2>

                <ul className="footer-link">
                    {
                        footerLinkData.data.map((item) => {
                            return (
                                <ListItem key={uuid()} component='a' href='/'>
                                    <ListItemIcon><ChevronRight /></ListItemIcon>
                                    <ListItemText primary={item.linkName} />
                                </ListItem>)
                        })
                    }
                </ul>
            </List>

        )
    }
}

export default FooterLink