import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import "./footer.scss";
import {
  getAllPageInfoByType,
  clearPageInfoByTypeFlag
} from "../../redux/actions/pageInfoByTypeAction";
import { footerLinks } from "../../util/enums/enums";

class Footer extends Component {
  state = {
    privacyModal: false
  };

  static getDerivedStateFromProps(props, prevState) {
    const { success, clearPageInfoByTypeFlag } = props;

    if (success) {
      clearPageInfoByTypeFlag();
      return {
        privacyModal: true
      };
    }
    //  else {
    //   return {
    //     privacyModal: false
    //   };
    // }

    return null;
  }

  createMarkup = data => {
    return { __html: data };
  };

  privacyPolicyHandler = () => {
    this.setState(prevState => ({
      privacyModal: !prevState.privacyModal
    }));
  };

  getPageByTitle = async getPageByTitle => {
    const { getAllPageInfoByType } = this.props;
    await getAllPageInfoByType({ title: getPageByTitle });
  };

  render() {
    const { privacyModal } = this.state;
    const { pageInfoByType } = this.props;

    return (
      <>
        <Grid container className="bg-gray mt-30 main-footer">
          <div className="width-70 d-flex m-auto pt-4 pb-4">
            <Grid item className="footer-links-div pl-3 pr-3 mb-3">
              <div className="footer-title mt-4 gray-dark footer-small-screen-font">Quick Sale</div>

              <ul className="footer-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Recently Added Boats
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Listed By Broker &amp; Dealer
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Must Buy
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Most Viewed
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Auction Rooms
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    All Boats List
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Top 20 Used Boats
                  </Link>
                </li>
              </ul>
            </Grid>

            <Grid item className="footer-links-div pl-3 pr-3 mb-3">
              <div className="footer-title mt-4 gray-dark footer-small-screen-font">Partnership</div>

              <ul className="footer-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Be a Partner
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Shipper
                  </Link>
                </li>
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Escrow
                  </Link>
                </li> */}
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Manufacturer
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Surveyor
                  </Link>
                </li>
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    All Boats List
                  </Link>
                </li> */}
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Top 20 Used Boats
                  </Link>
                </li> */}
              </ul>
            </Grid>
            <Grid item className="footer-links-div pl-3 pr-3 mb-3">
              <div className="footer-title mt-4 gray-dark footer-small-screen-font">World Boat Show</div>

              <ul className="footer-nav sub-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    List Of Boats Show
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Add Your Boat Show
                  </Link>
                </li>
              </ul>

              <div className="footer-title mt-4 gray-dark footer-small-screen-font">Tips and Resources</div>

              <ul className="footer-nav sub-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="/certify-boat" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Certify Your Boat
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="/help" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                   Help
                  </Link>
                </li>
              </ul>
            </Grid>

            <Grid item className="footer-links-div pl-3 pr-3 mb-3">
              <div className="footer-title mt-4 gray-dark footer-small-screen-font">AdamSea.com</div>

              <ul className="footer-nav sub-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Our Market
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                  Policy
                  </Link>
                </li>
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Leadership
                  </Link>
                </li> */}
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    What We Do
                  </Link>
                </li> */}
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Our Process
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Integrity And Compliance
                  </Link>
                </li>
                {/* <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="/boat-media" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    AdamSea Connect
                  </Link>
                </li> */}
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    About Us
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    AdamSea Community
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="/contact-us" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Contact AdamSea
                  </Link>
                </li>
              </ul>
            </Grid>

            <Grid item className="footer-links-div pl-3 pr-3 mb-3">
              <div className="footer-title mt-4 gray-dark footer-small-screen-font">Sell your Boat</div>

              <ul className="footer-nav">
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Boat Owner
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Broker
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Boat builder & Manufacture
                  </Link>
                </li>
                <li className="footer-nav-item small-screen-footer-nav-links">
                  <Link to="#" className="footer-link dark-silver footer-small-screen-font link-hover-text">
                    Selling Process
                  </Link>
                </li>
              </ul>

              <div className="footer-title mt-4 gray-dark footer-small-screen-font">AdamSea Connect</div>

              <div className="footer-app-icon">
                <img src={require("./appStore.png")} />
              </div>

              <div className="footer-app-icon mt-2">
                <img src={require("./googlePlay.png")} />
              </div>
            </Grid>
          </div>
        </Grid>
        <Grid container className="bg-navy-blue">
          <Grid item sm={12}>
            <div className="container-md">
              <div className="container footer-small-screen">
                <div className="row pt-5 pb-3">
                  <div className="col-12 text-center logo-footer-section small-screen-footer-logo-div">
                    <img
                      src={require("../../assets/images/boatInner/logo-white.png")}
                      alt="adamsea"
                      height="25"
                    />
                  </div>
                  <div className="col-12 ml-auto flex-row">
                    <ul className="social-network footer-small-screen-social-icons">
                      <li>
                        <a href="#">
                          <i className="adam-facebook small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-twitter-fill small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-google-plus small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-linkedin small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-pinterest-logo small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-blogspot-fill small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="adam-instagram small-screen-font-15 footer-icon-effect"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-12 text-center">
                    <ul className="footer-nav">
                      <li className="footer-nav-item mb-0">
                        <ul className="list-inline footer-small-screen-link-list footer-small-screen-padding-bottom">
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect"
                              onClick={() =>
                                this.getPageByTitle(
                                  footerLinks.Service_Agreement
                                )
                              }
                            >
                              Service Agreement
                            </span>
                          </li>
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect"
                              onClick={() =>
                                this.getPageByTitle(
                                  footerLinks.Dispute_Rules
                                )
                              }
                            >
                             Dispute Rules
                            </span>
                          </li>
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect"
                              onClick={() =>
                                this.getPageByTitle(
                                  footerLinks.Privacy_Policy
                                )
                              }
                            >
                              Privacy Policy
                            </span>
                          </li>
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect"
                              onClick={() =>
                                this.getPageByTitle(
                                  footerLinks.Product_Listing_Policy
                                )
                              }
                            >
                              Product Listing Policy
                            </span>
                          </li>
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect"
                              onClick={() =>
                                this.getPageByTitle(
                                  footerLinks.Terms_Of_Use
                                )
                              }
                            >
                             Terms Of Use
                            </span>
                          </li>
                          <li className="list-inline-item cursor-pointer footer-small-screen-font">
                            <span
                            className="footer-text-effect">
                             Sitemap
                            </span>
                          </li>
                        </ul>
                      </li>
                    </ul>
                      {/* dialog start */}

                    <Dialog
                      open={privacyModal}
                      onClose={this.privacyPolicyHandler}
                      aria-labelledby="alert-dialog-title"
                      aria-describedby="alert-dialog-description"
                    >
                      <DialogTitle id="alert-dialog-title" className="small-screen-footer-link-title">
                        <div className="d-flex justify-content-between align-items-center">{pageInfoByType.title}
                        <div onClick={() => this.privacyPolicyHandler()}>
                        <img src={require("./close.png")} alt="" className="footer-close-btn cursor-pointer" />
                        </div>
                        </div>
                      </DialogTitle>
                      <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                          <div
                          className="small-screen-footer-link-desc"
                            dangerouslySetInnerHTML={this.createMarkup(
                              pageInfoByType.pageContent
                            )}
                          />
                        </DialogContentText>
                      </DialogContent>
                    </Dialog>
                    {/* dialog end */}
                  </div>
                  <div className="col-12 text-center ">
                    <div className="sub-footer text-muted footer-rights-section small-screen-font-12">
                      © 2020 AdamSea.com All Rights Reserved.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => ({
  pageInfoByType:
    state.pageInfoByTypeReducer && state.pageInfoByTypeReducer.pageInfoByType,
  success: state.pageInfoByTypeReducer && state.pageInfoByTypeReducer.success
});

const mapDispatchToProps = dispatch => ({
  getAllPageInfoByType: data => dispatch(getAllPageInfoByType(data)),
  clearPageInfoByTypeFlag: data => dispatch(clearPageInfoByTypeFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
