import React from 'react'
import '../footer/footer.scss'
import { Grid } from '@material-ui/core';
// import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import { Link } from "react-router-dom";
import {Layout} from '../layout/layout';

class FooterContent extends React.Component {
    render() {
        return (
            <>
                <Layout>
                    <div className="contactFooter-bg">
                        {/* <div className="container footerContact-container"> */}
                        <Container maxWidth="lg" className="footerContact-container">
                            <div className="h-100 contact-title">
                                <div className="lines">
                                    <h1 className="line1 mb-0">Contact Us</h1>
                                </div>
                            </div>
                        </Container>
                        {/* </div> */}

                        <div className="mt-5">
                            {/* <div className="inner"> */}
                            <Container maxWidth="lg">

                                <Grid container spacing={3}>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Call Me
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Call</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                               Feedback
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt  ut labore et
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Feedback</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Advertise with us
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Advertise</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={3}>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Partner Access
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Access</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                News and Multimedia
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>News</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Partner Relation
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Relation</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={3}>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Suggestions
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Suggestions</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Customer Service
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Service</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                    <Grid item xs>
                                        <div className="contactDetail-box bg-gray pb-40 position-relative h-100">
                                            <h3 className="font-weight-400">
                                                Submit Complaint
                                             </h3>
                                            <p className="text-justify font-14 contactDetail-style">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                             </p>
                                            <p className="contact-link">
                                                <Link to="/">
                                                    <h5>Complaint</h5>
                                                </Link>
                                            </p>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Container>
                        </div>
                    </div>
                </Layout>
            </>
        )
    }
}

export default FooterContent