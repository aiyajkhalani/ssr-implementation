import React from "react";
import { Icons } from "../icons";
import moment from "moment";
import Truncate from "react-truncate";
import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import SharePopup from "../share/SharePopup";

export class ShowBoatCard extends React.Component {
  state = {
    width: 275,
    height: 350,
    selectedIndex: false
  };
  componentDidMount() {
    const { isSearch } = this.props;
    const className = isSearch ? ".section-grid" : ".section-heading";
    const width = getRatio(dimension, "boatShowItems", className);
    const height = getHeightRatio(dimension, "boatShowItems", className);

    this.setState({ width, height });
  }

  handleClick = index => {
    const { selectedIndex } = this.state;

    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {
    const { showBoatBtn, value, index } = this.props;
    const { width, height, selectedIndex } = this.state;
    return (
      <div className="boat-show-grid position-relative">
        <CommonAdamSeaStyle
          height={height}
          width={width}
          className="w-100"
          img={value && encodeURI(value.showLogo)}
        ></CommonAdamSeaStyle>
        <div
        className="card-action d-flex justify-content-end"
      >
        <div class="share-icon">
          <div class="heart-button">
            <i class="adam-heart-1"></i>
          </div>
          <SharePopup
            handleClick={() => this.handleClick(index)}
            index={index}
            selectedIndex={selectedIndex}
          />
        </div>
      </div>
        <div className="content-boat h-200 mt-2 pl-0">
          <h4 className="title-text text-capitalize font-16 mb-2 pb-1 small-screen-boat-show-title-font font-weight-500 gray-dark">
            {value && value.title}
          </h4>
          <div className="margin-bottom-boat-show d-flex align-items-center">
            <img
              src={require("../../containers/boatShow/image/location.png")}
              className="boat-show-desc-icons mr-2"
              alt=""
            />

            <span className="boat-show-text font13 small-screen-boat-show-desc-font dark-silver font-weight-400">
              {value && value.city}, {value && value.country}
            </span>
          </div>
          <div className="margin-bottom-boat-show">
            <div className="d-flex flex-column">
              <span className="boat-show-text d-flex align-items-center  font13 small-screen-boat-show-desc-font dark-silver font-weight-400">
                <img
                  src={require("../../containers/boatShow/image/calender.png")}
                  className="boat-show-desc-icons mr-2"
                  alt=""
                />
                {` ${moment(value && value.startDate).format(
                  "DD MMM YYYY"
                )} ${` - `} ${moment(value && value.endDate).format(
                  "DD MMM YYYY"
                )} `}
              </span>
            </div>
          </div>
          <div className="margin-bottom-boat-show">
            <div className="d-flex flex-column">
              <span className="boat-show-text d-flex align-items-center font13 small-screen-boat-show-desc-font dark-silver font-weight-400">
                <img
                  src={require("../../containers/boatShow/image/clock.png")}
                  className="boat-show-desc-icons mr-2"
                  alt=""
                />
                {` ${moment(value && value.startDate).format(
                  "hh : mm a"
                )} ${` - `} ${moment(value && value.endDate).format(
                  "hh : mm a"
                )}`}
              </span>
            </div>
          </div>
          <div className="mb-1 d-flex justify-content-between align-items-center">
            <div>
            <span className="dark-silver font13 small-screen-boat-show-desc-font font-weight-400">
              Posted by :
            </span>
            <span className="boat-show-text font13 small-screen-boat-show-desc-font dark-silver font-weight-400 ml-1">
              {value && value.user && value.user.firstName}
            </span>
            </div>
            <div className="boat-show-viewer-section">
              {value.user && value.user.image &&
              <div className="view-service-reviewer-div d-flex">
                <img
                  src= {value && value.user && value.user.image}
                  className="h-100 width-100"
                  alt=""
                />
              </div>
               } 
            </div>
          </div>
          <div className="boat-show-card-desc">
            <Truncate lines={1} ellipsis={<span>... </span>}>
              {value && value.showDescription}
            </Truncate>
          </div>
          <div className="mt-2">
            <span
              //  className={`badge badge-warning f-500 W-110  ${showBoatBtn}`}
              className={`btn btn-xs btn-link-info pl-2 pr-2 text-decoration-none text-white p-1 boat-show-submit-button coming-soon-btn ${showBoatBtn}`}
            >
              <span className="boat-show-submit-button">Coming soon</span>
            </span>
          </div>
        </div>
      </div>
    );
  }
}
ShowBoatCard.defaultProps = {
  isSearch: false
};
