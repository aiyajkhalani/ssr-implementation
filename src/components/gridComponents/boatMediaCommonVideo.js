import React from "react";

import UserContext from "../../UserContext";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { dimension } from "../../util/enums/enums";
import { getRatio, getHeightRatio, getWidthRatio } from "../../helpers/ratio";
import {
  BoatMediaMainStyle,
  BoatMediaWidthStyle
} from "../styleComponent/styleComponent";

export class BoatMediaCommonVideo extends React.Component {
  state = {
    height: dimension.boatMediaCommonVideo.height,
    width: dimension.boatMediaCommonVideo.width
  };

  static contextType = UserContext;

  componentDidMount() {
    const width = getRatio(
      dimension,
      "boatMediaCommonVideo",
      ".common-video"
    );
    const height = getHeightRatio(
      dimension,
      "boatMediaCommonVideo",
      ".common-video"
    );

    this.setState({ width, height });
  }

  render() {
    const { dimension, value } = this.props;
    const { height, width } = this.state;

    return (
      <>
        <div>
          <BoatMediaMainStyle
            bgHeight={height}
            bgWidth={width}
            img={value.img}
          ></BoatMediaMainStyle>
          <BoatMediaWidthStyle bgWidth={width}>
            <div className="boat-media-article-desc mt-3 d-flex flex-column">
              <span className="font-weight-500 article-desc-title gray-dark">
                {value.videoTitle}
              </span>
              <span className="font-weight-400 f-15 dark-silver">
                {value.videoDesc}
              </span>
            </div>
          </BoatMediaWidthStyle>
        </div>
      </>
    );
  }
}
