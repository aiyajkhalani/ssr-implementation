import React from "react";
import uuid from "uuid/v4";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import { BoatMediaVideo } from "./boatMediaVideo";

export class BoatMediaVideos extends React.Component {

  render() {
    const { boatMediaVideos,dimension, itemsLength} = this.props;
    return (
      <>
        <div className="boat-media-video-section d-flex flex-wrap justify-content-between">
          {boatMediaVideos && boatMediaVideos.length
            ? boatMediaVideos.map((value, index) => {
                return (
                  itemsLength > index && (
                  <div key={uuid()} className="mt-3">
                    <BoatMediaVideo
                      value={value}
                      dimension={dimension}
                    />
                  </div>
                  )
                );
              })
            : <div item xs={12} > No Record Found </div>}
        </div>
        {  (boatMediaVideos && boatMediaVideos.length) && boatMediaVideos.length > itemsLength ? (
          
          <div className="mt-50">
            <h2>
              <Link to={'#'} className="show-link mb-0 text-decoration-unset">
                Show all ({boatMediaVideos.length})
                <KeyboardArrowRight />
              </Link>
            </h2>
          </div>
        ) : <>
        
        </>}
      </>
    );
  }
}
BoatMediaVideos.defaultProps ={
  dimension: {
    width:380,
    height:280,
  }
}