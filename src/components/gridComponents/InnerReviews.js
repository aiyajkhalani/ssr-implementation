import React, { Component } from "react";
import uuid from "uuid/v4";
import { Grid } from "@material-ui/core";
import { InnerReview } from "./InnerReview";

class InnerReviews extends Component {

  render() {
    const { iconColor, data } = this.props;

    return (
      <Grid container spacing={3}>
        {data && data.length > 0 &&
          data.map(value => {
            return (
              <Grid key={uuid()} item xs={this.props.xs} sm={this.props.sm}>
                <InnerReview
                  value={value}
                  xs={this.props.xs}
                  sm={this.props.sm}
                  iconColor={iconColor}
                />
              </Grid>
            );
          })}
      </Grid>
    );
  }
}

export default InnerReviews;
