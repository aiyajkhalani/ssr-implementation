import React from "react";

import UserContext from "../../UserContext";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { dimension } from "../../util/enums/enums";
import { getRatio, getHeightRatio, getWidthRatio } from "../../helpers/ratio";
import Truncate from "react-truncate";
import moment from "moment";

import {
  BoatMediaMainStyle,
  BoatMediaWidthStyle
} from "../styleComponent/styleComponent";
import Video from "../../components/popUp/video";

export class BoatMediaVideo extends React.Component {
  state = {
    height: dimension.boatMediaVideo.height,
    width: dimension.boatMediaVideo.width
  };

  static contextType = UserContext;

  componentDidMount() {
    const width = getRatio(dimension, "boatMediaVideo", ".boat-media-video-section");
    const height = getHeightRatio(
      dimension,
      "boatMediaVideo",
      ".boat-media-video-section"
    );

    this.setState({ width, height });
  }

  render() {
    const { dimension, value } = this.props;
    const { height, width } = this.state;
    return (
      <>
        <div>
          <BoatMediaMainStyle
            bgHeight={height}
            bgWidth={width}
            img={value.thumbnail}
          ></BoatMediaMainStyle>
            {/* <Video
              videoUrl={value.url}
              thumbnail={value.thumbnail}
              isOpen={true}
            /> */}
          <BoatMediaWidthStyle bgWidth={width}>
            <div className="boat-media-article-desc mt-3 d-flex flex-column">
              <div className="d-flex justify-content-between">
                <span className="font-weight-400 article-desc-title gray-dark">
                  {value.title}
                </span>
                <span className="font-weight-400 aricle-user-title light-gray font13 gray-light">
                  {moment(value.createdAt).format('MMMM DD, YYYY')}
                </span>
              </div>
              <span className="col-gray f-15 mt-2 light-silver">
                <Truncate
                  lines={2}
                  ellipsis={<span>...Read More</span>}
                  className="article-desc"
                >
                  {value.description}
                </Truncate>
                {/* {value.description}
                <span className="ml-2 boat-media-read-more-text navy-blue">
                  Read More
                </span> */}
              </span>

              <div className="boat-media-aricle-user mt-2 d-flex align-items-center">
                <div className="boat-media-aricle-user-img rounded-circle">
                  <img
                    src={value.videoUserImg}
                    alt="article user"
                    className="h-100 width-100 rounded-circle"
                  />
                </div>
                <div className="boat-media-aricle-user-desc d-flex flex-column ml-3">
                  <div className="d-flex">
                    <span className="font-weight-400 aricle-user-title gray-dark">
                      {value.videoUserName}
                    </span>
                    <span className="font-weight-400 aricle-user-title light-silver ml-2">
                      {value.videoUserType}
                    </span>
                  </div>
                  <span className="font-weight-400 aricle-user-title light-silver">
                    {value.videoUserCity}, {value.videoUserCountry}
                  </span>
                </div>
              </div>
            </div>
          </BoatMediaWidthStyle>
        </div>
      </>
    );
  }
}
