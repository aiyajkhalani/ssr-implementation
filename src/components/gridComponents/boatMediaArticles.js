import React from "react";
import uuid from "uuid/v4";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import { BoatMediaArticle } from "./boatMediaArticle";

export class BoatMediaArticles extends React.Component {

  render() {
    const { boatMediaArticles,dimension, itemsLength} = this.props;
    return (
      <>
        <div className="d-flex flex-wrap justify-content-between">
          {boatMediaArticles && boatMediaArticles.length
            ? boatMediaArticles.map((value, index) => {
                return (
                  itemsLength > index && (
                  <div key={uuid()} className="mt-3">
                    <BoatMediaArticle
                      value={value}
                      dimension={dimension}
                    />
                  </div>
                  )
                );
              })
            : <div item xs={12} > No Record Found </div>}
        </div>
        {  (boatMediaArticles && boatMediaArticles.length) && boatMediaArticles.length > itemsLength ? (
          
          <div className="mt-50">
            <h2>
              <Link to={'#'} className="show-link mb-0 text-decoration-unset">
                Show all ({boatMediaArticles.length})
                <KeyboardArrowRight />
              </Link>
            </h2>
          </div>
        ) : <>
        
        </>}
      </>
    );
  }
}
BoatMediaArticles.defaultProps ={
  dimension: {
    width:360,
    height:180,
  }
}