import React from "react";
import { Grid } from "@material-ui/core";
import Truncate from "react-truncate";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { viewServiceBoatHandler } from "../../helpers/boatHelper";
import { UserConsumer } from "../../UserContext";
import SharePopup from "../share/SharePopup";

export class BoatAddedService extends React.Component {

  state = {
    selectedIndex: null
  }

  handleClick = index => {
    const { selectedIndex } = this.state;

    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  }

  static contextType = UserConsumer;

  getNewWidth = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5 - 15;
    const aspectRatio = dimensionAspectRatio(160, 73);
    const newWidth = actualWidth / aspectRatio - 3;
    return newWidth;
  };

  getNewHeight = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5;
    const aspectRatio = dimensionAspectRatio(170, 73);
    const newHeight = actualWidth / aspectRatio - 5;

    return newHeight;
  };

  render() {
    const { value, index } = this.props;
    const { selectedIndex } = this.state;
    const height = this.getNewHeight();
    const width = this.getNewWidth();
    const { history } = this.context;
    return (
      <div className="card border hcard cursor-pointer boat-service-added-div-border">
        <div
          className="card-body mt-0"

        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Grid
                container
                spacing={3}
                className="recently-added-services-div-padding"
              >
                <Grid item xs={4} className="pr-0 pb-2 pt-2 pl-2" onClick={() => viewServiceBoatHandler(value.id, history)}>
                  <div>
                    <CommonAdamSeaStyle height={height} width={width} >
                      <img
                        src={
                          (value.images &&
                            value.images.length &&
                            value.images[0]) ||
                          value.uploadSalesManPhoto
                        }
                        alt=""
                        className="radius-4 h-100 width-100"
                      />
                    </CommonAdamSeaStyle>
                  </div>
                </Grid>
                <div className="card-action service-share-icon">
                  <div class="share-icon">
                    <div className="heart-button">
                      <i className="adam-heart-1"></i>
                    </div>
                    <SharePopup handleClick={() => this.handleClick(index)} index={index} selectedIndex={selectedIndex} />
                  </div>
                </div>
                <Grid item xs={8} className="pl-0 pb-2 pt-2 pr-2">

                  <div className="card-block h-100" onClick={() => viewServiceBoatHandler(value.id, history)}>
                    <div className="d-flex hcard h-100 mb-0 w-100">
                      <div className="details d-flex justify-content-between pl-0">
                        <figcaption className="info-wrap pl-3">
                          <div className="service-country-name f-15 boat-service-recently-added-city gray-dark font-weight-500">
                            {value.city},{value.country || value.serviceCountry}
                          </div>


                          <div className="boat-added-recently-services-desc light-silver font-weight-500">
                            <Truncate lines={3} ellipsis={<span>... </span>}>
                              <span>
                                {value.service || value.serviceDesc}
                              </span>
                            </Truncate>
                          </div>
                          <div className="d-flex justify-content-between recently-service">
                            <span className="review-star sm float-left d-flex boat-service-added-rating">
                              <Rating
                                className="rating-clr d-flex boat-added-service-rating-align"
                                initialRating={0}
                                fullSymbol={<StarIcon />}
                                emptySymbol={<StarBorderIcon />}
                                placeholderSymbol={<StarBorderIcon />}
                                readonly
                              />
                              <span className="rate full review-text ml-1 mt-1 f-14 gray-light">
                                ({value.rating || `0`})
                              </span>
                            </span>
                          </div>
                          
                        </figcaption>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item sm={12} className="pt-0 pb-0 d-flex mt-2 pl-2 pr-0">
                  <span className="recent-service boat-service-quality-title font-weight-500 w-fix-service">
                    Our Quality of Services:
                  </span>
                  <div className="ml-1 recent-service-description margin-service-text boat-service-quality-desc light-silver font-weight-500">
                    {value.qualityOfService}
                  </div>
                </Grid>
                <Grid container>
                    <Grid item sm={12}>
                      <div className="d-flex mt-1 justify-content-between">
                        <div className="d-flex align-items-center">
                          <img
                            src={(value.images && value.images.length) && value.images[0]}
                            className="view-service-img-salesman"
                            alt=""
                          />

                          <div className="salesman-name f-15 boat-service-viewed-user font-weight-500 dark-silver">
                            By {value.salesManFullName}
                          </div>
                        </div>
                        <div className="boat-service-viewer-section">
                          {value.user && value.user.companyLogo &&
                            <div className="view-service-reviewer-div d-flex">
                              <img
                                src={value.user.companyLogo}
                                className="h-100 width-100"
                                alt=""
                              />
                            </div>
                          }
                        </div>
                      </div>
                    </Grid>
                  </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}
