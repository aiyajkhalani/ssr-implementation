import React, { Component, Fragment } from "react";
import { getHeightRatio, getRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import { SingleTopRatedMarina } from "../marinaStorage/singleTopRatedMarina";
import { ShowAllLink } from "../helper/showAllLink";

class TopRatedMarina extends Component {
  state = {
    height: 360,
    width: 280
  };
  componentDidMount() {
    const width = getRatio(dimension, "topRatedMarina", ".section-heading");
    const height = getHeightRatio(
      dimension,
      "topRatedMarina",
      ".section-heading"
    );
    this.setState({ width, height });
  }

  render() {
    const { height, width } = this.state;
    const {
      topRatedMarina,
      limit,
      total,
      showAllText
      ,
    } = this.props;

    return (
      <>
        <div className="most-popular-main top-rated-grid marina-ads-sections">
          {topRatedMarina &&
            topRatedMarina.map((item, index) => {
              return (
                <Fragment key={item.id}>
                  {index < limit && (
                    <SingleTopRatedMarina
                      value={item}
                      width={width}
                      height={height}
                      index={index}
                    />
                  )}
                </Fragment>
              );
            })}
        </div>
        <ShowAllLink 
          data={topRatedMarina} 
          total={total}
          itemsLength={limit} 
          showAllText={showAllText} 
          url="/show-all-marina-and-storage/topRated" />
      </>
    );
  }
}

export default TopRatedMarina;
