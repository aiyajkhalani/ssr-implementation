import React from "react";

import UserContext from "../../UserContext";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { FaRegThumbsUp, FaRegEye } from "react-icons/fa";
import Truncate from "react-truncate";

import {
  BoatMediaMainStyle,
  BoatMediaWidthStyle
} from "../styleComponent/styleComponent";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";

export class BoatMediaArticle extends React.Component {
  state = {
    height: dimension.boatMediaArticle.height,
    width: dimension.boatMediaArticle.width
  };

  static contextType = UserContext;

  componentDidMount() {
    const width = getRatio(dimension, "boatMediaArticle", ".section-heading");
    const height = getHeightRatio(
      dimension,
      "boatMediaArticle",
      ".section-heading"
    );

    this.setState({ width, height });
  }

  render() {
    const { dimension, value } = this.props;
    const { height, width } = this.state;

    return (
      <>
        <div>
          <BoatMediaMainStyle
            bgHeight={height}
            bgWidth={width}
            img={value.file}
          ></BoatMediaMainStyle>
          <BoatMediaWidthStyle bgWidth={width}>
            <div className="boat-media-article-desc mt-3 d-flex flex-column">
              <span className="font-weight-400 article-desc-title gray-dark">
                {value.title}
              </span>
              {/* <span className="light-silver f-15 mt-2 article-desc">
                {value.articleDesc}
                <span className="ml-2 boat-media-read-more-text navy-blue">
                  Read More
                </span>
              </span> */}
              <span className="article-desc-texts">
                <Truncate
                  lines={2}
                  ellipsis={<span>...Read More</span>}
                  className="article-desc"
                >
                  {value.description}
                </Truncate>
              </span>
              <div className="d-flex mt-2 mb-1">
                <div className="d-flex align-items-center gray-light">
                  <FaRegEye />
                  <span className="ml-2">{value.articleViewCount ? value.articleViewCount: 0}</span>
                </div>
                <div className="d-flex align-items-center ml-5 gray-light">
                  <FaRegThumbsUp />
                  <span className="ml-2">{value.articleLike ? value.articleLike : 0}</span>
                </div>
              </div>
              <div className="boat-media-aricle-user mt-2 d-flex align-items-center">
                <div className="boat-media-aricle-user-img rounded-circle">
                  <img
                    src={value.user.image}
                    alt="article user"
                    className="h-100 width-100 rounded-circle"
                  />
                </div>
                <div className="boat-media-aricle-user-desc d-flex flex-column ml-3">
                  <span className="font-weight-400 aricle-user-title gray-dark">
                    {value.user.firstName} {value.user.lastName}
                  </span>
                  <span className="font-weight-400 aricle-user-title light-silver">
                    {value.user.city}, {value.user.country}
                  </span>
                </div>
              </div>
            </div>
          </BoatMediaWidthStyle>
        </div>
      </>
    );
  }
}
