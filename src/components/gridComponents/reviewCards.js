import React from "react";
import { Grid } from "@material-ui/core";
// style
import "../../containers/boatServices/boatServices.scss";

import { ReviewCard } from "./reviewCard";


const ReviewCards = props => {
  
  const { iconColor, experience, isRent, isService, isMarina } = props;
  return (
    <Grid container spacing={3}>
      {experience &&
        experience.map((value, index) => {
          return (
            <Grid key={value.id} item xs={props.xs} sm={props.sm}>
              <ReviewCard
                value={value}
                xs={props.xs}
                sm={props.sm}
                isRent={isRent}
                isService={isService}
                isMarina={isMarina}
                iconColor={iconColor}
                experience={experience}
              />
            </Grid>
          );
        })}
    </Grid>
  );
};

export default ReviewCards;

ReviewCards.defaultProps ={
  isRent:false,
  isMarina:false,
  isService:false
}