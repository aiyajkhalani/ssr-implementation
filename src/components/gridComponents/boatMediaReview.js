import React from "react";

import UserContext from "../../UserContext";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { Col, Row } from "react-bootstrap";
import moment from "moment";

export class BoatMediaReview extends React.Component {
  static contextType = UserContext;

  getNewWidth = dimension => {
    if (dimension) {
      const width = document.querySelector(".section-heading");
      const actualWidth = width && width.offsetWidth / dimension.divide - 15;

      return actualWidth;
    }
  };

  getNewHeight = dimension => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / dimension.divide - 15;
    const aspectRatio = dimensionAspectRatio(dimension.width, dimension.height);
    const newHeight = actualWidth / aspectRatio;

    return newHeight;
  };

  render() {
    const { dimension, value, isBorder } = this.props;

    

    const height = dimension && this.getNewHeight(dimension);
    const width = dimension && this.getNewWidth(dimension);
    return (
      <>
        <div className="d-flex pl-3 pr-3 mt-3">
          <div
            className={`w-auto ${!isBorder &&
              "boat-media-reviewer-div-bottom-border"}`}
          >
            <div className="boat-media-reviwer-img rounded-circle">
              <img
                src={value.userId && value.userId.image}
                alt="article user"
                className="h-100 width-100 rounded-circle"
              />
            </div>
          </div>
          <div
            className={`boat-media-aricle-user-desc d-flex flex-column pl-3 width-100 ${!isBorder &&
              "boat-media-reviewer-div-bottom-border"} pb-4`}
          >
            <span className="font-weight-400 aricle-user-title gray-light font13">
              {moment(value.createdAt).format('MMMM DD, YYYY')}
            </span>
            <span className="font-weight-400 aricle-user-title font-14 mt-2 mb-2 dark-silver">
              {value.reviews}
              <span className="ml-2 boat-media-read-more-text navy-blue">
                Read More
              </span>
            </span>
            <div className="d-flex">
              <span className="font-weight-400 aricle-user-title font-14 gray-dark">
              {value.userId && value.userId.firstName} {value.userId && value.userId.lastName}
              </span>
              <span className="font-weight-400 aricle-user-title ml-2 font-14 light-silver">
                {value.userId && value.userId.reviewerType}
              </span>
            </div>
          </div>
        </div>
      </>
    );
  }
}
