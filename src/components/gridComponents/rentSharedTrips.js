import React from "react";
import Grid from "@material-ui/core/Grid";

import { RentSharedTrip } from "./rentSharedTrip";
import { ShowAllLink } from "../helper/showAllLink";

import "../../containers/rent/rent.scss";

export class RentSharedTrips extends React.Component {

  render() {
    const { xs, sm, isMostPopular, rentSharedTrips, dimension, isPrivate, itemsLength, showType, isRentPerHour, showAllText, className } = this.props;
    const url = isMostPopular ? `/show-all-rent/${showType}` : `/rent-type/${showType}`
    return (
      <>
        <Grid  container spacing={isMostPopular ? 0 : 2}>
          {rentSharedTrips && rentSharedTrips.length
            && rentSharedTrips.map((value, index) => {
              return (
                itemsLength > index && (
                  <Grid className={className} key={value.id} item xs={xs} sm={sm}>
                    <RentSharedTrip
                      value={value}
                      xs={xs}
                      index={index}
                      sm={sm}
                      dimension={dimension}
                      isPrivate={isPrivate}
                      isRentPerHour={isRentPerHour}
                    />
                  </Grid>
                )
              );
            })}
        </Grid>

        <ShowAllLink data={rentSharedTrips} itemsLength={itemsLength} showAllText={showAllText} url={url} />
        
      </>
    );
  }
}
RentSharedTrips.defaultProps = {
  dimension: {
    width: 380,
    height: 280,
  },
  isMostPopular: false,
  className: "",
}