import React, { Component } from "react";
import { Grid } from "@material-ui/core";

import { BoatAddedService } from "./boatAddedService";
import { ShowAllLink } from "../helper/showAllLink";

class BoatServicesAdded extends Component {
  render() {
    const { iconColor, boatAddedServices, xs, sm, showType, showAllText } = this.props;
    return (
      <Grid container spacing={2}>
        {boatAddedServices &&
          boatAddedServices.length &&
          boatAddedServices.map((value, index) => {
            return (
              <>
                {index < 6 && (
                  <Grid key={value.id} item xs={xs} sm={sm}>
                    <BoatAddedService
                      index={index}
                      value={value}
                      xs={xs}
                      sm={sm}
                      iconColor={iconColor}
                    />
                  </Grid>
                )}
              </>
            );
          })}
        <ShowAllLink 
          data={boatAddedServices} 
          itemsLength={3} 
          showAllText={showAllText} 
          url={`/show-all-service/${showType}`} />
      </Grid>
    );
  }
}

export default BoatServicesAdded;
