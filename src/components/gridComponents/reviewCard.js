import React from "react";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Truncate from "react-truncate";

import "../../containers/boatServices/boatServices.scss";
import { dimensionAspectRatio, priceFormat } from "../../util/utilFunctions";
import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import "../../styles/responsive.scss";
import { viewRentBoatHandler, viewServiceBoatHandler, viewMarinaDetails } from "../../helpers/boatHelper";
export class ReviewCard extends React.Component {
  state = {
    width: 385,
    height: 330
  };
  
  componentDidMount() {
    const width = getRatio(dimension, "boatServiceReview", ".section-heading");
    const height = getHeightRatio(
      dimension,
      "boatServiceReview",
      ".section-heading"
    );
    width && height && this.setState({ width, height });
  }
  viewHandler = (value) =>{
    const {isRent, isService, isMarina} = this.props
    isRent ?
     viewRentBoatHandler(value) : 
     (isService ? viewServiceBoatHandler(value.id) : 
     isMarina && viewMarinaDetails(value.id));
  }

  render() {
    const { iconColor, value } = this.props;
    const { userId, images, name, destination, description, rating } = value;
    const { width, height } = this.state;
    return (
      <div 
        className="h-100 review-card-div-border"
         onClick={() => this.viewHandler(value)}>
        <CommonAdamSeaStyle
          height={height}
          width={width}
          img={encodeURI(images && images.length && images[0])}
        ></CommonAdamSeaStyle>
        <CommonAdamSeaStyle width={width}>
          <div className="width-100 d-flex justify-content-between flex-column">
            <div className="flex-column d-flex justify-content-center width-100 pl-0 justify-content-between">
              <div>
                <div className="card-info mt-3 mb-2">
                  <div className="alignCenter">
                    <h6 className="rent-card-text f-500 mb-0">
                      <span className="brand-new-tag">{name}</span>
                    </h6>
                  </div>
                </div>
              </div>

              <h6 className="rent-card-text f-400 mb-1">{destination}</h6>
              <p className="desc-text mt-1 mb-1 desc-height-width">
                <Truncate lines={3} ellipsis={<span>... </span>}>
                  <span>{description}</span>
                </Truncate>
              </p>
            </div>

            <div className="d-flex align-items-center mt-1 mb-1 ">
              <div className="d-flex align-items-center w-100 justify-content-between">
                <div id="userbox" className="userbox d-flex align-items-center">
                  <div className="profile-picture">
                    <img
                      src={userId && userId.image}
                      className="rounded-circle experience-user-width img-auto"
                      width="30px"
                    />
                  </div>
                  {userId && userId.firstName && (
                    <div
                      className="profile-info text-overflow-unset"
                      data-lock-name="John Doe"
                    >
                      <span className="name-reviewer">
                        By {userId && userId.firstName}{" "}
                        {userId && userId.lastName}
                      </span>
                    </div>
                  )}
                </div>
                {rating && (
                  <div className="d-flex align-items-center">
                    <span className="review-star sm float-left">
                      <Rating
                        className="rating-clr"
                        initialRating={rating}
                        fullSymbol={<StarIcon />}
                        emptySymbol={<StarBorderIcon />}
                        placeholderSymbol={<StarBorderIcon />}
                        readonly
                      />
                    </span>
                    <div className="mt-1">
                      <span className="review-text ml-1">({rating})</span>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </CommonAdamSeaStyle>
      </div>
    );
  }
}

ReviewCard.defaultProps ={
  isRent:false,
  isMarina:false,
  isService:false
}