import React from "react";
import { pagination } from "../../util/enums/enums";
import { connect } from "react-redux";
import { Layout } from "../layout/layout";
import { PaginationBar } from "../pagination/PaginationBar";
import UserContext from "../../UserContext";
import { getAllBoatShow } from "../../redux/actions/boatShowAction";
import BoatListingsWithMap from "./BoatListingsWithMap";

class ShowAllBoatShow extends React.Component {
  static contextType = UserContext;
  state = {
    showMap: false
  };

  componentDidMount() {
    const { getAllBoatShow } = this.props;
    getAllBoatShow({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context && this.context.country
    });
  }

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {
    const { allBoatShows, allBoatShowCount } = this.props;
    const { country } = this.context;
    const { showMap } = this.state;

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
        
                      <BoatListingsWithMap
                        isBoatShow
                        boatsTypeCount={allBoatShowCount} 
                        boatsType={'Boat Show'}
                        showMap={showMap}
                        isNotHome={false}
                        toggleMapHandler={this.toggleMapHandler}
                        boats={allBoatShows}
                      />
                  
            {allBoatShowCount > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination">
              <PaginationBar
                action={getAllBoatShow}
                value={{ country }}
                totalRecords={allBoatShowCount}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  allBoatShows: state.boatShowReducer && state.boatShowReducer.allBoatShows,
  allBoatShowCount:
    state.boatShowReducer && state.boatShowReducer.allBoatShowCount
});

const mapDispatchToProps = dispatch => ({
  getAllBoatShow: value => dispatch(getAllBoatShow(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowAllBoatShow);
