import React, { Component } from "react";
import { connect } from "react-redux";

import { getRecentlyAddedServices } from "../../redux/actions";
import UserContext from "../../UserContext";
import { PaginationBar } from "../pagination/PaginationBar";
import { Layout } from "../layout/layout";
import { pagination } from "../../util/enums/enums";
import BoatListingsWithMap from "./BoatListingsWithMap";

class ShowAllService extends Component {

  state = {
    showMap: false,
  }
  static contextType = UserContext;

  componentDidMount() {
    const { getRecentlyAddedServices } = this.props;
    getRecentlyAddedServices({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context && this.context.country
    });
  }


  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {
    const {
      recentlyAddedService, totalRecentlyAddedService
    } = this.props;
    const { country } = this.context;
    const { showMap, action, total, isAuction, type } = this.state;


    return (
      <Layout isFooter>
        {/* <Grid container spacing={2} className="boat-service-layout mt-3 pl-3 pr-3">
          <Grid item sm={12} className="section-heading"> </Grid>
          {recentlyAddedService &&
            recentlyAddedService.length &&
            recentlyAddedService.map((value, index) => {
              return (
                <>
                    <Grid key={value.id} item sm={4}>
                      <BoatAddedService
                        index={index}
                        value={value}
                        sm={12}
                      />
                    </Grid>
                </>
              );
            })}
          {totalRecentlyAddedService > 6 &&
                        <div className="boat-pagination w-100">
                            <PaginationBar
                                action={getRecentlyAddedServices}
                                value={{ country }}
                                totalRecords={totalRecentlyAddedService}
                            />
                        </div>
                    }
        </Grid> */}

        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            isBoatService
            boatsTypeCount={totalRecentlyAddedService}
            boatsType="Recently added boat services"
            isShowAuction={isAuction}
            showMap={showMap}
            toggleMapHandler={this.toggleMapHandler}
            viewService={id => this.viewService(id)}
            boats={recentlyAddedService}
          />

          {total > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination mt-50">
              <PaginationBar
                action={action}
                value={{ country }}
                totalRecords={total}
              />
            </div>
          )}
        </div>

      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  recentlyAddedService:
    state.boatServiceReducer && state.boatServiceReducer.recentlyAddedService,
  totalRecentlyAddedService:
    state.boatServiceReducer &&
    state.boatServiceReducer.totalRecentlyAddedService
});

const mapDispatchToProps = dispatch => ({
  getRecentlyAddedServices: data => dispatch(getRecentlyAddedServices(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowAllService);
