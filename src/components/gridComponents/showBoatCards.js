import React, { Component } from "react";
import uuid from "uuid/v4";
import { Grid } from "@material-ui/core";
import { ShowBoatCard } from "./showBoatCard";
import { KeyboardArrowRight } from "@material-ui/icons";

class ShowBoatCards extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    
    const {  xs,sm, showBoatBtn, showBoatCards, isSearch, showAllText, isShowAll } = this.props
    return (
      <Grid container spacing={2}>
        {showBoatCards &&
          showBoatCards.map((value, index) => {
            return (
              <Grid key={uuid()} item xs={this.props.xs} sm={this.props.sm}>
                <ShowBoatCard
                  value={value}
                  xs={xs}
                  isSearch={isSearch}
                  sm={sm}
                  showBoatBtn={showBoatBtn}
                />
              </Grid>
            );
          })}

      { isShowAll && showBoatCards &&
        showBoatCards.length &&
        showBoatCards.length > 4 ? (
          <div className="w-100">
            <h5 className="mt-0">
  
              <a
                href={`show-all-boat-show`}
                target="_blank"
                className="show-link mb-0 text-decoration-unset"
              >
                Show all ({showBoatCards && showBoatCards.length}) {showAllText}
                <KeyboardArrowRight />
              </a>
            </h5>
          </div>
        ) : (
          <></>
        )}

      </Grid>
    );
  }
}

export default ShowBoatCards;

ShowBoatCard.defaultProps={
  isSearch:false,
  isShowAll:false
} 