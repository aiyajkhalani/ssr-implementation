import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";

import "../home/home.scss";
import UserContext from "../../UserContext";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import { ShowAllLink } from "../helper/showAllLink";
import { BoatDetail } from "../home/singleComponents/boatDetail";

export class BoatGrid extends React.Component {
  static contextType = UserContext;

  state = {
    width: 380,
    height: 280
  };

  componentDidMount() {
    const { isBottomRating } = this.props
    const className = isBottomRating ? "homeMostPopular" : "homeRecommended"
    const width = getRatio(dimension, className, ".section-heading");
    const height = getHeightRatio(
      dimension,
      className,
      ".section-heading"
    );
    this.setState({ width, height });
  }

  render() {
    const {
      boatGrid,
      itemsLength,
      showType,
      route,
      isTopLabel,
      isBottomRating,
      isCustomWidth,
      showAllText,
      xs,
      sm
    } = this.props;
    const { width, height } = this.state;

    const { history } = this.context;

    const routeName = route ? "show-all" : "show-all-boats";

    return (
      <>
        <Grid container spacing={2}>
          {boatGrid && boatGrid.length
            ? boatGrid.map((value, index) => {
              return (
                <Fragment key={value.id}>
                  {itemsLength > index && (
                    <BoatDetail
                      value={value}
                      isTopLabel={isTopLabel}
                      isBottomRating={isBottomRating}
                      isCustomWidth={isCustomWidth}
                      width={width}
                      height={height}
                      index={index}
                      history={history}
                      xs={xs}
                      sm={sm}
                    />
                  )}
                </Fragment>
              );
            })
            : "There is no boats found."}
        </Grid>

        <ShowAllLink data={boatGrid} itemsLength={itemsLength} showAllText={showAllText} url={`/${routeName}/${showType}`} />

      </>
    );
  }
}

BoatGrid.defaultProps = {
  isTopLabel: false,
  isBottomRating: false,
  isCustomWidth: false,
};
