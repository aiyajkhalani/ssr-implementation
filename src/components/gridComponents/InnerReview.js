import React from "react";
import { Grid } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import moment from "moment";

import { Icons } from '../icons'

import "../../containers/rentInner/rentInner.scss"
import { Modal, Button } from "react-bootstrap";

export class InnerReview extends React.Component {

  state = {
    show: false
  }

  modalHandler = () => {
    this.setState(preState => ({ show: !preState.show }));
  };

  render() {

    const { value } = this.props;

    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Grid item xs={12}>
            <div className="d-flex align-items-start">
              <div className="mr-4 ml-0 mb-0 mt-0 rentInnerReview-imgDiv">
                <img
                  src='/static/media/img-6.416f3757.jpg'
                  className="radius-4 h-100 width-100 rounded-circle"
                  alt=""
                />
              </div>
              <div className="width-100">
                <div className="rentInnerReview-nameRating justify-content-between">

                  <div className="d-flex align-items-center">
                    <h5 className="float-left mr-2 reviewer-name-section mb-0">{value && value.userId && value.userId.firstName}</h5>
                    <div className="rentInnerReview-time sm-font"> {moment(value && value.createdAt).fromNow()}</div>

                  </div>

                  <div className="d-flex justify-content-between width-100">
                    <span className="review-star sm float-left review-star-section">
                      <Rating
                        className="rating-clr"
                        initialRating={value.rating}
                        fullSymbol={<StarIcon />}
                        emptySymbol={<StarBorderIcon />}
                        placeholderSymbol={<StarBorderIcon />}
                        readonly
                      />
                    </span>
                  </div>

                </div>

                <span className="review-desc">
                  {value.reviews}
                  <span className="ml-1 sm-font cursor-pointer"  onClick={this.modalHandler}>
                    <Icons data="adam-country" />
                  </span>
                  <Modal show={this.state.show} onHide={this.modalHandler}>
                    <Modal.Header closeButton>
                      <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.modalHandler}>
                        {'Close'}
                      </Button>
                      <Button variant="primary" className="w-auto" onClick={this.modalHandler}>
                        {' Save Changes'}
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </span>
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}
