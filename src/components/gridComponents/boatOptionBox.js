import React from "react";
import uuid from "uuid/v4";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { dimensionAspectRatio, priceFormat, commonBoatType } from "../../util/utilFunctions";
import { ManufacturerStyle } from "../styleComponent/styleComponent";

import "../home/home.scss";
import "../../styles/common.scss";
import "../../containers/rent/rent.scss";
import "../../containers/boatInner/BoatInner.scss";
import UserContext from "../../UserContext";
import SharePopup from "../share/SharePopup";

import { viewBoatHandler } from "../../helpers/boatHelper";
import { cityCountryNameFormatter } from "../../helpers/jsxHelper";

export class BoatOptionBox extends React.Component {

  static contextType = UserContext;

  getNewHeight = dimension => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / dimension.divide - 15;
    const aspectRatio = dimensionAspectRatio(dimension.width, dimension.height);
    const newHeight = actualWidth / aspectRatio;

    return newHeight;
  };

  render() {
    const {
      value,
      dimension,
      selectedIndex,
      handleClick,
      index,
    } = this.props;

    const { history } = this.context;
    const {
      lengthInFt,
      boatType,
      yearBuilt,
      boatName,
      country,
      price,
      seller,
      images,
      rating,
      boatStatus:{alias},
      city
    } = value;

    const height = dimension && this.getNewHeight(dimension);
    return (
      <div key={uuid()} className="home-layout-page home-page">
        <div className="boat-box-shadow feature">
          <div className="card-action card-action-padding">
          <div>{commonBoatType(alias)}</div>
            <div>
              <div class="share-icon">
                <div className="heart-button">
                  <i className="adam-heart-1"></i>
                </div>
                  <SharePopup handleClick={handleClick} index={index} selectedIndex={selectedIndex}/>
              </div>
            </div>
          </div>
          <div>
            <ManufacturerStyle
              height={height}
              onClick={() => viewBoatHandler(value, history)}
              img={images && images.length && encodeURI(images[0])}
            />
            <div className="boat-info boat-type-carousel"  onClick={() => viewBoatHandler(value, history)}>
              <h5 className="mt-3 mb-0 boat-price">INR {priceFormat(price)}</h5>
              <div className="d-flex justify-content-between flex-direction-column spacing">
                <div className="carousel-title">{yearBuilt} {boatType && boatType.name} / {lengthInFt} ft </div>
                <div className="boat-country-home">
                  <span>{cityCountryNameFormatter(city, country)}</span>
                </div>
              </div>
              {seller && (
                <h4 className="margin-0  boat-owner f-14 f-500">
                  by <span>{seller.firstName}</span>
                </h4>
              )}

              <div className="d-flex">
                <div className="mr-1">
                  <Rating
                    className="rating-clr"
                    initialRating={rating}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="rating-count mt-1 align-items-start">
                  ({rating})
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
