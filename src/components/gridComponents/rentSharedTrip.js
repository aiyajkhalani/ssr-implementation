import React, { PureComponent } from "react";
import moment from "moment";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { viewRentBoatHandler } from "../../helpers/boatHelper";
import { dimensionAspectRatio, typeWiseTripLabel, priceFormat, rentTypeAddress, rentTypeName } from "../../util/utilFunctions";
import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import { rentIcon } from "../../util/enums/enums";
import SharePopup from "../share/SharePopup";
import { commonRentType } from '../../util/utilFunctions'

import "../../containers/rent/rent.scss";
import "../../containers/boatInner/BoatInner.scss";
import { displayDefaultNumericValue } from "../../helpers/string";

export class RentSharedTrip extends PureComponent {

  state = {
    selectedIndex: null,
    height: null,
    width: null
  }

  componentDidMount() {
    const { dimension } = this.props

    const height = dimension && this.getNewHeight(dimension);
    const width = dimension && this.getNewWidth(dimension);

    this.setState({ height, width })
  }

  getNewWidth = dimension => {
    const { isShowAllPage } = this.props
    const className = isShowAllPage ? ".section-grid" : ".section-heading"

    if (dimension) {
      const width = document.querySelector(className);
      const actualWidth = width && width.offsetWidth / dimension.divide - 15;

      return actualWidth;
    }
  };

  getNewHeight = dimension => {
    const { isShowAllPage } = this.props
    const className = isShowAllPage ? ".section-grid" : ".section-heading"
    const width = document.querySelector(className);
    const actualWidth = width && width.offsetWidth / dimension.divide - 15;
    const aspectRatio = dimensionAspectRatio(dimension.width, dimension.height);
    const newHeight = actualWidth / aspectRatio;

    return newHeight;
  };

  handleClick = index => {
    const { selectedIndex } = this.state;
    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {

    const { selectedIndex, width, height } = this.state
    const {
      value,
      isPrivate,
      index,
      isRentPerHour,
      isCustomWidth,
      isShowAllPage
    } = this.props;

    const {
      boatName,
      price,
      endTime,
      startTime,
      maximumGuest,
      boatLength,
      tripType,
      images,
      rating,
      noOfUnit,
      trip,
      address,
      tripAddress
    } = value;

    let privateBoat;
    if (isPrivate) {
      privateBoat = null;
    } else {
      privateBoat = (
        <div> {commonRentType(value)} </div>
      );
    }

    const tripTime = moment(endTime).diff(moment(startTime), "hours");
    const { history } = this.context;

    const isRentTrip = trip && trip.alias === "Rent Per Hour"
    return (
      <div
        className="position-relative cursor-pointer mb-30"
      >
        {privateBoat}
        <CommonAdamSeaStyle
          height={height}
          width={width}
          className={`${isCustomWidth && "w-100"} is-shared`}
          onClick={() => viewRentBoatHandler(value, history)}
          img={
            (images && images.length > 0 && encodeURI(images[0]))
          }
        ></CommonAdamSeaStyle>

        <div class="card-action">
          <div class="share-icon">
            <div class="heart-button">
              <i class="adam-heart-1"></i>
            </div>
            <SharePopup handleClick={() => this.handleClick(index)} index={index} selectedIndex={selectedIndex} />
          </div>
        </div>

        {/* need to change below width style */}
        <div   style={{ width: !isShowAllPage && `${width}px` }} className="directionRow spaceBetween mb-1 mt-3" onClick={() => viewRentBoatHandler(value, history)}>
          <div className="w-100">
            <div className="d-flex justify-content-between">
              <div className="d-flex align-items-center">
                <div className="mr-1">
                  <Rating
                    className="rating-clr"
                    initialRating={rating}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="rating-count-rent mt-1">
                  ({rating})
              </span>
              </div>
              <div className="d-flex align-items-center flex-wrap">
                <div className="rent-trip-type m-0">{tripType && tripType.name}</div>
              </div>
            </div>
            <div className="d-flex align-items-center">
              {isPrivate && (
                <div className="private-rent private-rent-bg boat-grid-badge">
                  {trip.alias}
                </div>
              )}
              <h6 className="mt-2 boat-price-rent">
                INR {priceFormat(price)}
                {<span className="per-person boat-price-rent">{typeWiseTripLabel(trip.alias)}</span>}
              </h6>
            </div>
            <div className="country-div-height">
              <h6 className="image-location boat-title mb-0 rent-boat-name-color">
                {rentTypeName(trip, boatName, tripType, boatLength)}
              </h6>
              <h6 className="image-location font-weight-none boat-country mb-2">
                {rentTypeAddress(trip, address, tripAddress)}
              </h6>
            </div>
          </div>
        </div>
        {(isRentPerHour || isRentTrip) ? (
          <div className="alignCenter mr-3">
            <img src={require("../../assets/images/rent/pieces.png")} className="piece-img" alt="" />
            <span className="pieces-text">{displayDefaultNumericValue(noOfUnit)} Pieces Available</span>
          </div>
        ) : (
            <div className="directionRow">
              <div className="alignCenter mr-3">
                <img src={rentIcon.clock} className="location-icon-rent width-height-icon-clock" alt="" />
                <span className="small-text">{displayDefaultNumericValue(tripTime)} Hrs</span>
              </div>
              <div className="alignCenter mr-3">
                <img src={rentIcon.user} className="location-icon-rent width-height-icon-user-measure" alt="" />
                <span className="small-text">{displayDefaultNumericValue(maximumGuest)} </span>
              </div>
              <div className="alignCenter mr-3">
                <img src={rentIcon.measure} className="location-icon-rent width-height-icon-user-measure" alt="" />
                <span className="small-text">{displayDefaultNumericValue(boatLength)} ft</span>
              </div>
            </div>
          )}
      </div>
    );
  }
}
RentSharedTrip.defaultProps ={

  isShowAllPage:false,
  isCustomWidth:false
}