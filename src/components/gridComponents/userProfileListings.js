import React from "react";
import uuid from "uuid/v4";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { UserProfileListing } from "./userProfileListing";



export class UserProfileListings extends React.Component {
  state = {
    selectedIndex:null,
  };

  handleClick = index => {
    const { selectedIndex } = this.state;

    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {
    const { dimension, itemsLength, data} = this.props;
    const { selectedIndex } = this.state;
    return (
        <>
        <div className="user-profile-listing-section d-flex flex-wrap">
          {data && data.length
            ? data.map((value, index) => {
                return (
                    itemsLength > index && (
                      <div key={uuid()} className="mt-3 mb-0">
                        <UserProfileListing
                          value={value}
                          dimension={dimension}
                          selectedIndex={selectedIndex}
                          handleClick={() => this.handleClick(index)}
                          index={index}
                        />
                      </div>
                  )
                );
              })
            : <div item xs={12} > No Record Found </div>}
        </div>
        {/* MIRAJ REMOVED BY ME */}
        {  (data && data.length) && data.length > 3 ? (
          
          <div className="mb-4">
            <h2>
            <a href="/related-boats" target="_blank" className="show-link mb-0 text-decoration-unset user-profile-show-all-link">
                Show all ({data.length})
                <KeyboardArrowRight />
              </a>
            </h2>
          </div>
        ) : <>
        
        </>}
      </>
    );
  }
}
UserProfileListings.defaultProps ={
  dimension: {
    width:380,
    height:280,
  }
}