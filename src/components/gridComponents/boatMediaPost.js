import React from "react";

import UserContext from "../../UserContext";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { Col, Row } from "react-bootstrap";
import { dimension } from "../../util/enums/enums";
import { getHeightRatio, getRatio } from "../../helpers/ratio";
import { BoatMediaMainStyle } from "../styleComponent/styleComponent";

export class BoatMediaPost extends React.Component {
  state = {
    height:100,
    width: 100
  };

  static contextType = UserContext;


  componentDidMount() {
    const width = getRatio(dimension, "boatMediaPost", ".section-heading");
    const height = getRatio(
      dimension,
      "boatMediaPost",
      ".section-heading"
    );

    this.setState({ width, height });
  }
  render() {
    const { dimension, value } = this.props;
    const { height, width } = this.state;

    return (
      <>
      <div className="mt-2 mb-2 d-flex">
        <div>
      <BoatMediaMainStyle
            bgHeight={height}
            bgWidth={width}
            img={value.img}
          ></BoatMediaMainStyle>
       </div>
        <div className="pr-0 pl-3 d-flex align-items-center">
          <div className="boat-media-aricle-user-desc d-flex flex-column">
            <span className="font-weight-400 aricle-user-title col-gray f-15 gray-dark">
            {value.postTitle}
            </span>
            <span className="font-weight-400 aricle-user-title gray-light mt-2 font13">
            {value.postDate}
            </span>
          </div>
          </div>
        </div>
      </>
    );
  }
}
