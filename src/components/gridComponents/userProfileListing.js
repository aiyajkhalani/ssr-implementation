import React from "react";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import UserContext from "../../UserContext";
import {
  dimensionAspectRatio,
  commonBoatType,
  priceFormat,
  commonBoatTypeProfile
} from "../../util/utilFunctions";
import { dimension } from "../../util/enums/enums";
import { getRatio, getHeightRatio, getWidthRatio } from "../../helpers/ratio";

import {
  BoatMediaMainStyle,
  BoatMediaWidthStyle
} from "../styleComponent/styleComponent";
import Rating from "react-rating";
import SharePopup from "../share/SharePopup";

export class UserProfileListing extends React.Component {
  state = {
    height: dimension.userProfileListing.height,
    width: dimension.userProfileListing.width
  };

  static contextType = UserContext;

  componentDidMount() {
    const width = getRatio(
      dimension,
      "userProfileListing",
      ".user-profile-listing-section"
    );
    const height = getHeightRatio(
      dimension,
      "userProfileListing",
      ".user-profile-listing-section"
    );

    this.setState({ width, height });
  }

  render() {
    const { dimension, value, selectedIndex, index, handleClick } = this.props;
    const { height, width } = this.state;

    return (
      <>
        <div className="position-relative cursor-pointer search-inner-show-all-div">
          <BoatMediaMainStyle
            bgHeight={height}
            bgWidth={width}
            img={
              value &&
              value.images &&
              value.images.length &&
              encodeURI(value.images[0])
            }
          ></BoatMediaMainStyle>
          <div className="card-action justify-content-between">
            <div>{commonBoatTypeProfile(value)}</div>
            {/* <div>{commonBoatType("Used")}</div> */}

            <div class="share-icon">
              <div class="heart-button">
                <i class="adam-heart-1"></i>
              </div>
              <SharePopup
                handleClick={handleClick}
                index={index}
                selectedIndex={selectedIndex}
              />
            </div>
          </div>
          <BoatMediaWidthStyle bgWidth={width}>
            <div className=" d-flex flex-column pb-2">
              <h6 className="mb-0 home-boat-price mt-3 gray-dark font-weight-bold">
                INR {priceFormat(value.price)}
              </h6>
              <div className="user-listing-spacing d-flex flex-column">
                <span className="rentInner-userListing-textBrown dark-silver f-15 font-weight-500">
                  {value.yearBuilt}
                  <span className="ml-1 mr-1"> {value.boatType.name}</span>/
                  {value.lengthInFt} ft
                </span>
                <span className="user-profile-country dark-silver f-15 font-weight-400">
                  {value.city} , {value.country}
                </span>
              </div>
              <span className="user-profile-manufacturer dark-silver font-14 font-weight-400">
                {/* need to change it with firstName */}
                by {value.manufacturedBy}
              </span>

              <div className="d-flex align-items-center">
                <div className="mr-1 rating-small-size">
                  <Rating
                    className="rating-clr"
                    initialRating={0}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <span className="rating-count font-12"> {value.rating} </span>
              </div>
            </div>
          </BoatMediaWidthStyle>
        </div>
      </>
    );
  }
}
