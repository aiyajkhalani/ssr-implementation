import React from "react";
import Grid from "@material-ui/core/Grid";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import { BoatOptionBox } from "./boatOptionBox";

import "../../containers/rent/rent.scss";

export class BoatOption extends React.Component {
  state ={
    selectedIndex:null
  }
  handleClick = index => {
    const { selectedIndex } = this.state;
    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };
  render() {
    const { xs, sm, boatByCity, dimension, itemsLength, showType } = this.props;
    const { selectedIndex } = this.state

    return (
      <>
        <Grid container spacing={2}>
          {boatByCity && boatByCity.length
            ? boatByCity.map((value, index) => {
              return (
                itemsLength > index && (
                  <Grid key={value.id} item xs={xs} sm={sm}>
                    <BoatOptionBox
                      value={value}
                      index={index}
                      selectedIndex={selectedIndex}
                      dimension={dimension}
                      handleClick={() => this.handleClick(index)}
                    />
                  </Grid>
                )
              );
            })
            : <Grid item xs={12} > No Record Found </Grid>}
        </Grid>
        {(boatByCity && boatByCity.length) && boatByCity.length > itemsLength ? (

          <div className="mt-0">
            <h2>
              <a href={`show-all-rent/${showType}`} target="_blank" className="show-link mb-0 text-decoration-unset">
                Show all ({boatByCity.length})
                <KeyboardArrowRight />
              </a>
            </h2>
          </div>
        ) : <></>}
      </>
    );
  }
}
BoatOption.defaultProps = {
  dimension: {
    width: 380,
    height: 280,
  }
}