import React from "react";
import uuid from "uuid/v4";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { BoatMediaReview } from "./boatMediaReview";

export class BoatMediaReviews extends React.Component {
  render() {
    const { xs, sm, boatMediaReviews, dimension, itemsLength } = this.props;
    return (
      <>
        <Grid container className="boat-media-review-div">
          {boatMediaReviews && boatMediaReviews.length ? (
            boatMediaReviews.map((value, index) => {
              return (
                itemsLength > index && (
                  <Grid key={uuid()} item xs={xs} sm={sm}>
                    <BoatMediaReview
                      value={value}
                      xs={xs}
                      sm={sm}
                      dimension={dimension}
                      isBorder={boatMediaReviews.length - 1 === index}
                    />
                  </Grid>
                )
              );
            })
          ) : (
            <Grid item xs={12}>
              No Record Found
            </Grid>
          )}
        </Grid>
        {boatMediaReviews &&
        boatMediaReviews.length &&
        boatMediaReviews.length > itemsLength ? (
          <div className="mt-50">
            <h2>
              <Link to={"#"} className="show-link mb-0 text-decoration-unset">
                Show all ({boatMediaReviews.length})
                <KeyboardArrowRight />
              </Link>
            </h2>
          </div>
        ) : (
          <></>
        )}
      </>
    );
  }
}
BoatMediaReviews.defaultProps = {
  dimension: {
    width: 380,
    height: 280
  }
};
