import React from "react";
import { Grid } from "@material-ui/core";
import Truncate from "react-truncate";
import { connect } from "react-redux";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";

import UserContext from "../../UserContext";
import { getMostViewedBoatServices } from "../../redux/actions";
import { pagination, dimension } from "../../util/enums/enums";
import { Layout } from "../layout/layout";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import {
  CommonAdamSeaStyle
} from "../styleComponent/styleComponent";
import { PaginationBar } from "../pagination/PaginationBar";
import SharePopup from "../share/SharePopup";
import { viewServiceBoatHandler } from "../../helpers/boatHelper";
import { dimensionAspectRatio } from "../../util/utilFunctions";

class ShowAllBoatServiceGallery extends React.Component {
  state = {
    width: 380,
    height: 280
  };

  getNewWidth = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5 - 15;
    const aspectRatio = dimensionAspectRatio(275, 350);
    const newWidth = actualWidth / aspectRatio - 5;

    return newWidth;
  };

  getNewHeight = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5;
    const aspectRatio = dimensionAspectRatio(275, 350);
    const newHeight = actualWidth / aspectRatio - 5;

    return newHeight;
  };

  static contextType = UserContext;

  componentDidMount() {
    const { getMostViewedBoatServices } = this.props;

    const width = getRatio(dimension, "homeRecommended", ".show-all-container");
    const height = getHeightRatio(
      dimension,
      "homeRecommended",
      ".show-all-container"
    );
    this.setState({ width, height });

    getMostViewedBoatServices({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context && this.context.country
    });
  }

  render() {
    const { mostViewedBoatServices, mostViewedBoatServicesTotal } = this.props;
    const { country } = this.context;

    const height = this.getNewHeight();
    const width = this.getNewWidth();
    const { history } = this.context;
    const { selectedIndex } = this.state;

    return (
      <Layout isFooter>
      <Grid
        container
        spacing={2}
        className="boat-service-layout mt-3 pl-3 pr-3"
      >
        <Grid item sm={12} className="section-heading"> </Grid>
        {mostViewedBoatServices &&
          mostViewedBoatServices.length &&
          mostViewedBoatServices.map((value, index) => {
            return (
              <>
                <Grid key={value.id} item sm={3}>
                  <div className="boat-box mt-2">
                    <div className="boat-box-shadow ">
                      <div className="card-action top-service-share-icon">
                        <div class="share-icon">
                          <div className="heart-button">
                            <i className="adam-heart-1"></i>
                          </div>
                          <SharePopup
                            handleClick={() => this.shareHandleClick(index)}
                            index={index}
                            selectedIndex={selectedIndex}
                          />
                        </div>
                      </div>
                      <CommonAdamSeaStyle
                        height={height}
                        width={width}
                        className="w-100"
                        img={
                          value.images &&
                          value.images.length &&
                          encodeURI(value.images[0])
                        }
                      ></CommonAdamSeaStyle>
                      <CommonAdamSeaStyle
                        width={width}
                        className="w-100"
                        onClick={() =>
                          viewServiceBoatHandler(value.id, history)
                        }
                      >
                        <div className="boat-info">
                          <div>
                            <h4 className="boat-title mt-3 mb-0 f-16 boat-service-viewed-count dark-silver font-weight-500">
                              <VisibilityOutlinedIcon /> {value.userCount}
                            </h4>
                          </div>

                          <div>
                            <h5 className="mt-2 mb-2 team-commitment-text boat-service-viewed-title font-weight-500 boat-rated-service-commitment">
                              <Truncate lines={2} ellipsis={<span>..</span>}>
                                {value.teamCommitment}
                              </Truncate>
                            </h5>
                          </div>

                          <Grid container>
                            <Grid item sm={12}>
                              <h4 className="boat-owner mt-0 f-15 boat-service-viewed-desc gray-light font-weight-500">
                                <Truncate lines={2} ellipsis={<span>..</span>}>
                                  {value.service}
                                </Truncate>
                              </h4>
                            </Grid>
                            <Grid container>
                              <Grid item sm={12}>
                                <div className="d-flex mt-1 justify-content-between">
                                  <div className="d-flex align-items-center">
                                    <img
                                      src={
                                        value.images &&
                                        value.images.length &&
                                        value.images[0]
                                      }
                                      className="view-service-img-salesman"
                                      alt=""
                                    />

                                    <div className="salesman-name f-15 boat-service-viewed-user font-weight-500 dark-silver">
                                      By {value.salesManFullName}
                                    </div>
                                  </div>
                                  <div className="boat-service-viewer-section">
                                    {value.user && value.user.companyLogo && (
                                      <div className="view-service-reviewer-div d-flex">
                                        <img
                                          src={value.user.companyLogo}
                                          className="h-100 width-100"
                                          alt=""
                                        />
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </Grid>
                            </Grid>
                          </Grid>
                        </div>
                      </CommonAdamSeaStyle>
                    </div>
                  </div>
                </Grid>
              </>
            );
          })}
        {mostViewedBoatServicesTotal > pagination.PAGE_RECORD_LIMIT && (
          <div className="boat-pagination w-100">
            <PaginationBar
              action={mostViewedBoatServices}
              value={{ country }}
              totalRecords={mostViewedBoatServicesTotal}
            />
          </div>
        )}
      </Grid>
  </Layout>
    );
  }
}

const mapStateToProps = state => ({
  mostViewedBoatServices:
    state.boatServiceReducer && state.boatServiceReducer.mostViewedBoatServices,
  mostViewedBoatServicesTotal:
    state.boatServiceReducer && state.boatServiceReducer.total
});

const mapDispatchToProps = dispatch => ({
  getMostViewedBoatServices: data => dispatch(getMostViewedBoatServices(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowAllBoatServiceGallery);
