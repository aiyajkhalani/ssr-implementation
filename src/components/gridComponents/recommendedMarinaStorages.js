import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import { SingleMarinaDetail } from "../marinaStorage/singleMarinaDetail";
import { ShowAllLink } from "../helper/showAllLink";

export class RecommendedMarinaStorages extends React.Component {

  render() {
    const {
      recommendedMarinaStorages,
      showAllText,
      textColor
    } = this.props;
    return (
      <>
        <Grid container spacing={2}>
          {recommendedMarinaStorages &&
            recommendedMarinaStorages.map((value, index) => {
              return (
                <Fragment key={value.id}>
                  {index < 8 ? (
                    <SingleMarinaDetail
                      value={value}
                      index={index}
                    />
                  ) : (
                      <> </>
                    )}
                </Fragment>
              );
            })}
        </Grid>
        <ShowAllLink 
          data={recommendedMarinaStorages} 
          itemsLength={8} 
          showAllText={showAllText} 
          url={"/show-all-marina-and-storage/recommended"} />

      </>
    )
  }
}
