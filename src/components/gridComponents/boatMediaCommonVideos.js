import React from "react";
import uuid from "uuid/v4";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import { BoatMediaCommonVideo } from "./boatMediaCommonVideo";


export class BoatMediaCommonVideos extends React.Component {

  render() {
    const { boatMediaCommonVideos,dimension, itemsLength} = this.props;
    return (
      <>
        <div className="common-video d-flex justify-content-between">
          {boatMediaCommonVideos && boatMediaCommonVideos.length
            ? boatMediaCommonVideos.map((value, index) => {
                return (
                  itemsLength > index && (
                  <div key={uuid()} className="mt-3">
                    <BoatMediaCommonVideo
                      value={value}
                      dimension={dimension}
                    />
                  </div>
                  )
                );
              })
            : <div > No Record Found </div>}
        </div>
        {  (boatMediaCommonVideos && boatMediaCommonVideos.length) && boatMediaCommonVideos.length > itemsLength ? (
          
          <div className="mt-50">
            <h2>
              <Link to={'#'} className="show-link mb-0 text-decoration-unset">
                Show all ({boatMediaCommonVideos.length})
                <KeyboardArrowRight />
              </Link>
            </h2>
          </div>
        ) : <>
        
        </>}
      </>
    );
  }
}
BoatMediaCommonVideo.defaultProps ={
  dimension: {
    width:380,
    height:280,
  }
}