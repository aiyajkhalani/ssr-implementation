import React, { Component, Fragment } from "react";
import uuid from "uuid/v4";
import { Grid, Box, FormControlLabel, Switch } from "@material-ui/core";

import { BoatCard } from "../../components/";
import GoogleMarker from "../../components/map/marker";
import {
  viewBoatHandler,
  getBoatMarkers,
  viewRentBoatHandler,
  getBoatShowMarker,
  getRentShowMarkers,
  getBoatServiceMarkers,
  viewMarinaDetails
} from "../../helpers/boatHelper";
import UserContext from "../../UserContext";
import { resultMessage, dimension } from "../../util/enums/enums";
import { MarinaStorageCard } from "../marinaStorage/marinaStorageCard";
import { RentBoatCard } from "../boat/rentBoatCard";
import { getMarinaStorageMarker } from "../../helpers/marinaHelper";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import ShowBoatCards from "./showBoatCards";
import { BoatShowCard } from "../boat/boatShowCard";
import {
  SectionHeadingShowStyle,
  MapShowStyle
} from "../styleComponent/styleComponent";
import { BoatAddedService } from "./boatAddedService";
import { BoatServiceMapCard } from "../boat/boatServiceCard";
import { BoatDetail } from "../home/singleComponents/boatDetail";
import { RentSharedTrip } from "./rentSharedTrip";
import { SingleMarinaDetail } from "../marinaStorage/singleMarinaDetail";

class BoatListingsWithMap extends Component {
  static contextType = UserContext;

  state = {
    width: 380,
    height: 280,
    top: 95,
    isFlag: false,
    mapShowTop: 169
  };

  componentDidMount() {
    const { boats } = this.props;
    const width = getRatio(dimension, "showAllGrid", ".section-grid");
    const topSelector = document.querySelector(".header-responsive");

    const top = topSelector.offsetHeight;

    const height = getHeightRatio(dimension, "showAllGrid", ".section-grid");

    this.setState({ width, height, top });

    setTimeout(() => {
      if (boats && !boats.length) {
        this.setState({ isFlag: true });
      }
    }, 800);
  }

  mapShowTopHandler = () => {
    const { top } = this.state;
    const { toggleMapHandler } = this.props;

    const mapTop = document.querySelector(".section-heading-show");
    const mapShowTop = mapTop.offsetHeight + top;
    mapShowTop && this.setState({ mapShowTop });
    toggleMapHandler();
  };

  render() {
    const {
      boats,
      message,
      showMap,
      isMarinaStorage,
      isRent,
      isNotHome,
      isBoatShow,
      boatsType,
      isShowAuction,
      boatsTypeCount,
      isBoatService
    } = this.props;

    const { width, height, top, isFlag, mapShowTop } = this.state;
    const { history } = this.context;

    return (
      <Grid
        className={
          isRent
            ? "show-all-secondary section-grid"
            : "show-all-primary section-grid"
        }
        container
        justify="center"
      >
        {boats && boats.length ? (
          <Grid item xs={12}>
            <SectionHeadingShowStyle top={top}>
              <Box
                className="section-heading-show"
                display="flex"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0}
              >
                <label className="label show-name">
                  {boatsType}{" "}
                  <span className="count-boats-types">{boatsTypeCount}</span>
                </label>

                <FormControlLabel
                  className={
                    isMarinaStorage
                      ? `show-marina-map-switch`
                      : `show-map-switch`
                  }
                  control={
                    <Switch
                      checked={showMap}
                      onChange={this.mapShowTopHandler}
                      value="showMap"
                      color="primary"
                    />
                  }
                  label="Show Map"
                />
              </Box>
            </SectionHeadingShowStyle>
            {!showMap && (
              <Grid className="without-show-map home-page boat-show-with-map" container justify="center">
                <Grid container spacing={2}>
                  {isNotHome && !isMarinaStorage && !isRent && !isBoatService && (

                    boats && boats.length && boats.map((value, index) => {
                      return (
                        <Fragment key={value.id}>
                          <BoatDetail
                            value={value}
                            isTopLabel
                            width={width}
                            height={height}
                            index={index}
                            history={history}
                            xs={12}
                            sm={3}
                          />
                        </Fragment>
                      );
                    })
                  )}
                  {isRent && (

                    <Grid className="rent-show-all-grid" container spacing={2}>
                      {boats && boats.length && boats.map((value, index) => {
                        return (

                          <Grid key={value.id} item xs={12} sm={3}>
                            <RentSharedTrip
                              value={value}
                              index={index}
                              dimension={dimension.showAllGrid}
                              isShowAllPage
                              isCustomWidth
                            />
                          </Grid>
                        );
                      })
                      }
                    </Grid>
                  )}

                  {isMarinaStorage && (
                    <div className="marina-storage-layout show-marina-storage-layout w-100">
                      <Grid className="most-popular-main cursor-pointer marina-ads-sections" container spacing={2}>
                        {boats && boats.length && boats.map((value, index) => {
                          return (<SingleMarinaDetail
                            value={value}
                            index={index}
                          />)
                        })}
                      </Grid>
                    </div>
                  )}
                  {isBoatShow && (
                    <ShowBoatCards
                      xs={3}
                      sm={3}
                      isSearch
                      showBoatCards={boats}
                    />
                  )}
                  {isBoatService && boats && boats.map((value, index) => {
                    return (
                      <>
                        <Grid container spacing={2} className="boat-service-layout mt-3 pl-3 pr-3">
                          <Grid item sm={12} className="section-heading"> </Grid>
                          <Grid key={value.id} item sm={4} className="home-page">
                            <BoatAddedService
                              index={index}
                              value={value}
                              sm={12}
                            />
                          </Grid>
                        </Grid>
                      </>
                    );
                  })
                  }
                </Grid>
              </Grid>
            )}

            {showMap && (
              <Grid container className="mt-70 ">
                <Grid item className="left-card pl-3 pr-3" xs={6}>
                  {isNotHome &&
                    !isMarinaStorage &&
                    !isRent &&
                    !isBoatService &&
                    boats.map((boat, i) => (
                      <BoatCard
                        index={i}
                        boat={boat}
                        key={uuid()}
                        onClick={() => viewBoatHandler(boat, history)}
                      />
                    ))}

                  {isRent &&
                    boats.map((boat, i) => (
                      <RentBoatCard
                        index={i}
                        boat={boat}
                        key={uuid()}
                        onClick={() => viewRentBoatHandler(boat, history)}
                      />
                    ))}
                  {isMarinaStorage &&
                    boats &&
                    boats.map((value, index) => {
                      return (
                        <>
                          <Grid key={uuid()} item xs={12}>
                            <MarinaStorageCard
                              marinaStorage={value}
                              xs={12}
                              index={index}
                              viewMarina={id => viewMarinaDetails(id)}
                              textColor="iconColor-marina"
                            />
                          </Grid>
                        </>
                      );
                    })}
                  {isBoatShow &&
                    boats &&
                    boats.map((boat, index) => {
                      return (
                        <>
                          <Grid key={boat.id} item xs={12}>
                            <BoatShowCard
                              boat={boat}
                              key={uuid()}
                              onClick={() => viewBoatHandler(boat, history)}
                            />
                          </Grid>
                        </>
                      );
                    })}
                  {isBoatService &&
                    boats &&
                    boats.map((boat, index) => {
                      return (
                        <>
                          <Grid key={boat.id} item xs={12}>
                            <BoatServiceMapCard
                              value={boat}
                              key={uuid()}
                              onClick={() => viewBoatHandler(boat, history)}
                            />
                          </Grid>
                        </>
                      );
                    })}
                </Grid>
                <Grid className="map-card-z-index" item xs={6}>
                  <div>
                    <MapShowStyle
                      top={mapShowTop}
                      className="show-map map-div-form google-marker-div"
                    >
                      {
                        <GoogleMarker
                          width={"100%"}
                          markers={
                            isBoatService ? getBoatServiceMarkers(boats) :
                              isBoatShow
                                ? getBoatShowMarker(boats)
                                : isMarinaStorage
                                  ? getMarinaStorageMarker(boats)
                                  : isRent
                                    ? getRentShowMarkers(boats)
                                    : getBoatMarkers(boats)
                          }
                        ></GoogleMarker>
                      }
                    </MapShowStyle>
                  </div>
                </Grid>
              </Grid>
            )}
          </Grid>
        ) : (
            <Grid item className="min-defualt-height" xs={10}>
              {isFlag && (
                <>
                  <Box
                    className="section-heading mb-15"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    No Results
                </Box>

                  <span>{message ? message : resultMessage.default}</span>
                </>
              )}
            </Grid>
          )}
      </Grid>
    );
  }
}
export default BoatListingsWithMap;

BoatListingsWithMap.defaultProps = {
  isRent: false,
  isNotHome: true,
  boatsType: "Items",
  boatsTypeCount: 0,
  isShowAuction: true
};
