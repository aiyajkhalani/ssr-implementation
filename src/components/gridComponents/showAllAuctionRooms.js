import React, { Fragment } from "react";
import { Grid } from "@material-ui/core";
import { connect } from "react-redux";

import UserContext from "../../UserContext";
import { pagination, defaultImage } from "../../util/enums/enums";
import { Layout } from "../layout/layout";
import { getAllAuctionRooms } from "../../redux/actions";
import { PaginationBar } from "../pagination/PaginationBar";
import { priceFormat } from "../../util/utilFunctions";

class ShowAllAuctionRooms extends React.Component {
  static contextType = UserContext;

  componentDidMount() {
    const { getAllAuctionRooms } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    };
    getAllAuctionRooms(data);
  }
  render() {
    const {
      allAuctionRooms,
      getAllAuctionRooms,
      allAuctionRoomCounts
    } = this.props;
    return (
      <Fragment>
        <Layout>
          <div className="container100">
            <Grid container spacing={3}>
              {allAuctionRooms && allAuctionRooms.length ? (
                allAuctionRooms.map(item => {
                  return (
                    <Grid item md={3} sm={4} key={item.id} className="mb-30">
                      <div className="boat-box">
                        <div className="boat-box-shadow ">
                          <div className="boat-img-box ">
                            <img
                              src={defaultImage}
                              className="boat-img carousel-img-fix"
                              alt="img"
                            />
                          </div>
                          <div className="boat-info">
                            <div className="yacht-country-company">
                              <div>
                                <h4 className="boat-title color-white f-500 mt-0">
                                  India
                                </h4>
                              </div>

                              <div>
                                <h5 className="mt-0 color-white boat-country">
                                  country
                                </h5>
                              </div>
                            </div>

                            <Grid container>
                              <Grid item sm={6} className="border-right mt-10">
                                <h5 className="mt-0 mb-0 bid-price">
                                  Starting Price
                                </h5>
                                <h5 className="mt-0 mb-0 boat-price">
                                  {item.boat && priceFormat(item.boat.price)}
                                </h5>
                              </Grid>
                              {/* last bid of auction */}
                              <Grid item sm={6} className="pl-50 mt-10 122">
                                <h5 className="mt-0 mb-0 bid-price">
                                  Current BID
                                </h5>
                                <h5 className="mt-0 mb-0 boat-price">
                                  INR 0000
                                </h5>
                              </Grid>
                            </Grid>
                          </div>
                        </div>
                      </div>
                    </Grid>
                  );
                })
              ) : (
                  <Grid item xs={12}>
                    {" "}
                    No Record Found{" "}
                  </Grid>
                )}
            </Grid>
          </div>
          {allAuctionRoomCounts > pagination.PAGE_RECORD_LIMIT && (
            <PaginationBar
              action={getAllAuctionRooms}
              totalRecords={allAuctionRoomCounts}
            />
          )}
        </Layout>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  allAuctionRooms: state.boatReducer && state.boatReducer.allAuctionRooms,
  allAuctionRoomCounts:
    state.boatReducer && state.boatReducer.allAuctionRoomCounts
});

const mapDispatchToProps = dispatch => ({
  getAllAuctionRooms: data => dispatch(getAllAuctionRooms(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowAllAuctionRooms);
