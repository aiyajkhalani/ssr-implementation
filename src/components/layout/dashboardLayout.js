import React from 'react';
import { Notification } from '..'
import Header from '../header/header';
import Footer from '../footer/footer'
import MobileMenu from '../header/MobileMenu';
import DashboardSidebar from '../../containers/dashboard/dashboardSidebar'

export const DashboardLayout = (props) => {

  return (
    <div>
    <>
      <Header />
      <MobileMenu />
      <Notification />
      <main className="dashboard-layout-responsive">
          <div className="width-100 d-flex dashboard-align">
          <DashboardSidebar />
          <div className="dashboard-right-div-bg width-100 bg-white">
        {props.children}
        </div>
        </div>
      </main>
      <hr className="mt-0 mb-0" />
      <div className="dashboard-footer">
      {!props.isFooter &&
      <Footer />
    }
    </div>
    </>
    </div>
  );
}

