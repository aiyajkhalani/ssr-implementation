import React, { useEffect, useState } from 'react';
import { Notification } from '../../components'
import Header from '../header/header';
import Footer from '../footer/footer'
import MobileMenu from '../header/MobileMenu';

export const Layout = (props) => {
  const { className, isHeader } = props
  const [height, setHeight] = useState(95);
  useEffect(() => {
  const selector = document.querySelector('.header-responsive');
  const height = selector.offsetHeight;
  setHeight(height)
  }, []);
  return (
    <>
      <Header customClassName={isHeader} />
      <MobileMenu />
      <Notification />
      <main style={{marginTop:height}} className={className} 
      // ref={mainRef}
      >
        {props.children}
      </main>
      {!props.isFooter &&
        <Footer />
      }
    </>
  );
}

Layout.defaultProps = {
  isHeader: ''
}