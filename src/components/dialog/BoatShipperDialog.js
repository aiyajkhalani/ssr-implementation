import React from 'react';

// dialog
import {
    Grid,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions
} from "@material-ui/core";

import { Row, Col } from "react-bootstrap";

import "./EstimateCost.scss";
import { defaultImage } from '../../util/enums/enums';

const BoatShipperDialog = (props) => {
    const { isOpen, handleClose, boatShipperList } = props

    return (
        <Dialog
            maxWidth="lg"
            open={isOpen}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" className="pb-2">
                <div className="d-flex align-items-center">
                    {/* <img
                        src={require("./checked_user.png")}
                        alt=""
                        className="mr-2 estimate-cost-user-icon"
                    /> */}
                    <span className="font-18">
                        AdamSea Certified Shippers
                 </span>
                </div>
            
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <Grid>
                        <Row className="mb-4">
                            {boatShipperList && boatShipperList.map((item) => <Col xs={4} className="mb-12">
                                <div className="shipper-div">
                                    <div className="shipper-logo-img">
                                        <img
                                            src={item.companyLogo || defaultImage}
                                            alt="company logo"
                                            className="h-100"
                                        />
                                    </div>
                                    <div className="d-flex flex-column">
                                        <span className="font-18 title-text font-weight-500 mb-1 mt-2">{item.companyName || '-'}</span>
                                        <div className="d-flex align-items-center">
                                            <span className="font-weight-500 font-14 gray-dark mr-2">Contact :</span>
                                            <span className="font-14 font-weight-400 dark-silver">{item.firstName} {item.lastName}</span>
                                        </div>
                                        <div className="d-flex align-items-center">
                                            <span className="font-weight-500 font-14 gray-dark mr-2">Mobile :</span>
                                            <span className="font-14 font-weight-400 dark-silver">{item.mobileNumber || '-'}</span>
                                        </div>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <div className="d-flex align-items-center">
                                            <span className="font-weight-500 font-14 gray-dark mr-2">Email :</span>
                                            <span className="font-14 font-weight-400 dark-silver">{item.email || '-'}</span>
                                        </div>
                                        <div className="d-flex align-items-center">
                                            <span className="font-weight-500 font-14 gray-dark mr-2">Country :</span>
                                            <span className="font-14 font-weight-400 dark-silver">{item.country || '-'}</span>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            )}
                        </Row>

                    </Grid>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <button
                    onClick={handleClose}
                    className="btn btn-xs btn-link-info text-decoration-none text-white boat-shipper-estimate-button pl-1 pr-1 p-1"
                    autoFocus
                >
                    Close
               </button>
            </DialogActions>
        </Dialog>
    );
}

export default BoatShipperDialog;