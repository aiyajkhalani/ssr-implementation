import React, { useState } from 'react';

// dialog
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import EmailIcon from '@material-ui/icons/Email';
import "./EstimateCost.scss";


const DialogCard = (props) => {
    const [costPerFeetFlag, toggleCostPerFeet] = useState(false);
    const { isOpen, onClose } = props;
    return (
        <Dialog
            maxWidth="md"
            open={isOpen}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <div className="close-button-right"
                onClick={onClose}
            >
                <i className="adam-close-round" style={{ color: 'grey', fontWeight: 'bold', fontSize: 15 }} />
            </div>
            <DialogTitle id="alert-dialog-title" className="darkBlue">
                {
                    "Estimate Cost"
                }
            </DialogTitle>
            <DialogContent>
                <div className="text-right">
                    <a href="#"><SaveAltIcon/></a> | <a href="#" id="send_email_a" className="cursor"><EmailIcon/></a>
                </div>
                <br />
                <strong>Get an approximated price of this boat from AdamSea (*shipment price excluded).</strong>

                <ul className="font-weight-light mt-20">
                    <li>Surveyor Cost per Feet : <span className="font-weight-light">$45.00</span>
                        <span className="cursor-pointer darkBlue f-9" onClick={() => toggleCostPerFeet(!costPerFeetFlag)}>{' '}What's it?</span>
                        {costPerFeetFlag && <div id="survey_desc" className="display_none bubble margin-bottom-0">
                            <small>
                                <b>Conduct full inspection survey for used yacht and boat and issue statues certificate. Calculated by (length * Cost/ft).</b>
                            </small>
                        </div>}
                    </li>
                    <li>Admin Fee : <span className="font-weight-light">$25.00</span></li>
                    <li>AdamSea Tax on Boat Price : <span className="font-weight-light"> %</span></li>
                    <li>PayPal Fee For Buyer Inside Canada: <span className="font-weight-light">2.9 % + $0.03</span></li>
                    <li>PayPal Fee For Buyer Outside Canada: <span className="font-weight-light">3.9 % + $0.03</span></li>
                </ul>

                <div className="col-md-12">
                    <p className="questionText"><b>How you would like to pay ?</b></p>
                    <p className="questionText"><input className="vertical_align_top" type="radio" name="payment_type" value="2" /> Credit Card </p>
                    <p className="questionText"><input className="vertical_align_top" type="radio" name="payment_type" value="2" /> Bank to bank money transfer</p>
                </div>
                <br />
                <table className="table table-bordered gray-dark">
                    <thead>
                        <tr>
                            <th className="text-center">Items</th>
                            <th className="text-center">Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><small>Original Item Cost</small></td>
                            <td className="text-right"><small>$8,999.00</small></td>
                        </tr>
                        <tr>
                            <td><small>Inspection and  Surveyor cost ( estimated )</small></td>
                            <td className="text-right">
                                <small>$11,250.00</small>
                            </td>
                        </tr>
                        <tr>
                            <td><small>Admin Fees</small></td>
                            <td className="text-right">
                                <small>$25.00</small>
                            </td>
                        </tr>
                        <tr>
                            <td><small>Payment Service Fees (Escrow)</small></td>
                            <td className="text-right">
                                <small>$2,000.00</small>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Total Estimated Cost</b></td>
                            <td className="text-right"><b>$22,274.00</b></td>
                        </tr>
                    </tbody>
                </table>
                <p className="text-left gray-dark">* Total estimated cost is an approximate and may change accordingly </p>
                <hr />
                <p className="font-weight-light gray-dark">
                    Never send or wire money to sellers or buyers. This includes mailing a cheque or using payment services like PayPal, Amazon Payments, Google Wallet/ Checkout, Canada Post, any international post, Western Union or MoneyGram. Typically, scammers will use these services and will even falsify documents from a legitimate company. It is best to avoid them entirely and keep things local in your AdamSea.com account! AdamSea will not be responsible for any transactions that may take place out of AdamSea.com.
                </p>
                <p className="text-right-dark"><span className="f-9">Powered by </span><span className="darkBlue">&nbsp; AdamSea</span></p>
            </DialogContent>
        </Dialog>
    );
}

export default DialogCard;