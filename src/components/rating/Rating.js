import React, { Component } from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Chip } from '@material-ui/core';
import './rating.scss'
class RatingComponent extends Component {
    constructor() {
        super()
        this.state = {
            value: 2
        }
    }
    render() {
        const { value } = this.state
        return (
            <div>
                <Box component="fieldset" mb={3} borderColor="transparent">
                    <Box fontSize={10}>Ad ID: SELLER96LV1NWF</Box>
                    <Box fontSize={18}>2000 Fishing Boat</Box>
                    <div className="rating-flex">
                        <Rating
                            name="simple-controlled"
                            value={value}
                            precision={0.5}
                            size="small"
                            onChange={(event, newValue) => {
                                this.setState({ value: newValue })
                            }}
                        />
                        <Box ml={2}>{value}</Box><Chip size="small" label="USD" />
                    </div>
                </Box>

            </div>
        );
    }
}
export default RatingComponent