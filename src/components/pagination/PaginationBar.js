import React from "react";
import ReactPaginate from "react-paginate";
import NavigateNextSharpIcon from '@material-ui/icons/NavigateNextSharp';
import { pagination } from "../../util/enums/enums";

import "./pagination.scss";

export const PaginationBar = props => {
  const { action, value, totalRecords } = props;

  const handlePageChange = pageNumber => {
    if (action) {
      action({
        ...value,
        page: pageNumber.selected + 1,
        limit: pagination.PAGE_RECORD_LIMIT
      });
    }
  };

  return (
    <>
      <div className="show-all-pagination">
        <ReactPaginate
          previousLabel={<NavigateNextSharpIcon/>}
          nextLabel={<NavigateNextSharpIcon/> }
          pageCount={totalRecords / pagination.PAGE_RECORD_LIMIT}
          marginPagesDisplayed={4}
          pageRangeDisplayed={2}
          onPageChange={handlePageChange}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
        />
      </div>
    </>
  );
};
