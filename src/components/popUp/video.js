import React from 'react';

import { Player } from 'video-react';

function Video(props) {
	return (
		<Player
			controls={false}
			playsInline
			poster={props.thumbnail}
			autoPlay
			src={props.videoUrl}
		/>
	);
}
export default Video;

Video.defaultProperty = {
	thumbnail: ''
}