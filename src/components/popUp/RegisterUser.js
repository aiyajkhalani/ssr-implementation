import React, { useState, useContext, useEffect } from "react";
import OutsideClickHandler from "react-outside-click-handler";
import {
  Grid,
  Button,
  Typography,
  Box,
  List,
  ListItem
} from "@material-ui/core";
import { Link } from "react-router-dom";
import UserContext from "../../UserContext";
import { userRoles } from "../../util/enums/enums";
import "./RegisterUser.scss";
import "../../../src/styles/loginDropdownResponsive.scss";

function RegisterUser(props) {
  function isShowPopUp(isOpen) {
    setIsOpen(isOpen);
    props.setSticky(isOpen);
  }
  const escFunction = (event)=> {
    if(event.keyCode === 27) {
      isShowPopUp(false);
    }
  }
  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);

    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);
  const [isOpen, setIsOpen] = React.useState(false);

  const { roles, history } = useContext(UserContext);

  const registerHandler = roleName => {
    if (roles && roles.length) {
      const role = roles.find(item => item.aliasName === roleName);
      if (role) {
        history.push({
          pathname: "/register",
          search: `?type=${roleName}`,
          state: role
        });
      }
    }
  };

  const loginHandler = () => {
    history && history.push("/login");
  };

  const {
    BOAT_OWNER,
    BOAT_MANUFACTURER,
    BROKER_AND_DEALER,
    MEMBER,
    SURVEYOR,
    YACHT_SHIPPER,
    RENT_AND_CHARTER,
    SERVICE_AND_MAINTENANCE,
    MARINA_AND_STORAGE,
    AGENT
  } = userRoles;

  return (
    <OutsideClickHandler
      onOutsideClick={() => {
        isShowPopUp(false);
      }}
    >
      <Button className="header-small-font font-weight-400">
        <i className="adam-user-5 pr-5"></i>
        <span onClick={loginHandler} className="login-signup-header-link">Login</span>
        <span className="mr-1 ml-1">|</span>
        <span
          onClick={() => {
            isOpen ? isShowPopUp(false) : isShowPopUp(true);
          }}
          className="login-signup-header-link"
        >
          Signup
        </span>
      </Button>

      {isOpen && (
        <div
          className="login-dropdown login-register-div login-small-screen-dropdown"
          onMouseLeave={e => {
            isShowPopUp(false);
          }}
        >
          <div className="header-dropdown-main-div">
            {/* welcome image */}
            <div className="header-dropdown-left-div">
              <div className="welcome-login h-100">
                <div className="wc-image h-100">
                  {/* <img
                    className="width-100 "
                    src={require("../../assets/images/header/rent.jpg")}
                    alt="login"
                  /> */}
                </div>
                <div className="wc-caption header-dropdown-welcome-section login-dropdown-padding-top">
                  <Box
                    fontSize={18}
                    className="small-screen-font-14 color-dark-grey font-weight-500 login-dropdown-welcome-text"
                  >
                    Welcome!
                  </Box>
                  <Typography variant="caption" className="description-text">
                    <div>
                      <span className="header-dropdown-div small-screen-font-9 color-dark-grey login-dropdown-welcome-desc-text">
                        Login to see your favorite boats, exclusive offers and
                        more.
                      </span>
                    </div>
                  </Typography>
                  <div className="button-text">
                    <Button
                      className="popup-login small-screen-font-10 color-dark-grey login-dropdown-button"
                      size="small"
                      variant="outlined"
                      onClick={loginHandler}
                    >
                      Login
                    </Button>
                  </div>
                </div>
              </div>
            </div>

            {/* list text */}
            <div className="d-flex flex-column justify-content-center header-dropdown-right-div pl-4 pr-4">
              <Grid container className="header-dropdown-listing-div">
                <Grid item sm={4} className="p-15 login-dropdown-padding-top">
                  <Box
                    fontWeight="bold"
                    fontSize={16}
                    className="d-flex mb-4 registerListing-font small-screen-font-12 color-dark-grey login-register-dropdown login-dropdown-main-title"
                  >
                    Register in Our Market
                  </Box>

                  <Grid
                    container
                    className="register-listing-text cursor-pointer"
                  >
                    <Grid item sm={12}>
                      <Grid container spacing={3}>
                        <Grid item sm={6} className="register-user-header-drop-down">
                          <Box
                            fontSize={14}
                            letterSpacing={0.5}
                            fontWeight={500}
                            className="register-type small-screen-font-9 login-register-dropdown-links login-dropdown-title-div header-signup-dropdown-titles"
                          >
                            Seller
                          </Box>
                          <List className="listing-menu">
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(BOAT_OWNER)}
                            >
                              Boat Owner
                            </ListItem>
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(BROKER_AND_DEALER)}
                            >
                              Broker and Dealer
                            </ListItem>
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(BOAT_MANUFACTURER)}
                            >
                              Boat Manufacturer
                            </ListItem>
                          </List>
                        </Grid>
                        <Grid item sm={6} className="register-user-header-drop-down">
                       
                          <Box
                            fontSize={14}
                            letterSpacing={0.5}
                            fontWeight={500}
                            className="register-type small-screen-font-9 login-register-dropdown-links color-dark-grey login-dropdown-title-div header-signup-dropdown-titles"
                          >
                            Buyer
                          </Box>
                          <List className="listing-menu">
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(MEMBER)}
                            >
                              Member
                            </ListItem>
                          </List>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item sm={12} className="mt-15">
                      <Grid container spacing={3}>
                        <Grid item sm={6} className="register-user-header-drop-down">
                        <Box
                              fontSize={14}
                              letterSpacing={0.5}
                              fontWeight={500}
                              className="register-type small-screen-font-9 login-register-dropdown-links color-dark-grey login-dropdown-title-div header-signup-dropdown-titles"
                            >
                              Agent
                            </Box>
                            <List className="listing-menu">
                              <ListItem className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(AGENT)}
                              >
                                AdamSea Agent
                              </ListItem>
                            </List>
                      
                        </Grid>
                        <Grid item sm={6} className="register-user-header-drop-down">
                        <Box
                            fontSize={14}
                            letterSpacing={0.5}
                            fontWeight={500}
                            className="register-type small-screen-font-9 login-register-dropdown-links color-dark-grey login-dropdown-title-div header-signup-dropdown-titles"
                          >
                            Other Category
                          </Box>
                          <List className="listing-menu">
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(YACHT_SHIPPER)}
                            >
                              Yacht Shipper
                            </ListItem>
                            <ListItem
                              className="pl-0 registerListing-innerType register-markets-page-links small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text"
                              onClick={() => registerHandler(SURVEYOR)}
                            >
                              Surveyor
                            </ListItem>
                          </List>
                      
                        </Grid>
                      </Grid>
                      {/* <Grid item sm={12} className="mt-15">
                        <Grid container>
                          <Grid item sm={6} className="register-user-header-drop-down">
                         
                          </Grid>
                        </Grid>
                      </Grid> */}
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item sm={8} className="p-15 login-dropdown-padding-top">
                  <Box
                    fontWeight="bold"
                    fontSize={16}
                    className="d-flex mb-4 registerListing-font small-screen-font-12 color-dark-grey login-register-dropdown header-dropdown-signup-main-title"
                  >
                    AdamSea Market
                  </Box>
                  <Grid
                    container
                    spacing={2}
                    className="header-dropdown-market-section"
                  >
                    <Grid item sm={4} className="pt-0">
                      <ListItem
                        onClick={() => registerHandler(SERVICE_AND_MAINTENANCE)} className="header-dropdown-market-img"
                      >
                        <div className="adamseaMarket-img cursor-pointer">
                          <img
                            className="border-radius small-screen-dropdown-sections-img image-opacity"
                            src={require("../../assets/images/header/boat-service.jpg")}
                            alt="adamsea"
                          />
                        </div>
                        <div className="adamseaMarket-name d-flex mt-3 pl-3 justify-content-center">
                          <span className="small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text">
                            Boat Services
                          </span>
                        </div>
                      </ListItem>
                    </Grid>
                    <Grid item sm={4} className="pt-0">
                      <ListItem onClick={() => registerHandler(MARINA_AND_STORAGE)} className="header-dropdown-market-img">
                        <div className="adamseaMarket-img cursor-pointer">
                          <img
                            className="border-radius small-screen-dropdown-sections-img image-opacity"
                            src={require("../../assets/images/header/marina-storage.jpg")}
                            alt="adamsea"
                          />
                        </div>
                        <div className="adamseaMarket-name d-flex mt-3 pl-3 justify-content-center">
                          <span className="small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text">
                            Marina & Storage
                          </span>
                        </div>
                      </ListItem>
                    </Grid>
                    <Grid item sm={4} className="pt-0">
                      {/* <Link to="/rent"> */}
                      <ListItem onClick={(e) => registerHandler(RENT_AND_CHARTER)} className="header-dropdown-market-img">
                        <div className="adamseaMarket-img cursor-pointer">
                          <img
                            className="border-radius small-screen-dropdown-sections-img image-opacity"
                            src={require("../../assets/images/rent/rent-link.png")}
                            alt="adamsea"
                          />
                        </div>
                        <div className="adamseaMarket-name d-flex mt-3 pl-3 justify-content-center">
                          <span className="small-screen-font-10 login-register-dropdown-links dark-silver link-hover-text">
                            Rent
                          </span>
                        </div>
                      </ListItem>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      )}
    </OutsideClickHandler>
  );
}
export default RegisterUser;
