import React, { useState } from 'react';
import { Map, GoogleApiWrapper, Polygon } from 'google-maps-react';
import { Grid, Typography, Card, CardContent } from '@material-ui/core';
import { ErrorMessage } from 'formik';

import '../../styles/common.scss'

const GoogleMap = (props) => {

    const { id, isError, columnName, title, latLng, width, height, placeHolder, fetch, google, value, isUpdate, isCloseBtn, onClose, isDisplayLocationOnly, isShipperLocation, pathPoints } = props

    const [mapStyle, setMapStyle] = useState({
        width: (width) ? `${width}%` : '50%',
        height: (height) ? `${height}vh` : '50vh',
    })

    const [mapInfo, setMapInfo] = useState()

    const icon = {
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
    };

    const getInformation = (place) => {

        let result = {}

        result.placeName = place.name
        result.address = place.formatted_address
        result.latitude = place.geometry.location.lat()
        result.longitude = place.geometry.location.lng()

        if (place && place.address_components && place.address_components.length) {

            place.address_components.map(component => {

                const addressType = component.types[0]

                switch (addressType) {
                    case 'street_number':
                        result.streetNumber = component.long_name
                        break;
                    case 'route':
                        result.route = component.short_name
                        break;
                    case 'locality':
                        result.city = component.long_name
                        break;
                    case 'administrative_area_level_1':
                        result.state = component.long_name
                        break;
                    case 'country':
                        result.country = component.long_name
                        break;
                    case 'postal_code':
                        result.postalCode = component.long_name
                        break;

                    default:
                        break
                }
            })
        }

        fetch({ ...result })
        setMapInfo({ ...result })
    }

    const fetchPlaces = (mapProps, map) => {
        const { google } = mapProps

        const searchBox = new google.maps.places.SearchBox(document.getElementById((id) ? id : 'pac-input'))

        if (isUpdate) {
            const currentPosition = new google.maps.LatLng(latLng.lat, latLng.lng)

            new google.maps.Marker({
                map: map,
                icon: icon,
                title: value.address,
                position: currentPosition
            })
            setMapInfo({ ...value })
        }

        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds())
        });

        let markers = []

        searchBox.addListener('places_changed', function () {
            const places = searchBox.getPlaces()

            if (!places.length) {
                return
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null)
            });
            markers = []

            // For each place, get the icon, name and location.
            let bounds = new google.maps.LatLngBounds()
            places.forEach(function (place) {

                if (!place.geometry) {
                    return
                }

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }))

                getInformation(place)

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport)
                } else {
                    bounds.extend(place.geometry.location)
                }
            })
            map.fitBounds(bounds)
        })

    }

    const mapInfoDisplay = () => (
        <>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    Country: {mapInfo.country}
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    State: {mapInfo.state}
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    City: {mapInfo.city}
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    Postal Code: {mapInfo.postalCode}
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    Full Address: {mapInfo.address}
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography component="div" className="map-field">
                    Place: {mapInfo.placeName}
                </Typography>
            </Grid>
        </>
    )

    return (
        <>

            <div className="map-design">

                <Card
                    className="map-card-div map-border map-div">
                    {isCloseBtn &&
                        <div className="d-flex justify-content-end pt-3 map-close-btn-position cursor-pointer" onClick={onClose}>
                            <img src={require("../../components/footer/close.png")} className="register-map-close-btn" alt="" />
                        </div>
                    }

                    <CardContent className="map-card-content">
                        <Grid container >

                            {!isDisplayLocationOnly && !isShipperLocation &&
                                <Grid item xs={7} className="map-search-place-div">
                                    <input
                                        id={(id) ? id : 'pac-input'} name="username" type="text"
                                        className='form-control' placeholder={(placeHolder) || 'Search Box'}>
                                    </input>
                                    {props && (columnName && isError) && <span className="error-message"><ErrorMessage name={columnName} /></span>}
                                </Grid>
                            }

                            {mapInfo && mapInfoDisplay()}

                            <Grid item xs={12} className="position-relative map-location-show">
                                {!isShipperLocation ? <div style={mapStyle}>
                                    <Map
                                        google={google}
                                        zoom={8}
                                        style={mapStyle}
                                        initialCenter={latLng}
                                        onReady={fetchPlaces}
                                    >
                                    </Map>
                                </div>
                                    :
                                    <div style={mapStyle}>
                                        <Map
                                            google={google}
                                            zoom={8}
                                            style={mapStyle}
                                            initialCenter={latLng}
                                            onReady={fetchPlaces} >
                                            <Polygon
                                                paths={pathPoints}
                                                strokeColor="#0000FF"
                                                strokeOpacity={0.8}
                                                strokeWeight={2}
                                                fillColor="#0000FF"
                                                fillOpacity={0.35} />
                                        </Map>
                                    </div>
                                }
                            </Grid>

                        </Grid>
                    </CardContent>
                </Card>
            </div>
        </>
    );
}

export default GoogleApiWrapper(
    props => ({
        apiKey: process.env.REACT_APP_MAP_API_KEY,
        language: props.language
    })
)(GoogleMap)