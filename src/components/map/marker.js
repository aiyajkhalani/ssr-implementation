import React, { useState, useEffect } from "react";
import uuid from "uuid/v4";
import Rating from "react-rating";
import { Map, Marker, InfoWindow, GoogleApiWrapper } from "google-maps-react";

import { Grid, Box, Card, CardContent } from "@material-ui/core";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import "./map.scss";
import {
  MapPlaceInfoStyle,
  MapPlaceImgStyle
} from "../styleComponent/styleComponent";
import { priceFormat } from "../../util/utilFunctions";

const GoogleMarker = props => {
  const { markers } = props;

  const [mapStyle, setMapStyle] = useState({
    width: props.width ? `${props.width}vh` : "100%",
    height: props.height ? `${props.height}vh` : "100%"
  });

  const [bounds, setBounds] = useState(null);

  useEffect(() => {
    const bounds = new props.google.maps.LatLngBounds();
    markers &&
      markers.length > 0 &&
      markers.forEach(marker => {
        bounds.extend(marker && marker.position);
      });
    setBounds(bounds);
  }, [markers, props.google.maps.LatLngBounds]);

  const [info, setInfo] = useState({
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    marker: {}
  });

  const onMarkerClick = (props, marker, e, markerValue) => {
    setInfo({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
      marker: markerValue
    });
  };

  const displayMarkers = () =>
    markers &&
    markers.length > 0 &&
    markers.map(markerValue => {
      return (
        markerValue && (
          <Marker
            onClick={(props, marker, e) =>
              onMarkerClick(props, marker, e, markerValue)
            }
            key={uuid()}
            title={markerValue.title}
            position={markerValue.position}
            icon={{
              url: markerValue.icon.url,
              anchor: new props.google.maps.Point(32, 32),
              scaledSize: new props.google.maps.Size(23, 23)
            }}
          />
        )
      );
    });

  const getNewWidth = () => {
    const width = document.querySelector("body");
    const actualWidth = width && width.offsetWidth / 6.7;
    return actualWidth;
  };

  const getNewHeight = () => {
    const width = document.querySelector("body");
    const actualWidth = width && width.offsetWidth / 9;
    return actualWidth;
  };

  return (
    <div className="map-design">
      <Card className="card-style">
        <CardContent>
          <Grid container spacing={2} className="map-div-height">
            {props.title && (
              <Grid item xs={12} className="map-height">
                <Box fontSize={20} letterSpacing={1} fontWeight={600}>
                  {props.title}
                </Box>
              </Grid>
            )}
            <Grid item xs={12} className="map-main-div-grid">
              <div style={mapStyle} className="map-class position-relative">
                <Map
                  className="responsive-map map-with-info"
                  google={props.google}
                  zoom={8}
                  style={mapStyle}
                  bounds={bounds}
                >
                  {displayMarkers()}

                  <InfoWindow
                    marker={info.activeMarker}
                    visible={info.showingInfoWindow}
                  >
                    <MapPlaceInfoStyle bgWidth={getNewWidth}>
                      {info.marker.image && (
                        <MapPlaceImgStyle bgHeight={getNewHeight}>
                          <img
                            src={info.marker.image}
                            alt="logo"
                            className="h-100 w-100 map-market-img"
                          />
                        </MapPlaceImgStyle>
                      )}
                      <div className="d-flex flex-column p-3 responsive-map-content">
                        <span className="map-info-title-section mb-1">
                          {info.selectedPlace.title}
                        </span>
                        <span className="map-info-desc-section mb-1">
                          {info.marker.city && `${info.marker.city},`}{" "}
                          {info.marker.country}
                        </span>
                        {info.marker.rating > 0 &&
                          <div className="d-flex align-items-center mb-1">
                            <Rating
                              className="rating-clr mr-2"
                              initialRating={info.marker.rating}
                              fullSymbol={<StarIcon />}
                              emptySymbol={<StarBorderIcon />}
                              placeholderSymbol={<StarBorderIcon />}
                              readonly
                            />
                            <h6 className="rating-avg mb-0 pt-1">
                              ({info.marker.rating})
                          </h6>
                          </div>
                        }
                        {info.marker.price && (
                          <span className="map-info-price-section">
                            Price : ₹ {priceFormat(info.marker.price)}
                          </span>
                        )}
                      </div>
                    </MapPlaceInfoStyle>
                  </InfoWindow>
                </Map>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_MAP_API_KEY
})(GoogleMarker);
