import React from 'react'
import Autocomplete from 'react-google-autocomplete'
import { GoogleApiWrapper } from 'google-maps-react'
import { getPlaceInformation } from '../../helpers/geoLocation'
import './map.scss'

const SearchComplete = ({ getPlaceName, className, placeHolder }) => {

    const placeSelectHandler = (place) => {

        const formatedResult = getPlaceInformation(place)

        getPlaceName && getPlaceName(formatedResult)
    }

    return (
        <>
            <Autocomplete
                onPlaceSelected={placeSelectHandler}
                types={['(cities)']}
                placeholder={placeHolder || "Search by city"}
                className={className}
            />
        </>
    )
}

export default GoogleApiWrapper({
    apiKey: process.env.REACT_APP_MAP_API_KEY
})(SearchComplete)