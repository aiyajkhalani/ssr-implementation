import React from "react";
import { Grid } from "@material-ui/core";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";

import { HomPageVideoStyle } from "../styleComponent/styleComponent";

export const VideoComponent = React.memo(({ video, setVideoUrlHandler, videoWidth, videoHeight, className }) => {

    return (
        <Grid item xs={12} sm={4} className="video-common-layout">
            <div 
                className={`${className} position-relative overflow-hidden cursor-pointer`}
                onClick={() => setVideoUrlHandler()}
            >
                <HomPageVideoStyle
                    img={encodeURI(video.thumbnail)}
                    bgWidth={videoWidth}
                    bgHeight={videoHeight}
                >
                    <PlayCircleOutlineIcon className="playVideo-icon" />
                </HomPageVideoStyle>
            </div>

            <div>
                <h4 className="mt-3 mb-1 text-capitalize">
                    {video.title}
                </h4>
                <div dangerouslySetInnerHTML={{ __html: video.description }} />
            </div>
        </Grid>
    )
})