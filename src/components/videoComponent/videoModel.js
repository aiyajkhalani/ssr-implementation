import React from "react";
import OutsideClickHandler from "react-outside-click-handler";
import Video from "../../components/popUp/video";

export const VideoModel = React.memo(({ closeVideo,videoFlag,videoUrl,videoThumbnail }) => {

    return (
        <>
        {videoFlag && videoUrl && (
            <>
              <div className="video-modal open-video-modal">
    
                <OutsideClickHandler
                  onOutsideClick={() => {
                    closeVideo()
                  }}
                >
                  <div className="video-modal-cross" onClick={() =>closeVideo()}>X</div>
                  <Video
                    videoUrl={videoUrl}
                    thumbnail={videoThumbnail}
                    isOpen={true}
                  />
                </OutsideClickHandler>
              </div>
              <div className="bg-video-popup"></div>
              </>
          )}
        </>
    )
})