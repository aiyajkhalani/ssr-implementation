import React from 'react'
import "./home/home.scss";
import "./home/icon.scss";
import "../components/boat/boatCard.scss"

export const Icons = (props) => {
    return <i className={props.data}></i>
}
