import React, { useState, useEffect } from 'react';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from "yup";

import { requireMessage } from '../../helpers/string';
import { Grid, Box, Button, makeStyles, Backdrop, Modal, Fade } from '@material-ui/core';
import { Field } from '../ws/Field';
import { SellerInformation } from './SellerInformation';
import { salesEngineAgreementType } from '../../util/enums/enums';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        margin: theme.spacing(1),
    },
    link: {
        margin: theme.spacing(3),
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export const SalesEngineDetail = props => {
    const classes = useStyles();

    const { open, closeModal, value, onSubmit, salesEngines, selectedSalesEngineId, agreementsContents } = props

    const salesEngine = salesEngines && salesEngines.find((item) => (item.id === selectedSalesEngineId))

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={`${classes.paper} p-0 shipment-proposal`}>
                        <Grid container spacing={2}>

                            <Grid xs={12} item>
                                <Box
                                    fontSize={20}
                                    letterSpacing={1}
                                    fontWeight={600}
                                    className="shipment-proposal-desc-title sales-engin-header text-white"
                                >
                                    {'Sales Engine Details'}
                                </Box>
                            </Grid>
                            <div className="d-flex flex-column w-100">
                                <div className="shipment-proposal-desc-div ">
                                    <Grid container spacing={2}>
                                        Seller:
                                        <SellerInformation salesEngine={salesEngine} isSeller={true} />

                                        Buyer:
                                        <SellerInformation salesEngine={salesEngine} isSeller={false} />
                                    </Grid>

                                    <div className="mr-3 mb-5">
                                        <span className="stepper-submitted-report-info">
                                            <a href={agreementsContents.content} download>Download Agreement</a>
                                        </span>
                                    </div>
                                </div>
                                <div className="shipment-proposal-btn-div d-flex justify-content-end">
                                    <Button
                                        className="profile-button mt-3 mb-3 font-weight-400 font13 shipment-proposal-cancel-btn mt-0 mb-0"
                                        onClick={closeModal}
                                    >
                                        {'Cancel'}
                                    </Button>
                                </div>
                            </div>
                        </Grid>

                    </div>
                </Fade>
            </Modal>
        </>
    );
}