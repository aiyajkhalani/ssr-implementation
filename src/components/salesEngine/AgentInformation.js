import React from "react";
import { Row, Col, Modal, Button, ButtonToolbar } from "react-bootstrap";
import { FormControlLabel, Checkbox, Grid } from "@material-ui/core";
import SharePopup from "../share/SharePopup";
import { AgentListings } from "./AgentListings";

export const AgentInformation = props => {
  const [sharePopUpOpen, setSharePopUpOpen] = React.useState(false);
  const [modalShow, setModalShow] = React.useState(false);

  const { agent, salesEngine, assignBroker, setAgent } = props;

  function AgentListingsModal(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Agents
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

          {props.agents && props.agents.length && props.agents.map((item) => {
            return <AgentListings agent={item} setAgent={props.setAgent} setModalShow={setModalShow} salesEngine={salesEngine} />
          })}

        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  return (
    <>

      <div className="stepper-agent-info-div float-right mt-5 stepper-info-boat">
        {salesEngine.agent ? <div className="mt-3 pl-3 pr-3">
          <Grid container spacing={2}>
            <Grid item xs={3}>
              <div className="stepper-agent-img-div">
                <img
                  src={agent.image}
                  className="h-100 width-100"
                  alt=""
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <div>
                <div>
                  <span> {agent.firstName} {agent.lastName}</span>
                </div>
                {/* <div className="mt-2 mb-0 agent-div-margin-top">
                  <span className="dark-silver">
                    {agent.mobileNumber}
                  </span>
                </div> */}
                <div>
                  <div className="d-flex mt-2 mb-0 agent-div-margin-top">
                    <img
                      src={require("../userProfile/image/group-message.png")}
                      className="user-profile-social-icon-div cursor-pointer mr-2"
                      alt=""
                    />
                    <img
                      src={require("../userProfile/image/profile.png")}
                      className="user-profile-social-icon-div cursor-pointer mr-2"
                      alt=""
                    />
                    <img
                      src={require("../userProfile/image/share.png")}
                      className="user-profile-social-icon-div cursor-pointer"
                      alt=""
                      onClick={() => setSharePopUpOpen(true)}
                    />
                    {sharePopUpOpen && (
                      <SharePopup
                        handleClick={() => setSharePopUpOpen(false)}
                        useOwnIcon={true}
                      />
                    )}
                  </div>
                </div>
                <ButtonToolbar>
                  <div className="mt-3 mb-0 agent-div-margin-top">
                    <button
                      type="button"
                      name="next"
                      className="btn w-auto stepper-agent-btn"
                      onClick={() => setModalShow(true)}
                    >
                      Change
                  </button>
                  </div>
                  <AgentListingsModal
                    show={modalShow}
                    agents={props.agents}
                    setAgent={setAgent}
                    onHide={() => setModalShow(false)}
                  />
                </ButtonToolbar>
              </div>
            </Grid>
            <Grid item xs={3}>
              <div>
                {agent.documentVerification &&
                  agent.documentVerification.commercialLicenceVerified
                  && agent.documentVerification.emailVerified
                  && agent.documentVerification.mobileVerified
                  && agent.documentVerification.governmentIdVerified &&
                  <div>
                    <img
                      src={require("../userProfile/image/verified.png")}
                      className="stepper-user-profile-icon-div mr-2"
                      alt=""
                    />
                    <span className="rentInner-userTextH4  font-16 dark-silver">
                      Verified
                  </span>
                  </div>}
                {agent.email && <div className="mt-2 mb-0 agent-div-margin-top">
                  <img
                    src={require("../userProfile/image/vector.png")}
                    className="stepper-user-profile-icon-div mr-2"
                    alt=""
                  />
                  <span className="rentInner-userTextH4  font-16 dark-silver">
                    Email address
                  </span>
                </div>}
                {agent.mobileNumber && <div className="mt-2 mb-0 agent-div-margin-top">
                  <img
                    src={require("../userProfile/image/vector.png")}
                    className="stepper-user-profile-icon-div mr-2"
                    alt=""
                  />
                  <span className="rentInner-userTextH4  font-16 dark-silver">
                    Phone number
                  </span>
                </div>}
              </div>
            </Grid>
            <Grid item xs={3}>
              <div>
                <div className="logo-box m-auto">
                  <img
                    src={agent.companyLogo}
                    height={50}
                    width={50}
                    alt=""
                  />
                </div>
                {/* )} */}
                <div className="company-name dark-silver  font-16 text-center">
                  {agent.companyName}
                </div>
              </div>
            </Grid>
          </Grid>
        </div> : <div className="pl-3 pr-3 stepper-asign-agent-div">
            <FormControlLabel
              control={
                <Checkbox onChange={(value) => assignBroker(value.target.value)} />
              }
              label=" Assign an agent to help you to buy this boat?"
            />
          </div>}
      </div>

    </>
  );
};
