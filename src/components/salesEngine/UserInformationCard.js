import React from "react";
import { Row, Col } from "react-bootstrap";
import { Grid } from "@material-ui/core";

import '../../containers/salesEngine/process/SalesEngineStepper.scss'
import { priceFormat } from "../../util/utilFunctions";
export const UserInformationCard = props => {

    const {
        userInfo,
        showSurveyorButtons,
        surveyorInfo,
        requestSurveyorHandler,
        declineSurveyor,
        isSurveyorRequested,
        proposalInfo,
        showShipperButtons,
        salesEngine,
        shipperAcceptRequest,
        shipperUserProfile,
        surveyorUserProfile,
        xs
    } = props;

    return (
        <>
            {/* <Grid container spacing={3}> */}
            <Grid item xs={xs ? xs : 4} className="mt-3">
                <div className="rentInnner-userCard">
                    <Row>
                        <Col xs={6} className="user-info-profile pr-4">
                            <div>
                                <div className="mt-4 mb-4">
                                    <div className="mt-3 mb-2 d-flex align-items-center">
                                        <img
                                            src={require("../../components/userProfile/image/profile.png")}
                                            className="user-profile-icon-div mr-2"
                                            alt=""
                                        />
                                        <span className="rentInner-userTextH4 text-transform-capitalize font13 dark-silver">
                                            {userInfo.role && userInfo.role.role}
                                        </span>
                                    </div>
                                    <div className="mb-2 d-flex align-items-center">
                                        <img
                                            src={require("../../components/userProfile/image/verified.png")}
                                            className="user-profile-icon-div mr-2"
                                            alt=""
                                        />
                                        <span className="rentInner-userTextH4 font13 dark-silver">
                                            Verified
                                        </span>
                                    </div>

                                </div>
                                <hr />
                                <div className="mt-4">
                                    <h6 className="mb-3 font13">
                                        {userInfo.firstName} has provided
                                        </h6>
                                    {userInfo.documentVerification &&
                                        userInfo.documentVerification
                                            .governmentIdVerified && (
                                            <div className="mb-2 d-flex align-items-center">
                                                <img
                                                    src={require("../../components/userProfile/image/vector.png")}
                                                    className="user-profile-icon-div mr-2"
                                                    alt=""
                                                />
                                                <span className="rentInner-userTextH4 font13 dark-silver">
                                                    Government ID
                                                </span>
                                            </div>
                                        )}
                                    {userInfo.documentVerification &&
                                        userInfo.documentVerification.emailVerified && (
                                            <div className="mb-2 d-flex align-items-center">
                                                <img
                                                    src={require("../../components/userProfile/image/vector.png")}
                                                    className="user-profile-icon-div mr-2"
                                                    alt=""
                                                />
                                                <span className="rentInner-userTextH4 font13 dark-silver">
                                                    Email address
                                                </span>
                                            </div>
                                        )}
                                    {userInfo.documentVerification &&
                                        userInfo.documentVerification.mobileVerified && (
                                            <div className="mb-2 d-flex align-items-center">
                                                <img
                                                    src={require("../../components/userProfile/image/vector.png")}
                                                    className="user-profile-icon-div mr-2"
                                                    alt=""
                                                />
                                                <span className="rentInner-userTextH4 font13 dark-silver">
                                                    Phone number
                                                    </span>
                                            </div>
                                        )}
                                    {userInfo.documentVerification &&
                                        userInfo.documentVerification
                                            .commercialLicenceVerified && (
                                            <div className="mb-2 d-flex align-items-center">
                                                <img
                                                    src={require("../../components/userProfile/image/vector.png")}
                                                    className="user-profile-icon-div mr-2"
                                                    alt=""
                                                />
                                                <span className="rentInner-userTextH4 font13 dark-silver">
                                                    Commercial Licence
                                                    </span>
                                            </div>
                                        )}
                                </div>
                            </div>
                        </Col>
                        <Col xs={6}>
                            <div className="d-flex justify-content-center align-items-center">
                                <div className="mb-3 mt-1">
                                    <div className="rentInner-userImg rounded-circle user-profile-online-section">
                                        <img
                                            className="rounded-circle"
                                            src={
                                                userInfo.image ||
                                                require("../../assets/images/rentInner/img-6.jpg")
                                            }
                                            // src={require(defaultImageForUser)}
                                            alt="placeholder"
                                        />
                                        <div className="user-online">
                                            <div className="online-div-user"></div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-evenly mt-3">
                                        <img
                                            src={require("../../components/userProfile/image/group-message.png")}
                                            className="user-profile-social-icon-div cursor-pointer"
                                            alt=""
                                        />
                                        <img
                                            src={require("../../components/userProfile/image/profile.png")}
                                            className="user-profile-social-icon-div cursor-pointer"
                                            alt=""
                                        />
                                        <img
                                            src={require("../../components/userProfile/image/share.png")}
                                            className="user-profile-social-icon-div cursor-pointer"
                                            alt=""
                                        />
                                    </div>
                                    {userInfo.companyLogo && (
                                        <div className="logo-box">
                                            <img
                                                src={userInfo.companyLogo}
                                                height={50}
                                                width={50}
                                                alt=""
                                            />
                                        </div>
                                    )}
                                    <div className="company-name dark-silver font13 text-center">
                                        {userInfo.companyName}
                                    </div>
                                </div>
                            </div>
                            {showSurveyorButtons && <>
                                <div className="mt-2 mb-0">
                                    <span className="stepper-nearest-surveyor-distance">
                                        Away from boat: {surveyorInfo.distance}
                                    </span>
                                </div>
                                <div className="mt-2 mb-0">
                                    <span className="stepper-nearest-surveyor-distance">
                                        Price: {surveyorInfo.pricePerFt}/ft
                                        </span>
                                </div>
                            </>}
                            {surveyorUserProfile && <>
                                <div className="mt-2 mb-0">
                                    <span className="stepper-nearest-surveyor-distance">
                                        Price: {surveyorInfo.pricePerFt}/ft
                                        </span>
                                </div>
                            </>}
                            {/* {showShipperButtons && <>
                                <div className="mt-2 mb-0">
                                    <span className="stepper-nearest-surveyor-distance">
                                        Price:{proposalInfo.price && `$${priceFormat(proposalInfo.price)}`}
                                    </span>
                                </div>
                                salesEngine.shipperAddedDocument && <div className="mr-3 mb-5">
                                    <span className="stepper-submitted-report-info">
                                        <a href={salesEngine.shipperAddedDocument} download>Download Proposal Document</a>
                                    </span>
                                </div>
                            </>} */}
                        </Col>

                    </Row>
                    <Row>
                        {showSurveyorButtons && <>
                            <button
                                type="button"
                                className={`w-auto btn mr-2 nearest-surveyor-btn ${isSurveyorRequested(surveyorInfo.id) ? 'grey-requested-btn' : 'btn-primary'}`}
                                onClick={() => requestSurveyorHandler(surveyorInfo.id)}
                                disabled={isSurveyorRequested(surveyorInfo.id)}>
                                {isSurveyorRequested(surveyorInfo.id) ? 'Requested' : 'Request'}
                            </button>
                            {isSurveyorRequested(surveyorInfo.id) && <button
                                type="button"
                                className={`w-auto btn grey-requested-btn nearest-surveyor-btn nearest-surveyor-decline-btn`}
                                onClick={() => declineSurveyor({ id: salesEngine.id, surveyorId: surveyorInfo.id })}
                            >
                                {'Cancel'}
                            </button>}
                        </>}
                        {showShipperButtons && <>
                            <div className="mt-3 mb-0">
                                <button
                                    type="button"
                                    className={`w-auto btn mr-2 btn-primary`}
                                    onClick={() => shipperAcceptRequest(proposalInfo.shipper.id)}
                                >
                                    {'Accept'}
                                </button>
                            </div>
                        </>

                        }
                    </Row>
                </div>
            </Grid>
            {/* </Grid> */}
        </>
    );
};
