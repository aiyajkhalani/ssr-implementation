
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Grid } from '@material-ui/core';

import { UserInformationCard } from "../../components";

export const SurveyorInformation = props => {

    const { surveyor, branch } = props

    return (
        <>
            <div>
                <div className="mt-5">
                    <span className="mr-4 ml-0 stepper-certify-surveyor-info">
                        {'Surveyor Information'}
                    </span>
                    <span className="ml-2 stepper-certify-surveyor-date">
                        {'Accepted Date: 28th Jan 2020'}
                    </span>
                </div>
                <div className="mt-3 mb-0">
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <div className="p-4 stepper-certify-boat-info-grid h-100">
                                <div className="d-flex flex-column">
                                    <span className="stepper-certify-surveyor-date">
                                        {'Branch Information'}
                                    </span>
                                    <span>
                                        We are located around your boat and we already received your inquiry
                                </span>
                                </div>
                                <div>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Contact Name
                                        </span>
                                                <span>
                                                    {branch.contactName}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Position
                                        </span>
                                                <span>
                                                    {branch.position}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Address
                                        </span>
                                                <span>
                                                    {branch.address}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    State
                                        </span>
                                                <span>
                                                    {branch.state}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Zipcode
                                        </span>
                                                <span>
                                                    {branch.postalCode || "-"}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    City
                                        </span>
                                                <span>
                                                    {branch.city}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Country
                                        </span>
                                                <span>
                                                    {branch.country}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Mobile Number
                                        </span>
                                                <span>
                                                    {branch.mobileNumber}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Branch Location
                                        </span>
                                                <span>
                                                    {branch.address}
                                                </span>
                                            </div>
                                        </Col>
                                        {/* <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Email Address
                                        </span>
                                                <span>
                                                    {branch.emailAddress}
                                                </span>
                                            </div>
                                        </Col> */}
                                    </Row>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <div className="p-4 stepper-certify-boat-info-grid h-100">
                                <div>
                                    <span className="stepper-certify-surveyor-date">
                                        {'Point of Contact'}
                                    </span>
                                </div>
                                <div>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Country
                                        </span>
                                                <span>
                                                    {surveyor.country}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Mobile Number
                                        </span>
                                                <span>
                                                    {surveyor.mobileNumber}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    First Name
                                        </span>
                                                <span>
                                                    {surveyor.firstName}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Last Name
                                        </span>
                                                <span>
                                                    {surveyor.lastName}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Company Name
                                        </span>
                                                <span>
                                                    {surveyor.companyName}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Position
                                        </span>
                                                <span>
                                                    {branch.position}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        {/* <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Email Address
                                        </span>
                                                <span>
                                                    {surveyor.email}
                                                </span>
                                            </div>
                                        </Col> */}
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    2nd Contact Name
                                        </span>
                                                <span>
                                                    {surveyor.secondContactFullName || "-"}  
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Alternative Mobile No
                                        </span>
                                                <span>
                                                    0000000000000
                                        </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Fax No
                                        </span>
                                                <span>
                                                    00000000
                                        </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Company Tel No
                                        </span>
                                                <span>
                                                    0000000000000
                                        </span>
                                            </div>
                                        </Col>
                                        {/* <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    2nd Email
                                        </span>
                                                <span>
                                                    {surveyor.secondContactEmail}
                                                </span>
                                            </div>
                                        </Col> */}
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Company Website
                                        </span>
                                                <span>
                                                    {surveyor.companyWebsite}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Business Phone No
                                        </span>
                                                <span>
                                                    00000000
                                        </span>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>

                        </Grid>
                        {/* <Grid item xs={4}> */}

                            {surveyor &&
                                <UserInformationCard userInfo={surveyor} />
                            }
                            {/* <div className="p-4 stepper-certify-boat-info-grid h-100">
                                <div>
                                    <span className="stepper-certify-surveyor-date">
                                        {'Office location'}
                                    </span>
                                </div>
                                <div>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Address
                                        </span>
                                                <span>
                                                    {surveyor.officeAddress}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Street
                                        </span>
                                                <span>
                                                    {surveyor.officeRoute}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    City or Town
                                        </span>
                                                <span>
                                                    {surveyor.officeCity}

                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    State or Province
                                        </span>
                                                <span>
                                                    {surveyor.officeState}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    Zipcode
                                        </span>
                                                <span>
                                                    {surveyor.officePostalCode}
                                                </span>
                                            </div>
                                        </Col>
                                        <Col xs={6}>
                                            <div className="d-flex flex-column">
                                                <span className="stepper-div-text">
                                                    P.O. Box
                                        </span>
                                                <span>
                                                    {surveyor.postBox}
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>

                                </div>
    </div>*/}
                        {/* </Grid> */}
                    </Grid>
                </div>
            </div>

            <hr />
        </>
    );
}