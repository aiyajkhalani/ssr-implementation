
import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { IoMdArrowRoundForward } from 'react-icons/io';

export const SurveyorCostInformation = props => {    

    return (
        <>
            <Row className="pl-4">
              <h3 className="sales-engine-info-title">
                Surveyor Cost Information
              </h3>
            </Row>
            <Row className="pl-4">
              <Col xs={4} className="pl-0 pr-0">
                <div className="d-flex mt-3">
                  <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                  <span className="font-weight-500 sales-engine-field-title">
                    Surveyor Cost per Feet :
                  </span>
                  <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                    $30.00
                  </span>
                </div>
                <div className="d-flex mt-4">
                  <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                  <span className="font-weight-500 sales-engine-field-title">
                    Length of the Boat :
                  </span>
                  <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                    $660.00
                  </span>
                </div>
                <div className="d-flex mt-4">
                  <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                  <span className="font-weight-500 sales-engine-field-title">
                    Total Surveyor Cost :
                  </span>
                  <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                    $660.00
                  </span>
                </div>
                <div className="d-flex mt-4">
                  <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                  <span className="font-weight-500 sales-engine-field-title">
                    PayPal Fee For Buyer Inside Canada :
                  </span>
                  <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                    2.9 % + $0.03
                  </span>
                </div>

                <div className="d-flex mt-4">
                  <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                  <span className="font-weight-500 sales-engine-field-title">
                    PayPal Fee For Buyer Outside Canada :
                  </span>
                  <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                    3.9 % + $0.03
                  </span>
                </div>
              </Col>
              <Col xs={8} className="pl-0">
                <Table striped bordered size="md" className="m-auto width-80">
                  <thead>
                    <tr>
                      <th>Description</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Surveyor Cost per Feet</td>
                      <td>$30.00</td>
                    </tr>
                    <tr>
                      <td>Length of the Boat</td>
                      <td>22 ft</td>
                    </tr>
                    <tr>
                      <td>Total Surveyor Cost</td>
                      <td>$660.00</td>
                    </tr>
                    <tr>
                      <td>Tax</td>
                      <td>$0.00</td>
                    </tr>
                    <tr>
                      <td>Sub Total</td>
                      <td>$660.00</td>
                    </tr>
                    <tr>
                      <td>Payment Service Fees</td>
                      <td>$0.00</td>
                    </tr>
                    <tr>
                      <td>Total </td>
                      <td>$660.00</td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </Row>
        </>
    );
}