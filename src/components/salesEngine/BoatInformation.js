import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import { priceFormat } from "../../util/utilFunctions";
import { StepperBoatInfoStyle } from "../styleComponent/styleComponent";
import { dimension } from "../../util/enums/enums";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { Grid } from "@material-ui/core";

export const BoatInformation = props => {
    const { salesEngine } = props;
    const { boat } = salesEngine;

    const [style, setStyle] = useState({
        width: 380,
        height: 280
    })

    useEffect(() => {
        const width = getRatio(dimension, "homeRecommended", "body");
        const height = getHeightRatio(dimension, "homeRecommended", "body");

        setStyle({ width, height });
    }, [])

    return (
        <>
            <Grid item xs={8} className="stepper-info-boat">
                <div className="d-flex stepper-info-boat-section">
                    <div>

                        <div className="stepper-boat-info-img">
                            <img
                                src={boat.images.length > 0 && boat.images[0]}
                                className="h-100 width-100"
                                alt="Boat"
                            />
                        </div>
                    </div>
                    <div className="width-100 p-5 section-stepper-info-boat-div">
                        <div className="d-flex justify-content-between">
                            <div className="d-flex flex-column">
                                <span className="stepper-boat-info-id">Ad ID: {boat.adId}</span>

                                <span className="stepper-boat-info-name">{boat.boatName}</span>
                                {salesEngine.negotiatedBoatPrice && <span className="stepper-boat-info-final-price">
                                    Final Price: ${priceFormat(salesEngine.negotiatedBoatPrice)}({boat.tax && boat.tax}{"%"} tax included )
                                </span>}
                                <span className="stepper-boat-info-price">
                                    Price: ${priceFormat(boat.price)}({boat.tax && boat.tax}{ boat.tax &&"%"} {boat.isTaxEnabled && "tax included"} )
                                </span>
                            </div>
                            {/* <div>
                                <button
                                    type="button"
                                    name="next"
                                    className="btn btn-primary w-auto float-right stepper-next-btn"
                                >
                                    {'Next'}
                                </button>
                            </div> */}
                        </div>
                        <div>

                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Year Built</span>
                                        <span> {boat.yearBuilt}</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Parking</span>
                                        <span>{boat.boatParking && boat.boatParking.name}</span>
                                    </div>
                                </Col>
                            </Row>


                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Trailer</span>
                                        <span>{boat.trailer}</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Status</span>
                                        <span> {boat.boatStatus && boat.boatStatus.alias}</span>
                                    </div>
                                </Col>
                            </Row>


                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Type</span>
                                        <span> {boat.boatType && boat.boatType.name}</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Engine Manufacturer</span>
                                        <span>{boat.engineManufacturer}</span>
                                    </div>
                                </Col>
                            </Row>


                        </div>
                    </div>
                </div>
            </Grid>
        </>
    );
};
