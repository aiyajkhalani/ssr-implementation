import React, { useState, useEffect } from 'react';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from "yup";

import { requireMessage } from '../../helpers/string';
import { Card, CardContent, Grid, Box, Button, makeStyles, Backdrop, Modal, Fade } from '@material-ui/core';
import { Field } from '../ws/Field';
import { cancelHandler } from '../../helpers/routeHelper';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        margin: theme.spacing(1),
    },
    link: {
        margin: theme.spacing(3),
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export const ShipmentDocument = props => {
    const classes = useStyles();

    const { open, closeModal, value, submitDocument } = props

    const [initValue, setInitValue] = useState({
        billOfLading: "",
        certificateOfOrigin: "",
        insuranceCertificate: "",
        receivedForShipment: "",
        invoiceOfShipmentPayment: "",
        otherDocument: "",
    });

    useEffect(() => {
        value && setInitValue(value)
    }, [value])

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Formik
                            initialValues={initValue}
                            onSubmit={values => {
                                submitDocument(values)
                            }}
                            validationSchema={Yup.object().shape({
                                billOfLading: Yup.string().required(requireMessage("Document 1")),
                                certificateOfOrigin: Yup.string().required(requireMessage("Document 2")),
                                insuranceCertificate: Yup.string().required(requireMessage("Document 3")),
                                receivedForShipment: Yup.string().required(requireMessage("Document 4")),
                                invoiceOfShipmentPayment: Yup.string().required(requireMessage("Document 5")),
                            })}
                            render={({
                                values,
                                setFieldValue,
                                handleSubmit
                            }) => (
                                    <Form className="shipment-image-popup-width">
                                        <Grid container spacing={2}>

                                            <Grid xs={12} item className="pb-1">
                                                <Box
                                                    fontSize={20}
                                                    letterSpacing={1}
                                                    fontWeight={600}
                                                >
                                                    {'Shipment Document'}
                                                </Box>
                                            </Grid>

                                            <Grid item sm={6}>
                                              
                                                <Field
                                                    label="Bill Of Loading"
                                                    name="billOfLoading"
                                                    type="shipment-document"
                                                    value={values.billOfLoading}
                                                    className="form-control"
                                                    onChangeText={setFieldValue}
                                                    required
                                                />
                                               
                                                <ErrorMessage
                                                    component="div"
                                                    name="billOfLading"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Field
                                                    label="Upload Certificate Of Origin"
                                                    name="certificateOfOrigin"
                                                    type="shipment-document"
                                                    value={values.certificateOfOrigin}
                                                    className="form-control"
                                                    onChangeText={setFieldValue}
                                                    required
                                                />
                                                <ErrorMessage
                                                    component="div"
                                                    name="certificateOfOrigin"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Field
                                                    label="Upload Insurance Certificate"
                                                    name="insuranceCertificate"
                                                    type="shipment-document"
                                                    value={values.insuranceCertificate}
                                                    className="form-control"
                                                    onChangeText={setFieldValue}
                                                    required
                                                />
                                                <ErrorMessage
                                                    component="div"
                                                    name="insuranceCertificate"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Field
                                                    label="Upload Received For Shipment"
                                                    name="receivedForShipment"
                                                    type="shipment-document"
                                                    value={values.receivedForShipment}
                                                    className="form-control"
                                                    onChangeText={setFieldValue}
                                                    required
                                                />
                                                <ErrorMessage
                                                    component="div"
                                                    name="receivedForShipment"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <Grid item sm={6}>
                                                <Field
                                                    label="Upload Invoice Of Shipment Payment"
                                                    name="invoiceOfShipmentPayment"
                                                    type="shipment-document"
                                                    value={values.invoiceOfShipmentPayment}
                                                    className="form-control"
                                                    onChangeText={setFieldValue}
                                                    required
                                                />
                                                <ErrorMessage
                                                    component="div"
                                                    name="invoiceOfShipmentPayment"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <Grid item sm={6}>
                                               
                                                <Field
                                                label="Upload Other Document If Any"
                                                name="otherDocument"
                                                type="shipment-document"
                                                value={values.otherDocument}
                                                className="form-control"
                                                onChangeText={setFieldValue}
                                                required
                                            />
                                                <ErrorMessage
                                                    component="div"
                                                    name="otherDocument"
                                                    className="error-message"
                                                />
                                            </Grid>
                                            <div className="d-flex justify-content-center w-100">
                                            <Button
                                                className="profile-button btn-dark mt-3 mb-3 font-weight-400 font13"
                                                onClick={handleSubmit}
                                            >
                                                {'Submit'}
                                            </Button>
                                            <Button
                                                className="profile-button btn-dark mt-3 mb-3 font-weight-400 font13"
                                                onClick={closeModal}
                                            >
                                                {'Cancel'}
                                            </Button>
                                            
                                            </div>
                                        </Grid>
                                    </Form>
                                )}
                        />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}