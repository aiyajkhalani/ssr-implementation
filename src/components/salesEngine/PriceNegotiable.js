import React, { Component } from "react";
import { Row, Col, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { addNegotiablePrice } from "../../redux/actions";
import { Formik, ErrorMessage } from "formik";
import { Field } from "..";
import { Button } from "@material-ui/core";
import * as Yup from "yup";
import { requireMessage } from "../../helpers/string";
import { SuccessNotify, ErrorNotify } from "../../helpers/notification";

class PriceNegotiable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      negotiablePrice: {
        id: props.salesEngine.id,
        price: props.salesEngine.negotiatedBoatPrice || 0
      }
    };
  }

  static getDerivedStateFromProps(nextProps) {

    if (nextProps.createNegotiablePriceSuccess) {
      SuccessNotify('Sales Engine Negotiated Price added successfully.')
    } else if (nextProps.createNegotiablePriceError) {
      ErrorNotify('Some error occurred while adding price.')
    }

    return null
  }

  render() {
    const { addNegotiablePrice } = this.props;
    const { negotiablePrice } = this.state;    

    return (
      <>
        <Formik
          initialValues={negotiablePrice}
          validationSchema={Yup.object().shape({
            price: Yup.number().required(requireMessage("price")).min(1),
          })}
          onSubmit={values => {
            values.price !== negotiablePrice.price && addNegotiablePrice(values)
          }}
          render={({ values, setFieldValue, handleSubmit, errors }) => (
            <Form>
              <Row className="pl-2">
                <h3 className="sales-engine-info-title">Final Boat Price</h3>
              </Row>
              <Row>
                <Col xs={12}>
                  <div className="d-flex mt-3">
                    <span className="sales-engine-info-field d-flex justify-content-center align-items-center mr-2">
                      <Field
                        placeholder="Negotiable Price"
                        id={"price"}
                        name={"price"}
                        value={values.price}
                        required
                        type="number"
                        onChangeText={e => {
                          setFieldValue("price", +e.target.value);
                        }}
                      />
                    </span>
                    <ErrorMessage
                      component="div"
                      name="price"
                      className="error-message"
                    ></ErrorMessage>
                    <Button
                      onClick={handleSubmit}
                      type="button"
                      className="button btn btn-primary w-auto addBoatService-btn primary-button"
                    >
                      Save
                    </Button>
                  </div>
                </Col>
              </Row>
            </Form>
          )}
        ></Formik>
      </>
    );
  }
}

const mapStateToProps = state => ({
  createNegotiablePriceSuccess: state.salesEngineReducer.createNegotiablePriceSuccess,
  createNegotiablePriceError: state.salesEngineReducer.createNegotiablePriceError,
});

const mapDispatchToProps = dispatch => ({
  addNegotiablePrice: data => dispatch(addNegotiablePrice(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PriceNegotiable);
