import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, Container, ListGroup, Table } from "react-bootstrap";
import { IoMdArrowRoundForward } from "react-icons/io";
import { paymentOptions } from "../../util/enums/enums";
import { Field } from "../ws/Field";
import { Formik, ErrorMessage, Form } from "formik";
import * as Yup from "yup";
// import { requireMessage } from "../../helpers/string";


class PaymentOption extends Component {
    state = {
        paymentMethod: paymentOptions[0].name,
        schema: {
            cardHolderName: "",
            payWith: "",
            expirationMonth: "",
            expirationYear: "",
            cardNumber: "",
            securityCode: "",
            type: "",
        },
        validationSchema: {}
    };

    creditSchemaValidation = Yup.object().shape({
        cardHolderName: Yup.string().required("Card Holder's Name"),
        expirationMonth: Yup.string().required("Expiration Month"),
        expirationYear: Yup.string().required("Expiration Year"),
        cardNumber: Yup.string().required("Card Number"),
        securityCode: Yup.string().required("Security Code"),
    })

    selectPaymentMethodHandler = name => {
        const validationSchema = name === "Credit Card" ? this.creditSchemaValidation : {}
        this.setState({ paymentMethod: name, validationSchema });
    };

    renderPaymentInfo = (name, setFieldValue) => {

        switch (name) {
            case paymentOptions[0].name:
                return (
                    <>
                        <Row>
                            <Col xs={2} className="pr-0">
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Bank Name :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        CIBC
                                    </span>
                                </div>
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Transit number :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        06122
                                    </span>
                                </div>
                            </Col>
                            <Col xs={2} className="pr-0">
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Swift code :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        CIBCCATT
                                    </span>
                                </div>
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Institution number :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        010
                                    </span>
                                </div>
                            </Col>
                            <Col xs={3} className="pr-0 pl-5">
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Account number :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        6300111
                                    </span>
                                </div>
                                <div className="d-flex mt-3">
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Company name :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        ADAMSEA Inc.
                                    </span>
                                </div>
                            </Col>
                            <Col xs={5} className="pr-0 pl-0">
                                <div className="d-flex mt-3">
                                    <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                                    <span className="font-weight-500 sales-engine-field-title">
                                        Branch address :
                                    </span>
                                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                                        6266 Dixie Road, Mississauga ON L5T 1A7 Canada
                                    </span>
                                </div>
                            </Col>
                        </Row>

                    </>
                )
            case paymentOptions[1].name:
                return (
                    <>
                        <Row>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Card Holder's Name :
                                            </label>
                                    <Field type="text"
                                        onChangeText={e => {
                                            setFieldValue("cardHolderName", e.target.value);
                                        }}
                                        className="light-gray-bg" />
                                    <ErrorMessage
                                        component="div"
                                        name="cardHolderName"
                                        className="error-message"
                                    />
                                </div>
                            </Col>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Pay With
                                            </label>
                                    <Field type="text" onChangeText={e => {
                                        setFieldValue("payWith", e.target.value);
                                    }} className="light-gray-bg" />
                                </div>
                            </Col>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Card No :
                                            </label>
                                    <Field type="number" onChangeText={e => {
                                        setFieldValue("cardNumber", parseInt(e.target.value));
                                    }} className="light-gray-bg" />
                                    <ErrorMessage
                                        component="div"
                                        name="cardNumber"
                                        className="error-message"
                                    />
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Expiration Year
                                            </label>
                                    <Field type="number" onChangeText={e => {
                                        setFieldValue("expirationYear", parseInt(e.target.value));
                                    }} className="light-gray-bg" />
                                    <ErrorMessage
                                        component="div"
                                        name="expirationYear"
                                        className="error-message"
                                    />
                                </div>
                            </Col>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Expiration Month
                                            </label>
                                    <Field type="number" onChangeText={e => {
                                        setFieldValue("expirationMonth", parseInt(e.target.value));
                                    }} className="light-gray-bg" />
                                    <ErrorMessage
                                        component="div"
                                        name="expirationMonth"
                                        className="error-message"
                                    />
                                </div>
                            </Col>
                            <Col xs={4}>
                                <div className="d-flex justify-content-center flex-column mt-2 mb-2">
                                    <label className="mb-1 mr-2 font-weight-500 sales-engine-field-title">
                                        Security Code
                                            </label>
                                    <Field type="number" onChangeText={e => {
                                        setFieldValue("securityCode", parseInt(e.target.value));
                                    }} className="light-gray-bg" />
                                    <ErrorMessage
                                        component="div"
                                        name="securityCode"
                                        className="error-message"
                                    />
                                </div>
                            </Col>
                        </Row>
                    </>
                );
            default:
                return
        }

    };
    render() {
        const { paymentMethod, schema, validationSchema } = this.state;
        const { paymentRequest, paymentTo, salesEngine, disable } = this.props;

        return (
            <>
                <Formik
                    initialValues={schema}
                    validationSchema={validationSchema}
                    enableReinitialize
                    onSubmit={values => {
                        values.type = paymentTo
                        values.salesEngineId = salesEngine.id
                        paymentRequest(values)
                    }}
                    render={({ values, setFieldValue, handleSubmit, errors }) => (
                        <Form>
                            <Row className="d-flex pl-3 pr-3 mt-3">
                                <Col xs={12} className="d-flex flex-column">
                                    <Row className="pl-2">
                                        <h3 className="sales-engine-info-title">Payment Option</h3>
                                    </Row>
                                    <div>
                                        <div className="mt-4 d-flex flex-column payment-option-div">
                                            <div className="d-flex flex-column">
                                                <div>
                                                    {paymentOptions.map((option, index) => (
                                                        <span
                                                            className={`cursor-pointer p-3 font-weight-500 ${
                                                                option.name === paymentMethod
                                                                    ? "sales-engine-selected-payment-div"
                                                                    : "sales-engine-payment-div"
                                                                }`}
                                                            key={option.name}
                                                            onClick={() =>
                                                                this.selectPaymentMethodHandler(option.name)
                                                            }
                                                        >
                                                            {option.name}
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </Col>
                                <Col xs={12}>
                                    <div className="pl-4 pr-4 d-flex flex-column payment-option-bg">
                                        <div className="mt-3">
                                            {this.renderPaymentInfo(paymentMethod, setFieldValue)}

                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            {paymentMethod && paymentMethod === paymentOptions[1].name && 
                                <Row>
                                    <Col xs={12}>
                                        <div className="d-flex justify-content-center mt-3 mb-2">
                                            <button
                                                type="button"
                                                className="btn btn-primary w-auto float-right"
                                                onClick={handleSubmit}
                                                disabled={!disable}
                                            >
                                                Pay
                                    </button>
                                        </div>
                                    </Col>
                                </Row>
                            }
                        </Form>
                    )}
                />
            </>
        )
    }
}

export default PaymentOption
PaymentOption.defaultProps = {
    paymentTo: "",
}