

import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { IoMdArrowRoundForward } from 'react-icons/io';
import { Grid } from '@material-ui/core';

export const SurveyorSubmittedReport = props => {

    const { surveyorReport } = props

    return (
        <>

            <div>
                <div className="mt-4">
                    <span className="mr-3 ml-0 stepper-certify-surveyor-info">
                        Report submitted by {surveyorReport && surveyorReport.surveyorId && surveyorReport.surveyorId.firstName} {" "}
                        {surveyorReport && surveyorReport.surveyorId && surveyorReport.surveyorId.lastName}
                    </span>
                    <span className="stepper-certify-surveyor-date">
                        {'on 28th Jan, 2020'}
                    </span>
                </div>
                <div className="mt-2">
                    <div className="stepper-certify-boat-info-grid p-4">
                        <div className="d-flex flex-column">
                            <span className="stepper-div-text">
                                Surveyor Comment
                        </span>
                            <span>
                                {surveyorReport.comment}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="mt-4 d-flex">
                    <div className="mr-3" href={surveyorReport.video}>
                        <span className="stepper-submitted-report-info">
                            <a href={surveyorReport.video} download target="_blank">Report a video</a>
                        </span>
                    </div>
                    <div className="mr-3">
                        <span className="stepper-submitted-report-info">
                            <a href={surveyorReport.boatVerifications} download>Boat Verification Document</a>
                        </span>
                    </div>
                    <div className="mr-3">
                        <span className="stepper-submitted-report-info">
                            <a href={surveyorReport.report} download>Survey Report</a>
                        </span>
                    </div>
                    <div className="mr-3">
                        <span className="stepper-submitted-report-info">
                            <a href={surveyorReport.otherDocument} download> Report Document 2</a>
                        </span>
                    </div>
                </div>
            </div>
            {/* <Row className="d-flex flex-column pl-3 pr-3 mt-4">
                <Row className="pl-2 d-flex align-items-center">
                    <div className="d-flex">
                        <h3 className="sales-engine-info-title mb-0">
                            Report submitted by {surveyorReport && surveyorReport.surveyorId && surveyorReport.surveyorId.firstName} {" "}
                            {surveyorReport && surveyorReport.surveyorId && surveyorReport.surveyorId.lastName}
                        </h3>
                        <h3 className="sales-engine-info-title font-20 navy-blue mb-0 ml-2">
                            on 08, March, 2017
                  </h3>
                    </div>
                </Row>
                <Row className="d-flex align-items-center mt-2">
                    <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                        You have confirmed the provided Report.
                </span>
                </Row>
                <Row className="d-flex flex-column">
                    <div className="d-flex mt-4">
                        <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                        <span className="font-weight-500 sales-engine-field-title">
                            {'Surveyor Comment :'}
                        </span>
                        <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                            {surveyorReport && surveyorReport.comment}
                        </span>
                    </div>
                    <div className="d-flex mt-4">
                        <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                        <span className="font-weight-500 sales-engine-field-title">
                            {'Download :'}
                        </span>
                        <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                            Report Video
                  </span>
                    </div>
                    <div className="d-flex mt-4">
                        <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                        <span className="font-weight-500 sales-engine-field-title">
                            {'Download Boat Verification Document :'}
                        </span>
                        <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                            2016.pdf
                  </span>
                    </div>
                    <div className="d-flex mt-4">
                        <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                        <span className="font-weight-500 sales-engine-field-title">
                            {'Download Survey Report :'}
                        </span>
                        <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                            RECEIPT_N_INVOICE62CM1Y9S_BUILDER22VCC5I8.pdf
                  </span>
                    </div>
                    <div className="d-flex mt-4">
                        <IoMdArrowRoundForward className="sales-engine-arrow-icon" />
                        <span className="font-weight-500 sales-engine-field-title">
                            {'Download Report Document 2 :'}
                        </span>
                        <span className="pl-2 sales-engine-info-field d-flex justify-content-center align-items-center">
                            11.pdf
                  </span>
                    </div>
                </Row>
            </Row> */}
        </>
    );
}













