import React, { useState, useEffect } from 'react';
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from "yup";

import { requireMessage } from '../../helpers/string';
import { Grid, Box, Button, makeStyles, Backdrop, Modal, Fade } from '@material-ui/core';
import { Field } from '../ws/Field';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        margin: theme.spacing(1),
    },
    link: {
        margin: theme.spacing(3),
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export const ShipmentProposal = props => {
    const classes = useStyles();

    const { open, closeModal, value, onSubmit, salesEngines, selectedSalesEngineId } = props

   const salesEngine = salesEngines && salesEngines.find((item) => (item.id === selectedSalesEngineId))
    // const shipperProposal = salesEngine.shipperRequested.find
   const [initValue, setInitValue] = useState({
        price: "",
        proposalDocument: "",
    });

    useEffect(() => {
        value && setInitValue(value)
    }, [value])

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={closeModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={`${classes.paper} p-0 shipment-proposal`}>
                        <Formik
                            initialValues={initValue}
                            onSubmit={values => onSubmit(values)}
                            validationSchema={Yup.object().shape({
                                price: Yup.string().required(requireMessage("Price")),
                            })}
                            render={({
                                values,
                                setFieldValue,
                                handleSubmit
                            }) => (
                                    <Form className="overflow-hidden">
                                        <Grid container spacing={2}>

                                            <Grid xs={12} item>
                                                <Box
                                                    fontSize={20}
                                                    letterSpacing={1}
                                                    fontWeight={600}
                                                    className="shipment-proposal-desc-title sales-engin-header text-white"
                                                >
                                                    {'Shipment Proposal'}
                                                </Box>
                                            </Grid>
                                            <div className="d-flex flex-column w-100">
                                                <div className="shipment-proposal-desc-div">
                                                    <Grid item sm={12}>
                                                        <Field
                                                            label="Mention Your Offer Price (USD)"
                                                            name="price"
                                                            type="number"
                                                            value={values.price}
                                                            className="form-control"
                                                            onChangeText={e => setFieldValue("price", +e.target.value)}
                                                            required
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="price"
                                                            className="error-message"
                                                        />
                                                    </Grid>

                                                    <Grid item sm={12} className="mt-2">
                                                        <Field
                                                            buttonText="Upload Proposal Document"
                                                            name="proposalDocument"
                                                            type="single-document"
                                                            value={values.proposalDocument}
                                                            className="form-control"
                                                            onChangeText={setFieldValue}
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="proposalDocument"
                                                            className="error-message"
                                                        />
                                                    </Grid>

                                                </div>
                                                <div className="shipment-proposal-btn-div d-flex justify-content-end">
                                                    <Button
                                                        className="profile-button mt-3 mb-3 font-weight-400 font13 shipment-proposal-cancel-btn mt-0 mb-0"
                                                        onClick={closeModal}
                                                    >
                                                        {'Cancel'}
                                                    </Button>
                                                    <Button
                                                        className="profile-button mt-3 mb-3 font-weight-400 font13 shipment-proposal-submit-btn mt-0 mb-0"
                                                        onClick={handleSubmit}
                                                    >
                                                        {'Submit'}
                                                    </Button>
                                                </div>
                                            </div>
                                        </Grid>
                                    </Form>
                                )}
                        />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}