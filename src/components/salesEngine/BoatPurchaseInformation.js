
import React from 'react';
import { Grid } from '@material-ui/core';

import { priceFormat } from '../../util/utilFunctions';

export const BoatPurchaseInformation = props => {

    const { salesEngine, costEstimation } = props
    const { boat } = salesEngine

    const getAdminFees = () => {
        return salesEngine && salesEngine.negotiatedBoatPrice && costEstimation && costEstimation.adminFee
            ? (salesEngine.negotiatedBoatPrice * costEstimation.adminFee) / 100 : 0
    }

    return (
        <>
            {/* <div className="mt-5">
                <h3 className="sales-engine-info-title">
                    {'AdamSea has successfully approved your offline payment.'}
                </h3>
            </div> */}

            <div className="mt-3 mb-0">
                <Grid container spacing={3}>
                    {boat &&
                        <Grid item xs={4}>
                            <div className="stepper-certify-boat-info-grid p-4">
                                <div>
                                    <div className="d-flex flex-column mt-3">
                                        <span>
                                            {'Total Boat Cost'}
                                        </span>
                                        <span className="navy-blue">
                                            ${priceFormat(salesEngine.negotiatedBoatPrice)}
                                        </span>
                                    </div>
                                    {/* <div className="d-flex flex-column mt-3">
                                        <span>
                                            {'Boat Original Price'}
                                        </span>
                                        <span className="navy-blue">
                                            ${priceFormat(boat.price)}
                                        </span>
                                    </div> */}
                                    <div className="d-flex flex-column mt-3">
                                        <span>
                                            {'Quantity Purchased'}
                                        </span>
                                        <span className="navy-blue">
                                            {'01'}
                                        </span>
                                    </div>
                                    <div className="d-flex flex-column mt-3">
                                        <span>
                                            {'Total Amount'}
                                        </span>
                                        <span className="navy-blue">
                                            ${priceFormat(salesEngine.negotiatedBoatPrice + getAdminFees())}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </Grid>
                    }
                    <Grid item xs={8}>
                        <div className="stepper-certify-boat-info-grid p-4 d-flex h-100">

                            <Grid item xs={4}>
                                <div>
                                    <span className="navy-blue font-16 font-weight-500">
                                        {'Quantity'}
                                    </span>
                                </div>
                                <div className="mt-3 mb-0 d-flex flex-column">
                                    <span>
                                        {'01'}
                                    </span>
                                    <span>
                                        {'Administration Fees'}
                                    </span>
                                    <span>
                                        {'Payment Service Fees'}
                                    </span>
                                    <span>
                                        {'Sub Total'}
                                    </span>
                                    <span>
                                        {'Total'}
                                    </span>
                                </div>
                            </Grid>
                            <Grid item xs={3}>
                                <div>
                                    <span className="navy-blue font-16 font-weight-500">
                                        {'Description'}
                                    </span>
                                </div>
                                <div className="mt-3 mb-0">
                                    <span>
                                        {'Item Cost'}
                                    </span>
                                </div>
                            </Grid>
                            <Grid item xs={3}>
                                <div>
                                    <span className="navy-blue font-16 font-weight-500">
                                        {'Unit Price'}
                                    </span>
                                </div>
                                <div className="mt-3 mb-0">
                                    <span>
                                        ${priceFormat(salesEngine.negotiatedBoatPrice)}
                                    </span>
                                </div>
                            </Grid>
                            <Grid item xs={2}>
                                <div className="d-flex justify-content-end">
                                    <div className="d-flex flex-column">
                                        <div className="d-flex justify-content-end">
                                            <span className="navy-blue font-16 font-weight-500">
                                                {'Total'}
                                            </span>
                                        </div>
                                        <div className="mt-3 mb-0 d-flex flex-column">
                                            <span className="d-flex justify-content-end">
                                                ${priceFormat(salesEngine.negotiatedBoatPrice)}
                                            </span>
                                            <span className="d-flex justify-content-end">
                                                ${priceFormat(getAdminFees())}
                                            </span>
                                            <span className="d-flex justify-content-end">
                                                {'$0.00'}
                                            </span>
                                            <span className="d-flex justify-content-end">
                                                ${priceFormat(salesEngine.negotiatedBoatPrice + getAdminFees())}
                                            </span>
                                            <span className="navy-blue d-flex justify-content-end font-weight-500">
                                                ${priceFormat(salesEngine.negotiatedBoatPrice + getAdminFees())}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        </div>
                    </Grid>
                </Grid>
            </div>

        </>
    );
}


























