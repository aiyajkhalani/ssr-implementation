import React from "react";
import { Row, Col } from "react-bootstrap";
import SharePopup from "../share/SharePopup";
import { Grid } from "@material-ui/core";

export const SellerInformation = props => {
  const [sharePopUpOpen, setSharePopUpOpen] = React.useState(false);

  const {
    isSeller,
    salesEngine
  } = props;

  let userInfo;

  if (salesEngine) {
    userInfo = isSeller ? salesEngine.buyer : salesEngine.seller;
  }

  return (
    <>
      <Grid item xs={4} className="stepper-info-boat">
        <div>
          <Row className="p-4 seller-stepper-div-padding">
            <Col xs={7} className="pr-0 pl-0">
              <div>
                <div className="mb-4">
                  <div className="mb-2 d-flex align-items-center">
                    <img
                      src={require("../userProfile/image/boat-shipper.png")}
                      className="stepper-user-profile-icon-div mr-2"
                      alt=""
                    />
                    <span className="rentInner-userTextH4 text-transform-capitalize font-16 dark-silver">
                      {userInfo && userInfo.role && userInfo.role.role}
                    </span>
                  </div>
                  <div className="mb-2 d-flex align-items-center">
                    <img
                      src={require("../userProfile/image/boat.png")}
                      className="stepper-user-profile-icon-div mr-2"
                      alt=""
                    />
                    <span className="rentInner-userTextH4  font-16 dark-silver">
                      View {userInfo && userInfo.relatedItems && userInfo.relatedItems.length} Boat
                  </span>
                  </div>
                  <div className="mb-2 d-flex align-items-center">
                    <img
                      src={require("../userProfile/image/verified.png")}
                      className="stepper-user-profile-icon-div mr-2"
                      alt=""
                    />
                    <span className="rentInner-userTextH4  font-16 dark-silver">
                      Verified
                  </span>
                  </div>
                  <div className="mb-2 d-flex align-items-center">
                    <img
                      src={require("../userProfile/image/reviews.png")}
                      className="stepper-user-profile-icon-div mr-2"
                      alt=""
                    />
                    <span className="rentInner-userTextH4  font-16 dark-silver">
                      {salesEngine && salesEngine.seller &&
                        userInfo &&
                        userInfo.reviews &&
                        userInfo.reviews.reviews &&
                        userInfo.reviews.reviews.length} Reviews
                  </span>
                  </div>
                </div>
                <hr />
                <div className="mt-4">
                  <h6 className="mb-3  font-16">{userInfo && userInfo.firstName} has provided</h6>
                  {userInfo && userInfo.documentVerification &&
                    (
                      <div className="mb-2 d-flex align-items-center">
                        <img
                          src={require("../userProfile/image/vector.png")}
                          className="stepper-user-profile-icon-div mr-2"
                          alt=""
                        />
                        <span className="rentInner-userTextH4  font-16 dark-silver">
                          Government ID
                  </span>
                      </div>
                    )}
                  {userInfo && userInfo.documentVerification &&
                    (
                      <div className="mb-2 d-flex align-items-center">
                        <img
                          src={require("../userProfile/image/vector.png")}
                          className="stepper-user-profile-icon-div mr-2"
                          alt=""
                        />
                        <span className="rentInner-userTextH4  font-16 dark-silver">
                          Email address
                  </span>
                      </div>
                    )}
                  {userInfo && userInfo.documentVerification &&
                    (
                      <div className="mb-2 d-flex align-items-center">
                        <img
                          src={require("../userProfile/image/vector.png")}
                          className="stepper-user-profile-icon-div mr-2"
                          alt=""
                        />
                        <span className="rentInner-userTextH4  font-16 dark-silver">
                          Phone number
                  </span>
                      </div>
                    )}
                </div>
                {/* <hr /> */}
                {/* <div className="mt-3">
                  <h6 className="mb-2  font-16">{userInfo && userInfo.firstName} information</h6>
                  {userInfo && userInfo.firstName && userInfo.lastName &&
                    (
                      <div className="mb-1 d-flex align-items-center">
                        <span className="rentInner-userTextH4  font-16 dark-silver">
                          seller :
                  </span> <span >
                          {userInfo && userInfo.firstName} {userInfo && userInfo.lastName}
                        </span>
                      </div>
                    )}
                  {userInfo && userInfo.email &&
                    (
                      <div className="mb-1 d-flex align-items-center">
                        <span className="rentInner-userTextH4  font-16 dark-silver">
                          userEmail :
                  </span> <span >
                          {userInfo.email}
                        </span>
                      </div>
                    )}
                </div> */}
              </div>
            </Col>
            <Col xs={5}>
              <div className="d-flex justify-content-center align-items-center">
                <div className="mb-3">
                  <div className="stepper-userImg rounded-circle user-profile-online-section">
                    <img
                      className="rounded-circle width-100"
                      src={
                        userInfo && userInfo.image
                      }
                      alt="placeholder"
                    />
                    <div className="stepper-user-online">
                      <div className="online-div-user"></div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-evenly mt-3">
                    <img
                      src={require("../userProfile/image/group-message.png")}
                      className="user-profile-social-icon-div cursor-pointer"
                      alt=""
                    />
                    <img
                      src={require("../userProfile/image/profile.png")}
                      className="user-profile-social-icon-div cursor-pointer"
                      alt=""
                    />
                    <img
                      src={require("../userProfile/image/share.png")}
                      className="user-profile-social-icon-div cursor-pointer"
                      alt=""
                      onClick={() => setSharePopUpOpen(true)}
                    />
                    {sharePopUpOpen && (
                      <SharePopup
                        handleClick={() => setSharePopUpOpen(false)}
                        useOwnIcon={true}
                      />
                    )}
                  </div>
                  {userInfo && userInfo.companyLogo && (
                    <div className="logo-box seller-logo-div">
                      <img
                        src={userInfo.companyLogo}
                        height={50}
                        width={50}
                        alt=""
                      />
                    </div>
                  )}
                  {userInfo && userInfo.firstName && userInfo.lastName &&
                    (
                      <div className="mb-1 d-flex align-items-center justify-content-center">
                        <span >
                          Name: {userInfo && userInfo.firstName} {userInfo && userInfo.lastName}
                        </span>
                      </div>
                    )}
                  <div className="company-name dark-silver  font-16 text-center">
                    Company: {salesEngine && salesEngine.seller && userInfo && userInfo.companyName}
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Grid>
    </>
  );
};
