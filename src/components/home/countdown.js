//[WIP] :Not Review @ghanshyam

import React from "react";
import { getHourFromDifferent } from "../../helpers/jsxHelper";

export class Countdown extends React.Component {
  state = {
    different: {},
    days: 0,
    hours: 0,
    min: 0,
    sec: 0,

  };
  componentDidMount() {
    const { endTime, startTime } = this.props;
    if (endTime && startTime) {
    this.interval = setInterval(() => {
        const date = getHourFromDifferent(endTime, startTime);
        date ? this.setState(date) : this.stop();
      }, 1000);
      const different = getHourFromDifferent(endTime, startTime);
      this.setState({ different });
    }
  }
  componentWillUnmount() {
    this.stop();
  }
  stop() {
    clearInterval(this.interval);
  }
  addLeadingZeros(value) {
    value = String(value);
    while (value.length < 2) {
      value = '0' + value;
    }
    return value;
  }
  render() {
    const {
      different: { days, hours, min, sec }
    } = this.state;
    return (
      <>
        <div className="auction-time-box">
          <span className="auction-value">{this.addLeadingZeros(days)}</span>
          <span className="auction-text">DAY</span>
        </div>
        <div className="auction-time-box">
          <span className="auction-value">{this.addLeadingZeros(hours)}</span>
          <span className="auction-text">HRS</span>
        </div>
        <div className="auction-time-box">
          <span className="auction-value">{this.addLeadingZeros(min)}</span>
          <span className="auction-text">Min</span>
        </div>
        <div className="auction-time-box">
          <span className="auction-value">{this.addLeadingZeros(sec)}</span>
          <span className="auction-text">Sec</span>
        </div>
      </>
    );
  }
}
