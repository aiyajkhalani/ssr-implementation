import React from 'react';
import { autoPlay } from "react-swipeable-views-utils";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import uuid from "uuid/v4";


const BannerCarousel = () => {

  const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

  const theme = useTheme();

  const useStyles = makeStyles(() => ({
    img: {
      display: "block",
      maxWidth: "100vw",
      width: "100%"
    },
  })
  )

  const classes = useStyles();

  const [activeStep, setActiveStep] = React.useState(0);

  const tutorialSteps = [
    {
      label: "Boat1",
      imgPath: "image/slide-1.jpg"
    },
    {
      label: "Boat2",
      imgPath: "image/slide-2.jpg"
    },
    {
      label: "Boat3",
      imgPath: "image/slide-3.jpg"
    },
    {
      label: "Boat4",
      imgPath: "image/slide-4.jpg"
    },
    {
      label: "Boat5",
      imgPath: "image/slide-5.jpg"
    },
    {
      label: "Boat6",
      imgPath: "image/slide-6.jpg"
    }
  ];

  function handleStepChange(step) {
    setActiveStep(step);
  }

  return (<AutoPlaySwipeableViews
    axis={theme.direction === "rtl" ? "x-reverse" : "x"}
    index={activeStep}
    onChangeIndex={handleStepChange}
    className="home-slider"
    enableMouseEvents
  >
    {tutorialSteps.map((step, index) => (
      <div key={uuid()}>
        {Math.abs(activeStep - index) <= 2 ? (
          <img
            className={classes.img}
            src={step.imgPath}
            alt={step.label}
          />
        ) : null}
      </div>
    ))}

  </AutoPlaySwipeableViews>)
}
export default BannerCarousel;