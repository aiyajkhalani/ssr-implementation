import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { HomeRecommendedContentStyle } from "../../styleComponent/styleComponent";
import { priceFormat, commonBoatType } from "../../../util/utilFunctions";
import SharePopup from "../../share/SharePopup";
import { cityCountryNameFormatter } from "../../../helpers/jsxHelper";
import { viewBoatHandler } from "../../../helpers/boatHelper";

export const BoatDetail = React.memo(({ value, isCarousel, isCustomWidth, isBottomRating, isTopLabel, index, width, height, history, xs, sm }) => {

    const [selectedIndex, setSelectedIndex] = useState(null);

    const handleClick = index => {
        setSelectedIndex(selectedIndex !== index ? index : null);
    };

    const {
        lengthInFt,
        boatType,
        yearBuilt,
        price,
        country,
        city,
        images,
        rating,
        boatStatus: { alias }
    } = value;


    return (
        <Grid item xs={xs} sm={sm} className={isCustomWidth && !isCarousel && 'MuiGrid-grid-sm-2-5'}>
            <div className="position-relative cursor-pointer">
                <HomeRecommendedContentStyle
                    bgWidth={width}
                    bgHeight={height}
                    onClick={() => viewBoatHandler(value, history)}
                    img={
                        images && images.length && encodeURI(images[0])
                    }
                />

                <div
                    className={`${
                        isTopLabel
                            ? "card-action justify-content-between"
                            : "card-action"
                        }`}
                >
                    {isTopLabel && <div>{commonBoatType(alias)}</div>}
                    <div class="share-icon">
                        <div class="heart-button">
                            <i class="adam-heart-1"></i>
                        </div>
                        <SharePopup
                            handleClick={() => handleClick(index)}
                            index={index}
                            selectedIndex={selectedIndex}
                        />
                    </div>
                </div>

                <div className="boat-info-grid mt-3 d-flex justify-content-between">
                    <div className={!isBottomRating && "price-with-rating"}>
                        {!isBottomRating &&
                            <div className="d-flex align-center">
                                <div className="mr-1">
                                    <Rating
                                        className="rating-clr"
                                        initialRating={rating}
                                        fullSymbol={<StarIcon />}
                                        emptySymbol={<StarBorderIcon />}
                                        placeholderSymbol={<StarBorderIcon />}
                                        readonly
                                    />
                                </div>
                                <span className="rating-count">({rating})</span>
                            </div>
                        }
                        <div className="new-boat-label">
                            {!isTopLabel && (
                                <div className="d-flex align-items-center used-new-build-font-line-height">
                                    {commonBoatType(alias)}
                                </div>
                            )}
                            <h6 className="mb-0 home-boat-price">
                                INR {priceFormat(price)}
                            </h6>
                        </div>
                    </div>
                    <h5 className="place-city boat-name">
                        {yearBuilt} {boatType && boatType.name} /{" "}
                        {lengthInFt} ft
                            </h5>
                    <h5 className="place-city">
                        <span>
                            {cityCountryNameFormatter(city, country)}
                        </span>
                    </h5>
                    <div>
                        <h6 className="manufactured-by">
                            <span>by</span>{" "}
                            <span className="text-capitalize">
                                {value.manufacturedBy}
                            </span>
                        </h6>
                    </div>
                    {isBottomRating && <div className="d-flex align-center">
                        <div className="mr-1">
                            <Rating
                                className="rating-clr"
                                initialRating={rating}
                                fullSymbol={<StarIcon />}
                                emptySymbol={<StarBorderIcon />}
                                placeholderSymbol={<StarBorderIcon />}
                                readonly
                            />
                        </div>
                        <span className="rating-count">({rating})</span>
                    </div>}
                </div>
            </div>
        </Grid>
    )
})

BoatDetail.defaultProps = {
    isTopLabel: false,
    isBottomRating: false,
    isCarousel: false,
    isCustomWidth: false,
};
