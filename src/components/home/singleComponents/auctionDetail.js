import React from "react";
import { viewBoatHandler } from "../../../helpers/boatHelper";
import { AuctionRoomStyle } from "../../styleComponent/styleComponent";
import { Countdown } from "../countdown";
import { priceFormat } from "../../../util/utilFunctions";
import { Menu, Grid, MenuItem } from "@material-ui/core";
import { auctionCarouselOptions } from "../../../util/enums/enums";

export const AuctionDetail = React.memo(({ value, history, height, width }) => {

    const {
        boat,
        startTime,
        endTime,
        startPrice,
        minimumRaisedAmount,
        isOpen,
    } = value

    return (
        <div className="boat-box" onClick={() => viewBoatHandler(boat, history)}>
            <div className="boat-box-shadow cursor-pointer">

                <div className="boat-auction-img-box">
                    <AuctionRoomStyle
                        img={
                            boat &&
                            boat.images && boat.images.length &&
                            encodeURI(boat.images[0])
                        }
                        bgHeight={height}
                        bgWidth={width}
                    >
                        <Countdown startTime={startTime} endTime={endTime} />
                        <div className="auction-boat-price">INR {boat && priceFormat(boat.price)}</div>
                    </AuctionRoomStyle>
                </div>

                <div className="boat-info">
                    <div className="boat-info-inner border-bottom">
                        <div className="d-flex flex-row justify-content-between">
                            <div>
                                <h4 className="boat-title-auction mt-0 mb-0">
                                    {boat && boat.yearBuilt} {boat && boat.boatType && boat.boatType.name} / {boat && boat.lengthInFt} {'ft'}
                                </h4>
                            </div>
                            <div className="justify-content-end d-flex dFlex-1">
                                <h5 className="mt-0 boat-country-auction">
                                    {boat && boat.city}, {boat && boat.country}
                                </h5>
                            </div>
                        </div>

                        <Grid container>
                            <Grid item sm={12}>
                                <h4 className="margin-0 boat-owner-auction">
                                    {boat && boat.seller && boat.seller.firstName}
                                    {boat && boat.seller && boat.seller.lastName}
                                </h4>
                            </Grid>
                        </Grid>
                    </div>

                    <Grid container className="bid-pricing">
                        <Grid item sm={6} className="border-right starting-pricing">
                            <h5 className="mt-0 mb-0 bid-price-auction">Starting Price</h5>
                            <h5 className="mt-0 mb-0 boat-price auction-price-font">INR {priceFormat(startPrice)}</h5>
                        </Grid>

                        <Grid item sm={6} className="pl-50 122 current-pricing">
                            <h5 className="mt-0 mb-0 bid-price-auction">Current BID</h5>
                            <h5 className="mt-0 mb-0 boat-price auction-price-font">
                                INR {minimumRaisedAmount}
                            </h5>
                        </Grid>
                    </Grid>
                </div>

                <Menu
                    id="long-menu"
                    keepMounted
                    open={isOpen}
                    onClose={() => this.setState({ isTrue: false })}
                >
                    {auctionCarouselOptions &&
                        auctionCarouselOptions.map((option) => (
                            <MenuItem
                                key={option}
                                onClick={() => this.setState({ isTrue: false })}
                                selected={option === "Pyxis"}
                            >
                                {option}
                            </MenuItem>
                        ))}
                </Menu>
            </div>
        </div>
    )
})