export const manufacture = [
  {
    img: "image/data/data1.jpg",
    boatTitle: "2017 Power Boat",
    boatPrice: "₹ 68,749,485.45",
    boatOwner: "by FYI International",
    boatCountry: "United States"
  },
  {
    img: "image/data/data2.jpg",
    boatTitle: "1990 Motor Yacht",
    boatPrice: "₹ 9,286,579.71",
    boatOwner: "by Qingdao ALLHEART Marine co.,ltd.",
    boatCountry: "Netherlands"
  },
  {
    img: "image/data/data3.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 50,032,348.05",
    boatOwner: "by Pi SUPER YACHTS Ltd",
    boatCountry: "United Kingdom"
  },
  {
    img: "image/data/data4.jpg",
    boatTitle: "2017 Fishing Boat",
    boatPrice: "₹ 56,871,302.10",
    boatOwner: "by Benetti Sail Shanghia Shipyard ",
    boatCountry: "China"
  },
  {
    img: "image/data/data5.jpg",
    boatTitle: "2005 Motor Yacht",
    boatPrice: "₹ 2,591,603.64",
    boatOwner: "by Rodman Polyships",
    boatCountry: "España"
  },
  {
    img: "image/data/data6.jpg",
    boatTitle: "1987 Power Boat",
    boatPrice: "₹ 11,878,183.35",
    boatOwner: "by FYI International",
    boatCountry: "United States"
  },
  {
    img: "image/data/data7.jpg",
    boatTitle: "2001 Fishing Boat",
    boatPrice: "₹ 2,771,576.12",
    boatOwner: "by Pi SUPER YACHTS Ltd",
    boatCountry: "Netherlands"
  },
  {
    img: "image/data/data8.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 212,367,520.50",
    boatOwner: "by Rodman Polyships",
    boatCountry: "United Kingdom"
  },
  {
    img: "image/data/data9.jpg",
    boatTitle: "2016 Power Boat",
    boatPrice: "₹ 23,036,476.80",
    boatOwner: "by Qingdao ALLHEART Marine co.,ltd.",
    boatCountry: "China"
  },
  {
    img: "image/data/data10.jpg",
    boatTitle: "1986 Fishing Boat",
    boatPrice: "₹ 16,485,478.71",
    boatOwner: "by Benetti Sail Division Shanghia Shipyard",
    boatCountry: "España"
  }
];

export const featuredBoats = [
  {
    img: "image/data/data3.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 50,032,348.05",
    boatCountry: "China"
  },
  {
    img: "image/data/data1.jpg",
    boatTitle: "2017 Power Boat",
    boatPrice: "₹ 68,749,485.45",
    boatCountry: "United States"
  },
  {
    img: "image/data/data4.jpg",
    boatTitle: "2017 Fishing Boat",
    boatPrice: "₹ 56,871,302.10",
    boatCountry: "Netherlands"
  },
  {
    img: "image/data/data2.jpg",
    boatTitle: "1990 Motor Yacht",
    boatPrice: "₹ 9,286,579.71",
    boatCountry: "España"
  },
  {
    img: "image/data/data8.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 212,367,520.50",
    boatCountry: "United States"
  },
  {
    img: "image/data/data6.jpg",
    boatTitle: "1987 Power Boat",
    boatPrice: "₹ 11,878,183.35",
    boatCountry: "China"
  },
  {
    img: "image/data/data7.jpg",
    boatTitle: "2001 Fishing Boat",
    boatPrice: "₹ 2,771,576.12",
    boatCountry: "Netherlands"
  },
  {
    img: "image/data/data9.jpg",
    boatTitle: "2016 Power Boat",
    boatPrice: "₹ 23,036,476.80",
    boatCountry: "España"
  },
  {
    img: "image/data/data5.jpg",
    boatTitle: "2005 Motor Yacht",
    boatPrice: "₹ 2,591,603.64",
    boatCountry: "United States"
  },
  {
    img: "image/data/data10.jpg",
    boatTitle: "1986 Fishing Boat",
    boatPrice: "₹ 16,485,478.71",
    boatCountry: "China"
  }
];

export const dealBoatsItems = [
  {
    img: "image/data/data3.jpg",
    boatTitle: "1989 Motor Yacht",
    boatCountry: "China",
    isProgress: true,
    isSoldOut: false
  },
  {
    img: "image/data/data1.jpg",
    boatTitle: "2017 Power Boat",
    boatCountry: "United States",
    isProgress: false,
    isSoldOut: true
  },
  {
    img: "image/data/data4.jpg",
    boatTitle: "2017 Fishing Boat",
    boatCountry: "Netherlands",
    isProgress: false,
    isSoldOut: true
  },
  {
    img: "image/data/data2.jpg",
    boatTitle: "1990 Motor Yacht",
    boatCountry: "España",
    isProgress: true,
    isSoldOut: false
  },
  {
    img: "image/data/data8.jpg",
    boatTitle: "1989 Motor Yacht",
    boatCountry: "United States",
    isProgress: false,
    isSoldOut: true
  },
  {
    img: "image/data/data6.jpg",
    boatTitle: "1987 Power Boat",
    boatCountry: "China",
    isProgress: false,
    isSoldOut: true
  },
  {
    img: "image/data/data7.jpg",
    boatTitle: "2001 Fishing Boat",
    boatCountry: "Netherlands",
    isProgress: true,
    isSoldOut: false
  },
  {
    img: "image/data/data9.jpg",
    boatTitle: "2016 Power Boat",
    boatCountry: "España",
    isProgress: true,
    isSoldOut: false
  },
  {
    img: "image/data/data5.jpg",
    boatTitle: "2005 Motor Yacht",
    boatCountry: "United States",
    isProgress: false,
    isSoldOut: true
  },
  {
    img: "image/data/data10.jpg",
    boatTitle: "1986 Fishing Boat",
    boatCountry: "China",
    isProgress: false,
    isSoldOut: true
  }
];

export const auctionBoatsItem = [
  {
    img: "image/black-1.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 50,032,348.05",
    boatCountry: "China",
    boatOwner: "by FYI International"
  },
  {
    img: "image/data/data6.jpg",
    boatTitle: "2017 Power Boat",
    boatPrice: "₹ 68,749,485.45",
    boatCountry: "United States",
    boatOwner: "by Pi SUPER YACHTS Ltd"
  },
  {
    img: "image/black-3.jpg",
    boatTitle: "2017 Fishing Boat",
    boatPrice: "₹ 56,871,302.10",
    boatCountry: "Netherlands",
    boatOwner: "by Rodman Polyships"
  },
  {
    img: "image/data/data2.jpg",
    boatTitle: "1990 Motor Yacht",
    boatPrice: "₹ 9,286,579.71",
    boatCountry: "España",
    boatOwner: "by Qingdao ALLHEART Marine co.,ltd."
  },
  {
    img: "image/data/data8.jpg",
    boatTitle: "1989 Motor Yacht",
    boatPrice: "₹ 212,367,520.50",
    boatCountry: "United States",
    boatOwner: "by Benetti Sail Shanghia Shipyard "
  },
  {
    img: "image/black-2.jpg",
    boatTitle: "1987 Power Boat",
    boatPrice: "₹ 11,878,183.35",
    boatCountry: "China",
    boatOwner: "by FYI International"
  },
  {
    img: "image/data/data7.jpg",
    boatTitle: "2001 Fishing Boat",
    boatPrice: "₹ 2,771,576.12",
    boatCountry: "Netherlands",
    boatOwner: "by Rodman Polyships"
  },
  {
    img: "image/data/data9.jpg",
    boatTitle: "2016 Power Boat",
    boatPrice: "₹ 23,036,476.80",
    boatCountry: "España",
    boatOwner: "by Rodman Polyships"
  }
];

export const boatType = [
  {
    Title: "Fishing Boat",
    icon: "adam-fishing-boat"
  },
  {
    Title: "Super Yacht",
    icon: "adam-super-yacht"
  },
  {
    Title: "Motor Yacht",
    icon: "adam-motor-yacht"
  },
  {
    Title: "Sailing Boat",
    icon: "adam-sailing-boat"
  },
  {
    Title: "Power Boat",
    icon: "adam-power-boat"
  },
  {
    Title: "Bow Boat",
    icon: "adam-bow-boat"
  },
  {
    Title: "Cuddy Boat",
    icon: "adam-cuddy-boat"
  },
  {
    Title: "Canone",
    icon: "adam-small-canoe"
  },
  {
    Title: "Kayak",
    icon: "adam-kayak"
  },
  {
    Title: "Jet Ski",
    icon: "adam-jetski"
  }
];

export const Trip = [
  {
    alias: "Shared Trip",
    icon: "adam-sharetrip"
  },
  {
    alias: "Private Trip",
    icon: "adam-preivatetrip"
  },
  {
    alias: "Rate Per Hour",
    icon: "adam-perhour"
  }
];

export const relatedGallery = [
  {
    img: "image/yacht services/img-1.jpg",
    boatCountry: "Greece",
    yachtCompany: "Greenwich Incorporated",
    teamCommit: "Team Commitment",
    teamDescription:
      "The wide exterior spaces are designed in every single detail as well, to guarantee"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatCountry: "Australia, GoldCost",
    yachtCompany: "Greenwich Incorporated",
    teamCommit: "Team Commitment",
    teamDescription:
      "The wide exterior spaces are designed in every single detail as well, to guarantee"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatCountry: "Australia, GoldCost",
    yachtCompany: "Greenwich Incorporated",
    teamCommit: "Team Commitment",
    teamDescription:
      "The wide exterior spaces are designed in every single detail as well, to guarantee"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatCountry: "Australia, GoldCost",
    yachtCompany: "Greenwich Incorporated",
    teamCommit: "Team Commitment",
    teamDescription:
      "The wide exterior spaces are designed in every single detail as well, to guarantee"
  },
  {
    img: "image/yacht services/img-1.jpg",
    boatCountry: "Australia, GoldCost",
    yachtCompany: "Greenwich Incorporated",
    teamCommit: "Team Commitment",
    teamDescription:
      "The wide exterior spaces are designed in every single detail as well, to guarantee"
  }
];

export const rentSharedTrips = [
  {
    img: "image/yacht services/img-1.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-1.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const boatGrid = [
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const beastDeal = [
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const addedBoats = [
  {
    image: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    image: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const topTwenty = [
  {
    image: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    image: "image/yacht services/img-4.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const mostPopular = [
  {
    image: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "4",
    height: "35"
  },
  {
    image: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  },
  {
    image: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    boatDate: "24",
    height: "35"
  }
];

export const exploreAdamSeaHome = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Greece"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One"
  }
];
export const rentSharedTripsData = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  }
];

export const rentGallery = [
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  }
];
export const gridSharedTrips = [
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35",
    extension: "person"
  }
];

export const popularTrips = [
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35",
    extension: "person"
  }
];
export const gridPrivateTrips = [
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "person"
  }
];
export const gridRecommendeTrips = [
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35",
    extension: "hour"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35",
    extension: "hour"
  }
];
export const exploreAdamSeaRent = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Deep Sea Fishing"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Sea Tour"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Cruiser Tour"
  }
];

export const rentReviews = [
  {
    img: "image/yacht services/img-2.jpg",
    viewerImg: "image/reviewer-1.jpg",
    reviewCountry: "Dubai, United Arab Emirates",
    reviewerPrice: "Price Gutierrez",
    review:
      "trip is not bad at all , it was amazing but the sea was high waves",
    reviewRating: "4.0",
    reviewer: "John Doe",
    reviewTime: "1"
  },
  {
    img: "image/yacht services/img-2.jpg",
    viewerImg: "image/reviewer-1.jpg",
    reviewCountry: "Dubai, United Arab Emirates",
    reviewerPrice: "Price Gutierrez ",
    review:
      "trip is not bad at all , it was amazing but the sea was high waves",
    reviewRating: "4.0",
    reviewer: "John Doe",
    reviewTime: "1"
  }
];
export const boatListing = [
  {
    img: "image/yacht services/img-2.jpg",
    boatLink: "2013 JET SKI WWSD",
    boatTopic: "Why to buy this boat and review",
    boatStatus: "Under Construction",
    boatCity: "Canada | Mississauga",
    boatRating: " 0.0",
    boatPrice: "1635.07",
    boatDate: "31,May,2015",
    boatLogoImg: "image/yacht services/img-3.jpg",
    companyName: "AdamSea"
  },
  {
    img: "image/yacht services/img-2.jpg",
    boatLink: "2013 JET SKI WWSD",
    boatTopic: "Why to buy this boat and review",
    boatStatus: "Under Construction",
    boatCity: "Canada | Mississauga",
    boatRating: " 0.0",
    boatPrice: "1635.07",
    boatDate: "31,May,2015",
    boatLogoImg: "image/yacht services/img-3.jpg",
    companyName: "AdamSea"
  }
];
export const boatAddedServiceData = [
  {
    img: "image/yacht services/img-5.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Dubai, United Arab Emirates",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  },
  {
    img: "image/yacht services/img-6.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Romania | Constanta",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  },
  {
    img: "image/yacht services/img-3.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Romania | Constanta",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  },
  {
    img: "image/yacht services/img-2.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Romania | Constanta",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  },
  {
    img: "image/yacht services/img-7.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Romania | Constanta",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  },
  {
    img: "image/yacht services/img-8.jpg",
    serviceName: "Sea Loyal Group Hsqe srl",
    serviceCountry: "Romania | Constanta",
    serviceDesc:
      "No investment it's enough expensive as knowledge and professionalism.",
    serviceRating: "4.0"
  }
];
export const rentInnerReview = [
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  }
];
export const marinaInnerReview = [
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  }
];
export const rentInnerTrip = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  }
];
export const marinaInnerTrip = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  }
];
export const boatInnerReview = [
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  },
  {
    img: "image/yacht services/img-8.jpg",
    rentInnerReviewName: "Wade Mitchell",
    rentInnerReviewTime: "2",
    rentInnerReviewDesc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
  }
];
export const boatInnerTrip = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-7.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatName: "Greece",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "24",
    height: "35"
  },
  {
    img: "image/yacht services/img-9.jpg",
    boatName: "Soleanis Afser One",
    boatPrice: "5784526",
    boatCountry: "Team Commitment",
    boatTime: "24",
    person: "4",
    height: "35"
  }
];
export const exploreBoatService = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Administration"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Barrier Coating"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Boat Show Organizing"
  },
  {
    img: "image/yacht services/img-5.jpg",
    boatName: "Crew Management"
  },
  {
    img: "image/yacht services/img-6.jpg",
    boatName: "Crew Management"
  }
];
export const exploreAdamSeaMarina = [
  {
    img: "image/yacht services/img-2.jpg",
    boatName: "Marina"
  },
  {
    img: "image/yacht services/img-3.jpg",
    boatName: "Storage"
  },
  {
    img: "image/yacht services/img-4.jpg",
    boatName: "Both"
  }
];
export const marinaReviews = [
  {
    img: "image/yacht services/img-2.jpg",
    viewerImg: "image/reviewer-1.jpg",
    reviewFor: "Marlen mckmary",
    reviewerTopic: "Hanse 311 - 2 Cabin Sailing Yacht for Rent",
    review:
      "trip is not bad at all , it was amazing but the sea was high waves",
    reviewRating: "4.0",
    reviewer: "John Doe",
    reviewTime: "1"
  },
  {
    img: "image/yacht services/img-2.jpg",
    viewerImg: "image/reviewer-1.jpg",
    reviewFor: "Marlen mckmary",
    reviewerTopic: "Hanse 311 - 2 Cabin Sailing Yacht for Rent",
    review:
      "trip is not bad at all , it was amazing but the sea was high waves",
    reviewRating: "4.0",
    reviewer: "John Doe",
    reviewTime: "1"
  }
];
export const gridMarinaStorage = [
  {
    img: "image/yacht services/img-2.jpg",
    storageCity: "Croatia, Korčula",
    storagePlace: "Leda Shipyard",
    storageDesc:
      "24/7 year round gated facility with guards and CCTV surveillance, for your vessel's...",
    storageRating: "4.0"
  },
  {
    img: "image/yacht services/img-3.jpg",
    storageCity: "Croatia, Korčula",
    storagePlace: "Leda Shipyard",
    storageDesc:
      "24/7 year round gated facility with guards and CCTV surveillance, for your vessel's...",
    storageRating: "4.0"
  },
  {
    img: "image/yacht services/img-4.jpg",
    storageCity: "Croatia, Korčula",
    storagePlace: "Leda Shipyard",
    storageDesc:
      "24/7 year round gated facility with guards and CCTV surveillance, for your vessel's...",
    storageRating: "4.0"
  },
  {
    img: "image/yacht services/img-5.jpg",
    storageCity: "Croatia, Korčula",
    storagePlace: "Leda Shipyard",
    storageDesc:
      "24/7 year round gated facility with guards and CCTV surveillance, for your vessel's...",
    storageRating: "4.0"
  }
];
export const marinaGallaryData = [
  {
    img: "image/yacht services/img-2.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-3.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-4.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-5.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-6.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-7.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-8.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  },
  {
    img: "image/yacht services/img-9.jpg",
    reviewerImg: "image/reviewer-1.jpg",
    storageName: "Greenwich Incorporated ",
    reviewerName: "John Doe",
    reviewDate: "29, April, 2015",
    reviewerCity: "United States",
    reviewerDesc:
      "The wide exterior spaces are designed in every single detail as well, to garantee...",
    view: "810"
  }
];

export const boatShowData = [
  {
    img: "image/yacht services/img-8.jpg",
    boatShowName: "Spring Cottage Life Show",
    boatShowCity: "Dubai, United Arab Emirates",
    boatShowDate: "12 Dec 2018 - 10 Jan 2020",
    boatShowTime: "10am - 5pm",
    boatShowDesc:
      "Welcome to the Spring Boat, Cottage & Outdoor Show at the beautiful Port of Orillia and waterfront parks, Lake Couchiching, Orillia, Ontario, Canada! New and pre-owned boats of all sizes make up this show, from inflatables to offshore performance to fishing boats to cruisers! Started in the early 1990's for the large pre-owned boat market, this is an established trade show for the boating industry and private sector. This is one of the few shows offering private owners an opportunity to sell their boat. Cost, location, time of year and ease of in-water testing all contribute to the success of the Spring Boat, Cottage & Outdoor Show! The show also features a variety of marine, cottage and outdoor products, land vendors and marine safety demonstrations."
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatShowName: "Spring Cottage Life Show",
    boatShowCity: "Dubai, United Arab Emirates",
    boatShowDate: "12 Dec 2018 - 10 Jan 2020",
    boatShowTime: "10am - 5pm",
    boatShowDesc:
      "Welcome to the Spring Boat, Cottage & Outdoor Show at the beautiful Port of Orillia and waterfront parks, Lake Couchiching, Orillia, Ontario, Canada! New and pre-owned boats of all sizes make up this show, from inflatables to offshore performance to fishing boats to cruisers! Started in the early 1990's for the large pre-owned boat market, this is an established trade show for the boating industry and private sector. This is one of the few shows offering private owners an opportunity to sell their boat. Cost, location, time of year and ease of in-water testing all contribute to the success of the Spring Boat, Cottage & Outdoor Show! The show also features a variety of marine, cottage and outdoor products, land vendors and marine safety demonstrations."
  },
  {
    img: "image/yacht services/img-8.jpg",
    boatShowName: "Spring Cottage Life Show",
    boatShowCity: "Dubai, United Arab Emirates",
    boatShowDate: "12 Dec 2018 - 10 Jan 2020",
    boatShowTime: "10am - 5pm",
    boatShowDesc:
      "Welcome to the Spring Boat, Cottage & Outdoor Show at the beautiful Port of Orillia and waterfront parks, Lake Couchiching, Orillia, Ontario, Canada! New and pre-owned boats of all sizes make up this show, from inflatables to offshore performance to fishing boats to cruisers! Started in the early 1990's for the large pre-owned boat market, this is an established trade show for the boating industry and private sector. This is one of the few shows offering private owners an opportunity to sell their boat. Cost, location, time of year and ease of in-water testing all contribute to the success of the Spring Boat, Cottage & Outdoor Show! The show also features a variety of marine, cottage and outdoor products, land vendors and marine safety demonstrations."
  }
];
export const rentInnerUserReviews = [
  {
    date: "October 2019",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
    name: "Lorem ipsum dolor",
    country: "Australia",
    joinedDate: "Joined in 2019"
  },
  {
    date: "January 2017",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
    name: " sit amet consectetur",
    country: "Taiwan",
    joinedDate: "Joined in 2016"
  },
  {
    date: "February 2013",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt",
    name: "adipiscing elit",
    country: "Mexico",
    joinedDate: "Joined in 2011"
  },
  {
    date: "March 2015",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
    name: "eiusmod tempor incididunt",
    country: "TN",
    joinedDate: "Joined in 2015"
  },
  {
    date: "April 2018",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ",
    name: "et dolore magna aliqua",
    country: "Italy",
    joinedDate: "Joined in 2016"
  },
  {
    date: "May 2015",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor ",
    name: "Ut enim ad minim veniam",
    country: "KY",
    joinedDate: "Joined in 2013"
  },
  {
    date: "October 2019",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
    name: "Lorem ipsum dolor",
    country: "Australia",
    joinedDate: "Joined in 2019"
  },
  {
    date: "January 2017",
    desc:
      "  Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
    name: " sit amet consectetur",
    country: "Taiwan",
    joinedDate: "Joined in 2016"
  }
];
export const multiSelectData = [
  {
    value: "qwe",
    label: "qwe"
  },
  {
    value: "rty",
    label: "rty"
  },
  {
    value: "uio",
    label: "uio"
  },
  {
    value: "pas",
    label: "pas"
  },
  {
    value: "dfg",
    label: "dfg"
  },
  {
    value: "hjk",
    label: "hjk"
  },
  {
    value: "lzx",
    label: "lzx"
  },
  {
    value: "cvb",
    label: "cvb"
  },
  {
    value: "nm",
    label: "nm"
  }
];

export const boatMediaArticle = [
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
  {
    "id": "5e19c0b5008679608e94bc40",
    "user": {
      "city": "ahmedabad",
      "country": "India",
      "firstName": "boat",
      "middleName": "",
      "lastName": "broker",
      "id": "5db2d040341e3c1dc396b40d",
      "image": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/people19.png.png",
    },
    "title": "asd",
    "titleSlug": "asd-712",
    "description": "<p>asd</p>",
    "file": "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/download.jpeg.jpeg",
    "articleViewCount": null,
    "articleApproved": null,
    "status": true,
    "adId": "ARALHU40PT9SVBW4",
    "createdAt": "2020-01-11T12:33:57.523Z",
    "updatedAt": "2020-01-11T12:33:57.523Z"
  },
];

export const boatMediaVideo = [
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    videoTitle: "NAME OF THE AD",
    videoDate: "December 05,2019",
    videoDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
    videoUserImg: require("../../assets/images/header/img-7.jpg"),
    videoUserName: "merinda Williamson",
    videoUserType: "User Type",
    videoUserCity: "Salerno",
    videoUserCountry: "Italy"
  }
];

export const mediaHome = {
  boatMediaImg: require("../../assets/images/boatInner/1.jpg"),
  articleImage: require("../../assets/images/marinaStorageInner/marina.jpeg"),
  reviewImage: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
}

export const boatMediaPost = [
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    postTitle:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
    postDate: "December 05,2019"
  }
];

export const boatMediaReview = [
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  },
  {
    img: require("../../assets/images/header/img-7.jpg"),
    reviewDate: "December 05,2019",
    reviewDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    reviewerName: "merinda Williamson",
    reviewerType: "User Type"
  }
];

export const boatMediaCommonVideo = [
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    videoTitle: "know how to pay with AdamSea",
    videoDesc:
      "use AdamSea escrow account the world secured payment",
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina.jpeg"),
    videoTitle: "SELL WITH US",
    videoDesc:
      "sell your yacht today with AdamSea",
  },
  {
    img: require("../../assets/images/marinaStorageInner/marina&storage1.jpeg"),
    videoTitle: "HOW TO BUY boat in AdamSea",
    videoDesc:
      "Today you can buy your boat from any where in the world",
  },
];

export const userProfileListingInner = [
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  },
  {
    img: require("../../assets/images/boatInner/1.jpg"),
    listingTitle: "The Beach at JBR",
    listingCity: "Dubai",
    listingCountry: "United Arab Emirates",
    listingDesc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
  }
];