import React from 'react'

export const Loader = () => {
    return (
        <img src={require('../../assets/images/loader/Loading_icon.gif')} alt="Loading..." />
    )
}
