import React, { Component } from "react";
import { CardContent, Card, Select } from "@material-ui/core";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import "../../containers/boatServices/boatServices.scss";
import SearchComplete from "../map/SearchComplete";
import { renderYachtServicesMenuItems } from "../../helpers/jsxHelper";
import { BoatServiceBannerFormStyle } from "../styleComponent/styleComponent";

class BoatServiceCard extends Component {
  setPlaceHandler = ({ country, city }, setFieldValue) => {
    setFieldValue("country", country);
    setFieldValue("city", city);
  };

  getNewWidth = () => {
    const width = document.querySelector("body");
    const actualWidth = width && width.offsetWidth / 4;
    return actualWidth;
  };

  render() {
    const { iconBg, services, search } = this.props;

    const width = this.getNewWidth();

    return (
      <Formik
        initialValues={{
          country: "",
          city: "",
          service: ""
        }}
        // validationSchema={Yup.object().shape({
        //   country: Yup.string().required("Please add any place"),
        //   city: Yup.string().required("Please add any place"),
        //   service: Yup.string().required("Service is required")
        // })}
        onSubmit={values => {
          search(values);
        }}
        render={({ values, setFieldValue, handleSubmit }) => (
          <Form className="bg-transparent-form" onSubmit={e => e.preventDefault()}>
            <BoatServiceBannerFormStyle bgWidth={width}>
              <div className="boat-service-banner-form-div">

                <Card className="boat-service-banner-form-card card p-0 br-10">
                  <CardContent className="p-4">
                    <div className="boat-service-banner-form-inner-size mt-2">
                      <div className="boat-service-banner-form-info">
                        <SearchComplete
                          placeHolder=" Where would you like to search?"
                          className="form-control font-14"
                          getPlaceName={place =>
                            this.setPlaceHandler(place, setFieldValue)
                          }
                        />
                        <ErrorMessage
                          component="div"
                          name={"city" || "country"}
                          className="error-message"
                        ></ErrorMessage>
                      </div>

                      <div className="boat-service-banner-form-info mt-3 mb-0">
                        <Select
                          className="form-control font-14 select-div-padding boat-service-banner-form-option"
                          fullWidth
                          labelId="service"
                          value={
                            values.service ? values.service : "placeholder"
                          }
                          variant="standard"
                          renderValue={selected => {
                            if (selected === "placeholder") {
                              return "  Service you looking for?";
                            } else {
                              const selectedService = services.find(
                                item => item.id === selected
                              );
                              return selectedService.name;
                            }
                          }}
                          onChange={e =>
                            setFieldValue("service", e.target.value)
                          }
                        >
                          {renderYachtServicesMenuItems(services)}
                        </Select>
                        <ErrorMessage
                          component="div"
                          name="service"
                          className="error-message"
                        ></ErrorMessage>
                      </div>

                      <div className="mt-3 mb-0 d-flex justify-content-between">
                        <a className="btn btn-link-info pl-0" href="#">
                          <u className="font-weight-500 boat-service-banner-notify-button light-green boatservice-notify">NOTIFY ME</u>
                        </a>
                        <button
                          type="button"
                          className={`btn btn-danger font-weight-600 boatService-banner-btn p-1 pl-2 pr-2 boat-service-banner-submit-button search-button-width ${iconBg}`}
                          onClick={handleSubmit}
                        >
                          Search Now
                        </button>
                      </div>
                    </div>
                  </CardContent>
                </Card>
              </div>
            </BoatServiceBannerFormStyle>
          </Form>
        )}
      />
    );
  }
}

export default BoatServiceCard;
