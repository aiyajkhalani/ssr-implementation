import React from "react";
import { ServicesStyle, ServiceCardTextStyle } from "../styleComponent/styleComponent";
import { Grid, Box } from "@material-ui/core";

export const ExploreAdamSea = React.memo(({ width, height, name,img, redirectRouteHandler }) => {
    return (

        <Grid item xs={12} sm={6} md={3} lg={3}>
            <div className="main-category">
                <div
                    className="service-card cursor-pointer"
                    onClick={() => redirectRouteHandler()}
                >
                    <ServicesStyle
                        width={width}
                        height={height}
                        img={img}
                    />
                    <ServiceCardTextStyle width={width} className="service-card-text">
                        <Box
                            className={`categoryContent-Font`}
                            fontSize={14}
                            fontWeight={600}
                        >
                            {name}
                        </Box>
                    </ServiceCardTextStyle>
                </div>
            </div>
        </Grid>

    )
})