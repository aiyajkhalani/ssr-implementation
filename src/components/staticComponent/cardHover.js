import React, { Component } from "react";
import styled from "@emotion/styled/macro";
import { BrowserView, MobileView } from "react-device-detect";
import { Button, Typography, Box } from "@material-ui/core";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import "../home/home.scss";

const Hover = styled.div({
  opacity: 0,
  transition: "opacity 350ms ease"
});

const DisplayOver = styled.div({
  height: "100%",
  left: "0",
  position: "absolute",
  top: "0",
  width: "100%",
  zIndex: 2,
  transition: "background-color 350ms ease",
  backgroundColor: "transparent",
  padding: "20px 20px 0 20px",
  boxSizing: "border-box",
  borderRadius: "16px"
});

const SubTitle = styled.div({});

const Paragraph = styled.p({});

const Background1 = styled.div({
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  color: "#FFF",
  position: "relative",
  width: "100%",
  height: "35vh",
  cursor: "pointer",
  backgroundImage: "url(image/yacht-2.jpg)",
  [`:hover ${DisplayOver}`]: {
    backgroundColor: "rgba(0,0,0,.5)",
    borderRadius: "4px"
  },
  [`:hover ${SubTitle}, :hover ${Paragraph}`]: {
    transform: "translate3d(0,0,0)"
  },
  [`:hover ${Hover}`]: {
    opacity: 1
  }
});

const CTA = styled.a({
  position: "absolute",
  bottom: "20px",
  right: "20px",
  fontWeight: "bold"
});
// Note =  I will Change this Component Tomorrow Morning
export class CardHover extends Component {
  createMarkup = data => {
    return { __html: data };
  };

  render() {
    const { adData, history } = this.props;
    return (
      <div className="Card-Hover">
        {this.props.isFlag && adData && (
          <div className="card-radius section-heading border-radius card-hover-background">
            <img
              src={adData.imageUrl && adData.imageUrl}
              alt="registeration"
              height="350"
              width={"100%"}
            />
            <DisplayOver className="card-hover-displayover card-hover-background">
              <BrowserView>
                <div className="text-left font-color-brochure">
                  <Box fontSize={24} fontWeight={500}>
                    {adData.title}
                  </Box>
                  <SubTitle>
                    <Box fontSize={16} width={"70%"} className="mt-16">
                      <div
                        dangerouslySetInnerHTML={this.createMarkup(
                          adData.description
                        )}
                      />
                    </Box>
                  </SubTitle>

                  <Button
                    type="button"
                    onClick={() => {
                      history.push(`${adData.link}`);
                    }}
                  >
                    {adData.linkTitle}
                    <KeyboardArrowRight />
                  </Button>
                </div>
              </BrowserView>
              <MobileView>
                <h2>Listed By Broker & Dealers</h2>
                <Hover>
                  <SubTitle>
                    <h3>Lorem ipsum dolor sit amet</h3>
                  </SubTitle>
                  <Paragraph>
                    Listed by in order to view all the yachts and boats by that
                    broker or owner
                  </Paragraph>
                  <CTA>
                    <h2>View More +</h2>
                  </CTA>
                </Hover>
              </MobileView>
            </DisplayOver>
          </div>
        )}
        {!this.props.isFlag && (
          <Background1 className="card-radius section-heading">
            <DisplayOver>
              <BrowserView>
                <div className="text-left font-color-brochure">
                  <Box fontSize={24} fontWeight={500}>
                    Listed By Broker & Dealers
                  </Box>
                  <SubTitle>
                    <Box fontSize={16} width={"70%"} className="mt-16">
                      Loren ipsum Listed by in order to view all the yachts and
                      boats by that broker or owner
                    </Box>
                  </SubTitle>
                  <Button>
                    View More
                    <KeyboardArrowRight />
                  </Button>
                </div>
              </BrowserView>
              <MobileView>
                <h2>Listed By Broker & Dealers</h2>
                <Hover>
                  <SubTitle>
                    <h3>Lorem ipsum dolor sit amet</h3>
                  </SubTitle>
                  <Paragraph>
                    Listed by in order to view all the yachts and boats by that
                    broker or owner
                  </Paragraph>
                  <CTA>
                    <h2>View More +</h2>
                  </CTA>
                </Hover>
              </MobileView>
            </DisplayOver>
          </Background1>
        )}
      </div>
    );
  }
}
