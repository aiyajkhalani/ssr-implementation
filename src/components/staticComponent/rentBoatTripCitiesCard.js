import React from "react";
import { Grid } from "@material-ui/core";

import { dimensionAspectRatio } from "../../util/utilFunctions";
import { CityWiseComponent } from "../cityWiseComponent/cityWiseComponent";

export const RentBoatTripCitiesCard = ({
  data,
  getCityWiseRentBoats,
  limit
}) => {
  const result =
    data &&
    data.length > 0 &&
    data.map((item, i) => {
      if (item.countryImage == null) {
        return {
          image: `image/slide-${i + 1}.jpg`,
          city: item.cityName,
          minimumPrice: item.minimumPrice
        };
      } else {
        return {
          image: item.countryImage,
          city: item.cityName,
          minimumPrice: item.minimumPrice
        };
      }
    });

  const getNewWidth = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5 - 15;
    const aspectRatio = dimensionAspectRatio(320, 330);
    const newWidth = actualWidth / aspectRatio - 5;

    return newWidth;
  };

  const getNewHeight = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5;
    const aspectRatio = dimensionAspectRatio(270, 270);
    const newHeight = actualWidth / aspectRatio - 5;

    return newHeight;
  };

  const height = getNewHeight();
  const width = getNewWidth();

  return (
    <div className="city-from-country">
      <Grid container spacing={2}>
        {result && result.length > 0 &&
          result.map((item, i) => (
            <>
              {limit > i && (
                <Grid
                  item
                  xs={12}
                  sm={4}
                  md={true}
                  key={item.id}
                  className="cursor-pointer"
                >
                  <CityWiseComponent
                      value={item}
                      img={encodeURI(item.image)}
                      getCityWiseBoats={() => getCityWiseRentBoats(item)}
                      city={item.city}
                      width={width}
                      height={height}
                    />
                </Grid>
              )}
            </>
          ))
        }
      </Grid>
    </div>
  );
};
