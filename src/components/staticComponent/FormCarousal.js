import React, { Component } from "react";
import {
  Grid,
  CardContent,
  Card,
  CardActions,
  Box,
  Select,
  MenuItem,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Form, Formik } from "formik";
import InputRange from "react-input-range";

import { renderBoatTypes } from "../../helpers/jsxHelper";
import SearchComplete from "../map/SearchComplete";
import UserContext from "../../UserContext";

import "../home/boatTypeCarousal.scss";
import "react-input-range/lib/css/index.css";
import { priceFormat } from "../../util/utilFunctions";

class FormCarousal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      boat: {
        country: "",
        city: "",
        boatType: "",

        minLength: null,
        maxLength: null,
        minPrice: null,
        maxPrice: null,
        minYearBuild: null,
        maxYearBuild: null
      },

      fields: {
        length: {
          min: 0,
          max: 0
        },
        price: {
          min: 0,
          max: 0
        },
        yearBuild: {
          min: 0,
          max: 0
        }
      },

      isUpdateValue: false
    };
  }

  static getDerivedStateFromProps(props, prevState) {
    const { boatSearchValues } = props;
    const { isUpdateValue } = prevState;

    if (boatSearchValues && Object.entries(boatSearchValues).length && !isUpdateValue) {
      return {
        fields: {
          length: {
            min: boatSearchValues.minLengthInFt,
            max: boatSearchValues.maxLengthInFt
          },
          price: {
            min: boatSearchValues.minPrice,
            max: boatSearchValues.maxPrice
          },
          yearBuild: {
            min: boatSearchValues.minYearBuild,
            max: boatSearchValues.maxYearBuild
          }
        },
        isUpdateValue: true
      };
    }

    return null;
  }

  static contextType = UserContext;

  setValueHandler = (value, name) => {
    const { fields } = this.state;
    fields[name] = value;

    this.setState({ fields });
  };

  setPlaceHandler = ({ country, city }, setFieldValue) => {
    setFieldValue("country", country);
    setFieldValue("city", city);
  };

  render() {
    const { boat, fields } = this.state;
    const { searchBoat, boatTypes, boatSearchValues } = this.props;

    const { length, price, yearBuild } = fields;

    return (
      <div className="home-card">
        {boatSearchValues && <Formik
          initialValues={boat}
          onSubmit={values => {
            values.minLength = length.min;
            values.maxLength = length.max;
            values.minPrice = price.min;
            values.maxPrice = price.max;
            values.minYearBuild = yearBuild.min;
            values.maxYearBuild = yearBuild.max;

            searchBoat(values);
          }}
          render={({ errors, setFieldValue, values, handleSubmit }) => (
            <Form className="bg-transparent-form" onSubmit={e => e.preventDefault()}>
              <Card className="profile-container card p-0 home-banner-form-div-border banner-home-form">
                <CardContent className="home-banner-form-div">
                  <h2 className="fontWeight-500 home-banner-form-title mb-3">Search Boat</h2>

                  {boatTypes && boatTypes.length > 0 && (
                    <>
                      <Select
                        className="h-33 placeholder-text-size"
                        fullWidth
                        value={
                          values.boatType === ""
                            ? "placeholder"
                            : values.boatType
                        }
                        variant="outlined"
                        renderValue={selected => {
                          if (selected === "placeholder") {
                            return "Boat Type";
                          } else {
                            const selectedboatTypes = boatTypes && boatTypes.find(
                              item =>
                                item.id === selected

                            );
                            return selectedboatTypes.name;
                          }
                        }}
                        onChange={e =>
                          setFieldValue("boatType", e.target.value)
                        }
                      >
                        {renderBoatTypes(boatTypes)}
                      </Select>
                    </>
                  )}

                  <SearchComplete
                    className="form-control mt-3"
                    getPlaceName={place =>
                      this.setPlaceHandler(place, setFieldValue)
                    }
                  />

                  <Grid container spacing={3} justify="space-between">
                    <Grid item sm={12} className="pt-0 range-div">


                      <div className="dFlex-1 flex-row d-flex justify-content-between padding-right-10 mt-2">
                        <Box
                          fontWeight={400}
                          className="form-title"
                          letterSpacing={1}
                          fontSize={14}
                        >
                          Length
                        </Box>
                        <div className="range-color-blue">{`${boatSearchValues.minLengthInFt > 10 ? boatSearchValues.minLengthInFt : 10} - ${boatSearchValues.maxLengthInFt}`} (ft)</div>
                      </div>

                      <div className="price-count-container pb-0 pl-2 pr-2 home-banner-search-form-div">
                        <InputRange
                          minValue={boatSearchValues.minLengthInFt}
                          maxValue={boatSearchValues.maxLengthInFt}
                          value={length}
                          formatLabel={value => null}
                          onChange={value =>
                            this.setValueHandler(value, "length")
                          }
                        />
                      </div>
                    </Grid>

                    <Grid item sm={12} className="pt-0">
                      <div className="dFlex-1 flex-row d-flex justify-content-between padding-right-10">
                        <Box
                          fontWeight={400}
                          className="form-title"
                          letterSpacing={1}
                          fontSize={14}
                        >
                          Price $
                        </Box>
                        <div className="range-color-blue">{`${boatSearchValues.minPrice > 0 ? priceFormat(boatSearchValues.minPrice) : 0} - ${boatSearchValues.maxPrice ? priceFormat(boatSearchValues.maxPrice) : 100}`} $</div>
                      </div>

                      <div className="price-count-container pb-0 pl-2 pr-2 home-banner-search-form-div">
                        <InputRange
                          minValue={boatSearchValues.minPrice}
                          maxValue={boatSearchValues.maxPrice}
                          value={price}
                          formatLabel={value => null}
                          onChange={value =>
                            this.setValueHandler(value, "price")
                          }
                        />
                      </div>
                    </Grid>

                    <Grid item sm={12} className="pt-0">
                      <div className="dFlex-1 flex-row d-flex justify-content-between padding-right-10">
                        <Box
                          fontWeight={400}
                          className="form-title"
                          letterSpacing={0.6}
                          fontSize={14}
                        >
                          Year Build
                        </Box>
                        <div className="range-color-blue">{`${boatSearchValues.minYearBuild > 1950 ? boatSearchValues.minYearBuild : 1950} - ${boatSearchValues.maxYearBuild}`} (Y)</div>
                      </div>

                      <div className="price-count-container pb-0 pl-2 pr-2 home-banner-search-form-div">
                        <InputRange
                          minValue={boatSearchValues.minYearBuild}
                          maxValue={boatSearchValues.maxYearBuild}
                          value={yearBuild}
                          formatLabel={value => null}
                          onChange={value =>
                            this.setValueHandler(value, "yearBuild")
                          }
                        />
                      </div>
                    </Grid>
                  </Grid>
                </CardContent>
                <CardActions className="home-banner-form-button-div d-flex justify-content-between">
                  <a className="f-10 float-right" href="#">
                    <u className="f-12 darkBlue notify-text-effect">Notify Me</u>
                  </a>
                  <button
                    type="button"
                    className="btn btn-danger home-form-btn font-weight-600 text-white background-dark-blue border-dark-blue home-banner-search-button home-search-button-width"
                    onClick={handleSubmit}
                  >
                    Search Now
                  </button>
                </CardActions>
              </Card>
            </Form>
          )}
        ></Formik>}
      </div>
    );
  }
}

export default FormCarousal;
