import React, { Component } from "react";
import Truncate from "react-truncate";

import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import { getHeightRatio, getRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";

class RegisterCard extends Component {
  createMarkup = data => {
    return { __html: data };
  };
  state = {
    height: 300,
    width: 1600
  };
  componentDidMount() {
    const { isDouble } = this.props
    const enumName = isDouble ? "marinaRegisterDouble" : "marinaRegister"
    const width = getRatio(dimension, enumName , ".section-heading");
    const height = getHeightRatio(
      dimension,
      "marinaRegister",
      ".section-heading"
    );
    this.setState({ width, height });
  }

  render() {
    const { adData, history, isSpace, isSpace2 } = this.props;
    const { height, width } = this.state;
    return (

      <div className="register-card-main">
        <div className="mt-50">
          <div className="row">
            <div className={`col-12 ${isSpace ? 'pr-3' : isSpace2 ?'pl-3' : '' }`}>
              <CommonAdamSeaStyle
                className="w-100"
                img={encodeURI(adData.imageUrl)}
                width={width}
                height={height}
              >
                <div className="d-flex register-card">
                  <div className="h2">{adData.title}</div>

                  <p className="margin-bottom-advertise">
                  <Truncate lines={2} ellipsis={<span>..</span>}>
                    <div className="register-description"
                      dangerouslySetInnerHTML={this.createMarkup(
                        adData.description
                      )}
                    />
                    </Truncate>
                  </p>
                  <div className="register-link-name cursor-pointer"
                  
                    onClick={() => {
                      // history.push(`${adData.link}`); WIP
                    }}
                  >
                    {adData.linkTitle}
                  </div>
                </div>
              </CommonAdamSeaStyle>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterCard;
RegisterCard.default ={
  isDouble: false,
  isSpace2: false,
  isSpace:false
}