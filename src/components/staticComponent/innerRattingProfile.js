import React, { Component } from "react";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Progress } from "react-sweet-progress";

import "../../containers/rentInner/rentInner.scss";

class InnerRattingProfile extends Component {
  
  countRatingPercentage = (star, total) => {
    return star && (100 * star) / total;
  };

  renderProgressBar = (star, total) => {
    const { progressColor } = this.props
    return (
      <Progress
        className={`progress-bar`}
        style={{backgroundColor: 'transparent'}}
        symbolClassName="symbol-progress-none"
        percent={this.countRatingPercentage(star, total)}
      />
    )
  }

  renderRatingByReviews = () => {
    const { reviews } = this.props;

    let data = {
      total: reviews.length,
      stars: {
        oneStar: 0,
        twoStar: 0,
        threeStar: 0,
        fourStar: 0,
        fiveStar: 0
      }
    };

    reviews &&
      reviews.length > 0 &&
      reviews.map(review => {
        switch (review.rating) {
          case 1:
            return (data.stars.oneStar += 1);
          case 2:
            return (data.stars.twoStar += 1);

          case 3:
            return (data.stars.threeStar += 1);

          case 4:
            return (data.stars.fourStar += 1);

          case 5:
            return (data.stars.fiveStar += 1);

          default:
            return null;
        }
      });

    return (
      <div>
        <div className="d-flex align-items-center mb-2">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled mr-3 rating-clr"></i>
            <span className="mr-3">
              <h6 className="mb-0">{data.stars.fiveStar}</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.fiveStar, data.total)}
          </div>
        </div>
        <div className="d-flex align-items-center mb-2">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled mr-3 rating-clr"></i>
            <span className="mr-3">
              <h6 className="mb-0">{data.stars.fourStar}</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.fourStar, data.total)}
          </div>
        </div>
        <div className="d-flex align-items-center mb-2">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled mr-3 rating-clr"></i>
            <span className="mr-3">
              <h6 className="mb-0">{data.stars.threeStar}</h6>
            </span>
          </div>
          <div className="progress flex-1">
          {this.renderProgressBar(data.stars.threeStar, data.total)}
          </div>
        </div>
        <div className="d-flex align-items-center mb-2">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled mr-3 rating-clr"></i>
            <span className="mr-3">
              <h6 className="mb-0">{data.stars.twoStar}</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.twoStar, data.total)}
          </div>
        </div>
        <div className="d-flex align-items-center mb-2">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled mr-3 rating-clr"></i>
            <span className="mr-3">
              <h6 className="mb-0">{data.stars.oneStar}</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.oneStar, data.total)}
          </div>
        </div>
      </div>
    );
  };

  render() {
    const {data} = this.props;
    return (
      <div>
        <div class="review-summary">
          <span className="user-profile-review-count">{data && data.averageRating}</span>
          <span class="review-star lg d-block">
            <Rating
              className="rating-clr"
              initialRating={data && data.averageRating}
              fullSymbol={<StarIcon />}
              emptySymbol={<StarBorderIcon />}
              placeholderSymbol={<StarBorderIcon />}
              readonly
            />
          </span>
          <div className="rentInner-userTextH4 mt-2 font13">Based on {data && data.count} Reviews</div>
          <hr />

          {this.renderRatingByReviews()}
        </div>
      </div>
    );
  }
}

export default InnerRattingProfile;
