import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Progress } from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import "../../containers/rentInner/rentInner.scss";
import { SvgIcon, Card, Grid } from "@material-ui/core";
import { createReview } from "../../redux/actions/ReviewAction";
import TextArea from "antd/lib/input/TextArea";
import { reviewType } from "../../util/utilFunctions";

class InnerRating extends Component {
  state = {
    visible: false,
    review: {
      rating: 0,
      reviews: ""
    }
  };

  toggleMenu = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };

  countRatingPercentage = (star, total) => {
    return star && (100 * star) / total
  }
  renderProgressBar = (star, total) => {
    const { progressColor } = this.props
    return (
      <Progress
        className={`progress-bar ${progressColor}`}
        symbolClassName="symbol-progress-none"
        percent={this.countRatingPercentage(star, total)}
      />
    )
  }
  renderRatingByReviews = () => {
    const { btnBlue, reviews } = this.props;

    let data = {
      total: reviews.length,
      stars: {
        oneStar: 0,
        twoStar: 0,
        threeStar: 0,
        fourStar: 0,
        fiveStar: 0,
      },
    }

    reviews && reviews.length > 0 && reviews.map(review => {
      switch (review.rating) {
        case 1:
          return data.stars.oneStar += 1
        case 2:
          return data.stars.twoStar += 1

        case 3:
          return data.stars.threeStar += 1

        case 4:
          return data.stars.fourStar += 1

        case 5:
          return data.stars.fiveStar += 1

        default:
          return null
      }
    })




    return (
      <div className="ratingLength width-100">
        <div className="d-flex align-items-center">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled rating-clr mr-3"></i>
            <span className="mr-3">
              <h6 className="mb-0">5</h6>
            </span>
          </div>
          <div className="progress flex-1">

            {this.renderProgressBar(data.stars.fiveStar, data.total)}
          </div>
        </div>

        <div className="d-flex align-items-center">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled rating-clr mr-3"></i>
            <span className="mr-3">
              <h6 className="mb-0">4</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.fourStar, data.total)}
          </div>
        </div>

        <div className="d-flex align-items-center">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled rating-clr mr-3"></i>
            <span className="mr-3">
              <h6 className="mb-0">3</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.threeStar, data.total)}
          </div>
        </div>

        <div className="d-flex align-items-center">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled rating-clr mr-3"></i>
            <span className="mr-3">
              <h6 className="mb-0">2</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.twoStar, data.total)}
          </div>
        </div>

        <div className="d-flex align-items-center">
          <div className="text-grey d-flex align-items-center">
            <i className="adam-star-filled rating-clr mr-3"></i>
            <span className="mr-3">
              <h6 className="mb-0">1</h6>
            </span>
          </div>
          <div className="progress flex-1">
            {this.renderProgressBar(data.stars.oneStar, data.total)}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { iconColor, btnColor, createReview, data, user } = this.props;
    const { review, rating } = this.state;

    return (
      <>
        <div className="mt-4">
          <div className="d-flex justify-content-between">
            {data && <div className="rating-num d-flex flex-column align-items-center width-100">
              <h1>{data.averageRating}</h1>
              <div>
                <Rating
                  className="rating-clr rating-start-icon"
                  initialRating={data.averageRating}
                  fullSymbol={<StarIcon />}
                  emptySymbol={<StarBorderIcon />}
                  placeholderSymbol={<StarBorderIcon />}
                  readonly
                />
              </div>
              <span className="based-on-review">Based on <span className="f-500">{data.count}</span> Reviews</span>
            </div>
            }

            {this.renderRatingByReviews()}

            <div className="d-flex justify-content-around pl-4 width-100">
              {reviewType(user)}
              <div className="ratingStar width-100 d-flex justify-content-around flex-column align-items-center">
                <span className="review-star sm">
                  <Rating
                    className="rating-clr quality-rating-row"
                    initialRating={0}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </span>
                <span className="review-star sm">
                  <Rating
                    className="rating-clr quality-rating-row"
                    initialRating={0}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </span>
                <span className="review-star sm">
                  <Rating
                    className="rating-clr quality-rating-row"
                    initialRating={0}
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </span>
              </div>
            </div>
            <div className="ratingButton width-100 d-flex justify-content-around flex-column align-items-center">
              {this.props.userId && <button
                type="button"
                className={`btn btn-primary w-auto ${btnColor}`}
                onClick={() => this.toggleMenu()}
              >
                Write Reviews
              </button>}
              <span className="rentInner-reviewLink">
                {/* <button
                  type="button"
                  className={`btn btn-link pl-0 pr-0 view-all-review-div ${iconColor}`}
                >
                  View All Reviews
                </button> */}
              </span>
            </div>
          </div>
        </div>
        {this.state.visible && (
          <Formik
            initialValues={{ ...review }}
            onSubmit={values => {
              const { moduleId, user } = this.props;
              values.moduleId = moduleId;
              values.userId = user && user.id;

              if (values.userId && values.moduleId) {
                createReview(values);
                this.toggleMenu()
              }
            }}
            validationSchema={Yup.object().shape({
              rating: Yup.string().required("rating field is required."),
              reviews: Yup.string().required("review field is required.")
            })}
            render={({
              errors,
              status,
              touched,
              setFieldValue,
              values,
              handleSubmit
            }) => (
                <Form>
                  <Card className="review-form">
                    <Grid container className="p-10">
                      <Grid item sm={6}>
                        <div className="ant-form-item-required" title="Rating">
                          Add Review
                      </div>
                        <Rating
                          className="rating-clr"
                          initialRating={0}
                          onClick={value => {
                            setFieldValue("rating", value);
                            this.setState({ rating: value });
                          }}
                          emptySymbol={<StarBorderIcon />}
                          placeholderSymbol={<StarIcon />}
                          fullSymbol={<StarIcon />}
                          placeholderRating={rating}
                        />
                        <ErrorMessage
                          className="invalid-feedback ant-typography-danger"
                          name={`rating`}
                          component="span"
                        />
                      </Grid>
                      <Grid item sm={12} className="gutter-box mt-10">

                        <TextArea
                          name="reviews"
                          className="form-control"
                          placeholder="Review"
                          rows="5"
                          value={values.reviews}
                          onChange={e => setFieldValue("reviews", e.target.value)}
                        />
                        <ErrorMessage
                          className="invalid-feedback ant-typography-danger"
                          name={`reviews`}
                          component="span"
                        />
                      </Grid>
                      <div className="w-100 float-left text-center">
                        <Grid item sm={12} className="mt-10 review-button">
                          <button
                            className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                            type="submit"
                          >
                            Save
                        </button>
                          <button
                            onClick={this.toggleMenu}
                            className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                            type="button"
                          >
                            Cancel
                        </button>
                        </Grid>
                      </div>
                    </Grid>
                  </Card>
                </Form>
              )}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  createSuccess: state.reviewReducer && state.reviewReducer.createSuccess,
  user: state.loginReducer && state.loginReducer.currentUser
});

const mapDispatchToProps = dispatch => ({
  createReview: data => dispatch(createReview(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(InnerRating);
