import React, { Component } from "react";
import {
  Grid,
  CardContent,
  Card,
  CardActions,
  Box,
  Select
} from "@material-ui/core";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import SearchIcon from "@material-ui/icons/Search";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import CarousalFormBoatTitle from "../carouselComponent/carousalFormBoatTitle";
import "../../containers/rent/rent.scss";
import {
  renderRentBoatTypes, getSelectedTrip, getBoatPassengerText
} from "../../helpers/jsxHelper";
import SearchComplete from "../map/SearchComplete";
import UserContext from "../../UserContext";

class RentCard extends Component {

  state = {
    clicks: 0,
    Show: true,
    startDate: "",
    endDate: "",
    focusedInput: "",
    boatRentType: "",
    boatType: "",
    place:""
  };

  static contextType = UserContext; 

  IncrementItem = () => {
    this.setState(prevState => {
      if (prevState.clicks < 200) {
        return {
          clicks: prevState.clicks + 1
        };
      } else {
        return this.state.clicks;
      }
    });
  };

  DecreaseItem = () => {
    this.setState(prevState => {
      if (prevState.clicks > 0) {
        return {
          clicks: prevState.clicks - 1
        };
      } else {
        return this.state.clicks;
      }
    });
  };

  getCountText = (tripList, boatType) => {
    const selectedTrip=getSelectedTrip(tripList,boatType);
    return getBoatPassengerText(selectedTrip);
 }

  handleSearch=()=>{
    const {startDate, endDate, place, boatRentType, boatType, clicks} = this.state;
    const {history} = this.context;
    const sDate= startDate && startDate.toDate().toISOString();
    const eDate=endDate && endDate.toDate().toISOString();
    
    history.push("/search-rent-boats",{country:place.country, city:place.city, tripType:boatRentType, trip:boatType, maximumGuest:clicks, startDate:sDate, endDate:eDate});
  }


  render() {
    const {
      clicks,
      startDate,
      endDate,
      focusedInput,
      boatType,
      boatRentType,
    } = this.state;
    const { tripTypeData, tripList } = this.props;
      
    const selectedTrip = getSelectedTrip(tripList, boatType);
    return (
      <Card className="card mt-0 mb-0 h-100 rent-card">
        <CardContent className="width-100 m-auto margin-padding-rent-card">
          <h4 className="text-left rent-card-title fontWeight-500">Rent Boat</h4>

          <CarousalFormBoatTitle
            data={tripList}
            fetchBoatType={value => this.setState({boatRentType: "",boatType: value})}
          />
          <Grid container >

            <Grid item sm={12} className="mb-15">
              <label className="form-label rent-card-label-text">Where are you looking for?</label>
              <SearchComplete
                placeHolder="Enter Country or City Name"
                className="form-control rent-card-label-text"
                 getPlaceName={place =>{
                  this.setState({ place })
                 }
                } 
              />
            </Grid>

            <Grid item sm={12} className="mb-15">
              <label className="form-label rent-card-label-text rent-card-banner-form-field">Trip Type</label>

              <Select
                fullWidth
                value={boatRentType}
                variant="outlined"

                onChange={e => {
                  this.setState({ boatRentType: e.target.value });
                }}
              >
                {renderRentBoatTypes(tripTypeData,selectedTrip)}
              </Select>
            </Grid>

            <Grid item sm={12} className="pt-30">
              <DateRangePicker
                startDate={startDate ? startDate : ''}
                startDateId="your_unique_start_date_id"
                endDate={endDate ? endDate : ''}
                endDateId="your_unique_end_date_id"
                numberOfMonths={1}
                onDatesChange={({ startDate, endDate }) => {
                    this.setState({ startDate ,endDate});
                  }                  
                }
                focusedInput={focusedInput}
                onFocusChange={focusedInput => {
                  this.setState({ focusedInput });
                }}
              />
            </Grid>

            <Grid item sm={12} className="pt-30">
              <Box
                fontWeight={400}
                className="form-title justify-rent-passenger justify-content-end"
                letterSpacing={0.6}
                fontSize={14}
              >
               
                  <label className="mr-rent-4 form-label mb-0 text-dark">{this.getCountText(tripList,boatType)} </label>
               
                <div className="d-flex align-items-center">
                  <button
                    className="rent-passenger rent-passenger-height"
                    onClick={this.DecreaseItem}
                  >
                    <RemoveIcon className="clr-btn" />
                  </button>
                  <span className="count-passenger"> {clicks} </span>
                  <button
                    className="rent-passenger rent-passenger-height"
                    onClick={this.IncrementItem}
                  >
                    <AddIcon className="clr-btn" />
                  </button>
                </div>
              </Box>
            </Grid>
          </Grid>
          <CardActions className="mt-30 d-flex justify-content-between">
            <a
              className="btn-link-info float-right"
              href="#"
            >
              <u className="rent-notify-color font-12 rent-notify-btn">NOTIFY ME</u>
            </a>
            
            <button
              onClick={this.handleSearch}
              type="button"
              className="btn btn-danger font-weight-600 rent-button-color rent-search-boat-btn rent-card-hovered-button-effect search-button-width"
              type="submit"
            >
              Search Now
              {/*<SearchIcon />*/}
            </button>
          </CardActions>
        </CardContent>
      </Card>
    );
  }
}





export default RentCard
