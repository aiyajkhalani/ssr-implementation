import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { dimension, defaultBoatHomeImage } from "../../util/enums/enums";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { CityWiseComponent } from "../cityWiseComponent/cityWiseComponent";

export default class SellAroundCards extends Component {

  state = {
    imageHeightRatio: dimension.sellAround.height,
    imageWidthRatio: dimension.sellAround.width
  }

  componentDidMount() {
    const imageWidthRatio = getRatio(dimension, "sellAround", "#section-heading")
    const imageHeightRatio = getHeightRatio(dimension, "sellAround", "#section-heading")
    this.setState({ imageWidthRatio, imageHeightRatio })
  }

  render() {
    const { data, getCityWiseBoats, limit } = this.props;
    const { imageWidthRatio, imageHeightRatio } = this.state;

    const result = data && data.map((item, i) => {
      if (item.countryImage == null) {
        item.image = `image/slide-${i + 1}.jpg`
      } else {
        item.image = item.countryImage
      }
      return item
    })

    return (
      <div className="city-from-country">
        <Grid container >
          <div className={result && result.length > 4 ? `sell-justify space-between` : `sell-justify `}>
            {result && result.length > 0 &&
              result.map((value, i) =>
                <div key={value.id}>
                  {limit > i &&
                    <CityWiseComponent
                      value={value}
                      img={encodeURI(value.image || defaultBoatHomeImage)}
                      getCityWiseBoats={() => getCityWiseBoats(value)}
                      city={value.cityName}
                      width={imageWidthRatio}
                      height={imageHeightRatio}
                      minimumPrice={value.minimumPrice}
                    />

                  }
                </div>
              )}
          </div>
        </Grid>
      </div>
    );
  }
}