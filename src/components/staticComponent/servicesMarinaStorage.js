import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import UserContext from "../../UserContext";
import { getWidthRatio, getHeightRatioRespectWidth } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import { ExploreAdamSea } from "./exploreAdamSea";

export default class ServicesMarinaStorage extends Component {
  state = {
    width: dimension.servicesMarinaStorage.width
  };
  static contextType = UserContext;

  componentDidMount() {
    const width = getWidthRatio(
      dimension,
      "servicesMarinaStorage",
      ".section-heading"
    );
    const height = getHeightRatioRespectWidth(
      dimension,
      "servicesMarinaStorage",
      ".section-heading"
    );

    this.setState({ width, height });
  }
  redirectRouteHandler = route => {
    window && window.open(`/${route}`, '_blank')
  };

  render() {
    const { width, height } = this.state;

    return (
      <div>
        <Grid container spacing={3}>
          <ExploreAdamSea
            width={width}
            name="Charter & Rent"
            img="image/rent-link.png"
            redirectRouteHandler={() => this.redirectRouteHandler("rent")}
            height={height}
          />
          <ExploreAdamSea
            width={width}
            name="Service & Maintenance"
            img="image/service-link.png"
            redirectRouteHandler={() => this.redirectRouteHandler("boat-service")}
            height={height}
          />
          <ExploreAdamSea
            width={width}
            name="Marina & Storage"
            img="image/marina-link.jpeg"
            redirectRouteHandler={() => this.redirectRouteHandler("marina-storage")}
            height={height}
          />
          <ExploreAdamSea
            width={width}
            name="Boat Show"
            img="image/boat-show-link.png"
            redirectRouteHandler={() => this.redirectRouteHandler("boat-show")}
            height={height}
          />
        </Grid>
      </div>
    );
  }
}
