import React, { PureComponent } from "react";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import Truncate from "react-truncate";
import { CommonAdamSeaStyle } from "../styleComponent/styleComponent";
import SharePopup from "../share/SharePopup";

import {  viewServiceBoatHandler } from "../../helpers/boatHelper";
import { Grid } from "@material-ui/core";

export class BoatServiceDetail extends PureComponent {

  state = {
    selectedIndex: null,
  }

  handleClick = index => {
    const { selectedIndex } = this.state;
    this.setState({
      selectedIndex: selectedIndex !== index ? index : null
    });
  };

  render() {

    const { selectedIndex } = this.state
    const { index, value, width, height } = this.props
   

    return (
        <div className="boat-box mt-2">
        <div className="boat-box-shadow ">
          <div className="card-action top-service-share-icon">
            <div class="share-icon">
              <div className="heart-button">
                <i className="adam-heart-1"></i>
              </div>
              <SharePopup
                handleClick={() => this.handleClick(index)}
                index={index}
                selectedIndex={selectedIndex}
              />
            </div>
          </div>
          <CommonAdamSeaStyle
            onClick={() => viewServiceBoatHandler(value.id)}
            height={height}
            width={width}
            className="w-100"
            img={
              value.images &&
              value.images.length &&
              encodeURI(value.images[0])
            }
          ></CommonAdamSeaStyle>
          <CommonAdamSeaStyle
            width={width}
            className="w-100"
            onClick={() => viewServiceBoatHandler(value.id)}
          >
            <div className="boat-info">
              <div>
                <h4 className="boat-title mt-3 mb-0 f-16 boat-service-viewed-count dark-silver font-weight-500">
                  <VisibilityOutlinedIcon /> {value.userCount}
                </h4>
              </div>

              <div>
                <h5 className="mt-2 mb-2 team-commitment-text boat-service-viewed-title font-weight-500 boat-rated-service-commitment">
                  <Truncate lines={2} ellipsis={<span>..</span>}>
                    {value.teamCommitment}
                  </Truncate>
                </h5>
              </div>

              <Grid container>
                <Grid item sm={12}>
                  <h4 className="boat-owner mt-0 f-15 boat-service-viewed-desc gray-light font-weight-500">
                    <Truncate lines={2} ellipsis={<span>..</span>}>
                      {value.service}
                    </Truncate>
                  </h4>
                </Grid>
                <Grid container>
                  <Grid item sm={12}>
                    <div className="d-flex mt-1 justify-content-between">
                      <div className="d-flex align-items-center">
                        <img
                          src={
                            value.images &&
                            value.images.length &&
                            value.images[0]
                          }
                          className="view-service-img-salesman"
                          alt=""
                        />

                        <div className="salesman-name f-15 boat-service-viewed-user font-weight-500 dark-silver">
                          By {value.salesManFullName}
                        </div>
                      </div>
                      <div className="boat-service-viewer-section">
                        {value.user && value.user.companyLogo && (
                          <div className="view-service-reviewer-div d-flex">
                            <img
                              src={value.user.companyLogo}
                              className="h-100 width-100"
                              alt=""
                            />
                          </div>
                        )}
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </CommonAdamSeaStyle>
        </div>
      </div>
    );
  }
}