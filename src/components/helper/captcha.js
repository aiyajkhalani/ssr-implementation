import React from 'react';
import ReCAPTCHA from "react-google-recaptcha";

const Captcha = (props) => {

    return (
        <ReCAPTCHA
            sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY}
            onChange={props.onChange}
        />
    )

};

export default Captcha;
