import { useContext, useEffect } from 'react';

import UserContext from '../../UserContext';

const Logout = () => {

    const { logout, history } = useContext(UserContext)

    const isAuthenticated = localStorage.getItem('isAuthenticated')

    const logoutHandler = () => {
        logout()
        return null
    }

    useEffect(() => {
        history && !isAuthenticated && history.push('/login')
    }, [history, isAuthenticated])

    return (
        localStorage.getItem('isAuthenticated') && logoutHandler()
    )
}

export default Logout