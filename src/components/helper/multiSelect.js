import React from "react";
import Select from "react-select";

import "./multiSelect.scss";


class MultiSelect extends React.Component {

  render() {

    const { selectedOption, onChangeValue, options } = this.props

    return (
      <div className="multi-select-div">
        <Select
          isMulti
          value={selectedOption}
          onChange={value => onChangeValue(value || [])}
          options={options}
        ></Select>
      </div>
    );
  }
}

export default MultiSelect;
