import React, { Component } from 'react'
import ReactTable from "react-table";
import "./TableComponent.scss"



class TableComponent extends Component {

    state = {
        data: [
            {
                boatType: "Fishing Boat",
                adId: "Adam Fast Craft 7 (SELLER29PVLTE4)",
                seller: "John Son Inc., Canada",
                requested: "13-02-2020 - 07:00 AM",
                status: "My Boat",
                action: "View"
            },
            {
                boatType: "Fishing Boat",
                adId: "Adam Fast Craft 7 (SELLER29PVLTE4)",
                seller: "John Son Inc., Canada",
                requested: "13-02-2020 - 07:00 AM",
                status: "My Boat",
                action: "View"
            },
            {
                boatType: "Fishing Boat",
                adId: "Adam Fast Craft 7 (SELLER29PVLTE4)",
                seller: "John Son Inc., Canada",
                requested: "13-02-2020 - 07:00 AM",
                status: "My Boat",
                action: "View"
            },
            {
                boatType: "Fishing Boat",
                adId: "Adam Fast Craft 7 (SELLER29PVLTE4)",
                seller: "John Son Inc., Canada",
                requested: "13-02-2020 - 07:00 AM",
                status: "My Boat",
                action: "View"
            }
        ]
    }

    render () {

        const columns = [
            {
                Header: "Boat Type",
                accessor: "boatType"
            },
            {
                Header: "Ad ID",
                accessor: "adId"
            },
            {
                Header: "Seller",
                accessor: "seller"
            },
            {
                Header: "Requested",
                accessor: "requested"
            },
            {
                Header: "Status",
                accessor: "status"
            },
            {
                Header: "Action",
                accessor: "action"
            },
            
        ] 

        return (
            <div className="table-component-dashboard">
            <ReactTable
            columns={columns}
            data={this.state.data}
            
            defaultFilterMethod={(filter, row) =>
              String(row[filter.id]).includes(filter.value)
            }
            className="-striped -highlight"
          />
          </div>
        )
    }
}

export default TableComponent