import styled from "styled-components";

const ServicesStyle = styled.div`
  height: ${props => props.height}px;
  width: ${props => props.width}px;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px 0 0 10px;
`;

const ServiceTextStyle = styled.div`
  width: calc(100% - ${props => props.actualWidth}px);
`;

const RentCardStyle = styled.div`
  width: ${props => props.width}px
`;

const RentBannerImage = styled.div`
  width: calc(100% - ${props => props.width}px);
`;

const RentHomeCardBanner = styled.div`
  height: calc(100% - ${props => props.height}px)`

/** Marina */
const RecommendedStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 10px 10px;
`;
/** Marina */
const MostPopularContentStyle = styled.div`
 width: ${props => props.bgWidth}px;
`

const LogInSignupBanner = styled.div`
  background-image: url(${props => props.img});
  background-size: cover;
  background-repeat: no-repeat;
`

const MoreServiceStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;
const MoreServiceTextStyle = styled.div`
  width: ${props => props.bgWidth}px;
`;
const MarinaVideoStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const RentVideoStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width:100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const BoatShowVideoStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: ${props => props.bgWidth}px;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const BoatServiceVideoStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const HomPageVideoStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const SellAroundStyle = styled.div`
  height: ${props => props.height}px;
  width: ${props => props.width}px;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  position: relative;
  background-repeat: no-repeat;
  border-radius: 10px;
`;
const ManufacturerStyle = styled.div`
  height: ${props => props.height}px;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
`
const CommonAdamSeaStyle = styled.div`
    height: ${props => props.height}px;
    width: ${props => props.width}px;
    background-image: url('${props => props.img}');
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    border-radius:3px;
`
const MostPopularStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 10px 10px;
`;
const AuctionRoomStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  justify-content:center;
  align-items:center;
  display:flex;
  border-radius: 10px 10px 0 0;
  padding: 0 10px;
  position:relative;
`;

const HomeRecommendedContentStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 10px 10px;
`;
const HomeMostPopularContentStyle = styled.div`
  width: ${props => props.bgWidth}px;
`;
const CommonBannerStyle = styled.div`
  height: 100%;
  width:100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;
const ArrowCarousel = styled.div`
  top: ${ props => props.top}px;
`;

const BoatMediaMainStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width: ${props => props.bgWidth}px;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 10px 10px;
`;
const BoatMediaWidthStyle = styled.div`
  width: ${props => props.bgWidth}px;
`;
const ShowAllGridStyle = styled.div`
  height: ${props => props.bgHeight}px;
  width:100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 10px 10px;
`;
const ServiceCardTextStyle = styled.div`
  width: calc(100% - ${props => props.width}px);
`
const BoatInnerStyle = styled.div`
  height: 100%;
  width:100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  position: relative;
  border-radius: 6px 6px;
`;
const FirstBannerStyle = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 6px 6px 6px 6px;
  height: 100%;
`;
const SecondBannerStyle = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 6px 6px 6px 6px;
  height: 100%;
`;
const ThirdBannerStyle = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 6px 6px 6px 6px;
  height: 100%;
`;
const BoatServiceCarouselStyle = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 6px 6px 6px 6px;
`;
const LeftArrowStyle = styled.div`
    background-image: url(${props => props.img});
    background-size: 100%;
    background-repeat:no-repeat;
    background-size: 100%;
    height: 11px;
    width: 11px;
    background-position: center;
`
const RightArrowStyle = styled.div`
    background-image: url(${props => props.img});
    background-repeat:no-repeat;
    background-size: 100%;
    height: 11px;
    width: 11px;
    background-position: center;
`
const BoatShowBannerFormStyle = styled.div`
    height: 100%;
    width: ${props => props.bgWidth}px;
`

const BoatServiceBannerFormStyle = styled.div`
    height: 100%;
    width: ${props => props.bgWidth}px;
`
const BannerHeight = styled.div`
  height: calc(100vh - ${props => props.height}px);
`;
const SectionHeadingShowStyle = styled.div`
  position:fixed;
  width:100%;
  z-index: 999;
  top: ${props => props.top}px;
`;
const SectionWithShowMapStyle = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 4px;
  height: 100%;
`;
const BoatInnerGallery = styled.div`
  width: 100%;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  height: 100%;`
const MapPlaceInfoStyle = styled.div`
height: ${props => props.bgHeight}px;
width: ${props => props.bgWidth}px;
`;
const MapPlaceImgStyle = styled.div`
height: ${props => props.bgHeight}px;
width: 100%;
`;
const InnerBannerLikesDiv = styled.div`
bottom: ${props => props.bottom}px !important;
`;
const BoatInnerRelatedVideoStyle = styled.div`
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 10px;
  display:flex;
  justify-content: center;
  align-items: center;
`;
const ViewBoatServiceStyle = styled.div`
margin-top: ${props => props.marginTop}px;
width: 100%;
`;
const MapShowStyle = styled.div`
top: ${props => props.top}px !important;
`;
const StepperBoatInfoStyle = styled.div`
height: ${props => props.bgHeight}px;
width: 100%;
background-image: url(${props => props.img});
background-position: center;
background-size: cover;
background-repeat: no-repeat;
position: relative;
border-radius: 10px 10px;
`;
export {
  ServicesStyle,
  ServiceTextStyle,
  RecommendedStyle,
  MoreServiceStyle,
  MarinaVideoStyle,
  MoreServiceTextStyle,
  SellAroundStyle,
  ManufacturerStyle,
  CommonAdamSeaStyle,
  MostPopularStyle,
  MostPopularContentStyle,
  AuctionRoomStyle,
  HomeRecommendedContentStyle,
  HomeMostPopularContentStyle,
  HomPageVideoStyle,
  CommonBannerStyle,
  ArrowCarousel,
  BoatMediaMainStyle,
  BoatMediaWidthStyle,
  RentVideoStyle,
  BoatShowVideoStyle,
  BoatServiceVideoStyle,
  ShowAllGridStyle,
  ServiceCardTextStyle,
  BoatInnerStyle,
  FirstBannerStyle,
  SecondBannerStyle,
  ThirdBannerStyle,
  BoatServiceCarouselStyle,
  LeftArrowStyle,
  RightArrowStyle,
  BoatShowBannerFormStyle,
  RentCardStyle,
  RentBannerImage,
  BoatServiceBannerFormStyle,
  BannerHeight,
  RentHomeCardBanner,
  SectionHeadingShowStyle,
  SectionWithShowMapStyle,
  BoatInnerGallery,
  MapPlaceInfoStyle,
  MapPlaceImgStyle,
  InnerBannerLikesDiv,
  BoatInnerRelatedVideoStyle,
  ViewBoatServiceStyle,
  MapShowStyle,
  StepperBoatInfoStyle,
  LogInSignupBanner
};
