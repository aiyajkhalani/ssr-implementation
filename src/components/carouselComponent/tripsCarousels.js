import React from "react";
import { isBrowser, isTablet } from "react-device-detect";

import { dimensionAspectRatio } from "../../util/utilFunctions";
import { carouselArrow } from "../../util/enums/enums";
import { CarouselWrapper } from "./carouselWrapper";
import { ShowAllLink } from "../helper/showAllLink";
import { RentSharedTrip } from "../gridComponents/rentSharedTrip";

// style
import "../../components/home/home.scss";
import "../../styles/common.scss";

export class TripCarousels extends React.Component {

  state = {
    height: 340
  }

  componentDidMount() {
    const height = this.getCarousalHeight()
    if (height) {
      this.setState({ height })
    }
  }

  getCarousalHeight = () => {
    const { items } = this.props
    const dividedBy = isBrowser ? items.isBrowser : isTablet ? items.isTablet : items.isMobile
    const width = document.querySelector(".section-heading")
    const actcualWidth = width && (width.offsetWidth / dividedBy)
    const aspectRatio = dimensionAspectRatio(500, 340)
    const newHeight = (actcualWidth / aspectRatio) - 5
    return newHeight
  }

  createChildren = () => {
    const { carouselData, itemsLength, dimension } = this.props

    return carouselData && carouselData.length
      && carouselData.map((value, index) => {
        return (itemsLength > index &&
          <RentSharedTrip
            value={value}
            sm={4}
            xs={12}
            index={index}
            dimension={dimension}
            isCustomWidth
          />
        )
      })

  };

  changeActiveItem = activeItemIndex => this.setState({ activeItemIndex });

  render() {

    const { height } = this.state;
    const { items, carouselData, itemsLength, totalLength, showType, showAllText } = this.props;
    const top = ((height / 2) - carouselArrow.ratio)

    return (
      <>
        <CarouselWrapper items={items} top={top}>
          {this.createChildren()}
        </CarouselWrapper>

        <ShowAllLink data={carouselData} itemsLength={itemsLength} totalLength={totalLength} showAllText={showAllText} url={`/show-all-rent/${showType}`} />
      </>
    );
  }
}

TripCarousels.defaultProps = {
  showType: ""
};