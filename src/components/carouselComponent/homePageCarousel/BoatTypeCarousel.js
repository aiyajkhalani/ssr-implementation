import React, { Fragment } from "react";
import { isBrowser, isTablet } from "react-device-detect";

import UserContext from "../../../UserContext";
// style
import "../../home/home.scss";
import "../../../styles/common.scss";
import { dimensionAspectRatio } from "../../../util/utilFunctions";
import { carouselArrow } from "../../../util/enums/enums";
import { CarouselWrapper } from "../carouselWrapper";
import { ShowAllLink } from "../../helper/showAllLink";
import { BoatDetail } from "../../home/singleComponents/boatDetail";
export class BoatTypeCarousel extends React.Component {

  static contextType = UserContext;

  //Get new height:
  getNewHeight = () => {
    const { items, isMustBuy } = this.props;
    const dividedBy =
      isBrowser
        ? items.isBrowser
        : isTablet
          ? items.isTablet
          : items.isMobile;
    const width = document.querySelector("#section-heading");
    const actcualWidth = width && width.offsetWidth / dividedBy - 16;
    /* dimensionAspectRatio(width, height) */
    const aspectRatio = isMustBuy
      ? dimensionAspectRatio(275, 350)
      : dimensionAspectRatio(295, 230);
    const newHeight = actcualWidth / aspectRatio - 5;

    return newHeight;
  };

  createChildren = () => {
    const { carouselData, itemsLength, isTopLabel, isBottomRating } = this.props;
    const { history } = this.context;
    const height = this.getNewHeight();

    return carouselData && carouselData.length
      ? carouselData.map((value, index) => {
        return (
          <Fragment key={value.id}>
            {itemsLength > index && (
              <BoatDetail
                value={value}
                isTopLabel={isTopLabel}
                isBottomRating={isBottomRating}
                height={height}
                index={index}
                history={history}
                isCarousel
              />
            )}
          </Fragment>
        );
      })
      : "There is no boats found.";
  };

  render() {
    const { items, itemsLength, showType, carouselData, showAllText } = this.props;
    const height = this.getNewHeight();
    const top = height / 2 - carouselArrow.ratio;

    return (
      <>

        <CarouselWrapper items={items} top={top}>
          {this.createChildren()}
        </CarouselWrapper>

        <ShowAllLink data={carouselData} itemsLength={itemsLength} showAllText={showAllText} url={`/show-all/${showType}`} />

      </>
    );
  }
}
BoatTypeCarousel.defaultProps = {
  showType: "",
  itemsLength: 5,
  isMustBuy: false
};
