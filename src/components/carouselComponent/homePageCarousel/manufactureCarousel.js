import React from "react";
import { Grid } from "@material-ui/core";
import { isBrowser, isTablet } from "react-device-detect";

import { dimensionAspectRatio } from "../../../util/utilFunctions";
import { CarouselWrapper } from "../carouselWrapper";
import { MarketServiceDetail } from "../../marketService/marketServiceDetail";

export default class ManufactureCarousel extends React.Component {

	//Get new height:
	getNewHeight = () => {

		const { items } = this.props
		const dividedBy = isBrowser ? items.isBrowser : isTablet ? items.isTablet : items.isMobile
		const width = document.querySelector("#section-heading")
		const actualWidth = width && (width.offsetWidth / dividedBy)
		/* dimensionAspectRatio(width, height) */
		const aspectRatio = dimensionAspectRatio(295, 230)
		const newHeight = (actualWidth / aspectRatio) - 5

		return newHeight
	}

	createChildren = () => {
		const { carouselData, getCategoryWiseBoats } = this.props
		const height = this.getNewHeight()
		return (
			carouselData &&
				carouselData.length > 0 &&
				carouselData.map(item => {
					return (
						<MarketServiceDetail
							value={item}
							getService={getCategoryWiseBoats}
							className="manufacturer-carousal cursor-pointer"
							height={height}
						/>
					);
				}) 
		);
	};

	render() {
		const { items, btnColor } = this.props;
		return (
			<>
				<CarouselWrapper items={items} btnColor={btnColor}>
					{this.createChildren()}
				</CarouselWrapper>
			</>
		);
	}
}
