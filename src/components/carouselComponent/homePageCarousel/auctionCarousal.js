import React, { Fragment } from "react";
import { Grid } from "@material-ui/core";

import UserContext from "../../../UserContext";
import { dimension, carouselArrow } from "../../../util/enums/enums";
import { getRatio, getHeightRatio } from "../../../helpers/ratio";
import { ShowAllLink } from "../../helper/showAllLink";
import { CarouselWrapper } from "../carouselWrapper";
import { AuctionDetail } from "../../home/singleComponents/auctionDetail";

import "../../home/home.scss";

export class AuctionCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTrue: false,
      height: 380,
      width: 280
    };
  }

  static contextType = UserContext;

  componentDidMount() {
    const width = getRatio(dimension, "auctionRoom", ".section-heading");
    const height = getHeightRatio(dimension, "auctionRoom", ".section-heading");
    this.setState({
      width,
      height
    });
  }

  createChildren = () => {
    const { width, height } = this.state;
    const { carouselData } = this.props;
    const { history } = this.context;

    return carouselData && carouselData.length > 0 ? (
      carouselData.map((value) => (
        <Fragment key={value.id}>
          <AuctionDetail value={value} history={history} height={height} width={width} />
        </Fragment>
      ))
    ) : (
        <Grid item xs={12}>
          No Record Found
      </Grid>
      );
  };

  render() {
    const { height } = this.state;
    const { items, itemsLength, showAllText, carouselData } = this.props;

    const top = ((height / 2) - carouselArrow.ratio)
    return (
      <>
        <CarouselWrapper items={items} top={top}>
          {this.createChildren()}
        </CarouselWrapper>

        <ShowAllLink data={carouselData} itemsLength={itemsLength} showAllText={showAllText} url={`/show-all-auction-rooms`} />
      </>
    );
  }
}
