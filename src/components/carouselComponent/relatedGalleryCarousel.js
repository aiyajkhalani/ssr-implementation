import React, { Fragment } from "react";
import { dimensionAspectRatio } from "../../util/utilFunctions";
import { UserConsumer } from "../../UserContext";
import { CarouselWrapper } from "./carouselWrapper";
import { ShowAllLink } from "../helper/showAllLink";
import { BoatServiceDetail } from "../boatService/boatServiceDetail";

import "../home/home.scss";

export class GalleryCarousal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselData:[]
    };
  }

  static contextType = UserConsumer;

  getNewWidth = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5 - 15;
    const aspectRatio = dimensionAspectRatio(275, 350);
    const newWidth = actualWidth / aspectRatio - 5;

    return newWidth;
  };

  getNewHeight = () => {
    const width = document.querySelector(".section-heading");
    const actualWidth = width && width.offsetWidth / 5;
    const aspectRatio = dimensionAspectRatio(275, 350);
    const newHeight = actualWidth / aspectRatio - 5;

    return newHeight;
  };


  

  componentDidMount() {
    
    // NEED TO CHANGE [Discuss For this @miraj]
    this.setState({
      carouselData: this.props.carouselData
    });
  }

  createChildren = () => {
    const height = this.getNewHeight();
    const width = this.getNewWidth();
    const { carouselData } = this.state
    return (
      carouselData &&
      carouselData.map((value, index) => (
        <Fragment key={value.id}>
          <BoatServiceDetail
            value={value}
            height={height}
            width={width}
            index={index}
          />
        </Fragment>
      ))
    );
  };


  render() {
    const { carouselData } = this.state;
    const { items, showAllText } = this.props;
    const top = this.getNewHeight() / 2;
    return (
      <>

        <CarouselWrapper items={items} top={top}>
          {this.createChildren()}
        </CarouselWrapper>

        <ShowAllLink data={carouselData} itemsLength={5} showAllText={showAllText} url="/show-all-boat-service-gallery" />
      </>
    );
  }
}