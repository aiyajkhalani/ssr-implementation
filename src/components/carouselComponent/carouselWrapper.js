import React, { useState } from 'react'
import ItemsCarousel from "react-items-carousel";
import { isBrowser, isTablet } from "react-device-detect";

import { RightArrowStyle, LeftArrowStyle, ArrowCarousel } from "../styleComponent/styleComponent";
import { arrows } from "../../util/enums/enums";

export const CarouselWrapper = ({ children, items, btnColor, top }) => {

    const [activeItemIndex, setActiveItemIndex] = useState(0)

    return (
        <ItemsCarousel
            // Placeholder configurations
            enablePlaceholder
            numberOfPlaceholderItems={isBrowser ? items.isBrowser : isTablet ? items.isTablet : items.isMobile}
            minimumPlaceholderTime={1000}
            className="carousel-arrow"
            // Carousel configurations
            numberOfCards={isBrowser ? items.isBrowser : isTablet ? items.isTablet : items.isMobile}
            gutter={16}
            showSlither={false}
            firstAndLastGutter={false}
            freeScrolling={false}
            // Active item configurations
            requestToChangeActive={(activeItemIndex) => setActiveItemIndex(activeItemIndex)}
            activeItemIndex={activeItemIndex}
            activePosition={'center'}
            chevronWidth={24}
            rightChevron={
                <ArrowCarousel top={top} className={`LeftRight-arrow right-arrow ${btnColor}`}>
                    <RightArrowStyle top={top} img={arrows.right} className="carousel-right-arrow" />
                </ArrowCarousel>
            }
            leftChevron={
                <ArrowCarousel top={top} className={`LeftRight-arrow left-arrow ${btnColor}`}>
                    <LeftArrowStyle top={top} img={arrows.left} className="carousel-left-arrow" />
                </ArrowCarousel>
            }
            outsideChevron={false}
        >
            {children}
        </ItemsCarousel>
    )
}

CarouselWrapper.defaultProps = {
    customWidthItem: 1,
    btnColor: "",
}