import React from "react";

import {  carouselArrow, dimension } from "../../util/enums/enums";
import { getRatio } from "../../helpers/ratio";
import { MarketServiceDetail } from "../marketService/marketServiceDetail";
import { CarouselWrapper } from "./carouselWrapper";

import "../../components/home/home.scss";
import "../../styles/common.scss";
import "../../containers/rent/rentResponsive.scss"

export default class ExploreAdamSeaIndia extends React.Component {

	state = {
		height: 160,
		width: 160
	}

	componentDidMount() {
		if (window.innerWidth < 1200) {
			this.setState({ customWidth: true });
		}
		const imageRatio = getRatio(dimension, 'yachtServices', ".section-heading");

		imageRatio && this.setState({ height: imageRatio, width: imageRatio })
	}




	createChildren = () => {
		const { carouselData, getCategoryWiseBoats } = this.props
		const { height } = this.state
		return (
			carouselData &&
			carouselData.length > 0 &&
			carouselData.map(value => {
				return (
					<MarketServiceDetail
						value={value}
						getService={getCategoryWiseBoats}
						className="rent-market-type border-explore-adamsea"
						height={height}
					/>
				);
			})
		);
	};



	render() {
		const {  height } = this.state;
		const { items } = this.props;
		const top = ((height / 2) - carouselArrow.ratio)
		return (
			<>
				<CarouselWrapper items={items} top={top}>
					{this.createChildren()}
				</CarouselWrapper>
			</>
		);
	}
}
