import React from 'react';
import ItemsCarousel from 'react-items-carousel';
import {
    isBrowser,
    isTablet
} from "react-device-detect";
import uuid from 'uuid/v4';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import ArrowForward from '@material-ui/icons/ArrowForward';
import ArrowBack from '@material-ui/icons/ArrowBack';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { Menu, MenuItem, Grid } from '@material-ui/core';

import { defaultLayout, arrows } from "../../util/enums/enums";

import "../home/home.scss";
import { RightArrowStyle, LeftArrowStyle } from '../styleComponent/styleComponent';

export class ImageCarousal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            children: [],
            activeItemIndex: 0,
            anchorEl: null,
            isTrue: false
        }
    }

    handleClick = index => {
        const newCarouselData = this.state.carouselData.map((carousel, carouselIndex) => {
            if (carouselIndex === index) {
                carousel.isOpen = true;
                return carousel;
            } else {
                return carousel;
            }
        })
        this.setState({
            carouselData: newCarouselData
        })
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    componentDidMount() {
        this.setState({
            children: [],
            activeItemIndex: 0,
            carouselData: []
        });


    }



    createChildren = () => {

        const { carouselData } = this.props

        return (
            carouselData && carouselData.map((i, index) => (

                <div key={uuid()} className="boat-box">
                    <div className="boat-box-shadow " >
                        <div className="boat-img-box br-10">
                            <img src={i}
                                className="boat-img carousel-img-fix"
                                alt={defaultLayout}
                            />
                        </div>
                    </div>
                </div>
            )
            )
        )
    };

    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

    render() {
        const {
            activeItemIndex,
            children,
        } = this.state;
        const { items, carouselData } = this.props

        return (
            <>
                <ItemsCarousel
                    enablePlaceholder
                    numberOfPlaceholderItems={isBrowser ? 1 : ((isTablet) ? 3 : 1)}
                    minimumPlaceholderTime={1000}

                    className="carousel-arrow"
                    // numberOfCards={isBrowser ? items.isBrowser : ((isTablet) ? items.isTablet : items.isMobile)}
                    numberOfCards={1}
                    gutter={12}
                    showSlither={false}
                    firstAndLastGutter={false}
                    freeScrolling={false}

                    requestToChangeActive={this.changeActiveItem}
                    activeItemIndex={activeItemIndex}
                    activePosition={'center'}

                    chevronWidth={24}
                    rightChevron={<div className="image-carousal-arrow ">
                        <RightArrowStyle img={arrows.right} className="carousel-right-arrow" />
                    </div>}
                    leftChevron={<div className="image-carousal-arrow left-arr">
                        <LeftArrowStyle img={arrows.left} className="carousel-left-arrow" />

                    </div>}
                    outsideChevron={false}
                >
                    {this.createChildren()}
                </ItemsCarousel>
                {/* {carouselData.length>4 && <div>
                    <h2>
                        <Link className="show-link mb-50">Show all 
                                <KeyboardArrowRight />
                        </Link>
                    </h2>
                </div>} */}

            </>
        );
    }
} 