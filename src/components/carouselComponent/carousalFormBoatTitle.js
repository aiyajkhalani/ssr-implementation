import React, { Component, useState } from "react";
import ItemsCarousel from "react-items-carousel";
import uuid from "uuid/v4";
import IconButton from "@material-ui/core/IconButton";
import ArrowForward from "@material-ui/icons/ArrowForward";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Button from "@material-ui/core/Button";

import { lowerHypenCase, snakeCase } from "../../helpers/string";

import "../home/boatTypeCarousal.scss";
import { icons, arrows } from "../../util/enums/enums";
import { renderMenuItems } from "../../helpers/jsxHelper";
import { Select, MenuItem } from "@material-ui/core";
import { RightArrowStyle, LeftArrowStyle } from "../styleComponent/styleComponent";

class CarousalFormBoatTitle extends Component {
  state = {
    activeItemIndex: 0,
    selectedBoatId: "",
  };

  componentDidMount() {
    const { fetchBoatType } = this.props;

    fetchBoatType("5da838c0131dda1ab49a1583")
    this.setState({
      activeItemIndex: 0, selectedBoatId: "5da838c0131dda1ab49a1583",
    });
  }

  selectedBoatHandler = value => {
    const { fetchBoatType } = this.props;
    let { selectedBoatId } = this.state;

    selectedBoatId = selectedBoatId !== value ? value : ''

    fetchBoatType(selectedBoatId)
    this.setState({ selectedBoatId });
  };

  changeActiveItem = activeItemIndex => this.setState({ activeItemIndex });

  renderItems = data => {
    const { selectedBoatId } = this.state

    return (
      data.map(value => {

        const { lookUp } = value;
        return (
          <div className="boatTitleButton-bg" key={uuid()}>
            <div
              className={
                lookUp && selectedBoatId && selectedBoatId === lookUp.id
                  ? "boatTitleBg-onClick"
                  : ""
              }
            >
              <div className="searchBoat-position">
                <Button
                  className={
                    lookUp && selectedBoatId && selectedBoatId === lookUp.id
                      ? "boatTitle-onChange"
                      : ""
                  }
                  onClick={() => this.selectedBoatHandler(lookUp && lookUp.id)}
                >
                  <div className="boatTitlePosition" key={uuid()}>
                    <div
                      className={
                        this.props.rent
                          ? "boatTitle-iconSize rent-icon-color"
                          : "boatTitle-iconSize "
                      }
                    >
                    
                      <img src={lookUp && icons[snakeCase(lookUp.alias)]} className="rent-card-icons"/>
                    </div>
                    <span className="boatTitle-Size">
                      {(lookUp && lookUp.alias) || value.alias}
                    </span>
                  </div>
                </Button>
              </div>
            </div>
          </div>
        );
      })
    )
  };

  render() {
    const { activeItemIndex } = this.state;
    const { data } = this.props;

    return (
      <>
        <div className="boatTitleArrow-margin">
          <ItemsCarousel
            enablePlaceholder
            numberOfPlaceholderItems={3}
            minimumPlaceholderTime={1000}
            className="carousel-arrow"
            numberOfCards={3}
            gutter={12}
            showSlither={false}
            firstAndLastGutter={false}
            freeScrolling={false}
            requestToChangeActive={this.changeActiveItem}
            activeItemIndex={activeItemIndex}
            activePosition="center"
            chevronWidth={24}
            rightChevron={
              <div className="Form-arrow right-formArrow">
                <RightArrowStyle img={arrows.right} className="carousel-right-arrow" />
              </div>
            }
            leftChevron={
              <div className="Form-arrow left-formArrow">
                <LeftArrowStyle img={arrows.left} className="carousel-left-arrow" />
              </div>
            }
            outsideChevron={false}
          >
            {data && data.length > 0 && this.renderItems(data)}
          </ItemsCarousel>
        </div>
      </>
    );
  }
}

export default CarousalFormBoatTitle;
