import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import { priceFormat } from "../../util/utilFunctions";
import { Grid } from "@material-ui/core";

export const OtherBoatInformation = props => {
  return (
    <>
      <div className="d-flex stepper-info-boat-section p-4">
        <Grid container>
          <Grid item sm={12} className="border-bottom-gray">
            <div className="d-flex mb-4 justify-content-between w-100 align-items-center">
              <div className="d-flex flex-column ">
                <span className="stepper-boat-info-id">
                  Ad ID: SELLER961nWR
                </span>
                <span className="stepper-boat-info-name">
                  Boat Name Goes Here
                </span>
              </div>
              <div>
                <img
                  height="100px"
                  width="100px"
                  src={require("../../assets/images/boatInner/Boat-img-3.jpg")}
                />
              </div>
            </div>
          </Grid>
          <hr />
          <Grid item sm={12} className="border-bottom-gray">
            <div className="mt-4 mb-4 d-flex flex-column">
              <span className="stepper-div-text">Friday, Feb 14</span>
              <span className="stepper-div-text">10:00 AM - 05:00 PM</span>
            </div>
          </Grid>
          <Grid item sm={12} className="border-bottom-gray">
            <div className="mt-4 mb-4 d-flex justify-content-between w-100 align-items-center">
              <span className="stepper-div-text">$ 300.15 x 6 Passengers</span>
              <span>$ 1800.90</span>
            </div>
          </Grid>
          <Grid item sm={12}>
            <div className="mt-4 mb-4 d-flex justify-content-between w-100 align-items-center">
              <span className="font-20 font-weight-600">Total</span>
              <span className="font-20 font-weight-600">$ 1800.90</span>
            </div>
          </Grid>
          
        </Grid>
      </div>
      <Grid container className="mt-4">
      <Grid item sm={12}>
            <div className="mb-2 review-div-head-text">
             Cancellation Policy
            </div>
            <div className="review-div-text">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </div>
          </Grid>
      </Grid>
    </>
  );
};
