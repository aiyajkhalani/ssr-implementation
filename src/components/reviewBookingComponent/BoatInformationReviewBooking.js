import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import { priceFormat } from "../../util/utilFunctions";
import { Grid } from "@material-ui/core";

export const BoatInformationReviewBooking = props => {

    return (
        <>
                <div className="d-flex stepper-info-boat-section">
                    <div>

                        <div className="stepper-boat-info-img">
                            <img
                                src={require('../../assets/images/boatInner/Boat-img-3.jpg')}
                                className="h-100 width-100"
                                alt="Boat"
                            />
                        </div>
                    </div>
                    <div className="width-100 p-5 section-stepper-info-boat-div">
                        <div className="d-flex justify-content-between">
                            <div className="d-flex flex-column">
                                <span className="stepper-boat-info-id">Ad ID: SELLER961nWR</span>

                                <span className="stepper-boat-info-name">Boat Name Goes Here</span>
                               <span className="stepper-boat-info-final-price">
                                    Final Price: ${priceFormat(9787)}
                                </span>
                                <span className="stepper-boat-info-price">
                                    Price: ${priceFormat(567)}
                                </span>
                            </div>
                           
                        </div>
                        <div>

                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Year Built</span>
                                        <span> 2004</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Parking</span>
                                        <span>Available</span>
                                    </div>
                                </Col>
                            </Row>


                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Trailer</span>
                                        <span>No</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Status</span>
                                        <span> Used</span>
                                    </div>
                                </Col>
                            </Row>


                            <Row className="mt-3">
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Boat Type</span>
                                        <span> Motor Yacht</span>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className="d-flex flex-column">
                                        <span className="stepper-div-text">Engine Manufacturer</span>
                                        <span>A suscipit commodo i</span>
                                    </div>
                                </Col>
                            </Row>


                        </div>
                    </div>
                </div>
        </>
    );
};
