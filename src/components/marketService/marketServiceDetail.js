import React from "react";
import { ManufacturerStyle } from "../styleComponent/styleComponent";

export const MarketServiceDetail = React.memo(({ value, height, getService, className }) => {
    return (
        <div key={value.id} className={className}>
            <ManufacturerStyle 
                height={height} 
                className="market-type-image"
                img={encodeURI(value.icon)} 
                onClick={() => getService(value)} 
            />
            <div className="boat-name">
                {value.name}
            </div>
        </div>
    )
})