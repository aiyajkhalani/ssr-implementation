import React from 'react';
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import { Field } from '../ws/Field';
import { requireMessage } from '../../helpers/string';


export const BoatOwnerProfile = ({ initValue, updateUser, cancelHandler, userType }) => {

    return (
        <Formik
            initialValues={{ ...initValue }}
            validationSchema={Yup.object().shape({

                firstName: Yup.string().required(requireMessage("First Name")),
                middleName: Yup.string().required(requireMessage("Middle Name")),
                lastName: Yup.string().required(requireMessage("Last Name")),
                email: Yup.string().required(requireMessage("Email"))
                    .email("Email is invalid"),
                mobileNumber: Yup.string().required(requireMessage("Mobile Number")),

                address1: Yup.string().required(requireMessage("Address")),
                street: Yup.string().required(requireMessage("Street")),
                city: Yup.string().required(requireMessage("City or Town")),
                state: Yup.string().required(requireMessage("State")),
                image: Yup.string().required(requireMessage("Profile Photo")),
                accountNumber: Yup.number(),
            })}
            onSubmit={values => {
                values.userType = userType
                updateUser(values);
            }}
            render={({ values, setFieldValue, handleSubmit }) =>
                (
                    <Container fluid>
                        <Form>
                            <Row>
                                <Col>
                                    <Card className="card-parent card-content p-4">
                                        <Card.Title className="ml-0 mt-0">
                                            Contact Info
                                        </Card.Title>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="First Name"
                                                        id={"firstName"}
                                                        name={"firstName"}
                                                        type="text"
                                                        value={values.firstName}
                                                        onChangeText={e => {
                                                            setFieldValue("firstName", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="firstName"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Middle Name"
                                                        id={"middleName"}
                                                        name={"middleName"}
                                                        type="text"
                                                        value={values.middleName}
                                                        onChangeText={e => {
                                                            setFieldValue("middleName", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="middleName"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Last Name"
                                                        id={"lastName"}
                                                        name={"lastName"}
                                                        value={values.lastName}
                                                        type="text"
                                                        onChangeText={e => {
                                                            setFieldValue("lastName", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="lastName"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Country"
                                                        id={"country"}
                                                        name={"country"}
                                                        type="text"
                                                        value={values.country}
                                                        onChangeText={e => {
                                                            setFieldValue("country", e.target.value);
                                                        }}
                                                        disabled
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Profile Picture"
                                                        id="image"
                                                        name="image"
                                                        type="single-image"
                                                        value={values.image}
                                                        onChangeText={setFieldValue}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="image"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col>
                                    <Card className="card-parent card-content p-4">
                                        <Card.Title className="ml-0 mt-0">Billing Address</Card.Title>

                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Address 1"
                                                        id={"address1"}
                                                        name={"address1"}
                                                        type="text"
                                                        value={values.address1}
                                                        onChangeText={e => {
                                                            setFieldValue("address1", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="address1"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Address 2"
                                                        id={"address2"}
                                                        name={"address2"}
                                                        type="text"
                                                        value={values.address2}
                                                        onChangeText={e => {
                                                            setFieldValue("address2", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Street"
                                                        id={"street"}
                                                        name={"street"}
                                                        type="text"
                                                        value={values.street}
                                                        onChangeText={e => {
                                                            setFieldValue("street", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="street"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="City or Town"
                                                        id={"city"}
                                                        name={"city"}
                                                        type="text"
                                                        value={values.city}
                                                        onChangeText={e => {
                                                            setFieldValue("city", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="city"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Select State"
                                                        id={"state"}
                                                        name={"state"}
                                                        type="text"
                                                        value={values.state}
                                                        onChangeText={e => {
                                                            setFieldValue("state", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="state"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Zip or Postal Code"
                                                        id={"zip"}
                                                        name={"zip"}
                                                        type="text"
                                                        value={values.zip}
                                                        onChangeText={e => {
                                                            setFieldValue("zip", e.target.value);
                                                        }}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="zip"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="P.O. Box"
                                                        id={"postBox"}
                                                        name={"postBox"}
                                                        type="text"
                                                        value={values.postBox}
                                                        onChangeText={e => {
                                                            setFieldValue("postBox", e.target.value);
                                                        }}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="postBox"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Card className="card-parent card-content p-4">
                                        <Card.Title className="ml-0 mt-0">
                                            Bank Information
                                        </Card.Title>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Holder Name"
                                                        id={"holderName"}
                                                        name={"holderName"}
                                                        type="text"
                                                        value={values.holderName}
                                                        onChangeText={e => {
                                                            setFieldValue("holderName", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Branch Name"
                                                        id={"branchName"}
                                                        name={"branchName"}
                                                        type="text"
                                                        value={values.branchName}
                                                        onChangeText={e => {
                                                            setFieldValue("branchName", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Branch Address"
                                                        id={"branchAddress"}
                                                        name={"branchAddress"}
                                                        type="text"
                                                        onChangeText={e => {
                                                            setFieldValue("branchAddress", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Account Number"
                                                        id={"accountNumber"}
                                                        name={"accountNumber"}
                                                        type="number"
                                                        value={values.accountNumber}
                                                        onChangeText={e => {
                                                            setFieldValue("accountNumber", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Transit Code"
                                                        id={"transitCode"}
                                                        name={"transitCode"}
                                                        type="text"
                                                        value={values.transitCode}
                                                        onChangeText={e => {
                                                            setFieldValue("transitCode", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Swift Code"
                                                        id={"swiftCode"}
                                                        name={"swiftCode"}
                                                        value={values.swiftCode}
                                                        type="text"
                                                        onChangeText={e => {
                                                            setFieldValue("swiftCode", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Bank Name"
                                                        id={"bankName"}
                                                        name={"bankName"}
                                                        type="text"
                                                        value={values.bankName}
                                                        onChangeText={e => {
                                                            setFieldValue("bankName", e.target.value);
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Institution Number"
                                                        id={"instituteNumber"}
                                                        name={"instituteNumber"}
                                                        type="text"
                                                        value={values.instituteNumber}
                                                        onChangeText={e => {
                                                            setFieldValue(
                                                                "instituteNumber",
                                                                e.target.value
                                                            );
                                                        }}
                                                    />
                                                </div>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Full Address Of Account Holder"
                                                        id={"addressOfAccountHolder"}
                                                        name={"addressOfAccountHolder"}
                                                        type="text"
                                                        value={values.addressOfAccountHolder}
                                                        onChangeText={e => {
                                                            setFieldValue(
                                                                "addressOfAccountHolder",
                                                                e.target.value
                                                            );
                                                        }}
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col>
                                    <Card className="card-parent card-content p-4">
                                        <Card.Title className="ml-0 mt-0">
                                            Verification Section
                                        </Card.Title>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Mobile Number"
                                                        type="mobile-number"
                                                        placeholder="Mobile Number"
                                                        id="mobileNumber"
                                                        name="mobileNumber"
                                                        value={values.mobileNumber}
                                                        onChangeText={value => setFieldValue("mobileNumber", value)}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="mobileNumber"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Email Address"
                                                        id={"email"}
                                                        name={"email"}
                                                        type="email"
                                                        value={values.email}
                                                        onChangeText={e => {
                                                            setFieldValue("email", e.target.value);
                                                        }}
                                                        required
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="email"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Commercial licence"
                                                        id="commercialLicence"
                                                        name="commercialLicence"
                                                        type="single-image"
                                                        isNotCircle
                                                        value={values.commercialLicence}
                                                        onChangeText={setFieldValue}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="commercialLicence"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="field dashboard-profile-label">
                                                    <Field
                                                        label="Government Id"
                                                        id="governmentId"
                                                        name="governmentId"
                                                        type="single-image"
                                                        isNotCircle
                                                        value={values.governmentId}
                                                        onChangeText={setFieldValue}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="governmentId"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                            </Row>
                            <div className="profile-container1">
                                <Button
                                    className="profile-button primary-button"
                                    color="primary"
                                    onClick={handleSubmit}
                                >
                                    Save My Profile
                                </Button>
                                <Button
                                    className="profile-button btn-dark"
                                    onClick={cancelHandler}
                                >
                                    Cancel
                                </Button>
                            </div>
                        </Form>
                    </Container>
                )}
        />
    );
}