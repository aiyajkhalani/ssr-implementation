import React from "react";
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import { Field } from "../ws/Field";
import { requireMessage } from "../../helpers/string";
import GoogleMap from "../../components/map/map";
import { Box } from "@material-ui/core";

export const SurveyorProfile = ({
  initValue,
  updateUser,
  cancelHandler,
  latLng,
  userType
}) => {
  return (
    <Formik
      initialValues={{ ...initValue }}
      validationSchema={Yup.object().shape({
        companyName: Yup.string().required(requireMessage("Company Name")),

        firstName: Yup.string().required(requireMessage("First Name")),
        lastName: Yup.string().required(requireMessage("Last Name")),
        email: Yup.string()
          .required(requireMessage("Email"))
          .email("Email is invalid"),
        mobileNumber: Yup.number().required(requireMessage("Mobile Number")),
        companyWebsite: Yup.string().required(requireMessage("Company Website")),

        companyLogo: Yup.string().required(requireMessage("Company Logo")),
        image: Yup.string().required(requireMessage("Profile Photo")),
        accountNumber: Yup.number(),
      })}
      onSubmit={values => {
        values.userType = userType;
        updateUser(values);
      }}
      render={({ values, setFieldValue, handleSubmit, errors }) => (
        <Container fluid>
          <Form>
            <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
              <Box
                fontSize={20}
                letterSpacing={1}
                fontWeight={600}
                className="map-title"
              >
                Office Location
              </Box>
            </div>
            <Row>
              <Col xs={6} className="pr-0">
                <div className="add-boat-map map-div-form h-100">
                  <GoogleMap
                    className="googleMap-position"
                    latLng={latLng}
                    fetch={result => { 
                      let location = {  
                        coordinates: [result.latitude , result.longitude]
                      }
                      setFieldValue('officePlaceName', result.placeName || null)
                      setFieldValue('officeAddress', result.address || null)
                      setFieldValue('officeLocation', location || null)
                      //setFieldValue('officeLongitude', result.longitude || null)
                      setFieldValue('officeCity', result.city || null)
                      setFieldValue('officeState', result.state || null)
                      setFieldValue('officeCountry', result.country || null)
                    }}
                    height={42}
                    width={100}                    
                    placeHolder="Surveyor Location"
                    columnName={"address"}
                    isError={errors.address}
                  ></GoogleMap>
                </div>
              </Col>
              <Col xs={6}>
                <div className="h-100">
                  <Card
                    className="card-parent card-content p-4
                border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0 mt-0"
                  >
                    <Card.Title className="ml-0 mt-0">Contact Info</Card.Title>
                    <Row>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="Country"
                            id={"country"}
                            name={"country"}
                            type="text"
                            value={values.country}
                            onChangeText={e => {
                              setFieldValue("country", e.target.value);
                            }}
                            disabled
                          />
                        </div>
                      </Col>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="Company Name"
                            id={"companyName"}
                            name={"companyName"}
                            type="text"
                            value={values.companyName}
                            onChangeText={e => {
                              setFieldValue("companyName", e.target.value);
                            }}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="companyName"
                            className="error-message"
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Profile Picture"
                          id="image"
                          name="image"
                          type="single-image"
                          value={values.image}
                          onChangeText={setFieldValue}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="image"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                  </Card>
                  <Card
                    className="card-parent card-content p-4 mb-0
                border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0"
                  >
                    <Card.Title className="ml-0 mt-0">
                      Point of Contact
                    </Card.Title>
                    <Row>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="First Name"
                            id={"firstName"}
                            name={"firstName"}
                            type="text"
                            value={values.firstName}
                            onChangeText={e => {
                              setFieldValue("firstName", e.target.value);
                            }}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="firstName"
                            className="error-message"
                          />
                        </div>
                      </Col>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="Last Name"
                            id={"lastName"}
                            name={"lastName"}
                            type="text"
                            value={values.lastName}
                            onChangeText={e => {
                              setFieldValue("lastName", e.target.value);
                            }}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="lastName"
                            className="error-message"
                          />
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="Company Website  "
                            id={"companyWebsite"}
                            name={"companyWebsite"}
                            type="text"
                            value={values.companyWebsite}
                            onChangeText={e => {
                              setFieldValue("companyWebsite", e.target.value);
                            }}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="companyWebsite"
                            className="error-message"
                          />
                        </div>
                      </Col>
                    </Row>
                  </Card>
                </div>
              </Col>
            </Row>
            <Row className="h-100">
              <Col xs={6} className="pr-0">
                <Card
                  className="card-parent card-content p-4
                    border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0 h-100 mb-0"
                >
                  <Card.Title className="ml-0 mt-0">
                    Bank Information
                  </Card.Title>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Holder Name"
                          id={"holderName"}
                          name={"holderName"}
                          type="text"
                          value={values.holderName}
                          onChangeText={e => {
                            setFieldValue("holderName", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Branch Name"
                          id={"branchName"}
                          name={"branchName"}
                          type="text"
                          value={values.branchName}
                          onChangeText={e => {
                            setFieldValue("branchName", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Branch Address"
                          id={"branchAddress"}
                          name={"branchAddress"}
                          type="text"
                          onChangeText={e => {
                            setFieldValue("branchAddress", e.target.value);
                          }}
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Account Number"
                          id={"accountNumber"}
                          name={"accountNumber"}
                          type="number"
                          value={values.accountNumber}
                          onChangeText={e => {
                            setFieldValue("accountNumber", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Transit Code"
                          id={"transitCode"}
                          name={"transitCode"}
                          type="text"
                          value={values.transitCode}
                          onChangeText={e => {
                            setFieldValue("transitCode", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Swift Code"
                          id={"swiftCode"}
                          name={"swiftCode"}
                          value={values.swiftCode}
                          type="text"
                          onChangeText={e => {
                            setFieldValue("swiftCode", e.target.value);
                          }}
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Bank Name"
                          id={"bankName"}
                          name={"bankName"}
                          type="text"
                          value={values.bankName}
                          onChangeText={e => {
                            setFieldValue("bankName", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Institution Number"
                          id={"instituteNumber"}
                          name={"instituteNumber"}
                          type="text"
                          value={values.instituteNumber}
                          onChangeText={e => {
                            setFieldValue("instituteNumber", e.target.value);
                          }}
                        />
                      </div>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Full Address Of Account Holder"
                          id={"addressOfAccountHolder"}
                          name={"addressOfAccountHolder"}
                          type="text"
                          value={values.addressOfAccountHolder}
                          onChangeText={e => {
                            setFieldValue(
                              "addressOfAccountHolder",
                              e.target.value
                            );
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </Card>
              </Col>

              <Col xs={6}>
                <Card
                  className="card-parent card-content p-4
                border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0 h-100 mb-0"
                >
                  <Card.Title className="ml-0 mt-0">
                    Information
                  </Card.Title>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Attach Company Logo"
                          id={"companyLogo"}
                          name={"companyLogo"}
                          type="single-image"
                          value={values.companyLogo}
                          onChangeText={setFieldValue}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="companyLogo"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>

            <Row>
              <Col>
                <Card className="card-parent card-content p-4">
                    <Card.Title className="ml-0 mt-0">
                        {'Verification Section'}
                    </Card.Title>
                    <Row>
                      <Col>
                        <div className="field dashboard-profile-label">
                        <Field
                          label="Mobile Number"
                          type="mobile-number"
                          placeholder="Mobile Number"
                          id="mobileNumber"
                          name="mobileNumber"
                          value={values.mobileNumber}
                          onChangeText={value => setFieldValue("mobileNumber", value)}
                          required
                        />
                          <ErrorMessage
                            component="div"
                            name="mobileNumber"
                            className="error-message"
                          />
                        </div>
                      </Col>
                      <Col>
                        <div className="field dashboard-profile-label">
                          <Field
                            label="E-Mail Address"
                            id={"email"}
                            name={"email"}
                            type="text"
                            value={values.email}
                            onChangeText={e => {
                              setFieldValue("email", e.target.value);
                            }}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="email"
                            className="error-message"
                          />
                        </div>
                      </Col>
                        <Col>
                          <div className="field dashboard-profile-label">
                              <Field
                                  label="Commercial licence"
                                  id="commercialLicence"
                                  name="commercialLicence"
                                  type="single-image"
                                  isNotCircle
                                  value={values.commercialLicence}
                                  onChangeText={setFieldValue}
                              />
                              <ErrorMessage
                                  component="div"
                                  name="commercialLicence"
                                  className="error-message"
                              />
                          </div>
                      </Col>
                      <Col>
                          <div className="field dashboard-profile-label">
                              <Field
                                  label="Government Id"
                                  id="governmentId"
                                  name="governmentId"
                                  type="single-image"
                                  isNotCircle
                                  value={values.governmentId}
                                  onChangeText={setFieldValue}
                              />
                              <ErrorMessage
                                  component="div"
                                  name="governmentId"
                                  className="error-message"
                              />
                          </div>
                      </Col>
                    </Row>
                </Card>
              </Col>
            </Row>

            <div className="profile-container1">
              <Button
                className="profile-button primary-button"
                color="primary"
                onClick={handleSubmit}
              >
                Save My Profile
              </Button>
              <Button
                className="profile-button btn-dark"
                onClick={cancelHandler}
              >
                Cancel
              </Button>
            </div>
          </Form>
        </Container>
      )}
    />
  );
};
