import React from "react";
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import { Field } from "../ws/Field";
import { requireMessage } from "../../helpers/string";

export const RentAndCharterProfile = ({
  initValue,
  updateUser,
  cancelHandler,
  userType
}) => {
  return (
    <Formik
      initialValues={{ ...initValue, type: "individual" }}
      validationSchema={Yup.object().shape({

        firstName: Yup.string().required(requireMessage("First Name")),
        lastName: Yup.string().required(requireMessage("Last Name")),
        email: Yup.string()
          .required(requireMessage("Email"))
          .email("Email is invalid"),
        mobileNumber: Yup.string().required(requireMessage("Mobile Number")),

        address1: Yup.string().required(requireMessage("Address")),
        street: Yup.string().required(requireMessage("Street")),
        city: Yup.string().required(requireMessage("City or Town")),
        state: Yup.string().required(requireMessage("State")),
        image: Yup.string().required(requireMessage("Profile Photo")),
      })}
      onSubmit={values => {
        values.userType = userType
        const { type, ...restValues } = values
        updateUser(restValues);
      }}
      render={({ values, setFieldValue, handleSubmit, errors }) => (
        <Container fluid>
          <Form>
            <Row>
              <Col xs={8}>
                <Card className="card-parent card-content p-4 mt-0 mb-0 h-100">
                  <Card.Title className="ml-0 mt-0">Contact Info</Card.Title>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Country"
                          id={"country"}
                          name={"country"}
                          type="text"
                          value={values.country}
                          onChangeText={e => {
                            setFieldValue("country", e.target.value);
                          }}
                          disabled
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="You are"
                          id={"type"}
                          name={"type"}
                          type="text"
                          value={values.type}
                          onChangeText={e => {
                            setFieldValue("type", e.target.value);
                          }}
                          disabled
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="City Or Town"
                          id={"city"}
                          name={"city"}
                          type="text"
                          value={values.city}
                          onChangeText={e => {
                            setFieldValue("city", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="city"
                          className="error-message"
                        />
                      </div>
                    </Col>

                  </Row>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="First Name"
                          id={"firstName"}
                          name={"firstName"}
                          type="text"
                          value={values.firstName}
                          onChangeText={e => {
                            setFieldValue("firstName", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="firstName"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Last Name"
                          id={"lastName"}
                          name={"lastName"}
                          value={values.lastName}
                          type="text"
                          onChangeText={e => {
                            setFieldValue("lastName", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="lastName"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>

                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Company Name"
                          id={"companyName"}
                          name={"companyName"}
                          type="text"
                          value={values.companyName}
                          onChangeText={e => {
                            setFieldValue("companyName", e.target.value);
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="companyName"
                          className="error-message"
                        />
                      </div>
                    </Col>

                  </Row>

                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Company Website "
                          id={"companyWebsite"}
                          name={"companyWebsite"}
                          value={values.companyWebsite}
                          type="text"
                          onChangeText={e => {
                            setFieldValue("companyWebsite", e.target.value);
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="companyWebsite"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="State Or Province"
                          id={"state"}
                          name={"state"}
                          type="text"
                          value={values.state}
                          onChangeText={e => {
                            setFieldValue("state", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="state"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Profile Picture"
                          id="image"
                          name="image"
                          type="single-image"
                          value={values.image}
                          onChangeText={setFieldValue}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="image"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col xs={4} className="pl-0">
                <Card className="card-parent card-content p-4 mt-0 mb-0">
                  <Card.Title className="ml-0 mt-0">Address</Card.Title>

                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Address 1"
                          id={"address1"}
                          name={"address1"}
                          type="textarea"
                          value={values.address1}
                          onChangeText={e => {
                            setFieldValue("address1", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="address1"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Address 2"
                          id={"address2"}
                          name={"address2"}
                          type="textarea"
                          value={values.address2}
                          onChangeText={e => {
                            setFieldValue("address2", e.target.value);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12}>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Street"
                          id={"street"}
                          name={"street"}
                          type="text"
                          value={values.street}
                          onChangeText={e => {
                            setFieldValue("street", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="street"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col></Col>
                  </Row>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Zip or Postal Code"
                          id={"zip"}
                          name={"zip"}
                          type="text"
                          value={values.zip}
                          onChangeText={e => {
                            setFieldValue("zip", e.target.value);
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="zip"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="P.O. Box"
                          id={"postBox"}
                          name={"postBox"}
                          type="text"
                          value={values.postBox}
                          onChangeText={e => {
                            setFieldValue("postBox", e.target.value);
                          }}
                        />
                        <ErrorMessage
                          component="div"
                          name="postBox"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>

                </Card>
              </Col>
            </Row>

            <Row>
              <Col>
                <Card className="card-parent card-content p-4">
                  <Card.Title className="ml-0 mt-0">
                    {'Verification Section'}
                  </Card.Title>
                  <Row>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Mobile Number"
                          type="mobile-number"
                          placeholder="Mobile Number"
                          id="mobileNumber"
                          name="mobileNumber"
                          value={values.mobileNumber}
                          onChangeText={value => setFieldValue("mobileNumber", value)}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="mobileNumber"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label=" E-Mail Address "
                          id={"email"}
                          name={"email"}
                          type="text"
                          value={values.email}
                          onChangeText={e => {
                            setFieldValue("email", e.target.value);
                          }}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="email"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Commercial licence"
                          id="commercialLicence"
                          name="commercialLicence"
                          type="single-image"
                          isNotCircle
                          value={values.commercialLicence}
                          onChangeText={setFieldValue}
                        />
                        <ErrorMessage
                          component="div"
                          name="commercialLicence"
                          className="error-message"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="field dashboard-profile-label">
                        <Field
                          label="Government Id"
                          id="governmentId"
                          name="governmentId"
                          type="single-image"
                          isNotCircle
                          value={values.governmentId}
                          onChangeText={setFieldValue}
                        />
                        <ErrorMessage
                          component="div"
                          name="governmentId"
                          className="error-message"
                        />
                      </div>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>

            <div className="profile-container1">
              <Button
                className="profile-button primary-button"
                color="primary"
                onClick={handleSubmit}
              >
                Save My Profile
              </Button>
              <Button
                className="profile-button btn-dark"
                onClick={cancelHandler}
              >
                Cancel
              </Button>
            </div>
          </Form>
        </Container>
      )}
    />
  );
};
