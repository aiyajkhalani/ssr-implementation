import React, { useContext, useState } from "react";
import { MdRateReview } from "react-icons/md";
import { FaCheckCircle, FaHome, FaUser, FaShip } from "react-icons/fa";
import { IoMdCheckmarkCircleOutline, IoIosChatbubbles } from "react-icons/io";
import { GoQuote } from "react-icons/go";
import uuid from "uuid/v4";
import moment from "moment";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import Rating from "react-rating";
import Truncate from "react-truncate";
import { connect } from "react-redux";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Grid, Card, Box } from "@material-ui/core";
import TextArea from "antd/lib/input/TextArea";

import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Layout, rentInnerUserReviews } from "../../components";
import { UserReview } from "./userReview";
import "../../containers/rentInner/rentInner.scss";
import UserContext from "../../UserContext";
import "../../components/home/icon.scss";
import InnerRattingProfile from "../staticComponent/innerRattingProfile";
import { displayDefaultReview } from "../../helpers/string";

import "./userProfile.scss";
import { UserProfileListings } from "../gridComponents/userProfileListings";
import { dimension } from "../../util/enums/enums";
import { userProfileListingInner } from "../home/data";
import { KeyboardArrowRight } from "@material-ui/icons";
import SharePopup from "../share/SharePopup";
import { reviewTypeProfile } from "../../util/utilFunctions";
import { getAllReviews } from "../../graphql/mediaReviewsSchema";

const UserProfile = props => {
  const { history, createReview } = useContext(UserContext);
  const [
    sharePopUpOpen,
    setSharePopUpOpen,
  ] = React.useState(false);
  const [visible, toggleMenu] = React.useState(false);
  const [review] = React.useState({
    review: "",
    rating: ""
  });
  const { location } = history;
  const {
    state: { seller }
  } = location && location;

  const { firstName, address1, address2 } = seller && seller;
  const { boat, currentUser, itemsLength, allReviews } = props;

  //  componentDidMount() {
  //    const { getAllReviews } = props;
  //    getAllReviews()
  //  }
  return (
    <Layout>
      <div>
        <Container>
          <Row>
            <Col xs={12} className="mt-5">
              <Row>
                <Col xs={7} className="pr-4">
                  <div className="user-profile-desc-div h-100">
                    <h1 className="font-weight-400 user-color-profile profile-user-name gray-dark">
                      Hi, {firstName && firstName}{" "}
                      {seller.lastName && seller.lastName}
                    </h1>
                    <h6 className="font-weight-400 mt-2 rentInner-userTextH4 font-12 dark-silver">
                      Joined in {moment(seller.createAt).format("YYYY")}
                    </h6>
                    <h4 className="font-weight-300 rentInner-userTextH4 mb-0 user-profile-desc">
                      <Truncate lines={3} ellipsis={<span>..</span>}>
                        Lorem ipsum dolor sit amet, consequis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat.ctetur adipiscing elit, sed do eiusmod
                        tquis nostrud exercitation
                      </Truncate>
                      {/* <span className="ml-1 boat-media-read-more-text navy-blue cursor-pointer">
                        Read More
                      </span> */}
                    </h4>
                    <hr className="ml-0 mr-0" />

                    <div className="mb-2 d-flex align-items-center">
                      <img
                        src={require("../../components/userProfile/image/address.png")}
                        className="user-profile-icon-div mr-2"
                        alt=""
                      />
                      <span className="rentInner-userTextH4 font13 dark-silver">
                        Lives in {seller.city}, {seller.country}
                      </span>
                    </div>
                    <div className="mb-2 d-flex align-items-center">
                      <img
                        src={require("../../components/userProfile/image/message.png")}
                        className="user-profile-icon-div mr-2"
                        alt=""
                      />
                      <span className="rentInner-userTextH4 font13 dark-silver">
                        Speaks English
                      </span>
                    </div>
                    <div className="mb-2 d-flex align-items-center">
                      <img
                        src={require("../../components/userProfile/image/work.png")}
                        className="user-profile-icon-div mr-2"
                        alt=""
                      />
                      <span className="rentInner-userTextH4 font13 dark-silver">
                        Work: Own business
                      </span>
                    </div>
                  </div>
                </Col>
                <Col xs={5}>
                  <div className="rentInnner-userCard">
                    <Row>
                      <Col xs={6} className="user-info-profile pr-4">
                        <div>
                          <div className="mt-4 mb-4">
                            <div className="mt-3 mb-2 d-flex align-items-center">
                              <img
                                src={require("../../components/userProfile/image/boat-shipper.png")}
                                className="user-profile-icon-div mr-2"
                                alt=""
                              />
                              <span className="rentInner-userTextH4 text-transform-capitalize font13 dark-silver">
                                {seller.role && seller.role.role}
                              </span>
                            </div>
                            <div className="mb-2 d-flex align-items-center">
                              <img
                                src={require("../../components/userProfile/image/boat.png")}
                                className="user-profile-icon-div mr-2"
                                alt=""
                              />
                              <span className="rentInner-userTextH4 font13 dark-silver">
                                {seller.relatedItems &&
                                  seller.relatedItems.length}{" "}
                                Boat
                              </span>
                            </div>
                            <div className="mb-2 d-flex align-items-center">
                              <img
                                src={require("../../components/userProfile/image/verified.png")}
                                className="user-profile-icon-div mr-2"
                                alt=""
                              />
                              <span className="rentInner-userTextH4 font13 dark-silver">
                                Verified
                              </span>
                            </div>
                            <div className="mb-2 d-flex align-items-center">
                              <img
                                src={require("../../components/userProfile/image/reviews.png")}
                                className="user-profile-icon-div mr-2"
                                alt=""
                              />
                              <span className="rentInner-userTextH4 font13 dark-silver">
                                {seller &&
                                  seller.reviews &&
                                  seller.reviews.reviews &&
                                  seller.reviews.reviews.length}{" "}
                                Reviews
                              </span>
                            </div>
                          </div>
                          <hr />
                          <div className="mt-4">
                            <h6 className="mb-3 font13">
                              {seller.firstName} has provided
                            </h6>
                            {seller.documentVerification &&
                              (
                                <div className="mb-2 d-flex align-items-center">
                                  <img
                                    src={seller.documentVerification.governmentIdVerified
                                      ? require("../../components/userProfile/image/vector.png")
                                      : require("../../components/userProfile/image/cross-icon.jpg")
                                    }
                                    className="user-profile-icon-div mr-2"
                                    alt=""
                                  />
                                  <span className="rentInner-userTextH4 font13 dark-silver">
                                    Government ID
                                  </span>
                                </div>
                              )}
                            {seller.documentVerification &&
                              (
                                <div className="mb-2 d-flex align-items-center">
                                  <img
                                    src={seller.documentVerification.emailVerified
                                      ? require("../../components/userProfile/image/vector.png")
                                      : require("../../components/userProfile/image/cross-icon.jpg")
                                    }
                                    className="user-profile-icon-div mr-2"
                                    alt=""
                                  />
                                  <span className="rentInner-userTextH4 font13 dark-silver">
                                    Email address
                                  </span>
                                </div>
                              )}
                            {seller.documentVerification &&
                              (
                                <div className="mb-2 d-flex align-items-center">
                                  <img
                                    src={seller.documentVerification.mobileVerified
                                      ? require("../../components/userProfile/image/vector.png")
                                      : require("../../components/userProfile/image/cross-icon.jpg")
                                    }
                                    className="user-profile-icon-div mr-2"
                                    alt=""
                                  />
                                  <span className="rentInner-userTextH4 font13 dark-silver">
                                    Phone number
                                  </span>
                                </div>
                              )}
                            {seller.documentVerification &&
                              (
                                <div className="mb-2 d-flex align-items-center">
                                  <img
                                    src={seller.documentVerification.commercialLicenceVerified
                                      ? require("../../components/userProfile/image/vector.png")
                                      : require("../../components/userProfile/image/cross-icon.jpg")
                                    }
                                    className="user-profile-icon-div mr-2"
                                    alt=""
                                  />
                                  <span className="rentInner-userTextH4 font13 dark-silver">
                                    Commercial Licence
                                  </span>
                                </div>
                              )}
                          </div>
                        </div>
                      </Col>
                      <Col xs={6}>
                        <div className="d-flex justify-content-center align-items-center">
                          <div className="mb-3 mt-1">
                            <div className="rentInner-userImg rounded-circle user-profile-online-section">
                              <img
                                className="rounded-circle"
                                src={
                                  seller.image ||
                                  require("../../assets/images/rentInner/img-6.jpg")
                                }
                                // src={require(defaultImageForUser)}
                                alt="placeholder"
                              />
                              <div className="user-online">
                                <div className="online-div-user"></div>
                              </div>
                            </div>
                            <div className="d-flex justify-content-evenly mt-3">
                              <img
                                src={require("../../components/userProfile/image/group-message.png")}
                                className="user-profile-social-icon-div cursor-pointer"
                                alt=""
                              />
                              <img
                                src={require("../../components/userProfile/image/profile.png")}
                                className="user-profile-social-icon-div cursor-pointer"
                                alt=""
                              />
                              <img
                                src={require("../../components/userProfile/image/share.png")}
                                className="user-profile-social-icon-div cursor-pointer"
                                alt=""
                                onClick={() => setSharePopUpOpen(true)}
                              />
                              {sharePopUpOpen && (
                                <SharePopup
                                  handleClick={() => setSharePopUpOpen(false)}
                                  useOwnIcon={true}
                                />
                              )}
                            </div>
                            {seller.companyLogo && (
                              <div className="logo-box">
                                <img
                                  src={seller.companyLogo}
                                  height={50}
                                  width={50}
                                  alt=""
                                />
                              </div>
                            )}
                            <div className="company-name dark-silver font13 text-center">
                              {seller.companyName}
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col xs={12} className="mt-4">
              <div>
                {/* start right div */}

                <h4 className="pt-3 mb-4 color-dark-grey">
                  {seller.firstName}'s listing
                </h4>
                {seller &&
                  seller.relatedItems &&
                  seller.relatedItems.length && (
                    <UserProfileListings
                      itemsLength={seller.relatedItems.length}
                      data={seller && seller.relatedItems}
                      dimension={dimension.boatMediaVideo}
                    />
                  )}
                {/* user profile listing start */}

                {/* <div className=" d-flex flex-column rentInner-userListing-imgDiv mb-4 pb-2">
                  <div className="rentInner-userListing-img">
                    <img
                      className="br-10"
                      src={require("../../assets/images/rentInner/img-11.jpg")}
                      alt="placeholder"
                    />
                  </div>
                  <span className="rentInner-userListing-textBrown mt-2 dark-silver f-15">
                    The Beach at JBR
                  </span>
                  <span className="user-profile-country blue-dark font13">Dubai , UAE</span>
                  <span className="rentInner-userListing-textDesc rentInner-userTextH4 font13 font-weight-400">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                  </span>
                  <div className="d-flex align-items-center">
                    <div className="mr-1 rating-small-size">
                      <Rating
                        className="rating-clr"
                        initialRating={0}
                        fullSymbol={<StarIcon />}
                        emptySymbol={<StarBorderIcon />}
                        placeholderSymbol={<StarBorderIcon />}
                        readonly
                      />
                    </div>
                    <span className="rating-count font-12"> (2) </span>
                  </div>
                </div> */}

                {/* user profile listing end */}

                <hr className="pt-3" />
                <div
                  className="text-right f-14 width-100 mb-0 cursor-pointer add-feedback-mt"
                  // onClick={() => this.toggleMenu()}
                  onClick={() => toggleMenu(true)}
                >
                  <img
                    src={require("../../assets/images/boatInner/boatinner-like.png")}
                    className="inner-feedback-img mr-2"
                    alt=""
                  />
                  <span className="add-feedback-text">Add Feedback</span>
                </div>
                {visible && (
                  <Formik
                    initialValues={{ ...review }}
                    onSubmit={values => {
                      const { moduleId, currentUser, boat } = this.props;
                      values.moduleId = boat && boat.seller.id;
                      values.userId = currentUser && currentUser.id;

                      if (values.userId && values.moduleId) {
                        createReview(values);
                        toggleMenu(true);
                      }
                    }}
                    validationSchema={Yup.object().shape({
                      rating: Yup.string().required(
                        "rating field is required."
                      ),
                      reviews: Yup.string().required(
                        "review field is required."
                      )
                    })}
                    render={({
                      errors,
                      status,
                      touched,
                      setFieldValue,
                      values,
                      handleSubmit
                    }) => (
                        <Form>
                          <Card className="review-form">
                            <Grid container className="p-10">
                              <Grid item sm={6}>
                                <div
                                  className="ant-form-item-required"
                                  title="Rating"
                                >
                                  Add Review
                              </div>
                                <div className="d-flex align-items-center">
                                  <div className="mr-1">{reviewTypeProfile(seller)} </div>
                                  {/* <Rating
                                className="rating-clr"
                                initialRating={0}
                                onClick={value => {
                                  setFieldValue("rating", value);
                                  this.setState({ rating: value });
                                }}
                                emptySymbol={<StarBorderIcon />}
                                placeholderSymbol={<StarIcon />}
                                fullSymbol={<StarIcon />}
                                // placeholderRating={rating}
                              />*/}
                                </div>
                                <ErrorMessage
                                  className="invalid-feedback ant-typography-danger"
                                  name={`rating`}
                                  component="span"
                                />
                              </Grid>
                              <Grid item sm={12} className="gutter-box mt-10">
                                <TextArea
                                  name="reviews"
                                  className="form-control"
                                  placeholder="Review"
                                  rows="5"
                                  value={values.reviews}
                                  onChange={e =>
                                    setFieldValue("reviews", e.target.value)
                                  }
                                />
                                <ErrorMessage
                                  className="invalid-feedback ant-typography-danger"
                                  name={`reviews`}
                                  component="span"
                                />
                              </Grid>
                              <div className="w-100 float-left text-center">
                                <Grid
                                  item
                                  sm={12}
                                  className="mt-10 review-button"
                                >
                                  <button
                                    className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                                    type="submit"
                                  >
                                    Save
                                </button>
                                  <button
                                    onClick={() => toggleMenu(false)}
                                    className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                                    type="submit"
                                  >
                                    Cancel
                                </button>
                                </Grid>
                              </div>
                            </Grid>
                          </Card>
                        </Form>
                      )}
                  />
                )}
                {/* {seller &&
                seller.reviews &&
                seller.reviews.reviews &&
               seller.reviews.reviews.length ? (*/}
                <Row spacing={3}>
                  <Col xs={9}>
                    <div>
                      <h4 className=" color-dark-grey mb-3">
                        {seller.reviews.reviews.length} reviews
                      </h4>
                      {/* component start */}

                      {seller.reviews.reviews.map((item, index) => {
                        return (
                          <UserReview
                            key={uuid()}
                            review={item}
                            isBorder={
                              seller.reviews.reviews.length - 1 === index
                            }
                          />
                        );
                      })}

                      {/*  {seller.reviews.reviews.length > itemsLength ? (*/}
                      <div className="mt-50">
                        <h2>
                          <Link
                            to={"#"}
                            className="show-link mb-0 text-decoration-unset"
                          >
                            Show all ({seller.reviews.reviews.length})
                            <KeyboardArrowRight />
                          </Link>
                        </h2>
                      </div>
                      {/* ) : (
                          <></>
                       )}*/}
                      {/* component end */}
                    </div>
                  </Col>
                  <Col xs={3}>
                    <InnerRattingProfile
                      iconColor="iconColor-boatInner"
                      btnColor="boatInner-btnBg"
                      btnBlue="boatInner-btnOrange"
                      moduleId={seller.id}
                      userId={currentUser.id}
                      data={displayDefaultReview(seller.reviews.ratings)}
                      reviews={seller.reviews.reviews}
                    />
                  </Col>
                </Row>
                {/*  ) : (
                  <> </>
              )}*/}
                {/* end right div */}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </Layout>
  );
};

const mapStateToProps = state => ({
  boat: state.boatReducer.boat || {},
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  allReviews: state.mediaReviewsReducer && state.mediaReviewsReducer.reviews && state.mediaReviewsReducer.reviews.getAllReviews
});
const mapDispatchToProps = dispatch => ({
  getAllReviews: () => dispatch(getAllReviews()),
  // verifyUserEmail: data => dispatch(verifyUserEmail(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
