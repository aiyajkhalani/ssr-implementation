import React from "react";
import Truncate from "react-truncate";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";

export class UserReview extends React.Component {
  render() {
    const { review, isBorder } = this.props;
    return (
      <>
        <div className="d-flex flex-column">
          <span className="font-12 rentInner-userTextH4 font-weight-400 gray-light">
            {review.date}
          </span>
          <Rating
            className="rating-clr"
            initialRating={review.rating}
            fullSymbol={<StarIcon />}
            emptySymbol={<StarBorderIcon />}
            placeholderSymbol={<StarBorderIcon />}
            readonly
          />
          <span className="rentInner-userTextH4 mt-2 font-weight-400 dark-silver font13">
            <Truncate lines={3} ellipsis={<span>..</span>}>
            {review.reviews}
            </Truncate>
          </span>
          {/* <div className="d-flex mt-3">
            <div className="rentInnerUser-reviewerImage rounded-circle mr-3">
              <img
                className="rounded-circle"
                src={require("../../assets/images/rentInner/img-6.jpg")}
                alt="placeholder image"
              />
            </div>
            <div className="d-flex flex-column justify-content-center">
              <div>
              <span className="font13 rentInner-userTextH4 rentInnerUser-reviewerName gray-dark font-weight-400">
                {review.name}, </span> 
                <span className="light-silver font-weight-400 font13">{review.country}</span>
                </div>
              <span className="rentInner-userTextH4 font-12 gray-light">
                {review.joinedDate}.
              </span>
            </div>
          </div> */}
        </div>
        {!isBorder && <hr className={"user-profile-review-border-top"}/>}
      </>
    );
  }
}
