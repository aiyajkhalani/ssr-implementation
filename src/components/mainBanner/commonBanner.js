import React, { Component } from "react";

import { Player, BigPlayButton, ControlBar, Shortcut } from "video-react";
import { CommonBannerStyle, BannerHeight } from "../styleComponent/styleComponent";

export default class CommonBanner extends Component {
  state = {
    height : 95
  }
  componentDidMount(){
    const selector = document.querySelector('.header-responsive');
    const height = selector.offsetHeight;
    this.setState({height})
  }
  render() {
    const { data } = this.props;
    const { height } = this.state;
    return (
      <BannerHeight height={height} className="home-video-banner">
        {data && data.length > 0 && (
          <>
            {data[0].metatype === "image" ? (
              <div className="banner-size">
                <CommonBannerStyle img={encodeURI(data[0].url)} />
              </div>
            ) : (
              <div className="banner-size">
                <Player
                  controls={false}
                  muted
                  autoPlay={true}
                  playsInline
                  loop= {true}
                  poster={data[0].thumbnail}
                  src={data[0].url}
                >
                  <Shortcut shortcuts={this.newShortcuts} clickable={false} />
                  <BigPlayButton className="visible-none" />
                  <ControlBar disableCompletely className="my-class" />
                </Player>
              </div>
            )}
          </>
        )}
      </BannerHeight>
    );
  }
}
CommonBanner.defaultProps = {
  // height:95
}