import React from 'react'
import { createMemoryHistory } from 'history';
import { Route, Router, Switch } from 'react-router-dom';
import ScrollToTop from '../components/SrollToTop';
// import Home from '../Home';
import Login from '../containers/login/login'



let history

// if (typeof document !== 'undefined') {
//   const createBrowserHistory = require('history/createBrowserHistory').default

  history = createMemoryHistory()
// }

export const AppRoutes = () => {
    return (
        <Router history={history}>
            <ScrollToTop>
                {/* <Switch>
                <Route path='/login' exact component={Home} />
                </Switch> */}
                <Login />
            </ScrollToTop>
        </Router>
    )
}
