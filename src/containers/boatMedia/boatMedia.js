import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import uuid from "uuid/v4";
import { isMobile } from "react-device-detect";

import {
  Layout,
  boatMediaArticle,
  boatMediaVideo,
  boatMediaPost,
  mediaHome,
  boatMediaCommonVideo
} from "../../components";

import "./boatMedia.scss";
import { AiFillMessage } from "react-icons/ai";
import { Row, Col } from "react-bootstrap";
import { FaUser } from "react-icons/fa";
import { getCategoryWiseVideos } from "../../redux/actions/VideoAction";
import { BoatMediaMainStyle, BoatMediaWidthStyle } from "../../components/styleComponent/styleComponent";
import { BoatMediaArticles } from "../../components/gridComponents/boatMediaArticles";
import { BoatMediaVideos } from "../../components/gridComponents/boatMediaVideos";
import { BoatMediaPosts } from "../../components/gridComponents/boatMediaPosts";
import { BoatMediaReviews } from "../../components/gridComponents/boatMediaReviews";
import { dimension, moduleCategory } from "../../util/enums/enums";
import UserContext from "../../UserContext";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { BoatMediaCommonVideos } from "../../components/gridComponents/boatMediaCommonVideos";
import { getAllReviews } from "../../redux/actions/mediaReviewsAction";
import { getAllMediaArticles } from "../../redux/actions/mediaArticleAction";

class BoatMedia extends Component {
  state = {
    setVideoFlag: false,
    videoUrl: "",
    height: dimension.boatMediaMainDiv.height,
    width: dimension.boatMediaMainDiv.width,
  };

  static contextType = UserContext;

  playVideo = url => {
    this.setState(prevState => ({
      setVideoFlag: !prevState.setVideoFlag,
      videoUrl: url
    }));
  };
  componentDidMount() {
    const width = getRatio(dimension, "boatMediaMainDiv", ".boat-media-main-section");
    const height = getHeightRatio(
      dimension,
      "boatMediaMainDiv",
      ".boat-media-main-section"
      );
      
      this.setState({ width, height });

      const { getAllReviews, getAllMediaArticles, getCategoryWiseVideos } = this.props;
  
      getAllReviews();
      getAllMediaArticles({page: 1, limit: 10, isApproved: true});
      getCategoryWiseVideos({ type: moduleCategory.BOAT_MEDIA, metatype: "video" });
  }

  render() {
    const { categoryVideos, articles, allReviews } = this.props;
    const { setVideoFlag, videoUrl, height, width } = this.state;

    return (
        <Layout className="boat-media-layout">
          <Grid container className=" position-relative">
            <div className="boat-media-header-img width-100 boat-media-header-img-responsive">
              <img
                src={require("../boatMedia/image/banner11.jpg")}
                alt="adamsea"
                className="h-100 width-100"
              />
            </div>
            <div className={isMobile ? "d-none" : "d-block"}>
              <div className="boat-media-banner-text position-absolute">
                <h5 className="font-weight-500 media-banner-page-title">Home / Boat Show</h5>
                <h1 className="font-weight-400 mt-4 media-banner-title">AdamSea Boat Media</h1>
                <h5 className="font-weight-400 media-banner-title-desc">
                  Join the conversation and connect with other hosts who are
                  creating a world where anyone can belong.
                </h5>
                <div className="property-meta mt-4 pt-3">
                  <div className="d-flex">
                    <span className="d-flex align-items-center color-yellow member-post-value">
                      <FaUser className="font-22 mr-2 member-post-value-icon" />
                      689,837 members
                    </span>
                    <span className="d-flex align-items-center ml-4 color-yellow member-post-value">
                      <AiFillMessage className="font-22 mr-2 member-post-value-icon" />
                      928,548 posts
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </Grid>

          <div className="container100 mt-5 pt-4 mb-5 pb-4">
            <div className="boat-media-main-section d-flex justify-content-between">
                <div className="boat-media-div">
                  <BoatMediaMainStyle
                    bgHeight={height}
                    bgWidth={width}
                    img={mediaHome.boatMediaImg}
                  ></BoatMediaMainStyle>
                  <BoatMediaWidthStyle bgWidth={width}>
                  <div className="boat-media-text-div mt-4">
                    <h4 className="font-weight-400 gray-dark media-main-div-title">Boat Media</h4>
                    <span className="light-silver f-15 media-main-div-desc">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    </span>
                  </div>
                  </BoatMediaWidthStyle>
                </div>
                <div className="boat-media-div">
                   <BoatMediaMainStyle
                    bgHeight={height}
                    bgWidth={width}
                    img={mediaHome.articleImage}
                  ></BoatMediaMainStyle>
                  <BoatMediaWidthStyle bgWidth={width}>
                  <div className="boat-media-text-div mt-4">
                    <h4 className="font-weight-400 gray-dark media-main-div-title">Article</h4>
                    <span className="light-silver f-15 media-main-div-desc">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam,
                    </span>
                  </div>
                  </BoatMediaWidthStyle>
                </div>
                <div className="boat-media-div">
                    <BoatMediaMainStyle
                    bgHeight={height}
                    bgWidth={width}
                    img={mediaHome.reviewImage}
                  ></BoatMediaMainStyle>
                  <BoatMediaWidthStyle bgWidth={width}>
                  <div className="boat-media-text-div mt-4">
                    <h4 className="font-weight-400 gray-dark media-main-div-title">Review</h4>
                    <span className="light-silver f-15 media-main-div-desc">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut.
                    </span>
                  </div>
                  </BoatMediaWidthStyle>
                </div>
              </div>
            </div>
          {articles && articles.length !== 0 && <hr className="mb-0" />}
          {articles && articles.length !== 0 && <div className="container100">
            <Grid container>
              <Row className="width-100">
                <Col xs={9} className="boat-media-article-div">
                  <div className="mt-5 pt-2 mb-5 pb-2">
                    <h4 className="font-weight-500 section-heading mt-0 gray-dark media-article-title">
                      Articles
                    </h4>

                    <div className="mt-2 pr-4">
                      <BoatMediaArticles
                        itemsLength={6}
                        boatMediaArticles={articles}
                        dimension={dimension.boatMediaArticle}
                      />
                    </div>
                  </div>
                </Col>
                <Col xs={3} className="pl-5 pb-2">
                  <div className="mt-5 pt-2">
                    <h4 className="font-weight-500 gray-dark">Categories</h4>

                    <div className="boat-inner-facilities-div d-flex align-items-center mt-4">
                      <ul className="rent pl-0 d-flex width-100 flex-wrap">
                        <li className="font-weight-400 dark-silver">
                          Articles (20)
                        </li>
                        <li className="font-weight-400 dark-silver">
                          Boat Media (20)
                        </li>
                        <li className="font-weight-400 dark-silver">
                          Marina Storage (19)
                        </li>
                        <li className="font-weight-400 dark-silver">
                          Fishing (36)
                        </li>
                        <li className="font-weight-400 dark-silver">Yacht (24)</li>
                        <li className="font-weight-400 dark-silver">
                          Reviews (43)
                        </li>
                        <li className="font-weight-400 dark-silver">
                          Super Yacht (38)
                        </li>
                        <li className="font-weight-400 dark-silver">
                          Downloads (38)
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div className="mt-4">
                    <h4 className="font-weight-500 gray-dark">Popular Posts</h4>

                    <div className="boat-media-popular-posts-div">
                      <Grid container className="mt-3">
                        <BoatMediaPosts
                          boatMediaPosts={boatMediaPost}
                          xs={12}
                          sm={12}
                          dimension={dimension.boatMediaPost}
                        />
                      </Grid>
                    </div>
                  </div>
                </Col>
              </Row>
            </Grid>
          </div>}
          {categoryVideos && categoryVideos.length !== 0 && <hr className="m-0" />}
          {categoryVideos && categoryVideos.length !== 0 && <div className="container100">
            <div>
              <div className="width-100">
                <div className="boat-media-video-div">
                  <div className="mt-5 pt-2 mb-5">
                    <h4 className="font-weight-500 mt-0 gray-dark">
                      Videos
                    </h4>

                    <div>
                      <BoatMediaVideos
                        itemsLength={6}
                        boatMediaVideos={categoryVideos}
                        dimension={dimension.boatMediaVideo}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>}
          {allReviews && allReviews.length !== 0 && <hr className="m-0" />}
          {allReviews && allReviews.length !== 0 && <div className="container100">
            <Grid container>
              <Row className="width-100">
                <div className="mt-5 pt-2 mb-5 width-100 boat-media-review-section">
                  <h4 className="font-weight-500 gray-dark">Reviews</h4>

                  <BoatMediaReviews
                    itemsLength={8}
                    boatMediaReviews={allReviews}
                    xs={6}
                    sm={6}
                  />
                </div>
              </Row>
            </Grid>
          </div>}
          <hr className="m-0" />
          <div className="container100">
            <div>
              <div className="width-100">
                <div className="mt-5 pt-2 mb-5 width-100">

                  <BoatMediaCommonVideos
                    itemsLength={8}
                    boatMediaCommonVideos={boatMediaCommonVideo}
                  />
                </div>
              </div>
            </div>
          </div>
        </Layout>
    );
  }
}

const mapStateToProps = state => ({
  articles: state.mediaArticleReducer && state.mediaArticleReducer.articles,
  categoryVideos: state.videoReducer && state.videoReducer.categoryVideos,
  allReviews: state.mediaReviewsReducer && state.mediaReviewsReducer.reviews && state.mediaReviewsReducer.reviews.getAllReviews
});

const mapDispatchToProps = dispatch => ({
  getAllReviews: () => dispatch(getAllReviews()),
  getAllMediaArticles: data => dispatch(getAllMediaArticles(data)),
  getCategoryWiseVideos: data => dispatch(getCategoryWiseVideos(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatMedia);
