import React from "react";
import { Layout } from "../../components/layout/layout";
import { Tab, Row, Col, Nav, Container } from "react-bootstrap";

import "./userGuide.scss";
import { InputBase, Grid } from "@material-ui/core";
import { IoIosArrowForward } from "react-icons/io";
import { pagination } from "../../util/enums/enums";
import { getAllUserGuide } from "../../redux/actions/userGuideAction";
import searchFilter from "../../helpers/filterUtil";
import { connect } from "react-redux";
import { getCategoriesWiseBanners } from "../../redux/actions";

class UserGuide extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      filterData: []
    }
  }

  createMarkup = data => {
    return { __html: data };
  };

  componentDidMount() {
    const { getAllUserGuide, getCategoriesWiseBanners } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    };
    const faqInput = {
      type: "userGuide",
      fieldName:"userGuideBanner"
    }
    getCategoriesWiseBanners(faqInput)
    getAllUserGuide(data);
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.userguides !== this.props.userguides) {
      this.setState({filterData: this.props.userguides})
    }
  }

  searchHandler() {
    const {userguides} = this.props;
    const {searchValue} = this.state;
    let temp = [];
    if (userguides.length) {
			temp = searchFilter(userguides, searchValue, 'title');
		} else {
			temp =  userguides || []
    }
    this.setState({filterData: temp});
  };

  render() {
    const { filterData, searchValue } = this.state;
    const {userGuideBanners } =this.props
    return (
      <Layout>
        <div className="user-guide-banner-div">
        <img src={userGuideBanners && userGuideBanners.length ? userGuideBanners[0].url : require("./slide-1.jpg")} alt="" style={{width: '100%', height: '100%'}} />

          <div className="user-guide-banner-text-div">
            <div>
              <h4 className="font-weight-400 text-white user-guide-title">
                Frequently Asked Questions
              </h4>
              <div className="d-flex mt-5 search-adamsea-help-desk">
                <h1 className="text-white user-guide-search-adamsea-title mb-0">Search AdamSea</h1>
                <h1 className="ml-2 user-guide-search-adamsea-title font-weight-bold mb-0">Help Desk</h1>
              </div>
              <div className="main-home-search user-guide-main-search mt-1">
                <InputBase
                  placeholder="Ask a question or search by keyword"
                  inputProps={{ "aria-label": "search google maps" }}
                  className="search-input"
                  value={searchValue}
                  onChange={e => {
                    this.setState({searchValue: e.target.value});
                  }}
                />
                <div
                  className="btn btn-primary btn-s mr--3 w-auto"
                  onClick={() => this.searchHandler()}
                >
                  <i className="adam-search1"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container100 mt-5 pt-4 pb-5 user-guide-main-container">
          <span className="font-22 user-guide-questions-main-title">Questions</span>
          <Tab.Container id="left-tabs-example" defaultActiveKey="0">
            <Row className="mt-4 user-guide-left-section">
              <Col sm={3} className="user-guide-left-main-div-size">
                <Nav
                  variant="pills"
                  className="user-guide-nav-left-div user-guide-scrollbar pr-3"
                >
                  {filterData && filterData.map((value, index) => (
                    <Nav.Item className="user-guide-nav-div d-flex align-items-center justify-content-between">
                      <Nav.Link eventKey={index} className="col-gray pl-0 pr-0 user-guide-left-side-questions-list">
                        {value.title}
                        <div>
                          <IoIosArrowForward className="col-gray user-guide-nav-icon" />
                        </div>
                      </Nav.Link>
                    </Nav.Item>
                  ))}
                </Nav>

                <div className="mt-5 pr-3">
                  <Grid item xs className="border-user-guide-section">
                    <div className="d-flex pl-4 pr-4 user-guide-img-left-div">
                      <div className="pt-4">
                        <div className="user-guide-div-img">
                          <img
                            src={require("../../assets/images/help/buying-process.png")}
                            alt="User Guide"
                            className="width-100 h-100"
                          />
                        </div>
                      </div>

                      <div className="m-auto">
                        <span className="font-20 font-weight-500 user-guide-img-left-div-title">AdamSea</span>
                        <div className="d-flex flex-column mt-2">
                          <span className="user-guide-img-div-desc">
                            Neque porro quisquam
                          </span>
                          <span className="user-guide-img-div-desc">
                            Dolorem ipsum estqui
                          </span>
                        </div>
                      </div>
                    </div>
                  </Grid>
                </div>

                <div className="mt-4 pr-3 user-guide-left-img-div">
                  <Grid item xs className="border-user-guide-section">
                    <div className="d-flex pl-4 pr-4 user-guide-img-left-div">
                      <div className="pt-4">
                        <div className="user-guide-div-img">
                          <img
                            src={require("../../assets/images/help/downloads.png")}
                            alt="User Guide"
                            className="width-100 h-100"
                          />
                        </div>
                      </div>

                      <div className="m-auto">
                        <span className="font-20 font-weight-500 user-guide-img-left-div-title">
                          User Guide
                        </span>
                        <div className="d-flex flex-column mt-2">
                          <span className="user-guide-img-div-desc">
                            Processes explained
                          </span>
                          <span className="user-guide-img-div-desc">
                            Clear your doubts
                          </span>
                        </div>
                      </div>
                    </div>
                  </Grid>
                </div>
              </Col>
              <Col sm={9} className="user-guide-right-main-div-size">
                <Tab.Content>
                  {filterData && filterData.map((value, index) => (
                    <Tab.Pane eventKey={index}>
                    <div className="user-guide-right-side-ans-div">
                      <div
                        dangerouslySetInnerHTML={this.createMarkup(value.description)}
                      />
                      </div>
                    </Tab.Pane>
                  ))}
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </div>
        <hr className="mt-5 faq-divider" />
        <Container className="mt-5 frequent-questions-container">
          <span className="font-22 font-weight-500 frequent-questions-title">
            Most Frequent Questions
          </span>
          <Grid container spacing={3} className="mt-2 frequent-questions-main-div">
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Single Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                Sign Into Your Account
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Single Seller
              </span>
            </Grid>
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Yatch Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Update My Profile
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Reset My Password
              </span>
            </Grid>
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Broker Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Broker Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Broker Seller
              </span>
            </Grid>
          </Grid>
        </Container>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  userGuideBanners: state.bannerReducer && state.bannerReducer["userGuideBanner"],
  userguides: state.userGuideReducer && state.userGuideReducer.userguides
});

const mapDispatchToProps = dispatch => ({
  getCategoriesWiseBanners: (data) => (dispatch(getCategoriesWiseBanners(data))),
  getAllUserGuide: data => dispatch(getAllUserGuide(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserGuide);
