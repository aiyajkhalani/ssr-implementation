import React, { Component } from "react";
import { DashboardLayout } from "../../components";
import "./ReviewBooking.scss";
import { Grid } from "@material-ui/core";
import { BoatInformationReviewBooking } from "../../components/reviewBookingComponent/BoatInformationReviewBooking";
import { OtherBoatInformation } from "../../components/reviewBookingComponent/OtherBoatInformation";

class ReviewBooking extends Component {
  render() {
    return (
      <DashboardLayout>
        <div className="p-5">
          <div className="review-pay-head">Review and Pay</div>
          <Grid container className="p-5" spacing={3}>
            <Grid item sm={8}>
              <Grid container>
                <Grid item sm={12}>
                  <div className="mb-2 review-div-head-text">
                    Contrary to popular belief, Lorem Ipsum is not simply random
                    text.
                  </div>
                  <div className="review-div-text">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic
                  </div>
                </Grid>
                <Grid container className="mt-5">
                  <div className="block-heading mb-3">Boat Information</div>
                  <Grid item sm={12}>
                    <BoatInformationReviewBooking />
                  </Grid>
                </Grid>
                <Grid container className="mt-5">
                  <div className="block-heading mb-2">
                    Passenger Information
                  </div>
                  <Grid item sm={12} >
                    <div className="review-div-text">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    galley of type and scrambled it to make a type specimen
                    book. It has survived not only five centuries, but also the
                    leap into electronic
                    </div>
                    <div className="stepper-boat-info-id mt-2">Passengers: 10 Persons</div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item sm={4}>
            <Grid container>
                    <Grid item sm={12}>
                        <OtherBoatInformation/>
                    </Grid>
                </Grid>
            </Grid>
                
          </Grid>
        </div>
      </DashboardLayout>
    );
  }
}

export default ReviewBooking;
