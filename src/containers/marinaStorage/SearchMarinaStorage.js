import React, { Component } from 'react';
import { connect } from 'react-redux'

import { Layout, PaginationBar } from '../../components';
import { resultMessage, pagination } from '../../util/enums/enums';
import UserContext from '../../UserContext';
import BoatListingsWithMap from '../../components/gridComponents/BoatListingsWithMap';
import { searchMarinaStorage } from '../../redux/actions';


class SearchMarinaStorage extends Component {

    state = {
        showMap: false,
        values: null,
    }

    static contextType = UserContext

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    componentWillMount() {
        const { searchMarinaStorage, location } = this.props

        const urlParams = new URLSearchParams(location.search)

        let values = {}

        for (const [key, value] of urlParams.entries()) {
            values = { ...values, [key]: value }
        }

        this.setState({ values })

        searchMarinaStorage(values)
    }

    render() {

        const { searchedMarinaStorage, totalSearchedMarinaStorage, searchMarinaStorage } = this.props
        const { showMap, values } = this.state

        return (
            <Layout isFooter>
                <BoatListingsWithMap
                    boatsType="Searched Marina & Storage"
                    boatsTypeCount={totalSearchedMarinaStorage}
                    showMap={showMap}
                    isMarinaStorage
                    toggleMapHandler={this.toggleMapHandler}
                    boats={searchedMarinaStorage}
                    message={resultMessage.search}
                />
               {totalSearchedMarinaStorage > pagination.PAGE_RECORD_LIMIT &&
                <PaginationBar
                    action={searchMarinaStorage}
                    value={values}
                    totalRecords={totalSearchedMarinaStorage}
                />}
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    searchedMarinaStorage: state.marinaAndStorageReducer.searchedMarinaStorage,
    totalSearchedMarinaStorage: state.marinaAndStorageReducer.totalSearchedMarinaStorage,
})

const mapDispatchToProps = dispatch => ({
    searchMarinaStorage: data => dispatch(searchMarinaStorage(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchMarinaStorage) 