import React from "react";
import ItemsCarousel from "react-items-carousel";
import uuid from "uuid/v4";
import { isBrowser, isTablet } from "react-device-detect";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import ArrowForward from "@material-ui/icons/ArrowForward";
import ArrowBack from "@material-ui/icons/ArrowBack";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { connect } from "react-redux";

import UserContext from "../../UserContext";
import { makeStyles } from "@material-ui/core/styles";
import "../../components/home/home.scss";
import "../../styles/common.scss";
import { getLookUpImages } from "../../util/utility";
import { Grid } from "@material-ui/core";

export class MarinaStorageExplore extends React.Component {
  state = {
    children: [],
    activeItemIndex: 0,
    anchorEl: null,
    isTrue: false
  };

  static contextType = UserContext;

  handleClick = (isOpen, index) => {
    const { carouselData } = this.state;
    const newCarouselData = carouselData.map((carousel, carouselIndex) => {
      if (carouselIndex === index && !isOpen) {
        carousel.isOpen = true;
        return carousel;
      } else {
        carousel.isOpen = false;
        return carousel;
      }
    });
    this.setState({
      carouselData: newCarouselData
    });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  componentDidMount() {
    this.setState({
      children: [],
      activeItemIndex: 0,
      carouselData: [],
      customWidth: false
    });

    if (window.innerWidth < 1250) {
      this.setState({ customWidth: true });
    }
    this.setState({
      carouselData: this.props.carouselData
    });
  }

  createChildren = () => {
    const { carouselData, viewMarina } = this.props;

    return (
      carouselData &&
      carouselData.length &&
      carouselData.map((item, index) => {
        return (
          <Grid item xs={4}>
              <div key={uuid()} className="boat-box" onClick={() => viewMarina(item.alias)}>
                <div className="boat-box-shadow position-relative">
                  <div className="boatImage-div position-relative overflow-hidden mb-4">
                    <img
                      src={getLookUpImages(item)}
                      className="boat-img carousel-img-fix"
                      alt="img"
                    />
                  </div>
                  <div className="BoatCardText-cardContentPosition">
                    <h1 className="country-name">{item.alias}</h1>
                  </div>
                </div>
              </div>
          </Grid>
        );
      })
    );
  };

  changeActiveItem = activeItemIndex => this.setState({ activeItemIndex });

  render() {
    const { activeItemIndex, children, customWidth } = this.state;
    const { items, customWidthItem, btnColor } = this.props;
    return <Grid container>{this.createChildren()}</Grid>;
  }
}
