import React, { Component, Fragment } from "react";
import { Grid, Box } from "@material-ui/core";
import { connect } from "react-redux";

import {
  getTypeWiseLookup,
  GetRecentlyAddedMarinaAndStorage,
  getMostViewedMarinaStorage,
  searchMarinaStorage,
  getMoreServices,
  getTopRatedMarinaAndStorage,
  getExperiences
} from "../../redux/actions";
import { getHomeVideos, getModuleWiseBanners } from "../../redux/actions";
import { Layout } from "../../components/layout/layout";
import RegisterCard from "../../components/staticComponent/registerCard";
import ReviewCards from "../../components/gridComponents/reviewCards";

import "./marinaStorage.scss";
import { RecommendedMarinaStorages } from "../../components/gridComponents/recommendedMarinaStorages";
import { pagination, moduleCategory, dimension, mediaCategory, advertisementCategory, showAllMarina } from "../../util/enums/enums";
import { getCategoryWiseVideos } from "../../redux/actions/VideoAction";
import UserContext from "../../UserContext";
import { getCategoryWiseAdvertisements } from "../../redux/actions/advertisementAction";
import { MarinaStorageSearchCard } from "../../components/marinaStorage/marinaStorageSearchCard";
import ServicesMarinaStorage from "../../components/staticComponent/servicesMarinaStorage";
import MarinaMoreServices from "./marinaMoreServices";
import { getHeightRatio, getRatio } from "../../helpers/ratio";
import MarinaMostPopular from "../../components/marinaStorage/marinaMostPopular";
import TopRatedMarina from "../../components/gridComponents/topRatedMarina";
import { objectToQuerystring } from '../../helpers/routeHelper'
import { VideoModel } from "../../components/videoComponent/videoModel";
import { VideoComponent } from "../../components/videoComponent/videoComponent";

import '../../styles/marinaResponsive.scss';

const marinaGallery = {
  isBrowser: 4,
  isTablet: 2,
  isMobile: 1
};

class MarinaStorage extends Component {

  static contextType = UserContext

  state = {
    setVideoFlag: false,
    videoUrl: "",
    videoThumbnail: "",
    videoWidth: dimension.marinaStorageVideo.width,
    videoHeight: dimension.marinaStorageVideo.height,
    selectedIndex: null,

    queryResult: ""
  };

  searchMarinaStorageHandler = (values) => {
    const { searchMarinaStorage } = this.props

    this.setState({ queryResult: objectToQuerystring(values) })
    searchMarinaStorage(values)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isMarinaStorageSearched, history } = nextProps;
    const { queryResult } = prevState;

    if (isMarinaStorageSearched) {
      history.push(`/search-marina-storage${queryResult && queryResult}`);
    }
    return null;
  }

  async componentDidMount() {
    const { country } = this.context;

    const {
      getTypeWiseLookup,
      GetRecentlyAddedMarinaAndStorage,
      getCategoryWiseVideos,
      getMostViewedMarinaStorage,
      getCategoryWiseAdvertisements,
      getMoreServices,
      getTopRatedMarinaAndStorage,
      getModuleWiseBanners,
      getExperiences
    } = this.props;

    getCategoryWiseVideos({ type: moduleCategory.MARINA_STORAGE, metatype: 'video' });

    const videoWidth = getRatio(
      dimension,
      "marinaStorageVideo",
      ".section-heading"
    );

    const videoHeight = getHeightRatio(
      dimension,
      "marinaStorageVideo",
      ".section-heading"
    );

    this.setState({ videoWidth, videoHeight });

    getCategoryWiseAdvertisements(advertisementCategory.MARINA_STORAGE);

    await getTypeWiseLookup("You are An");
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: country
    };


    await GetRecentlyAddedMarinaAndStorage(data);
    await getMostViewedMarinaStorage(data);
    getExperiences("marina")
    getTopRatedMarinaAndStorage(data);
    getMoreServices();
    getModuleWiseBanners({ type: mediaCategory.marinaStorage.type, fieldName: mediaCategory.marinaStorage.fieldName, isBanner: true });
  }

  playVideo = video => {
    this.setState(prevState => ({
      setVideoFlag: !prevState.setVideoFlag,
      videoUrl: video.url,
      videoThumbnail: video.thumbnail
    }));
  };

  viewMarinaStorage = alias => {
    const { history } = this.props;
    history.push("/marina-storage-view", {
      alias: alias
    });
  };

  closeVideo = () => {
    this.setState({ setVideoFlag: false })
  }
  render() {
    const { country } = this.context
    const {
      recentMarinaStorage,
      categoryVideos,
      mostViewedMarinaStorage,
      marinaTypeWiseLookUps,
      advertisements,
      moreMarinaService,
      topRatedMarinaStorage,
      topRatedMarinaTotal,
      history,
      marinaStorageBanner,
      mostViewedMarinaStorageTotal,
      experience,
    } = this.props;
    const { setVideoFlag, videoUrl, videoWidth, videoHeight, videoThumbnail, selectedIndex } = this.state;

    return (
      <Fragment>
        <Layout isHeader="marina-storage-header-logo" className="marina-storage-layout">
          <MarinaStorageSearchCard
            marinaBanner={marinaStorageBanner}
            types={marinaTypeWiseLookUps}
            services={moreMarinaService}
            search={this.searchMarinaStorageHandler}
          />

          <div className="container100 marinaStorageMainDiv">
            <Grid container>
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Explore AdamSea
                </Box>
                <ServicesMarinaStorage className="mb-20" />
              </Grid>

              {moreMarinaService && moreMarinaService.length > 0 && (
                <Grid item xs={12}>
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    More Services We Do
                </Box>
                  <MarinaMoreServices
                    moreServices={moreMarinaService}
                    limit={18}
                  />
                </Grid>
              )}
              {recentMarinaStorage && recentMarinaStorage.length > 0 && (
                <Grid item xs={12} className="recommended-marina-div section-grid">
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    Recommended Marina & Storage Around You
                </Box>
                  <RecommendedMarinaStorages
                    recommendedMarinaStorages={recentMarinaStorage}
                    textColor="iconColor-marina"
                    showAllText="recommended marina & storage boats"
                  />
                </Grid>
              )}
              <Grid item xs={12}>
                <Grid container>
                  {advertisements && advertisements.length > 0 && (
                    <Grid item xs={12}>
                      <div>
                        <RegisterCard
                          adData={advertisements[0]}
                          history={history}
                        />
                      </div>
                    </Grid>
                  )}
                </Grid>
              </Grid>

              {mostViewedMarinaStorage && mostViewedMarinaStorage.length > 0 && (
                <Grid item xs={12}>
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    Most Popular Marina Facility
                </Box>
                  <>

                    <MarinaMostPopular
                      limit={8}
                      total={mostViewedMarinaStorageTotal}
                      showType={showAllMarina.mostPopular}
                      mostPopular={mostViewedMarinaStorage}
                      selectedIndex={selectedIndex}
                      showAllText="most popular marina facility"
                    />
                  </>
                </Grid>
              )}

              {advertisements && advertisements.length > 1 && (
                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <div className="double-card">
                        <RegisterCard isSpace isDouble adData={advertisements[1]} />
                      </div>
                    </Grid>
                    {advertisements && advertisements.length > 2 && (
                      <Grid item xs={6}>
                        <div className="double-card">
                          <RegisterCard isSpace2 isDouble adData={advertisements[2]} />
                        </div>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              )}
              {topRatedMarinaTotal > 0 &&
                <Grid item xs={12}>
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    Top Rated AdamSea Marina & Storage Services
                </Box>
                  <TopRatedMarina
                    topRatedMarina={topRatedMarinaStorage}
                    iconColor="iconColor-marina"
                    showType={showAllMarina.topRated}
                    total={topRatedMarinaTotal}
                    limit={10}
                    country={country}
                    showAllText="top rated marina & storage services"
                  />
                </Grid>
              }

              {/* remove below commented code after discuss with @miraj [NOTE] }
              {/* <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Reviews
                </Box>
                <ReviewCards
                  xs={12}
                  sm={6}
                  reviewCard={marinaReviews}
                  iconColor="iconColor-marina"
                />
              </Grid> */}
              {experience && experience.length > 0 && (
                <Grid item xs={12}>
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    Live the Experience with us
                </Box>
                  <ReviewCards
                    isMarina
                    xs={12}
                    sm={3}
                    experience={experience}
                  />
                </Grid>
              )}
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                ></Box>

                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    {categoryVideos &&
                      categoryVideos.length > 0 &&
                      categoryVideos.map((video, index) => {
                        if (index < 3) {
                          return (
                            <Fragment key={video.id}>
                              <VideoComponent
                                setVideoUrlHandler={() => this.playVideo(video)}
                                video={video}
                                videoWidth={videoWidth}
                                videoHeight={videoHeight}
                              />
                            </Fragment>
                          );
                        }
                      })}
                  </Grid>
                  <VideoModel
                    videoFlag={setVideoFlag}
                    videoUrl={videoUrl}
                    thumbnail={videoThumbnail}
                    closeVideo={this.closeVideo}
                  />
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Layout>
      </Fragment>
    );
  }
}

// export default ;

const mapStateToProps = state => ({
  videos: state.dashboardReducer.videos,
  typeWiseLookup:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaTypeWiseLookUps,
  user: state.loginReducer && state.loginReducer.currentUser,
  categoryVideos: state.videoReducer && state.videoReducer.categoryVideos,
  recentMarinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.recentMarinaStorage,
  mostViewedMarinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.mostViewedMarinaStorage,
  marinaTypeWiseLookUps: state.marinaAndStorageReducer.marinaTypeWiseLookUps,
  isMarinaStorageSearched:
    state.marinaAndStorageReducer.isMarinaStorageSearched,
  advertisements: state.advertisementReducer.advertisements,
  moreMarinaService:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.moreMarinaService,
  topRatedMarinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.topRatedMarinaStorage,
  topRatedMarinaTotal:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.topRatedMarinaTotal,
  marinaStorageBanner: state.dashboardReducer && state.dashboardReducer[mediaCategory.marinaStorage.fieldName],
  mostViewedMarinaStorageTotal:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.mostViewedMarinaStorageTotal,
  experience: state.dashboardReducer.experience,


});

const mapDispatchToProps = dispatch => ({
  getHomeVideos: () => dispatch(getHomeVideos()),
  getMoreServices: () => dispatch(getMoreServices()),
  getTypeWiseLookup: data => dispatch(getTypeWiseLookup(data)),
  GetRecentlyAddedMarinaAndStorage: data =>
    dispatch(GetRecentlyAddedMarinaAndStorage(data)),
  getCategoryWiseVideos: data => dispatch(getCategoryWiseVideos(data)),
  getMostViewedMarinaStorage: data =>
    dispatch(getMostViewedMarinaStorage(data)),
  searchMarinaStorage: data => dispatch(searchMarinaStorage(data)),
  getCategoryWiseAdvertisements: data =>
    dispatch(getCategoryWiseAdvertisements(data)),
  getTopRatedMarinaAndStorage: data =>
    dispatch(getTopRatedMarinaAndStorage(data)),
  getModuleWiseBanners: type => dispatch(getModuleWiseBanners(type)),
  getExperiences: data => dispatch(getExperiences(data)),

});

export default connect(mapStateToProps, mapDispatchToProps)(MarinaStorage);
