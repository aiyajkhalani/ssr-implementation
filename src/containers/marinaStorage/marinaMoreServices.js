import React, { Component, Fragment } from "react";

import UserContext from "../../UserContext";

import { getRatio, getMarginRatio } from "../../helpers/ratio";
import { dimension } from "../../util/enums/enums";
import { MarketServiceDetail } from "../../components/marketService/marketServiceDetail";

class MarinaMoreServices extends Component {
  static contextType = UserContext;
  state = {
    imageRatio: 230,
    margin: 20,
  };

  componentDidMount() {
    const imageRatio = getRatio(dimension, 'marinaMoreServices', ".section-heading");
    const margin = getMarginRatio(5, '.more-marina-service')
    this.setState({ imageRatio, margin });
  }

  listingMarinaStorageByService = value => {

    value && window && window.open(`/marina-storage-service-view/${value}`, '_blank')

  };

  render() {

    const { imageRatio } = this.state;
    const { limit, moreServices } = this.props;
    return (
      <div className="more-marina-service">
        {moreServices &&
          moreServices.length &&
          moreServices.map((service, index) => {
            return (
              <Fragment key={service.id}>
                {index < limit && (
                  <MarketServiceDetail
                    value={service}
                    getService={() => this.listingMarinaStorageByService(service.id)}
                    className="more-marina-content"
                    height={imageRatio}
                  />)}
              </Fragment>
            );
          })}
      </div>
    );
  }
}

export default MarinaMoreServices;
