import React, { Component } from "react";
import { connect } from "react-redux";

import { Layout, PaginationBar } from "../../components";
import { getExploreMarina } from "../../redux/actions/marinaAndStorageAction";
import UserContext from "../../UserContext";
import { pagination } from "../../util/enums/enums";
import { getMarinaType } from "../../helpers/marinaHelper";
import BoatListingsWithMap from "../../components/gridComponents/BoatListingsWithMap";

class MarinaStorageView extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      alias:
        props.location && props.location.state && props.location.state.alias,
    };
  }

  async componentDidMount() {
    const { country } = this.context;
    const { getExploreMarina } = this.props;
    const data = {
      country: country,
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      typeId: await this.getType()
    };
    getExploreMarina(data);
  }

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  getType = () => {
    const { alias } = this.state;
    const { typeWiseLookup }= this.props
    const lookUp =
      typeWiseLookup && typeWiseLookup.length && typeWiseLookup.find(item => item.alias === alias);
    return getMarinaType(lookUp);
  };

  render() {
    const { getExploreMarina, exploreMarina,totalExploreMarina } = this.props;
    const { showMap } = this.state;
    const { country } = this.context;

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            isMarinaStorage
            showMap={showMap}
            toggleMapHandler={this.toggleMapHandler}
            boats={exploreMarina}
          />

          {totalExploreMarina > pagination.PAGE_RECORD_LIMIT && (
            <PaginationBar
              action={getExploreMarina}
              value={{ country, typeId: this.getType() }}
              totalRecords={totalExploreMarina}
            />
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  exploreMarina:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.exploreMarina,
  totalExploreMarina:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.totalExploreMarina,
    typeWiseLookup:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaTypeWiseLookUps,
});

const mapDispatchToProps = dispatch => ({
  getExploreMarina: data => dispatch(getExploreMarina(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(MarinaStorageView);
