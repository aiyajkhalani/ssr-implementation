import React, { Component } from "react";
import { connect } from "react-redux";
import ReactTable from "react-table";

import { Layout, DashboardLayout } from "../../components";
import { verifiedCheck, readableString } from "../../helpers/string";

import "react-table/react-table.css";
import "../../styles/common.scss";
import { getAllAuctionBids } from "../../redux/actions";
import { pagination } from "../../util/enums/enums";
import "../../styles/manageDashboardTableResponsive.scss";
import { priceFormat } from "../../util/utilFunctions";

class ManageAuctionBids extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auctionId:
        props.location && props.location.state && props.location.state.auctionId
    };
  }
  componentDidMount() {
    const { getAllAuctionBids } = this.props;
    const { auctionId } = this.state;

    if (auctionId) {
      const data = {
        page: pagination.PAGE_COUNT,
        limit: pagination.PAGE_RECORD_LIMIT,
        auctionId: auctionId
      };
      getAllAuctionBids(data);
    }
  }
  render() {
    const columns = [
      {
        Header: "Sr No.",
        Cell: ({ index }) => index + 1
      },
      {
        Header: "User",
        accessor: "user",
        Cell: data => <span >{`${data.original.user.firstName} ${data.original.user.lastName}`} </span>

      },
      {
        Header: "Price",
        accessor: "price",
        Cell: data => <span>{priceFormat(data.value)} </span>
      },
      {
        Header: "Status",
        accessor: "status",
        Cell: data => <span className={`bg-green-color font-13 text-capitalize m-auto ${verifiedCheck(data.row.adStatus)}`} >{verifiedCheck(data.original.status)} </span>
      },
    ];

    const { allBids } = this.props;
    return (
      <DashboardLayout>
        <div className="row m-0 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Auction Room Bids</h4>
              </div>
              <div className="card-body mt-0">
                <div className="table-responsive">
                  {allBids && allBids.length > 0 && (
                    <ReactTable
                      columns={columns}
                      data={allBids}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                      className="-striped -highlight"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  allBids: state.boatReducer && state.boatReducer.allBids
});

const mapDispatchToProps = dispatch => ({
  getAllAuctionBids: data => dispatch(getAllAuctionBids(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageAuctionBids);
