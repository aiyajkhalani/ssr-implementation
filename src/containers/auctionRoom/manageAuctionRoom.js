import React, { Component } from "react";
import { connect } from "react-redux";
import ReactTable from "react-table";

import { Layout } from "../../components";
import { verifiedCheck, readableString, verifiedCheckAuction } from "../../helpers/string";
import UserContext from "../../UserContext";

import "react-table/react-table.css";
import "../../styles/common.scss";
import { getUserAuctionRooms } from "../../redux/actions";
import { pagination } from "../../util/enums/enums";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import "../../styles/manageDashboardTableResponsive.scss";
import { priceFormat } from "../../util/utilFunctions";

class ManageAuctionRoom extends Component {
  componentDidMount() {
    const { getUserAuctionRooms } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    };
    getUserAuctionRooms(data);
  }
  static contextType = UserContext;
  render() {
    const { history } = this.context
    const columns = [
      {
        Header: "Boat Name",
        accessor: "boatName",
        Cell: data => <span>{data.original.boat.boatName} </span>
      },
      {
        Header: "Ad ID",
        accessor: "adId"
      },
      {
        Header: "Boat Price",
        accessor: "Original Price",
        Cell: data => <span>{data.original.boat && priceFormat(data.original.boat.price)} </span>
      },

      {
        Header: "Ad Status",
        accessor: "adStatus",
        Cell: data => (
          <>
          
            {data.original.status === "Approve" ? (
              <span className={`Verified text-capitalize m-auto `}>
               Verified
              </span>
            ) : data.original.status === "Decline" ? (
              <span className={`Decline text-capitalize m-auto `}>
                Decline
              </span>
            ) : 
              <span className={`Unverified text-capitalize m-auto `}>
                Unverified
              </span> }
          </>
        )
      },
      {
        Header: "Actions",
        Cell: data => (
          <div className="d-flex flex-row justify-content-around">
           {data.original.status &&  <button
              type="button"
              onClick={() =>
                history.push("/manage-auction-bids", {
                  auctionId: data.original.id
                })
              }
              className="btn btn-outline-info"
            >
              View Bids
            </button>}
          </div>
        )
      }
    ];

    const { userAuctions } = this.props;
    return (
      <DashboardLayout>
        <div className="row pl-3 pr-3 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Manage Auction Rooms</h4>
              </div>
              <div className="card-body mt-0">
                <div className="table-responsive">
                  {userAuctions && userAuctions.length && (
                    <ReactTable
                      columns={columns}
                      data={userAuctions}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                      className="-striped -highlight"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  userAuctions: state.boatReducer && state.boatReducer.userAuctions
});

const mapDispatchToProps = dispatch => ({
  getUserAuctionRooms: data => dispatch(getUserAuctionRooms(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageAuctionRoom);
