import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Layout, PaginationBar } from '../../components'
import { getBoatByType } from '../../redux/actions'
import UserContext from "../../UserContext";
import { pagination } from '../../util/enums/enums'
import BoatListingsWithMap from '../../components/gridComponents/BoatListingsWithMap';


class ShowAll extends Component {

    state = {
        type: '',
        fieldName: '',
        showMap: false,
        name: '',
        isFlag: true,
        isData: true,
    }

    static contextType = UserContext

    static getDerivedStateFromProps(nextProps, prevState) {

        const { isFlag, isData, type } = prevState
        const { match } = nextProps
        const { params } = match && match

        if (params && params.hasOwnProperty('type') && params.type && isFlag) {
            return {
                type: params.type,
                isFlag: false
            }
        }

        if (isData && type) {
            switch (type) {
                case 'featured-boats':
                    return { name: 'Feature Boats', isData: false }

                case 'must-buy-boats':
                    return { name: 'Must Buy Boats', isData: false }

                case 'best-deal-boats':
                    return { name: 'Best Deal Recommended Boats', isData: false }

                default:
                    return
            }
        }

        return null
    }

    componentDidMount() {
        const { type } = this.state
        type && this.checkType(type)
    }

    checkType = (type) => {
        const { getBoatByType } = this.props
        const { country } = this.context

        switch (type) {
            case 'featured-boats':
                getBoatByType({ input: { country, fieldName: "featureStatus" }, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
                this.setState({ fieldName: 'featureStatus' });
                break;
            case 'must-buy-boats':
                getBoatByType({ input: { country, fieldName: "mustBuyStatus" }, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
                this.setState({ fieldName: 'mustBuyStatus' });
                break;
            case 'best-deal-boats':
                getBoatByType({ input: { country, fieldName: "bestDealStatus" }, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
                this.setState({ fieldName: 'bestDealStatus' });
                break;
            default:
                break
        }
    }



    fetchTotalRecord = () => {
        const { type } = this.state
        const { totalFeatureBoats, totalBestDealBoats, totalMustBuyBoats } = this.props

        switch (type) {
            case 'featured-boats':
                return totalFeatureBoats
            case 'must-buy-boats':
                return totalMustBuyBoats
            case 'best-deal-boats':
                return totalBestDealBoats
            default:
                return
        }
    }

    fetchData = () => {
        const { type } = this.state
        const { featureBoats, mustBuyBoats, bestDealBoats } = this.props

        switch (type) {
            case 'featured-boats':
                return featureBoats
            case 'must-buy-boats':
                return mustBuyBoats
            case 'best-deal-boats':
                return bestDealBoats
            default:
                return
        }
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {
        const { getBoatByType } = this.props
        const { country } = this.context

        const { fieldName, showMap, name } = this.state
        return (
            <Layout isFooter>
                <div className={`${showMap ? 'is-show-map' : ''} w-100`}>
                    <BoatListingsWithMap
                        boatsTypeCount={this.fetchTotalRecord()}
                        boatsType={name}
                        showMap={showMap}
                        toggleMapHandler={this.toggleMapHandler}
                        boats={this.fetchData()}
                    />
                    {this.fetchTotalRecord() > pagination.PAGE_RECORD_LIMIT &&
                    <PaginationBar
                        action={getBoatByType}
                        value={{ input: { country, fieldName } }}
                        totalRecords={this.fetchTotalRecord()}
                    />}
                </div>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    featureBoats: state.boatReducer.featureBoats,
    bestDealBoats: state.boatReducer.bestDealBoats,
    mustBuyBoats: state.boatReducer.mustBuyBoats,
    totalFeatureBoats: state.boatReducer.totalFeatureBoats,
    totalBestDealBoats: state.boatReducer.totalBestDealBoats,
    totalMustBuyBoats: state.boatReducer.totalMustBuyBoats,
})

const mapDispatchToProps = dispatch => ({
    getBoatByType: data => dispatch(getBoatByType(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ShowAll) 