import React, { Component } from "react";
import { connect } from "react-redux";

import { Layout, PaginationBar } from "../../components";
import {
  GetRecentlyAddedMarinaAndStorage,
  getMostViewedMarinaStorage,
  getTopRatedMarinaAndStorage
} from "../../redux/actions";
import UserContext from "../../UserContext";
import { pagination, showAllMarina } from "../../util/enums/enums";
import BoatListingsWithMap from "../../components/gridComponents/BoatListingsWithMap";

class ShowAllMarinaAndStorage extends Component {
  static contextType = UserContext;

  state = {
    country: "",
    city: "",
    showMap: false,
    showAllData: {},
    type: "",
    action: null,
    total: 0,
    isAuction: false,
    name: "Marina Boats",
    data: {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context.country
    }
  };

  static getDerivedStateFromProps(nextProps, state) {
    const { match } = nextProps;
    const { params } = match && match;

    if (params && params.type) {
      return {
        type: params.type
      };
    }


    return null;
  }

  componentDidMount() {
    const { type } = this.state;
    const showAllData = this.typeWiseData(type);
    this.setState({
      showAllData: showAllData.data,
      action: showAllData.action,
      total: showAllData.total,
      isAuction: showAllData.isAuction,
      name: showAllData.name
    });
  }

  typeWiseData = type => {
    const {
      recentMarinaStorage,
      mostViewed,
      topRatedMarinaStorage,
      totalRecentMarinaCount,
      mostViewedMarinaStorageTotal,
      topRatedMarinaTotal,
      GetRecentlyAddedMarinaAndStorage,
      getMostViewedMarinaStorage,
      getTopRatedMarinaAndStorage
    } = this.props;
    const { data } = this.state;

    switch (type) {
      case showAllMarina.recommended:
        GetRecentlyAddedMarinaAndStorage(data);
        return {
          data: recentMarinaStorage,
          action: GetRecentlyAddedMarinaAndStorage,
          total: totalRecentMarinaCount,
          name: "Recommended Marina",
          isAuction: true
        };

      case showAllMarina.mostPopular:
        getMostViewedMarinaStorage(data);
        return {
          data: mostViewed,
          action: getMostViewedMarinaStorage,
          total: mostViewedMarinaStorageTotal,
          name: "Most Popular Marina",
          isAuction: false
        };

      case "topRated":
        getTopRatedMarinaAndStorage(data);
        return {
          data: topRatedMarinaStorage,
          action: getTopRatedMarinaAndStorage,
          total: topRatedMarinaTotal,
          name: "Top Rated Marina",
          isAuction: false
        };

      default:
        return;
    }
  };

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  getData = (type) => {

    switch (type) {
      case showAllMarina.recommended:
     
        return this.props.recentMarinaStorage
      case showAllMarina.mostPopular:
          return this.props.mostViewed
      case "topRated":
          return this.props.topRatedMarinaStorage

          

      default:
        return;
    }
  }

  render() {
    const { country } = this.context;
    const { showMap, action, total, isAuction, name, type } = this.state;

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            isMarinaStorage
            boatsTypeCount={total}
            boatsType={name}
            isShowAuction={isAuction}
            showMap={showMap}
            toggleMapHandler={this.toggleMapHandler}
            boats={this.getData(type)}
          />

          {total > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination mt-50">
              <PaginationBar
                action={action}
                value={{ country }}
                totalRecords={total}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  user: state.loginReducer && state.loginReducer.currentUser,
  recentMarinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.recentMarinaStorage,
  totalRecentMarinaCount:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.totalRecentMarinaCount,
  mostViewed:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.mostViewedMarinaStorage,
  mostViewedMarinaStorageTotal:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.mostViewedMarinaStorageTotal,
  topRatedMarinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.topRatedMarinaStorage,
  topRatedMarinaTotal:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.topRatedMarinaTotal
});

const mapDispatchToProps = dispatch => ({
  GetRecentlyAddedMarinaAndStorage: data =>
    dispatch(GetRecentlyAddedMarinaAndStorage(data)),
  getMostViewedMarinaStorage: data =>
    dispatch(getMostViewedMarinaStorage(data)),
  getTopRatedMarinaAndStorage: data =>
    dispatch(getTopRatedMarinaAndStorage(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowAllMarinaAndStorage);
