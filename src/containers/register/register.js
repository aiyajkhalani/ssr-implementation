import React, { Component } from "react";
import uuid from "uuid/v4";
import { connect } from "react-redux";
import { Grid, Modal, Fade, Backdrop, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FaEyeSlash } from "react-icons/fa";
import { FaEye } from "react-icons/fa";
import { IoMdPerson } from "react-icons/io";
import { MdEmail } from "react-icons/md";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import publicIp from "public-ip";
import { Spinner } from "react-bootstrap"

import {
  register,
  clearAuthorizationFlag,
  getUserRoles,
  setCurrentLocation,
  getAllPageInformationByType,
  getUserLocation,
  getCategoriesWiseBanners,
  clearErrorMessageShow
} from "../../redux/actions";
import { ErrorNotify } from "../../helpers/notification";
import { getGeoLocation } from "../../helpers/geoLocation";
import { Notification, Field } from "../../components";
import { userRoles, preferenceOptions, latLng } from "../../util/enums/enums";
import TermAndPolicy from "../../components/termAndPolicy";
import { renderErrorMessage, getClientIp } from "../../helpers/jsxHelper";
import { requireMessage } from "../../helpers/string";
import ErrorComponent from "../../components/error/errorComponent";
import GoogleMap from "../../components/map/map";

import "../../styles/common.scss";
import "./register.scss";
import "../../styles/registerResponsive.scss";
import { LogInSignupBanner } from "../../components/styleComponent/styleComponent";
import { getAllPageInfoByType, clearPageInfoByTypeFlag } from "../../redux/actions/pageInfoByTypeAction";
import { handleSingleFileDelete, handleSingleFileUpload } from "../../helpers/s3FileUpload";
import Captcha from "../../components/helper/captcha";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newUser: {
        country: "",
        role: "",
        agreementId: "",
        companyName: "",
        companyLogo: "",
        firstName: "",
        lastName: "",
        email: "",
        mobileNumber: "",
        password: "",
        address: "",
        roleAlias: "",
        captchaValue: "",
      },

      agree: null,

      type: "password",
      wording: <FaEye />,

      termsModal: false,
      privacyModal: false,
      errorMessages: {},

      isCheckedAgreementId: false,
      openMapDialog: false,

      isDisplayHint: false,

      resultLocation: {},
      isLoading: false,

    };
  }

  static async getDerivedStateFromProps(nextProps, prevState) {
    const {
      isAuthorized,
      history,
      clearAuthFlag,
      isError,
      location,
      roles,
      currentLocation
    } = nextProps;
    const { newUser } = prevState;

    if (isAuthorized) {
      history.push("/");
      clearAuthFlag();
    } else if (
      location &&
      location.search
    ) {
      const urlParams = new URLSearchParams(location.search);
      const role = urlParams.has("type") && roles.find(item => item.aliasName === urlParams.get("type"));

      if (role) {
        newUser.role = role.id;
        newUser.roleType = role.role;
        newUser.roleAlias = role.aliasName;
        newUser.userType = role.aliasName;
        newUser.country = currentLocation;

        return { newUser };
      }

    } else if (roles && roles.length) {
      const role = roles.find(item => item.aliasName === userRoles.MEMBER);
      if (role) {
        newUser.role = role.id;
        newUser.roleType = role.role;
        newUser.roleAlias = role.aliasName;
        newUser.userType = role.aliasName;
        newUser.country = currentLocation;

        return { newUser };
      }
    }

    return null;
  }

  createMarkup = data => {
    return { __html: data };
  };

  async componentDidMount() {
    const { getUserLocation, clearErrorMessageShow } = this.props;
    const { newUser } = this.state;
    const { country } = newUser;

    clearErrorMessageShow()
    if (!country) {
      const ip = await publicIp.v4()
      // getGeoLocation(this.fetchCountry, true);
      getUserLocation({ ip })
    }

    const { getUserRoles, getAllPageInformationByType, getCategoriesWiseBanners } = this.props;
    await getUserRoles();
    await getAllPageInformationByType({
      input: { module: /* newUser.userType */ "member" }
    });
    const singUpInput = {
      type: "signUp",
      fieldName: "signUpBanner"
    }
    await getCategoriesWiseBanners(singUpInput)
    // await getAllPageInformationByType( {input :{}});
  }

  // fetchCountry = response => {
  //   const { country } = response;
  //   const { newUser } = this.state;
  //   const { setCurrentLocation } = this.props;

  //   newUser.country = country;
  //   this.setState({ newUser });
  //   setCurrentLocation(country);
  // };

  termsHandler = () => {
    const { getAllPageInfoByType } = this.props
    getAllPageInfoByType({ title: "terms-of-use-731" });
    this.setState(prevState => ({
      termsModal: !prevState.termsModal
    }));
  };

  privacyPolicyHandler = () => {
    const { getAllPageInfoByType } = this.props
    getAllPageInfoByType({ title: "privacy-policy-518" });
    this.setState(prevState => ({
      privacyModal: !prevState.privacyModal
    }));
  };

  changeState = () => {
    const { type } = this.state;

    const newType = type === "password" ? "text" : "password";
    const newWord = type === "password" ? <FaEyeSlash /> : <FaEye />;

    this.setState({ type: newType, wording: newWord });
  };

  onClickAgree = () => {
    this.setState(prevState => ({
      agree: !prevState.agree,
    }));
  };

  checkUserRolesForMobile = newUser => {
    const selectedUserRoles = [
      userRoles.BOAT_MANUFACTURER,
      userRoles.SURVEYOR,
      userRoles.YACHT_SHIPPER,
      userRoles.MEMBER
    ];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  checkUserRolesForCompanyLogo = newUser => {
    const selectedUserRoles = [
      userRoles.BOAT_MANUFACTURER,
      userRoles.SURVEYOR,
      userRoles.YACHT_SHIPPER
    ];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  checkUserRolesOfficeLocation = newUser => {
    const selectedUserRoles = [userRoles.SURVEYOR];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  checkUserRolesForPreference = newUser => {
    const selectedUserRoles = [userRoles.RENT_AND_CHARTER];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  checkUserRolesForAgreementId = newUser => {
    const selectedUserRoles = [
      userRoles.BOAT_MANUFACTURER,
      userRoles.YACHT_SHIPPER,
      userRoles.SURVEYOR
    ];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  checkUserRolesForCompanyName = newUser => {
    const selectedUserRoles = [
      userRoles.BROKER_AND_DEALER,
      userRoles.BOAT_MANUFACTURER,
      userRoles.SERVICE_AND_MAINTENANCE,
      userRoles.MARINA_AND_STORAGE,
      userRoles.YACHT_SHIPPER,
      userRoles.SURVEYOR
    ];

    return selectedUserRoles.includes(newUser.roleAlias);
  };

  agreementIdCheckHandler = () => {
    this.setState(prevState => ({
      isCheckedAgreementId: !prevState.isCheckedAgreementId
    }));
  };

  fetchMapInfo = (result, setValue) => {
    const {
      address,
      country,
      state,
      city,
      placeName,
      postalCode,
      route,
      latitude,
      longitude
    } = result;

    setValue("address", address);
    // setValue("latitude", latitude);
    // setValue("longitude", longitude);
    setValue("city", city);
    setValue("state", state);
    setValue("country", country);
    setValue("route", route);
    setValue("placeName", placeName);
    setValue("postalCode", postalCode);
    setValue("geometricLocation", {
      coordinates: [longitude, latitude]
    })

    this.setState({ latitude, longitude })
    this.setState({ resultLocation: result })
  };

  mapHandler = () => {
    this.setState(preState => ({ openMapDialog: !preState.openMapDialog }));
  };

  validate = (e, setValue, email) => {
    const { errorMessages } = this.state;
    let password = e.target.value;
    let capsCount,
      smallCount,
      symbolCount,
      numberCount,
      userEmail,
      uniqPassword;
    if (password.length < 8) {
      errorMessages.passwordLength = true;
    } else {
      errorMessages.passwordLength = false;
    }
    userEmail = email.toLowerCase();
    uniqPassword = password.toLowerCase();
    capsCount = (password.match(/[A-Z]/g) || []).length;
    smallCount = (password.match(/[a-z]/g) || []).length;
    symbolCount = (password.match(/\W/g) || []).length;
    numberCount = (password.match(/[0-9]/g) || []).length;

    if (capsCount < 1) {
      errorMessages.capsCount = true;
    } else {
      errorMessages.capsCount = false;
    }
    if (smallCount < 1) {
      errorMessages.smallCount = true;
    } else {
      errorMessages.smallCount = false;
    }
    if ((symbolCount && numberCount) < 1) {
      errorMessages.symbolOrNumberCount = true;
    } else {
      errorMessages.symbolOrNumberCount = false;
    }

    this.setState({
      errorMessages
    });
    this.measureStrength(password, setValue);
  };

  measureStrength = (password, setValue) => {
    let score = 0;
    let passwordStrength;
    let regexPositive = ["[A-Z]", "[a-z]", "[0-9]", "\\W"];
    regexPositive.forEach((regex, index) => {
      if (new RegExp(regex).test(password)) {
        score += 1;
      }
    });
    switch (score) {
      case 1:
        passwordStrength = "poor";
        break;

      case 2:
        passwordStrength = "poor";
        break;

      case 3:
        passwordStrength = "average";
        setValue("password", password);
        break;

      case 4:
        passwordStrength = "strong";
        setValue("password", password);
        break;

      default:
        passwordStrength = "weak";
    }
    this.setState({
      passwordStrength,
      isDisplayHint: true
    });
  };


  getRoleWiseValidations = (newUser) => {
    return newUser.roleAlias === userRoles.MEMBER ?
      this.memberValidations() :
      (newUser.roleAlias === userRoles.BROKER_AND_DEALER ||
        newUser.roleAlias === userRoles.SERVICE_AND_MAINTENANCE ||
        newUser.roleAlias === userRoles.MARINA_AND_STORAGE) ?
        this.brokerValidations() :
        (newUser.roleAlias === userRoles.BOAT_MANUFACTURER ||
          newUser.roleAlias === userRoles.YACHT_SHIPPER) ?
          this.manufacturerValidations() :
          newUser.roleAlias === userRoles.SURVEYOR ?
            this.surveyorValidations() :
            {};
  }

  memberValidations = () => {
    return {
      mobileNumber: Yup.string().required("Mobile Number is required"),
    }
  }

  brokerValidations = () => {
    return {
      companyName: Yup.string().required("Company Name is required"),
    }
  }

  manufacturerValidations = () => {
    return {
      companyName: Yup.string().required("Company Name is required"),
      mobileNumber: Yup.string().required("Mobile Number is required"),
      companyLogo: Yup.string().required("Company Logo is required"),
    }
  }

  surveyorValidations = () => {
    const rules = this.manufacturerValidations()
    return {
      ...rules,
      address: Yup.string().required("Address is required"),
    }
  }

  agreementValidations = () => {
    return {
      agreementId: Yup.string().required("Agreement Id  is required")
    }
  }

  handleSingleFile = async (files, name, setFieldValue) => {
    this.setState({ isLoading: true })
    await handleSingleFileUpload(files, name, setFieldValue)
    this.setState({ isLoading: false })
  }

  render() {
    const {
      newUser,
      agree,
      passwordStrength,
      errorMessages,
      isCheckedAgreementId,
      type,
      wording,
      termsModal,
      privacyModal,
      openMapDialog,
      isDisplayHint,
      latitude,
      longitude,
      resultLocation,
      isLoading,
    } = this.state;
    const { register, isError, errorMessage, infoList, signUpBanners, clearErrorMessageShow, infoData } = this.props;
    const roleValidations = this.getRoleWiseValidations(newUser);
    const agreement = this.checkUserRolesForAgreementId(newUser)
    const agreementValidations = (agreement && isCheckedAgreementId) ? this.agreementValidations() : {}
    return (
      <>
        <Notification />
        <Grid
          container
          className="flex-1 overflow-hidden h100-vh login-responsive regi-responsive register-responsive"
        >
          <Grid
            item
            className="height-inherit img-holder register-background-img"
            sm={6}
          >
            <LogInSignupBanner img={signUpBanners && signUpBanners.length ? encodeURI(signUpBanners[0].url) : null}>
              <Grid container className="flex-1" alignItems="center">
                <div className="website-logo logo-left-margin">
                  <Link to="/">
                    <div className="logo">
                      <img
                        className="logo-size"
                        src={require("../../assets/images/login/logo-white.png")}
                        alt="logo"
                      />
                    </div>
                  </Link>
                </div>
                <Grid item sm={12}>
                  <div className="d-flex justify-content-center align-items-start flex-column registerMain-div">
                    <div className="d-flex justify-content-center align-items-center flex-column">
                      <h3 className="text-white registration-free-text"> Registration Free</h3>
                      <div className="d-flex align-self-start flex-column registrationFree-div">
                        <span className="text-white registrationJoin-font website-join-text">
                          Join us TODAY to start buying
                      </span>
                        <span className="text-left text-white registrationWorldWide-text website-join-text">
                          boats worldwide.
                      </span>
                      </div>
                    </div>

                    <div className="d-flex float-left flex-column">
                      {infoList && infoList.length
                        ? infoList.map((data, index) => (
                          <div
                            key={uuid()}
                            className="d-flex justify-content-start align-items-center registration-infoDiv"
                          >
                            <span className="text-left registrationDiv">
                              {index + 1}
                            </span>
                            <span>
                              <Link
                                to="#"
                                className="text-left text-white Font12"
                              >
                                {data.header}
                              </Link>
                            </span>
                          </div>
                        ))
                        : null}
                    </div>

                    <TermAndPolicy className="register-terms-policy" />
                  </div>
                </Grid>
              </Grid>
            </LogInSignupBanner>
          </Grid>

          <Grid item sm={6} className="overflow-auto h-100">
            <Formik
              initialValues={{ ...newUser, agreementId: "" }}
              validationSchema={Yup.object().shape({
                firstName: Yup.string().required("First Name is required"),
                lastName: Yup.string().required("Last Name is required"),
                email: Yup.string()
                  .email("Email is invalid")
                  .required("Email is required"),
                password: Yup.string().required(requireMessage("Password")),
                captchaValue: Yup.string().required("Captcha Validation is required"),
                ...roleValidations,
                ...agreementValidations,
              })}
              onSubmit={values => {
                clearErrorMessageShow()
                const { errorMessages, passwordStrength, agree } = this.state;

                if (agree === null) {
                  this.setState({ agree: false })
                }
                if (agree) {
                  delete values.roleType;
                  delete values.roleAlias;
                  delete values.captchaValue;

                  if (
                    !errorMessages.symbolOrNumberCount &&
                    !errorMessages.passwordLength &&
                    (passwordStrength !== "poor" || passwordStrength !== "weak")
                  ) {
                    register(values);
                  }
                }
              }}
              render={({
                errors,
                touched,
                setFieldValue,
                values,
                handleSubmit
              }) => (
                  <div className="form-holder register-form-holder">
                    <div className="form-content form-text-fix">
                      <div className="form-items">
                        <h3>Register</h3>
                        <p>{newUser && newUser.roleType}</p>
                        <div className="page-links">
                          <p className="login-link-ragister-page">
                            Already have an AdamSea account?
                          <Link to="/login" className="font-weight-500">
                              {" Log in"}
                            </Link>
                          </p>
                        </div>
                        <Form>
                          <Grid container spacing={2} className="register-input-field">
                            {this.checkUserRolesForAgreementId(newUser) && (
                              <Grid item xs={12}>
                                <div className="d-flex align-center">

                                  <label for="agreement" className="agreement-label">Do you have agreement ID ? </label>
                                  <input
                                    id="agreement"
                                    type="checkbox"
                                    onChange={this.agreementIdCheckHandler}
                                  />
                                </div>

                                {isCheckedAgreementId && (
                                  <div className="position-relative">
                                    <input
                                      name="agreementId"
                                      id="agreementId"
                                      type="text"
                                      className="form-control font-16"
                                      placeholder="Agreement ID"
                                      onChange={e =>
                                        setFieldValue(
                                          "agreementId",
                                          e.target.value
                                        )
                                      }
                                      required
                                    ></input>
                                    <span className="form-side-icon">
                                      {/* <MdEmail /> */}
                                    </span>
                                    <ErrorMessage
                                      component="div"
                                      name="agreementId"
                                      className="error-message"
                                    />
                                  </div>
                                )}
                              </Grid>
                            )}

                            {this.checkUserRolesForPreference(newUser) && (
                              <Grid item xs={12}>
                                <div className="position-relative">
                                  <Field
                                    label="Provider"
                                    id={"provider"}
                                    name={"provider"}
                                    type="radio"
                                    value={values.provider}
                                    onChangeText={e => {
                                      setFieldValue("provider", e.target.value);
                                    }}
                                    options={preferenceOptions}
                                    flex
                                  />
                                  {errors.provider && (
                                    <div className="text-danger fontSize12">
                                      {errors.provider}
                                    </div>
                                  )}
                                </div>
                              </Grid>
                            )}

                            {this.checkUserRolesForCompanyName(newUser) && (
                              <Grid item xs={12}>
                                <div className="position-relative">
                                  <input
                                    name="companyName"
                                    id="companyName"
                                    type="text"
                                    className="form-control font-16"
                                    placeholder="Company Name"
                                    onChange={e =>
                                      setFieldValue("companyName", e.target.value)
                                    }
                                    required
                                  ></input>
                                  <span className="form-side-icon">
                                    {/* <MdEmail /> */}
                                  </span>
                                  <ErrorMessage
                                    component="div"
                                    name="companyName"
                                    className="error-message"
                                  />
                                </div>
                              </Grid>
                            )}
                            <Grid item xs={12} sm={6}>
                              <div className="position-relative">
                                <input
                                  id="firstName"
                                  name="firstName"
                                  type="text"
                                  className="form-control font-16"
                                  placeholder="First Name"
                                  onChange={e =>
                                    setFieldValue("firstName", e.target.value)
                                  }
                                  required
                                ></input>
                                <span className="form-side-icon">
                                  <IoMdPerson />
                                </span>
                                <ErrorMessage
                                  component="div"
                                  name="firstName"
                                  className="error-message"
                                />
                              </div>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                              <div className="position-relative">
                                <input
                                  id="lastName"
                                  name="lastName"
                                  type="text"
                                  className="form-control font-16"
                                  placeholder="Last Name"
                                  onChange={e =>
                                    setFieldValue("lastName", e.target.value)
                                  }
                                  required
                                ></input>
                                <span className="form-side-icon">
                                  <IoMdPerson />
                                </span>
                                <ErrorMessage
                                  component="div"
                                  name="lastName"
                                  className="error-message"
                                />
                              </div>
                            </Grid>

                            <Grid item xs={12}>
                              <div className="position-relative">
                                <input
                                  name="email"
                                  id="email"
                                  type="text"
                                  className="form-control font-16"
                                  placeholder="Email Address"
                                  onChange={e =>
                                    setFieldValue("email", e.target.value)
                                  }
                                  required
                                ></input>
                                <span className="form-side-icon">
                                  <MdEmail />
                                </span>
                                <ErrorMessage
                                  component="div"
                                  name="email"
                                  className="error-message"
                                />
                              </div>
                            </Grid>

                            {this.checkUserRolesForMobile(newUser) && (
                              <Grid item xs={12} className="phone-number">
                                <Field
                                  className="mobile-placeholder"
                                  type="mobile-number"
                                  placeholder="Mobile Number"
                                  id="mobileNumber"
                                  name="mobileNumber"
                                  value={values.mobileNumber}
                                  onChangeText={value => setFieldValue("mobileNumber", value)}
                                />

                                <ErrorMessage
                                  component="div"
                                  name="mobileNumber"
                                  className="error-message"
                                />
                              </Grid>
                            )}

                            {this.checkUserRolesForCompanyLogo(newUser) && (
                              <Grid item xs={6} className="attach-logo">
                                {values.companyLogo ?
                                  (
                                    <div className="addBoat-container mr-2 mb-2">
                                      <img src={values.companyLogo} alt={"companyLogo"} height="100px" width="100px" />
                                      <span onClick={() => handleSingleFileDelete(values.companyLogo, "companyLogo", setFieldValue)} > × </span>
                                    </div>
                                  )
                                  :
                                  (

                                    <div className="register-img-uploader">
                                      <input type="file" id="companyLogo" name="companyLogo" accept="image/png, image/jpeg"
                                        onChange={e => this.handleSingleFile(e.target.files, "companyLogo", setFieldValue)} />
                                      <Button variant="contained" className="attach-btn">Attach Company Logo</Button>
                                    </div>
                                  )}

                                {isLoading && <div className="register-loader"><Spinner animation="border" role="status" variant="primary">
                                  <span className="sr-only" >Loading...</span>
                                </Spinner></div>}
                                <ErrorMessage
                                  component="div"
                                  name="companyLogo"
                                  className="error-message"
                                />

                              </Grid>
                            )}

                            {this.checkUserRolesOfficeLocation(newUser) && (
                              <Grid item xs={6} className="register-map-main-div">
                                <button type="button" onClick={this.mapHandler}>
                                  Office Location
                                </button>
                                <br />
                                {values.address}
                                <ErrorMessage
                                  component="div"
                                  name="address"
                                  className="error-message"
                                />
                                <Dialog
                                  open={openMapDialog}
                                  onClose={this.mapHandler}
                                  classes={{ container: "model-width" }}
                                  aria-labelledby="responsive-dialog-title"
                                >

                                  <DialogContent className="overflow-hidden" >
                                    <GoogleMap
                                      className="googleMap-position"
                                      latLng={
                                        values.address
                                        ? {
                                          lat: latitude,
                                            lng: longitude
                                          }
                                          : latLng
                                      }
                                      fetch={result =>
                                        this.fetchMapInfo(result, setFieldValue)
                                      }
                                      height={60}
                                      width={100}
                                      placeHolder="Surveyor Location"
                                      columnName={"address"}
                                      isError={errors.address}
                                      value={resultLocation}
                                      onClose={this.mapHandler}
                                      isCloseBtn
                                      isUpdate={values.address ? true : false }
                                    ></GoogleMap>
                                  </DialogContent>
                                </Dialog>

                              </Grid>
                            )}

                            <Grid item xs={12}>
                              <div className="position-relative register-field">
                                <input
                                  id="password"
                                  name="password"
                                  type={type}
                                  className={
                                    "form-control font-16" +
                                    (errors.password
                                      ? " is-invalid"
                                      : "")
                                  }
                                  placeholder="Password"
                                  onChange={e => {
                                    setFieldValue("password", e.target.value);
                                    this.validate(e, setFieldValue, values.email || "");
                                  }}
                                  required
                                ></input>
                                <ErrorMessage
                                  component="div"
                                  name="password"
                                  className="error-message"
                                />
                                <span
                                  className="form-side-icon password-show-icon"
                                  onClick={this.changeState}
                                >
                                  {wording}
                                </span>
                              </div>
                              {isDisplayHint && (
                                <div className="mt-2 register-password-hint">
                                  {errorMessages && errorMessages.passwordLength
                                    ? renderErrorMessage(
                                      "At least 8 characters",
                                      "error-message mt-0",
                                      true
                                    )
                                    : renderErrorMessage(
                                      "At least 8 characters",
                                      "error-message-fullfill",
                                      false
                                    )}
                                  {errorMessages &&
                                    errorMessages.symbolOrNumberCount
                                    ? renderErrorMessage(
                                      "At least 1 symbol & Number",
                                      "error-message mt-0",
                                      true
                                    )
                                    : renderErrorMessage(
                                      "At least 1 symbol & Number",
                                      "error-message-fullfill",
                                      false
                                    )}
                                  {passwordStrength && (passwordStrength === "poor" ||
                                    passwordStrength === "weak")
                                    ? renderErrorMessage(
                                      `Password Strength: ${passwordStrength}`,
                                      "error-message mt-0",
                                      true
                                    )
                                    : renderErrorMessage(
                                      `Password Strength: ${passwordStrength}`,
                                      "error-message-fullfill",
                                      false
                                    )}
                                  {errorMessages && errorMessages.capsCount
                                    ? renderErrorMessage(
                                      "password must contain one capital",
                                      "error-message mt-0",
                                      true
                                    )
                                    : renderErrorMessage(
                                      "password must contain one capital",
                                      "error-message-fullfill",
                                      false
                                    )}
                                </div>
                              )}
                            </Grid>

                            <Grid item xs={12}>
                              <Captcha
                                onChange={(value) => setFieldValue('captchaValue', value)}
                              />
                              {errors && errors.captchaValue &&
                                <div className="invalid-feedback-error">{errors.captchaValue}</div>
                              }
                            </Grid>
                          </Grid>

                          <div className="clearfix mt-3 agreement-mb">
                            <div
                              className="custom-control custom-checkbox float-left mb-none register-agree-section d-flex flex-column"
                              onClick={this.onClickAgree}
                            >
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id="agree"
                                checked={agree}
                              />
                              <label
                                className="custom-control-label font-14 register-custom-control-label mb-0 d-flex cursor-pointer register-agree-text"
                                for="remember me"
                              >
                                I Agree to Adamsea's
                              <div
                                  onClick={this.termsHandler}
                                  className="ml-1 darkBlue mr-1"
                                >
                                  Terms
                              </div>
                                <Dialog
                                  open={termsModal}
                                  onClose={this.termsHandler}
                                  aria-labelledby="alert-dialog-title"
                                  aria-describedby="alert-dialog-description"
                                >
                                  <DialogTitle id="alert-dialog-title">
                                    {
                                      infoData.title
                                    }
                                  </DialogTitle>
                                  <DialogContent>
                                    <DialogContentText id="alert-dialogquis">

                                      <div
                                        dangerouslySetInnerHTML={this.createMarkup(
                                          infoData.pageContent
                                        )}
                                      />
                                    </DialogContentText>
                                  </DialogContent>
                                </Dialog>
                                and
                              <div
                                  onClick={this.privacyPolicyHandler}
                                  className="ml-1 darkBlue"
                                >
                                  Privacy Policy
                              </div>
                                <Dialog
                                  open={privacyModal}
                                  onClose={this.privacyPolicyHandler}
                                  aria-labelledby="alert-dialog-title"
                                  aria-describedby="alert-dialog-description"
                                >
                                  <DialogTitle id="alert-dialog-title">
                                    {
                                      infoData.title
                                    }
                                  </DialogTitle>
                                  <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                      <div
                                        dangerouslySetInnerHTML={this.createMarkup(
                                          infoData.pageContent
                                        )}
                                      />
                                    </DialogContentText>
                                  </DialogContent>
                                </Dialog>
                              </label>
                              {agree !== null & !agree ? (
                                <div className="text-danger fontSize12">
                                  please accept terms and conditions
                                  </div>
                              ) : <> </>}
                            </div>
                          </div>

                          <div className="clearfix mt-3 md-mt-2" onClick={handleSubmit}>
                            <button className="btn btn-block btn-lg btn-primary w-100 cursor-pointer" disabled={isLoading}>
                              Sign up
                          </button>
                          </div>
                        </Form>

                        {isError && errorMessage && errorMessage.length ? <ErrorComponent errors={errorMessage} /> : <> </>}

                        <div className="other-links register-website-link">
                          <span>
                            Ensure that whenever you sign in to AdamSea, the Web
                            address in your browser starts with
                        </span>
                          <a href="#" className="darkBlue link">
                            www.adamsea.com
                        </a>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isError: state.loginReducer.isError,
  isAuthorized: state.loginReducer.isAuthorized,
  errorMessage: state.errorReducer.errorMessage,
  roles: state.loginReducer.roles,
  currentLocation: state.loginReducer.currentLocation,
  infoList: state.loginReducer && state.loginReducer.infoList,
  signUpBanners: state.bannerReducer && state.bannerReducer["signUpBanner"],
  infoData: state.pageInfoByTypeReducer && state.pageInfoByTypeReducer.pageInfoByType
});

const mapDispatchToProps = dispatch => ({
  register: data => dispatch(register(data)),
  clearAuthFlag: () => dispatch(clearAuthorizationFlag()),
  getUserRoles: () => dispatch(getUserRoles()),
  setCurrentLocation: data => dispatch(setCurrentLocation(data)),
  getUserLocation: data => dispatch(getUserLocation(data)),
  getAllPageInformationByType: data =>
    dispatch(getAllPageInformationByType(data)),
  getCategoriesWiseBanners: (data) => (dispatch(getCategoriesWiseBanners(data))),
  clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
  getAllPageInfoByType: data => dispatch(getAllPageInfoByType(data)),
  clearPageInfoByTypeFlag: data => dispatch(clearPageInfoByTypeFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
