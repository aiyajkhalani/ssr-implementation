import React, { Component } from "react";
import { Layout } from "../../components";
import { Grid } from "@material-ui/core";
import { Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { defaultImage } from "../../util/enums/enums";
import "../../components/dialog/EstimateCost.scss";
import { getBoatShipperAction } from "../../redux/actions";

const BoatShipper = props => {
  const { boatShipperList } = props;
  return (
    <Layout className="boat-shipper container100 ">
      <div>
        <div className="pb-3 pt-3">
          <div className="d-flex align-items-center">
            <span className="font-18">AdamSea Certified Shippers</span>
          </div>
        </div>
        <div>
          <div id="alert-dialog-description">
            <Grid>
              <Row className="mb-4">
                {boatShipperList &&
                  boatShipperList.map(item => (
                    // <Col sm={3} className="mb-12">
                    //   <div className="shipper-div pb-0">
                    //     <div className="shipper-logo-img">
                    //       <img
                    //         src={item.companyLogo || defaultImage}
                    //         alt="company logo"
                    //         className="h-100"
                    //       />
                    //     </div>
                    //     <div className="d-flex flex-column mt-3">
                    //       <div className="d-flex align-items-center mb-2">
                    //         <i class="adam-user mr-2 icon-color inner-agent-info-icon"></i>
                    //         <span className="font-weight-500 font-14 gray-dark mr-2 boat-shipper-div-info mb-0 boat-shipper-name">
                    //           Name :
                    //         </span>
                    //         <span className="font-14 font-weight-400 dark-silver boat-shipper-div-info mb-0">
                    //           {item.firstName} {item.lastName}
                    //         </span>
                    //       </div>
                    //       <div className="d-flex align-items-center mb-2">
                    //         <i class="adam-phone-call mr-2 icon-color inner-agent-info-icon"></i>
                    //         <span className="font-weight-500 font-14 gray-dark mr-2 boat-shipper-div-info mb-0">
                    //           Mobile :
                    //         </span>
                    //         <span className="font-14 font-weight-400 dark-silver boat-shipper-div-info mb-0">
                    //           {item.mobileNumber || "-"}
                    //         </span>
                    //       </div>

                    //       <div className="d-flex flex-column">
                    //         <div className="d-flex align-items-center">
                    //           <i class="adam-envlope mr-2 icon-color inner-agent-info-icon"></i>
                    //           <span className="font-weight-500 font-14 gray-dark mr-2 boat-shipper-div-info mb-0">
                    //             Email :
                    //           </span>
                    //           <span className="font-14 font-weight-400 dark-silver boat-shipper-div-info mb-0">
                    //             {item.email || "-"}
                    //           </span>
                    //         </div>
                    //       </div>
                    //     </div>
                    //   </div>
                    // </Col>
                    <>
                      <Grid item xs={3} className="stepper-info-boat">
                        <div>
                          <Row className="p-4 seller-stepper-div-padding">
                            <Col xs={7} className="pr-0 pl-0">
                              <div>
                                <div className="mb-4">
                                <div className="mb-2 d-flex align-items-center company-logo">
                                <img
                                      src={
                                        item.companyLogo || defaultImage
                                      }
                                      alt="companyName"
                                    />
                                </div>
                                  <div className="mb-2 d-flex align-items-center">
                                    <img
                                      src={require("../../components/userProfile/image/boat-shipper.png")}
                                      className="stepper-user-profile-icon-div mr-2"
                                      alt=""
                                    />
                                    <span className="rentInner-userTextH4 text-transform-capitalize font-16 dark-silver">
                                      Shipper
                                    </span>
                                  </div>
                                  
                                  <div className="mb-2 d-flex align-items-center">
                                    <img
                                      src={require("../../components/userProfile/image/verified.png")}
                                      className="stepper-user-profile-icon-div mr-2"
                                      alt=""
                                    />
                                    <span className="rentInner-userTextH4  font-16 dark-silver">
                                      Verified
                                  </span>
                                  </div>
                                  <div className="mb-2 d-flex align-items-center">
                                    <img
                                      src={require("../../components/userProfile/image/reviews.png")}
                                      className="stepper-user-profile-icon-div mr-2"
                                      alt=""
                                    />
                                    <span className="rentInner-userTextH4  font-16 dark-silver">
                                      {item &&
                                        item.reviews &&
                                        item.reviews.reviews &&
                                        item.reviews.reviews.length} Reviews
                                  </span>
                                  </div>
                                </div>
                                <hr />
                                <div className="mt-4">
                                  {item.documentVerification &&
                                    (
                                      <div className="mb-2 d-flex align-items-center">
                                        <img
                                          src={require("../../components/userProfile/image/vector.png")}
                                          className="stepper-user-profile-icon-div mr-2"
                                          alt=""
                                        />
                                        <span className="rentInner-userTextH4  font-16 dark-silver">
                                          Government ID
                                  </span>
                                      </div>
                                    )}
                                  {item.documentVerification &&
                                    (
                                      <div className="mb-2 d-flex align-items-center">
                                        <img
                                          src={require("../../components/userProfile/image/vector.png")}
                                          className="stepper-user-profile-icon-div mr-2"
                                          alt=""
                                        />
                                        <span className="rentInner-userTextH4  font-16 dark-silver">
                                          Email address
                                  </span>
                                      </div>
                                    )}
                                  {item.documentVerification &&
                                    (
                                      <div className="mb-2 d-flex align-items-center">
                                        <img
                                          src={require("../../components/userProfile/image/vector.png")}
                                          className="stepper-user-profile-icon-div mr-2"
                                          alt=""
                                        />
                                        <span className="rentInner-userTextH4  font-16 dark-silver">
                                          Phone number
                                  </span>
                                      </div>
                                    )}
                                </div>
                              </div>
                            </Col>
                            <Col xs={5}>
                              <div className="d-flex justify-content-center align-items-center">
                                <div className="mb-3">
                                  <div className="stepper-userImg rounded-circle user-profile-online-section">
                                    <img
                                      className="rounded-circle h-100 width-100"
                                      src={
                                        item.image || defaultImage
                                      }
                                      alt="placeholder"
                                    />
                                    <div className="stepper-user-online">
                                      <div className="online-div-user"></div>
                                    </div>
                                  </div>
                                  <div className="company-name dark-silver  font-16 text-center">
                                    {item && item.companyName}
                                  </div>
                                  <div className="d-flex justify-content-evenly mt-3">
                                    <img
                                      src={require("../../components/userProfile/image/group-message.png")}
                                      className="user-profile-social-icon-div cursor-pointer"
                                      alt=""
                                    />
                                    <img
                                      src={require("../../components/userProfile/image/profile.png")}
                                      className="user-profile-social-icon-div cursor-pointer"
                                      alt=""
                                    />
                                    <img
                                      src={require("../../components/userProfile/image/share.png")}
                                      className="user-profile-social-icon-div cursor-pointer"
                                      alt=""
                                      //onClick={() => setSharePopUpOpen(true)}
                                    />
                                    {/* {sharePopUpOpen && (
                                      <SharePopup
                                        handleClick={() => setSharePopUpOpen(false)}
                                        useOwnIcon={true}
                                      />
                                    )} */}
                                  </div>
                                  {/* {userInfo.companyLogo && (
                                    <div className="logo-box">
                                      <img
                                        src={userInfo.companyLogo}
                                        height={50}
                                        width={50}
                                        alt=""
                                      />
                                    </div>
                                  )} */}
                                  
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Grid>
                    </>
                  ))}
              </Row>
            </Grid>
          </div>
        </div>
      </div>
    </Layout>
  );
};

// export default BoatShipper;

const mapStateToProps = state => ({
  boatShipperList: state.boatReducer.boatShipperList
});

const mapDispatchToProps = dispatch => ({
  getBoatShipperAction: () => dispatch(getBoatShipperAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatShipper);
