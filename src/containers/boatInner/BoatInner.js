import React, { Component } from "react";
import { connect } from "react-redux";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Player, BigPlayButton } from "video-react";
import { Grid, Card, Box } from "@material-ui/core";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import TextArea from "antd/lib/input/TextArea";
import Carousel, { Modal, ModalGateway } from "react-images";
import { Row, Col } from "react-bootstrap";
import moment from "moment";
import Truncate from "react-truncate";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";

import {
  boatInformation,
  mechanicalSystem,
  electricalSystem,
  boatDimensions,
  icons,
  iconImages,
  dimension,
  showAll,
  userRoles
} from "../../util/enums/enums";
import {
  displayDefaultValue,
  displayDefaultReview,
  displayDefaultImage,
  snakeCase
} from "../../helpers/string";
import { Layout, Loader } from "../../components";
import ReadMore from "../../components/helper/truncate";
import {
  getBoatById,
  getAllBoatLookUps,
  increaseBoatViewCount,
  createSalesEngine,
  clearCreateSalesEngineFlag,
  createAuctionBid,
  clearBidFlag,
  getBoatShipperAction,
  getBoatByCities,
  clearBoatByCity
} from "../../redux/actions";
import InnerRating from "../../components/staticComponent/InnerRating";
import { ImageCarousal } from "../../components/carouselComponent/imageCarousal";
import InnerReviews from "../../components/gridComponents/InnerReviews";
import { BoatOption } from "../../components/gridComponents/boatOption";
import GoogleMarker from "../../components/map/marker";
import { getSingleBoatMarker, isBuyerEligible } from "../../helpers/boatHelper";
import { InfoNotify, SuccessNotify } from "../../helpers/notification";
import {
  BoatInnerGallery,
  HomPageVideoStyle,
  BoatInnerRelatedVideoStyle
} from "../../components/styleComponent/styleComponent";
import { bidPopup } from "../../helpers/bidPopup";
import {
  createReview,
  clearReviewFlag
} from "../../redux/actions/ReviewAction";

import "./BoatInner.scss";
import DialogCard from "../../components/dialog/DialogCard";
import BoatShipperDialog from "../../components/dialog/BoatShipperDialog";
import { prepareGalleryData, priceFormat } from "../../util/utilFunctions";
import SharePopup from "../../components/share/SharePopup";
import { cityCountryNameFormatter } from "../../helpers/jsxHelper";

const auctionCarouselItem = {
  isBrowser: 4,
  isTablet: 2,
  isMobile: 1
};

class BoatInner extends Component {
  state = {
    boatId: "",
    review: {
      rating: 0,
      reviews: "",
      estimateCostFlag: false
    },
    boatByCityList: [],
    shipperModalFlag: false,

    isGallery: false,
    isLayoutGallery: false,

    sharePopUpOpen: false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      match: { params },
      history,
      createSuccess,
      createError,
      clearCreateSalesEngineFlag,
      clearBidFlag,
      createBidSuccess,
      isReview,
      getBoatById,
      clearReviewFlag,
      errors,
    } = nextProps;

    const { boatId } = prevState;
    if (isReview) {
      boatId && getBoatById({ id: boatId });
      clearReviewFlag();
    }
    if (createBidSuccess) {
      SuccessNotify("Your bid added successfully");
      clearBidFlag();
    }
    if (createSuccess) {
      clearCreateSalesEngineFlag();
      history && history.push(`/sales-engine-process`);
    } else if (createError && errors) {
      clearCreateSalesEngineFlag();
      InfoNotify(errors);
    } else if (params && params.hasOwnProperty("id") && params.id) {
      return {
        boatId: params.id
      };
    }

    return null;
  }

  async componentDidMount() {
    const { boatId } = this.state;
    const {
      getBoatById,
      getAllBoatLookUps,
      increaseBoatViewCount,
      getBoatShipperAction,
      getBoatByCities
    } = this.props;

    getAllBoatLookUps();
    getBoatShipperAction();

    boatId && (await getBoatById({ id: boatId }));
    // boatId && (await getBoatByCities({ id: boatId }));
    boatId && (await getBoatByCities({ id: '5def50f2751846baccd0782a' }));

    boatId && increaseBoatViewCount({ boatId });
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      boatByCityLoad,
      boatByCitySuccess,
      boatByCityError,
      boatByCityData,
      clearBoatByCity
    } = this.props;

    if (prevProps.boatByCityLoad && !boatByCityLoad && boatByCitySuccess) {
      this.setState({ boatByCityList: boatByCityData });
      clearBoatByCity();
    }

    if (prevProps.boatByCityLoad && !boatByCityLoad && boatByCityError) {
      InfoNotify(boatByCityError);
      clearBoatByCity();
    }
  }

  setEstimateCost = data => {
    this.setState({
      estimateCostFlag: data
    });
  };

  purchaseBoatHandler = async () => {
    const { boat, currentUser, createSalesEngine } = this.props;

    if (
      isBuyerEligible(currentUser) &&
      boat.hasOwnProperty("id") &&
      boat.seller
    ) {
      await createSalesEngine({
        buySellProcess: "myBoat",
        boat: boat.id,
        seller: boat.seller.id
      });
    }
  };

  toggleMenu = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };

  onClickBoatShipper = () => {
    this.setState(prevState => ({
      shipperModalFlag: !prevState.shipperModalFlag
    }));
  };

  openLightbox = () => {
    this.setState({ isGallery: true });
  };
  closeLightbox = () => {
    this.setState({ isGallery: false });
  };

  openNewLightbox = () => {
    this.setState({ isLayoutGallery: true });
  };
  closeNewLightbox = () => {
    this.setState({ isLayoutGallery: false });
  };

  redirectRouteHandler = route => {
    window && window.open(`/${route}`, "_blank");
  };

  // MyVerticallyCenteredModal = (props) => {
  //   const { boat } = this.props;
  //   return (
  //     <Modal size="lg" aria-labelledby="shipper-modal" centered>
  //       <Modal.Header closeButton>
  //         <Modal.Title id="contained-modal-title-vcenter">
  //           Modal heading
  //         </Modal.Title>
  //       </Modal.Header>
  //       <Modal.Body>
  //         <h4>Centered Modal</h4>
  //         <p>
  //           Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
  //           dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta
  //           ac consectetur ac, vestibulum at eros.
  //         </p>
  //       </Modal.Body>
  //       <Modal.Footer>
  //         <button onClick={props.onHide}>Close</button>
  //       </Modal.Footer>
  //     </Modal>
  //   );
  // };

  // boatShipperModal = () => {
  //   const { boat } = this.props;
  //   return (
  //     <ButtonToolbar>
  //     <button
  //     className="btn  btn-xs btn-link-info text-decoration-none text-white boat-shipper-estimate-button pl-1 pr-1 p-1"
  //     onClick={() => this.setState({modalShow:true})}
  //   >
  //     ESTIMATE COST
  //   </button>

  //     <MyVerticallyCenteredModal
  //       show={this.modalShow}
  //       onHide={() =>  this.setState({modalShow:false})}
  //     />
  //   </ButtonToolbar>
  //   );
  // }

  renderBuyNowSection = () => {
    const { boat, currentUser } = this.props;
    const { price } = boat;

    return (
      <div className="d-flex buy-right-button h-100  pl-0 pt-3 pb-3 pr-0">
        <figcaption>
          <Link
            className=" pl-0 font-10 f-500 text-right f-12 add-feedback-text"
            to="#"
            data-toggle="modal"
            data-target="#currency-modal"
          >
            USE CURRENCY CONVERTER
          </Link>
          <div className="price-wrap">
            <span className="price-new f-500">INR {priceFormat(price)}</span>
          </div>

          {/* {currentUser && currentUser.id && (
            <div className="mt-1">
              <Link
                to="#"
                className="btn btn-md btn-rounded boat-inner-header-button light-sky-blue boat-inner-card-hovered-button-effect pl-4 pr-4 buy-it-now-inner-button"
              >
                Buy It Now
              </Link>
            </div>
          )} */}
        </figcaption>

        <div>
          {currentUser &&
            currentUser.id &&
            currentUser.role &&
            currentUser.role.aliasName === "member" && (
              <button
                type="button"
                class="btn btn-primary w-auto float-right buy-now-inner buy-it-now-inner-button"
                onClick={this.purchaseBoatHandler}
              >
                Buy It Now
              </button>
            )}
        </div>
      </div>
    );
  };

  renderBoatInfo = () => {
    const { boat } = this.props;
    const {
      boatType,
      boatName,
      boatParking,
      trailer,
      yearBuilt,
      manufacturedBy,
      hullMaterial,
      hullColor,
      usedHours
    } = boat;
    const {
      TITLE,
      BOAT_TYPE,
      BOAT_NAME,
      BOAT_PARKING,
      TRAILER,
      YEARS_OF_BUILT,
      MANUFACTURED_BY,
      HULL_MATERIAL,
      HULL_COLOR,
      HOURS_USED
    } = boatInformation;

    return (
      <div className="inner-page-info-div">
        <Grid container>
          <Grid item md={12}>
            {/* <div className="f-500 mb-4 boat-inner-info-title">
              <i className="adam-boat2 mr-2 side-icon text-black d-flex"></i>
              {TITLE}
            </div> */}
            <div className="width-100 pr-3 boat-inner-info-title d-flex align-items-center">
              <i className="adam-boat2 mr-2 side-icon text-black d-flex mb-4"></i>
              <h3 className="mb-4 f-500"> {TITLE}</h3>
            </div>

            <Grid container className="mb-2">
              {/* <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {BOAT_TYPE}
                </h6>
                {boatType && (
                  <p className="boat-inner-info-desc mt-0 mt-0">
                    {displayDefaultValue(boatType.name)}
                  </p>
                )}
              </Grid> */}
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {BOAT_NAME}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(boatName)}
                </p>
              </Grid>
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {BOAT_PARKING}
                </h6>
                {boatParking && (
                  <p className="boat-inner-info-desc mt-0">
                    {displayDefaultValue(boatParking.alias)}
                  </p>
                )}
              </Grid>
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {HOURS_USED}
                </h6>
                <p className="boat-inner-info-desc mt-0 mb-0">
                  {displayDefaultValue(usedHours)}
                </p>
              </Grid>
            </Grid>

            <Grid container className=" mb-2">
              {/* <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {YEARS_OF_BUILT}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(yearBuilt)}
                </p>
              </Grid> */}
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {MANUFACTURED_BY}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(manufacturedBy)}
                </p>
              </Grid>
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {HULL_MATERIAL}
                </h6>
                {hullMaterial && (
                  <p className="boat-inner-info-desc mt-0">
                    {displayDefaultValue(hullMaterial.alias)}
                  </p>
                )}
              </Grid>
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {TRAILER}
                </h6>
                <p className="boat-inner-info-desc mt-0 mb-0">
                  {displayDefaultValue(trailer)}
                </p>
              </Grid>
            </Grid>

            <Grid container className=" mb-2">
              <Grid item md={4}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {HULL_COLOR}
                </h6>
                <p className="boat-inner-info-desc mt-0 mb-0">
                  {displayDefaultValue(hullColor)}
                </p>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  };

  renderBoatLayout = () => {
    const { boat } = this.props;

    return (
      <Grid item md={12}>
        {boat.layout && boat.layout.length > 0 && (
          <div>
            <Grid
              item
              md={12}
              className="p-0 align-items-end justify-content-between"
            >
              <h4 className="mt-4 mb-2 f-500">Boat Layout</h4>

              <figure
                className="card-product m-0 boat-inner-layout-carousel"
                onClick={this.openNewLightbox}
                target="_blank"
              >
                <div data-ride="carousel">
                  <div className="video-container">
                    <div className="carousel-item active boatInner-boatLayout">
                      <ImageCarousal
                        items={auctionCarouselItem}
                        carouselData={displayDefaultImage(boat.layout)}
                      />
                    </div>
                  </div>
                </div>
              </figure>
            </Grid>
          </div>
        )}
      </Grid>
    );
  };

  renderAboutBoat = () => {
    const { boat } = this.props;

    return (
      <Grid item sm={12}>
        <div className="width-100 pr-3 padding-inner-about">
          <h4 className="mt-5 mb-4 mr-2 f-500 inner-main-title">
            {" "}
            About The Boat
          </h4>
          <p className="text-justify a-inner-fix">
            <ReadMore className="text-justify ">
              {displayDefaultValue(boat.description)}
            </ReadMore>
          </p>
        </div>
      </Grid>
    );
  };

  renderMechanicalSystem = () => {
    const { boat } = this.props;
    const {
      waterMarker,
      bowThruster,
      steeringSystem,
      stabilizerSystem,
      fuelType,
      oilWaterSeparator
    } = boat;
    const {
      TITLE,
      WATER_MAKER,
      BOW_THRUSTER,
      STEERING_SYSTEM,
      STABILIZER_SYSTEM,
      FUEL_TYPE,
      OIL_WATER_SEPARATOR
    } = mechanicalSystem;

    return (
      <div className="inner-page-info-div">
        <Grid item md={12}>
          <h3 className="mt-4 pt-2 mt-50 f-500 mb-4">
            <i className="adam-mechanical mr-2 text-black"></i>
            {TITLE}
          </h3>
          <Grid container className="mb-2 pt-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {WATER_MAKER}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(waterMarker)}
              </p>
            </Grid>
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {BOW_THRUSTER}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(bowThruster)}
              </p>
            </Grid>
          </Grid>
          <Grid container className=" mb-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {STEERING_SYSTEM}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(steeringSystem)}
              </p>
            </Grid>
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {STABILIZER_SYSTEM}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(stabilizerSystem)}
              </p>
            </Grid>
          </Grid>
          <Grid container className=" mb-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {FUEL_TYPE}
              </h6>
              {fuelType && (
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(fuelType.alias)}
                </p>
              )}
            </Grid>
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {OIL_WATER_SEPARATOR}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(oilWaterSeparator)}
              </p>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  };

  renderElectricalSystem = () => {
    const { boat } = this.props;
    const {
      output,
      generators,
      emergencyGenerator,
      batteryType,
      batteriesCount
    } = boat;
    const {
      TITLE,
      OUTPUT,
      GENERATORS,
      EMERGENCY_GENERATORS,
      TYPES_OF_BATTERIES,
      NO_OF_BATTERIES,
      CONTENT
    } = electricalSystem;
    return (
      <div className="inner-page-info-div">
        <Grid item md={12}>
          <h3 className="mt-4 pt-2 mt-50 f-500 mb-4">
            <i className="adam-electrical mr-2 text-black"></i>
            {TITLE}
          </h3>
          <Grid container className="mb-2 pt-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {OUTPUT}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(output)}
              </p>
            </Grid>
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {GENERATORS}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(generators)}
              </p>
            </Grid>
          </Grid>
          <Grid container className="mb-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {EMERGENCY_GENERATORS}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(emergencyGenerator)}
              </p>
            </Grid>
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {TYPES_OF_BATTERIES}
              </h6>
              <p className="sub-title-sm mt-0">
                {displayDefaultValue(batteryType)}
              </p>
            </Grid>
          </Grid>
          <Grid container className="mb-2">
            <Grid item md={6}>
              <h6 className="title-sm mb-0 title-description font-weight-500">
                {NO_OF_BATTERIES}
              </h6>
              <p className="boat-inner-info-desc mt-0">
                {displayDefaultValue(batteriesCount)}
              </p>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  };

  renderDimensions = () => {
    const { boat } = this.props;
    const {
      decks,
      draft,
      heightInFt,
      displacement,
      lengthInFt,
      widthInFt,
      weightInKg,
      mainSaloon,
      mainSaloonConvertible,
      beam,
      numberOfHeads,
      crewHeadsCount,
      crewCabinCount,
      crewBerthCount
    } = boat;
    const {
      TITLE,
      DECK_NUMBER,
      HEIGHT,
      LENGTH,
      WIDTH,
      WEIGHT,
      BEAM,
      DRAFT,
      DISPLACEMENT,
      MAIN_SALOON,
      MAIN_SALOON_CONVERTIBLE,
      NO_OF_HEADS,
      NO_OF_CREW_CABIN,
      NO_OF_CREW_BIRTHS,
      NO_OF_CREW_HEADS
    } = boatDimensions;

    return (
      <>
        <div className="inner-page-info-div">
          <Grid item md={12}>
            <h4 className="mt-4 f-500 mb-4">
              <i className="adam-dimension-1 mr-2 text-black"></i>
              {TITLE}
            </h4>
            <Grid container className="pt-2">
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {DECK_NUMBER}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(decks)}
                </p>
              </Grid>

              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {HEIGHT}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(heightInFt)} ft
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {LENGTH}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(lengthInFt)} m
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {WIDTH}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(widthInFt)} cm
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {WEIGHT}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(weightInKg)} kg
                </p>
              </Grid>

              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {BEAM}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(beam)}
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {DRAFT}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(draft)}
                </p>
              </Grid>

              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {DISPLACEMENT}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(displacement)}
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {MAIN_SALOON}
                </h6>
                {mainSaloon && (
                  <p className="boat-inner-info-desc mt-0">
                    {displayDefaultValue(mainSaloon.alias)}
                  </p>
                )}
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {MAIN_SALOON_CONVERTIBLE}
                </h6>
                {mainSaloonConvertible && (
                  <p className="boat-inner-info-desc mt-0">
                    {displayDefaultValue(mainSaloonConvertible.alias)}
                  </p>
                )}
              </Grid>

              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {NO_OF_HEADS}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(numberOfHeads)}
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {NO_OF_CREW_CABIN}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(crewCabinCount)}
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {NO_OF_CREW_BIRTHS}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(crewBerthCount)}
                </p>
              </Grid>
              <Grid item md={3}>
                <h6 className="title-sm mb-0 title-description font-weight-500">
                  {NO_OF_CREW_HEADS}
                </h6>
                <p className="boat-inner-info-desc mt-0">
                  {displayDefaultValue(crewHeadsCount)}
                </p>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </>
    );
  };

  renderAmenities = () => {
    const { boat, boatLookups } = this.props;
    const { amenities } = boat;

    return (
      <Grid container className="mt-4 baot-amenities">
        <Grid item md={12}>
          <h4 className="mb-4 f-500">
            <img src={require("../../assets/images/boatInner/amenities.png")} className="accessories-amenities-icon mr-2" /> Amenities
          </h4>

          {amenities && amenities.length > 0 && (
            <div className="col-md-12 pt-2">
              <Grid container>
                <div className="row d-flex">
                  {amenities &&
                    amenities.length &&
                    amenities.map((item, index) => {
                      return (
                        <div className="mb-4 mr-5 res-m-0 mt-0 ml-0" key={index}>
                          <div className="inner-type-div m-auto">
                            <div className="inner-trip-type-icon-div mb-2 mt-1 dark-silver">
                              {icons[
                                snakeCase(item.alias.replace(/[{()}]/g, ""))
                              ] ? (
                                  <i
                                    className={
                                      item &&
                                      icons[
                                      snakeCase(
                                        item.alias.replace(/[{()}]/g, "")
                                      )
                                      ]
                                    }
                                  />
                                ) : (
                                  <img
                                    src={
                                      iconImages[
                                      snakeCase(
                                        item.alias.replace(/[{()}]/g, "")
                                      )
                                      ]
                                    }
                                    height="20px"
                                  />
                                )}
                            </div>
                            <div className="inner-trip-type-text-div">
                              <span className="font-14 trip-type-inner sub-title-sm dark-silver">
                                {item.alias}
                              </span>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </Grid>
            </div>
          )}
        </Grid>
      </Grid>
    );
  };

  renderAccessories = () => {
    const { boat } = this.props;
    const { accessories } = boat;
    let privateBoat;

    return (
      <Grid container className="mt-4 ">
        <Grid item md={12}>
          <h4 className="mb-4 f-500">
            <img src={require("../../assets/images/boatInner/accessories.png")} className="accessories-amenities-icon mr-2" /> Accessories
          </h4>

          {accessories && accessories.length > 0 && (
            <div className="col-md-12 pt-2">
              <Grid container>
                <div className="row d-flex">
                  {accessories &&
                    accessories.length &&
                    accessories.map((item, index) => {
                      return (
                        <div className="mb-4 mr-5 res-m-0 mt-0 ml-0" key={index}>
                          <div className="inner-type-div m-auto">
                            <div className="inner-trip-type-icon-div mb-2 mt-1 dark-silver">
                              {icons[snakeCase(item.alias)] ? (
                                <i
                                  className={
                                    item && icons[snakeCase(item.alias)]
                                  }
                                />
                              ) : (
                                  <img
                                    src={iconImages[snakeCase(item.alias)]}
                                    height="20px"
                                  />
                                )}
                            </div>
                            <div className="inner-trip-type-text-div">
                              <span className="font-14 trip-type-inner sub-title-sm dark-silver">
                                {item.alias}
                              </span>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </Grid>
            </div>
          )}
        </Grid>
      </Grid>
    );
  };

  renderNavigationEquipments = () => {
    const { boat, boatLookups } = this.props;
    const { navigationEquipments } = boat;

    return (
      <Grid container className="mt-4 mb-4 p-0">
        <Grid item sm={12}>
          <h4 className="mb-4 col-12 f-500">
            <i className="adam-navigation-1 mr-2 text-black"></i> Navigation
            Equipment
          </h4>

          {navigationEquipments && navigationEquipments.length > 0 && (
            <div className="col-md-12 pt-2">
              <Grid container>
                <div className="row d-flex">
                  {navigationEquipments &&
                    navigationEquipments.length &&
                    navigationEquipments.map((item, index) => {
                      return (
                        <div className="mb-4 mr-5 mt-0 res-m-0 ml-0" key={index}>
                          <div className="inner-type-div m-auto">
                            <div className="inner-trip-type-icon-div mb-2 mt-1 dark-silver">
                              {icons[
                                snakeCase(item.alias.replace(/[{()}]/g, ""))
                              ] ? (
                                  <i
                                    className={
                                      (item &&
                                        icons[
                                        snakeCase(
                                          item.alias.replace(/[{()}]/g, "")
                                        )
                                        ]) ||
                                      "adam-ship-radar"
                                    }
                                  />
                                ) : (
                                  <img
                                    src={
                                      iconImages[
                                      snakeCase(
                                        item.alias.replace(/[{()}]/g, "")
                                      )
                                      ]
                                    }
                                    height="20px"
                                  />
                                )}
                            </div>
                            <div className="inner-trip-type-text-div">
                              <span className="font-14 trip-type-inner sub-title-sm dark-silver">
                                {item.alias}
                              </span>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </Grid>
            </div>
          )}
        </Grid>
      </Grid>
    );
  };

  renderCommercial = () => {
    const { boat } = this.props;

    return (
      <Grid
        container
        className="container-fluid clr-fluid boat-inner-boat-info pl-5"
      >
        <Grid item sm={12}>
          <Grid container>
            <Grid item md={6} className="pr-1 pl-1">
              <h4 className="mb-2 col-12 f-500 ">Boat was used for</h4>
              <p className="pl-3 text-justify boat-inner-boat-info-desc">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {displayDefaultValue(boat.usage)}
                </Truncate>
              </p>
            </Grid>
            <Grid item md={6} className="pr-1 pl-1">
              <h4 className="mb-2 col-12 f-500">Accident History</h4>
              <p className="pl-3 text-justify boat-inner-boat-info-desc">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {displayDefaultValue(boat.accidentHistory)}
                </Truncate>
              </p>
            </Grid>
          </Grid>
          <Grid container>
            {/* <Grid item md={6} className="pr-1 pl-1">
              <h4 className=" mt-4 mb-2 col-12 f-500">
               Why to buy this
                Boat?
              </h4>
              <p className="pl-3 text-justify boat-inner-boat-info-desc">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {displayDefaultValue(boat.boatReview)}
                </Truncate>
              </p>
            </Grid> */}
            <Grid item md={6} className="pr-1 pl-1">
              <h4 className=" mt-4 mb-2 col-12 f-500">
                Last time Maintenance was
              </h4>
              <p className="pl-3 text-justify boat-inner-boat-info-desc">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {moment(displayDefaultValue(boat.lastMaintenance)).format(
                    "DD-MM-YYYY"
                  )}
                </Truncate>
              </p>
            </Grid>
            <Grid item md={6} className="pr-1 pl-1">
              <h4 className=" mt-4 mb-2 col-12 f-500">Repair History</h4>
              <p className="pl-3 text-justify boat-inner-boat-info-desc">
                <Truncate lines={3} ellipsis={<span>..</span>}>
                  {displayDefaultValue(boat.repairHistory)}
                </Truncate>
              </p>
            </Grid>
          </Grid>
          <Grid container></Grid>
        </Grid>
      </Grid>
    );
  };

  renderRatingAndReview = () => {
    const { boat, currentUser } = this.props;

    return (
      <div className="container">
        <div className="row mt-50 mb-5">
          <div className="col-lg-12 col-md-12 col-sm-12 p-0">
            <h4 className="mr-3">
              <i className="iconColor adam-review mr-2 text-black"></i>
              Review and Rating
            </h4>
            {boat.reviews && (
              <>
                {boat.reviews.ratings && <div>
                  <InnerRating
                    iconColor="iconColor-boatInner"
                    btnColor="boatInner-btnBg"
                    btnBlue="boatInner-btnOrange"
                    moduleId={boat.id}
                    userId={currentUser.id}
                    progressColor="boat-progress"
                    data={displayDefaultReview(boat.reviews.ratings)}
                    reviews={boat.reviews.reviews}
                  />
                </div>}
                {boat.reviews.reviews && boat.reviews.reviews.length ? <div className="mt-5">
                  <InnerReviews
                    xs={12}
                    sm={12}
                    iconColor="iconColor-boatInner"
                    data={boat.reviews.reviews}
                  />
                </div> : "No reviews found"}
              </>
            )}
          </div>
        </div>
        <hr />
      </div>
    );
  };

  redirectToUserProfile = seller => {
    const { history } = this.props;

    history &&
      history.push("/user-profile", {
        seller
      });
  };

  renderAssignAgent = seller => {
    const { firstName, state, country, images, city } = seller;
    return (
      <Grid item sm={12} className="col-12 h-100 pr-0">
        <div className="col-12 p-0 h-100">
          <div className="inner-agent-div h-100">
            <Card
              className="box-shadow-none"
              onClick={() => this.redirectToUserProfile(seller)}
            >
              <div className="card-body mt-0">
                <div className="media media-lg pl-15 display-flex mb-2 pt-0">
                  <div className="user-profile-online-section">
                    <img
                      className="rounded-circle cursor-pointer"
                      src={
                        seller.image ||
                        require("../../assets/images/boatInner/photographer-boatinner.jpg")
                      }
                      alt="placeholder"
                    />
                    <div className="user-online">
                      <div className="online-div-user"></div>
                    </div>
                  </div>
                  <div className=" mt-3 mb-0">
                    <div className="author-info">
                      <h5 className="f-500 inner-author-name">
                        {firstName}
                      </h5>
                      <p className="mb-0 mt-1">
                        {cityCountryNameFormatter(city, country)}
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-12 pl-3 pr-0 inner-user-profile-info">
                  <ul className="list-group">
                    <li className="list-group-item list-item-color border-none pl-0">
                      <i className="adam-user mr-2 icon-color inner-agent-info-icon"></i>
                      <span className="text-transform-capitalize font-14 inner-user-profile-info-text">
                        {seller && seller.role && seller.role.role.toLowerCase()}
                      </span>
                    </li>
                    <li className="list-group-item list-item-color border-none d-flex align-items-center pl-0">
                      <div>
                        <i className="adam-calendar11 mr-2 icon-color inner-agent-info-icon"></i>
                      </div>
                      <div className="font-14 inner-user-profile-info-text">
                        On AdamSea since {moment(seller.createdAt).format("MMM, YYYY")}
                      </div>
                    </li>
                    <li className="list-group-item list-item-color border-none pl-0">
                      <i className="adam-sailingboat-1 mr-2 icon-color inner-agent-info-icon"></i>
                      <span className="font-14 mr-1 inner-user-profile-info-text">
                        {seller.relatedItems && seller.relatedItems.length}
                      </span>
                      <span className="font-14 inner-user-profile-info-text">
                        boats
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Grid>
    );
  };

  sharePopup = () => {
    this.setState(prevState => ({
      sharePopUpOpen: !prevState.sharePopUpOpen
    }));
  };

  render() {
    const {
      boat,
      currentUser,
      toggleMenu,
      createReview,
      createAuctionBid,
      boatShipperList,
      history,
      getAllShipment,
      screenScroll
    } = this.props;
    const imageLength = boat && boat.images && boat.images.length;

    const {
      review,
      rating,
      estimateCostFlag,
      isGallery,
      isLayoutGallery,
      sharePopUpOpen,
      boatByCityList
    } = this.state;

    return (
      <Layout className="boat-inner-responsive">
        {boat && !boat.hasOwnProperty("id") ? (
          <Loader />
        ) : (
            <>
              <Grid
                container
                className="container-fluid clr-fluid justify-center boat-inner-sticky sticky-on-top"
              >
                <Grid container className="w-85vw buy-inner p-0">
                  <Grid item className="h-70 d-flex pr-0 pt-3 pb-3 pl-0" sm={4}>
                    <div className="mr-auto  align-self-center">
                      <div>
                        <span className="header-small-font boat-ad-id">
                          Ad ID: {boat.adId}
                        </span>
                      </div>
                      <div className="d-flex align-items-center">
                        <span className="boat-inner-name mr-1">
                          {boat.yearBuilt}
                        </span>
                        <span className="boat-inner-name ml-1">
                          {boat.boatType.name}
                        </span>
                        <h6 className="badge badge-light ml-2 header-small-font mb-0">
                          {boat.boatStatus &&
                            boat.boatStatus.alias &&
                            boat.boatStatus.alias}
                        </h6>
                      </div>
                      {boat.reviews && (
                        <div className="d-flex align-items-center inner-review mt-0">
                          <Rating
                            className="rating-clr"
                            initialRating={
                              boat.reviews.ratings &&
                              boat.reviews.ratings.averageRating
                            }
                            fullSymbol={<StarIcon />}
                            emptySymbol={<StarBorderIcon />}
                            placeholderSymbol={<StarBorderIcon />}
                            readonly
                          />
                          <h6 className="mr-2 mb-0 rating-avg">
                            {boat.reviews.ratings
                              ? boat.reviews.ratings.averageRating
                              : 0}
                          </h6>
                        </div>
                      )}
                    </div>
                  </Grid>

                  <Grid
                    item
                    className="h-70 d-flex align-items-center pt-3 pb-3 pr-0 pl-0"
                    sm={4}
                  >
                    <div className="ml-auto mr-auto  widget-media text-center">
                      <ul className="icon-effect icon-effect-1a d-flex">
                        <li>
                          <Link
                            // to="/"
                            className="icon d-flex justify-content-center"
                          >
                            <i
                              className="adam-share header-inner-icons"
                              onClick={this.sharePopup}
                            ></i>
                            {sharePopUpOpen && (
                              <SharePopup
                                handleClick={() => this.sharePopup()}
                                useOwnIcon={true}
                                screenScroll={screenScroll}
                              />
                            )}
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center"
                          >
                            <i className="adam-heart1 header-inner-icons"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center"
                          >
                            <i className="adam-flag-3 header-inner-icons"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center"
                          >
                            <i className="adam-envlope header-inner-icons"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center"
                          >
                            <i className="adam-pdf header-inner-icons"></i>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </Grid>

                  <Grid item className="h-70" sm={4}>
                    {boat && this.renderBuyNowSection(boat.seller)}
                  </Grid>
                </Grid>
              </Grid>

              <Grid container className="w-85vw m-auto h-600 position-relative">
                <div className="position-absolute inner-banner-likes-div d-flex align-items-center font13 gray-dark img-show-text">
                  <div className="d-flex justify-content-center align-items-center ">
                    <img
                      src={require("../../assets/images/rentInner/like1.png")}
                      className="inner-banner-views-img mr-1"
                      alt=""
                    />
                    <div className="mt-1 banner-count-text">{boat.likeCount}</div>
                  </div>
                </div>
                <div className="position-absolute inner-banner-top-social-div">
                  <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
                    <div className="d-flex justify-content-center align-items-center ">
                      <img
                        src={require("../../assets/images/rentInner/view.png")}
                        className="inner-banner-views-img mr-2"
                        alt=""
                      />
                    </div>
                    <div className="banner-count-text">{boat.userCount}</div>
                  </div>
                  <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
                    <div className="d-flex justify-content-center align-items-center mr-2">
                      <img
                        src={require("../../assets/images/rentInner/image-count.png")}
                        className="inner-banner-views-img"
                        alt=""
                      />
                    </div>
                    <div className="banner-count-text">{boat.images.length}</div>
                  </div>
                </div>
                <Grid
                  item
                  xs={6}
                  className="hidden border-radius-padding-img boat-inner-first-img"
                >
                  <div className="boatInner-banner-section">
                    <BoatInnerGallery
                      onClick={this.openLightbox}
                      img={
                        boat.images && boat.images.length > 0
                          ? encodeURI(boat.images[0])
                          : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                      }
                    />
                  </div>
                </Grid>

                <Grid item xs={6} className="h-100">
                  <Grid container className="h-50">
                    <Grid item xs={6} className="four-inner-bg h-100">
                      <div className="boatInner-banner-section">
                        <BoatInnerGallery
                          onClick={this.openLightbox}
                          img={
                            boat.images && boat.images.length > 1
                              ? encodeURI(boat.images[1])
                              : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                          }
                        />
                      </div>
                    </Grid>
                    <Grid item xs={6} className="four-inner-bg h-100">
                      <div className="boatInner-banner-section">
                        <BoatInnerGallery
                          onClick={this.openLightbox}
                          img={
                            boat.images && boat.images.length > 2
                              ? encodeURI(boat.images[2])
                              : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                          }
                        />
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container className="h-50">
                    <Grid item xs={6} className="four-inner-bg h-100">
                      <div className="boatInner-banner-section">
                        <BoatInnerGallery
                          onClick={this.openLightbox}
                          img={
                            boat.images && boat.images.length > 3
                              ? encodeURI(boat.images[3])
                              : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                          }
                        />
                      </div>
                    </Grid>
                    <Grid item xs={6} className="four-inner-bg h-100">
                      <div className="boatInner-banner-section">
                        <BoatInnerGallery
                          onClick={this.openLightbox}
                          img={
                            boat.images && boat.images.length > 4
                              ? encodeURI(boat.images[4])
                              : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                          }
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>

              <Grid container className="container-fluid clr-fluid w-85vw p-0">
                <Grid container>
                  <Grid item sm={9} className="d-flex flex-column">
                    <Grid container>
                      <Grid item sm={7}>
                        {boat && this.renderAboutBoat()}
                      </Grid>
                      <Grid item sm={5} className="mt-5">
                        <Grid
                          item
                          sm={12}
                          className="mt-4 pl-5 pr-4 pb-4 pt-0 shipping-block"
                        >
                          <div className="d-flex justify-content-between align-items-center">
                            <h4 className=" mt-3 mb-3 f-500">
                              <i className="adam-big-anchor mr-2 text-black"></i>
                              Shipping
                          </h4>

                            <button
                              className="btn btn-xs btn-link-info text-decoration-none boat-shipper-estimate-button-for-inner pl-1 pr-1 p-1 common-hovered-blue-button-effect boat-inner-btn-link-info"
                              // onClick={this.onClickBoatShipper}
                              boatShipperList={boatShipperList}
                              onClick={() => {
                                this.redirectRouteHandler("boat-shipper");
                              }}
                            >
                              BOAT SHIPPERS
                          </button>
                          </div>

                          <figcaption className="col-12 pl-0">
                            {/* <h6 className="mb-0 title-description">
                            Stack Blue Corporation
                          </h6> */}
                            <div className="destination-view sub-title-sm">
                              {/* Miami, United States */}
                              See All {boatShipperList &&
                                boatShipperList.length}{" "}
                              Companies are approved to move your boats around the
                              world
                          </div>
                          </figcaption>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item sm={7}>
                        {boat && this.renderBoatInfo()}
                      </Grid>
                      <Grid item sm={5}>
                        <Grid item sm={12}>
                          <Grid
                            item
                            sm={12}
                            className="pl-5 pr-4 pb-0 pt-0 shipping-block"
                          >
                            <div className="d-flex justify-content-between align-items-center">
                              <h4 className=" mt-3 mb-2 f-500">
                                <i className="adam-coins mr-2 text-black"></i>
                                Payment
                            </h4>

                              <button
                                className="btn btn-xs btn-link-info text-decoration-none boat-shipper-estimate-button-for-inner pl-1 pr-1 p-1 common-hovered-blue-button-effect boat-inner-btn-link-info"
                                onClick={() => this.setEstimateCost(true)}
                              >
                                ESTIMATE COST
                            </button>
                              <DialogCard
                                isOpen={estimateCostFlag}
                                onClose={() =>
                                  this.setEstimateCost(!estimateCostFlag)
                                }
                              />
                            </div>

                            <p className="mb-0">
                              Seller accept payment of the boat
                          </p>

                            <div className="row payment-grid mt-4">
                              <div className="col-6 p-0">
                                <div className="inner-payment-div">
                                  <i className="adam-creditcard text-black d-flex align-items-center justify-content-center inner-payment-icon mb-2"></i>
                                  <h6 className="text-center title-description gray-dark font-14">
                                    Bank to bank to transfer
                                </h6>
                                </div>
                              </div>
                              <div className="col-6 p-0">
                                <div className="inner-payment-div">
                                  <div className="mb-2">
                                    <div className="d-flex justify-content-center align-items-center">
                                      <img
                                        src={require("../../assets/images/rentInner/payment.png")}
                                        className="inner-payment-cash-icon"
                                        alt=""
                                      />
                                    </div>
                                  </div>
                                  <h6 className="text-center title-description gray-dark font-14">
                                    Credit card
                                </h6>
                                </div>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item sm={3}>
                    {/* <Grid container> */}
                    {/* <Grid item sm={6} className="p-4 mt-5 shipping-block"> */}

                    {/* </Grid> */}
                    <Grid item sm={12} className="mt-5 pt-0 p-0">
                      {boat &&
                        boat.seller &&
                        boat.seller.hasOwnProperty("id") &&
                        this.renderAssignAgent(boat.seller)}
                    </Grid>
                    {/* </Grid> */}
                  </Grid>
                </Grid>
                {/* add feedback field start */}
                <div className="width-100">
                  {currentUser && currentUser.id && (
                    <div
                      className="text-right f-14 width-100 mb-0 cursor-pointer add-feedback-mt"
                      onClick={() => this.toggleMenu()}
                    >
                      {/* <i className="adam-edit-2 mr-2"></i> */}
                      <img
                        src={require("../../assets/images/boatInner/boatinner-like.png")}
                        className="inner-feedback-img mr-2"
                        alt=""
                      />
                      <span className="add-feedback-text">Add Feedback</span>
                    </div>
                  )}
                  {this.state.visible && (
                    <Formik
                      initialValues={{ ...review }}
                      onSubmit={values => {
                        const { moduleId, currentUser, boat } = this.props;
                        values.moduleId = boat && boat.seller.id;
                        values.userId = currentUser && currentUser.id;

                        if (values.userId && values.moduleId) {
                          createReview(values);
                          this.toggleMenu();
                        }
                      }}
                      validationSchema={Yup.object().shape({
                        rating: Yup.string().required(
                          "rating field is required."
                        ),
                        reviews: Yup.string().required(
                          "review field is required."
                        )
                      })}
                      render={({
                        errors,
                        status,
                        touched,
                        setFieldValue,
                        values,
                        handleSubmit
                      }) => (
                          <Form>
                            <Card className="review-form">
                              <Grid container className="p-10">
                                <Grid item sm={6}>
                                  <div
                                    className="ant-form-item-required"
                                    title="Rating"
                                  >
                                    Add Review
                              </div>
                                  <Rating
                                    className="rating-clr"
                                    initialRating={0}
                                    onClick={value => {
                                      setFieldValue("rating", value);
                                      this.setState({ rating: value });
                                    }}
                                    emptySymbol={<StarBorderIcon />}
                                    placeholderSymbol={<StarIcon />}
                                    fullSymbol={<StarIcon />}
                                    placeholderRating={rating}
                                  />
                                  <ErrorMessage
                                    className="invalid-feedback ant-typography-danger"
                                    name={`rating`}
                                    component="span"
                                  />
                                </Grid>
                                <Grid item sm={12} className="gutter-box mt-10">
                                  <TextArea
                                    name="reviews"
                                    className="form-control"
                                    placeholder="Review"
                                    rows="5"
                                    value={values.reviews}
                                    onChange={e =>
                                      setFieldValue("reviews", e.target.value)
                                    }
                                  />
                                  <ErrorMessage
                                    className="invalid-feedback ant-typography-danger"
                                    name={`reviews`}
                                    component="span"
                                  />
                                </Grid>
                                <div className="w-100 float-left text-center">
                                  <Grid
                                    item
                                    sm={12}
                                    className="mt-10 review-button"
                                  >
                                    <button
                                      className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                                      type="submit"
                                    >
                                      Save
                                </button>
                                    <button
                                      onClick={() => this.toggleMenu()}
                                      className="ml-15 ant-btn ant-btn-primary mt-10 rating-btn d-flex justify-content-center mr-auto ml-auto"
                                      type="submit"
                                    >
                                      Cancel
                                </button>
                                  </Grid>
                                </div>
                              </Grid>
                            </Card>
                          </Form>
                        )}
                    />
                  )}
                </div>
                {/* add feedback field end */}

                {/* API WILL BE CHANGE [WIP]*/}
                {(boat.auctionRoom && boat.auctionRoom.length > 0) && (currentUser &&
                  currentUser.role &&
                  currentUser.role.aliasName === userRoles.MEMBER) && (
                    <Grid container spacing={3} className="width-100 mt-50">
                      <Grid item sm={12}>
                        <div className="boat-inner-auction-content mt-5 mb-4">
                          <div className="boat-inner-auction-action ">
                            <h4 className=" f-500">Auction Room</h4>
                            <button
                              type="button"
                              class="common-hovered-blue-button-effect"
                              onClick={() =>
                                bidPopup(createAuctionBid, boat.auctionRoom[0].id)
                              }
                            >
                              BID NOW
                        </button>
                          </div>
                          <div className="boat-inner-auction-detail">
                            <div className="auction-detail-inner">
                              <h6 class="title-sm mb-0 title-description">
                                Bid Duration
                          </h6>
                              <p class="sub-title-sm">
                                {boat.auctionRoom[0].bidDuration}
                              </p>
                            </div>

                            <div className="auction-detail-inner">
                              <h6 class="title-sm mb-0 title-description">
                                Original Price
                          </h6>
                              <p class="sub-title-sm">
                                INR{" "}
                                {boat.auctionRoom[0].boat &&
                                  priceFormat(boat.auctionRoom[0].boat.price)}
                              </p>
                            </div>
                            <div className="auction-detail-inner">
                              <h6 class="title-sm mb-0 title-description">
                                Start Price
                          </h6>
                              <p class="sub-title-sm">
                                INR{" "}
                                {boat.auctionRoom[0] &&
                                  priceFormat(boat.auctionRoom[0].startPrice)}
                              </p>
                            </div>
                            <div className="auction-detail-inner">
                              <h6 class="title-sm mb-0 title-description">
                                Minimum RaisedA mount
                          </h6>
                              <p class="sub-title-sm">
                                INR{" "}
                                {boat.auctionRoom[0] &&
                                  priceFormat(
                                    boat.auctionRoom[0].minimumRaisedAmount
                                  )}
                              </p>
                            </div>
                            <div className="auction-detail-inner">
                              <h6 class="title-sm mb-0 title-description">
                                Auction Current Bid
                          </h6>
                              <p class="sub-title-sm">
                                INR{" "}
                                {boat.auctionRoom[0].auctionCurrentBid
                                  ? priceFormat(
                                    boat.auctionRoom[0].auctionCurrentBid
                                  )
                                  : 0}
                              </p>
                            </div>
                          </div>
                        </div>
                      </Grid>
                    </Grid>
                  )}

                <Grid container spacing={3} className="width-100">
                  <Grid item sm={4}>
                    <div className="mt-4 pt-2 mb-4 d-flex align-items-center">
                      <i class="adam-navigation-main pr-5 mr-1"></i>
                      <h4 className="f-500 mb-0">Location on Map</h4>
                    </div>
                    <div className="boat-inner-map-div">
                      {boat && (
                        <GoogleMarker
                          // height={45}
                          // width={45}
                          markers={getSingleBoatMarker(boat)}
                          className="boat-inner-map-height"
                        />
                      )}
                    </div>
                  </Grid>
                  <Grid item sm={4}>
                    {boat && this.renderMechanicalSystem()}
                  </Grid>
                  <Grid item sm={4}>
                    {boat && this.renderElectricalSystem()}
                  </Grid>
                </Grid>

                <hr className="mt-4 mb-4" />

                <Grid container className="mb-2">
                  <Grid item sm={8}>
                    {boat && this.renderDimensions()}
                  </Grid>
                  <Grid item sm={4}>
                    {boat && this.renderBoatLayout()}
                  </Grid>
                </Grid>
                <hr className="mt-4" />
                <Grid container spacing={3}>
                  <Grid item sm={4}>
                    {boat && this.renderAmenities()}
                  </Grid>
                  <Grid item sm={4}>
                    {boat && this.renderAccessories()}
                  </Grid>
                  <Grid item sm={4}>
                    {boat && this.renderNavigationEquipments()}
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <hr />
                  {boat && boat.video && <Grid item className="pl-0 mt-2" sm={12}>
                    <h4 className="f-500">Related Videos</h4>
                  </Grid>}
                  {boat && boat.video && (
                    <Grid
                      sm={4}
                      className="col-12 p-0 align-items-end justify-content-between "
                    >
                      {boat.video && (
                        // Discussed with ghanshyam

                        // <Player
                        //   controls={false}
                        //   playsInline
                        //   muted
                        //   poster={
                        //     boat.images &&
                        //     boat.images.length > 0 &&
                        //     boat.images[0]
                        //   } //WIP
                        //   src={boat.video}
                        //   className="br-10"
                        // >
                        //   <BigPlayButton position="center" />
                        // </Player>
                        <BoatInnerRelatedVideoStyle
                          img={encodeURI(
                            "https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/boat_4.jpg.jpeg"
                          )}
                          className="boat-inner-related-video"
                        >
                          <PlayCircleOutlineIcon className="playVideo-icon" />
                        </BoatInnerRelatedVideoStyle>
                      )}
                    </Grid>
                  )}
                  <Grid item sm={8} className="pt-0">
                    {boat && this.renderCommercial()}
                  </Grid>
                </Grid>
              </Grid>

              <div className="rating-section">
                {boat && this.renderRatingAndReview()}
              </div>

              {/* WILL BE NEEDED LATER ON */}
              <div className="container-fluid clr-fluid w-85vw p-0">
                {boatByCityList && boatByCityList.length !== 0 && <div className="container-fluid">
                  <Grid item xs={12} className="mb-5">
                    <Box
                      className="section-heading"
                      fontSize={24}
                      fontWeight={500}
                      letterSpacing={0.5}
                    >
                      Explore More Similar Boat Option
                  </Box>

                    <BoatOption
                      xs={3}
                      sm={3}
                      boatByCity={boatByCityList}
                      dimension={dimension.sharedTrip}
                      showType={showAll.sharedTrip}
                      itemsLength={8}
                    />
                  </Grid>
                </div>}
              </div>
            </>
          )}

        {boat && boat.images && boat.images.length > 0 && isGallery && (
          <ModalGateway>
            <Modal onClose={this.closeLightbox}>
              <Carousel views={prepareGalleryData(boat.images)} />
            </Modal>
          </ModalGateway>
        )}
        {boat.layout && boat.layout.length > 0 && isLayoutGallery && (
          <ModalGateway>
            <Modal onClose={this.closeNewLightbox}>
              <Carousel views={prepareGalleryData(boat.layout)} />
            </Modal>
          </ModalGateway>
        )}
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  boat: state.boatReducer.boat || {},
  boatLookups: state.boatReducer && state.boatReducer.boatLookups,
  boatShipperList: state.boatReducer.boatShipperList,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  createSuccess: state.salesEngineReducer.createSuccess,
  errors: state.salesEngineReducer.errors,
  createBidSuccess: state.boatReducer.createBidSuccess,
  createError: state.salesEngineReducer.createError,
  reviews: state.reviewReducer && state.reviewReducer.reviews,
  isReview: state.reviewReducer && state.reviewReducer.isReview,
  boatByCityLoad: state.boatReducer && state.boatReducer.boatByCityLoad,
  boatByCitySuccess: state.boatReducer && state.boatReducer.boatByCitySuccess,
  boatByCityError: state.boatReducer && state.boatReducer.boatByCityError,
  boatByCityData: state.boatReducer && state.boatReducer.boatByCityData,
});

const mapDispatchToProps = dispatch => ({
  getBoatShipperAction: () => dispatch(getBoatShipperAction()),
  getBoatById: data => dispatch(getBoatById(data)),
  createAuctionBid: data => dispatch(createAuctionBid(data)),
  getAllBoatLookUps: () => dispatch(getAllBoatLookUps()),
  increaseBoatViewCount: data => dispatch(increaseBoatViewCount(data)),
  createSalesEngine: data => dispatch(createSalesEngine(data)),
  clearCreateSalesEngineFlag: () => dispatch(clearCreateSalesEngineFlag()),
  clearBidFlag: () => dispatch(clearBidFlag()),
  clearReviewFlag: () => dispatch(clearReviewFlag()),
  createReview: data => dispatch(createReview(data)),
  getBoatByCities: data => dispatch(getBoatByCities(data)),
  clearBoatByCity: () => dispatch(clearBoatByCity())
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatInner);
