import React, { Component, Fragment } from "react";
import { Grid, Box } from "@material-ui/core";
import { connect } from "react-redux";

import {
  getHomeVideos,
  clearCityWiseBoats,
  getRentBoatTripCities,
  getBoatRentLookUps,
  getAllRentTypes,
  getRentBoatByTripAction,
  getRentBoatMostPopularTripAction,
  getRecommendedTrips,
  getModuleWiseBanners,
  searchBoatRent,
  getExperiences
} from "../../redux/actions";
import { Layout } from "../../components/layout/layout";
import RentCard from "../../components/staticComponent/rentCard";
import { rentReviews, CardHover } from "../../components";
import { TripCarousels } from "../../components/carouselComponent/tripsCarousels";
import ExploreAdamSeaIndia from "../../components/carouselComponent/exploreAdamSeaIndia";
import { RentSharedTrips } from "../../components/gridComponents/rentSharedTrips";
import ReviewCards from "../../components/gridComponents/reviewCards";
import UserContext from "../../UserContext";
import {
  pagination,
  lookupTypes,
  moduleCategory,
  dimension,
  mediaCategory,
  advertisementCategory,
  showAll
} from "../../util/enums/enums";

import { getCategoryWiseVideos } from "../../redux/actions/VideoAction";
import { getCategoryWiseAdvertisements } from "../../redux/actions/advertisementAction";
import { RentBoatTripCitiesCard } from "../../components/staticComponent/rentBoatTripCitiesCard";
import ServicesMarinaStorage from "../../components/staticComponent/servicesMarinaStorage";
import CommonBanner from "../../components/mainBanner/commonBanner";
import RegisterCard from "../../components/staticComponent/registerCard";
import {  RentCardStyle, RentBannerImage, RentHomeCardBanner } from "../../components/styleComponent/styleComponent";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import { VideoComponent } from "../../components/videoComponent/videoComponent";
import { VideoModel } from "../../components/videoComponent/videoModel";

import "./rent.scss";
import "./rentResponsive.scss"

const recommendedTrip = {
  isBrowser: 3,
  isTablet: 3,
  isMobile: 1
};
const rentGallary = {
  isBrowser: 3,
  isTablet: 2,
  isMobile: 1
};
const ExploreAdamSeaRentCarousel = {
  isBrowser: 6,
  isTablet: 2,
  isMobile: 1
};
const rentBoatCities = {
  isBrowser: 5,
  isTablet: 2,
  isMobile: 1
};
class Rent extends Component {

  state = {
    setVideoFlag: false,
    videoUrl: "",
    videoThumbnail: "",
    isFlag: false,
    country: "",
    videoWidth: dimension.rentVideo.width,
    videoHeight: dimension.rentVideo.height,
    height: 75
  };

  static contextType = UserContext;

  static getDerivedStateFromProps(props, state) {
    const { lookUpSuccess, tripLists, getRentBoatByTripAction } = props;
    const { isFlag, country } = state;

    if (lookUpSuccess && isFlag) {
      getRentBoatByTripAction({
        page: pagination.PAGE_COUNT,
        limit: pagination.PAGE_RECORD_LIMIT,
        country,
        trip: tripLists && tripLists.length && tripLists[0].lookUp.id
      });
      getRentBoatByTripAction({
        page: pagination.PAGE_COUNT,
        limit: pagination.PAGE_RECORD_LIMIT,
        country,
        trip: tripLists && tripLists.length && tripLists[1].lookUp.id
      });
      getRentBoatByTripAction({
        page: pagination.PAGE_COUNT,
        limit: pagination.PAGE_RECORD_LIMIT,
        country,
        trip: tripLists && tripLists.length && tripLists[2].lookUp.id
      });
      return { isFlag: false };
    }
    return null;
  }

  componentDidMount() {
    const {
      getRentBoatMostPopularTripAction,
      getRentBoatTripCities,
      getBoatRentLookUps,
      getAllRentTypes,
      getRecommendedTrips,
      getCategoryWiseVideos,
      getCategoryWiseAdvertisements,
      getModuleWiseBanners,
      getExperiences
    } = this.props;
    const { country } = this.context;
    const mostPopularRequestData = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: country
    };

    getRentBoatTripCities({ country });

    getRentBoatMostPopularTripAction(mostPopularRequestData);

    getCategoryWiseAdvertisements(advertisementCategory.RENT);

    getRecommendedTrips({
      country,
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    });

    getBoatRentLookUps(lookupTypes.TRIP_TYPE);
    getAllRentTypes(lookupTypes.TRIP_TYPE);
    getExperiences("boat-rent")
    getCategoryWiseVideos({ type: moduleCategory.RENT });
    getModuleWiseBanners({
      type: mediaCategory.rent.type,
      fieldName: mediaCategory.rent.fieldName,
      isBanner: true
    });

    const videoWidth = getRatio(
      dimension,
      "rentVideo",
      ".section-heading"
    )

    const videoHeight = getHeightRatio(
      dimension,
      "rentVideo",
      ".section-heading"
    );

    const selector = document.querySelector('.header-responsive');
    const height = selector.offsetHeight;

    this.setState({ videoWidth, videoHeight, height, isFlag: true, country })
  }

  getNewWidth = () => {
    const width = document.querySelector("body");
    const actualWidth = width && width.offsetWidth / 3.5

    return actualWidth;
  };

  playVideo = video => {
    this.setState(prevState => ({
      setVideoFlag: !prevState.setVideoFlag,
      videoUrl: video.url,
      videoThumbnail: video.thumbnail
    }));
  };
  closeVideo = () => {
    this.setState({setVideoFlag:false})
  }

  getCityWiseBoatsHandler = value => {
    const { country } = this.context;
    value && window.open(`/rent-city-wise-boats/${value && value.city}/${country}`, '_blank')
  };

  getCategoryWiseBoatsHandler = value => {
    const { country } = this.context;
    value && window.open(`/rent-category-wise-boats/${value && value.id}/${country}`, '_blank')
  };

  render() {
    const {
      categoryVideos,
      rentSharedTripDataTest,
      rentMostPopularTrips,
      recommendedTrips,
      privateTrips,
      recommendedTripsPerHour,
      tripLists,
      totalRecommendedTrips,
      advertisements,
      history,
      boatRentTypes,
      rentBoatTripCities,
      rentBanner,
      experience,
    } = this.props;

    const width = this.getNewWidth();


    const { setVideoFlag, videoUrl, videoThumbnail, videoHeight, videoWidth, height } = this.state;

    return (
      <Layout className="rent-layout">
        <RentHomeCardBanner height={height} className="rent-content">
          <RentCardStyle className="rent-home" width={width}>
            <div className="rent-card-wrap">
              <RentCard tripList={tripLists} tripTypeData={boatRentTypes} />
            </div>
          </RentCardStyle>
          <RentBannerImage width={width}>
            <div className="overflow-hidden rent-home-img">
              {rentBanner && rentBanner.length > 0 && (
                <CommonBanner data={rentBanner} />
              )}
            </div>
          </RentBannerImage>
        </RentHomeCardBanner>
        <div className="container100 Rent rent-image">
          <div container>
            <Grid item xs={12}>
              <Box
                className="section-heading"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                Explore AdamSea
              </Box>
              <ServicesMarinaStorage className="mb-20" />
            </Grid>

            {boatRentTypes && boatRentTypes.length > 0 && (
              <Grid className="rent-arrow" item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Explore AdamSea Rent Services
              </Box>
                <ExploreAdamSeaIndia
                  customWidthItem={6}
                  items={ExploreAdamSeaRentCarousel}
                  carouselData={boatRentTypes}
                  getCategoryWiseBoats={value =>
                    this.getCategoryWiseBoatsHandler(value)
                  }
                />
              </Grid>
            )}

            {rentBoatTripCities && rentBoatTripCities.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Trip Destinations around the world
              </Box>
                <RentBoatTripCitiesCard
                  limit={5}
                  data={rentBoatTripCities}
                  items={rentBoatCities}
                  getCityWiseRentBoats={value =>
                    this.getCityWiseBoatsHandler(value)
                  }
                />
              </Grid>
            )}

            {recommendedTrips && recommendedTrips.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  AdamSea Recommended Trips
              </Box>

                <TripCarousels
                  customWidthItem={3}
                  itemsLength={10}
                  totalLength={totalRecommendedTrips}
                  items={recommendedTrip}
                  showType={showAll.recommended}
                  carouselData={recommendedTrips}
                  showAllText="recommended boats"
                  dimension={dimension.recommended}
                />
              </Grid>
            )}

            {rentSharedTripDataTest && rentSharedTripDataTest.length > 0 && (
              <Grid item xs={12} className="shared-trip">
                <Box
                  className="section-heading "
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  AdamSea Shared Trip with Other
              </Box>
                <RentSharedTrips
                  xs={12}
                  sm={4}
                  itemsLength={3}
                  showType={showAll.sharedTrip}
                  dimension={dimension.sharedTrip}
                  rentSharedTrips={rentSharedTripDataTest}
                  showAllText="shared trips"
                />
              </Grid>
            )}

            {rentMostPopularTrips && rentMostPopularTrips.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Most Popular Trips
              </Box>
                <RentSharedTrips
                  isBadge
                  className="most-popular-grid-home"
                  itemsLength={10}
                  dimension={dimension.popularTrip}
                  showType={showAll.mostPopular}
                  rentSharedTrips={rentMostPopularTrips}
                  showAllText="most popular trips trips"
                  isMostPopular
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 0 && (
              <Grid item xs={12}>
                <div>
                  <RegisterCard adData={advertisements[0]} history={history} isFlag />
                </div>
              </Grid>
            )}

            {privateTrips && privateTrips.length > 0 && (
              <Grid item xs={12} className="private-trip">
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Private Trip
              </Box>

                <RentSharedTrips
                  isPrivate={true}
                  xs={12}
                  sm={3}
                  itemsLength={8}
                  showType={showAll.privateTrip}
                  dimension={dimension.privateTrip}
                  rentSharedTrips={privateTrips}
                  showAllText="private trips"
                />
              </Grid>
            )}

            {recommendedTripsPerHour && recommendedTripsPerHour.length > 0 && (
              <Grid item xs={12} className="adamsea-rent-per-hour">
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  AdamSea RENT per Hour
              </Box>

                <RentSharedTrips
                  isRentPerHour={true}
                  xs={12}
                  sm={3}
                  itemsLength={8}
                  rentSharedTrips={recommendedTripsPerHour}
                  showType={showAll.tripsPerHour}
                  dimension={dimension.perHourTrip}
                  showAllText="rent per hour trips"
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 1 && (
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item sm={6}>
                    <div className="double-card">
                      <RegisterCard isSpace isDouble adData={advertisements[1]} history={history} isFlag />
                    </div>
                  </Grid>
                  {advertisements && advertisements.length > 2 && (
                    <Grid item sm={6}>
                      <div className="double-card">
                        <RegisterCard isSpace2 isDouble adData={advertisements[2]} history={history} isFlag />
                      </div>
                    </Grid>
                  )}
                </Grid>
              </Grid>
            )}

            {/* [NOTE] Below commented code not removed @miraj*/}

            {/* <Grid item xs={12}>
              <Box
                className="section-heading"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                AdamSea Rent Gallery
              </Box>

              <TripCarousels
                customWidthItem={2}
                items={rentGallary}
                carouselData={rentGallery}
              />
            </Grid> */}

            {experience && experience.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Live the Experience with us
              </Box>
                <ReviewCards
                  isRent
                  xs={12}
                  sm={3}
                  experience={experience}
                />
              </Grid>
            )}
            {categoryVideos && categoryVideos.length > 0 && (
              <div item xs={12} className="rent-video-section">
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                ></Box>

                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    {categoryVideos &&
                      categoryVideos.map((video, index) => {
                        if (index < 3) {
                          return (
                            <Fragment key={video.id}>
                              <VideoComponent
                                setVideoUrlHandler={() => this.playVideo(video)}
                                video={video}
                                videoWidth={videoWidth}
                                videoHeight={videoHeight}
                              />
                            </Fragment>
                          );
                        }
                      })}
                  </Grid>
                  <VideoModel
                    videoFlag={setVideoFlag}
                    videoUrl={videoUrl}
                    thumbnail={videoThumbnail}
                    closeVideo={this.closeVideo}
                  />
                </Grid>
              </div>
            )}
          </div>
        </div>
        
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  videos: state.dashboardReducer.videos,
  categoryVideos: state.videoReducer && state.videoReducer.categoryVideos,
  rentSharedTripDataTest:
    state.rentReducer && state.rentReducer.rentSharedTrips,
  rentMostPopularTrips:
    state.rentReducer && state.rentReducer.rentMostPopularTrips,
  rentMostPopularTripsTotal:
    state.rentReducer && state.rentReducer.rentMostPopularTripsTotal,
  privateTrips: state.rentReducer.privateTrips,
  recommendedTripsPerHour: state.rentReducer.recommendedTripsPerHour,
  tripTypes: state.rentReducer.tripTypes,
  boatRentTypes: state.boatRentReducer.boatRentTypes,
  recommendedTrips: state.rentReducer.recommendedTrips,
  tripsPerHourTotal: state.rentReducer.tripsPerHourTotal,
  privateTripsTotal: state.rentReducer.privateTripsTotal,
  rentSharedTripsTotal: state.rentReducer.rentSharedTripsTotal,
  totalRecommendedTrips: state.rentReducer.totalRecommendedTrips,
  tripLists:
    state.boatRentReducer &&
    state.boatRentReducer.boatRentLookUps &&
    state.boatRentReducer.boatRentLookUps.trip,
  lookUpSuccess: state.boatRentReducer && state.boatRentReducer.lookUpSuccess,
  advertisements: state.advertisementReducer.advertisements,
  rentBoatTripCities: state.rentReducer.rentBoatTripCities,
  experience: state.dashboardReducer.experience,
  rentBanner:
    state.dashboardReducer &&
    state.dashboardReducer[mediaCategory.rent.fieldName],

});

const mapDispatchToProps = dispatch => ({
  getHomeVideos: () => dispatch(getHomeVideos()),
  getRentBoatByTripAction: data => dispatch(getRentBoatByTripAction(data)),
  getCategoryWiseVideos: data => dispatch(getCategoryWiseVideos(data)),
  getRentBoatMostPopularTripAction: data =>
    dispatch(getRentBoatMostPopularTripAction(data)),
  getRentBoatTripCities: data => dispatch(getRentBoatTripCities(data)),
  clearCityWiseBoats: () => dispatch(clearCityWiseBoats()),
  getBoatRentLookUps: data => dispatch(getBoatRentLookUps(data)),
  getAllRentTypes: data => dispatch(getAllRentTypes(data)),
  getExperiences: data => dispatch(getExperiences(data)),
  getRecommendedTrips: data => dispatch(getRecommendedTrips(data)),
  // searchBoatRent: data => dispatch(searchBoatRent(data)),
  getCategoryWiseAdvertisements: data =>
    dispatch(getCategoryWiseAdvertisements(data)),
  getModuleWiseBanners: type => dispatch(getModuleWiseBanners(type))
});

export default connect(mapStateToProps, mapDispatchToProps)(Rent);
