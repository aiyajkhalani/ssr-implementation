import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  TextField,
  FormControlLabel,
  Checkbox,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FaEyeSlash } from "react-icons/fa";
import { FaEye } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

import { login, clearAuthorizationFlag, getCategoriesWiseBanners } from "../../redux/actions";
import { ErrorNotify } from "../../helpers/notification";
import { Notification } from "../../components";
import TermAndPolicy from "../../components/termAndPolicy";
import Captcha from "../../components/helper/captcha";

import "./login.scss";
import ErrorComponent from "../../components/error/errorComponent";
import { LogInSignupBanner } from "../../components/styleComponent/styleComponent";

class Login extends Component {
  state = {
    user: {
      username: "",
      password: "",
      captchaValue: "",
      // onChecked: false
    },
    type: "password",
    wording: <FaEye />,
    rememberMe: false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isAuthorized, history, clearAuthFlag, isError } = nextProps;

    if (isAuthorized) {
      history.push("/");
      clearAuthFlag();
    }

    return null;
  }
  componentDidMount() {
    const { clearAuthFlag, getCategoriesWiseBanners } = this.props;
    clearAuthFlag();
    const logInInput = {
      type: "login",
      fieldName: "logInBanner"
    }
    getCategoriesWiseBanners(logInInput)
  }

  changeState = () => {
    var oldState = this.state.type;
    var isTextOrHide = oldState === "password";
    var newState = isTextOrHide ? "text" : "password";
    var newWord = isTextOrHide ? <FaEyeSlash /> : <FaEye />;
    this.setState({
      type: newState,
      wording: newWord
    });
  };

  onClickCheckBox = () => {
    this.setState(prevState => ({
      rememberMe: !prevState.rememberMe
    }));
  };

  render() {
    const { user, onChecked, rememberMe } = this.state;
    const { login, isError, errorMessage, logInBanners } = this.props;
    return (
      <>
        <Notification />
        <Grid container className="flex-1 overflow-hidden login-responsive" alignItems="center">
          <Grid item className="h100-vh img-holder" sm={6}>
            <LogInSignupBanner img={logInBanners && logInBanners.length ? encodeURI(logInBanners[0].url) : null}>
              <div className="website-logo ml-50">
                <Link to="/">
                  <div className="logo">
                    <img
                      className="logo-size"
                      src={require("../../assets/images/login/logo-white.png")}
                    />
                  </div>
                </Link>
              </div>
              <Grid container className="flex-1" alignItems="center">
                <div className="d-flex justify-content-center align-items-start flex-column loginMain-div">
                  <h3 className="text-white">Welcome</h3>
                  <h3 className="text-white">Back To Adamsea</h3>

                  <span className="text-white loginSide-text">
                    Login to see your favorite boats,
                </span>
                  <span className="text-white loginOffer-text">
                    exclusive offers and more
                </span>
                  <TermAndPolicy />
                </div>
              </Grid>
            </LogInSignupBanner>
          </Grid>
          <Grid item sm={6}>
            <Formik
              initialValues={user}
              validationSchema={Yup.object().shape({
                username: Yup.string()
                  .email("Email is invalid")
                  .required("Email is required"),
                password: Yup.string()
                  // .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, "Password Must be of 8 Character")
                  .required("Password is required"),
                captchaValue: Yup.string().required("Captcha Validation is required"),
              })}
              onSubmit={values => login({ username: values.username, password: values.password, })}
              render={({
                errors,
                status,
                touched,
                setFieldValue,
                handleSubmit
              }) => (
                  <div className="form-holder">
                    <div className="form-content">
                      <div className="form-items login-form-width">
                        <h3>Log in</h3>
                        <div className="page-links">
                          <p className="font-16">
                            Don’t have account as member?
                          <Link to="/register" className="font-weight-500">
                              {" Sign up"}
                            </Link>
                          </p>
                        </div>
                        <Form>
                          <div className="position-relative login-field">
                            <input
                              id="username"
                              name="username"
                              type="text"
                              className={
                                "form-control font-16" +
                                (errors.username && touched.username
                                  ? " is-invalid"
                                  : "")
                              }
                              placeholder="Email Address"
                              onChange={e =>
                                setFieldValue("username", e.target.value)
                              }
                              required
                            ></input>
                            <span className="form-side-icon">
                              <MdEmail />
                            </span>

                            <ErrorMessage
                              name="username"
                              component="div"
                              className="invalid-feedback"
                            />
                          </div>
                          <div className="position-relative login-field">
                            <input
                              id="password"
                              name="password"
                              type={this.state.type}
                              className={
                                "form-control mt-10 font-16" +
                                (errors.password && touched.password
                                  ? " is-invalid"
                                  : "")
                              }
                              placeholder="Password"
                              onChange={e =>
                                setFieldValue("password", e.target.value)
                              }
                              required
                            ></input>
                            <span
                              className="form-side-icon"
                              onClick={this.changeState}
                            >
                              {this.state.wording}
                            </span>
                            <ErrorMessage
                              name="password"
                              component="div"
                              className="invalid-feedback"
                            />
                          </div>

                          <Grid container className="mt-10">
                            <Grid item xs>
                              <div className="clearfix"></div>
                            </Grid>
                            <Link
                              to="/send-reset-password-link"
                              className="float-right forgetTextStyle font-14"
                            >
                              Forget password?
                          </Link>
                          </Grid>

                          <div className="position-relative login-field error-captcha">
                            <Captcha
                              onChange={(value) => setFieldValue('captchaValue', value)}
                            />
                            {errors && errors.captchaValue &&
                              <div className="invalid-feedback-error">{errors.captchaValue}</div>
                            }
                          </div>

                          <div className="clearfix mt-3">
                            <div
                              className="custom-control custom-checkbox float-left mb-none d-flex align-items-center h-100 remember-text"
                              onClick={this.onClickCheckBox}
                            >
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id="rememberme"
                                checked={rememberMe}
                              />
                              <span
                                className="custom-control-label cursor-pointer"
                                for="remember me font-16"
                              >
                                Remember me
                            </span>
                            </div>
                          </div>
                          <div className="clearfix mt-3" onClick={handleSubmit}>
                            <a
                              id="submit"
                              type=""
                              className="btn btn-block btn-lg btn-primary font-16 font-weight-500 w-100 cursor-pointer"
                            >
                              Log in
                          </a>
                          </div>
                          {isError && <ErrorComponent errors={errorMessage} />}
                        </Form>
                      </div>
                    </div>
                  </div>
                )}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isError: state.loginReducer.isError,
  errorMessage: state.errorReducer.errorMessage,
  isAuthorized: state.loginReducer.isAuthorized,
  logInBanners: state.bannerReducer && state.bannerReducer["logInBanner"],
});

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(login(data)),
  clearAuthFlag: () => dispatch(clearAuthorizationFlag()),
  getCategoriesWiseBanners: (data) => (dispatch(getCategoriesWiseBanners(data))),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
