import React, { Component } from 'react';
import { connect } from 'react-redux'
import uuid from 'uuid/v4';

import { Layout } from '../../components';
import { resultMessage } from '../../util/enums/enums';
import { Grid, Box } from '@material-ui/core';
import { BoatAddedService } from '../../components/gridComponents/boatAddedService';


class SearchBoatServices extends Component {

    render() {

        const { searchedYachtServices, totalSearchedYachtServices } = this.props

        return (
            <Layout isFooter>
                <Grid item xs={12}>
                    {searchedYachtServices && searchedYachtServices.length > 0 ?

                        (searchedYachtServices.map((value, index) => {
                            return (
                                <>
                                    {index < 6 && (
                                        <Grid key={uuid()} item xs={12} sm={4}>
                                            <BoatAddedService value={value} xs={12} sm={4} iconColor='iconColor-boatInner' />
                                        </Grid>
                                    )}
                                </>
                            );
                        })
                        )
                        :
                        (
                            <Grid item className="min-defualt-height" xs={10}>
                                <>
                                    <Box
                                        className="section-heading mb-15"
                                        fontSize={24}
                                        fontWeight={500}
                                        letterSpacing={0.5}
                                    >
                                        No Results
                          </Box>

                                    <span>{resultMessage.search}</span>
                                </>
                            </Grid>
                        )
                    }
                </Grid>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    searchedYachtServices: state.boatServiceReducer.searchedYachtServices,
    totalSearchedYachtServices: state.boatServiceReducer.totalSearchedYachtServices,
})

const mapDispatchToProps = dispatch => ({
    //
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchBoatServices) 