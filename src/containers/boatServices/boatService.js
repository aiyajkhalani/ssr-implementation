import React, { Component, Fragment } from "react";
import { Grid, Box } from "@material-ui/core";
import "../../../node_modules/video-react/styles/scss/video-react.scss";
import { connect } from "react-redux";

import {
  getHomeVideos,
  getRecentlyAddedServices,
  getMostViewedBoatServices,
  searchYachtService,
  getAllBoatServiceTypes,
  getModuleWiseBanners,
  getExperiences
} from "../../redux/actions";
import { Layout } from "../../components/layout/layout";
import BoatServicesAdded from "../../components/gridComponents/boatAddedServices";
import RegisterCard from "../../components/staticComponent/registerCard";
import { GalleryCarousal } from "../../components/carouselComponent/relatedGalleryCarousel";
import ReviewCards from "../../components/gridComponents/reviewCards";
import BoatServiceCard from "../../components/staticComponent/boatServiceCard";

import "./boatServices.scss";
import ExploreAdamSeaIndia from "../../components/carouselComponent/exploreAdamSeaIndia";
import {
  pagination,
  moduleCategory,
  mediaCategory,
  dimension,
  advertisementCategory,
  showAllService
} from "../../util/enums/enums";
import { getCategoryWiseVideos } from "../../redux/actions/VideoAction";
import UserContext from "../../UserContext";
import { getCategoryWiseAdvertisements } from "../../redux/actions/advertisementAction";
import ServicesMarinaStorage from "../../components/staticComponent/servicesMarinaStorage";
import CommonBanner from "../../components/mainBanner/commonBanner";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import "../../../src/styles/boatServiceResponsive.scss";
import { VideoModel } from "../../components/videoComponent/videoModel";
import { VideoComponent } from "../../components/videoComponent/videoComponent";

const galleryCarouselItem = {
  isBrowser: 5,
  isTablet: 2,
  isMobile: 1
};

const ExploreAdamSeaBoatCarousel = {
  isBrowser: 6,
  isTablet: 2,
  isMobile: 1
};


class BoatService extends Component {
  static contextType = UserContext;

  constructor(props, context) {
    super(props, context);
    this.newShortcuts = [
      {
        keyCode: 32, // spacebar
        disable: false,
        handle: function () { } // a function that does nothing
      }
    ];
  }
  state = {
    setVideoFlag: false,
    videoUrl: "",
    videoThumbnail: "",
    data: {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context && this.context.country
    },
    videoHeight: dimension.boatServiceVideo.height,
    videoWidth: dimension.boatServiceVideo.width
  };

  static getDerivedStateFromProps(nextProps) {
    const { isYachtSearched, history } = nextProps;

    if (isYachtSearched) {
      history.push("/search-boat-services");
    }
    return null;
  }

  playVideo = video => {
    this.setState(prevState => ({
      setVideoFlag: !prevState.setVideoFlag,
      videoUrl: video.url,
      videoThumbnail: video.thumbnail
    }));
  };
  
  closeVideo = () => {
    this.setState({ setVideoFlag: false })
  }
  async componentDidMount() {
    const {
      getAllBoatServiceTypes,
      getRecentlyAddedServices,
      getCategoryWiseVideos,
      getMostViewedBoatServices,
      getCategoryWiseAdvertisements,
      getModuleWiseBanners,
      getExperiences
    } = this.props;
    const { data } = this.state;
    getCategoryWiseVideos({
      type: moduleCategory.BOAT_SERVICE,
      metatype: "video"
    });
    getModuleWiseBanners({
      type: mediaCategory.boatService.type,
      fieldName: mediaCategory.boatService.fieldName,
      isBanner: true
    });
    getExperiences("service")

    getCategoryWiseAdvertisements(advertisementCategory.BOAT_SERVICE);

    await getAllBoatServiceTypes();
    if (data) {
      await getRecentlyAddedServices(data);
      await getMostViewedBoatServices(data);
    }

    const videoWidth = getRatio(
      dimension,
      "boatServiceVideo",
      ".section-heading"
    );

    const videoHeight = getHeightRatio(
      dimension,
      "boatServiceVideo",
      ".section-heading"
    );

    this.setState({ videoWidth, videoHeight });
  }

  redirectHandler = value => {

    value && window && window.open(`/view-boat-service/${value.id}`)

  };

  render() {
    const {
      videos,
      providedServices,
      history,
      recentlyAddedService,
      categoryVideos,
      mostViewedBoatServices,
      searchYachtService,
      boatServiceBanner,
      advertisements,
      mostViewedBoatServicesTotal,
      experience
    } = this.props;
    const {
      setVideoFlag,
      videoUrl,
      videoThumbnail,
      videoWidth,
      videoHeight
    } = this.state;
    return (
      <Layout isHeader="boat-service-logo" className="boat-service-layout">
        <Grid container>
          <Grid item xs={12}>
            <div className="boat-service-img">
              {boatServiceBanner && boatServiceBanner.length > 0 && (
                <>
                  <CommonBanner data={boatServiceBanner} />
                  <div className="service-card-banner">
                    <BoatServiceCard
                      iconBg="boatInner-btnBg"
                      search={searchYachtService}
                      services={providedServices}
                    />
                  </div>
                </>
              )}
            </div>
          </Grid>
        </Grid>
        <div className="container100">
          <Grid container>
            <Grid item xs={12}>
              <Box
                className="section-heading"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                Explore AdamSea
              </Box>
              <ServicesMarinaStorage className="mb-20" />
            </Grid>

            {providedServices && providedServices.length > 0 && (
              <Grid
                className="boat-service-arrow boat-service-explore-india-div"
                item
                xs={12}
              >
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Explore AdamSea Boat Services in India
              </Box>

                <ExploreAdamSeaIndia
                  customWidthItem={5}
                  items={ExploreAdamSeaBoatCarousel}
                  carouselData={providedServices}
                  btnColor="boatInner-btnBg"
                  getCategoryWiseBoats={value => this.redirectHandler(value)}
                />
              </Grid>
            )}

            {recentlyAddedService && recentlyAddedService.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Recently Added Services
              </Box>
                <BoatServicesAdded
                  xs={12}
                  sm={4}
                  boatAddedServices={recentlyAddedService}
                  iconColor="iconColor-boatInner"
                  showType={showAllService.recentlyAdded}
                  showAllText="recently added services"
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 0 && (
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12}>
                    <div>
                      <RegisterCard adData={advertisements[0]} />
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            )}

            {mostViewedBoatServices && mostViewedBoatServices.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Top Rated AdamSea Boat Services
              </Box>
                <GalleryCarousal
                  items={galleryCarouselItem}
                  total={mostViewedBoatServicesTotal}
                  carouselData={mostViewedBoatServices}
                  btnColor="boatInner-btnBg"
                  showAllText="top rated adamsea boat service"
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 1 && (
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <div className="double-card">
                      <RegisterCard isDouble isSpace adData={advertisements[1]} />
                    </div>
                  </Grid>
                  {advertisements && advertisements.length > 2 && (
                    <Grid item xs={6}>
                      <div className="double-card">
                        <RegisterCard isDouble isSpace2 adData={advertisements[2]} />
                      </div>
                    </Grid>
                  )}
                </Grid>
              </Grid>
            )}

            {/** @miraj as per discuss not removed commented code. */}
            {/* <Grid item xs={12}>
              <Box
                className="section-heading"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                Top Rated AdamSea Baot Services
              </Box>
              <ReviewCards
                xs={12}
                sm={3}
                reviewCard={rentReviews}
                iconColor="iconColor-boatInner"
              />
            </Grid> */}
            {experience && experience.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Live the Experience with us
              </Box>
                <ReviewCards
                  isService
                  xs={12}
                  sm={3}
                  experience={experience}
                />
              </Grid>
            )}
            {categoryVideos && categoryVideos.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                ></Box>

                <Grid item xs={12} className="boat-service-video-div">
                  <Grid container spacing={2}>
                    {categoryVideos &&
                      categoryVideos.map((video, index) => {
                        if (index < 3) {
                          return (
                            <Fragment key={video.id}>
                              <VideoComponent
                                setVideoUrlHandler={() => this.playVideo(video)}
                                video={video}
                                videoWidth={videoWidth}
                                videoHeight={videoHeight}
                              />
                            </Fragment>
                          );
                        }
                      })}
                  </Grid>
                  <VideoModel
                    videoFlag={setVideoFlag}
                    videoUrl={videoUrl}
                    thumbnail={videoThumbnail}
                    closeVideo={this.closeVideo}
                  />
                </Grid>
              </Grid>
            )}

          </Grid>
        </div>
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  providedServices:
    state.boatServiceReducer && state.boatServiceReducer.providedServices,
  recentlyAddedService:
    state.boatServiceReducer && state.boatServiceReducer.recentlyAddedService,
  videos: state.dashboardReducer.videos,
  user: state.loginReducer && state.loginReducer.currentUser,
  categoryVideos: state.videoReducer && state.videoReducer.categoryVideos,
  mostViewedBoatServices:
    state.boatServiceReducer && state.boatServiceReducer.mostViewedBoatServices,
  mostViewedBoatServicesTotal:
    state.boatServiceReducer && state.boatServiceReducer.total,
  isYachtSearched: state.boatServiceReducer.isYachtSearched,
  advertisements: state.advertisementReducer.advertisements,
  experience: state.dashboardReducer.experience,
  boatServiceBanner:
    state.dashboardReducer &&
    state.dashboardReducer[mediaCategory.boatService.fieldName]
});

const mapDispatchToProps = dispatch => ({
  getHomeVideos: () => dispatch(getHomeVideos()),
  getAllBoatServiceTypes: () => dispatch(getAllBoatServiceTypes()),
  getRecentlyAddedServices: data => dispatch(getRecentlyAddedServices(data)),
  getCategoryWiseVideos: data => dispatch(getCategoryWiseVideos(data)),
  getMostViewedBoatServices: data => dispatch(getMostViewedBoatServices(data)),
  searchYachtService: data => dispatch(searchYachtService(data)),
  getCategoryWiseAdvertisements: data =>
    dispatch(getCategoryWiseAdvertisements(data)),
  getModuleWiseBanners: type => dispatch(getModuleWiseBanners(type)),
  getExperiences: data => dispatch(getExperiences(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatService);
