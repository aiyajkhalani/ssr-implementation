import React, { Component } from "react";
import { connect } from "react-redux";
import * as Yup from "yup";
import uuid from "uuid/v4";
import ImageUploader from "react-images-upload";
import { ErrorMessage, Formik, Form } from "formik";
import {
  Grid,
  Card,
  Box,
  CardContent,
  Button,
  Select,
  Chip
} from "@material-ui/core";
import { Row, Col } from "react-bootstrap";
import HelpIcon from '@material-ui/icons/Help';

import { Loader, Field } from "../../components";
import GoogleMap from "../../components/map/map";
import { cancelHandler } from "../../helpers/routeHelper";
import { SuccessNotify, ErrorNotify } from "../../helpers/notification";
import { uploadToS3 } from "../../helpers/s3FileUpload";
import { randomAdId, requireMessage, getIds } from "../../helpers/string";
import { latLng } from "../../util/enums/enums";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import {
  addBoatService,
  clearEditFlag,
  getUserBoatService,
  getAllBoatServiceTypes,
  clearErrorMessageShow
} from "../../redux/actions";
import { renderSelectOptions } from "../../helpers/string"
import MultiSelect from "../../components/helper/multiSelect";

import "./boatServices.scss";
import ErrorComponent from "../../components/error/errorComponent";

class AddBoatService extends Component {
  constructor(props) {
    super(props);
    const { userService } = props;
    this.state = {
      selected: [],
      latLng: latLng,
      boatService: {
        address: "",
        country: "",
        state: "",
        city: "",
        placeName: "",
        postalCode: "",
        route: "",
        service: "",
        reviews: "",
        featuresAndAdvantages: "",
        teamCommitment: "",
        qualityOfService: "",
        serviceProvide: [],
        salesManFullName: "",
        contactOfSalesMan: "",
        uploadSalesManPhoto: userService ? userService.uploadSalesManPhoto : "",
        youtubeUrl: "",
        images: userService ? userService.images : []
      },
      adId: "",
      mediaUpdate: false,
      oldSwp: [],
      location: {},
      infoSWP: false,
      infoSalesMan: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      isAddSuccess,
      isAddError,
      history,
      clearEditFlag
    } = nextProps;
    const { adId } = prevState;

    if (isAddSuccess) {
      setTimeout(
        () => SuccessNotify("Boat Service is added successfully"),
        100
      );
      clearEditFlag();
      history.push("/dashboard");
    } else if (isAddError) {
      ErrorNotify("Error occurred");
    } else if (!adId) {
      return {
        adId: randomAdId("BS")
      };
    }

    return null;
  }

  componentDidMount() {
    const { getAllBoatServiceTypes, getUserBoatService } = this.props;
    getAllBoatServiceTypes()
    getUserBoatService();
  }

  fetchMapInfo = (result, setValue) => {
    const { location } = this.state;
    const {
      address,
      country,
      state,
      city,
      placeName,
      postalCode,
      route,
      latitude,
      longitude
    } = result;

    location.address = address || "";
    location.country = country;
    location.state = state;
    location.city = city;
    location.placeName = placeName;
    location.postalCode = postalCode;
    location.route = route;
    location.latitude = latitude;
    location.longitude = longitude;

    setValue("address", location.address);
    this.setState({ location });
  };

  handleFileUpload = async (file, name, value, setValue) => {
    const { boatService } = this.state;
    let oldValues = boatService[name];
    let formValue = value || [];

    if (file && file.length) {
      let res = await uploadToS3(file[file.length - 1]);

      if (res && res.location) {
        const newValues = oldValues.concat(res.location);
        setValue(name, formValue.concat(res.location));
        boatService[name] = newValues;
        this.setState({ boatService });
      }
    }
  };

  handleFileDelete = async (index, name, setValue) => {
    const { boatService } = this.state;

    const oldValues = boatService[name];

    if (oldValues && oldValues.length) {
      boatService[name].splice(index, 1);

      setValue(name, boatService[name]);
      this.setState({ boatService });
    }
  };

  handleChange = e => {
    this.setState({ selected: e.target.value });
  };

  prepareServiceValue = () => {
    const { userService } = this.props;
    const { boatService, mediaUpdated, adId, location } = this.state;

    if (userService && userService.hasOwnProperty("id")) {
      if (!mediaUpdated) {
        const {
          address,
          country,
          state,
          city,
          placeName,
          geometricLocation,
          postalCode,
          route,
          service,
          reviews,
          featuresAndAdvantages,
          teamCommitment,
          qualityOfService,
          serviceProvide,
          salesManFullName,
          contactOfSalesMan,
          uploadSalesManPhoto,
          youtubeUrl,
          images
        } = userService;

        location.address = address || "";
        location.country = country;
        location.state = state;
        location.city = city;
        location.placeName = placeName;
        location.postalCode = postalCode;
        location.route = route;
        location.latitude = geometricLocation && geometricLocation.coordinates.length > 0 && geometricLocation.coordinates[1];
				location.longitude = geometricLocation && geometricLocation.coordinates.length > 0 && geometricLocation.coordinates[0];

        boatService.service = service;
        boatService.reviews = reviews;
        boatService.featuresAndAdvantages = featuresAndAdvantages;
        boatService.teamCommitment = teamCommitment;
        boatService.qualityOfService = qualityOfService;
        boatService.salesManFullName = salesManFullName;
        boatService.contactOfSalesMan = contactOfSalesMan;
        boatService.uploadSalesManPhoto = uploadSalesManPhoto;
        boatService.youtubeUrl = youtubeUrl;
        boatService.images = images;        
        
        this.setState({
          boatService,
          location,
          latLng: { lat:  location.latitude, lng: location.longitude },                
          adId,
          mediaUpdated: true
        });
      }
      // delete extra field from boat-service object

      const { createdAt, __typename, ...filteredUserService } = userService

      return {
        ...filteredUserService
      };
    }
  };

  renderInfo = (setFieldValue, values) => {
    const { boatService, infoSWP } = this.state;
    let { providedServices } = this.props;
    const { serviceProvide } = boatService;

    providedServices = renderSelectOptions(providedServices,"name", "id")

    return (
      <Card className="card-style h-100 m-0 box-shadow-none map-div border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2 overflow-card-visible">
        <CardContent className="h-100">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box fontSize={20} letterSpacing={1} fontWeight={600}>
                Information
              </Box>
            </Grid>

            <Grid item xs={6}>
              <label className="required">Describe Your Services</label>
              <textarea
                className="form-control"
                placeholder="Describe Your Services"
                name="service"
                value={values.service}
                onChange={e => setFieldValue("service", e.target.value)}
              />
              <ErrorMessage
                component="div"
                name="service"
                className="error-message"
              />
            </Grid>

            <Grid item xs={6}>
              <label className="required">Team Commitment</label>
              <textarea
                className="form-control"
                placeholder="Team Commitment"
                name="teamCommitment"
                value={values.teamCommitment}
                onChange={e => setFieldValue("teamCommitment", e.target.value)}
              />
              <ErrorMessage
                component="div"
                name="teamCommitment"
                className="error-message"
              />
            </Grid>

            <Grid item xs={6}>
              <label className="required">Reviews</label>
              <textarea
                className="form-control"
                placeholder="Reviews"
                name="reviews"
                value={values.reviews}
                onChange={e => setFieldValue("reviews", e.target.value)}
              />
              <ErrorMessage
                component="div"
                name="reviews"
                className="error-message"
              />
            </Grid>

            <Grid item xs={6}>
              <label className="required">Our Quality Of Service</label>
              <textarea
                className="form-control"
                placeholder="Our Quality Of Service"
                name="qualityOfService"
                value={values.qualityOfService}
                onChange={e =>
                  setFieldValue("qualityOfService", e.target.value)
                }
              />
              <ErrorMessage
                component="div"
                name="qualityOfService"
                className="error-message"
              />
            </Grid>

            <Grid item xs={12}>
              <label className="required">Our Features And Advantage</label>
              <textarea
                className="form-control "
                placeholder="Our Features And Advantage"
                name="featuresAndAdvantages"
                value={values.featuresAndAdvantages}
                onChange={e =>
                  setFieldValue("featuresAndAdvantages", e.target.value)
                }
              />
              <ErrorMessage
                component="div"
                name="featuresAndAdvantages"
                className="error-message"
              />
            </Grid>

            <Grid item xs={12} className="boat-service-drop-down multi-select-chip-margin">
              <div className="d-flex justify-content-between">
                <label className="required">Service We Provide  </label> 
                <div className="position-relative">
                  <HelpIcon onMouseEnter={() => { this.setState({ infoSWP: true }) }} onMouseLeave={() => { this.setState({ infoSWP: false }) }}  style={{ height: '15px', width: '15px' }} />
                    {infoSWP && <span class="info-class">Click in the box and select multiple items</span>} 
                </div>
              </div>
              {
                 serviceProvide && (
                  <MultiSelect
                  selectedOption={values.serviceProvide}
                  onChangeValue={item => {
                    setFieldValue("serviceProvide", [...item])
                  }}
                  options={providedServices} />)
              }
             
              <ErrorMessage
                component="div"
                name="serviceProvide"
                className="error-message"
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  };

  renderSalesmanInfo = (setFieldValue, values) => {    

    const { infoSalesMan } = this.state

    return (
      <Card className="card-style h-100 m-0 box-shadow-none map-div border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2">
        <CardContent className="h-100 m-auto">
          <Grid container spacing={2} className="h-100 m-auto">
                <Grid item xs={12}>
                  <Box fontSize={20} letterSpacing={1} fontWeight={600}>
                    Salesman Contact Information
                  </Box>
                </Grid>
            <Row className="width-100 pl-2 mt-auto mb-auto">
              <Col xs={6}>

                <div>
                  <div className="mb-3">
                    <label className="required">
                      Full Name Of Salesman To Contact{" "}
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Full Name Of Salesman To Contact "
                      name="salesManFullName"
                      value={values.salesManFullName}
                      onChange={e =>
                        setFieldValue("salesManFullName", e.target.value)
                      }
                    />
                    <ErrorMessage
                      component="div"
                      name="salesManFullName"
                      className="error-message"
                    />
                  </div>
                  <div>
                    <label className="required">Contact No. Of Salesman</label>
                    <input
                      type="number"
                      className="form-control"
                      placeholder="Contact No. Of Salesman"
                      name="contactOfSalesMan"
                      value={values.contactOfSalesMan}
                      onChange={e =>
                        setFieldValue("contactOfSalesMan", e.target.value)
                      }
                    />
                    <ErrorMessage
                      component="div"
                      name="contactOfSalesMan"
                      className="error-message"
                    />
                  </div>
                </div>
              </Col>
              <Col xs={6}>
                <Grid item xs={12} className="d-flex justify-content-center">
                  <Box fontSize={20} letterSpacing={1} fontWeight={600}></Box>
                  <div className="d-flex justify-content-between">
                <label className="required">SalesMan Photo </label> 
                <div className="position-relative">
                  <HelpIcon onMouseEnter={() => { this.setState({ infoSalesMan: true }) }} onMouseLeave={() => { this.setState({ infoSalesMan: false }) }}  style={{ height: '15px', width: '15px' }} />
                    {infoSalesMan && <span class="info-class">Add Your sales man photo here so any one can contact directly</span>} 
                </div>
              </div>
                    <div className="field dashboard-profile-label">
                        <Field
                          id="uploadSalesManPhoto"
                          name="uploadSalesManPhoto"
                          type="single-image"
                          value={values.uploadSalesManPhoto}
                          onChangeText={setFieldValue}
                          required
                        />
                        <ErrorMessage
                          component="div"
                          name="uploadSalesManPhoto"
                          className="error-message"
                        />
                      </div>
                </Grid>
              </Col>
            </Row>
          </Grid>
        </CardContent>
      </Card>
    );
  };

  renderUploadPictures = (setFieldValue, values) => {
    const { boatService } = this.state;
    const { images } = boatService;

    return (
      <Card
        className="card-style h-100 m-0 box-shadow-none map-div border-top-right-radius-0 border-top-left-radius-0 
      border-bottom-right-radius-2 border-bottom-left-radius-2"
      >
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={6}>
            <Grid item xs={12} className="mb-3">
              <Box fontSize={20} letterSpacing={1} fontWeight={600}>
                Upload Pictures and Video
              </Box>
            </Grid>

              <label>YouTube URL</label>
              <input
                type="text"
                className="form-control"
                placeholder="YouTube URL"
                name="youtubeUrl"
                value={values.youtubeUrl}
                onChange={e => setFieldValue("youtubeUrl", e.target.value)}
              />
              <ErrorMessage
                component="div"
                name="youtubeUrl"
                className="error-message"
              />
            </Grid>

            <Grid item xs={6}>
              <div className="d-flex flex-wrap justify-content-center">
                {/* <label className="required">Upload Pictures</label> */}
                {images && images.length
                  ? images.map((img, index) => (
                      <>
                        <div className="addBoat-container mr-2 mb-2">
                          <img
                            src={img}
                            key={uuid()}
                            alt="uploaded pic"
                            height="100px"
                            width="100px"
                          />
                          <span
                            onClick={() =>
                              this.handleFileDelete(
                                index,
                                "images",
                                setFieldValue
                              )
                            }
                          >
                            ×
                          </span>
                        </div>
                      </>
                    ))
                  : null}
              </div>
              <div className="addBoatShow-imgUploader required">
                <ImageUploader
                  withIcon={true}
                  buttonText="Upload Pictures"
                  onChange={file =>
                    this.handleFileUpload(
                      file,
                      "images",
                      values.images,
                      setFieldValue
                    )
                  }
                  maxFileSize={5242880}
                />
                <ErrorMessage
                  component="div"
                  name="images"
                  className="error-message"
                />
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  };

  cancelHandler = () => {
    const { history, clearEditFlag } = this.props;
    clearEditFlag();
    cancelHandler(history);
  };

  render() {
    const { latLng, boatService, adId, location } = this.state;
    const { addBoatService, userService, isLoading, isAddError, errorMessage } = this.props;
    const initValue = userService ? this.prepareServiceValue() : boatService;
    
    return (
      <DashboardLayout>
        <div className="pr-3 pl-3">
          {isLoading ? (
            <Loader></Loader>
          ) : (
            <Formik
              initialValues={initValue}
              validationSchema={
                Yup.object().shape({
                address: Yup.string().required(requireMessage("Service Location")),
                service: Yup.string().required(
                  requireMessage("Describe Your Services")
                ),
                reviews: Yup.string().required(requireMessage("Review")),
                featuresAndAdvantages: Yup.string().required(
                  requireMessage("Features And Advantage")
                ),
                teamCommitment: Yup.string().required(
                  requireMessage("Team Commitment")
                ),
                qualityOfService: Yup.string().required(
                  requireMessage("Quality Of Service")
                ),
                serviceProvide: Yup.string().required(
                  requireMessage("Service We Provide")
                ),
                salesManFullName: Yup.string().required(
                  requireMessage("Salesman Full Name")
                ),
                contactOfSalesMan: Yup.string().required(
                  requireMessage("Contact No. Of Salesman")
                ),
                uploadSalesManPhoto: Yup.string().required(
                  requireMessage("Salesman Photo")
                ),
                images: Yup.string().required(requireMessage("Pictures"))
              })
            }
              onSubmit={values => {

                const { location } = this.state;
                const { clearErrorMessageShow } = this.props;
                clearErrorMessageShow()

                values.address = location.address;
                values.country = location.country;
                values.state = location.state;
                values.city = location.city;
                values.placeName = location.placeName;
                values.postalCode = location.postalCode;
                values.geometricLocation = {
                  coordinates: [location.longitude, location.latitude]
                }
                values.adId = adId;                
                delete values.__typename;
                delete values.createdAt;
                delete values.rating;
                delete values.userStatus;
                delete values.accountStatus;
                
                const input = {...values, serviceProvide: getIds(values.serviceProvide) }
                addBoatService(input);
              }}
              render={({ errors, setFieldValue, values, handleSubmit }) => (
                <Form>
                  <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                    <Box
                      fontSize={20}
                      letterSpacing={1}
                      fontWeight={600}
                      className="map-title"
                    >
                      {`Service Location`}{" "}
                      <span className="font-weight-400 font-14">
                        Ad ID: {`${adId}`}
                      </span>
                    </Box>
                  </div>
                  
                  <Row className="mb-3">
                    <Col xs={6}>
                      <div className="add-boat-map map-div-form h-100">
                        <GoogleMap
                          latLng={latLng}
                          height={25}
                          width={100}
                          placeHolder="Enter a location"
                          columnName="address"
                          fetch={result =>
                            this.fetchMapInfo(result, setFieldValue)
                          }
                          isError={errors.address}
                          isUpdate
                          value={location}
                        ></GoogleMap>
                      </div>
                    </Col>
                    <Col xs={6} className="pl-0">
                      {this.renderInfo(setFieldValue, values)}
                    </Col>
                  </Row>

                  <Row>
                    <Col xs={6}>
                      {this.renderSalesmanInfo(setFieldValue, values)}
                    </Col>
                    <Col xs={6} className="pl-0">
                      {this.renderUploadPictures(setFieldValue, values)}
                    </Col>
                  </Row>
                  {isAddError && <ErrorComponent errors={errorMessage} />}

                  <div className="d-flex justify-content-center">
                    <Button
                      type="button"
                      onClick={handleSubmit}
                      className="button btn btn-primary w-auto addBoatService-btn primary-button"
                    >
                      Save
                    </Button>
                    <Button
                      variant="contained"
                      className="button btn w-auto btn-dark"
                      onClick={() => this.cancelHandler()}
                    >
                      Cancel
                    </Button>
                  </div>

                </Form>
              )}
            ></Formik>
          )}
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  isAddSuccess:
    state.boatServiceReducer && state.boatServiceReducer.isAddSuccess,
  isAddError: state.boatServiceReducer && state.boatServiceReducer.isAddError,
  providedServices:
    state.boatServiceReducer && state.boatServiceReducer.providedServices,
  userService: state.boatServiceReducer && state.boatServiceReducer.userService,
  isLoading: state.boatServiceReducer && state.boatServiceReducer.isLoading,
	errorMessage: state.errorReducer.errorMessage,
});

const mapDispatchToProps = dispatch => ({
	clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
  getAllBoatServiceTypes: () => dispatch(getAllBoatServiceTypes()),
  addBoatService: data => dispatch(addBoatService(data)),
  getUserBoatService: () => dispatch(getUserBoatService()),
  clearEditFlag: () => dispatch(clearEditFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBoatService);
