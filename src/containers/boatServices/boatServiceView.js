import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Box
} from "@material-ui/core";

import { Layout, PaginationBar } from "../../components";
import UserContext from "../../UserContext";
import { pagination } from "../../util/enums/enums";
import { getTypeWiseService } from "../../redux/actions";
import BoatServicesAdded from "../../components/gridComponents/boatAddedServices";
import "./boatServices.scss";
import { ViewBoatServiceStyle } from "../../components/styleComponent/styleComponent";

class BoatServiceView extends Component {
  state = {
    top: 95,
    typeId: ''
  }
  static contextType = UserContext;
  static getDerivedStateFromProps(nextProps) {

    const { match } = nextProps
    const { params } = match && match

    if (params && params.typeId) {
      return {
        typeId: params.typeId
      }
    }

    return null
  }
  componentDidMount() {
    const { typeId } = this.state
    const topSelector = document.querySelector('.header-responsive');
    const top = topSelector.offsetHeight
    this.setState({ top })

    const { getTypeWiseService } = this.props;

    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: this.context && this.context.country,
      typeId: typeId
    }

    typeId && getTypeWiseService(data)

  }

  render() {
    const { typeWiseServices, location, getTypeWiseService, totalTypeWiseServices } = this.props;
    const { top } = this.state
    
    return (
      <ViewBoatServiceStyle marginTop={top} className="boat-service-view-grid-margin">
        <Layout className="boat-service-layout" isFooter>
          <Grid item xs={12} >
            <Box
              className="section-heading-show"
              display="flex"
              fontSize={24}
              fontWeight={500}
              letterSpacing={0.5}
            >
              <label className="label show-name">
                Services
              </label>
            </Box>
            <div className="container100 mt-3">
              <Grid item xs={12}>
                {typeWiseServices && typeWiseServices.length ? (
                  <BoatServicesAdded
                    xs={12}
                    sm={4}
                    boatAddedServices={typeWiseServices}
                    iconColor="iconColor-boatInner"
                  />
                ) : (
                    "No Record Found."
                  )}
              </Grid>
              {totalTypeWiseServices > pagination.PAGE_RECORD_LIMIT && (
                <div className="boat-pagination">
                  <PaginationBar
                    action={getTypeWiseService}
                    value={location.state.data}
                    totalRecords={totalTypeWiseServices}
                  />
                </div>
              )}
            </div>
          </Grid>
        </Layout>
      </ViewBoatServiceStyle>
    );
  }
}

const mapStateToProps = state => ({
  typeWiseServices:
    state.boatServiceReducer && state.boatServiceReducer.typeWiseServices,
  providedServices:
    state.boatServiceReducer && state.boatServiceReducer.providedServices,
  totalTypeWiseServices:
    state.boatServiceReducer && state.boatServiceReducer.totalTypeWiseServices
});

const mapDispatchToProps = dispatch => ({
  getTypeWiseService: data => dispatch(getTypeWiseService(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatServiceView);
