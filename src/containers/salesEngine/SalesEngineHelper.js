import { salesEngineStatus } from "../../util/enums/enums";

export const getMySalesEngineStatus = (salesEngine) => {
    if (salesEngine.buySellProcess.length) {
        const status = salesEngine.buySellProcess[salesEngine.buySellProcess.length - 1];

        if (status === salesEngineStatus.negotiationPending) {
            return "Seller has not added final price for boat."
        }
        if (status === salesEngineStatus.negotiation && !salesEngine.isSurveyorSkip) {
            return "Surveyor selection process is left."
        }
        if (status === salesEngineStatus.surveyorRequested) {
            return "No surveyor has accepted the request"
        }
        if (status === salesEngineStatus.surveyorPaymentPending) {
            return "Surveyor payment process is left"
        }
        if (status === salesEngineStatus.surveyorPaymentCompleted) {
            return "Surveyor has not added any report yet"
        }        
        if (status === salesEngineStatus.agreementPending) {
            return "Agreement process is pending"
        }       
        else {
            return status
        }
    }
}