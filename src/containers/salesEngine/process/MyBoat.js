import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import { Grid } from "@material-ui/core";

import withAuth from "../../../hoc/withAuth";
import {
  BoatInformation,
  SellerInformation,
  AgentInformation,
} from "../../../components";
import PriceNegotiable from "../../../components/salesEngine/PriceNegotiable";
import { addSurveyOption, getAllAgents, clearAgentFlag, assignAgent } from "../../../redux/actions";

import "../SalesEngine.scss";

class MyBoat extends Component {

  state = {
    isSurveyorSkip: null,
    agentNotAvailable: false
  }

  addSurveyOptionHandler = () => {
    const { salesEngine, addSurveyOption } = this.props
    const { isSurveyorSkip } = this.state

    addSurveyOption({ id: salesEngine.id, isSurveyorSkip: isSurveyorSkip === 'true' })
  }

  assignBroker = (value) => {
    const { getAllAgents, salesEngine } = this.props
    getAllAgents({ country: salesEngine.boat.country })
  }

  setAgent = (id) => {
    const { salesEngine, assignAgent } = this.props
    assignAgent({ agentId: id, id: salesEngine.id })
  }

  static getDerivedStateFromProps(nextProps) {
    const { agentSuccess, clearAgentFlag, assignAgent, agents, salesEngine } = nextProps

    if (agentSuccess) {
      clearAgentFlag()
      agents && agents.length && assignAgent({ agentId: agents[0].id, id: salesEngine.id })
      if (!agents.length) { return { agentNotAvailable: true } }
    }
    return null;
  }

  render() {

    const { isSurveyorSkip, agentNotAvailable } = this.state
    const { salesEngine, isSeller, agents, currentUser, isAgent } = this.props

    return (
      <>
        <div>
          <Container fluid className="pl-0 pr-0">

            <Grid container spacing={2}>
              {salesEngine && salesEngine.boat && <BoatInformation salesEngine={salesEngine} />}

              <SellerInformation salesEngine={salesEngine} isSeller={isSeller} />
            </Grid>

            {agentNotAvailable
              ? <span>No Agents available in this country</span>
              : currentUser.role.type === "buyer" && salesEngine &&
              <AgentInformation agent={salesEngine.agent} salesEngine={salesEngine} assignBroker={this.assignBroker} agents={agents} setAgent={this.setAgent} />
            }

            {isSeller
              && <PriceNegotiable salesEngine={salesEngine} />
              // : (
              //   <>
              //     <div className="d-flex flex-column">
              //       {salesEngine && salesEngine.buySellProcess.length > 2 && <span>Boat Survey : {salesEngine && salesEngine.isSurveyorSkip ? "No" : "Yes"}</span>}
              //     </div>

              //     {salesEngine && salesEngine.buySellProcess[salesEngine.buySellProcess.length - 1] === salesEngineStatus.negotiation &&
              //       !salesEngine.isSurveyorSkip && (
              //         <div className="mt-5">
              //           <div className="boat-survey-selection d-flex flex-column mb-2">
              //             <Field
              //               label="Do you want to survey your boat ?"
              //               type="select"
              //               value={isSurveyorSkip}
              //               onChangeText={e => this.setState({ isSurveyorSkip: e.target.value })}
              //               options={[{ id: false, value: 'Yes' }, { id: true, value: 'No' }]}
              //               placeholder='Select Survey Option'
              //               labelClass="d-flex mb-2"
              //             />
              //           </div>
              //           <Button
              //             onClick={this.addSurveyOptionHandler}
              //             type="button"
              //             disabled={!isSurveyorSkip}
              //             className="button btn w-auto stepper-add-survey-option ml-0 mr-0"
              //           >
              //             {'Add Survey Option'}
              //           </Button>
              //         </div>
              //       )}
              //   </>
              // )
            }
          </Container>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  agentSuccess: state.salesEngineReducer && state.salesEngineReducer.agentSuccess,
  agents: state.salesEngineReducer && state.salesEngineReducer.agents,
  salesEngine: state.salesEngineReducer.salesEngine,
  currentUser: state.loginReducer && state.loginReducer.currentUser
});

const mapDispatchToProps = dispatch => ({
  addSurveyOption: data => dispatch(addSurveyOption(data)),
  getAllAgents: data => dispatch(getAllAgents(data)),
  clearAgentFlag: () => dispatch(clearAgentFlag()),
  assignAgent: (data) => dispatch(assignAgent(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withAuth(MyBoat));
