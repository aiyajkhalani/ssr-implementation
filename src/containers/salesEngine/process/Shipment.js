import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Container } from "react-bootstrap";
import { Grid, Dialog, DialogContent } from "@material-ui/core";

import { salesEngineAssignShipper, getAllShipmentProposal, paymentRequest, skipShipment, addBoatShipmentLocation } from "../../../redux/actions";
import { priceFormat } from "../../../util/utilFunctions";
import PaymentOption from "../../../components/salesEngine/paymentOption";
import GoogleMarker from "../../../components/map/marker";
import GoogleMap from "../../../components/map/map";
import { salesEngineStatus, latLng } from "../../../util/enums/enums";
import { BoatInformation, SellerInformation, UserInformationCard } from "../../../components";
import { getSingleBoatMarker } from "../../../helpers/boatHelper";

import "./SalesEngineStepper.scss"

class Shipment extends Component {

    state = {
        locationModal: false,
        shipmentLocation: {
            id: this.props.salesEngine.id,
            input: {
                address: "",
                city: "",
                country: "",
                placeName: "",
                postalCode: "",
                route: "",
                state: "",
                geometricLocation: {
                    coordinates: []
                }
            }
        }
    }

    componentDidMount() {
        const { getAllShipmentProposal, salesEngine } = this.props
        salesEngine && getAllShipmentProposal(salesEngine.id)
    }

    shipperAcceptRequest = (shipperId) => {
        const { salesEngine, salesEngineAssignShipper } = this.props
        shipperId && salesEngine.id && salesEngineAssignShipper({ shipperId: shipperId, id: salesEngine.id })
    }

    locationModalHandler = () => {
        this.setState(preState => ({ locationModal: !preState.locationModal }));
    };

    addShipmentLocation = () => {
        const { shipmentLocation } = this.state
        const { addBoatShipmentLocation } = this.props

        addBoatShipmentLocation(shipmentLocation)
    };

    fetchMapInfo = (result) => {

        const { shipmentLocation } = this.state

        const {
            address,
            country,
            state,
            city,
            placeName,
            postalCode,
            route,
            latitude,
            longitude
        } = result;

        shipmentLocation.input.address = address
        shipmentLocation.input.city = city
        shipmentLocation.input.state = state
        shipmentLocation.input.country = country
        shipmentLocation.input.route = route
        shipmentLocation.input.placeName = placeName
        shipmentLocation.input.postalCode = postalCode

        shipmentLocation.input.geometricLocation = {
            coordinates: [longitude, latitude]
        }

        this.setState({ shipmentLocation, longitude, latitude })
    };

    render() {
        const { locationModal, shipmentLocation: { input }, longitude, latitude } = this.state
        const { salesEngine, shipmentProposals, isSeller, paymentRequest, skipShipment, currentUser } = this.props

        return (
            <Container fluid className="pl-0 pr-0">

                {currentUser.role.type === "buyer" && !salesEngine.selectedShipper && !salesEngine.isShipperSkip &&
                    <div className="mb-3 d-flex justify-content-center">
                        <button
                            type="button"
                            className={`button btn w-auto btn-primary inspection-payment-skip-btn`}
                            onClick={() => skipShipment({ id: salesEngine.id, isShipperSkip: true })}
                            disabled={salesEngine.selectedShipper}>
                            {'Pick shipment on your own'}
                        </button>
                    </div>
                }

                <Grid container spacing={2}>
                    {salesEngine.boat && <BoatInformation salesEngine={salesEngine} />}

                    <SellerInformation salesEngine={salesEngine} isSeller={isSeller} />
                </Grid>

                {!isSeller && !salesEngine.selectedShipper ?
                    <>
                        <Row className="mt-5">
                            <Grid container className="stepper-info-boat" spacing={3}>
                                <Grid item xs={8} className="pl-4 d-flex flex-wrap">
                                    {shipmentProposals && shipmentProposals.length
                                        ? shipmentProposals.map((item) => {
                                            return (
                                                item.shipper && 
                                                
                                                <>
                                                <UserInformationCard
                                                  userInfo={item.shipper}
                                                  proposalInfo={item}
                                                  showShipperButtons
                                                  shipperAcceptRequest={this.shipperAcceptRequest}
                                                  salesEngine={salesEngine}
                                                  xs={6}
                                                />
                                              </>
                                                
                                                // <Grid item xs={3} className="stepper-info-boat pb-3 pr-3">
                                                //     <div className="stepper-surveyor-inspection-info">
                                                //         <div className="stepper-nearest-surveyor-img">
                                                //             <img src={require("../../../assets/images/boatInner/1.jpg")} alt="" className="h-100 width-100" />
                                                //         </div>
                                                //         <div className="mt-3 mb-0">
                                                //             <span className="stepper-nearest-surveyor-name">
                                                //                 {`${item.shipper.firstName} ${item.shipper.lastName}`}
                                                //             </span>
                                                //         </div>
                                                //         <div className="mt-2 mb-0">
                                                //             <span className="stepper-nearest-surveyor-distance">
                                                //                 {item.price && `$${priceFormat(item.price)}`}
                                                //             </span>
                                                //         </div>
                                                //         <div className="mt-3 mb-0">
                                                //             <button
                                                //                 type="button"
                                                //                 className={`w-auto btn mr-2 btn-primary`}
                                                //                 onClick={() => this.shipperAcceptRequest(item.shipper.id)}
                                                //             >
                                                //                 {'Accept'}
                                                //             </button>
                                                //         </div>
                                                //     </div>
                                                // </Grid>
                                            )
                                        })
                                        : <span>No shipper available for shipment.</span>
                                    }
                                </Grid>
                                <Grid item xs={4}>
                                    {salesEngine.boat &&
                                        <div>
                                        <h4 className="mb-4 gray-dark">Boat Location</h4>
                                        <GoogleMarker
                                            height={35}
                                            width={'100%'}
                                            markers={getSingleBoatMarker(salesEngine.boat)}
                                            className="boat-inner-map-height"
                                        />
                                        </div>
                                    }
                                </Grid>
                            </Grid>
                        </Row>
                    </> :
                    salesEngine.shipper && <UserInformationCard userInfo={salesEngine.shipper}/>
                }


                {!isSeller && !salesEngine.shipmentLocation &&
                    salesEngine.buySellProcess[salesEngine.buySellProcess.length - 1] === salesEngineStatus.paymentCompleted &&
                    <Row className="mt-5">
                        <Grid item xs={6}>
                            <button className="w-auto btn mr-2 btn-secondary" type="button" onClick={this.locationModalHandler}>
                                {'Select Boat Shipment Location'}
                            </button>
                            <br />

                            {input.address}
                            <br />

                            {input.address &&
                                <button className="w-auto btn mr-2 btn-primary" type="button" onClick={this.addShipmentLocation} disabled={!input.address}>
                                    {'Add'}
                                </button>
                            }

                            <Dialog
                                open={locationModal}
                                onClose={this.locationModalHandler}
                                classes={{ container: "model-width" }}
                                aria-labelledby="responsive-dialog-title"
                            >

                                <DialogContent >
                                    <GoogleMap
                                        className="googleMap-position"
                                        latLng={
                                            input.address
                                                ? {
                                                    lat: latitude,
                                                    lng: longitude
                                                }
                                                : latLng
                                        }
                                        fetch={result => this.fetchMapInfo(result)}
                                        height={60}
                                        width={100}
                                        placeHolder="Shipment Location"
                                        columnName={"address"}
                                        isError={!input.address}
                                        value={{ ...input.address }}
                                        onClose={this.locationModalHandler}
                                        isCloseBtn
                                    ></GoogleMap>
                                </DialogContent>
                            </Dialog>
                        </Grid>
                    </Row>
                }

                {/* {!isSeller &&  salesEngine.buySellProcess[salesEngine.buySellProcess.length - 1]
                    === salesEngineStatus.shipperPaymentCompleted && 
                    currentUser.id === salesEngine.shipper.id && 
                    <div className="mb-5 d-flex justify-content-center">
                    
                        <Button
                            type="button"
                            className="button btn w-auto btn-primary inspection-payment-skip-btn"
                            // onClick={() => addSurveyOption({ id: salesEngine.id, isSurveyorSkip: true })}
                        >
                            {'Start Shipment'}
                        </Button>
                
                </div>} */}

                {!isSeller && !salesEngine.shipperPayment &&
                    <PaymentOption paymentRequest={paymentRequest} paymentTo="shipper" salesEngine={salesEngine} disable={salesEngine.shipper} />
                }




            </Container>
        )
    }
}
const mapStateToProps = state => ({
    shipmentProposals: state.salesEngineReducer.shipmentProposals,
    currentUser: state.loginReducer && state.loginReducer.currentUser
});

const mapDispatchToProps = dispatch => ({
    getAllShipmentProposal: (data) => dispatch(getAllShipmentProposal(data)),
    salesEngineAssignShipper: (data) => dispatch(salesEngineAssignShipper(data)),
    paymentRequest: (data) => dispatch(paymentRequest(data)),
    skipShipment: (data) => dispatch(skipShipment(data)),
    addBoatShipmentLocation: (data) => dispatch(addBoatShipmentLocation(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Shipment);