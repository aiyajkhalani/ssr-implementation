import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, Container } from "react-bootstrap";
import { Grid, Button } from "@material-ui/core";

import { paymentOptions } from "../../../util/enums/enums";
import { getNearestSurveyors, paymentRequest, requestSurveyor, addSurveyOption, declineSurveyor } from "../../../redux/actions";
import { BoatInformation, SellerInformation, UserInformationCard } from "../../../components";
import PaymentOption from "../../../components/salesEngine/paymentOption";
import { SuccessNotify } from "../../../helpers/notification";
import GoogleMarker from "../../../components/map/marker";
import { getSurveyorMarkersWithBoat } from "../../../helpers/boatHelper";
import { priceFormat } from "../../../util/utilFunctions";

import "./SalesEngineStepper.scss"

class SurveyMyBoat extends Component {
  state = {
    paymentMethod: paymentOptions[0].name
  };

  componentDidMount() {
    const { getNearestSurveyors, salesEngine: { boat }, isSeller } = this.props

    if (boat && boat.country) {
      !isSeller && getNearestSurveyors({
        country: boat.country,
        latitude: boat.geometricLocation.coordinates[1],
        longitude: boat.geometricLocation.coordinates[0],
      })
    }
  }

  requestSurveyorHandler = (surveyorBranchId) => {
    const { requestSurveyor, salesEngine } = this.props

    if (salesEngine && salesEngine.id) {
      requestSurveyor({ id: salesEngine.id, surveyorId: surveyorBranchId })
      SuccessNotify('Surveyor Requested Successfully.')
    }
  }

  selectPaymentMethodHandler = name => {
    this.setState({ paymentMethod: name });
  };

  isSurveyorRequested = (surveyorBranchId) => {
    const { salesEngine } = this.props
    return salesEngine.requestedSurveyor.includes(surveyorBranchId)
  }

  render() {
    const { nearestSurveyors, salesEngine, paymentRequest, isSeller, addSurveyOption, declineSurveyor, currentUser, isAgent, costEstimation } = this.props

    return (
      <>
        <Grid>
          <Container fluid className="pl-0 pr-0">

            {currentUser.role.type === "buyer" && 
            <div className="mb-5 d-flex justify-content-center">
              {!salesEngine.surveyorAccepted && !salesEngine.surveyorPayment &&
                <Button
                  type="button"
                  className=" btn btn-outline-primary skip-survey-border"
                  onClick={() => addSurveyOption({ id: salesEngine.id, isSurveyorSkip: true })}
                >
                  {'Skip Survey'}
                </Button>
              }
            </div>
          }

            <Grid container spacing={3}>
              <BoatInformation salesEngine={salesEngine} />

              <SellerInformation salesEngine={salesEngine} isSeller={isSeller} />

              {/* {!isAgent && !isSeller && <Grid item xs={4}>
                <div className="d-flex">
                  <span className="stepper-inspection-payment-title mr-4 ml-0 mb-0 mt-0">
                    Payment
                </span>
                  <div className="mr-2">
                    <span className="stepper-inspection-bank-payment stepper-inspection-payment">
                      Bank to Bank Transfer
                  </span>
                  </div>
                  <div>
                    <span className="stepper-inspection-card-payment stepper-inspection-payment">
                      Credit Card
                  </span>
                  </div>
                </div>
                <div className="mt-4">
                  <span className="stepper-div-text font-14">
                    Card Number
                </span>
                  <Field type="text" className="light-gray-bg" />
                </div>
                <Grid container spacing={2} className="mt-1">
                  <Grid item xs={6}>
                    <div>
                      <span className="stepper-div-text font-14">
                        Expiration Date
                </span>
                      <Field type="text" className="light-gray-bg" />
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <div>
                      <span className="stepper-div-text font-14">
                        CVV
                </span>
                      <Field type="text" className="light-gray-bg" />
                    </div>
                  </Grid>
                </Grid>
                <div className="mt-3 mb-0">
                  <span className="stepper-div-text font-14">
                    Name on Code
                </span>
                  <Field type="text" className="light-gray-bg" />
                </div>
                <Grid container spacing={2} className="mt-1">
                  <Grid item xs={6}>
                    <div>
                      <span className="stepper-div-text font-14">
                        Zipcode
                </span>
                      <Field type="text" className="light-gray-bg" />
                    </div>
                  </Grid>
                  <Grid item xs={6} className="d-flex justify-content-center align-items-center">

                    <button
                      type="button"
                      className="w-auto btn mr-2 btn btn-danger w-auto decline-btn-form text-white inspection-payment-pay-now-btn">
                      {'Pay Now'}
                    </button>

                  </Grid>
                </Grid>
              </Grid>} */}

              {salesEngine.surveyor &&
                <UserInformationCard userInfo={salesEngine.surveyor.user} salesEngine={salesEngine} surveyorInfo={{ pricePerFt: salesEngine.surveyor.pricePerFt }} surveyorUserProfile />
              }
            </Grid>
            {/* NEEDED LATER ON */}
            {/* <SurveyorCostInformation /> */}



            {!isAgent && !isSeller && !salesEngine.surveyorPayment &&
              <>
                <Col xs={12} className="mt-5">
                  {nearestSurveyors && nearestSurveyors.length > 0
                    &&
                    <>
                      <Row>
                        <Grid container spacing={2}>
                          <Grid item xs={8} >
                            <h3 className="sales-engine-info-title">
                              {'Nearest Surveyors'}
                            </h3>
                            <Grid container className="stepper-info-boat" spacing={2}>
                              {/* <Grid item xs={4} className="pl-3 d-flex flex-wrap"> */}
                              {nearestSurveyors && nearestSurveyors.length
                                ?
                                nearestSurveyors.map((item) => {
                                  return (
                                    item.user &&
                                    <>
                                      <UserInformationCard
                                        userInfo={item.user}
                                        showSurveyorButtons
                                        surveyorInfo={item}
                                        isSurveyorRequested={this.isSurveyorRequested}
                                        requestSurveyorHandler={this.requestSurveyorHandler}
                                        salesEngine={salesEngine}
                                        xs={6}
                                      />
                                    </>
                                    // <Grid item xs={3} className="stepper-info-boat pr-3 pb-3">
                                    //   <div className="stepper-surveyor-inspection-info h-100">
                                    //     <div className="stepper-nearest-surveyor-img">
                                    //       <img src={require("../../../assets/images/boatInner/1.jpg")} alt="" className="h-100 width-100" />
                                    //     </div>
                                    //     <div className="mt-3 mb-0">
                                    //       <span className="stepper-nearest-surveyor-name">
                                    //         {`${item.user.firstName} ${item.user.lastName}`}
                                    //       </span>
                                    //     </div>
                                    //     <div className="mt-2 mb-0">
                                    //       <span className="stepper-nearest-surveyor-distance">
                                    //         Away from the boat: {item.distance}
                                    //       </span>
                                    //     </div>
                                    //     <div className="mt-2 mb-0">
                                    //       <span className="stepper-nearest-surveyor-distance">
                                    //         Price: {item.pricePerFt}/ft
                                    //       </span>
                                    //     </div>
                                    //     <div className="mt-2 mb-0">
                                    //       <span className="stepper-nearest-surveyor-distance">
                                    //         Total cost:  ${salesEngine.boat && salesEngine.boat.lengthInFt && priceFormat(item.pricePerFt * salesEngine.boat.lengthInFt)}
                                    //       </span>
                                    //     </div>
                                    //     <div className="mt-3 mb-0">
                                    //       <button
                                    //         type="button"
                                    //         className={`w-auto btn mr-2 nearest-surveyor-btn ${this.isSurveyorRequested(item.id) ? 'grey-requested-btn' : 'btn-primary'}`}
                                    //         onClick={() => this.requestSurveyorHandler(item.id)}
                                    //         disabled={this.isSurveyorRequested(item.id)}>
                                    //         {this.isSurveyorRequested(item.id) ? 'Requested' : 'Request'}
                                    //       </button>
                                    //       {this.isSurveyorRequested(item.id) && <button
                                    //         type="button"
                                    //         className={`w-auto btn grey-requested-btn nearest-surveyor-btn nearest-surveyor-decline-btn`}
                                    //         onClick={() => declineSurveyor({ id: salesEngine.id, surveyorId: item.id })}
                                    //       >
                                    //         {'Cancel'}
                                    //       </button>}
                                    //     </div>
                                    //   </div>
                                    // </Grid>
                                  )
                                })
                                : <span>There is no nearby surveyor available.</span>
                              }
                              {/* </Grid> */}
                            </Grid>
                          </Grid>
                          <Grid item xs={4} >
                            <h3 className="sales-engine-info-title">
                              {'Nearby Surveyor Locations'}
                            </h3>
                            <Grid container>
                              <Grid item xs={12} className="pl-0 nearest-surveyor-map">

                                {salesEngine.boat &&
                                  <GoogleMarker
                                    height={31}
                                    width={'100%'}
                                    markers={getSurveyorMarkersWithBoat(nearestSurveyors, salesEngine.boat)}
                                    className="boat-inner-map-height"
                                  />
                                }
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Row>
                    </>
                  }
                </Col>
              </>
            }

            {!isSeller && !salesEngine.surveyorPayment && <PaymentOption
              paymentRequest={paymentRequest}
              paymentTo="surveyor" salesEngine={salesEngine}
              disable={salesEngine.surveyorAccepted && !salesEngine.surveyorPayment} />}

          </Container >
        </Grid >
      </>
    );
  }
}

const mapStateToProps = state => ({
  nearestSurveyors: state.salesEngineReducer.nearestSurveyors,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  costEstimation: state.salesEngineReducer.costEstimation,
});

const mapDispatchToProps = dispatch => ({
  getNearestSurveyors: data => dispatch(getNearestSurveyors(data)),
  paymentRequest: data => dispatch(paymentRequest(data)),
  requestSurveyor: data => dispatch(requestSurveyor(data)),
  declineSurveyor: data => dispatch(declineSurveyor(data)),
  addSurveyOption: data => dispatch(addSurveyOption(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SurveyMyBoat);
