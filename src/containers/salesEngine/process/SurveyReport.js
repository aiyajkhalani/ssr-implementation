import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";
import { Grid } from "@material-ui/core";

import { BoatInformation, SurveyorInformation, SurveyorSubmittedReport, SellerInformation } from "../../../components";
import { getSurveyorReport, getDocumentLinks } from "../../../redux/actions";

class SurveyReport extends Component {

  componentDidMount() {
    const { getSurveyorReport, salesEngine, getDocumentLinks } = this.props

    getSurveyorReport({ surveyorId: salesEngine.surveyor && salesEngine.surveyor.user.id, salesEngineId: salesEngine.id })
    getDocumentLinks({ id: salesEngine.id, type: "Surveyor" })
  }

  render() {

    const { salesEngine, surveyorReport, isSeller } = this.props

    return (
      <>
        <div>
          <Container fluid className="pl-0 pr-0">

            <Grid container spacing={3}>
              <BoatInformation salesEngine={salesEngine} />

              <SellerInformation salesEngine={salesEngine} isSeller={isSeller} />
            </Grid>

            {salesEngine && salesEngine.surveyor && <SurveyorInformation surveyor={salesEngine.surveyor.user} branch={salesEngine.surveyor} />}

            {surveyorReport && <SurveyorSubmittedReport surveyorReport={surveyorReport} />}

          </Container>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  surveyorReport: state.salesEngineReducer.surveyorReport
});

const mapDispatchToProps = dispatch => ({
  getSurveyorReport: data => dispatch(getSurveyorReport(data)),
  getDocumentLinks: data => dispatch(getDocumentLinks(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SurveyReport);
