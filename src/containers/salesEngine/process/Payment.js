import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row, Container, } from "react-bootstrap";
import {
  FormControlLabel,
  Checkbox,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from "@material-ui/core";

import PaymentOption from "../../../components/salesEngine/paymentOption";
import { paymentRequest, clearSalesEnginePaymentFlags } from "../../../redux/actions";
import { BoatInformation, BoatPurchaseInformation, SellerInformation } from "../../../components";
import { checkAgreement, getAgreementContents } from "../../../redux/actions";
import { salesEngineAgreementType } from "../../../util/enums/enums";
import { SuccessNotify } from "../../../helpers/notification";
import '../SalesEngine.scss'

class Payment extends Component {

  state = {
    isAgreed: false,
    agree: false,
    agreementModal: false
  };

  static getDerivedStateFromProps(nextProps) {
    const { isAgreementChecked, clearSalesEnginePaymentFlags } = nextProps

    if (isAgreementChecked) {
      clearSalesEnginePaymentFlags()
      SuccessNotify("Agreement Checked Successfully.")
    }
    return null;
  }

  componentDidMount() {
    const { getAgreementContents, salesEngine } = this.props;

    getAgreementContents({
      id: salesEngine.id,
      type: salesEngineAgreementType.transactionGeneralRules
    });
  }

  checkAgreementHandler = () => {
    const { checkAgreement, salesEngine, isSeller } = this.props;

    const agreementData = { id: salesEngine.id, buyerId: salesEngine.buyer.id };
    checkAgreement(agreementData);
  };

  termsHandler = () => {
    this.setState(prevState => ({
      agreementModal: !prevState.agreementModal
    }));
  };


  render() {
    const {
      paymentRequest,
      agreementsContents,
      salesEngine,
      isSeller,
      isAgent,
      costEstimation
    } = this.props;
    const { isAgreed, agree, agreementModal } = this.state;

    return (
      <>
        <div>
          <Container fluid className="pl-0 pr-0">
            <Grid container spacing={2}>

              <BoatInformation salesEngine={salesEngine} />

              <SellerInformation salesEngine={salesEngine} isSeller={isSeller} />
            </Grid>

            <BoatPurchaseInformation salesEngine={salesEngine} costEstimation={costEstimation} />

            <Row>
              {!isAgent && !isSeller && < Col xs={12}>

                <div className="clearfix mt-3">
                <div
                  className="custom-control custom-checkbox float-left mb-none"
                  onClick={this.checkAgreementHandler}
                >
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="agree"
                    checked={isSeller ? salesEngine.sellerAgreement : salesEngine.buyerAgreement}
                    disabled={(isSeller ? salesEngine.sellerAgreement : salesEngine.buyerAgreement)}
                  />
                  <label
                    className="custom-control-label font-14 register-custom-control-label mb-0 d-flex cursor-pointer"
                    for="remember me"
                  >
                    I have read and agree to
                              {/* Boats Agreement Fee Service */}
                    <div
                      onClick={this.termsHandler}
                      className="ml-1 darkBlue mr-1"
                    >
                      Agreement of purchase and Sales
                              </div>
                    <div >
                      <Dialog
                        className="agreement-popup"
                        open={agreementModal}
                        onClose={this.termsHandler}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                      >
                        <DialogContent>
                          <DialogContentText
                            id="alert-dialogquis"
                            className="d-flex flex-column"
                          >
                            <div
                              dangerouslySetInnerHTML={{
                                __html: agreementsContents && agreementsContents.content
                              }}
                            />
                            <div className="mr-3">
                              <span className="stepper-submitted-report-info">
                                <a href={agreementsContents.content} download>Download Agreement</a>
                              </span>
                            </div>
                          </DialogContentText>
                        </DialogContent>
                      </Dialog>
                    </div>
                    and understand that this application is to buy boats with AdamSea.Com{" "}
                  </label>
                </div>
              </div>
              </Col>}

              {/* {(!isAgent &&
                ((isSeller && !salesEngine.sellerAgreement) || !salesEngine.buyerAgreement)) &&
                <>
                  <Col xs={12}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={isAgreed}
                          onChange={() =>
                            this.setState({ isAgreed: !isAgreed })
                          }
                          value={isAgreed}
                        />
                      }
                      label="Agree to contract"
                    />
                  </Col>

                  <Col xs={12}>
                    <button
                      className={
                        isAgreed
                          ? "btn-primary btn width"
                          : "btn text-capitalize bg-disabled"
                      }
                      type="button"
                      onClick={this.checkAgreementHandler}
                      disabled={!isAgreed}
                    >
                      {isSeller ? "Continue" : "Continue to payment"}
                    </button>
                  </Col>
                </>
              } */}
            <div className="mr-3 mb-5">
              <span className="stepper-submitted-report-info">
                <a href={agreementsContents.content} download>Download Agreement</a>
              </span>
            </div>
            </Row>

          {salesEngine.buyerAgreement && <span>Agreement Signed by Buyer.</span>}
          <br />
          {salesEngine.sellerAgreement && <span>Agreement Signed by Seller.</span>}

          {!isSeller && !isAgent && (
            <Row>
              <Col xs={12} className="pl-0 mt-3">
                <PaymentOption
                  paymentTo="buyer"
                  paymentRequest={paymentRequest}
                  salesEngine={salesEngine}
                  disable={salesEngine.buyerAgreement}
                />
              </Col>

            </Row>
          )}

          </Container>
      </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  agreementsContents: state.salesEngineReducer.agreementsContents,
  isAgreementChecked: state.salesEngineReducer.isAgreementChecked,
  costEstimation: state.salesEngineReducer.costEstimation,
});

const mapDispatchToProps = dispatch => ({
  paymentRequest: data => dispatch(paymentRequest(data)),
  getAgreementContents: data => dispatch(getAgreementContents(data)),
  checkAgreement: data => dispatch(checkAgreement(data)),
  clearSalesEnginePaymentFlags: data => dispatch(clearSalesEnginePaymentFlags(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
