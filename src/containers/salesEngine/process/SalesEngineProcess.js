import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "react-bootstrap";

import { Loader, DashboardLayout } from "../../../components";
import MyBoat from "./MyBoat";
import SurveyMyBoat from "./SurveyMyBoat";
import SurveyReport from "./SurveyReport";
import Payment from "./Payment";
import Shipment from "./Shipment";
import { salesEngineSteps } from "../../../util/enums/enums";
import { getSingleSalesEngine, getCostEstimate } from "../../../redux/actions";
import { salesEngineStatusCheck } from "../../../helpers/string";
import { SuccessNotify } from "../../../helpers/notification";
import { getMySalesEngineStatus } from "../SalesEngineHelper";

import "../SalesEngine.scss";

class SalesEngineProcess extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 1, // Current step.
      stepName: "", // Current Step Name.
      totalStep: 0, // Total number of steps for sales engine process/

      sellerUser: false,
      salesEngineStepsState: salesEngineSteps(),
      isSurveyorSkip: null,

      stepperWidth: 0,
      isAgent: props.location && props.location.state && props.location.state.isAgent,
      hrWidthCalc: 0,
    };
  }

  componentDidMount() {
    const { getSingleSalesEngine, salesEngine, currentUser, getCostEstimate } = this.props;
    const { salesEngineStepsState } = this.state;

    this.setState({
      stepName: salesEngineStepsState[0].name,
      totalStep: salesEngineStepsState.length,
    })

    getCostEstimate()
    if (salesEngine && salesEngine.id) {
      getSingleSalesEngine(salesEngine.id);
    }

    currentUser &&
      salesEngine &&
      salesEngine.seller &&
      this.setState({ sellerUser: currentUser.id === salesEngine.seller.id });
  }
  stepperWidth = (salesEngine, salesEngineStepsState) => {
    const { hrWidthCalc } = this.state
    const stepperWidth = salesEngine && salesEngineStepsState && document.querySelector(".sales-engine-stepper");
    const stepperActualWidth = stepperWidth && stepperWidth.offsetWidth / ((salesEngine && salesEngineStepsState && salesEngineStepsState.length - 1))
    !hrWidthCalc && stepperActualWidth && this.setState({ hrWidthCalc: (stepperActualWidth + 5) })
  }
  componentDidUpdate(prevProps, prevState) {
    const { salesEngineStepsState } = prevState;
    const { salesEngine } = prevProps;
    setTimeout(() => {
      salesEngine && 
      salesEngineStepsState && 
      salesEngineStepsState.length &&
      this.stepperWidth(salesEngine, salesEngineStepsState)
    }, 500);
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    const { salesEngine, paymentSuccess, skipShipmentSuccess, shipmentLocationAddedSuccess } = nextProps
    const { isSurveyorSkip } = prevState

    if (salesEngine && salesEngine.id && isSurveyorSkip !== salesEngine.isSurveyorSkip) {
      return {
        activeStep: salesEngineStatusCheck(salesEngine, salesEngine.isSurveyorSkip),
        salesEngineStepsState: salesEngineSteps(salesEngine.isSurveyorSkip),
        totalStep: salesEngineSteps(salesEngine.isSurveyorSkip).length,
        isSurveyorSkip: salesEngine.isSurveyorSkip,
      }
    }

    if (paymentSuccess) {
      SuccessNotify("Payment Success.")
    }
    if (skipShipmentSuccess) {
      SuccessNotify("Shipment Process Skipped.")
    }
    if (shipmentLocationAddedSuccess) {
      SuccessNotify("Shipment Location Added Successfully.")
    }

    return null
  }

  // activateStep = (stepToActivate) => {
  //   const { salesEngineSteps } = this.state
  //   const updatedSalesEngineSteps = salesEngineSteps.map((item, index) => index === stepToActivate ? { ...item, isActivated: true } : item)
  //   this.setState({ salesEngineSteps: updatedSalesEngineSteps })
  // }

  stepChangeHandler = (value, step) => {
    step &&
      step.isActivated &&
      this.setState({ activeStep: value, stepName: step.name });
  };

  nextStepHandler = () => {
    const { activeStep, totalStep, salesEngineStepsState } = this.state;

    activeStep < totalStep &&
      this.setState({
        activeStep: activeStep + 1,
        stepName: salesEngineStepsState[activeStep].name
      });
  };

  previousStepHandler = () => {
    const { activeStep, salesEngineStepsState } = this.state;

    activeStep > 0 &&
      this.setState({
        activeStep: activeStep - 1,
        stepName: salesEngineStepsState[activeStep - 2].name
      });
  };

  activeClassHandler = value => {
    const { activeStep } = this.state;
    return activeStep >= value
      ? "bg-white stepper-selected-div-text stepper-line-color"
      : "bg-white";
  };

  activeLineClassHandler = value => {
    const { activeStep } = this.state;
    return activeStep > value ? "stepper-line-selected-color" : "stepper-line-color-not-selected";
  };


  renderButtons = () => {
    const { activeStep, totalStep, stepName } = this.state;
    const { salesEngine } = this.props

    return (
      <div className="sales-engine-buttons">

        {activeStep !== 1 && (
          <button
            type="button"
            name="previous"
            className="btn btn-outline-dark"
            value="Previous"
            onClick={this.previousStepHandler}
          >
            {"Previous"}
          </button>
        )}

        <button
          type="button"
          name="next"
          className="btn btn-outline-primary float-right"
          disabled={
            (activeStep === 1 && salesEngine && !salesEngine.negotiatedBoatPrice) ||
            (activeStep === 2 && salesEngine && !salesEngine.surveyorReport && !salesEngine.isSurveyorSkip) ||
            (stepName === "Payment" && salesEngine && (!salesEngine.buyerAgreement || !salesEngine.sellerAgreement))
          }
          onClick={this.nextStepHandler}
        >
          {activeStep !== totalStep ? "Next" : "Submit"}
        </button>

      </div>
    );
  };

  renderTabs = salesEngine => {
    const { salesEngineStepsState, hrWidthCalc } = this.state;

    return (
      <div className="d-flex width-100 pb-4 pt-4 sales-engine-stepper justify-content-between">
        {salesEngine &&
          salesEngineStepsState.map((step, index) => (
            <div className="position-relative stepper-main-div">
              <div className="stepper-main-section" key={step.name}>
                <div className=" d-flex justify-content-center align-items-start flex-direction-column width-100"
                  ref={this.stepperFunction}
                >

                  <div
                    className={`stepper-info-rounded-circle stepper-number-div stepper-number-div-new ${step.isActivated &&
                      "cursor-pointer"} ${this.activeClassHandler(index + 1)}`}
                  // onClick={() => this.stepChangeHandler(index + 1, step)}
                  >
                    <span className="d-flex justify-content-center align-items-center h-100 font-14 stepper-div-text">
                      {step.name}
                    </span>

                  </div>

                </div>
                <hr
                  style={{ width: `${hrWidthCalc}px` }}
                  className={`stepper-line-div ${this.activeLineClassHandler(
                    index + 1
                  )}`}
                />
              </div>
            </div>
          ))}
      </div>
    );
  };

  render() {
    const { activeStep, stepName, sellerUser, isAgent } = this.state;
    const { salesEngine } = this.props;
    const statusInfo = salesEngine && salesEngine.id && getMySalesEngineStatus(salesEngine);

    return (
      <>
        <DashboardLayout>
          {salesEngine && !salesEngine.hasOwnProperty("id") ? (
            <Loader />
          ) : (
              <Container fluid className="pb-5 stepper-main-div-section">
                <div>

                  <div className="d-flex flex-row">
                    <div className="stepper-header">
                      <span>Buy It Now</span>
                    </div>
                    <div className="stepper-info-main-section" ref={this.calculateHeaderWidth}>
                      {this.renderTabs(salesEngine)}
                    </div>
                  </div>

                  <div className="bg-white p-4 d-flex flex-column width-100 m-auto">
                    {this.renderButtons()}

                    {statusInfo &&
                      <div className="mb-2 d-flex justify-content-center">
                        <span className="sales-engine-status inspection-payment-sales-engine-status">
                          Status: {statusInfo}
                        </span>
                      </div>
                    }

                    {salesEngine && salesEngine.isSurveyorSkip && salesEngine.isSurveyorSkip ?
                      <>
                        {activeStep === 1 && <MyBoat salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                        {activeStep === 2 && <Payment salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                        {activeStep === 3 && <Shipment salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                      </>
                      :
                      <>
                        {activeStep === 1 && <MyBoat salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                        {activeStep === 2 && <SurveyMyBoat salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                        {activeStep === 3 && <SurveyReport salesEngine={salesEngine} isSeller={sellerUser} />}
                        {activeStep === 4 && <Payment salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                        {activeStep === 5 && <Shipment salesEngine={salesEngine} isSeller={sellerUser} isAgent={isAgent} />}
                      </>
                    }

                  </div>

                </div>
              </Container>
            )}
        </DashboardLayout>
      </>
    );
  }
}

const mapStateToProps = state => ({
  salesEngine: state.salesEngineReducer.salesEngine,
  paymentSuccess: state.salesEngineReducer.paymentSuccess,
  skipShipmentSuccess: state.salesEngineReducer.skipShipmentSuccess,
  shipmentLocationAddedSuccess: state.salesEngineReducer.shipmentLocationAddedSuccess,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
});

const mapDispatchToProps = dispatch => ({
  getSingleSalesEngine: data => dispatch(getSingleSalesEngine(data)),
  getCostEstimate: data => dispatch(getCostEstimate(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SalesEngineProcess);
