import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    getSalesEngineBySeller, getSalesEngineByAgent, getSingleSalesEngine,
} from '../../../redux/actions/salesEngineAction';
import { TableCard } from '../../../components'

import "../../../styles/manageDashboardTableResponsive.scss"
import { pagination } from '../../../util/enums/enums';

class AgentSalesEngine extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {
                    Header: "Boat Name",
                    accessor: "boat.boatName"
                },
                {
                    Header: "Seller",
                    accessor: "seller.firstName"
                },
                {
                    Header: "Buyer",
                    accessor: "buyer.firstName"
                },
                {
                    Header: "BoatPrice",
                    accessor: "negotiatedBoatPrice"
                },
                {
                    Header: "Ad ID",
                    accessor: "boat.adId"
                },
                {
                    Header: "Sales Engine",
                    accessor: "id",
                    Cell: ({ value }) => (
                        <div className="d-flex flex-row justify-content-around">
                            <button
                                type="button"
                                onClick={() => props.getSingleSalesEngine(value)}
                                className="btn btn-outline-info"
                            >
                                View
                            </button>
                        </div>
                    )
                },
            ]
        }
    }

    componentDidMount() {
        const { getSalesEngineByAgent } = this.props
        getSalesEngineByAgent({ limit: pagination.PAGE_RECORD_LIMIT, page: pagination.PAGE_COUNT })
    }

    render() {

        const { columns } = this.state
        const { salesEngineByAgent } = this.props

        return (
            <>
                {columns &&
                    <div className="manage-dashboard-table">
                        <TableCard
                            title="Sales Engine"
                            columns={columns}
                            data={salesEngineByAgent}
                            noButton
                        />
                    </div>
                }
            </>
        )
    }

}


const mapStateToProps = state => ({
    salesEngineByAgent: state.salesEngineReducer && state.salesEngineReducer.salesEngineByAgent
})

const mapDispatchToProps = dispatch => ({
    getSalesEngineByAgent: data => dispatch(getSalesEngineByAgent(data)),
    getSingleSalesEngine: data => dispatch(getSingleSalesEngine(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(AgentSalesEngine)