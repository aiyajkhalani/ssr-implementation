import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    clearSalesEngines, clearSalesEngineShipperDashboardFlags,
} from '../../../redux/actions/salesEngineAction';
import { DashboardLayout } from '../../../components'
import { userRoleTypes, userRoles } from '../../../util/enums/enums';
import BuyerSalesEngine from './BuyerSalesEngine';
import SurveyorSalesEngine from './SurveyorSalesEngine';
import ShipperSalesEngine from './ShipperSalesEngine';
import SellerSalesEngine from './SellerSalesEngine';

import "../../../styles/manageDashboardTableResponsive.scss"
import { SuccessNotify } from '../../../helpers/notification';
import AgentSalesEngine from './AgentSalesEngine';

class SalesEngine extends Component {

    state = {
        isSeller: false,
        isBuyer: false,
        isShipper: false,
        isSurveyor: false,
        isBuyItNow: false,
        isAgent: false,
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const { isSeller, isAgent } = prevState
        const { getSalesEngineSuccess,
            history,
            addShipmentProposalSuccess,
            clearSalesEngineShipperDashboardFlags,
            shipperDeclineShipperRequestSuccess,
            currentUser
        } = nextProps

        if (getSalesEngineSuccess) {
            history && history.push('/sales-engine-process', { isSeller, isAgent })
        }
        if (addShipmentProposalSuccess) {
            SuccessNotify("Shipment Proposal Sent Successfully.")
            clearSalesEngineShipperDashboardFlags()
        }
        if (history.location && history.location.search) {
            return {
                isBuyItNow: history.location.search === "?buyItNow=true"
            }
        }
        if (currentUser.role.aliasName === "surveyor" || currentUser.role.aliasName === "yacht-shipper") {
            if (shipperDeclineShipperRequestSuccess) {
                SuccessNotify("Request Decline Successfully.")
            }
        }


        return null
    }

    componentDidMount() {
        this.roleWiseSalesEngines()
    }

    componentWillUnmount() {
        const { clearSalesEngines } = this.props
        clearSalesEngines()
    }

    roleWiseSalesEngines = () => {
        const { currentUser } = this.props

        if (currentUser && currentUser.role) {

            switch (currentUser.role.type) {

                case userRoleTypes.BUYER:
                    this.setState({ isBuyer: true })
                    break;

                case userRoleTypes.SELLER:
                    this.setState({ isSeller: true })
                    break;

                case userRoleTypes.AGENT:
                    this.setState({ isAgent: true })
                    break;

                case userRoleTypes.OTHER:
                    if (currentUser.role.aliasName === userRoles.SURVEYOR) {
                        this.setState({ isSurveyor: true })
                    } else if (currentUser.role.aliasName === userRoles.YACHT_SHIPPER) {
                        this.setState({ isShipper: true })
                    }
                    break;

                default:
                    break;
            }
        }
    }

    render() {

        const { isSeller, isShipper, isSurveyor, isBuyer, isBuyItNow, isAgent } = this.state
        const { history } = this.props

        return (
            <DashboardLayout>
                {isBuyer && <BuyerSalesEngine history={history} isBuyItNow={isBuyItNow} />}
                {isSurveyor && <SurveyorSalesEngine history={history} />}
                {isShipper && <ShipperSalesEngine history={history} />}
                {isSeller && <SellerSalesEngine history={history} />}
                {isAgent && <AgentSalesEngine history={history} />}
            </DashboardLayout>
        )
    }

}


const mapStateToProps = state => ({
    currentUser: state.loginReducer.currentUser,
    getSalesEngineSuccess: state.salesEngineReducer.getSalesEngineSuccess,
    addShipmentProposalSuccess: state.salesEngineReducer.addShipmentProposalSuccess,
    addShipmentProposalError: state.salesEngineReducer.addShipmentProposalError,
    surveyorAcceptSuccess: state.salesEngineReducer.surveyorAcceptSuccess,
    surveyorDeclineSuccess: state.salesEngineReducer.surveyorDeclineSuccess,
    shipperAcceptShipperRequestSuccess: state.salesEngineReducer.shipperAcceptShipperRequestSuccess,
    shipperDeclineShipperRequestSuccess: state.salesEngineReducer.shipperDeclineShipperRequestSuccess,
})

const mapDispatchToProps = dispatch => ({
    clearSalesEngines: () => dispatch(clearSalesEngines()),
    clearSalesEngineShipperDashboardFlags: data => dispatch(clearSalesEngineShipperDashboardFlags(data)),    
})

export default connect(mapStateToProps, mapDispatchToProps)(SalesEngine)