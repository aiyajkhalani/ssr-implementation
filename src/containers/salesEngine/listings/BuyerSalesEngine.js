import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    getSalesEngineByBuyer,
    getSingleSalesEngine,
} from '../../../redux/actions/salesEngineAction';
import { TableCard } from '../../../components'
import { formattedDate } from '../../../helpers/string';

import "../../../styles/manageDashboardTableResponsive.scss"

class BuyerSalesEngine extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {
                    Header: "Boat Type",
                    accessor: "boat.boatType.name"
                },
                {
                    Header: "Ad ID",
                    accessor: "boat.adId"
                },
                {
                    Header: "Seller",
                    accessor: "seller",
                    Cell: ({ value }) => <span>{value.firstName} {value.lastName}</span>
                },
                {
                    Header: "Requested",
                    accessor: "createdAt",
                    Cell: ({ value }) => formattedDate(value)
                },
                {
                    Header: "Status",
                    accessor: "buySellProcess",
                    Cell: ({ value }) => <span className="sales-engine-status">{value[value.length - 1]}</span>
                },
                {
                    Header: "Sales Engine",
                    accessor: "id",
                    Cell: ({ value }) => (
                        <div className="d-flex flex-row justify-content-around">
                            <button
                                type="button"
                                onClick={() => props.getSingleSalesEngine(value)}
                                className="btn btn-outline-info"
                            >
                                View
                          </button>
                        </div>
                    )
                },
            ],
        }
    }

    componentDidMount() {
        const { getSalesEngineByBuyer, isBuyItNow } = this.props

        getSalesEngineByBuyer({ isBuyItNow })
    }

    render() {

        const { columns } = this.state
        const { salesEngines } = this.props

        return (
            <>
                {columns &&
                    <div className="manage-dashboard-table">
                        <TableCard
                            title="Sales Engine"
                            columns={columns}
                            data={salesEngines}
                            noButton
                        />
                    </div>
                }
            </>
        )
    }

}


const mapStateToProps = state => ({
    salesEngines: state.salesEngineReducer.salesEngines,
})

const mapDispatchToProps = dispatch => ({
    getSalesEngineByBuyer: data => dispatch(getSalesEngineByBuyer(data)),
    getSingleSalesEngine: data => dispatch(getSingleSalesEngine(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(BuyerSalesEngine)