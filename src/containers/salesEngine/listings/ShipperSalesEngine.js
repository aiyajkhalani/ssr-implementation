import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    getSalesEngineByShipper,
    shipperAcceptShipmentRequest,
    shipperDeclineShipmentRequest,
    addShipmentProposal,
    clearSalesEngineShipperDashboardFlags,
    startShipment,
    completeShipment,
    addShipperDocuments,
    clearShipperDocument,
    getAgreementContents,
} from '../../../redux/actions/salesEngineAction';
import { TableCard } from '../../../components'
import { formattedDate } from '../../../helpers/string';
import { ShipmentProposal } from '../../../components/salesEngine/ShipmentProposal';
import { ShipmentDocument } from '../../../components/salesEngine/ShipmentDocument';

import { SuccessNotify } from '../../../helpers/notification';
import { salesEngineStatus, salesEngineAgreementType } from '../../../util/enums/enums';
import { Dialog, DialogContent, Grid } from '@material-ui/core';
import { latLng } from '../../../util/enums/enums';
import GoogleMap from "../../../components/map/map";

import "../../../styles/manageDashboardTableResponsive.scss"
import { SalesEngineDetail } from '../../../components/salesEngine/SalesEngineDetail';

class ShipperSalesEngine extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [

                {
                    Header: "Boat Type",
                    accessor: "boat.boatType.name"
                },
                {
                    Header: "Boat Name",
                    accessor: "boat.boatName"
                },
                {
                    Header: "View Details",
                    accessor: "shipmentLocation",
                    Cell: ({ value, original }) =>
                        <button className="btn btn-outline-primary" disabled={!value} type="button" onClick={() => this.locationModalHandler(value, original.id)}>
                            {'View Shipment Location'}
                        </button>
                },
                {
                    Header: "Status",
                    accessor: "buySellProcess",
                    Cell: ({ value }) => <span className="sales-engine-status">{value[value.length - 1]}</span>
                },
                {
                    Header: "Action",
                    accessor: "id",
                    Cell: ({ value, original }) => {

                        const { currentUser, shipperAcceptShipmentRequest, shipperDeclineShipmentRequest, startShipment, completeShipment } = props

                        const shipperHasAcceptedRequest = original.shipperRequested.reduce((ids, item) => {
                            return ids = item.shipper ? [...ids, item.shipper.id] : ids
                        }, []).includes(currentUser.id)

                        const shipper = original.shipperRequested && original.shipperRequested.find(item => item.shipper && item.shipper.id === currentUser.id)
                        const status = shipper ? shipper.price ? "Submitted Proposal" : "In Progress" : ""

                        return (
                            <div className="d-flex flex-column justify-content-center align-items-center width-100">
                                {shipperHasAcceptedRequest &&
                                    <span className="mb-2 sales-engine-status">{status}</span>
                                }

                                {!shipperHasAcceptedRequest &&
                                    < div className="d-flex w-100 justify-content-center">
                                        <div>
                                            <button
                                                type="button "
                                                onClick={() => shipperAcceptShipmentRequest(value)}
                                                className="btn btn-xs btn-secondary radius-20 info-button background-dark-blue border-dark-blue mr-2 shipper-sales-engine-dashboard-btn"
                                            >
                                                {'Accept'}
                                            </button>
                                        </div>
                                        <div>
                                            <button
                                                type="button"
                                                onClick={() => shipperDeclineShipmentRequest(value)}
                                                className="btn btn-xs btn-danger radius-20 shipper-sales-engine-dashboard-btn"
                                            >
                                                {'Decline'}
                                            </button>
                                        </div>
                                    </div>
                                }
                                {
                                    original.buySellProcess[original.buySellProcess.length - 1]
                                    === salesEngineStatus.shipperPaymentCompleted && shipperHasAcceptedRequest &&
                                    <div>
                                        <button
                                            type="button "
                                            onClick={() => startShipment({ id: original.id })}
                                            className="btn btn-xs btn-secondary radius-20 info-button background-dark-blue border-dark-blue mr-2 shipper-sales-engine-dashboard-btn"
                                        >
                                            {'Start Shipment'}
                                        </button>
                                    </div>}
                                {shipperHasAcceptedRequest && original.buySellProcess[original.buySellProcess.length - 1]
                                    === salesEngineStatus.buyerAddedShipmentLocationCompleted && <div>
                                        <button
                                            type="button"
                                            onClick={() => this.modalHandler('proposalModal', value)}
                                            className="btn btn-outline-primary"
                                        >
                                            {'Add Proposal'}
                                        </button>
                                    </div>
                                }
                                {
                                    original.buySellProcess[original.buySellProcess.length - 1]
                                    === salesEngineStatus.shipmentStarted && <div>
                                        <button
                                            type="button"
                                            onClick={() => completeShipment({ id: original.id })}
                                            className="btn btn-outline-primary"
                                        >
                                            {'Complete Shipment'}
                                        </button>
                                    </div>
                                }
                                {original.buySellProcess[original.buySellProcess.length - 1]
                                    === salesEngineStatus.shipmentCompleted &&
                                    <div>
                                        <button
                                            type="button"
                                            onClick={() => this.modalHandler('documentModal', original.id)}
                                            className="btn btn-outline-primary"
                                        >
                                            {'Add Document'}
                                        </button>
                                    </div>
                                }
                                {original.buySellProcess[original.buySellProcess.length - 1]
                                    === salesEngineStatus.shipperAddedDocument &&
                                    <div>
                                        <button
                                            type="button"
                                            disabled
                                            className="btn btn-outline-info"
                                        >
                                            {'Shipment Process Done'}
                                        </button>
                                    </div>
                                }
                            </div >
                        )
                    }
                },
                {
                    Header: "Sales Engine Details",
                    accessor: "id",
                    Cell: ({ value, original }) =>
                        <div>
                            <button
                                type="button"
                                onClick={() => {
                                    this.props.getAgreementContents({
                                        id: value,
                                        type: salesEngineAgreementType.transactionGeneralRules
                                    })
                                    this.modalHandler('salesEngineModal', value)
                                }}
                                className="btn btn-outline-primary"
                            >
                                {'View'}
                            </button>
                        </div>

                },
                {
                    Header: "Date",
                    accessor: "createdAt",
                    Cell: ({ value }) => formattedDate(value)
                },
            ],

            proposalModal: false,
            documentModal: false,
            locationModal: false,
            salesEngineModal: false,

            selectedSalesEngineId: "",
            shipmentLocation: null
        }
    }

    componentDidMount() {
        const { getSalesEngineByShipper } = this.props
        getSalesEngineByShipper()
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        const { shipperAcceptShipperRequestSuccess, getSalesEngineByShipper, clearSalesEngineShipperDashboardFlags, shipperDocumentSuccess, clearShipperDocument } = nextProps
        if (shipperAcceptShipperRequestSuccess) {
            SuccessNotify("Request Accept Successfully.")
            getSalesEngineByShipper()
            clearSalesEngineShipperDashboardFlags()
        }

        if (shipperDocumentSuccess) {
            SuccessNotify("Document uploaded successfully.")
            clearShipperDocument()
            getSalesEngineByShipper()
        }

        return null
    }

    renderMapDetails = (boat, shipment) => {

        return (
            <>
                <div>
                    <label>Boat Location</label>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                Country: {boat.country}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                State: {boat.state}
                            </div>
                        </Grid>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                City: {boat.city}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                Postal Code: {boat.postalCode}
                            </div>
                        </Grid>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                Full Address: {boat.address}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                Place: {boat.placeName}
                            </div>
                        </Grid>
                    </div>
                </div>
                <div>
                    <label>Shipment Location</label>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                Country: {shipment.country}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                State: {shipment.state}
                            </div>
                        </Grid>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                City: {shipment.city}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                Postal Code: {shipment.postalCode}
                            </div>
                        </Grid>
                    </div>
                    <div style={{ display: "flex", flexDirection: "row" }}>

                        <Grid item xs={6}>
                            <div className="map-field">
                                Full Address: {shipment.address}
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="map-field">
                                Place: {shipment.placeName}
                            </div>
                        </Grid>
                    </div>
                </div>
            </>
        )
    }

    modalHandler = (key, selectedSalesEngineId = "") => this.setState(prevState => ({ [key]: !prevState[key], selectedSalesEngineId }))

    locationModalHandler = (shipmentLocation = null, selectedSalesEngineId) => {
        this.setState(preState => ({ locationModal: !preState.locationModal, shipmentLocation, selectedSalesEngineId }));
    };

    submitProposalHandler = async (values) => {
        const { selectedSalesEngineId } = this.state
        const { addShipmentProposal } = this.props
        await addShipmentProposal({ ...values, id: selectedSalesEngineId })
        this.modalHandler('proposalModal')
    }

    submitDocumentHandler = (values) => {
        const { currentUser, addShipperDocuments } = this.props
        const { selectedSalesEngineId } = this.state

        values.shipper = currentUser.id
        values.salesEngine = selectedSalesEngineId
        addShipperDocuments(values)
        this.modalHandler('documentModal')
    }

    render() {

        const { columns, proposalModal, documentModal, locationModal, shipmentLocation, selectedSalesEngineId, salesEngineModal } = this.state
        const { salesEngines, history, agreementsContents } = this.props
        const salesEngine = salesEngines && salesEngines.find((item) => (item.id === selectedSalesEngineId))

        return (
            <>
                {shipmentLocation && shipmentLocation.address && shipmentLocation.geometricLocation && shipmentLocation.geometricLocation.coordinates.length &&
                    <Dialog
                        open={locationModal}
                        onClose={this.locationModalHandler}
                        classes={{ container: "model-width" }}
                        aria-labelledby="responsive-dialog-title"
                    >
                        <DialogContent>
                            <>
                                {this.renderMapDetails(salesEngine.boat, salesEngine.shipmentLocation)}
                                <GoogleMap
                                    className="googleMap-position"
                                    latLng={{ lat: salesEngine.boat.geometricLocation.coordinates[1], lng: salesEngine.boat.geometricLocation.coordinates[0] }}
                                    height={42}
                                    width={150}
                                    columnName={"address"}
                                    pathPoints={[{ lat: salesEngine.boat.geometricLocation.coordinates[1], lng: salesEngine.boat.geometricLocation.coordinates[0] },
                                    { lat: salesEngine.shipmentLocation.geometricLocation.coordinates[1], lng: salesEngine.shipmentLocation.geometricLocation.coordinates[0] }]}
                                    isShipperLocation
                                ></GoogleMap>
                            </>

                        </DialogContent>
                    </Dialog>
                }

                <ShipmentProposal
                    history={history}
                    open={proposalModal}
                    selectedSalesEngineId={selectedSalesEngineId}
                    salesEngines={salesEngines}
                    value={null}
                    closeModal={() => this.modalHandler('proposalModal')}
                    onSubmit={this.submitProposalHandler}
                />
                <SalesEngineDetail
                    history={history}
                    open={salesEngineModal}
                    selectedSalesEngineId={selectedSalesEngineId}
                    salesEngines={salesEngines}
                    value={null}
                    closeModal={() => this.modalHandler('salesEngineModal')}
                    agreementsContents={agreementsContents}
                />
                <ShipmentDocument
                    history={history}
                    open={documentModal}
                    value={null}
                    closeModal={() => this.modalHandler('documentModal')}
                    submitDocument={this.submitDocumentHandler}
                />
                {columns &&
                    <div className="manage-dashboard-table">
                        <TableCard
                            title="Sales Engine"
                            columns={columns}
                            data={salesEngines}
                            noButton
                        />
                    </div>
                }
            </>
        )
    }

}


const mapStateToProps = state => ({
    currentUser: state.loginReducer.currentUser,
    salesEngines: state.salesEngineReducer.salesEngines,
    shipperAcceptShipperRequestSuccess: state.salesEngineReducer.shipperAcceptShipperRequestSuccess,
    shipperDeclineShipperRequestSuccess: state.salesEngineReducer.shipperDeclineShipperRequestSuccess,
    shipperDocumentSuccess: state.salesEngineReducer.shipperDocumentSuccess,
    agreementsContents: state.salesEngineReducer.agreementsContents,

})

const mapDispatchToProps = dispatch => ({
    getSalesEngineByShipper: data => dispatch(getSalesEngineByShipper(data)),
    shipperAcceptShipmentRequest: data => dispatch(shipperAcceptShipmentRequest(data)),
    shipperDeclineShipmentRequest: data => dispatch(shipperDeclineShipmentRequest(data)),
    addShipmentProposal: data => dispatch(addShipmentProposal(data)),
    clearSalesEngineShipperDashboardFlags: () => dispatch(clearSalesEngineShipperDashboardFlags()),
    startShipment: (data) => dispatch(startShipment(data)),
    completeShipment: (data) => dispatch(completeShipment(data)),
    addShipperDocuments: (data) => dispatch(addShipperDocuments(data)),
    clearShipperDocument: () => dispatch(clearShipperDocument()),
    getAgreementContents: data => dispatch(getAgreementContents(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ShipperSalesEngine)