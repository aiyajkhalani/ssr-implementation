import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    getSalesEngineBySeller,
} from '../../../redux/actions/salesEngineAction';
import { TableCard } from '../../../components'

import "../../../styles/manageDashboardTableResponsive.scss"

class SellerSalesEngine extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {
                    Header: "Model Name/No.",
                    accessor: "boat.boatName"
                },
                {
                    Header: "No. of Boat Inquiries",
                    accessor: "sellerBoatInquiry"
                },
                {
                    Header: "Ad ID",
                    accessor: "boat.adId"
                },
                {
                    Header: "Sales Engine",
                    accessor: "boat.id",
                    Cell: ({ value }) => (
                        <div className="d-flex flex-row justify-content-around">
                            <button
                                type="button"
                                onClick={() => props.history.push(`boat-sales-engines/${value}`)}
                                className="btn btn-outline-info"
                            >
                                View
                      </button>
                        </div>
                    )
                },
            ]
        }
    }

    componentDidMount() {
        const { getSalesEngineBySeller } = this.props
        getSalesEngineBySeller()
    }

    render() {

        const { columns } = this.state
        const { salesEnginesBoats } = this.props

        return (
            <>
                {columns &&
                    <div className="manage-dashboard-table">
                        <TableCard
                            title="Sales Engine"
                            columns={columns}
                            data={salesEnginesBoats}
                            noButton
                        />
                    </div>
                }
            </>
        )
    }

}


const mapStateToProps = state => ({
    salesEnginesBoats: state.salesEngineReducer.salesEnginesBoats,
})

const mapDispatchToProps = dispatch => ({
    getSalesEngineBySeller: data => dispatch(getSalesEngineBySeller(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SellerSalesEngine)