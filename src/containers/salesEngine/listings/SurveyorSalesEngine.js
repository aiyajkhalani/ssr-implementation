import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';

import {
    getSalesEngineBySurveyor,
    surveyorAcceptBuyer,
    surveyorDeclineBuyer,
    clearSalesEngineSurveyorDashboardFlags,
} from '../../../redux/actions/salesEngineAction';
import { TableCard } from '../../../components'
import { salesEngineStatus } from '../../../util/enums/enums';

import "../../../styles/manageDashboardTableResponsive.scss"
import { SuccessNotify } from '../../../helpers/notification';

class SurveyorSalesEngine extends Component {


    static getDerivedStateFromProps(nextProps, prevState) {

        const { surveyorAcceptSuccess, clearSalesEngineSurveyorDashboardFlags, getSalesEngineBySurveyor, surveyorDeclineSuccess } = nextProps
        if (surveyorAcceptSuccess) {
            SuccessNotify("Request Accept Successfully.")
            clearSalesEngineSurveyorDashboardFlags()
            getSalesEngineBySurveyor()
        }
        if (surveyorDeclineSuccess) {
            SuccessNotify("Request Declined Successfully.")
            clearSalesEngineSurveyorDashboardFlags()
            getSalesEngineBySurveyor()
        }

        return null
    }

    state = {
        columns: [
            {
                Header: "Boat Type",
                accessor: "boat.boatType.name"
            },
            {
                Header: "Ad ID",
                accessor: "boat.adId"
            },
            {
                Header: "Buyer",
                accessor: "buyer",
                Cell: ({ value }) => <span>{value.firstName} {value.lastName}</span>
            },
            {
                Header: "Seller",
                accessor: "seller",
                Cell: ({ value }) => <span>{value.firstName} {value.lastName}</span>
            },
            {
                Header: "Status",
                accessor: "buySellProcess",
                Cell: ({ value }) => <span className="sales-engine-status">{value[value.length - 1]}</span>
            },
            {
                Header: "Download",
                Cell: ({ value }) =>
                    <div className="d-flex flex-column">
                        <a href="https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/1441686261-AdamSea_Report_Sample_page.docx.vnd.openxmlformats-officedocument.wordprocessingml.document" >Survey Report</a>
                        <a href="https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/CORP62S1AVCI-surveyor_report (3).doc.msword" >Request</a>
                        <a href="https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/CORP62S1AVCI-report_verification (2).doc.msword" >Verification</a>
                    </div>
            },
            {
                Header: "Action",
                accessor: "action",
                Cell: ({ original }) => (
                    <>
                        {original.surveyorAccepted && (
                            original.surveyorReport
                                ? <a href={original.surveyDocument} >Report Submitted</a>
                                :
                                <Button className="btn submit-report-btn"
                                    onClick={() => this.submitReport(original.id)}
                                    disabled={original.buySellProcess[original.buySellProcess.length - 1] === salesEngineStatus.surveyorPaymentPending}>
                                    {'Submit Report'}
                                </Button>
                        )}

                        {!original.surveyorAccepted &&
                            <>
                                <Button className="btn btn-primary mr-10 w-auto accept-btn-form mr-2 surveyor-sales-engine-button"
                                    onClick={() => this.props.surveyorAcceptBuyer({ id: original.id, surveyorId: this.props.currentUser.id })}>
                                    {'Accept'}
                                </Button>
                                <Button className="btn btn-danger w-auto decline-btn-form text-white surveyor-sales-engine-button"
                                    onClick={() => this.props.surveyorDeclineBuyer({ id: original.id, surveyorId: this.props.currentUser.id })}>
                                    {'Decline'}
                                </Button>
                            </>
                        }
                    </>
                ),
            }
        ]
    }

    componentDidMount() {
        const { getSalesEngineBySurveyor } = this.props
        getSalesEngineBySurveyor()
    }

    submitReport = (id) => {
        this.props.history.push('/add-surveyor-report', { salesEngineId: id })
    }

    render() {

        const { columns } = this.state
        const { salesEngines } = this.props

        return (
            <>
                {columns &&
                    <div className="manage-dashboard-table">
                        <TableCard
                            title="Sales Engine"
                            columns={columns}
                            data={salesEngines}
                            noButton
                        // salesEngineHeader={salesEngineHeader}
                        />
                    </div>
                }
            </>
        )
    }

}


const mapStateToProps = state => ({
    currentUser: state.loginReducer.currentUser,
    salesEngines: state.salesEngineReducer.salesEngines,
    surveyorAcceptSuccess: state.salesEngineReducer.surveyorAcceptSuccess,
    surveyorDeclineSuccess: state.salesEngineReducer.surveyorDeclineSuccess,
})

const mapDispatchToProps = dispatch => ({
    getSalesEngineBySurveyor: data => dispatch(getSalesEngineBySurveyor(data)),
    surveyorAcceptBuyer: data => dispatch(surveyorAcceptBuyer(data)),
    surveyorDeclineBuyer: data => dispatch(surveyorDeclineBuyer(data)),
    clearSalesEngineSurveyorDashboardFlags: () => dispatch(clearSalesEngineSurveyorDashboardFlags()),
})

export default connect(mapStateToProps, mapDispatchToProps)(SurveyorSalesEngine)