import React, { Component } from 'react'
import { connect } from 'react-redux';

import {
    getSalesEngineByBoat,
    getSingleSalesEngine,
    clearSalesEngines,
} from '../../../redux/actions/salesEngineAction';
import { TableCard, DashboardLayout } from '../../../components'

import "../../../styles/manageDashboardTableResponsive.scss"
import { formattedDate } from '../../../helpers/string';

class BoatSalesEngine extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columns: [
                {
                    Header: "Model Name/No.",
                    accessor: "boat.boatName"
                },
                {
                    Header: "Ad ID",
                    accessor: "boat.adId"
                },
                {
                    Header: "Buyer",
                    accessor: "buyer",
                    Cell: ({ value }) => <span>{value.firstName} {value.lastName}</span>
                },
                {
                    Header: "Requested",
                    accessor: "createdAt",
                    Cell: ({ value }) => formattedDate(value)
                },
                {
                    Header: "Status",
                    accessor: "buySellProcess",
                    Cell: ({ value }) => <span >{value[value.length - 1]}</span>
                },
                {
                    Header: "Sales Engine",
                    accessor: "id",
                    Cell: ({ value }) => (
                        <div className="d-flex flex-row justify-content-around">
                            <button
                                type="button"
                                onClick={() => props.getSingleSalesEngine(value)}
                                className="btn btn-outline-info"
                            >
                                View
                          </button>
                        </div>
                    )
                },
            ]
        }
    }

    static getDerivedStateFromProps(nextProps) {
        const { getSalesEngineSuccess, history } = nextProps

        if (getSalesEngineSuccess) {
            history && history.push('/sales-engine-process')
        }

        return null
    }

    componentDidMount() {
        const { getSalesEngineByBoat, match: { params } } = this.props
        params && params.id && getSalesEngineByBoat(params.id)
    }

    componentWillUnmount() {
        const { clearSalesEngines } = this.props
        clearSalesEngines()
    }

    render() {

        const { columns } = this.state
        const { salesEngines } = this.props

        return (
            <>
                <DashboardLayout>
                    {columns &&
                        <div className="manage-dashboard-table">
                            <TableCard
                                title="View Boat Sales Engine"
                                columns={columns}
                                data={salesEngines}
                                noButton
                            />
                        </div>
                    }
                </DashboardLayout>
            </>
        )
    }

}


const mapStateToProps = state => ({
    salesEngines: state.salesEngineReducer.salesEngines,
    getSalesEngineSuccess: state.salesEngineReducer.getSalesEngineSuccess,
})

const mapDispatchToProps = dispatch => ({
    getSalesEngineByBoat: data => dispatch(getSalesEngineByBoat(data)),
    getSingleSalesEngine: data => dispatch(getSingleSalesEngine(data)),
    clearSalesEngines: () => dispatch(clearSalesEngines()),
})

export default connect(mapStateToProps, mapDispatchToProps)(BoatSalesEngine)