import React, { Component } from "react";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Grid, Link, Box } from "@material-ui/core";
import Rating from "react-rating";
import Truncate from "react-truncate";
import { Player, BigPlayButton } from "video-react";
import { connect } from "react-redux";
import ImageGallery from "react-image-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import "react-image-gallery/styles/css/image-gallery.css";

import InnerRating from "../../components/staticComponent/InnerRating";
import InnerReviews from "../../components/gridComponents/InnerReviews";
import GoogleMarker from "../../components/map/marker";
import { Layout, Loader } from "../../components";
import "./boatServiceInner.scss";
import { editYachtService } from "../../redux/actions";
import { getBoatServiceMarkers } from "../../helpers/boatHelper";
import { icons } from "../../util/enums/enums";
import {
  snakeCase,
  sliderImageHelper,
  displayDefaultReview
} from "../../helpers/string";
import { clearReviewFlag } from "../../redux/actions/ReviewAction";
import moment from "moment";
import { InnerBannerLikesDiv } from "../../components/styleComponent/styleComponent";
import ReadMore from "../../components/helper/truncate";
import { getSingleServiceMarker } from "../../helpers/marinaHelper";
import { prepareGalleryData } from "../../util/utilFunctions";

class BoatServiceInner extends Component {
  state = {
    yachtId: "",
    bottom: 110,
    isGallery: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isReview, clearReviewFlag, editYachtService, match } = nextProps;
    const { params } = match && match;

    const { yachtId } = prevState;
    if (isReview) {
      yachtId && editYachtService(yachtId);
      clearReviewFlag();
    }
    if (params && params.id) {
      return {
        yachtId: params.id
      };
    }

    return null;
  }

  async componentDidMount() {
    const { yachtId } = this.state;
    const { editYachtService } = this.props;

    const bottomHeight = document.querySelector(".image-gallery-thumbnails");
    const bottom = bottomHeight && bottomHeight.offsetHeight + 15;
    bottom && this.setState({ bottom });
    yachtId && editYachtService(yachtId);
  }

  toggleGalleryHandler = () => this.setState(prevState => ({ isGallery: !prevState.isGallery }));

  render() {

    const { history, currentUser, yachtService } = this.props;
    const { bottom, isGallery } = this.state;

    return (
      <Layout className="boat-service-inner-responsive">
        {yachtService && !yachtService.id ? (
          <Loader />
        ) : (
            <>
              <Grid
                container
                className="container-fluid clr-fluid sticky-on-top mb-3"
              >
                <div className="m-auto d-flex h-100 w-85vw pt-2 pb-2">
                  <Grid item className="h-70 d-flex pr-0 pt-3 pb-3 pl-0" sm={4}>
                    <div className="width-100 align-self-center">
                      <div className="font-weight-500">
                        <span className="ad-id-inner font-weight-400 light-green">
                          Ad ID: {yachtService.adId}
                        </span>
                      </div>
                      <div className="d-flex align-items-center">
                        <span className="service-boat-name gray-dark">
                          {(yachtService.user && yachtService.user.companyName) ||
                            "ServiceInner"}
                        </span>
                      </div>
                      <span className="city-boat-inner font-weight-400 gray-light">
                        Glasgow, United Kingdom
                    </span>
                    </div>
                  </Grid>

                  <Grid
                    item
                    className="h-70 d-flex align-items-center pt-3 pb-3 pr-0 pl-0 justify-content-center"
                    sm={4}
                  >
                    <div className="widget-media text-center">
                      <ul className="icon-effect icon-effect-1a d-flex">
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center cursor-pointer service-inner-header-social-icon"
                          >
                            <i className="adam-share gray-light"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center cursor-pointer service-inner-header-social-icon"
                          >
                            <i className="adam-heart1 gray-light"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center cursor-pointer service-inner-header-social-icon"
                          >
                            <i className="adam-flag-3 gray-light"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center cursor-pointer service-inner-header-social-icon"
                          >
                            <i className="adam-envlope gray-light"></i>
                          </Link>
                        </li>
                        <li>
                          <Link
                            to="/"
                            className="icon d-flex justify-content-center cursor-pointer service-inner-header-social-icon"
                          >
                            <i className="adam-pdf gray-light"></i>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </Grid>

                  {yachtService && yachtService.ratedReviews && (
                    <Grid item className="h-70" sm={4}>
                      <div className="d-flex justify-content-end align-items-center h-100 pl-0 pr-0">
                        <div className="d-flex flex-column h-100 justify-content-evenly">
                          <div className="d-flex flex-column">
                            <div className="d-flex align-items-center justify-content-end">
                              <div className="d-flex mr-2 mb-1 d-m-o">
                                <Rating
                                  className="rating-clr rating-small-size"
                                  initialRating={
                                    yachtService.ratedReviews.ratings &&
                                    yachtService.ratedReviews.ratings
                                      .averageRating
                                  }
                                  fullSymbol={<StarIcon />}
                                  emptySymbol={<StarBorderIcon />}
                                  placeholderSymbol={<StarBorderIcon />}
                                  readonly
                                />
                              </div>
                              <h6 className="m-0 rated-reviews-inner-avg font-weight-400 dark-silver">
                                {(yachtService.ratedReviews &&
                                  yachtService.ratedReviews.ratings &&
                                  yachtService.ratedReviews.ratings
                                    .averageRating) ||
                                  0.0}
                              </h6>
                            </div>
                            {/* <h6 className="m-0 rated-reviews-inner-avg font-weight-400 dark-silver">
                              {yachtService.ratedReviews.ratings
                                .averageRating || 0.0}
                            </h6>
                          </div> */}

                            <span className="rated-reviews-inner font-weight-400 gray-light">
                              <span className="rated-reviews-inner-count mr-1">
                                {yachtService.ratedReviews.ratings
                                  ? yachtService.ratedReviews.ratings.count
                                  : 0}
                              </span>
                              Customer Reviews
                          </span>
                          </div>
                        </div>
                      </div>
                    </Grid>
                  )}
                </div>
              </Grid>

              <div className="boat-service-inner-container">
                <Grid container spacing={3}>
                  <Grid item sm={6}>
                    <div className="boat-service-inner-carousel position-relative">
                      <InnerBannerLikesDiv
                        bottom={bottom}
                        className="position-absolute boat-inner-banner-likes-div d-flex align-items-center font13 gray-dark img-show-text"
                      >
                        <div className="d-flex justify-content-center align-items-center ">
                          <img
                            src={require("../../assets/images/rentInner/like1.png")}
                            className="inner-banner-views-img mr-1"
                            alt=""
                          />
                          <div className="mt-1 banner-count-text">0</div>
                        </div>
                      </InnerBannerLikesDiv>
                      <div className="position-absolute inner-banner-top-social-div">
                        <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
                          <div className="d-flex justify-content-center align-items-center ">
                            <img
                              src={require("../../assets/images/rentInner/view.png")}
                              className="inner-banner-views-img mr-2"
                              alt=""
                            />
                          </div>
                          <div className="banner-count-text">
                            {yachtService.userCount}
                          </div>
                        </div>
                        <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
                          <div className="d-flex justify-content-center align-items-center mr-2">
                            <img
                              src={require("../../assets/images/rentInner/image-count.png")}
                              className="inner-banner-views-img"
                              alt=""
                            />
                          </div>
                          <div className="banner-count-text">
                            {yachtService.images.length}
                          </div>
                        </div>
                      </div>

                      <ImageGallery
                        items={sliderImageHelper(yachtService.images)}
                        showFullscreenButton={false}
                        showPlayButton={false}
                        onClick={this.toggleGalleryHandler}
                      />
                    </div>
                  </Grid>
                  <Grid item sm={6} className="pl-5">
                    <Grid container>
                      <Grid item sm={12} className="boat-service-inner-detail">
                        <div>
                          <h4 className="our-service-inner gray-dark">
                            {/* <i className="adam-invoice mr-2 gray-dark"></i> */}
                            Our Service
                        </h4>
                          <p className="text-justify a-inner-fix">
                            <ReadMore className="text-justify ">
                              {yachtService.service}
                            </ReadMore>
                          </p>
                        </div>
                      </Grid>
                      <Grid item sm={12}>
                        <div className="col-12 p-0">
                          <div className="card border mb-2 shadow-sm user-card-responsive">
                            <div className="card-body mt-0">
                              <div className="col-12 brand">
                                <div className="col-12">
                                  <div className="media media-lg mt-3 mb-3 d-flex flex-column h-100 pt-0 no-width">
                                    <div className="d-flex w-100 justify-content-between">
                                      {yachtService && yachtService.user && (
                                        <img
                                          className="rounded-circle cursor-pointer"
                                          src={yachtService.user.image}
                                          alt=""
                                          onClick={() => {
                                            history.push("/user-profile", {
                                              seller: yachtService.user
                                            });
                                          }}
                                        />
                                      )}
                                      <div className="media-body mt-2 d-flex pl-5 align-items-center">
                                        <div className="author-info d-flex flex-column">
                                          <span className="name-service-user font-weight-500 gray-dark">
                                            <span className="mr-1">
                                              {yachtService.user &&
                                                yachtService.user.firstName}
                                            </span>
                                            <span>
                                              {" "}
                                              {yachtService.user &&
                                                yachtService.user.lastName}
                                            </span>
                                          </span>

                                          <span className=" font-weight-400 dark-silver">
                                            {yachtService.user &&
                                              yachtService.user.city}
                                            ,
                                          {yachtService.user &&
                                              yachtService.user.country}
                                          </span>
                                        </div>
                                      </div>
                                      {yachtService &&
                                        yachtService.user &&
                                        yachtService.user.companyLogo && (
                                          <span className="logo-user-service">
                                            <img
                                              className="d-block img-fluid"
                                              src={yachtService.user.companyLogo}
                                              alt=""
                                            />
                                          </span>
                                        )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-12">
                                <ul className="list-group f-14 d-flex flex-row">
                                  <li className="list-group-item dark-silver border-0">
                                    <span className="card-i">
                                      <i className="adam-user dark-silver"></i>
                                    </span>
                                    {yachtService &&
                                      yachtService.user &&
                                      yachtService.user.role &&
                                      yachtService.user.role.role}
                                  </li>
                                  <li className="list-group-item dark-silver border-0">
                                    <span className="card-i">
                                      <i className="adam-calendar11 dark-silver"></i>
                                    </span>
                                    On AdamSea since{" "}
                                    {moment(
                                      yachtService &&
                                      yachtService.user &&
                                      yachtService.user.createdAt
                                    ).format("MMM, YYYY")}
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Grid>

                      <Grid item sm={12}>
                        <div className="col-12 p-0">
                          <div className="mb-0 text-right width-100 d-flex justify-content-end align-items-center light-green">
                            <div className="mr-2">
                              <img
                                src={require("../../assets/images/boatInner/serviceinner-like.png")}
                                alt=""
                                className="feedback-section light-green inner-feedback-img"
                              />
                            </div>
                            <span className="feedback-section-text font-weight-400 light-green">
                              ADD FEEDBACK
                          </span>
                          </div>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container className="mt-4 mt-re-0" spacing={3}>
                  <Grid item sm={6}>
                    <h4 className="mt-5 mb-4 gray-dark d-flex align-items-center mt-d-0-inner">
                      <div className="mr-2">
                        <img
                          src={require("../../assets/images/rentInner/service.png")}
                          alt=""
                          className="inner-title-icons"
                        />
                      </div>
                      Service We Provide
                  </h4>

                    {yachtService &&
                      yachtService.serviceProvide &&
                      yachtService.serviceProvide.length > 0 && (
                        <div className="col-md-12">
                          <Grid container>
                            <div className="row d-flex">
                              {yachtService &&
                                yachtService.serviceProvide &&
                                yachtService.serviceProvide.length &&
                                yachtService.serviceProvide.map(item => {
                                  return (
                                    <div className="mb-4 mr-4 mt-0 ml-0 inner-type-div-margin ">
                                      <div className="inner-type-div m-auto">
                                        <div className="inner-trip-type-icon-div mb-2 mt-1 dark-silver ">
                                          <i
                                            className={`dark-silver ${item &&
                                              icons[snakeCase(item.name)]}  `}
                                          ></i>
                                        </div>
                                        <div className="inner-trip-type-text-div">
                                          <span className="font-16 trip-type-inner sub-title-sm dark-silver">
                                            {item.name}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  );
                                })}
                            </div>
                          </Grid>
                        </div>
                      )}
                  </Grid>
                  <Grid item sm={6} className="pl-5">
                    <Grid container spacing="3">
                      <Grid item sm={6} className="map-service-inner">
                        <div className="">
                          <h4 className="mt-5 mb-4 gray-dark d-flex align-items-center mt-d-0-inner">
                            <div className="mr-2">
                              <img
                                src={require("../../assets/images/rentInner/location.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Facility Location
                        </h4>
                          <div className="boat-inner-map-div">
                            {yachtService && (
                              <GoogleMarker
                                className="boat-inner-map-height"
                                markers={getSingleServiceMarker(yachtService)}
                              />
                            )}
                          </div>
                        </div>
                      </Grid>
                      { yachtService && yachtService.youtubeUrl && 
                        yachtService.youtubeUrl.length > 0 && (
                      <Grid item sm={6}>
                        <div className="align-items-end justify-content-between">
                          <h4 className="mt-5 mb-4 d-flex align-items-center gray-dark mt-d-0-inner">
                            <div className="mr-2">
                              <img
                                src={require("../../assets/images/rentInner/videos.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Related Videos
                        </h4>
                          <div className="video-container h-100 w-100">
                            <Player
                              controls={false}
                              autoPlay="false"
                              playsInline
                              muted={true}
                              poster="https://adamsea-dev.s3-ap-southeast-1.amazonaws.com/images/boat7.jpeg.jpeg"
                              src={yachtService.youtubeUrl}
                            >
                              <BigPlayButton position="center" />
                            </Player>
                          </div>
                        </div>
                      </Grid>
                         ) }
                    </Grid>
                  </Grid>
                </Grid>
                <hr />
                <Grid container spacing={3} className="mt-50 last-service">
                  <Grid item sm={6}>
                    <Grid container>
                      <Grid item sm={12}>
                        <div className="mb-3">
                          <h4 className=" mb-3 gray-dark d-flex align-items-center">
                            <div className="mr-2">
                              <img
                                src={require("../../assets/images/rentInner/feature.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Our Features & Advantages
                        </h4>
                          <Truncate
                            lines={3}
                            className="image-location recent-service-description font-14 dark-silver"
                            ellipsis={
                              <span className="ml-1 boat-media-read-more-text light-green cursor-pointer">
                                Read More
                            </span>
                            }
                          >
                            {yachtService.featuresAndAdvantages}
                          </Truncate>
                        </div>
                      </Grid>
                      <Grid item sm={12}>
                        <div className="mt-20 mb-3">
                          <h4 className=" mb-3  gray-dark d-flex align-items-center">
                            <div className="mr-2">
                              <img
                                src={require("../../assets/images/rentInner/commitment.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Team Commitment
                        </h4>
                          {/* <p>
                          <Truncate
                            lines={4}
                            ellipsis={
                              <span className="font-12 read-more-text">
                                ..Read More
                              </span>
                            }
                          >
                            {yachtService.teamCommitment}
                          </Truncate>
                        </p> */}
                          <Truncate
                            lines={3}
                            className="image-location recent-service-description font-14 dark-silver"
                            ellipsis={
                              <span className="ml-1 boat-media-read-more-text light-green cursor-pointer">
                                Read More
                            </span>
                            }
                          >
                            <span> {yachtService.teamCommitment}</span>
                          </Truncate>
                        </div>
                      </Grid>
                      <Grid item sm={12}>
                        <div className="mt-20 mb-3">
                          <h4 className=" mb-3  gray-dark d-flex align-items-center">
                            <div className="mr-2 d-flex">
                              <img
                                src={require("../../assets/images/rentInner/star.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Our Quality of Service
                        </h4>
                          {/* <p>
                          <Truncate
                            lines={4}
                            ellipsis={
                              <span className="font-12 read-more-text">
                                ..Read More
                              </span>
                            }
                          >
                            {yachtService.qualityOfService}
                          </Truncate>
                        </p> */}
                          <Truncate
                            lines={3}
                            className="image-location recent-service-description font-14 dark-silver"
                            ellipsis={
                              <span className="ml-1 boat-media-read-more-text light-green cursor-pointer">
                                Read More
                            </span>
                            }
                          >
                            <span> {yachtService.qualityOfService} </span>
                          </Truncate>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item sm={6} className="pl-5">
                    <h4 className="mb-1  gray-dark h4-mb-0">
                      <i className="adam-coins mr-2 gray-light"></i>
                      Payment and Services Booking
                  </h4>
                    <span className="font-14 dark-silver recent-service-description">
                      Seller accept payment of the boat
                  </span>

                    <Grid container>
                      <div className="row width-100 mt-4">
                        <div className="col-4">
                          <div className="inner-payment-div">
                            <i className="adam-creditcard text-black d-flex align-items-center justify-content-center inner-payment-icon mb-2"></i>
                            <h6 className="title-description">
                              Credit Card Payment
                          </h6>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="inner-payment-div ml-18">
                            <div className="mb-2">
                              <div className="d-flex justify-content-center align-items-center mr-3">
                                <img
                                  src={require("../../assets/images/rentInner/payment.png")}
                                  className="inner-payment-cash-icon"
                                  alt=""
                                />
                              </div>
                            </div>
                            <h6 className="title-description">Cash on Arrival</h6>
                          </div>
                        </div>
                      </div>

                      <div className="row width-100 mt-4">
                        <div className="col-4">
                          <div className="inner-payment-div">
                            <div className="mb-2">
                              <div className="d-flex justify-content-center align-items-center mr-3">
                                <img
                                  src={require("../../assets/images/rentInner/deliverypayment.png")}
                                  className="inner-payment-cash-icon"
                                  alt=""
                                />
                              </div>
                            </div>
                            <h6 className="title-description">
                              Payment after Delivery
                          </h6>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="inner-payment-div">
                            <div className="mb-2">
                              <div className="d-flex justify-content-center align-items-center mr-3">
                                <img
                                  src={require("../../assets/images/rentInner/boxcoin.png")}
                                  className="inner-payment-cash-icon"
                                  alt=""
                                />
                              </div>
                            </div>
                            <h6 className="title-description">
                              50% Down Payment
                          </h6>
                          </div>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <hr />
                <Grid container>
                  <Grid item sm={12} md={12} lg={12}>
                    <div className="container">
                      <div className="row mt-50 mb-5">
                        <div className="col-lg-12 col-md-12 col-sm-12 p-0">
                          <h4 className="gray-dark">
                            <i className="iconColor adam-review mr-2 text-black gray-dark"></i>
                            Review and Rating
                        </h4>
                          {yachtService && yachtService.ratedReviews && (
                            <>
                              <div className="rent-inner-contain inner-rating-section service-inner-ratings inner-rating-div">
                                <InnerRating
                                  iconColor="iconColor-rentInner"
                                  btnColor="rentInner-btnBg"
                                  btnBlue="rentInner-btnBlue"
                                  progressColor="rent-progress"
                                  moduleId={yachtService.id}
                                  userId={currentUser.id}
                                  data={displayDefaultReview(
                                    yachtService.ratedReviews.ratings
                                  )}
                                  reviews={yachtService.ratedReviews.reviews}
                                />
                              </div>
                              <div className="mt-5 user-review-rent inner-review-section">
                                <InnerReviews
                                  xs={12}
                                  sm={12}
                                  data={yachtService.ratedReviews.reviews}
                                  iconColor="iconColor-rentInner"
                                />
                              </div>
                            </>
                          )}
                        </div>
                      </div>
                      <hr />
                    </div>
                  </Grid>
                </Grid>
              </div>

              {yachtService && yachtService.images && yachtService.images.length > 0 && isGallery && (
                <ModalGateway>
                  <Modal onClose={this.toggleGalleryHandler}>
                    <Carousel views={prepareGalleryData(yachtService.images)} />
                  </Modal>
                </ModalGateway>
              )}

            </>
          )}
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  yachtService: state.boatServiceReducer.editYachtService,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  isReview: state.reviewReducer && state.reviewReducer.isReview
});

const mapDispatchToProps = dispatch => ({
  editYachtService: data => dispatch(editYachtService(data)),
  clearReviewFlag: () => dispatch(clearReviewFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatServiceInner);
