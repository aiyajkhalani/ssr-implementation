/* eslint-disable no-fallthrough */
/* eslint-disable default-case */
import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Layout,
  PaginationBar
} from "../../../components";
import { getRecommendedTrips, getRentBoatMostPopularTripAction, getRentBoatByTripAction, getBoatRentLookUps } from "../../../redux/actions";
import { pagination, showAll, lookupTypes } from "../../../util/enums/enums";
import UserContext from "../../../UserContext";
import BoatListingsWithMap from "../../../components/gridComponents/BoatListingsWithMap";

class RentTypeBoat extends Component {

  state = {
    showMap: false,
    type: "",
    action: null,
    isFlag: true,
    isAction: true,
  };

  static contextType = UserContext;
  static getDerivedStateFromProps(nextProps, prevState) {


    const {
      match, getBoatRentLookUps, lookUpSuccess, tripLists, getRentBoatByTripAction,
      country
    } = nextProps

    const { isFlag, type, isAction } = prevState
    const { params } = match && match



    if (isFlag && country) {
      getBoatRentLookUps(lookupTypes.TRIP_TYPE)
      return { isFlag: false };
    }

    if (type && lookUpSuccess && isAction) {
      switch (type) {
        case showAll.sharedTrip:
          getRentBoatByTripAction({
            page: pagination.PAGE_COUNT,
            limit: pagination.PAGE_RECORD_LIMIT,
            country,
            trip: tripLists && tripLists.length && tripLists[0].lookUp.id,
          });

          return {
            name: "Rent Shared Trip",
            isAction: false
          }

        case showAll.privateTrip:
          getRentBoatByTripAction({
            page: pagination.PAGE_COUNT,
            limit: pagination.PAGE_RECORD_LIMIT,
            country,
            trip: tripLists && tripLists.length && tripLists[1].lookUp.id,
          });

          return {
            name: "Rent Private Trip",
            isAction: false
          }

        case showAll.tripsPerHour:
          getRentBoatByTripAction({
            page: pagination.PAGE_COUNT,
            limit: pagination.PAGE_RECORD_LIMIT,
            country,
            trip: tripLists && tripLists.length && tripLists[2].lookUp.id,
          });

          return {
            name: "Rent Trips Per Hour",
            isAction: false
          }
      }
    }

    if (params && params.type) {
      return {
        type: params.type,
      }
    }

    return null
  }

  fetchValue = () => {
    const { type } = this.state
    const { tripLists, country } = this.props

    switch (type) {
      case showAll.sharedTrip:
        return {
          country,
          trip: tripLists && tripLists.length && tripLists[0].lookUp.id,
        }

      case showAll.privateTrip:
        return {
          country,
          trip: tripLists && tripLists.length && tripLists[1].lookUp.id,
        }

      case showAll.tripsPerHour:
        return {
          country,
          trip: tripLists && tripLists.length && tripLists[2].lookUp.id,
        }

      default:
        return
    }
  }

  fetchTotal = () => {
    const { type } = this.state
    const { privateTripsTotal, rentSharedTripsTotal, tripsPerHourTotal } = this.props
    if ((type)) {
      switch (type) {
        case showAll.privateTrip:
          return privateTripsTotal
        case showAll.sharedTrip:
          return rentSharedTripsTotal
        case showAll.tripsPerHour:
          return tripsPerHourTotal
        default:
          return
      }
    }
  }

  fetchData = () => {
    const { type } = this.state
    const {
      privateTrips,
      rentSharedTrips,
      recommendedTripsPerHour
    } = this.props
    switch (type) {
      case showAll.privateTrip:
        return privateTrips

      case showAll.sharedTrip:
        return rentSharedTrips

      case showAll.tripsPerHour:
        return recommendedTripsPerHour

      default:
        return
    }
  }

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {

    const { showMap, name } = this.state;

    const {
      getRentBoatByTripAction
    } = this.props
    const argument = this.fetchValue()

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            boatsTypeCount={this.fetchTotal()}
            boatsType={name}
            isRent
            showMap={showMap}
            isNotHome={false}
            toggleMapHandler={this.toggleMapHandler}
            boats={this.fetchData()}
          />

          {this.fetchTotal() > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination">
              <PaginationBar
                action={getRentBoatByTripAction}
                value={argument}
                totalRecords={this.fetchTotal()}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  recommendedTrips: state.rentReducer.recommendedTrips,
  totalRecommendedTrips: state.rentReducer.totalRecommendedTrips,
  privateTrips: state.rentReducer.privateTrips,
  rentMostPopularTrips: state.rentReducer && state.rentReducer.rentMostPopularTrips,
  rentMostPopularTripsTotal: state.rentReducer && state.rentReducer.rentMostPopularTripsTotal,
  rentSharedTrips: state.rentReducer && state.rentReducer.rentSharedTrips,
  recommendedTripsPerHour: state.rentReducer.recommendedTripsPerHour,
  tripsPerHourTotal: state.rentReducer.tripsPerHourTotal,
  privateTripsTotal: state.rentReducer.privateTripsTotal,
  rentSharedTripsTotal: state.rentReducer.rentSharedTripsTotal,
  tripLists:
    state.boatRentReducer &&
    state.boatRentReducer.boatRentLookUps &&
    state.boatRentReducer.boatRentLookUps.trip,
  lookUpSuccess: state.boatRentReducer && state.boatRentReducer.lookUpSuccess,
  country: state.loginReducer && state.loginReducer.currentLocation

});

const mapDispatchToProps = dispatch => ({
  getRecommendedTrips: data => dispatch(getRecommendedTrips(data)),
  getRentBoatMostPopularTripAction: data =>
    dispatch(getRentBoatMostPopularTripAction(data)),
  getRentBoatByTripAction: data => dispatch(getRentBoatByTripAction(data)),
  getBoatRentLookUps: data => dispatch(getBoatRentLookUps(data)),


});

export default connect(mapStateToProps, mapDispatchToProps)(RentTypeBoat);
