import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Layout, PaginationBar } from '../../../components'
import { getLatestBoats, getPopularBoats, getTopRatedBoats } from '../../../redux/actions'
import { pagination } from '../../../util/enums/enums';
import UserContext from "../../../UserContext";
import BoatListingsWithMap from '../../../components/gridComponents/BoatListingsWithMap';

class ShowAllBoats extends Component {

    state = {
        showMap: false,
        type: '',
        name: '',
        isFlag: true,
        isData: true,
    }

    static contextType = UserContext
    static getDerivedStateFromProps(nextProps, prevState) {

        const { isFlag, type, isData } = prevState
        const { match } = nextProps
        const { params } = match && match

        if (params && params.type && params.type && isFlag) {
            return {
                isFlag: false,
                type: params.type
            }
        }

        if (type && isData) {
            switch (type) {
                case 'recently-added-boats':
                    return { name: 'Our Market Boats', isData: false }
                case 'most-popular-boats':
                    return { name: 'Popular Boats', isData: false }
                case 'top-related-boats':
                    return { name: 'Top Rated Boats', isData: false }

                default:
                    break;
            }
        }

        return null
    }

    componentDidMount() {
        const { type } = this.state
        type && this.checkType(type)
    }

    checkType = (type) => {
        const { getLatestBoats, getPopularBoats, getTopRatedBoats } = this.props
        const { country } = this.context

        switch (type) {
            case 'recently-added-boats':
                getLatestBoats({ country, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
                break;

            case 'most-popular-boats':
                getPopularBoats({ country, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
                break;

            case 'top-related-boats':
                getTopRatedBoats({ country, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
                break;

            default:
                break;
        }
    }

    checkAction = () => {
        const { type } = this.state
        const { getLatestBoats, getPopularBoats, getTopRatedBoats } = this.props
        switch (type) {
            case 'recently-added-boats':
                return getLatestBoats
            case 'most-popular-boats':
                return getPopularBoats
            case 'top-related-boats':
                return getTopRatedBoats

            default:
                break;
        }
    }

    fetchTotalRecord = () => {
        const { type } = this.state
        const { totalLatestBoats, totalPopularBoats, totalTopRatedBoats } = this.props

        switch (type) {
            case 'recently-added-boats':
                return totalLatestBoats
            case 'most-popular-boats':
                return totalPopularBoats
            case 'top-related-boats':
                return totalTopRatedBoats

            default:
                break;
        }
    }

    fetchData = () => {
        const { type } = this.state
        const { latestBoats, popularBoats, topRatedBoats } = this.props

        switch (type) {
            case 'recently-added-boats':
                return latestBoats
            case 'most-popular-boats':
                return popularBoats
            case 'top-related-boats':
                return topRatedBoats

            default:
                break;
        }
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {

        const { showMap, name } = this.state
        const { country } = this.context
        return (
            <Layout isFooter>

                <div className={`${showMap ? 'is-show-map' : ''} w-100`}>
                    <BoatListingsWithMap
                        boatsTypeCount={this.fetchTotalRecord()}
                        boatsType={name}
                        showMap={showMap}
                        toggleMapHandler={this.toggleMapHandler}
                        boats={this.fetchData()}
                    />

                    {this.fetchTotalRecord() > pagination.PAGE_RECORD_LIMIT &&
                        <div className="boat-pagination">
                            <PaginationBar
                                action={this.checkAction()}
                                value={{ country }}
                                totalRecords={this.fetchTotalRecord()}
                            />
                        </div>
                    }
                </div>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    latestBoats: state.boatReducer.latestBoats,
    popularBoats: state.boatReducer.popularBoats,
    topRatedBoats: state.boatReducer.topRatedBoats,
    totalLatestBoats: state.boatReducer.totalLatestBoats,
    totalPopularBoats: state.boatReducer.totalPopularBoats,
    totalTopRatedBoats: state.boatReducer.totalTopRatedBoats,
})

const mapDispatchToProps = dispatch => ({
    getLatestBoats: data => dispatch(getLatestBoats(data)),
    getPopularBoats: data => dispatch(getPopularBoats(data)),
    getTopRatedBoats: data => dispatch(getTopRatedBoats(data)),

})

export default connect(mapStateToProps, mapDispatchToProps)(ShowAllBoats) 