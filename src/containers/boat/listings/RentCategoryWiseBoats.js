import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, PaginationBar } from '../../../components'
import { rentCategoryWiseBoats } from '../../../redux/actions'
import { pagination } from '../../../util/enums/enums';
import BoatListingsWithMap from '../../../components/gridComponents/BoatListingsWithMap';


class RentCategoryWiseBoats extends Component {

    state = {
        country: '',
        tripTypeId: '',
        showMap: false
    }


    static getDerivedStateFromProps(nextProps) {

        const { match } = nextProps
        const { params } = match && match

        if (params && params.hasOwnProperty('country') && params.country && params.hasOwnProperty('tripTypeId') && params.tripTypeId) {
            return {
                country: params.country,
                tripTypeId: params.tripTypeId
            }
        }

        return null
    }

    componentDidMount() {
        const { country, tripTypeId } = this.state
        const { rentCategoryWiseBoats } = this.props
        rentCategoryWiseBoats({ country, tripTypeId, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {

        const { categoryWiseBoat, totalCategoryWiseBoats } = this.props
        const { country, tripTypeId, showMap } = this.state

        return (
            <Layout isFooter>
                <div className={`${showMap ? 'is-show-map' : ''} w-100`}>

                    <BoatListingsWithMap isRent boatsTypeCount={totalCategoryWiseBoats} boatsType="Rent Boats" showMap={showMap} toggleMapHandler={this.toggleMapHandler} boats={categoryWiseBoat} />

                    {totalCategoryWiseBoats > pagination.PAGE_RECORD_LIMIT &&
                        <div className="boat-pagination"> 
                        <PaginationBar
                            action={rentCategoryWiseBoats}
                            value={{ country, tripTypeId }}
                            totalRecords={totalCategoryWiseBoats}
                        />
                        </div>
                    }

                </div>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    categoryWiseBoat: state.boatRentReducer.rentCategoryWiseBoat,
    totalCategoryWiseBoats: state.boatRentReducer.totalCategoryWiseBoats,
})

const mapDispatchToProps = dispatch => ({
    rentCategoryWiseBoats: data => dispatch(rentCategoryWiseBoats(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(RentCategoryWiseBoats) 