import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Layout,
  PaginationBar
} from "../../../components";
import { getRecommendedTrips, getRentBoatMostPopularTripAction, getRentBoatByTripAction, getBoatRentLookUps } from "../../../redux/actions";
import { pagination, showAll } from "../../../util/enums/enums";
import UserContext from "../../../UserContext";
import BoatListingsWithMap from "../../../components/gridComponents/BoatListingsWithMap";

class Recommended extends Component {

  state = {
    showMap: false,
    type: "",
    isAction: true,
    name: ""
  };

  static contextType = UserContext;
  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      match,
      getRentBoatMostPopularTripAction,
      getRecommendedTrips,
      country
    } = nextProps

    const { isAction } = prevState
    const { params } = match && match

    if (params && params.type === showAll.recommended && isAction) {
      getRecommendedTrips({ country, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
      return {
        isAction: false,
        action: getRecommendedTrips,
        name: 'Rent Recommended'
      }
    }

    if (params && params.type === showAll.mostPopular && isAction) {
      getRentBoatMostPopularTripAction({ country, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
      return {
        isAction: false,
        action: getRentBoatMostPopularTripAction,
        name: 'Rent Most Popular'
      }
    }

    if (params && params.type) {
      return {
        type: params.type,
      }
    }
    return null
  }

  fetchData = () => {
    const { type } = this.state

    const {
      recommendedTrips,
      rentMostPopularTrips
    } = this.props

    switch (type) {
      case showAll.recommended:
        return recommendedTrips

      case showAll.mostPopular:
        return rentMostPopularTrips

      default:
        return
    }
  }

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {

    const { showMap, name, action } = this.state;

    const {
      totalRecommendedTrips,
      rentMostPopularTripsTotal, } = this.props;
    const { country } = this.context;
    const totalRecords = totalRecommendedTrips ? totalRecommendedTrips : rentMostPopularTripsTotal
    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            boatsTypeCount={totalRecords}
            boatsType={name}
            isRent
            showMap={showMap}
            isNotHome={false}
            toggleMapHandler={this.toggleMapHandler}
            boats={this.fetchData()}
          />

          {totalRecords > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination">
              <PaginationBar
                action={action}
                value={{ country }}
                totalRecords={totalRecords}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  recommendedTrips: state.rentReducer.recommendedTrips,
  totalRecommendedTrips: state.rentReducer.totalRecommendedTrips,

  rentMostPopularTrips: state.rentReducer && state.rentReducer.rentMostPopularTrips,
  rentMostPopularTripsTotal: state.rentReducer && state.rentReducer.rentMostPopularTripsTotal,

  tripLists:
    state.boatRentReducer &&
    state.boatRentReducer.boatRentLookUps &&
    state.boatRentReducer.boatRentLookUps.trip,
  lookUpSuccess: state.boatRentReducer && state.boatRentReducer.lookUpSuccess,
  country: state.loginReducer && state.loginReducer.currentLocation

});

const mapDispatchToProps = dispatch => ({
  getRecommendedTrips: data => dispatch(getRecommendedTrips(data)),
  getRentBoatMostPopularTripAction: data =>
    dispatch(getRentBoatMostPopularTripAction(data)),
  getRentBoatByTripAction: data => dispatch(getRentBoatByTripAction(data)),
  getBoatRentLookUps: data => dispatch(getBoatRentLookUps(data)),


});

export default connect(mapStateToProps, mapDispatchToProps)(Recommended);
