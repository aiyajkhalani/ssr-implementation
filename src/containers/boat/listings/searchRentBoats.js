import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Layout,
  PaginationBar
} from "../../../components";
import { searchBoatRent, clearSearchBoatRentFlag } from "../../../redux/actions";
import UserContext from "../../../UserContext";
import BoatListingsWithMap from "../../../components/gridComponents/BoatListingsWithMap";
import { pagination } from "../../../util/enums/enums";

class SearchRentBoats extends Component {
  state = {
    showMap: false,
    isFlag: true
  };

  static contextType = UserContext;

  static getDerivedStateFromProps(nextProps, prevState) {
    const { history: { location }, searchBoatRent, clearSearchBoatRentFlag, isBoatRentSearched } = nextProps

    if (isBoatRentSearched) {
      clearSearchBoatRentFlag()
    }

    if (location && location.state && prevState.isFlag) {
      searchBoatRent(location.state)
      return { isFlag: false }
    }

    return null
  }

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {

    const { showMap } = this.state;
    const { searchedBoatRent, totalSearchedBoatRent } = this.props;

    const { country: countryName } = this.context;

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            isRent
            showMap={showMap}
            toggleMapHandler={this.toggleMapHandler}
            boats={searchedBoatRent}
            boatsType="Searched Rent Boats"
            boatsTypeCount={totalSearchedBoatRent}
          />

          {totalSearchedBoatRent > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination">
              <PaginationBar
                action={searchBoatRent}
                value={{ countryName }}
                totalRecords={totalSearchedBoatRent}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  searchedBoatRent: state.rentReducer.searchedBoatRent,
  totalSearchedBoatRent: state.rentReducer.totalSearchedBoatRent,
  isBoatRentSearched: state.rentReducer && state.rentReducer.isBoatRentSearched,
});

const mapDispatchToProps = dispatch => ({
  searchBoatRent: data => dispatch(searchBoatRent(data)),
  clearSearchBoatRentFlag: () => dispatch(clearSearchBoatRentFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchRentBoats);
