import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Layout,  PaginationBar } from '../../../components'
import { categoryWiseBoats } from '../../../redux/actions'
import { pagination } from '../../../util/enums/enums';
import BoatListingsWithMap from '../../../components/gridComponents/BoatListingsWithMap';


class CategoryWiseBoats extends Component {

    state = {
        country: '',
        categoryId: '',
        showMap: false,
        name:''
    }


    static getDerivedStateFromProps(nextProps) {

        const { match } = nextProps
        const { params } = match && match

        if (params && params.hasOwnProperty('country') && params.country && params.hasOwnProperty('categoryId') && params.categoryId) {
            return {
                country: params.country,
                categoryId: params.categoryId
            }
        }
        
        return null
    }

    componentDidMount() {
        const { country, categoryId } = this.state
        const { categoryWiseBoats } = this.props
        categoryWiseBoats({ country, categoryId, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {

        const { categoryWiseBoat, totalCategoryWiseBoats, categoryWiseBoats } = this.props
        const { country, categoryId, showMap } = this.state

        return (
            <Layout isFooter>
                <div className={`${showMap ? 'is-show-map' : ''} w-100`}>

                    <BoatListingsWithMap 
                            boatsTypeCount={totalCategoryWiseBoats} 
                            boatsType={categoryWiseBoat && categoryWiseBoat.length > 0 && categoryWiseBoat[0].boatType.name}
                            showMap={showMap} 
                            toggleMapHandler={this.toggleMapHandler}
                            boats={categoryWiseBoat} 
                    />

                    {totalCategoryWiseBoats > pagination.PAGE_RECORD_LIMIT &&
                        <div className="boat-pagination">
                        <PaginationBar
                            action={categoryWiseBoats}
                            value={{ country, categoryId }}
                            totalRecords={totalCategoryWiseBoats}
                        />
                        </div>
                    }

                </div>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    categoryWiseBoat: state.boatReducer.categoryWiseBoat,
    totalCategoryWiseBoats: state.boatReducer.totalCategoryWiseBoats,
})

const mapDispatchToProps = dispatch => ({
    categoryWiseBoats: data => dispatch(categoryWiseBoats(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryWiseBoats) 