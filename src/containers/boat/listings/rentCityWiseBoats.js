import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Layout, PaginationBar } from '../../../components'
import { getRentCityWiseBoats } from '../../../redux/actions'
import { pagination } from '../../../util/enums/enums';
import BoatListingsWithMap from '../../../components/gridComponents/BoatListingsWithMap';
import { cityCountryNameFormatter } from '../../../helpers/jsxHelper';


class RentCityWiseBoats extends Component {

    state = {
        country: '',
        city: '',
        showMap: false
    }


    static getDerivedStateFromProps(nextProps) {

        const { match } = nextProps
        const { params } = match && match

        if (params && params.hasOwnProperty('country') && params.country && params.hasOwnProperty('city') && params.city) {
            return {
                country: params.country,
                city: params.city
            }
        }

        return null
    }

    componentDidMount() {
        const { country, city } = this.state
        const { getRentCityWiseBoats } = this.props

        getRentCityWiseBoats({ country, city, page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT })
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {

        const { cityWiseBoats, getRentCityWiseBoats, totalCityWiseBoats } = this.props
        const { country, city, showMap } = this.state

        return (
            <Layout isFooter>
                <div className={`${showMap ? 'is-show-map' : ''} w-100`}>
                    {totalCityWiseBoats > 0 &&
                        <BoatListingsWithMap
                         isRent
                         boatsType={`${cityCountryNameFormatter(city, country)} Rent Boats`} boatsTypeCount={totalCityWiseBoats}
                         showMap={showMap} isRent toggleMapHandler={this.toggleMapHandler} boats={cityWiseBoats} />
                    }
                    {totalCityWiseBoats > pagination.PAGE_RECORD_LIMIT &&
                    <div className="boat-pagination">
                    <PaginationBar
                        action={getRentCityWiseBoats}
                        value={{ country, city }}
                        totalRecords={totalCityWiseBoats}
                    />
                    </div>
                    }
                </div>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    cityWiseBoats: state.rentReducer.cityWiseBoats,
    totalCityWiseBoats: state.rentReducer.totalCityWiseBoats,
})

const mapDispatchToProps = dispatch => ({
    getRentCityWiseBoats: data => dispatch(getRentCityWiseBoats(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(RentCityWiseBoats) 