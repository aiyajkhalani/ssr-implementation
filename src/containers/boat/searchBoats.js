import React, { Component } from 'react';
import { connect } from 'react-redux'

import { Layout } from '../../components';
import { clearSearchedResults } from '../../redux/actions';
import { resultMessage } from '../../util/enums/enums';
import BoatListingsWithMap from '../../components/gridComponents/BoatListingsWithMap';


class SearchBoats extends Component {

    state = {
        showMap: false
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    componentWillUnmount() {
        const { clearSearchedResults } = this.props
        clearSearchedResults()
    }

    render() {

        const { searchedBoats, searchedBoatsCount } = this.props
        const { showMap } = this.state

        return (
            <Layout isFooter>
                {searchedBoats &&
                    <BoatListingsWithMap
                        boatsType="Searched Boats"
                        boatsTypeCount={searchedBoatsCount}
                        showMap={showMap}
                        toggleMapHandler={this.toggleMapHandler}
                        boats={searchedBoats}
                        message={resultMessage.search}
                    />
                }
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    searchedBoats: state.boatReducer.searchedBoats,
    searchedBoatsCount: state.boatReducer.searchedBoatsCount,
})

const mapDispatchToProps = dispatch => ({
    clearSearchedResults: () => dispatch(clearSearchedResults())
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchBoats) 