import React, { Component, Fragment } from "react";
import uuid from "uuid/v4";
import { Formik, ErrorMessage } from "formik";
import { connect } from "react-redux";
import ImageUploader from "react-images-upload";
import * as Yup from "yup";
import Datetime from "react-datetime";
import Input from '@material-ui/core/Input';
import { Container, Card, Row, Col, Form } from "react-bootstrap";
import {
  Button,
  RadioGroup,
  Radio,
  FormControl,
  FormGroup,
  FormControlLabel,
  FormLabel,
  Chip,
  Select,
  MenuItem,
  Box,
} from "@material-ui/core";
import "react-dates/initialize";
import { DateRangePicker, SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

import { DashboardLayout } from '../../components/layout/dashboardLayout';
import GoogleMap from "../../components/map/map";
import { Field } from "../../components";
import {
  createBoatRent,
  clearBoatRentFlag,
  getBoatRentLookUps,
  getAllBoatRentOfUser,
  getAllRentTypes,
  updateBoatRent
} from "../../redux/actions/boatRentAction";
import { uploadToS3 } from "../../helpers/s3FileUpload";
import { renderOptions, dateStringFormate } from "../../util/utilFunctions";
import { randomAdId, renderSelectOptions, getIds } from "../../helpers/string";
import { cancelHandler } from "../../helpers/routeHelper";
import { renderMenuItems } from "../../helpers/jsxHelper";
import { timePeriod, getListOfYears, latLng } from "../../util/enums/enums";
import MultiSelect from "../../components/helper/multiSelect";

import "../../styles/common.scss";
import "./CreateRentBoat.scss";
import { SuccessNotify } from "../../helpers/notification";
import moment from "moment";
import ErrorComponent from "../../components/error/errorComponent";


class CreateRentBoat extends Component {
  constructor(props) {
    super(props);
    
    
    this.state = {
      isRentPerHourTrip:false,
      isEdit: props.location.state && props.location.state.isEdit ? props.location.state.isEdit : false,
      isHours:false,
      editRentBoat:  props.location.state && props.location.state.editRent ? props.location.state.editRent : {},
      adId: "",
      tripInformation: {
        boatName: "",
        captainName: "",
        trip: props.boatRentLookUps && props.boatRentLookUps.trip && props.boatRentLookUps.trip.length && props.boatRentLookUps.trip[0].lookUp.id,
        tripType: [],
        tripDuration: 1,
        durationUnit: "",
        startTime: "",
        endTime: "",
        startDate: "",
        endDate: "",
        noOfUnit: 1,
        address: "",
        tripAddress: ""
      },
      tripTypeName: [],
      boatInformation: {
        boatLength: "",
        bedroomAndCabins: "",
        maximumGuest: "",
        bathrooms: 1,
        seatsOnBoat: "",
        model: "",
        manufacturer: "",
        yearOfManufacturer: "",
        engine: ""
      },
      deckEntertainment: {
        deckAndEntertainment: "",
        whatToBring: "",
        aboutUs: "",
        price: "",
        descriptionOfOurTrip: "",
        policy: "",
        youtubeLinkVideo: "",
        deposit: "",
        boatLayout: [],
        boatLength: "",
        images: []
      },
      openRangePicker: false,
      location: props.location.state && props.location.state.isEdit && props.location.state.editRent ? {latitude:props.location.state.editRent.location.latitude,longitude:props.location.state.editRent.location.longitude} : {},
      destinationLocation: {},
    };
  }

  componentDidMount() {
    const {
      getBoatRentLookUps,
      clearBoatRentFlag,
      getAllRentTypes
    } = this.props;

    getBoatRentLookUps();
    clearBoatRentFlag();
    getAllRentTypes({ isAdmin: true})
  }

  static getDerivedStateFromProps(props, state) {
    const { history, createSuccess, clearBoatRentFlag, updatedSuccess } = props;
    const { adId } = state;
    if (updatedSuccess) {
      setTimeout(() => SuccessNotify("Rent Boat is updated successfully"), 100);
      history.push("/manage-boat-rents");
    }
    if (createSuccess) {
      clearBoatRentFlag();
      setTimeout(() => SuccessNotify("Rent Boat is added successfully"), 100);
      history.push("/manage-boat-rents");
    } else if (!adId) {
      return {
        adId: randomAdId("RB")
      };
    }
    return null;
  }
  common = {
    address: Yup.string().required("Address field is required."),
    tripAddress: Yup.string().required("Address field is required."),
    aboutUs: Yup.string().required("About Us field is required."),
    boatName: Yup.string().required("Boat Name field is required."),
    descriptionOfOurTrip: Yup.string().required("Description Of Our Trip field is required."),
    endTime: Yup.string().required("End Time field is required."),
    engine: Yup.string().required("Engine field is required."),
    images: Yup.array().min(5, "Minimum 5 trip picture upload required."),
    model: Yup.string().required("Model field is required."),
    manufacturer: Yup.string().required("Manufacturer field is required."),
    price: Yup.number().required("Price field is required."),
    policy: Yup.string().required("Policy field is required."),
    startDate: Yup.string().required("Start Date field is required."),
    startTime: Yup.string().required("Start Time field is required."),
    trip: Yup.string().required("Trip field is required."),
    tripType: Yup.string().required("Trip Type field is required."),
    yearOfManufacturer: Yup.string().required("Year of manufacturer field is required."),
  }
  rentPerHourSchema = Yup.object().shape({
    ...this.common,
    noOfUnit: Yup.number().required("NoOfUnit field is required."),
  })

  privateAndSharedCommon = {
    ...this.common,
      boatLength: Yup.string().required("Boat Length field is required."),
      bedroomAndCabins: Yup.string().required("Bedroom & Cabins field is required."),
      deckAndEntertainment: Yup.string().required("Decks & Entertainment field is required."),
      durationUnit: Yup.string().required("Duration unit field is required."),
      maximumGuest: Yup.string().required("Maximum Guest field is required."),
      seatsOnBoat: Yup.string().required("Seats on Boat field is required."),
      tripDuration: Yup.number().required("Trip Duration field is required."),
      whatToBring: Yup.string().required("What to Bring field is required."),
  }
   privateAndShared =  Yup.object().shape({
     ...this.privateAndSharedCommon,
    endDate: Yup.string().required("End date field is required."),
   })
   privateAndSharedWithHour =  Yup.object().shape({
     ...this.privateAndSharedCommon,
   })

  handleFileUpload = async (file, name, value, setValue) => {
    const { deckEntertainment } = this.state;
    let oldValues = deckEntertainment[name]
    let formValue = value || []

    if (file && file.length) {
      let res = await uploadToS3(file[file.length - 1]);

      if (res && res.location) {
        const newValues = oldValues.concat(res.location)
        setValue(name, formValue.concat(res.location))
        deckEntertainment[name] = newValues
        this.setState({ deckEntertainment });
      }
    }
  };
  getTypeWiseValues = (values) => {
    const { 
      captainName,
      durationUnit,
      boatLength,
      endDate,
      bedroomAndCabins,
      maximumGuest,
      bathrooms,
      seatsOnBoat,
      deckAndEntertainment,
      whatToBring,
      ...newData} = values
    return newData
  }
  handleFileDelete = async (index, name, setValue,values) => {
    const oldValues = values[name];
    if (oldValues && oldValues.length) {
      values[name].splice(index, 1)
      setValue(name, values[name]);
    }
  };

  renderRadioItems = items => {
    return items.map((item, index) => {
      return (
        <FormControlLabel
          key={uuid()}
          label={item.lookUp.alias}
          control={<Radio name={item.lookUp.alias} value={item.lookUp.id} />}
        />
      );
    });
  };

  fetchMapInfo = (result, setValue) => {
    const { location } = this.state;
    location.location = result.place_name;
    location.country = result.country;
    location.state = result.state;
    location.postalCode = result.postalCode;
    location.city = result.city;
    location.address = result.address;
    location.latitude = result.latitude;
    location.longitude = result.longitude;
    location.route = result.route;

    setValue('address', result.address)
  };
  fetchMapInfo2 = (result, setValue) => {
    const { destinationLocation } = this.state;
    destinationLocation.location = result.place_name;
    destinationLocation.country = result.country;
    destinationLocation.state = result.state;
    destinationLocation.postalCode = result.postalCode;
    destinationLocation.city = result.city;
    destinationLocation.address = result.address;
    destinationLocation.latitude = result.latitude;
    destinationLocation.longitude = result.longitude;
    destinationLocation.route = result.route;

    setValue('tripAddress', result.address)
  };

  renderOptions = options => {
    return options.map(item => {
      return <option key={uuid()}>{item.lookUp.alias}</option>;
    });
  };

  menuItems(values) {
    if (values) {
      return values.map(name => (
        <MenuItem value={name.lookUp.id} key={uuid()}>
          {name.lookUp.alias}
        </MenuItem>
      ));
    }
  }
   

  getYearListing = () => {
    const listOfYears = getListOfYears();

    return listOfYears &&
      listOfYears.map(year => {
        return {
          id: year,
          value: year
        }
      })
  };
  editData = (values) =>{
     const { 
              location,
              tripLocation,
              latLng,
              tripLatLng,
              rating,
              user,
              __typename,
            ...newValues
          } = values
     return newValues
  }
 
  render() {
    const {
      tripInformation,
      boatInformation,
      deckEntertainment,
      tripTypeName,
      adId,
      startDate,
      endDate,
      date,
      focused,
      isRentPerHourTrip,
      isHours,
      isEdit,
      editRentBoat,
      focusedInput,
      
    } = this.state;
    const { createBoatRent, boatRentLookUps, history, updateBoatRent, validationError, errorMessage } = this.props;

    let { boatRentTypes} = this.props
    

    boatRentTypes = boatRentTypes && boatRentTypes.length > 0 && boatRentTypes.map(item => {
      return { label: item.name, value: item.id, tripIds: item.tripId.map(trip => trip.id) }
    })

    console.log(boatRentTypes, "boatRentTypes")

    const decksAndEntertainment = boatRentLookUps && renderSelectOptions(boatRentLookUps.decksAndEnvironment,"alias", "id", true)

    return (
      
      <Fragment>
     
        <DashboardLayout>
          <Container fluid={true}>
           {/* 
              editRentBoat Data Prepare from Manage Boat Rents 
           */}
            <Formik
              initialValues={ isEdit ? editRentBoat 
              :
               { 
                 ...tripInformation,
                 ...boatInformation,
                 ...deckEntertainment
                }
              }
              onSubmit={(values, state) => {
                
                const { location, destinationLocation } = this.state;

                values.address = location.address;
                values.city = location.city;
                values.country = location.country;
                
                values.geometricLocation = {
                  coordinates: [location.longitude, location.latitude]
                }
                values.placeName = location.location;
                values.postalCode = location.postalCode;
                values.route = location.route;
                values.state = location.state;
                values.tripAddress = destinationLocation.address;
                values.tripCity = destinationLocation.city;
                values.tripCountry = destinationLocation.country;
                values.tripLatitude = destinationLocation.latitude;
                values.tripLongitude = destinationLocation.longitude;
                values.tripPlaceName = destinationLocation.location;
                values.tripPostalCode = destinationLocation.postalCode;
                values.tripRoute = destinationLocation.route;
                values.tripState = destinationLocation.state;
                values.adId = adId;

                values.endDate = values.durationUnit === "Hours" ? values.startDate : values.endDate
                
                const newValue = isRentPerHourTrip ? this.getTypeWiseValues(values) : values
                values.tripDuration = isRentPerHourTrip ? 1 : values.tripDuration


                const input = {...newValue, deckAndEntertainment: getIds(newValue.deckAndEntertainment)}
                
                isEdit ? updateBoatRent(this.editData(input)) : createBoatRent(input)
              }}

              validationSchema={(isRentPerHourTrip ? this.rentPerHourSchema : ((isHours) ? this.privateAndSharedWithHour: this.privateAndShared)) }
            
              render={({ values, setFieldValue, handleSubmit, errors }) => (
                <Form>
                <div className="pb-3">
                    <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                      <Box
                        fontSize={20}
                        letterSpacing={1}
                        fontWeight={600}
                        className="map-title"
                      >{`Add Rent Boat`} <span className="font-weight-400 font-14">Ad ID: {`${adId}`}</span></Box>
                    </div>
                    <Row className="addRentBoat-field">
                      <Col>
                        <Row>
                          <Col>
                            <GoogleMap
                              className="map-rent"
                              fetch={(result) => this.fetchMapInfo(result, setFieldValue)}
                              height={35}
                              width={100}
                              latLng={values.latLng || latLng}
                              isUpdate={isEdit}
                              value={values.location}
                              id="destination-input"
                              title={""}
                              placeHolder={"Boat Location"}
                              columnName={"address"}
                              isError={errors.address}
                            ></GoogleMap>
                          </Col>
                        </Row>
                        <Row className="mt-2">
                          <Col>
                            <GoogleMap
                              className="map-rent"
                              fetch={(result) => this.fetchMapInfo2(result, setFieldValue)}
                              id="pac-input"
                              height={35}
                              latLng={values.tripLatLng || latLng}
                              value={values.tripLocation}
                              isUpdate={isEdit}
                              width={100}
                              title={""}
                              placeHolder={"Trip Destination"}
                              columnName={"tripAddress"}
                              isError={errors.tripAddress}
                            ></GoogleMap>
                          </Col>
                        </Row>
                      </Col>
                      <Col className="pl-0">
                        <Card className="p-10 mt-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 
                        border-bottom-left-radius-2">
                          <Card.Title className="ml-0">
                            Trip Information
                          </Card.Title>
                          <Row>
                            <Col>
                              <div>
                                <div className="trip-kind">
                                  <FormControl component="fieldset">
                                    <FormLabel
                                      component="legend"
                                      className="mb-0 addMarinaStorage-info required"
                                    >
                                      Trip 
                                    </FormLabel>
                                   
                                    <RadioGroup
                                      row
                                      aria-label="gender"
                                      className="radioButton"
                                      defaultChecked={boatRentLookUps && boatRentLookUps.trip && boatRentLookUps.trip.length && boatRentLookUps.trip[0].lookUp.alias}
                                      value={values.trip}
                                      onChange={e => {
                                        setFieldValue("trip", e.target.value)
                                        this.setState({isRentPerHourTrip: e.target.name === "Rent Per Hour"})
                                      }}
                                    >
                                      {boatRentLookUps &&
                                        boatRentLookUps.trip &&
                                        this.renderRadioItems(
                                          boatRentLookUps.trip
                                        )}
                                    </RadioGroup>
                                  </FormControl>
                                </div>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="trip"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <div className="mb-2">
                                <span className="form-label required">Trip Type</span>
                                <FormControl className="select-container minWidth-40 ml-2">
                                {boatRentTypes && boatRentTypes.length > 0 && (
                                      <Select
                                      MenuProps={{
                                        anchorOrigin: {
                                          vertical: "bottom",
                                          horizontal: "left"
                                        },
                                        transformOrigin: {
                                          vertical: "top",
                                          horizontal: "left"
                                        },
                                        getContentAnchorEl: null
                                      }}
                                          fullWidth
                                          name="tripType"
                                          value={values.tripType}
                                          onChange={e =>
                                            setFieldValue(
                                              "tripType",
                                              e.target.value
                                            )
                                          }
                                        >
                                        {boatRentTypes.filter(item => item.tripIds.includes(values.trip)).map(item => {
                                          
                                          return (
                                            <MenuItem value={item.value}>{item.label}</MenuItem>
                                          );
                                        })}
                                        </Select>
                                      )}
                                  </FormControl>
                                
                                <ErrorMessage
                                  component="div"
                                  name="tripType"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                            {isRentPerHourTrip && <Col>
                              <div className="mb-2">
                                <label className="form-label required">
                                  Units Available For Rent
                                </label>
                                <Form.Control
                                  as="select"
                                  value={values.noOfUnit}
                                  onChange={e =>
                                    setFieldValue("noOfUnit", +e.target.value)
                                  }
                                >
                                  {renderOptions(50)}
                                </Form.Control>
                                
                                <ErrorMessage
                                  component="div"
                                  name="noOfUnit"
                                  className="error-message"
                                />
                              </div>
                            </Col>}
                          </Row>
                          <Row>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="Boat Name"
                                  id={"boatName"}
                                  name={"boatName"}
                                  value={values.boatName}
                                  required
                                  type="text"
                                  onChangeText={e => {
                                    setFieldValue("boatName", e.target.value);
                                  }}
                                />
                                 
                                <ErrorMessage
                                  component="div"
                                  name="boatName"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              {!isRentPerHourTrip && (
                                <div className="mb-2">
                                  <Field
                                    label="Captain Name"
                                    id={"captainName"}
                                    name={"captainName"}
                                    value={values.captainName}
                                    type="text"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "captainName",
                                        e.target.value
                                      );
                                    }}
                                  ></Field>
                                   
                                  <ErrorMessage
                                    component="div"
                                    name="captainName"
                                    className="error-message"
                                  />
                                </div>
                              )}
                              <div className="mb-2">
                                <div className="boatShow-field">
                                  <Field
                                    label="Price"
                                    id={"price"}
                                    name={"price"}
                                    value={values.price}
                                    type="number"
                                    required
                                    onChangeText={e => {
                                      setFieldValue("price", +e.target.value);
                                    }}
                                  ></Field>
                                   
                                  <ErrorMessage
                                    component="div"
                                    name="price"
                                    className="error-message"
                                  />
                                </div>
                              </div>
                            </Col>
                            <Col>
                              {!isRentPerHourTrip && (
                                <Row>
                                  <Col>
                                    <div className="mb-2">
                                      <span className="form-label required">
                                        Trip Duration
                                      </span>
                                      <Form.Control
                                        as="select"
                                        value={values.tripDuration}
                                        onChange={e => {
                                          setFieldValue(
                                            "tripDuration",
                                            +e.target.value
                                          );
                                        }}
                                      >
                                        {renderOptions(31)}
                                      </Form.Control>
                                       
                                      <ErrorMessage
                                        component="div"
                                        name="tripDuration"
                                        className="error-message"
                                      />
                                    </div>
                                  </Col>
                                  <Col>
                                    <div className="mb-2">
                                      <span className="form-label required">
                                        Duration Unit
                                      </span>
                                      <select
                                        class="custom-select custom-select-sm"
                                        onChange={e => {
                                         const isHours = (e.target.value === "Hours")
                                         this.setState({isHours})
                                          setFieldValue(
                                            "durationUnit",
                                            e.target.value
                                          );
                                        }}
                                        value={values.durationUnit}
                                      >
                                        <option selected value="">
                                          Select time
                                        </option>
                                        {timePeriod.map(item => {
                                          return (
                                            <option key={uuid()} value={item}>{item}</option>
                                          );
                                        })}
                                      </select>
                                       
                                      <ErrorMessage
                                        component="div"
                                        name="durationUnit"
                                        className="error-message"
                                      />
                                    </div>
                                  </Col>
                                </Row>
                              )}
                              <Row>
                                <Col>
                                  <div className="mb-2">
                                    <div className="boatShow-field">
                                      <label className="required">Start Time</label>
                                      <Datetime
                                        dateFormat={false}
                                        value={values.startTime ? moment(values.startTime).format('hh:mm A'): null}
                                        onChange={value => {
                                          setFieldValue(
                                            "startTime",
                                            dateStringFormate(value)
                                          );
                                        }}
                                      />
                                    </div>
                                     
                                    <ErrorMessage
                                      component="div"
                                      name="startTime"
                                      className="error-message"
                                    />
                                  </div>
                                </Col>
                                <Col>
                                  <div className="mb-2">
                                    <div className="boatShow-field ">
                                      <label className="required">End Time</label>
                                      <Datetime
                                        dateFormat={false}
                                        value={values.endTime ? moment(values.endTime).format('hh:mm A') : null}
                                        onChange={value => {
                                          setFieldValue(
                                            "endTime",
                                            dateStringFormate(value)
                                          );
                                        }}
                                      />
                                    </div>
                                     
                                    <ErrorMessage
                                      component="div"
                                      name="endTime"
                                      className="error-message"
                                    />
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col>
                                  {(values.durationUnit &&
                                    values.durationUnit === "Hours") ||
                                    isRentPerHourTrip ? (
                                      <>
                                        <label className="required">Select Date</label>
                                        <div>
                                          <SingleDatePicker
                                            date={date}
                                            onDateChange={date => {
                                              this.setState({ date })
                                              date && setFieldValue("startDate", date.toDate().toISOString())
                                            }}
                                            focused={focused}
                                            onFocusChange={({ focused }) =>
                                              this.setState({ focused })
                                            }
                                            id="startDate"
                                          />
                                        </div>
                                        <ErrorMessage
                                          component="div"
                                          name="startDate"
                                          className="error-message mr-2"
                                        />
                                      </>
                                    ) : (
                                      <> </>
                                    )}
                                  {(values.durationUnit &&
                                    values.durationUnit === "Days") && !isRentPerHourTrip
                                    ? (
                                      <> 
                                      
                                        <label className="required">Select Start & End Dates</label>
                                        <div>
                                          <DateRangePicker
                                            startDate={values.endDate ? moment(values.startDate) : null}
                                            startDateId="startDate"
                                            endDate={values.endDate ? moment(values.endDate) : null }
                                            endDateId="endDate"
                                            onDatesChange={({
                                              startDate,
                                              endDate
                                            }) => {
                                              startDate && setFieldValue("startDate", startDate.toDate().toISOString())
                                              endDate && setFieldValue("endDate", endDate.toDate().toISOString())
                                            }}
                                            focusedInput={focusedInput}
                                            onFocusChange={(focusedInput) => {
                                              this.setState({ focusedInput })
                                            }}
                                          />
                                        </div>
                                        <div className="d-flex">
                                          <ErrorMessage
                                            component="div"
                                            name="startDate"
                                            className="error-message mr-2"
                                          />
                                         {!isRentPerHourTrip && values.durationUnit === "Days" &&
                                          <ErrorMessage
                                            component="div"
                                            name="endDate"
                                            className="error-message"
                                          />
                                         }
                                        </div>
                                      </>
                                    ) : (
                                      <> </>
                                    )}
                                    
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </Card>

                        <Card className="p-10">
                          <Card.Title className="ml-0">
                            Other Info
                            </Card.Title>
                          <Row>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="Model"
                                  id="model"
                                  name="model"
                                  required
                                  type="text"
                                  value={values.model}
                                  onChangeText={e => setFieldValue("model", e.target.value)}
                                ></Field>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="model"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="Manufacturer"
                                  id="manufacturer"
                                  name="manufacturer"
                                  required
                                  type="text"
                                  value={values.manufacturer}
                                  onChangeText={e => setFieldValue("manufacturer", e.target.value)}
                                ></Field>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="manufacturer"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="Year Of Manufacturer"
                                  id="yearOfManufacturer"
                                  name="yearOfManufacturer"
                                  type="select"
                                  required
                                  value={values.yearOfManufacturer}
                                  onChangeText={e => setFieldValue("yearOfManufacturer", e.target.value)}
                                  options={this.getYearListing()}
                                  placeholder='Select Year Of Manufacturer'
                                />
                                 
                                <ErrorMessage
                                  component="div"
                                  name="yearOfManufacturer"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="Engine"
                                  id="engine"
                                  name="engine"
                                  required
                                  type="text"
                                  value={values.engine}
                                  onChangeText={e => setFieldValue("engine", e.target.value)}
                                ></Field>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="engine"
                                  className="error-message"
                                />
                              </div>
                            </Col>
                          </Row>
                        </Card>

                        {!isRentPerHourTrip && (
                          <Card className="p-10">
                            <Card.Title className="ml-0">
                              Boat Information
                            </Card.Title>
                            <Row>
                              <Col>
                                <div className="mb-2">
                                  <Field
                                    label="Boat Length(ft)"
                                    id={"boatLength"}
                                    name={"boatLength"}
                                    value={values.boatLength}
                                    required
                                    type="number"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "boatLength",
                                        +e.target.value
                                      );
                                    }}
                                  ></Field>
                                   
                                  <ErrorMessage
                                    component="div"
                                    name="boatLength"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <div controlId="exampleForm.ControlSelect1">
                                    <span className="form-label required">
                                      No. of Guest capacity
                                    </span>
                                    <Form.Control
                                      as="select"
                                      value={values.maximumGuest}
                                      onChange={e => {
                                        setFieldValue(
                                          "maximumGuest",
                                          +e.target.value
                                        );
                                      }}
                                    >
                                      {renderOptions(50)}
                                    </Form.Control>
                                     
                                    <ErrorMessage
                                      component="div"
                                      name="maximumGuest"
                                      className="error-message"
                                    />
                                  </div>
                                </div>
                                <div className="mb-2">
                                  <div controlId="exampleForm.ControlSelect1">
                                    <span className="form-label required">
                                      No of Seats on Board
                                    </span>
                                    <Form.Control
                                      as="select"
                                      value={values.seatsOnBoat}
                                      onChange={e => {
                                        setFieldValue(
                                          "seatsOnBoat",
                                          +e.target.value
                                        );
                                      }}
                                    >
                                      {renderOptions(50)}
                                    </Form.Control>
                                     
                                    <ErrorMessage
                                      component="div"
                                      name="seatsOnBoat"
                                      className="error-message"
                                    />
                                  </div>
                                </div>
                              </Col>
                              <Col>
                                <div className="mb-2">
                                  <div controlId="exampleForm.ControlSelect1">
                                    <span className="form-label required">
                                      Bedrooms And Cabins
                                    </span>
                                    <Form.Control
                                      as="select"
                                      value={values.bedroomAndCabins}
                                      onChange={e => {
                                        setFieldValue(
                                          "bedroomAndCabins",
                                          +e.target.value
                                        );
                                      }}
                                    >
                                      {renderOptions(50)}
                                    </Form.Control>
                                     
                                    <ErrorMessage
                                      component="div"
                                      name="bedroomAndCabins"
                                      className="error-message"
                                    />
                                  </div>
                                </div>
                                <div className="mb-2">
                                  <div controlId="exampleForm.ControlSelect1">
                                    <span className="form-label">
                                      Bathrooms
                                    </span>
                                    <Form.Control
                                      as="select"
                                      value={values.bathrooms}
                                      onChange={e => {
                                        setFieldValue(
                                          "bathrooms",
                                          +e.target.value
                                        );
                                      }}
                                    >
                                      {renderOptions(50)}
                                    </Form.Control>
                                    
                                  </div>
                                </div>
                              </Col>
                            </Row>
                          </Card>
                        )}

                        {!isRentPerHourTrip && (
                          <Card className="p-10">
                            <Card.Title>Deck & Entertainment</Card.Title>

                            <Row>
                              <Col>
                                <div
                                  controlId="exampleForm.ControlSelect1"
                                  className="mt-15"
                                >
                                  <span className="form-label required">
                                    Deck & Entertainment
                                  </span>
                  
                                  <FormControl className="select-container minWidth-40 ml-2">
                                    {boatRentLookUps &&
                                      decksAndEntertainment && (
                                    <MultiSelect
                                    selectedOption={values.deckAndEntertainment}
                                    onChangeValue={item => setFieldValue("deckAndEntertainment", [...item])}
                                    options={decksAndEntertainment}
                                    />
                                      )}
                                  </FormControl>
                                   
                                  <ErrorMessage
                                    component="div"
                                    name="deckAndEntertainment"
                                    className="error-message"
                                  />
                                </div>
                              </Col>
                              <Col>
                                <div className="mb-2">
                                  <Field
                                    label="What To Bring"
                                    id={"whatToBring"}
                                    name={"whatToBring"}
                                    value={values.whatToBring}
                                    required
                                    type="text"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "whatToBring",
                                        e.target.value
                                      );
                                    }}
                                  ></Field>
                                  
                                  <ErrorMessage
                                    component="div"
                                    name="whatToBring"
                                    className="error-message"
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Card>
                        )}
                        <Card className="p-10">
                          <Card.Title>Rent Information</Card.Title>
                          <Row>
                            <Col>
                              <div className="mb-2">
                                <Field
                                  label="About US"
                                  id={"aboutUs"}
                                  name={"aboutUs"}
                                  value={values.aboutUs}
                                  required
                                  type="text"
                                  onChangeText={e => {
                                    setFieldValue("aboutUs", e.target.value);
                                  }}
                                ></Field>
                                
                                <ErrorMessage
                                  component="div"
                                  name="aboutUs"
                                  className="error-message"
                                />
                              </div>
                              <div className="mb-2">
                                <Field
                                  label="Description Of Our Trip"
                                  id={"descriptionOfOurTrip"}
                                  name={"descriptionOfOurTrip"}
                                  required
                                  value={values.descriptionOfOurTrip}
                                  type="text"
                                  onChangeText={e => {
                                    setFieldValue(
                                      "descriptionOfOurTrip",
                                      e.target.value
                                    );
                                  }}
                                ></Field>
                                
                                <ErrorMessage
                                  component="div"
                                  name="descriptionOfOurTrip"
                                  className="error-message"
                                />
                              </div>

                              <div className="mb-2">
                                <Field
                                  label="Add your policy"
                                  id={"policy"}
                                  name={"policy"}
                                  value={values.policy}
                                  type="text"
                                  required
                                  onChangeText={e => {
                                    setFieldValue("policy", e.target.value);
                                  }}
                                ></Field>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="policy"
                                  className="error-message"
                                />
                              </div>

                              <div className="mb-2">
                                <Field
                                  label="YouTube Link Video"
                                  id={"youtubeLinkVideo"}
                                  name={"youtubeLinkVideo"}
                                  value={values.youtubeLinkVideo}
                                  type="text"
                                  onChangeText={e => {
                                    setFieldValue(
                                      "youtubeLinkVideo",
                                      e.target.value
                                    );
                                  }}
                                ></Field>
                                 
                                <ErrorMessage
                                  component="div"
                                  name="youtubeLinkVideo"
                                  className="error-message"
                                />
                              </div>
                              <div className="mb-2">
                                <div
                                  controlId="exampleForm.ControlSelect1"
                                  className="mt-15"
                                >
                                  <span className="form-label">Deposit</span>

                                  <FormControl className="select-container minWidth-40 ml-2">
                                    {boatRentLookUps &&
                                      boatRentLookUps.tripDeposit && (
                                        <Select
                                          fullWidth
                                          name="deposit"
                                          value={values.deposit}
                                          onChange={e =>
                                            setFieldValue(
                                              "deposit",
                                              e.target.value
                                            )
                                          }
                                        >
                                          {renderMenuItems(
                                            boatRentLookUps.tripDeposit
                                          )}
                                        </Select>
                                      )}
                                  </FormControl>
                                 
                                </div>
                              </div>
                            </Col>

                            <Col>
                              <div className="d-flex justify-content-center flex-column h-100">
                                <div className="d-flex flex-wrap justify-content-center">
                                {values &&
                                  values.boatLayout && values.boatLayout.length ? 
                                  values.boatLayout.map((img, index) => (
                                    <div className="addBoat-container mr-2 mb-2">
                                        <img
                                          src={img}
                                          alt="uploaded layout"
                                          height="100px"
                                          width="100px"
                                        />
                                        <span
                                          onClick={() =>
                                            this.handleFileDelete(
                                              index,
                                              "boatLayout",
                                              setFieldValue,
                                              values
                                            )
                                          }
                                        >
                                          ×
                                      </span>
                                      </div>
                                    )) : null }
                                      <>
                                        <div className="addBoatShow-imgUploader">
                                          <ImageUploader
                                            withIcon={true}
                                            buttonText="Upload Layout Image"
                                            onChange={file =>
                                              this.handleFileUpload(
                                                file,
                                                "boatLayout",
                                                values.boatLayout,
                                                setFieldValue
                                              )
                                            }
                                            maxFileSize={5242880}
                                          />
                                           
                                          <ErrorMessage
                                            component="div"
                                            name="boatLayout"
                                            className="error-message"
                                          />
                                        </div>
                                      </>
                                    
                                </div>

                                <div className="d-flex flex-wrap justify-content-center">
                                  {values &&
                                    values.images && values.images.length ?
                                      values.images.map((img, index) => (
                                      <div className="addBoat-container mr-2 mb-2">
                                        <img
                                          src={img}
                                          alt="uploaded trip pictures"
                                          height="100px"
                                          width="100px"
                                        />
                                        <span
                                          onClick={() =>
                                            this.handleFileDelete(
                                              index,
                                              "images",
                                              setFieldValue,
                                              values
                                            )
                                          }
                                        >
                                          ×
                                      </span>
                                      </div>
                                    )) : null }
                                      <>
                                        <div className="addBoatShow-imgUploader required">
                                          <ImageUploader
                                            withIcon={true}
                                            buttonText="Upload Trip Picture"
                                            onChange={file =>
                                              this.handleFileUpload(
                                                file,
                                                "images",
                                                values.images,
                                                setFieldValue
                                              )
                                            }
                                            maxFileSize={5242880}
                                          />
                                          
                                          <ErrorMessage
                                            component="div"
                                            name="images"
                                            className="error-message"
                                          />
                                        </div>
                                      </>
                                    

                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Card>
                      </Col>
                    </Row>

                    {validationError && <ErrorComponent errors={errorMessage} />}

                    <div className="d-flex justify-content-center">
                      <Button
                        onClick={handleSubmit}
                        type="button"
                        className="button btn btn-primary w-auto addBoatService-btn primary-button"
                      >
                        Save
                      </Button>
                      <Button
                        variant="contained"
                        className="button btn w-auto  btn-dark"
                        onClick={() => cancelHandler(history)}
                      >
                        Cancel
                      </Button>
                    </div>
                  </div>
                </Form>
              )}
            ></Formik>
          </Container>
        </DashboardLayout>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  success: state.boatRentReducer && state.boatRentReducer.success,
  createSuccess: state.boatRentReducer && state.boatRentReducer.createSuccess,
  updatedSuccess: state.boatRentReducer && state.boatRentReducer.updatedSuccess,
  boatRentLookUps:
    state.boatRentReducer && state.boatRentReducer.boatRentLookUps,
  boatRentTypes: state.boatRentReducer.boatRentTypes,
  validationError: state.boatRentReducer.validationError,
  errorMessage: state.errorReducer.errorMessage,
});

const mapDispatchToProps = dispatch => ({
  createBoatRent: data => dispatch(createBoatRent(data)),
  clearBoatRentFlag: () => dispatch(clearBoatRentFlag()),
  getBoatRentLookUps: () => dispatch(getBoatRentLookUps()),
  getAllBoatRentOfUser: () => dispatch(getAllBoatRentOfUser()),
  getAllRentTypes: data => dispatch(getAllRentTypes(data)),
  updateBoatRent: data => dispatch(updateBoatRent(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateRentBoat);
