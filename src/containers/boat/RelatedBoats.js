import React, { Component } from 'react';
import { connect } from 'react-redux'

import { Layout } from '../../components';
import { resultMessage } from '../../util/enums/enums';
import BoatListingsWithMap from '../../components/gridComponents/BoatListingsWithMap';


class RelatedBoats extends Component {

    state = {
        showMap: false
    }

    toggleMapHandler = () => {
        this.setState(prevState => ({ showMap: !prevState.showMap }))
    }

    render() {

        const { boat } = this.props
        const { showMap } = this.state

        return (
            <Layout isFooter>
                {boat.seller.relatedItems &&
                    <BoatListingsWithMap
                        boatsType="Related Boats"
                        boatsTypeCount={boat.seller.relatedItems.length}
                        showMap={showMap}
                        toggleMapHandler={this.toggleMapHandler}
                        boats={boat.seller.relatedItems}
                        message={resultMessage.search}
                    />
                }
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    boat: state.boatReducer.boat || {},
})

const mapDispatchToProps = dispatch => ({
    // 
})

export default connect(mapStateToProps, mapDispatchToProps)(RelatedBoats) 