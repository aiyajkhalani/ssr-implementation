import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch } from "@material-ui/core";
import ReactTable from "react-table";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";

import {
  getAllBoatRentOfUser,
  deleteBoatRent,
  clearBoatRentDeleteFlag
} from "../../redux/actions/boatRentAction";
import UserContext from "../../UserContext";
import { verifiedCheck, renderSelectOptions } from "../../helpers/string";
import { confirmSubmitHandler } from "../../helpers/confirmationPopup";
import { pagination } from "../../util/enums/enums";
import { SuccessNotify } from "../../helpers/notification";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import { viewRentBoatHandler } from "../../helpers/boatHelper";
// import "../../styles/manageDashboardResponsive.scss"
import "../../styles/manageDashboardTableResponsive.scss";

class ManageBoatRents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          Header: "Boat Name",
          accessor: "boatName"
        },
        {
          Header: "Ad ID",
          accessor: "adId"
        },
        {
          Header: "Trip",
          accessor: "trip",
          Cell: data => <span>{data.original.trip.alias}</span>
        },
        {
          Header: "Trip Duration",
          accessor: "tripDuration"
        },
        {
          Header: "Ad Status",
          accessor: "adStatus",
          Cell: data => (
            <div className="d-flex justify-content-center">
              <Switch
                checked={data.original.status}
                // onChange={() => toggleBoatStatus({ id: data.original.id, columnName: 'adStatus' })}
                value={data.original.adStatus}
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
            </div>
          )
        },
        {
          Header: "Boat Rent Status",
          accessor: "status",
          Cell: data => (
            <span
              className={`bg-green-color font-13 text-capitalize m-auto ${verifiedCheck(
                data.original.status
              )}`}
            >
              {verifiedCheck(data.original.status)}
            </span>
          )
        },
        {
          Header: "Actions",
          Cell: data => (
            <div className="d-flex flex-row justify-content-around action">
              <button
                type="button"
                className="btn btn-outline-success mr-2"
                onClick={() => this.editRent(data.original)}
              >
                Edit
              </button>

              <button
                type="button"
                className="btn btn-outline-info mr-2"
                onClick={() =>
                  viewRentBoatHandler(data.original, props.history)
                }
              >
                View
              </button>

              <button
                type="button"
                className="btn btn-outline-danger mr-2"
                onClick={() =>
                  confirmSubmitHandler(props.deleteBoatRent, data.original.id)
                }
              >
                Delete
              </button>
            </div>
          )
        }
      ]
    };
  }
  static contextType = UserContext;

  componentDidMount() {
    const { getAllBoatRentOfUser } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: 1000
    };
    getAllBoatRentOfUser(data);
  }

  prePareEditRecord = values => {
    const {
      trip: { id },
      deckAndEntertainment,
      tripType,
      deposit: { id: depositId },
      geometricLocation,
      geometricLocation: { coordinates },
      city,
      state,
      country,
      placeName,
      postalCode,
      tripCity,
      tripCountry,
      tripLatitude,
      tripLongitude,
      tripPlaceName,
      tripPostalCode,
      tripState,
      boatLayout,
      images
    } = values;

    return {
      ...values,
      trip: id,
      deckAndEntertainment: renderSelectOptions(
        deckAndEntertainment,
        "alias",
        "id"
      ),
      tripType: tripType.id,
      deposit: depositId,
      geometricLocation: geometricLocation,
      latLng: { lng: coordinates[0], lat: coordinates[1] },
      tripLatLng: { lng: tripLongitude, lat: tripLatitude },
      location: {
        city,
        state,
        country,
        placeName,
        postalCode,
        longitude: coordinates[0],
        latitude: coordinates[1]
      },
      tripLocation: {
        city: tripCity,
        state: tripState,
        country: tripCountry,
        placeName: tripPlaceName,
        postalCode: tripPostalCode,
        latitude: tripLatitude,
        longitude: tripLongitude
      }
    };
  };
  editRent = record => {
    const { history } = this.context;

    history.push("/add-rent-boat", {
      editRent: this.prePareEditRecord(record),
      isEdit: true
    });
  };

  static getDerivedStateFromProps(nextProps) {
    const { deleteSuccess } = nextProps;

    if (deleteSuccess) {
      SuccessNotify("Rent Boat deleted Successfully");
      clearBoatRentDeleteFlag();
    }
    return null;
  }

  render() {
    const { boatRents, history } = this.props;
    const { columns } = this.state;

    return (
      <DashboardLayout>
        <div className="row ml-0 mr-0 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header d-flex justify-content-between">
                <h4 className="card-title">Manage Boat Rents</h4>
                <button
                  type="button"
                  class="btn btn-primary primary-button"
                  onClick={() => {
                    history.push("/add-rent-boat");
                  }}
                >
                  Add Boat Rent
                </button>
              </div>

              <div className="card-body mt-0">
                <div className="table-responsive">
                  {boatRents && (
                    <ReactTable
                      columns={columns}
                      data={boatRents}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  boatRents: state.boatRentReducer && state.boatRentReducer.boatRents,
  deleteSuccess: state.boatRentReducer && state.boatRentReducer.deleteSuccess
});

const mapDispatchToProps = dispatch => ({
  getAllBoatRentOfUser: data => dispatch(getAllBoatRentOfUser(data)),
  deleteBoatRent: data => dispatch(deleteBoatRent(data)),
  clearBoatRentDeleteFlag: () => dispatch(clearBoatRentDeleteFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageBoatRents);
