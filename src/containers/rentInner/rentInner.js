import React, { Component } from "react";
import { Grid, Link, Box } from "@material-ui/core";
import { Carousel } from "react-bootstrap";
import { connect } from "react-redux";
import Rating from "react-rating";
import { Player, BigPlayButton } from "video-react";

import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Truncate from "react-truncate";
import CarouselGallery, { Modal, ModalGateway } from "react-images";

import { Layout, rentInnerReview, rentInnerTrip } from "../../components";
import InnerRating from "../../components/staticComponent/InnerRating";

import RentInnerReviews from "../../components/gridComponents/InnerReviews";
import { RentSharedTrips } from "../../components/gridComponents/rentSharedTrips";
import InnerReviews from "../../components/gridComponents/InnerReviews";
import "./rentInner.scss";
import "./rentInnerResponsive.scss";
import {
  getRentInnerBoat,
  increaseBoatRentViewCount,
  getRentTripCityWise
} from "../../redux/actions";
import {
  dateStringFormateByHour,
  prepareGalleryData,
  priceFormat
} from "../../util/utilFunctions";
import { icons, showAll, dimension } from "../../util/enums/enums";
import { snakeCase, displayDefaultReview } from "../../helpers/string";
import GoogleMarker from "../../components/map/marker";
import { clearReviewFlag } from "../../redux/actions/ReviewAction";
import { BoatInnerGallery } from "../../components/styleComponent/styleComponent";
import moment from "moment";
import ReadMore from "../../components/helper/truncate";
import { getSingleBoatMarker, getRentMarker } from "../../helpers/boatHelper";

const length = 5;
class RentInner extends Component {
  state = {
    boatId: "",
    isGallery: false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { match, isReview, clearReviewFlag, getRentInnerBoat } = nextProps;
    const { params } = match && match;
    const { boatId } = prevState;
    if (isReview) {
      boatId && getRentInnerBoat({ id: boatId });
      clearReviewFlag();
    }
    if (params && params.hasOwnProperty("id") && params.id) {
      return {
        boatId: params.id
      };
    }

    return null;
  }

  openLightbox = () => {
    this.setState({ isGallery: true });
  };
  closeLightbox = () => {
    this.setState({ isGallery: false });
  };

  componentDidMount() {
    const {
      getRentInnerBoat,
      increaseBoatRentViewCount,
      getRentTripCityWise
    } = this.props;
    const { boatId } = this.state;

    boatId && getRentInnerBoat({ id: boatId });
    boatId && increaseBoatRentViewCount({ boatRentId: boatId });

    boatId && getRentTripCityWise({ id: boatId });
  }

  renderBuyNowSection = () => {
    const { boatRentInnerData } = this.props;
    const { price } = boatRentInnerData;

    return (
      <div className="d-flex justify-content-end align-items-center h-100 pl-0 pr-0">
        <div className="d-flex flex-column h-100 justify-content-evenly">
          <Link
            className="btn btn-xs btn-link-info pl-0 font-10 f-500 text-right p-0 mb-1"
            to="#"
            data-toggle="modal"
            data-target="#currency-modal"
          >
            <u className="text-decoration-none font13 font-weight-400 sky-blue">
              USE CURRENCY CONVERTER
            </u>
          </Link>
          <div className="price-wrap m-0 h5 d-flex mb-1 pb-1">
            <span className="f-400 font-22">
              INR {price} <span className="font-14">/hour</span>
            </span>
            {/* {priceType && (
              <div className=" light font-11 font-normal">
                Price {priceType.alias}
              </div>
            )} */}
          </div>
          <div className="mt-1">
            <Link
              to="#"
              className="btn btn-md btn-rounded f-500 w-100 inner-header-button light-sky-blue rent-inner-card-hovered-button-effect"
            >
              Book Now
            </Link>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const {
      boatRentInnerData,
      history,
      boat,
      currentUser,
      rentTripCityWise
    } = this.props;
    const {
      aboutUs,
      adStatus,
      address,
      adId,
      boatName,
      bathrooms,
      bedroomAndCabins,
      boatLayout,
      boatLength,
      boatRentStatus,
      descriptionOfOurTrip,
      captainName,
      city,
      country,
      startTime,
      deposit,
      engine,
      id,
      endTime,
      latitude,
      longitude,
      manufacturer,
      maximumGuest,
      model,
      noOfUnit,
      placeName,
      policy,
      postalCode,
      price,
      rating,
      reviews,
      route,
      seatsOnBoat,
      state,
      trip,
      tripAddress,
      tripCity,
      tripCountry,
      tripDuration,
      tripLatitude,
      tripLongitude,
      images,
      tripPlaceName,
      tripPostalCode,
      tripRoute,
      tripState,
      tripType,
      user,
      userCount,
      whatToBring,
      yearOfManufacturer,
      youtubeLinkVideo,
      deckAndEntertainment
    } = boatRentInnerData || {};
    const { isGallery } = this.state;
    return (
      <Layout className="rent-inner-layout">
        <Grid
          container
          className="container-fluid clr-fluid sticky-on-top rent-inner-contain mb-3"
        >
          <div className="m-auto d-flex h-100 w-85vw pt-2 pb-2">
            <Grid item className="h-70 d-flex pr-0 pt-3 pb-3 pl-0" sm={4}>
              <div className="width-100 align-self-center">
                <div className="rent-text-color font-weight-500">
                  <span className="font-12 sky-blue font-weight-400">
                    Ad ID:
                    {adId}
                  </span>
                </div>
                <div className="d-flex align-items-center">
                  <span className="font-22">{boatName}</span>
                  <h6 className=" ml-3 mb-0 inner-header-badge dark-silver">
                    {trip && trip.alias}
                  </h6>
                  <h6 className=" ml-2 mb-0 text-capitalize inner-header-badge dark-silver">
                    {tripType && tripType.name}
                  </h6>
                </div>
                {boatRentInnerData && boatRentInnerData.reviews && (
                  <div className="d-flex align-items-center">
                    <div className="d-flex mr-2 mb-1">
                      <Rating
                        className="rating-clr rating-small-size"
                        initialRating={
                          boatRentInnerData.reviews.ratings &&
                          boatRentInnerData.reviews.ratings.averageRating
                        }
                        fullSymbol={<StarIcon />}
                        emptySymbol={<StarBorderIcon />}
                        placeholderSymbol={<StarBorderIcon />}
                        readonly
                      />
                    </div>
                    <h6 className="mr-3 m-0 font13 font-weight-400 dark-silver">
                      1
                      {boatRentInnerData.reviews.ratings &&
                        boatRentInnerData.reviews.ratings.averageRating}
                    </h6>
                    <span className="font13 gray-light font-weight-400">
                      {boatRentInnerData.reviews.ratings &&
                        boatRentInnerData.reviews.ratings.count}{" "}
                      Customer Reviews
                    </span>
                  </div>
                )}
              </div>
            </Grid>

            <Grid
              item
              className="h-70 d-flex align-items-center pt-3 pb-3 pr-0 pl-0 justify-content-center"
              sm={4}
            >
              <div className="widget-media text-center inner-header-social-icon">
                <ul className="icon-effect icon-effect-1a d-flex">
                  <li>
                    <Link to="/" className="icon d-flex justify-content-center">
                      <i className="adam-share dark-silver"></i>
                    </Link>
                  </li>
                  <li>
                    <Link to="/" className="icon d-flex justify-content-center">
                      <i className="adam-heart1 dark-silver"></i>
                    </Link>
                  </li>
                  <li>
                    <Link to="/" className="icon d-flex justify-content-center">
                      <i className="adam-flag-3 dark-silver"></i>
                    </Link>
                  </li>
                  {/* <li>
                    <Link to="/" className="icon d-flex justify-content-center">
                      <i className="adam-envlope dark-silver"></i>
                    </Link>
                  </li> */}
                  <li>
                    <Link to="/" className="icon d-flex justify-content-center">
                      <i className="adam-pdf dark-silver"></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </Grid>

            <Grid item className="h-70" sm={4}>
              {boatRentInnerData && this.renderBuyNowSection()}
            </Grid>
          </div>
        </Grid>

        <Grid
          container
          className="w-85vw m-auto position-relative rent-galley-image-grid cursor-pointer"
        >
          <div className="position-absolute inner-banner-likes-div d-flex align-items-center font13 gray-dark img-show-text">
            <div className="d-flex justify-content-center align-items-center ">
              <img
                src={require("../../assets/images/rentInner/like1.png")}
                className="inner-banner-views-img mr-1"
                alt=""
              />
              <div className="mt-1">3000</div>
            </div>
          </div>
          <div className="position-absolute inner-banner-top-social-div">
            <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
              <div className="d-flex justify-content-center align-items-center ">
                <img
                  src={require("../../assets/images/rentInner/view.png")}
                  className="inner-banner-views-img mr-2"
                  alt=""
                />
              </div>
              <div>{userCount}</div>
            </div>
            <div className="inner-banner-views-div mr-2 d-flex font13 gray-dark img-show-text">
              <div className="d-flex justify-content-center align-items-center mr-2">
                <img
                  src={require("../../assets/images/rentInner/image-count.png")}
                  className="inner-banner-views-img"
                  alt=""
                />
              </div>
              <div>{images && images.length}</div>
            </div>
          </div>
          <Grid item xs={6} className="hidden border-radius-padding-img ">
            <BoatInnerGallery
              onClick={this.openLightbox}
              img={
                images && images.length > 0
                  ? encodeURI(images[0])
                  : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
              }
            />
          </Grid>

          <Grid item xs={6}>
            <Grid container className="h-100 ">
              <Grid item xs={12}>
                <Grid container className="h-100">
                  <Grid
                    item
                    xs={6}
                    className="hidden border-radius-padding-img   h-100"
                  >
                    <BoatInnerGallery
                      onClick={this.openLightbox}
                      img={
                        images && images.length > 1
                          ? encodeURI(images[1])
                          : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                      }
                    />
                  </Grid>

                  <Grid
                    item
                    xs={6}
                    className="hidden border-radius-padding-img h-100"
                  >
                    <BoatInnerGallery
                      onClick={this.openLightbox}
                      img={
                        images && images.length > 2
                          ? encodeURI(images[2])
                          : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid container className="h-100">
                  <Grid
                    item
                    xs={6}
                    className="hidden border-radius-padding-img h-100 "
                  >
                    <BoatInnerGallery
                      onClick={this.openLightbox}
                      img={
                        images && images.length > 3
                          ? encodeURI(images[3])
                          : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                      }
                    />
                  </Grid>

                  <Grid
                    item
                    xs={6}
                    className="hidden border-radius-padding-img h-100"
                  >
                    <BoatInnerGallery
                      onClick={this.openLightbox}
                      img={
                        images && images.length > 4
                          ? encodeURI(images[4])
                          : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                      }
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <div className="rent-inner-contain">
          <div className="container-fluid clr-fluid w-85vw">
            <Grid container spacing={3} className="mt-50 pb-3">
              <Grid item md={9} className="pt-0 pb-0">
                <Grid container className="h-100">
                  <Grid item md={12}>
                    <div>
                      <h4 className=" mb-3 gray-dark">
                        <i className="adam-invoice mr-2 text-black"></i>
                        Our Trip
                      </h4>
                      <p className="text-justify a-inner-fix">
                        <ReadMore className="text-justify ">
                        {descriptionOfOurTrip}
                        </ReadMore>
                      </p>
                    </div>
                  </Grid>
                  <Grid item md={12}>
                    <div className="row">
                      <div className="col-md-3">
                        <h6 className="title-sm title-description mb-0 pb-1 font-weight-400 font-14 gray-dark">
                          Boat Name
                        </h6>
                        <p className="sub-title-sm m-0 font-14 dark-silver">
                          {boatName}
                        </p>
                      </div>
                      <div className="col-md-3">
                        <h6 className="title-sm title-description mb-0 pb-1 font-weight-400 font-14 gray-dark">
                          Captian Name
                        </h6>
                        <p className="sub-title-sm m-0 font-14 dark-silver">
                          {captainName}
                        </p>
                      </div>
                    </div>
                  </Grid>
                  <Grid item md={12} className="d-flex">
                    <div className="card border d-flex justify-content-center width-100 rent-inner-border m-0">
                      <div className="d-flex justify-content-around">
                        <div className="d-flex flex-column">
                          <span className="f-15 font-weight-500 sky-blue">
                            Payment & Booking
                          </span>
                          <span className="font-14 sub-title-sm">
                            Neque Porro quisquam dolorem ipsum
                          </span>
                        </div>

                        <div className="d-flex align-items-center">
                          <div>
                            <i className="adam-creditcard credit-icon text-black inner-payment-icon mr-3 sky-blue font-weight-500"></i>
                          </div>
                          <div className="d-flex flex-column">
                            <span className="f-15 font-weight-500 sky-blue">
                              Credit Card Payment
                            </span>
                            <span className="font-14 sub-title-sm">
                              Neque Porro quisquam
                            </span>
                          </div>
                        </div>

                        <div className="d-flex align-items-center">
                          <div>
                            <div className="d-flex justify-content-center align-items-center mr-3">
                              <img
                                src={require("../../assets/images/rentInner/payment.png")}
                                className="inner-payment-cash-icon"
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="d-flex flex-column">
                            <span className="f-15 sky-blue font-weight-500">
                              Cash on Arrival
                            </span>
                            <span className="font-14 sub-title-sm">
                              Neque Porro quisquam
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item md={3} className="pt-0 pb-0">
                <div className="col-12 p-0 h-100">
                  <div className="card border m-0 shadow-sm">
                    <div className="card-body mt-0 pt-3 pb-2 pl-3 pr-3">
                      <div className="col-12 brand">
                        <div className="col-12 pl-0 pr-0">
                          <div className="media  mt-3 mb-3 d-flex flex-column h-100 pt-0">
                            <div className="d-flex w-100 justify-content-between flex-wrap">
                              <div>
                                {user && user.images && (
                                  <div className="inner-user-info-section">
                                    <img
                                      className="rounded-circle cursor-pointer h-100 width-100 user-img-logo"
                                      src={encodeURI(user.images[0])}
                                      alt="user-img"
                                      onClick={() => {
                                        history.push("/user-profile", {
                                          seller: user
                                        });
                                      }}
                                    />
                                  </div>
                                )}
                              </div>
                              <div>
                                {user && user.companyLogo && (
                                  <div className="inner-user-info-section">
                                    <img
                                      className="d-block img-fluid h-100 width-100 rent-inner-logo"
                                      src={encodeURI(
                                        user &&
                                          user.companyLogo &&
                                          user.companyLogo
                                      )}
                                      alt="logo"
                                    />
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="media-body mt-2">
                            <div className="author-info">
                              <div>
                                <span className="font-16 font-weight-500 gray-dark">
                                  {user && user.firstName && user.firstName}{" "}
                                  {user && user.lastName && user.lastName} ,
                                </span>
                              </div>
                              <span className="font-14 font-weight-400 dark-silver">
                                {user && user.city && user.city},{" "}
                                {user && user.country && user.country}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12">
                        <ul className="list-group f-14">
                          <li className="list-group-item dark-silver border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                            <div className=" mr-2">
                              <img
                                src={require("../../assets/images/rentInner/user.png")}
                                className="inner-user-info-icon"
                                alt=""
                              />
                            </div>
                            <div className="rent-user-card-text">
                              {user && user.role && user.role.role}
                            </div>
                          </li>
                          <li className="list-group-item dark-silver border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                            <div className=" mr-2">
                              <img
                                src={require("../../assets/images/rentInner/place.png")}
                                className="inner-user-info-icon"
                                alt=""
                              />
                            </div>
                            <div className="rent-user-card-text">
                              On AdamSea since{" "}
                              {moment(
                                user && user.createdAt && user.createdAt
                              ).format("MMM, YYYY")}
                            </div>
                          </li>
                          <li className="list-group-item dark-silver border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                            <div className=" mr-2">
                              <img
                                src={require("../../assets/images/rentInner/vehicles.png")}
                                className="inner-user-info-icon"
                                alt=""
                              />
                            </div>
                            <div className="rent-user-card-text">
                              {" "}
                              View{" "}
                              {user &&
                                user.relatedItems &&
                                user.relatedItems.length}{" "}
                              vehicles
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div className="col-12 text-right">
                        <span className="font13 font-weight-500 sky-blue">
                          ABOUT US
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </Grid>
              <div className="mt-3 mb-0 text-right width-100 pr-3 d-flex justify-content-end align-items-center">
                <div className="mr-2">
                  <img
                    src={require("../../assets/images/rentInner/rentinner-like.png")}
                    alt=""
                    className="feedback-section inner-feedback-img"
                  />
                </div>
                <span className="font13 font-weight-500 sky-blue">
                  ADD FEEDBACK
                </span>
              </div>
            </Grid>
            <hr />
            <Grid container spacing={3}>
              <Grid item md={4}>
                <div className="">
                  <h4 className="mt-5 mb-4 gray-dark">Boat Location</h4>
                  <div className="boat-inner-map-div">
                    <GoogleMarker 
                    // height={35}
                    className="boat-inner-map-height"
                    markers={getRentMarker(boatRentInnerData)} />
                  </div>
                </div>
              </Grid>
              <Grid item md={8} className="pl-15">
                <h4 className="mt-5 mb-4 pl-3 pr-3 gray-dark">Trip Details</h4>
                <div className="col-md-12">
                  <div className="row mb-4">
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        Trip Destination
                      </h6>
                      <span className="sub-title-sm d-block font-14 dark-silver">
                        {tripAddress}
                        <span className="text-left  f-500 text-uppercase d-block mt-2 sky-blue">
                          <a
                            className="sky-blue d-flex align-items-center"
                            href="#"
                          >
                            <i className="adam-navigation-main mr-1 sky-blue"></i>
                            view map
                          </a>
                        </span>
                      </span>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        Available Trip Days
                      </h6>
                      <span className="sub-title-sm font-14 dark-silver">
                        Su, Mo, Tu, We, Th, Fr, Sa
                      </span>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        Start Time
                      </h6>
                      <span className="sub-title-sm font-14 dark-silver">
                        {dateStringFormateByHour(startTime)}
                      </span>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        End Time
                      </h6>
                      <span className="sub-title-sm font-14 dark-silver">
                        {dateStringFormateByHour(endTime)}
                      </span>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        Trip Duration
                      </h6>
                      <span className="sub-title-sm font-14 dark-silver">
                        {tripDuration}
                      </span>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-1 title-sm title-description font-weight-400 font-14 gray-dark">
                        GATHERING POINT
                      </h6>
                      <span className="sub-title-sm d-block font-14 dark-silver">
                        {address}
                        <span className="text-left  f-500 text-uppercase d-block mt-2 sky-blue">
                          <a
                            className="sky-blue d-flex align-items-center"
                            href="#"
                          >
                            <i className="adam-navigation-main mr-1 sky-blue"></i>
                            view map
                          </a>
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              </Grid>
            </Grid>
            <hr />
            <Grid container spacing={3}>
              <Grid item md={8}>
                <div className="col-md-12 mt-5 mb-4">
                  <h4 className="mt-5 mb-4 gray-dark">
                    <i className="adam-boat-berth mr-2 gray-dark"></i> Boat
                    Information
                  </h4>
                  <div className="row mb-2">
                    <div className="col-md-3">
                      <h6 className="mb-2 font-14 title-description font-weight-400 gray-dark">
                        Boat Length
                      </h6>
                      <p className="sub-title-sm font-14 dark-silver">
                        {boatLength} Feet
                      </p>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-2 font-14 title-description font-weight-400 gray-dark">
                        Maximum Guests
                      </h6>
                      <p className="sub-title-sm font-14 dark-silver">
                        {maximumGuest}
                      </p>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-2 font-14 title-description font-weight-400 gray-dark">
                        No. Of Seats on Board
                      </h6>
                      <p className="sub-title-sm font-14 dark-silver">
                        {seatsOnBoat}
                      </p>
                    </div>
                    <div className="col-md-3">
                      <h6 className="mb-2 font-14 title-description font-weight-400 gray-dark">
                        No. of Bedrooms & Cabins
                      </h6>
                      <p className="sub-title-sm font-14 dark-silver">
                        {bedroomAndCabins}
                      </p>
                    </div>
                  </div>

                  <div className="row mb-2">
                    <div className="col-md-6">
                      <h6 className="mb-2 font-14 title-description font-weight-400 gray-dark">
                        Bathroom
                      </h6>
                      <p className="sub-title-sm font-14 dark-silver">
                        {bathrooms}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-12 mt-5 mb-4">
                  <h4 className="mt-5 mb-4 gray-dark">
                    <i className="adam-five-stars mr-2 text-black gray-dark"></i>{" "}
                    Deck & Entertainment
                  </h4>

                  <div className="col-md-12 pl-0">
                    <Grid container>
                      <div className="row width-100 pl-0">
                        {deckAndEntertainment &&
                          deckAndEntertainment.length > 0 &&
                          deckAndEntertainment.map(item => {
                            return (
                              <div className="mb-4 mr-4 mt-0 ml-0">
                                <div className="inner-type-div m-auto">
                                  <div className="inner-trip-type-icon-div mb-2 mt-1">
                                    <i className="adam-sailingboat-1 list-item-color dark-silver"></i>
                                  </div>
                                  <div className="inner-trip-type-text-div">
                                    <span className="font-14 trip-type-inner sub-title-sm dark-silver">
                                      {item.alias}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </Grid>
                  </div>
                </div>
              </Grid>
              <Grid item md={4}>
                <div>
                  <h4 className="mt-5 mb-4 gray-dark">Boat Layout</h4>
                  <Carousel>
                    <Carousel.Item>
                      <img
                        className="d-block w-100 br-10 h-100"
                        src={require("../../assets/images/rentInner/img-11.jpg")}
                        alt="First slide"
                      />
                    </Carousel.Item>
                    <Carousel.Item>
                      <img
                        className="d-block w-100 br-10 h-100"
                        src={require("../../assets/images/rentInner/img-2.jpg")}
                        alt="Third slide"
                      />
                    </Carousel.Item>
                    <Carousel.Item>
                      <img
                        className="d-block w-100 br-10 h-100"
                        src={require("../../assets/images/rentInner/img-3.jpg")}
                        alt="Third slide"
                      />
                    </Carousel.Item>
                  </Carousel>
                </div>
              </Grid>
            </Grid>
            {youtubeLinkVideo && youtubeLinkVideo.length > 0 && ( 
             <Grid container>
              <Grid item sm={4}>
                <div className="align-items-end justify-content-between">
                  <h4 className="mt-5 mb-4 gray-dark">Related Videos</h4>
                  <div className="video-container h-100 w-100">
                    <Player
                      controls={false}
                      playsInline
                      muted={true}
                      poster="/assets/poster.png"
                      src={youtubeLinkVideo}
                    >
                      <BigPlayButton position="center" />
                    </Player>
                  </div>
                </div>
              </Grid>
            </Grid>
            )}

            <Grid container>
              <Grid item sm={12} md={12} lg={12}>
                <div className="container">
                  <div className="row mt-50 mb-5">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-0">
                      <h4 className="gray-dark">
                        <i className="iconColor adam-review mr-2 text-black gray-dark"></i>
                        Review and Rating
                      </h4>
                      {boatRentInnerData && boatRentInnerData.reviews && (
                        <>
                          <div className="rent-inner-contain inner-rating-section inner-rating-div">
                            <InnerRating
                              iconColor="iconColor-rentInner"
                              btnColor="rentInner-btnBg"
                              btnBlue="rentInner-btnBlue"
                              progressColor="rent-progress"
                              moduleId={id}
                              userId={currentUser.id}
                              data={displayDefaultReview(
                                boatRentInnerData.reviews.ratings
                              )}
                              reviews={boatRentInnerData.reviews.reviews}
                            />
                          </div>
                          <div className="mt-5 user-review-rent inner-review-section">
                            <InnerReviews
                              xs={12}
                              sm={12}
                              data={boatRentInnerData.reviews.reviews}
                              iconColor="iconColor-rentInner"
                            />
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                  <hr />
                </div>
              </Grid>
            </Grid>
            {/* Don't delete below code @miraj */}
            {rentTripCityWise && rentTripCityWise.length > 0 && (
              <div className="container-fluid rent-layout">
                <Grid item xs={12} className="mb-5">
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  >
                    Similar Trip in India
                  </Box>

                  <RentSharedTrips
                    xs={12}
                    sm={3}
                    itemsLength={8}
                    showType={showAll.privateTrip}
                    dimension={dimension.privateTrip}
                    rentSharedTrips={rentTripCityWise}
                    showAllText="private trips"
                  />
                </Grid>
              </div>
            )}
          </div>
        </div>
        {images && images.length > 0 && isGallery && (
          <ModalGateway>
            <Modal onClose={this.closeLightbox}>
              <CarouselGallery views={prepareGalleryData(images)} />
            </Modal>
          </ModalGateway>
        )}
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  boatRentInnerData:
    state.boatRentReducer && state.boatRentReducer.boatRentInnerData,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  isReview: state.reviewReducer && state.reviewReducer.isReview,
  rentTripCityWise:
    state.boatRentReducer && state.boatRentReducer.rentTripCityWise
});

const mapDispatchToProps = dispatch => ({
  getRentInnerBoat: data => dispatch(getRentInnerBoat(data)),
  increaseBoatRentViewCount: data => dispatch(increaseBoatRentViewCount(data)),
  clearReviewFlag: () => dispatch(clearReviewFlag()),
  getRentTripCityWise: data => dispatch(getRentTripCityWise(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(RentInner);
