import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FaEyeSlash, FaEye } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";

import { Layout } from "../../components/layout/layout";
import { clearAuthorizationFlag, resetPassword, forgotPassword } from "../../redux/actions";
import { ErrorNotify, SuccessNotify } from "../../helpers/notification";
import { Notification } from "../../components";
import TermAndPolicy from "../../components/termAndPolicy";
import "../login/login.scss";
import { renderErrorMessage } from "../../helpers/jsxHelper";

class ForgetPassword extends Component {
  state = {
    user: {
      token: "",
      password: "",
      confirmPassword: ""
    },
    type: "password",
    wording: <FaEye />,
    errorMessages: {},

  };

  changeState = () => {
    var oldState = this.state.type;
    var isTextOrHide = oldState === "password";
    var newState = isTextOrHide ? "text" : "password";
    var newWord = isTextOrHide ? <FaEyeSlash /> : <FaEye />;
    this.setState({
      type: newState,
      wording: newWord
    });
  };

  validate = (e, setValue, email) => {
    const { errorMessages } = this.state;
    let password = e.target.value;
    let capsCount,
      smallCount,
      symbolCount,
      numberCount,
      userEmail,
      uniqPassword;
    if (password.length < 8) {
      errorMessages.passwordLength = true;
    } else {
      errorMessages.passwordLength = false;
    }
    userEmail = email.toLowerCase();
    uniqPassword = password.toLowerCase();
    capsCount = (password.match(/[A-Z]/g) || []).length;
    smallCount = (password.match(/[a-z]/g) || []).length;
    symbolCount = (password.match(/\W/g) || []).length;
    numberCount = (password.match(/[0-9]/g) || []).length;

    if (capsCount < 1) {
      errorMessages.capsCount = true;
    } else {
      errorMessages.capsCount = false;
    }
    if (smallCount < 1) {
      errorMessages.smallCount = true;
    } else {
      errorMessages.smallCount = false;
    }
    if ((symbolCount && numberCount) < 1) {
      errorMessages.symbolOrNumberCount = true;
    } else {
      errorMessages.symbolOrNumberCount = false;
    }

    if (userEmail.includes(uniqPassword)) {
      errorMessages.emailIncludePassword = true;
    } else {
      errorMessages.emailIncludePassword = false;
    }
    this.setState({
      errorMessages
    });
    this.measureStrength(password, setValue);
  };

  measureStrength = (password, setValue) => {
    let score = 0;
    let passwordStrength;
    let regexPositive = ["[A-Z]", "[a-z]", "[0-9]", "\\W"];
    regexPositive.forEach((regex, index) => {
      if (new RegExp(regex).test(password)) {
        score += 1;
      }
    });
    switch (score) {
      case 1:
        passwordStrength = "poor";
        break;

      case 2:
        passwordStrength = "poor";
        break;

      case 3:
        passwordStrength = "average";
        setValue("password", password);
        break;

      case 4:
        passwordStrength = "strong";
        setValue("password", password);
        break;

      default:
        passwordStrength = "weak";
    }
    this.setState({
      passwordStrength,
      isDisplayHint: true
    });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { forgotPasswordSuccess, isPasswordResetError, history } = nextProps;
    const { user } = prevState;
    const { location } = history;

    if (location && location.search && !forgotPasswordSuccess) {
      const urlParams = new URLSearchParams(location.search);

      if (urlParams.has("token")) {
        const token = urlParams.get("token");
        localStorage.setItem('token', token)
        user.token = token;
        return {
          user
        };
      }
    } else if (forgotPasswordSuccess) {
      setTimeout(() => SuccessNotify("your password reset successfully"), 100);
      history.push("/login");
    } else if (isPasswordResetError) {
      ErrorNotify("invalid token provided");
    }

    return null;
  }

  render() {
    const { user, errorMessages, passwordStrength, isDisplayHint } = this.state;
    const { forgotPassword } = this.props;

    return (
      <>
        <Layout>
          <Notification />
          <Grid container alignItems="center">
            <Grid item sm={12}>
              <Formik
                initialValues={user}
                validationSchema={Yup.object().shape({
                  password: Yup.string()
                    .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/, " Week Password!")
                    .required("Password field is required"),
                  confirmPassword: Yup.string()
                    .oneOf([Yup.ref("password"), null], "Passwords must match")
                    .required("Confirm password field is required")
                })}
                onSubmit={values => {
                  delete values.confirmPassword;
                  if (
                    !errorMessages.symbolOrNumberCount &&
                    !errorMessages.passwordLength &&
                    passwordStrength !== "poor"
                  ) {
                    forgotPassword(values);
                  }
                }}
                render={({
                  errors,
                  status,
                  touched,
                  setFieldValue,
                  handleSubmit
                }) => (
                    <div className="form-holder">
                      <div className="form-content min-height-unset">
                        <div className="form-items forget-password-form p-5">
                          <h3 className="text-left font-weight-500">
                            Forget Password
                        </h3>
                          <div className="page-links">
                            <span className="title-text">
                              Your password needs to have at least one symbol or
                              number, and have at least 8 characters and should also have one capital.
                          </span>
                          </div>
                          <Form className="mt-4 mb-4">
                            <div className="position-relative">
                              <input
                                id="password"
                                name="password"
                                type={this.state.type}
                                className={
                                  "form-control mt-10" +
                                  (errors.password && touched.password
                                    ? " is-invalid"
                                    : "")
                                }
                                placeholder="New Password"
                                onChange={e => {
                                  setFieldValue("password", e.target.value)
                                  this.validate(e, setFieldValue, "");
                                }
                                }
                                required
                              ></input>
                              <span
                                className="form-side-icon"
                                onClick={this.changeState}
                              >
                                {this.state.wording}
                              </span>

                              <div className="mt-2">
                                {isDisplayHint && (
                                  <div className="mt-2 register-password-hint">
                                    {errorMessages && errorMessages.passwordLength
                                      ? renderErrorMessage(
                                        "At least 8 characters",
                                        "error-message mt-0",
                                        true
                                      )
                                      : renderErrorMessage(
                                        "At least 8 characters",
                                        "error-message-fullfill",
                                        false
                                      )}
                                    {errorMessages &&
                                      errorMessages.symbolOrNumberCount
                                      ? renderErrorMessage(
                                        "At least 1 symbol & Number",
                                        "error-message mt-0",
                                        true
                                      )
                                      : renderErrorMessage(
                                        "At least 1 symbol & Number",
                                        "error-message-fullfill",
                                        false
                                      )}
                                    {passwordStrength && (passwordStrength === "poor" || passwordStrength === "weak")
                                      ? renderErrorMessage(
                                        `Password Strength: ${passwordStrength}`,
                                        "error-message mt-0",
                                        true
                                      )
                                      : renderErrorMessage(
                                        `Password Strength: ${passwordStrength}`,
                                        "error-message-fullfill",
                                        false
                                      )}
                                    {errorMessages &&
                                      errorMessages.emailIncludePassword
                                      ? renderErrorMessage(
                                        "Can not Contain Email or name in password",
                                        "error-message mt-0",
                                        true
                                      )
                                      : renderErrorMessage(
                                        "Can not Contain Email or name in password",
                                        "error-message-fullfill",
                                        false
                                      )}
                                    {errorMessages && errorMessages.capsCount
                                      ? renderErrorMessage(
                                        "password must contain one capital",
                                        "error-message mt-0",
                                        true
                                      )
                                      : renderErrorMessage(
                                        "password must contain one capital",
                                        "error-message-fullfill",
                                        false
                                      )}
                                  </div>
                                )}
                              </div>

                              {/* <p className="mb-2 mt-2 text-left font-14 darkBlue">{errors.password}</p> */}

                              <ErrorMessage
                                name="password"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                            <div className="position-relative">
                              <input
                                id="confirmPassword"
                                name="confirmPassword"
                                type={this.state.type}
                                className={
                                  "form-control mt-10" +
                                  (errors.confirmPassword && touched.confirmPassword
                                    ? " is-invalid"
                                    : "")
                                }
                                placeholder="Confirm Password"
                                onChange={e =>
                                  setFieldValue("confirmPassword", e.target.value)
                                }
                                required
                              ></input>
                              <span
                                className="form-side-icon"
                                onClick={this.changeState}
                              >
                                {this.state.wording}
                              </span>
                              <p className="mb-2 mt-2 text-left font-14 darkBlue">{errors.confirmPassword}</p>
                              <ErrorMessage
                                name="confirmPassword"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                            <Grid container className="mt-10">
                              <Grid item xs>
                                <div className="clearfix"></div>
                              </Grid>
                            </Grid>

                            <div className="clearfix mt-3" onClick={handleSubmit}>
                              <a
                                id="submit"
                                type=""
                                className="btn btn-block btn-lg btn-primary w-auto"
                              >
                                Submit
                            </a>
                            </div>
                          </Form>
                          <div>
                            {/* <span>
                            Ensure that whenever you sign in to AdamSea, the Web
                            address in your browser starts with{" "}
                          </span>
                          <a href="#" className="darkBlue">
                            www.adamsea.com
                          </a> */}
                            <span className="title-text font-14">
                              By clicking “Submit”, you confirm that you accept
                              the Terms of Service & Privacy Policy.
                          </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
              />
            </Grid>
          </Grid>
        </Layout>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isPasswordReset: state.loginReducer.isPasswordReset,
  isPasswordResetError: state.loginReducer.isPasswordResetError,
  forgotPasswordSuccess: state.loginReducer.forgotPasswordSuccess
});

const mapDispatchToProps = dispatch => ({
  forgotPassword: data => dispatch(forgotPassword(data)),
  clearAuthFlag: () => dispatch(clearAuthorizationFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);
