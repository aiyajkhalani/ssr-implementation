import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Grid,
  TextField,
  FormControlLabel,
  Checkbox,
  Button
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { MdEmail } from "react-icons/md";

import {
  login,
  clearAuthorizationFlag,
  forgetPasswordMail
} from "../../redux/actions";
import "../login/login.scss";
import { ErrorNotify, SuccessNotify } from "../../helpers/notification";
import { Notification } from "../../components";
import TermAndPolicy from "../../components/termAndPolicy";
import ErrorComponent from "../../components/error/errorComponent";

class SendResetLink extends Component {
  state = {
    user: {
      email: ""
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { forgotPasswordMailSent, history } = nextProps;

    if (forgotPasswordMailSent) {
      SuccessNotify("mail has been sent. please check your mail");
    }

    return null;
  }
  componentDidMount(){
    const { clearAuthFlag } = this.props;
    clearAuthFlag();
  }
  render() {
    const { user } = this.state;
    const { forgetPasswordMail, errorMessage, isError } = this.props;

    return (
      <>
        <Notification />
        <Grid container className="flex-1" alignItems="center">
          <Grid item className="h100-vh img-holder" sm={6}>
            <div className="website-logo ml-50">
              <Link to="/">
                <div className="logo">
                  <img
                    className="logo-size"
                    src={require("../../assets/images/login/logo-white.png")}
                  />
                </div>
              </Link>
            </div>
            <Grid container className="flex-1" alignItems="center">
              <div
                style={{
                  display: "flex",
                  flex: 0.5,
                  justifyContent: "center",
                  alignItems: "flex-start",
                  minHeight: "100vh",
                  flexDirection: "column",
                  marginLeft: "3em"
                }}
              >
                <h3 style={{ color: "white" }}>Welcome</h3>
                <h3 style={{ color: "white" }}>Back To Adamsea</h3>

                <TermAndPolicy />
              </div>
            </Grid>
          </Grid>
          <Grid item sm={6}>
            <Formik
              initialValues={user}
              validationSchema={Yup.object().shape({
                email: Yup.string()
                  .email("Email is invalid")
                  .required("This field is required")
              })}
              onSubmit={values => forgetPasswordMail(values)}
              render={({
                errors,
                status,
                touched,
                setFieldValue,
                handleSubmit
              }) => (
                <div className="form-holder">
                  <div className="form-content">
                    <div className="form-items">
                      <h3>Get help signing in</h3>
                      <div className="page-links"></div>
                      <Form>
                      <p className="font-16 text-left mb-2">
                      Enter the email address associated with your account, and we’ll email you a link to reset your password 
                          </p>
                        <div className="position-relative">
                          <input
                            id="email"
                            name="email"
                            type="email"
                            className={
                              "form-control mt-10 font-16 mb-3" +
                              (errors.email && touched.email
                                ? " is-invalid"
                                : "")
                            }
                            placeholder="Email Address"
                            onChange={e =>
                              setFieldValue("email", e.target.value)
                            }
                            required
                          ></input>
                          <span className="form-side-icon">
                            <MdEmail />
                          </span>
                          <ErrorMessage
                            name="email"
                            component="div"
                            className="invalid-feedback"
                          />
                         
                          <div className="clearfix mt-4 mb-4 w-100" onClick={handleSubmit}>
                            <a
                              id="submit"
                              type=""
                              className="btn btn-block btn-lg btn-primary font-weight-500 w-100"
                            >
                              Send Reset Link
                            </a>
                          </div>
                          
                          {isError && <ErrorComponent errors={errorMessage} />}
                         
                          <p className="font-16 text-left">
                          You need more help ?
                          <Link to="#" className="font-weight-500 ml-1">{"Contact us"}</Link>     
                          </p>
                        </div>
                      </Form>
                    </div>
                  </div>
                </div>
              )}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isError: state.loginReducer.isError,
  errorMessage: state.errorReducer.errorMessage,
  forgotPasswordMailSent: state.loginReducer.forgotPasswordMailSent,
  isAuthorized: state.loginReducer.isAuthorized
});

const mapDispatchToProps = dispatch => ({
  forgetPasswordMail: data => dispatch(forgetPasswordMail(data)),
  clearAuthFlag: () => dispatch(clearAuthorizationFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(SendResetLink);
