import React, { Component } from "react";
import { connect } from "react-redux";
import ReactTable from "react-table";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";

import { confirmSubmitHandler } from "../../helpers/confirmationPopup";
import {
  getUserBoatShows,
  clearBoatShowFlag,
  deleteBoatShow
} from "../../redux/actions/boatShowAction";
import moment from "moment";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import { pagination } from "../../util/enums/enums";
import { viewBoatHandler } from "../../helpers/boatHelper";
import "../../styles/manageDashboardTableResponsive.scss";

class ManageBoatShows extends Component {
  componentDidMount() {
    const { getUserBoatShows } = this.props;
    getUserBoatShows({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    });
  }

  static getDerivedStateFromProps(props, state) {
    const { clearBoatShowFlag, success } = props;
    if (success) {
      clearBoatShowFlag();
    }
  }

  editBoatShow = record => {
    const { history } = this.props;
    history.push("/add-boat-show", { record });
  };

  render() {
    const { deleteBoatShow } = this.props;
    const columns = [
      {
        Header: "Boat Show Title",
        accessor: "title",
        Cell: data => (
          <div className="boatShow-action-button justify-content-start">
            <span>{data.original.title}</span>
          </div>
        )
      },
      {
        Header: "Ad ID",
        accessor: "adId",
        Cell: data => (
          <div className="boatShow-action-button justify-content-start">
            <span>{data.original.adId}</span>
          </div>
        )
      },
      {
        Header: "Place",
        accessor: "country",
        Cell: data => (
          <div className="boatShow-action-button justify-content-start">
            <span>
              {data.original.city}, {data.original.country}
            </span>
          </div>
        )
      },
      {
        Header: "Duration",
        accessor: "availableTripDays",
        Cell: data => {
          let date;
          if (
            moment(data.original.endDate).diff(
              moment(data.original.startDate),
              "days"
            ) === 0
          ) {
            date =
              moment(data.original.endDate)
                .diff(moment(data.original.startDate), "hours")
                .toString() + "Hours";
          } else {
            date =
              moment(data.original.endDate)
                .diff(moment(data.original.startDate), "days")
                .toString() + "Days";
          }
          return (
            <div className="boatShow-action-button">
              <span>{date}</span>
            </div>
          );
        }
      },
      {
        Header: "Created At",
        accessor: "createdAt",
        Cell: data => (
          <div className="boatShow-action-button justify-content-start">
            <span>
              {moment(data.original.createdAt).format("DD-MM-YYYY hh:mm a")}
            </span>
          </div>
        )
      },
      {
        Header: "Actions",
        Cell: row => (
          <div className="d-flex flex-row justify-content-around action">
            <button
              type="button"
              className="btn btn-outline-success mr-2"
			  onClick={() => this.editBoatShow(row.original.id)}
            >
              Edit
            </button>

            <button
              type="button"
              className="btn btn-outline-info mr-2"
			  onClick={() => viewBoatHandler(row.original, history)}
            >
              View
            </button>

            <button
              type="button"
              className="btn btn-outline-danger mr-2"
              onClick={() =>
                confirmSubmitHandler(deleteBoatShow, row.original.id)
              }
            >
              Delete
            </button>
            
          </div>
        )
      }
    ];

    const { history, boatShows } = this.props;
    return (
      <DashboardLayout>
        <div className="row pl-3 pr-3 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Manage Boat Shows</h4>
                <button
                  type="button"
                  class="btn btn-primary primary-button"
                  onClick={() => {
                    history.push("/add-boat-show");
                  }}
                >
                  Add Boat Show
                </button>
              </div>
              <div className="card-body mt-0">
                {boatShows && boatShows.length > 0 && (
                  <div className="table-responsive">
                    <ReactTable
                      columns={columns}
                      data={boatShows}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  boatShows: state.boatShowReducer && state.boatShowReducer.boatShows,
  success: state.boatShowReducer && state.boatShowReducer.success
});

const mapDispatchToProps = dispatch => ({
  getUserBoatShows: data => dispatch(getUserBoatShows(data)),
  clearBoatShowFlag: () => dispatch(clearBoatShowFlag()),
  deleteBoatShow: data => dispatch(deleteBoatShow(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageBoatShows);
