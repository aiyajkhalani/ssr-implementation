import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ImageUploader from 'react-images-upload';
import 'react-dates/initialize';
import * as moment from 'moment';
import Datetime from 'react-datetime';
import 'react-dates/lib/css/_datepicker.css';
import {
	Button, Box, Dialog,
	DialogTitle,
	DialogContent,
	DialogContentText
} from '@material-ui/core';
import { Row, Col, Container, Form, Card } from 'react-bootstrap';

import GoogleMap from '../../components/map/map';
import { randomAdId, requireMessage } from '../../helpers/string';
import { Field, Loader } from '../../components';
import { uploadToS3 } from '../../helpers/s3FileUpload';
import {
	createBoatShow,
	clearBoatShowFlag,
	getBoatShowById,
	updateBoatShow,
	clearEditBoatShow,
} from '../../redux/actions/boatShowAction';
import { dateStringFormate, valid } from '../../util/utilFunctions';
import { ErrorNotify, InfoNotify } from '../../helpers/notification';
import { DashboardLayout } from '../../components/layout/dashboardLayout';

import './boatShow.scss';
import ErrorComponent from '../../components/error/errorComponent';
import { clearErrorMessageShow } from '../../redux/actions';

class AddBoatShow extends Component {
	constructor(props) {
		super(props);
		this.state = {
			adId: '',
			boatShow: {
				country: '',
				city: '',
				title: '',
				startDate: '',
				endDate: '',
				showLink: '',
				showDescription: '',
				showLogo: '',
				address: ''
			},
			editBoatId: props.location && props.location.state && props.location.state.record,
			location: {},
			latLng: { lat: 23.0225, lng: 72.5714 },
			mediaUpdated: false,
			agree: false,
			termsModal: false,
		};
	}
	componentDidMount() {
		const { editBoatId } = this.state;
		const { getBoatShowById } = this.props;
		if (editBoatId) {
			getBoatShowById(editBoatId);
		}
	}
	fetchMapInfo = (result, setValue) => {
		const { location } = this.state;
		const { address, country, state, city, placeName, postalCode, latitude, longitude } = result;
		location.address = address;
		location.country = country;
		location.state = state;
		location.city = city;
		location.placeName = placeName;
		location.postalCode = postalCode;
		location.latitude = latitude;
		location.longitude = longitude;

		setValue('address', address)
	};


	onClickAgree = () => {
		this.setState(prevState => ({
			agree: !prevState.agree
		}));
	};


	static getDerivedStateFromProps(props, state) {
		const { clearBoatShowFlag, createSuccess, history, updateSuccess, getSuccess, clearEditBoatShow, createError } = props;
		const { adId } = state;
		if (createSuccess) {
			clearBoatShowFlag();
			history.push('/manage-boat-shows');
		} else if (getSuccess) {
			clearBoatShowFlag();
		} else if (!adId) {
			return {
				adId: randomAdId('BSH'),
			};
		}
		if (updateSuccess) {
			clearEditBoatShow()
			clearBoatShowFlag();
			history.push('/manage-boat-shows');
		}
	}

	cancelHandler = () => {
		const { history, clearEditBoatShow } = this.props;
		clearEditBoatShow();
		history.push('/manage-boat-shows');
	};

	prepareValue = () => {
		const { editBoatShow } = this.props;
		const { boatShow, mediaUpdated, adId, location, editBoatId } = this.state;

		if (editBoatId && editBoatShow.hasOwnProperty('id')) {
			if (!mediaUpdated) {
				const {
					title,
					startDate,
					endDate,
					showLink,
					showDescription,
					showLogo,
					address,
					country,
					state,
					city,
					placeName,
					postalCode,
					geometricLocation,
				} = editBoatShow;

				location.address = address;
				location.country = country;
				location.state = state;
				location.city = city;
				location.placeName = placeName;
				location.postalCode = postalCode;
				location.latitude = geometricLocation && geometricLocation.coordinates.length > 0 && geometricLocation.coordinates[1];
				location.longitude = geometricLocation && geometricLocation.coordinates.length > 0 && geometricLocation.coordinates[0];

				boatShow.title = title;
				boatShow.startDate = startDate;
				boatShow.endDate = endDate;
				boatShow.showLink = showLink;
				boatShow.showDescription = showDescription;
				boatShow.showLogo = showLogo;
				boatShow.showLogo = showLogo;

				this.setState({
					boatShow,
					location,
					latLng: { lat: location.latitude, lng: location.longitude },
					adId,
					mediaUpdated: true,
				});
			}
			// delete extra field from boat object
			const { createdAt, __typename, ...filteredEditBoatShow } = editBoatShow

			return {
				...filteredEditBoatShow,
			};
		}
	};

	render() {
		const { boatShow, latLng, adId, editBoatId, location } = this.state;
		const { createBoatShow, updateBoatShow, editBoatShow, isLoading, createError, errorMessage } = this.props;
		const initValue = editBoatId && editBoatShow ? editBoatShow.hasOwnProperty('id') && this.prepareValue() : boatShow;

		return (
			<DashboardLayout>
				<Container fluid>
					{editBoatId && !editBoatShow.hasOwnProperty("id") ? (
						<Loader></Loader>
					) : (
							<Formik
								initialValues={initValue}
								onSubmit={values => {
									const { location, boatShow } = this.state;
									const { clearErrorMessageShow } = this.props

									clearErrorMessageShow()
									if (!Object.keys(location).length) {
										InfoNotify('please add location');
									} else {
										if (new Date(values.startDate) > new Date(values.endDate)) {
											ErrorNotify('Please select valid dates');
										} else {
											values.address = location.address;
											values.country = location.country;
											values.state = location.state;
											values.city = location.city;
											values.placeName = location.placeName;
											values.postalCode = location.postalCode;
											values.geometricLocation = {
												coordinates: [location.longitude, location.latitude]
											}
											if (editBoatId) {
												updateBoatShow(values);
											} else {
												values.adId = adId;
												createBoatShow(values);
											}
										}
									}
								}}
								validationSchema={
									Yup.object().shape({
									address: Yup.string().required(requireMessage('Address')),
									title: Yup.string().required(requireMessage('Boat Show Title')),
									showLink: Yup.string().required(requireMessage('Boat Show Link')),
									showLogo: Yup.string().required(requireMessage('Boat Show Logo')),
									startDate: Yup.date().required(requireMessage('Start Date')),
									endDate: Yup.date().min(Yup.ref('startDate'), 'End date must be later than start date.').required(requireMessage('End Date'))
								})}
								render={({ errors, status, touched, values, setFieldValue, handleSubmit }) => (
									
									<Form>
										<div className="pl-2 pt-3 pb-3 map-div map-title-bg">
											<Box
												fontSize={20}
												letterSpacing={1}
												fontWeight={600}
												className="map-title">
												{`Boat Show Location`}
												<span className="font-weight-400 font-14">
													Ad ID: {editBoatId ? editBoatShow.adId : adId}
												</span>
											</Box>
										</div>
										<Row>
											<Col xs={6} className="">
												<div className="add-boat-map add-boat-show-map">
													<GoogleMap
														latLng={latLng}
														fetch={result => this.fetchMapInfo(result, setFieldValue)}
														height={25}
														width={100}
														placeHolder="Boat Address"
														columnName={'address'}
														value={location}
														isError={errors.address}
														isUpdate></GoogleMap>
												</div>
											</Col>
											<Col xs={6} className="pl-0">
												<Card className="boatShow-body h-100 m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2">
													<Row className="boatShow-container addBoatShow-fields pt-3 pb-3 mt-auto mb-auto">
														<Col>
															<div className="boatShow-field mt-0">
																<Field
																	label="Boat Show Title"
																	id={'title'}
																	name={'title'}
																	type="text"
																	value={values.title}
																	onChangeText={e => {
																		setFieldValue('title', e.target.value);
																	}}
																	required
																/>
																<ErrorMessage component="div" name="title" className="error-message" />
															</div>
															<div className="boatShow-field">
																<Field
																	label="Boat Show Link"
																	id={'showLink'}
																	name={'showLink'}
																	type="text"
																	value={values.showLink}
																	onChangeText={e => {
																		setFieldValue('showLink', e.target.value);
																	}}
																	required
																/>
																<ErrorMessage component="div" name="showLink" className="error-message" />
															</div>
															<div className="boatShow-field">
																<Field
																	label="Boat Show Description"
																	id={'showDescription'}
																	name={'showDescription'}
																	type="textarea"
																	value={values.showDescription}
																	onChangeText={e => {
																		setFieldValue('showDescription', e.target.value);
																	}}
																/>
															</div>
															<ErrorMessage component="div" name="showDescription" className="error-message" />
															<div className="boatShow-field">
																<label className="required">Start Date</label>
																<Datetime
																	value={editBoatId ? moment(values.startDate).format("DD/MM/YYYY hh:mm a") : ''}
																	closeOnSelect
																	isValidDate={valid}
																	onChange={value => {
																		setFieldValue('startDate', dateStringFormate(value));
																	}}
																/>
																<ErrorMessage component="div" name="startDate" className="error-message" />
															</div>
															<div className="boatShow-field">
																<label className="required">End Date</label>
																<Datetime
																	isValidDate={valid}
																	value={editBoatId ? moment(values.endDate).format("DD/MM/YYYY hh:mm a") : ''}
																	closeOnSelect
																	onChange={value => {
																		setFieldValue('endDate', dateStringFormate(value));
																	}}
																/>
																<ErrorMessage component="div" name="endDate" className="error-message" />
															</div>
														</Col>
														<Col>
															<div className="d-flex flex-wrap justify-content-center width-100 m-auto h-100">
																<div className="text-center m-auto">
																	<label className="required">Boat Show Logo</label>
																	<Field
																		id="showLogo"
																		name="showLogo"
																		type="single-image"
																		value={values.showLogo}
																		onChangeText={setFieldValue}
																	/>
																	<ErrorMessage
																		component="div"
																		name="showLogo"
																		className="error-message"
																	/>
																</div>
															</div>
														</Col>
													</Row>
													<Row></Row>
												</Card>
											</Col>
										</Row>
										<br />
										{createError && <ErrorComponent errors={errorMessage} />}
										<div className="d-flex justify-content-center">
											<Button
												type="button"
												className="button btn btn-primary w-auto addBoatService-btn primary-button"
												onClick={handleSubmit}>
												Save
										</Button>
											<Button
												type="button"
												className="profile-button btn-dark" onClick={this.cancelHandler}
											>
												Cancel
										</Button>
										</div>
									</Form>
								)}></Formik>
						)}
				</Container>

			</DashboardLayout>
		);
	}
}

const mapStateToProps = state => ({
	createSuccess: state.boatShowReducer && state.boatShowReducer.createSuccess,
	createError: state.boatShowReducer && state.boatShowReducer.createError,
	editBoatShow: state.boatShowReducer && state.boatShowReducer.editBoatShow,
	updateSuccess: state.boatShowReducer && state.boatShowReducer.updateSuccess,
	getSuccess: state.boatShowReducer && state.boatShowReducer.getSuccess,
	isLoading: state.boatShowReducer && state.boatShowReducer.isLoading,
	errorMessage: state.errorReducer.errorMessage,
});

const mapDispatchToProps = dispatch => ({
	clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
	createBoatShow: data => dispatch(createBoatShow(data)),
	clearBoatShowFlag: () => dispatch(clearBoatShowFlag()),
	getBoatShowById: data => dispatch(getBoatShowById(data)),
	updateBoatShow: data => dispatch(updateBoatShow(data)),
	clearEditBoatShow: () => dispatch(clearEditBoatShow()),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddBoatShow);
