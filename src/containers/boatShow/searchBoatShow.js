import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Layout,
  PaginationBar
} from "../../components";
import UserContext from "../../UserContext";
import BoatListingsWithMap from "../../components/gridComponents/BoatListingsWithMap";
import { searchBoatShow } from "../../graphql/boatShowSchema";
import { pagination } from "../../util/enums/enums";

class SearchRentBoats extends Component {

  state = {
    showMap: false,
  };

  static contextType = UserContext;

  toggleMapHandler = () => {
    this.setState(prevState => ({ showMap: !prevState.showMap }));
  };

  render() {
    const { showMap } = this.state;
    const { searchedBoatShowData, totalSearchedBoatShowData } = this.props;
    const { country: countryName } = this.context;

    return (
      <Layout isFooter>
        <div className={`${showMap ? "is-show-map" : ""} w-100`}>
          <BoatListingsWithMap
            isBoatShow
            showMap={showMap}
            isNotHome={false}
            toggleMapHandler={this.toggleMapHandler}
            boats={searchedBoatShowData}
            boatsType="Searched Boat Shows"
            boatsTypeCount={totalSearchedBoatShowData}
          />

          {totalSearchedBoatShowData > pagination.PAGE_RECORD_LIMIT && (
            <div className="boat-pagination">
              <PaginationBar
                action={searchBoatShow}
                value={{ countryName }}
                totalRecords={totalSearchedBoatShowData}
              />
            </div>
          )}
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  searchedBoatShowData: state.boatShowReducer.searchedBoatShowData,
  totalSearchedBoatShowData: state.boatShowReducer.totalSearchedBoatShowData,
});

const mapDispatchToProps = dispatch => ({
  searchBoatShow: data => dispatch(searchBoatShow(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchRentBoats);
