import React, { Component } from "react";
import { CardContent, Card } from "@material-ui/core";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import SearchComplete from "../../components/map/SearchComplete";
import { BoatShowBannerFormStyle } from "../../components/styleComponent/styleComponent";

import "../../containers/boatServices/boatServices.scss";

class BoatShowSearchCard extends Component {
  setPlaceHandler = ({ country, city }, setFieldValue) => {
    setFieldValue("country", country);
    setFieldValue("city", city);
  };

  getNewWidth = () => {
    const width = document.querySelector("body");
    const actualWidth = width && width.offsetWidth / 3
    return actualWidth;
  };

  render() {
    const { search } = this.props;

    const width = this.getNewWidth()

    return (
      <Formik
        initialValues={{
          country: "",
          city: "",
          query: ""
        }}
        // validationSchema={Yup.object().shape({
        //   country: Yup.string().required("Please add any place"),
        //   city: Yup.string().required("Please add any place"),
        //   query: Yup.string().required("query is required")
        // })}
        onSubmit={values => {
          search(values);
        }}
        render={({ values, setFieldValue, handleSubmit }) => (
          <>
            <Form className="bg-transparent-form" onSubmit={e => e.preventDefault()}>
              <BoatShowBannerFormStyle bgWidth={width}>
                <div>
                  <Card className="profile-container card p-0 br-10">
                    <CardContent className="p-4">
                      <div className="boat-show-banner-card-info-div p-2">
                        <div className="pb-1 boat-show-banner-card-info-grid mb-2">

                          <SearchComplete
                            placeHolder="Where would you like to search?"
                            className="form-control font-14 boat-show-banner-form-field"
                            getPlaceName={place =>
                              this.setPlaceHandler(place, setFieldValue)
                            }
                          />
                          <ErrorMessage
                            component="div"
                            name={"city" || "country"}
                            className="error-message"
                          ></ErrorMessage>
                        </div>

                        <div className="pb-1 boat-show-banner-card-info-grid mt-2">

                          <input
                            id="query"
                            name="query"
                            type="text"
                            className="form-control font-14 boat-show-banner-form-field"
                            placeholder="City you're looking for?"
                            onChange={e => setFieldValue("query", e.target.value)}
                            required
                          />
                          <ErrorMessage
                            component="div"
                            name="query"
                            className="error-message"
                          ></ErrorMessage>
                        </div>

                      <div className="d-flex justify-content-between flex-row-reverse mt-3 mb-0">
                        <button
                          type="button"
                          className={`btn font-weight-600 search-background boat-show-banner-form-search common-hovered-blue-button-effect text-white search-button-width`}
                          onClick={handleSubmit}
                        >
                          Search Now
                        </button>
                          <br />
                          <a
                            className="btn btn-link-info float-right notify-color pl-0 pr-0"
                            href="#"
                          >
                            <u className="font-12 boat-show-banner-form-notify font-weight-500 boatshow-notify">
                              NOTIFY ME
                          </u>
                          </a>
                        </div>
                      </div>
                    </CardContent>
                  </Card>
                </div>
              </BoatShowBannerFormStyle>
            </Form>
          </>
        )}
      />
    );
  }
}

export default BoatShowSearchCard;
