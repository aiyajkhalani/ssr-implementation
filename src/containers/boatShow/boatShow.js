import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Grid, Box } from "@material-ui/core";

import { Layout } from "../../components/layout/layout";
import ShowBoatCards from "../../components/gridComponents/showBoatCards";

import {
  getAllBoatShow,
  searchBoatShow
} from "../../redux/actions/boatShowAction";
import {
  pagination,
  moduleCategory,
  mediaCategory,
  dimension
} from "../../util/enums/enums";
import { getCategoryWiseVideos } from "../../redux/actions/VideoAction";
import UserContext from "../../UserContext";

import "./boatShow.scss";
import { getModuleWiseBanners } from "../../redux/actions";
import CommonBanner from "../../components/mainBanner/commonBanner";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import ServicesMarinaStorage from "../../components/staticComponent/servicesMarinaStorage";
import BoatShowSearchCard from "./boatShowSearchCard";
import { VideoComponent } from "../../components/videoComponent/videoComponent";
import { VideoModel } from "../../components/videoComponent/videoModel";
import "../../../src/styles/boatShowResponsive.scss";
class BoatShow extends Component {
  state = {
    setVideoFlag: false,
    videoUrl: "",
    videoThumbnail: "",
    videoWidth: dimension.boatShowVideo.width,
    videoHeight: dimension.boatShowVideo.height
  };

  static contextType = UserContext;

  static getDerivedStateFromProps(nextProps) {
    const { isBoatShowSearched, history } = nextProps;

    if (isBoatShowSearched) {
      history.push("/search-boat-show");
    }
    return null;
  }

  componentDidMount() {
    const { country } = this.context;
    const {
      getAllBoatShow,
      getCategoryWiseVideos,
      getModuleWiseBanners
    } = this.props;

    getModuleWiseBanners({
      type: mediaCategory.boatShow.type,
      fieldName: mediaCategory.boatShow.fieldName,
      isBanner: mediaCategory.boatShow.isBanner
    });

    getCategoryWiseVideos({
      type: moduleCategory.BOAT_SHOW,
      metatype: "video"
    });
    const input = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT,
      country: country
    };

    const videoWidth = getRatio(dimension, "boatShowVideo", ".section-heading");

    const videoHeight = getHeightRatio(
      dimension,
      "boatShowVideo",
      ".section-heading"
    );

    this.setState({ videoWidth, videoHeight });

    getAllBoatShow(input);
  }

  playVideo = video => {
    this.setState(prevState => ({
      setVideoFlag: !prevState.setVideoFlag,
      videoUrl: video.url,
      videoThumbnail: video.thumbnail
    }));
  };

  closeVideo = () => {
    this.setState({setVideoFlag:false})
  }
  render() {
    const {
      allBoatShows,
      categoryVideos,
      boatShowBanner,
      searchBoatShow
    } = this.props;
    const {
      setVideoFlag,
      videoUrl,
      videoThumbnail,
      videoWidth,
      videoHeight
    } = this.state;

    return (
      <Layout isHeader="boat-show-logo" className="boatshow-layout-page">
        <div className="row width-100 ml-0">
          <div className="col hidden position-relative border-solid-gray pr-0 pl-0">
            {boatShowBanner && boatShowBanner.length > 0 && (
              <CommonBanner data={boatShowBanner} />
            )}
            <div className="boat-show-search-card">
              <BoatShowSearchCard search={searchBoatShow} />
            </div>
          </div>
        </div>

        <hr className="m-0" />

        <div className="container100 boatShowMainDiv">
          <Grid container>

            <Grid item xs={12}>
              <Box
                className="section-heading"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                Explore AdamSea
              </Box>
              <ServicesMarinaStorage className="mb-20" />
            </Grid>

            {allBoatShows && allBoatShows.length > 0 && (
              <Grid item xs={12} className="mt-80 boat-show-main-div">
                <ShowBoatCards
                  xs={3}
                  sm={3}
                  isShowAll
                  showBoatCards={allBoatShows}
                  showBoatBtn="boatShow-btnBg"
                  showAllText="boat show images"
                />

              </Grid>
            )}

            {categoryVideos && categoryVideos.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                ></Box>

                <Grid item xs={12} className="mb-5">
                  <Grid container spacing={2}>
                    {categoryVideos &&
                      categoryVideos.map((video, index) => {
                        if (index < 3) {
                          return (
                            <Fragment key={video.id}>
                              <VideoComponent
                                className="boat-show-video-section"
                                setVideoUrlHandler={() => this.playVideo(video)}
                                video={video}
                                videoWidth={videoWidth}
                                videoHeight={videoHeight}
                              />
                            </Fragment>
                          );
                        }
                      })}
                  </Grid>
                  <VideoModel
                    videoFlag={setVideoFlag}
                    videoUrl={videoUrl}
                    thumbnail={videoThumbnail}
                    closeVideo={this.closeVideo}
                  />
                </Grid>
              </Grid>
            )}

          </Grid>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  allBoatShows: state.boatShowReducer && state.boatShowReducer.allBoatShows,
  categoryVideos: state.videoReducer && state.videoReducer.categoryVideos,
  allBoatShowCount:
    state.boatShowReducer && state.boatShowReducer.allBoatShowCount,
  boatShowBanner:
    state.dashboardReducer &&
    state.dashboardReducer[mediaCategory.boatShow.fieldName],
  isBoatShowSearched: state.boatShowReducer.isBoatShowSearched
});

const mapDispatchToProps = dispatch => ({
  getAllBoatShow: value => dispatch(getAllBoatShow(value)),
  getCategoryWiseVideos: data => dispatch(getCategoryWiseVideos(data)),
  getModuleWiseBanners: type => dispatch(getModuleWiseBanners(type)),
  searchBoatShow: data => dispatch(searchBoatShow(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(BoatShow);
