import React, { Component } from "react";
import { connect } from "react-redux";

import { Layout, BoatManufacturerProfile, MarinaAndStorageProfile, BoatOwnerProfile, BrokerDealerProfile, BoatServiceProfile, ShipperProfile, SurveyorProfile, RentAndCharterProfile, MemberProfile } from "../../components";
import { getAllStates, updateUser } from "../../redux/actions";
import { SuccessNotify, ErrorNotify } from "../../helpers/notification";

import "./profile.scss";
import { userRoles } from "../../util/enums/enums";
import { DashboardLayout } from "../../components/layout/dashboardLayout";

class Profile extends Component {
  state = {};

  async componentDidMount() {
    const { getAllStates } = this.props;
    await getAllStates();
  }

  static getDerivedStateFromProps(nextProps) {
    const { isUpdate, isUpdateError } = nextProps;
    if (isUpdate) {
      setTimeout(() => SuccessNotify("profile updated"), 100);
      nextProps.history.push("/dashboard");
    } else if (isUpdateError) {
      ErrorNotify("invalid inputs.");
    }
    return null;
  }

  cancelHandler = () => {
    const { history } = this.props;
    history.push("/dashboard");
  };

  profileRenderHandler = (role, initValue) => {

    const { updateUser } = this.props;

    const latLng = {
      lat: 23.0225, lng: 72.5714
    }

    let ProfileComponent

    switch (role) {
      case userRoles.MEMBER:
        ProfileComponent = MemberProfile
        break;
      case userRoles.AGENT:
        ProfileComponent = BoatServiceProfile
        break;
      case userRoles.BOAT_OWNER:
        ProfileComponent = BoatOwnerProfile
        break;
      case userRoles.BROKER_AND_DEALER:
        ProfileComponent = BrokerDealerProfile
        break;
      case userRoles.BOAT_MANUFACTURER:
        ProfileComponent = BoatManufacturerProfile
        break;
      case userRoles.SURVEYOR:
        ProfileComponent = SurveyorProfile
        break;
      case userRoles.YACHT_SHIPPER:
        ProfileComponent = ShipperProfile
        break;
      case userRoles.SERVICE_AND_MAINTENANCE:
        ProfileComponent = BoatServiceProfile
        break;
      case userRoles.MARINA_AND_STORAGE:
        ProfileComponent = MarinaAndStorageProfile
        break;
      case userRoles.RENT_AND_CHARTER:
        ProfileComponent = RentAndCharterProfile
        break;

      default:
        break;
    }

    return <ProfileComponent initValue={initValue} updateUser={updateUser} cancelHandler={this.cancelHandler} latLng={latLng} userType={role} />

  }

  render() {
    const { states, currentUser } = this.props;

    const {
      id,
      firstName,
      middleName,
      lastName,
      country,
      mobileNumber,
      email,
      address1,
      address2,
      street,
      city,
      zip,
      postBox,
      state,
      holderName,
      accountNumber,
      bankName,
      transitCode,
      branchName,
      instituteNumber,
      branchAddress,
      swiftCode,
      addressOfAccountHolder,
      secondContactFullName,
      secondContactEmail,
      companyName,
      companyWebsite,
      preference,
      title,
      language,
      companyLogo,
      image,
      officeAddress,

      officeCity,
      officeState,
      officeCountry,
      officeRoute,
      officePlaceName,
      officePostalCode,
      commercialLicence,
      governmentId
    } = currentUser;

    let { officeLocation } = currentUser

    if (!officeLocation || officeLocation === null) {
      officeLocation = {
        __typename: null,
        coordinates: null
      }
    }

    const { __typename, ...branchLocation } = officeLocation;

    const initValue = {
      id: id,
      firstName: firstName,
      middleName: middleName || "",
      lastName: lastName || "",
      country: country,
      mobileNumber: mobileNumber || "",
      email: email,
      address1: address1 || "",
      address2: address2 || "",
      street: street || "",
      city: city || "",
      zip: zip || "",
      postBox: postBox || "",
      state: state || "",
      holderName: holderName || "",
      accountNumber: accountNumber || "",
      bankName: bankName || "",
      transitCode: transitCode || "",
      branchName: branchName || "",
      instituteNumber: instituteNumber || "",
      branchAddress: branchAddress || "",
      swiftCode: swiftCode || "",
      addressOfAccountHolder: addressOfAccountHolder || "",
      companyName: companyName || "",
      companyWebsite: companyWebsite || "",
      companyLogo: companyLogo || "",
      title: title || "",
      language: language || "",
      preference: preference || "",
      secondContactFullName: secondContactFullName || "",
      secondContactEmail: secondContactEmail || "",
      image: image || "",
      officeAddress: officeAddress || "",
      officeLocation: branchLocation || null,
      officeCity: officeCity || "",
      officeState: officeState || "",
      officeCountry: officeCountry || "",
      officeRoute: officeRoute || "",
      officePlaceName: officePlaceName || "",
      officePostalCode: officePostalCode || "",
      governmentId: governmentId || '',
      commercialLicence: commercialLicence || "",
    }


    return (
      <DashboardLayout>
        {currentUser && currentUser.role && currentUser.role.aliasName && this.profileRenderHandler(currentUser.role.aliasName, initValue)}
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  states: state.stateReducer.states,
  currentUser: state.loginReducer.currentUser,
  isUpdate: state.loginReducer.isUpdate,
  isUpdateError: state.loginReducer.isUpdateError
});

const mapDispatchToProps = dispatch => ({
  getAllStates: () => dispatch(getAllStates()),
  updateUser: data => dispatch(updateUser(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
