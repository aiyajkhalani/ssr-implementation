import React, { Component } from "react";
import { connect } from "react-redux";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import { clearForgotPassword, resetPassword } from "../../redux/actions";
import { SuccessNotify } from "../../helpers/notification";
import "./profile.scss";
import { cancelHandler } from "../../helpers/routeHelper";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import ErrorComponent from "../../components/error/errorComponent";
import { renderErrorMessage } from "../../helpers/jsxHelper";

class ChangePassword extends Component {
  static getDerivedStateFromProps(nextProps) {
    if (nextProps.isPasswordReset) {
      setTimeout(() => {
        SuccessNotify('Reset Password Successfully')
      }, 1000);
      nextProps.history.push('/dashboard')
    }
    return null;
  }

  state = {
    isDisplayHint: false,
    errorMessages: {},
  }

  validate = (e, setValue, email) => {
    const { errorMessages } = this.state;
    let password = e.target.value;
    let capsCount,
      smallCount,
      symbolCount,
      numberCount,
      userEmail,
      uniqPassword;
    if (password.length < 8) {
      errorMessages.passwordLength = true;
    } else {
      errorMessages.passwordLength = false;
    }
    userEmail = email.toLowerCase();
    uniqPassword = password.toLowerCase();
    capsCount = (password.match(/[A-Z]/g) || []).length;
    smallCount = (password.match(/[a-z]/g) || []).length;
    symbolCount = (password.match(/\W/g) || []).length;
    numberCount = (password.match(/[0-9]/g) || []).length;

    if (capsCount < 1) {
      errorMessages.capsCount = true;
    } else {
      errorMessages.capsCount = false;
    }
    if (smallCount < 1) {
      errorMessages.smallCount = true;
    } else {
      errorMessages.smallCount = false;
    }
    if ((symbolCount && numberCount) < 1) {
      errorMessages.symbolOrNumberCount = true;
    } else {
      errorMessages.symbolOrNumberCount = false;
    }

    this.setState({
      errorMessages
    });
    this.measureStrength(password, setValue);
  };

  measureStrength = (password, setValue) => {
    let score = 0;
    let passwordStrength;
    let regexPositive = ["[A-Z]", "[a-z]", "[0-9]", "\\W"];
    regexPositive.forEach((regex, index) => {
      if (new RegExp(regex).test(password)) {
        score += 1;
      }
    });
    switch (score) {
      case 1:
        passwordStrength = "poor";
        break;

      case 2:
        passwordStrength = "poor";
        break;

      case 3:
        passwordStrength = "average";
        setValue("password", password);
        break;

      case 4:
        passwordStrength = "strong";
        setValue("password", password);
        break;

      default:
        passwordStrength = "weak";
    }
    this.setState({
      passwordStrength,
      isDisplayHint: true
    });
  };


  render() {
    const { resetPassword, history, errorMessage, isError } = this.props;
    const { isDisplayHint, errorMessages, passwordStrength } = this.state
    return (
      <DashboardLayout>
        <Formik
          initialValues={{
            oldPassword: "",
            newPassword: ""
          }}
          validationSchema={Yup.object().shape({
            oldPassword: Yup.string()
              .required("Old Password is required"),
            newPassword: Yup.string()
              .required("New Password is required"),
            confirmPassword: Yup.string()
              .oneOf([Yup.ref("newPassword"), null], "Passwords must match")
              .required("Confirm Password is required")
          })}
          onSubmit={values => {

            if (
              !errorMessages.symbolOrNumberCount &&
              !errorMessages.passwordLength &&
              passwordStrength !== "poor"
            ) {
              resetPassword({ oldPassword: values.oldPassword, password: values.newPassword });
            }

          }}
          render={({ errors, status, touched, setFieldValue }) => (
            <>
              <div className="h-100 w-100 d-flex align-items-center justify-content-center">
                <div className="changePassword-div p-5 bg-white">
                  <h4>Change Password</h4>

                  <Form>
                    <div className="mb-2 width-100">
                      <label htmlFor="oldPassword" className="mb-0">
                        Old Password:
                    </label>

                      <Field
                        name="oldPassword"
                        type="password"
                        className={
                          "form-control" +
                          (errors.oldPassword && touched.oldPassword
                            ? " is-invalid"
                            : "")
                        }
                      />
                      <ErrorMessage
                        name="oldPassword"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    <div className="mb-2 width-100">
                      <label htmlFor="newPassword" className="mb-0">
                        New Password:
                    </label>
                      <Field
                        name="newPassword"
                        type="password"
                        onChange={(e) => {
                          setFieldValue("newPassword", e.target.value);
                          this.validate(e, setFieldValue, "");
                        }}
                        className={
                          "form-control" +
                          (errors.newPassword && touched.newPassword
                            ? " is-invalid"
                            : "")
                        }
                      />
                      {isDisplayHint && (
                        <div className="mt-2 register-password-hint">
                          {errorMessages && errorMessages.passwordLength
                            ? renderErrorMessage(
                              "At least 8 characters",
                              "error-message mt-0",
                              true
                            )
                            : renderErrorMessage(
                              "At least 8 characters",
                              "error-message-fullfill",
                              false
                            )}
                          {errorMessages &&
                            errorMessages.symbolOrNumberCount
                            ? renderErrorMessage(
                              "At least 1 symbol & Number",
                              "error-message mt-0",
                              true
                            )
                            : renderErrorMessage(
                              "At least 1 symbol & Number",
                              "error-message-fullfill",
                              false
                            )}
                          {passwordStrength && passwordStrength === "poor"
                            ? renderErrorMessage(
                              `Password Strength: ${passwordStrength}`,
                              "error-message mt-0",
                              true
                            )
                            : renderErrorMessage(
                              `Password Strength: ${passwordStrength}`,
                              "error-message-fullfill",
                              false
                            )}
                          {errorMessages && errorMessages.capsCount
                            ? renderErrorMessage(
                              "password must contain one capital",
                              "error-message mt-0",
                              true
                            )
                            : renderErrorMessage(
                              "password must contain one capital",
                              "error-message-fullfill",
                              false
                            )}
                        </div>
                      )}
                    </div>
                    <div className="mb-4 width-100">
                      <label htmlFor="confirmPassword" className="mb-0">
                        Confirm Password:
                    </label>
                      <Field
                        name="confirmPassword"
                        type="password"
                        className={
                          "form-control" +
                          (errors.confirmPassword && touched.confirmPassword
                            ? " is-invalid"
                            : "")
                        }
                      />
                      <ErrorMessage
                        name="confirmPassword"
                        component="div"
                        className="invalid-feedback"
                      />
                    </div>
                    {isError && <ErrorComponent errors={errorMessage} />}
                    <div className="width-100 text-center">
                      <button
                        type="submit"
                        className="btn btn-primary mr-2 w-auto primary-button"
                      >
                        Change Password
                    </button>
                      <button type="reset" className="btn btn-secondary btn-dark" onClick={() => cancelHandler(history)} >
                        Cancel
                    </button>
                    </div>

                  </Form>
                </div>
              </div>
            </>
          )}
        />
      </DashboardLayout>
    );
  }
}
const mapStateToProps = state => ({
  isError: state.loginReducer.isError,
  errorMessage: state.errorReducer.errorMessage,
  forgotPasswordSuccess: state.loginReducer.forgotPasswordSuccess,
  isPasswordReset: state.loginReducer.isPasswordReset
});

const mapDispatchToProps = dispatch => ({
  resetPassword: data => dispatch(resetPassword(data)),
  clearForgotPassword: () => dispatch(clearForgotPassword())
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
