import React, { useEffect, useContext, useState, createRef, Fragment } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Box } from "@material-ui/core";

import { Layout } from "../../components/";
import {
  getUserRoles,
  verifyUserEmail,
  searchBoat,
  getHomeVideos,
  clearSearchBoatFlag,
  getBoatByType,
  getLatestBoats,
  getPopularBoats,
  getTopRatedBoats,
  getGlobalMinimumPriceBoats,
  clearCityWiseBoats,
  clearCategoryWiseBoats,
  getAllAuctionRooms,
  getBoatTypeStart,
  getModuleWiseBanners,
  getBoatTypeForManufacture,
  getBoatSearchMinimumValues
} from "../../redux/actions";
import FormCarousal from "../../components/staticComponent/FormCarousal";
import UserContext from "../../UserContext";
import { AuctionCarousel } from "../../components/carouselComponent/homePageCarousel/auctionCarousal";
import { BoatGrid } from "../../components/gridComponents/boatGrid";
import {
  pagination,
  moduleCategory,
  dimension,
  mediaCategory,
  advertisementCategory
} from "../../util/enums/enums";
import ManufacureCarousal from "../../components/carouselComponent/homePageCarousel/manufactureCarousel";
import { BoatTypeCarousel } from "../../components/carouselComponent/homePageCarousel/BoatTypeCarousel";
import SellAroundCards from "../../components/staticComponent/sellAroundCards";
import ServicesMarinaStorage from "../../components/staticComponent/servicesMarinaStorage";
import { getRatio, getHeightRatio } from "../../helpers/ratio";
import CommonBanner from "../../components/mainBanner/commonBanner";
import RegisterCard from "../../components/staticComponent/registerCard";
import { getCategoryWiseAdvertisements } from "../../redux/actions/advertisementAction";
import { VideoComponent } from "../../components/videoComponent/videoComponent";
import { VideoModel } from "../../components/videoComponent/videoModel";

import "./home.scss";
import "../../styles/responsive.scss";
const featuredBoatsCarousel = {
  isBrowser: 5,
  isTablet: 2,
  isMobile: 1
};

const mustBuyCarousel = {
  isBrowser: 5,
  isTablet: 2,
  isMobile: 1
};
const auctionCarouselItem = {
  isBrowser: 4,
  isTablet: 4,
  isMobile: 1
};
const ExploreAdamSeaCarousel = {
  isBrowser: 5,
  isTablet: 3,
  isMobile: 1
};

const useStyles = makeStyles(theme => ({
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default
  },
  img: {
    display: "block",
    maxWidth: "100vw",
    width: "100%"
  },
  bodyContainer: {
    height: "100%",
    maxWidth: "90vw",
    width: "90vw"
  },
  gridDiv: {
    textAlign: "center"
  },
  bottomMargin3: {
    marginBottom: "3rem"
  }
}));

const Home = props => {
  const {
    getUserRoles,
    history,
    currentUser,
    verifyUserEmail,
    isMailVerify,
    searchBoat,
    getHomeVideos,
    videos,
    isSearched,
    clearSearchBoatFlag,
    getBoatByType,
    getLatestBoats,
    getPopularBoats,
    getTopRatedBoats,
    featureBoats,
    bestDealBoats,
    mustBuyBoats,
    latestBoats,
    popularBoats,
    topRatedBoats,
    getGlobalMinimumPriceBoats,
    cityLists,
    clearCityWiseBoats,
    clearCategoryWiseBoats,
    getAllAuctionRooms,
    allAuctionRooms,
    getBoatTypeStart,
    getModuleWiseBanners,
    getCategoryWiseAdvertisements,
    homeBanner,
    boatTypes,
    advertisements,
    boatTypesForManufacture,
    getBoatTypeForManufacture,
    getBoatSearchMinimumValues,
    boatSearchValues
  } = props;

  const classes = useStyles();
  const mainRef = createRef()

  const { country } = useContext(UserContext);
  const [searchedValue, setSearchedValue] = useState(null);
  const [videoFlag, setVideoFlag] = useState(false);
  const [videoUrl, setVideoUrl] = useState("");
  const [videoThumbnail, setVideoThumbnail] = useState("");
  const [videoWidth, setVideoWidth] = useState(dimension.homePageVideo.width);
  const [videoHeight, setVideoHeight] = useState(
    dimension.homePageVideo.height
  );
  // const [screenScroll, onScreenScroll] = useState(false)


  useEffect(() => {
    const { location } = history;

    if (location && location.search && !currentUser.isActivated) {
      const urlParams = new URLSearchParams(location.search);

      if (urlParams.has("token")) {
        const token = urlParams.get("token");
        localStorage.setItem("token", token);
        verifyUserEmail({ token });
      }
    }
  }, [currentUser.isActivated, history, verifyUserEmail]);

  useEffect(() => {
    if (isMailVerify) {
      history.push("/");
    }
  }, [history, isMailVerify]);

  useEffect(() => {
    if (isSearched) {
      history.push({
        pathname: "/search-boats",
        search: ``
      });
      clearSearchBoatFlag();
    }
  }, [clearSearchBoatFlag, history, isSearched]);

  useEffect(() => {
    getUserRoles();
    getBoatByType({ input: { country, fieldName: "featureStatus" } });
    getBoatByType({ input: { country, fieldName: "bestDealStatus" } });
    getBoatByType({ input: { country, fieldName: "mustBuyStatus" } });
    getModuleWiseBanners({ type: mediaCategory.home.type, fieldName: mediaCategory.home.fieldName });
    getLatestBoats({ country });
    getCategoryWiseAdvertisements(advertisementCategory.HOME);
    getPopularBoats({ country });
    getTopRatedBoats({ country });
    getGlobalMinimumPriceBoats();
    getAllAuctionRooms({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    });
    getHomeVideos(moduleCategory.HOME);
    getBoatTypeStart();
    getBoatTypeForManufacture();
    const videoWidth = getRatio(
      dimension,
      "marinaStorageVideo",
      ".section-heading"
    );
    setVideoWidth(videoWidth);
    const videoHeight = getHeightRatio(
      dimension,
      "marinaStorageVideo",
      ".section-heading"
    );
    setVideoHeight(videoHeight);
    getBoatSearchMinimumValues()
  }, [country, getAllAuctionRooms, getGlobalMinimumPriceBoats, getBoatByType, getHomeVideos, getLatestBoats, getPopularBoats, getTopRatedBoats, getUserRoles,
    getBoatTypeStart, getModuleWiseBanners, getCategoryWiseAdvertisements, getBoatTypeForManufacture, getBoatSearchMinimumValues]);



  const getCityWiseBoatsHandler = value => {
    clearCityWiseBoats();
    value && window.open(`/city-wise-boats/${value.cityName}/${value.country}`, '_blank')

  };

  const getCategoryWiseBoatsHandler = value => {
    clearCategoryWiseBoats();
    value && window.open(`/category-wise-boats/${value && value && value.id}/${country}`, '_blank')
  };

  function onScroll() {
    const scrollY = window.scrollY //Don't get confused by what's scrolling - It's not the window
    const scrollTop = mainRef.current.scrollTop
  }
  const setVideoUrlHandler = (url, thumbnail) => {
    setVideoFlag(true);
    setVideoUrl(url);
    setVideoThumbnail(thumbnail);
  }
  const closeVideo = () => {
    setVideoFlag(false);
  }
  return (
    <Layout className="home-layout-page">
      <div ref={mainRef} className="home-page" onScroll={onScroll}>


        {homeBanner && homeBanner.length > 0 && (
          <div className="home-carousal">
            <CommonBanner data={homeBanner} />
            {boatTypes && boatTypes.length > 0 && (
              <FormCarousal
                searchBoat={searchBoat}
                setSearchedValue={() => setSearchedValue}
                boatTypes={boatTypes}
                boatSearchValues={boatSearchValues}
              />
            )}
          </div>
        )}

        <div className="container100 home-content">
          <Grid container>
            <Grid item xs={12}>
              <Box
                className="section-heading section-heading-first"
                fontSize={24}
                fontWeight={500}
                letterSpacing={0.5}
              >
                Explore AdamSea
                </Box>
              <ServicesMarinaStorage className="mb-20" />
            </Grid>

            {boatTypesForManufacture && boatTypesForManufacture.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  id="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Direct from the manufacture
                </Box>
                <ManufacureCarousal
                  items={ExploreAdamSeaCarousel}
                  carouselData={boatTypesForManufacture}
                  getCategoryWiseBoats={value =>
                    getCategoryWiseBoatsHandler(value)
                  }
                />
              </Grid>
            )}

            {featureBoats && featureBoats.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Featured Boats
                </Box>
                <BoatTypeCarousel
                  itemsLength={10}
                  items={featuredBoatsCarousel}
                  showType="featured-boats"
                  carouselData={featureBoats}
                  showAllText="feature boat"
                  isTopLabel
                  isBottomRating
                />
              </Grid>
            )}

            {cityLists && cityLists.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Boat for sell around the world
                </Box>
                <SellAroundCards
                  data={cityLists}
                  limit={5}
                  getCityWiseBoats={value => getCityWiseBoatsHandler(value)}
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 0 && (
              <Grid item xs={12} className="mt-md-5">
                <div>
                  <RegisterCard adData={advertisements[0]} />
                </div>
              </Grid>
            )}

            {bestDealBoats && bestDealBoats.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  AdamSea best deal recommended boats
                </Box>

                <BoatGrid
                  xs={12}
                  sm={3}
                  isTopLabel
                  route
                  showType="best-deal-boats"
                  itemsLength={8}
                  boatGrid={bestDealBoats}
                  showAllText="best recommended boats"
                />

              </Grid>
            )}

            {latestBoats && latestBoats.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  List of boats added to our market
                </Box>
                <BoatGrid
                  className="section-heading"
                  xs={12}
                  sm={3}
                  showType="recently-added-boats"
                  itemsLength={8}
                  boatGrid={latestBoats}
                  showAllText="boats added recently to our market"
                />
              </Grid>
            )}

            {advertisements && advertisements.length > 1 && (
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <div className="double-card">
                      <RegisterCard isSpace isDouble adData={advertisements[1]} />
                    </div>
                  </Grid>
                  {advertisements && advertisements.length > 2 && (
                    <Grid item xs={6}>
                      <div className="double-card">
                        <RegisterCard isSpace2 isDouble adData={advertisements[2]} />
                      </div>
                    </Grid>
                  )}
                </Grid>
              </Grid>
            )}

            {mustBuyBoats && mustBuyBoats.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Must Buy
                </Box>

                <BoatTypeCarousel
                  itemsLength={10}
                  items={mustBuyCarousel}
                  showType="must-buy-boats"
                  isMustBuy
                  carouselData={mustBuyBoats}
                  showAllText="must buy boats"
                  isTopLabel
                  isBottomRating
                />
              </Grid>
            )}

            {/* <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Top 20 Boats
                </Box>
                {topRatedBoats && (
                  <BoatGrid
                    showType="top-related-boats"
                    itemsLength={8}
                    xs={12}
                    sm={3}
                    boatGrid={topRatedBoats}
                  />
                )}
              </Grid> */}

            {popularBoats && popularBoats.length > 0 && (
              <Grid item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Best boats for sale in our market
                </Box>

                <BoatGrid
                  showType="most-popular-boats"
                  itemsLength={8}
                  xs={12}
                  sm={3}
                  isTopLabel
                  isBottomRating
                  boatGrid={popularBoats}
                  showAllText="most popular boats"
                  isCustomWidth
                />
              </Grid>
            )}

            {/* 
              Not remove below code. [NOTE] 
              <Grid item xs={12}>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={12}>
                    <CardHover isFlag={false} />
                  </Grid>
                </Grid>
              </Grid>
               */}

            {allAuctionRooms && allAuctionRooms.length > 0 && (
              <Grid className="auction-room" item xs={12}>
                <Box
                  className="section-heading"
                  fontSize={24}
                  fontWeight={500}
                  letterSpacing={0.5}
                >
                  Auction Room
                </Box>
                <AuctionCarousel
                  items={auctionCarouselItem}
                  carouselData={allAuctionRooms}
                  itemsLength={4}
                  showAllText="auction rooms"
                />
              </Grid>
            )}

          </Grid>

          {videos && videos.length > 0 && (
            <div className="container100">
              <Grid container>
                <Grid item xs={12}>
                  <Box
                    className="section-heading"
                    fontSize={24}
                    fontWeight={500}
                    letterSpacing={0.5}
                  ></Box>
                  <Grid item xs={12} className="home-video-section">
                    <Grid container spacing={2}>
                      {videos &&
                        videos.map((video, index) => {
                          if (index < 3) {
                            return (
                              <Fragment key={video.id}>
                                <VideoComponent
                                  setVideoUrlHandler={() => setVideoUrlHandler(video.url, video.thumbnail)}
                                  video={video}
                                  videoWidth={videoWidth}
                                  videoHeight={videoHeight}
                                />
                              </Fragment>
                            );
                          }
                        })}
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <VideoModel
                videoFlag={videoFlag}
                videoUrl={videoUrl}
                thumbnail={videoThumbnail}
                closeVideo={closeVideo}
              />
            </div>
          )}

        </div>
      </div>
    </Layout>
  );
};

const mapStateToProps = state => ({
  currentUser: state.loginReducer.currentUser,
  isMailVerify: state.loginReducer.isMailVerify,
  videos: state.dashboardReducer.videos,
  isSearched: state.boatReducer.isSearched,
  featureBoats: state.boatReducer.featureBoats,
  bestDealBoats: state.boatReducer.bestDealBoats,
  mustBuyBoats: state.boatReducer.mustBuyBoats,
  latestBoats: state.boatReducer.latestBoats,
  popularBoats: state.boatReducer.popularBoats,
  topRatedBoats: state.boatReducer.topRatedBoats,
  cityLists: state.dashboardReducer.cityLists,
  allAuctionRooms: state.boatReducer && state.boatReducer.allAuctionRooms,
  boatTypes: state.boatReducer.boatTypes,
  boatTypesForManufacture: state.boatReducer.boatTypesForManufacture,
  advertisements: state.advertisementReducer.advertisements,
  homeBanner: state.dashboardReducer[mediaCategory.home.fieldName],
  totalPopularBoats: state.boatReducer.totalPopularBoats,
  boatSearchValues: state.boatReducer.boatSearchValues,
});

const mapDispatchToProps = dispatch => ({
  getUserRoles: () => dispatch(getUserRoles()),
  verifyUserEmail: data => dispatch(verifyUserEmail(data)),
  clearSearchBoatFlag: () => dispatch(clearSearchBoatFlag()),
  searchBoat: data => dispatch(searchBoat(data)),
  getHomeVideos: data => dispatch(getHomeVideos(data)),
  getBoatByType: data => dispatch(getBoatByType(data)),
  getLatestBoats: data => dispatch(getLatestBoats(data)),
  getPopularBoats: data => dispatch(getPopularBoats(data)),
  getTopRatedBoats: data => dispatch(getTopRatedBoats(data)),
  getGlobalMinimumPriceBoats: data =>
    dispatch(getGlobalMinimumPriceBoats(data)),
  clearCityWiseBoats: () => dispatch(clearCityWiseBoats()),
  clearCategoryWiseBoats: () => dispatch(clearCategoryWiseBoats()),
  getAllAuctionRooms: data => dispatch(getAllAuctionRooms(data)),
  getBoatTypeStart: () => dispatch(getBoatTypeStart()),
  getBoatTypeForManufacture: () => dispatch(getBoatTypeForManufacture()),
  getModuleWiseBanners: type => dispatch(getModuleWiseBanners(type)),
  getCategoryWiseAdvertisements: data => dispatch(getCategoryWiseAdvertisements(data)),
  getBoatSearchMinimumValues: () => dispatch(getBoatSearchMinimumValues()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
