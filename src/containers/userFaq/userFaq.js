import React from "react";
import { Layout } from "../../components/layout/layout";
import { Tab, Row, Col, Nav, Container } from "react-bootstrap";

import "./userFaq.scss";
import { InputBase, Grid } from "@material-ui/core";
import { IoIosArrowForward } from "react-icons/io";
import { pagination } from "../../util/enums/enums";
import { getAllUserFaq } from "../../redux/actions/userFaqAction";
import searchFilter from "../../helpers/filterUtil";
import { connect } from "react-redux";
import { getCategoriesWiseBanners } from "../../redux/actions";

class UserFaq extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      filterData: []
    }
  }

  createMarkup = data => {
    return { __html: data };
  };

  componentDidMount() {
    const { getAllUserFaq, getCategoriesWiseBanners } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    };
    const faqInput = {
      type: "faq",
      fieldName: "faqBanner"
    }
    getCategoriesWiseBanners(faqInput)
    getAllUserFaq(data);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userfaqs !== this.props.userfaqs) {
      this.setState({ filterData: this.props.userfaqs })
    }
  }

  searchHandler() {
    const { userfaqs } = this.props;
    const { searchValue } = this.state;
    let temp = [];
    if (userfaqs.length) {
      temp = searchFilter(userfaqs, searchValue, 'question');
    } else {
      temp = userfaqs || []
    }
    this.setState({ filterData: temp });
  };

  render() {
    const { history, faqBanners } = this.props;
    const { filterData, searchValue } = this.state;
    
    return (
      <Layout>
        <div className="user-faq-banner-div">
          <img src={faqBanners && faqBanners.length ? faqBanners[0].url : require("./slide-1.jpg")} alt="" style={{width: '100%', height: '100%'}} />
          <div className="user-guide-banner-text-div">
            <div>
              <h4 className="font-weight-400 text-white faq-sub-title">
                Frequently Asked Questions
              </h4>
              <div className="d-flex mt-5 faq-main-title-div">
                <h1 className="text-white faq-main-title">Search AdamSea</h1>
                <h1 className="ml-2 faq-main-title">Help Desk</h1>
              </div>
              <div className="main-home-search faq-main-search">
                <InputBase
                  placeholder="Ask a question or search by keyword"
                  inputProps={{ "aria-label": "search google maps" }}
                  className="search-input"
                  value={searchValue}
                  onChange={e => {
                    this.setState({ searchValue: e.target.value });
                  }}
                />
                <div
                  className="btn btn-primary btn-s mr--3 w-auto faq-banner-search-btn"
                  onClick={() => this.searchHandler()}
                >
                  <i className="adam-search1"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container100 mt-5 pt-4 pb-5 user-faq-main-container">
          <span className="font-22 faq-questions-main-title">Questions</span>
          <Tab.Container id="left-tabs-example" defaultActiveKey="0">
            <Row className="mt-4 faq-main-row">
              <Col sm={3} className="faq-left-div">
                <Nav
                  variant="pills"
                  className="user-guide-nav-left-div user-guide-scrollbar pr-3"
                >
                  {filterData && filterData.map((value, index) => (
                    <Nav.Item className="user-guide-nav-div d-flex align-items-center justify-content-between">
                      <Nav.Link eventKey={index} className="col-gray pl-0 pr-0 faq-left-side-question-link">
                        {value.question}
                        <div>
                          <IoIosArrowForward className="col-gray user-guide-nav-icon" />
                        </div>
                      </Nav.Link>
                    </Nav.Item>
                  ))}
                </Nav>

                <div className="mt-5 pr-3 user-faq-left-img-section">
                  <Grid item xs className="border-user-guide-section">
                    <div className="d-flex pl-4 pr-4 user-faq-left-img-div">
                      <div className="pt-4 user-faq-left-img">
                        <div className="faq-left-img">
                          <img
                            src={require("../../assets/images/help/buying-process.png")}
                            alt="User Guide"
                            className="width-100 h-100"
                          />
                        </div>
                      </div>

                      <div className="m-auto">
                        <span className="font-20 font-weight-500 user-faq-left-img-div-title">AdamSea</span>
                        <div className="d-flex flex-column mt-2 user-faq-left-img-desc-div">
                          <span className="user-faq-left-img-desc">
                            Neque porro quisquam
                          </span>
                          <span className="user-faq-left-img-desc">
                            Dolorem ipsum estqui
                          </span>
                        </div>
                      </div>
                    </div>
                  </Grid>
                </div>

                <div className="mt-4 pr-3 user-faq-left-img-section user-faq-left-second-img-section">
                  <Grid item xs className="border-user-guide-section">
                    <div className="d-flex pl-4 pr-4 user-faq-left-img-div" onClick={() => history && history.push("/user-guide")}>
                      <div className="pt-4 user-faq-left-img">
                        <div className="faq-left-img">
                          <img
                            src={require("../../assets/images/help/downloads.png")}
                            alt="User Guide"
                            className="width-100 h-100"
                          />
                        </div>
                      </div>

                      <div className="m-auto" >
                        <span className="font-20 font-weight-500 user-faq-left-img-div-title">
                          User Guide
                        </span>
                        <div className="d-flex flex-column mt-2 user-faq-left-img-desc-div">
                          <span className="user-faq-left-img-desc">
                            Processes explained
                          </span>
                          <span className="user-faq-left-img-desc">
                            Clear your doubts
                          </span>
                        </div>
                      </div>
                    </div>
                  </Grid>
                </div>
              </Col>
              <Col sm={9} className="faq-right-div">
                <Tab.Content>
                  {filterData && filterData.map((value, index) => (
                    <Tab.Pane eventKey={index}>
                    <div className="faq-right-side-answer-div">
                      <div
                        dangerouslySetInnerHTML={this.createMarkup(value.description)}
                      />
                      </div>
                    </Tab.Pane>
                  ))}
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </div>
        <hr className="mt-5 faq-divider" />
        <Container className="mt-5 frequent-questions-container">
          <span className="font-22 font-weight-500 frequent-questions-title">
            Most Frequent Questions
          </span>
          <Grid container spacing={3} className="mt-2 frequent-questions-main-div">
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Single Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                Sign Into Your Account
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Single Seller
              </span>
            </Grid>
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Yatch Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Update My Profile
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Reset My Password
              </span>
            </Grid>
            <Grid item sm={4} className="d-flex flex-column">
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Open Account As Broker Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Broker Seller
              </span>
              <span className="col-gray pl-0 pr-0 f-15 mb-2 mt-1 frequent-questions">
                How To Post Ads as Broker Seller
              </span>
            </Grid>
          </Grid>
        </Container>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  faqBanners: state.bannerReducer && state.bannerReducer["faqBanner"],
  userfaqs: state.userFaqReducer && state.userFaqReducer.userfaqs
});

const mapDispatchToProps = dispatch => ({
  getCategoriesWiseBanners: (data) => (dispatch(getCategoriesWiseBanners(data))),
  getAllUserFaq: data => dispatch(getAllUserFaq(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserFaq);
