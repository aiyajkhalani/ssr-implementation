import React, { Component, Fragment } from "react";
import * as Yup from "yup";
import { connect } from "react-redux";
import uuid from "uuid/v4";
import InputLabel from "@material-ui/core/InputLabel";
import { DateRangePicker, SingleDatePicker } from "react-dates";

import { Formik, ErrorMessage } from "formik";
import {
  Button,
  Grid,
  TextField,
  Box,
  Divider,
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  RadioGroup,
  Radio,
  Input,
  Container,
  CardContent,
  Select,
  Chip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText
} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Datetime from "react-datetime";

// import './profile.scss'
import { Layout, Loader, Field } from "../../../components";
import {
  getAllBoatLookUps,
  saveBoat,
  getBoatById,
  updateBoat,
  getBoatTypeStart,
  clearErrorMessageShow,
  clearAddBoatFlag,
  clearUpdateBoatFlag
} from "../../../redux/actions";
import GoogleMap from "../../../components/map/map";
import ImageUploader from "react-images-upload";
import { uploadToS3, deleteFromS3 } from "../../../helpers/s3FileUpload";
import { getListOfYears } from "../../../util/enums/enums";
import { SuccessNotify, ErrorNotify } from "../../../helpers/notification";

// style
import "../../../styles/common.scss";
import {
  randomAdId,
  renderSelectOptions,
  getIds
} from "../../../helpers/string";
import "./AddBoat.scss";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import { Row, Col } from "react-bootstrap";
import { renderBoatTypes } from "../../../helpers/jsxHelper";
import { containerFluid } from "../../../assets/jss/material-kit-pro-react";
import { cancelHandler } from "../../../helpers/routeHelper";
import { dateStringFormate } from "../../../util/utilFunctions";
import moment from "moment";
import ErrorComponent from "../../../components/error/errorComponent";
import MultiSelect from "../../../components/helper/multiSelect";

class AddBoat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boatId: "",
      isUpdate: false,
      mediaUpdated: false,
      adId: "",
      location: {
        placeName: "",
        country: "",
        state: "",
        postalCode: "",
        city: "",
        address: "",
        latitude: 0.0,
        longitude: 0.0
      },
      boatOwnership: {
        complianceLabelNumber: "",
        hullSerialNumber: "",
        vehicleIdentificationNumber: "",
        licenceNo: "",
        attachId: "",
        ownershipAttachment: ""
      },
      boatInfo: {
        listedBy: "",
        boatType: "",
        boatStatus: "",
        expectedCompletion: null,
        projectDuration: "",
        boatName: "",
        boatParking: "",
        trailer: "",
        yearBuilt: "",
        manufacturedBy: "",
        hullMaterial: "",
        hullColor: "",
        usedHours: ""
      },
      boatEngine: {
        noOfEngines: "",
        modelYear: "",
        fuelType: "",
        fuelCapacity: "",
        holdingCapacity: "",
        freshWater: "",
        engineManufacturer: "",
        engineModel: "",
        engineHp: "",
        engineDrives: "",
        engineStroke: ""
      },
      mechanicalSystem: {
        waterMarker: "",
        stabilizerSystem: "",
        bowThruster: "",
        oilWaterSeparator: "",
        steeringSystem: "",
        fireBilgePump: ""
      },
      electricalSystem: {
        output: "",
        batteriesCount: "",
        generators: "",
        batteryType: "",
        emergencyGenerator: ""
      },
      dimensions: {
        decks: "",
        displacement: "",
        heightInFt: "",
        lengthInFt: "",
        widthInFt: "",
        numberOfHeads: "",
        weightInKg: "",
        crewCabinCount: "",
        beam: "",
        crewBerthCount: "",
        draft: "",
        crewHeadsCount: "",
      },
      amenities: [],
      accessories: [],
      navigationEquipments: [],
      Commercial: {
        usage: "",
        boatReview: "",
        accidentHistory: "",
        repairHistory: "",
        lastMaintenance: "",
        price: "",
        isTaxEnabled: false,
        tax: "",
        description: "",
        accidentDescribe: ""
      },
      otherInformation: {
        images: [],
        layout: [],
        video: ""
      },
      latLng: { lat: 23.0225, lng: 72.5714 },

      agree: false,
      termsModal: false,
      error: false,
      expectedCompletion: "",
      focused: false,
    };
  }

  async componentDidMount() {
    const { boatId } = this.state;

    const {
      getBoatLookUps,
      getBoatById,
      getBoatTypeStart,
      clearErrorMessageShow,
      clearAddBoatFlag,
      clearUpdateBoatFlag
    } = this.props;
    await getBoatLookUps();
    getBoatTypeStart();
    clearAddBoatFlag();
    clearUpdateBoatFlag();
    clearErrorMessageShow();

    if (boatId) {
      await getBoatById({ id: boatId });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      history,
      isBoatCreated,
      isBoatUpdated,
      isBoatCreateAndUpdateError,
      currentUser
    } = nextProps;
    const { location } = history;
    const { adId } = prevState;
    if (isBoatCreated) {
      setTimeout(() => SuccessNotify("Boat is added successfully"), 100);
      history.push("/manage-boats");
    } else if (isBoatUpdated) {
      setTimeout(() => SuccessNotify("Boat is updated successfully"), 100);
      history.push("/manage-boats");
    } else if (isBoatCreateAndUpdateError) {
    } else if (location && location.search) {
      const urlParams = new URLSearchParams(location.search);

      if (urlParams.has("id")) {
        const id = urlParams.get("id");
        return {
          boatId: id,
          isUpdate: true
        };
      }
    } else if (!adId) {
      const roleUser =
        currentUser.role && currentUser.role.aliasName === "boat-owner"
          ? "SELLER"
          : currentUser.role && currentUser.role.aliasName === "broker-and-dealer"
          ? "BROKER"
          : currentUser.role && currentUser.role.aliasName === "boat-manufacturer"
          ? "FACTORY"
          : "";

      return {
        adId: randomAdId(roleUser)
      };
    }

    return null;
  }

  fetchMapInfo = (result, setValue) => {
    const { location } = this.state;
    const {
      address,
      country,
      state,
      city,
      placeName,
      postalCode,
      latitude,
      longitude
    } = result;

    location.address = address;
    location.country = country;
    location.state = state;
    location.city = city;
    location.placeName = placeName;
    location.postalCode = postalCode;
    location.latitude = latitude;
    location.longitude = longitude;

    setValue("address", address);
    this.setState({ location });
  };

  handleFileUpload = async (file, name, value, setValue) => {
    const { otherInformation } = this.state;
    let oldValues = otherInformation[name];
    let formValue = value || [];

    if (file && file.length) {
      let res = await uploadToS3(file[file.length - 1]);

      if (res && res.location) {
        const newValues = oldValues.concat(res.location);
        setValue(name, formValue.concat(res.location));
        otherInformation[name] = newValues;
        this.setState({ otherInformation });
      }
    }
  };

  handleFileDelete = async (index, name, setValue) => {
    const { otherInformation } = this.state;
    const oldValues = otherInformation[name];

    if (oldValues && oldValues.length) {
      otherInformation[name].splice(index, 1);

      setValue(name, otherInformation[name]);
      this.setState({ otherInformation });
    }
  };

  handleSingleFileDelete = async (name, setValue) => {
    const { boatOwnership } = this.state;
    const oldValues = boatOwnership[name];

    if (oldValues && oldValues.length) {
      boatOwnership[name] = "";

      setValue(name, boatOwnership[name]);
      this.setState({ boatOwnership });
    }
  };

  handleSingleFileUpload = async (file, name, setFieldValue) => {
    const { boatOwnership } = this.state;
    if (file.length) {
      let res = await uploadToS3(file[file.length - 1]);
      if (res.location) {
        boatOwnership[name] = res.location;
        setFieldValue(name, res.location);
        this.setState({ boatOwnership });
      }
    }
  };

  menuItems(values) {
    if (values) {
      return values.map(name => (
        <MenuItem key={name.lookUp.id} value={name.lookUp.id}>
          {name.lookUp.alias}
        </MenuItem>
      ));
    }
  }

  renderRadioButtons = () => (
    <>
      <FormControlLabel control={<Radio value="Yes" />} label="Yes" />
      <FormControlLabel control={<Radio value="No" />} label="No" />
    </>
  );

  getYearListing = () => {
    const listOfYears = getListOfYears();

    return (
      listOfYears &&
      listOfYears.map(year => (
        <MenuItem value={year} key={year}>
          {year}
        </MenuItem>
      ))
    );
  };

  onClickAgree = () => {
    this.setState(prevState => ({
      agree: !prevState.agree
    }));
  };

  termsHandler = () => {
    this.setState(prevState => ({
      termsModal: !prevState.termsModal
    }));
  };


  renderStatusType = (boatStatus, fieldName) => {
    const { boatLookUps } = this.props
    const selectedStatus = boatLookUps && 
    boatLookUps.manufacturerBoatStatus && 
    boatLookUps.manufacturerBoatStatus.length && 
    boatLookUps.manufacturerBoatStatus.find((item) => ( item.lookUp.id === boatStatus ));
    if(selectedStatus && fieldName === selectedStatus.lookUp.alias) {
      return true;
   } else {
      return false;
    }
  }


  prepareBoatValue = () => {
    const { boat } = this.props;
    const {
      boatOwnership,
      otherInformation,
      location,
      mediaUpdated
    } = this.state;

    if (boat.hasOwnProperty("id")) {
      if (!mediaUpdated) {
        const {
          attachId,
          ownershipAttachment,
          images,
          layout,
          video,
          address,
          country,
          state,
          city,
          placeName,
          postalCode,
          geometricLocation,
          adId
        } = boat;

        boatOwnership.attachId = attachId;
        boatOwnership.ownershipAttachment = ownershipAttachment;
        otherInformation.images = images;
        otherInformation.layout = layout;
        otherInformation.video = video;

        location.address = address;
        location.country = country;
        location.state = state;
        location.city = city;
        location.placeName = placeName;
        location.postalCode = postalCode;
        location.latitude =
          geometricLocation &&
          geometricLocation.coordinates.length > 0 &&
          geometricLocation.coordinates[1];
        location.longitude =
          geometricLocation &&
          geometricLocation.coordinates.length > 0 &&
          geometricLocation.coordinates[0];

        this.setState({
          boatOwnership,
          otherInformation,
          location,
          latLng: { lat: location.latitude, lng: location.longitude },
          adId,
          mediaUpdated: true
        });
      }

      const {
        boatType,
        hullMaterial,
        boatStatus,
        boatParking,
        mainSaloon,
        mainSaloonConvertible,
        fuelType,
        engineDrives,
        engineStroke,
        accessories,
        amenities,
        navigationEquipments
      } = boat;

      // delete extra field from boat object
      delete boat.seller;
      delete boat.adStatus;
      delete boat.featureStatus;
      delete boat.bestDealStatus;
      delete boat.mustBuyStatus;
      delete boat.likeCount
      delete boat.status;
      delete boat.userCount;
      delete boat.reviews;
      delete boat.auctionRoom;
      delete boat.__typename;

      return {
        ...boat,

        // select drop-down
        boatType: boatType && boatType.id,
        hullMaterial: hullMaterial && hullMaterial.id,
        fuelType: hullMaterial && fuelType.id,
        engineDrives: hullMaterial && engineDrives.id,
        engineStroke: hullMaterial && engineStroke.id,

        // radio-button
        boatStatus: boatStatus && boatStatus.id,
        boatParking: boatParking && boatParking.id,
        mainSaloon: mainSaloon && mainSaloon.id,
        mainSaloonConvertible:
          mainSaloonConvertible && mainSaloonConvertible.id,

        accessories: renderSelectOptions(accessories, "alias", "id"),
        amenities: renderSelectOptions(amenities, "alias", "id"),
        navigationEquipments: renderSelectOptions(
          navigationEquipments,
          "alias",
          "id"
        )
      };
    }
  };

  render() {
    const {
      boatOwnership,
      boatInfo,
      boatEngine,
      mechanicalSystem,
      electricalSystem,
      dimensions,
      otherInformation,
      latLng,
      Commercial,
      amenities,
      accessories,
      navigationEquipments,
      location,
      boatId,
      isUpdate,
      adId,
      agree,
      termsModal,
      expectedCompletion,
      focused,
      error
    } = this.state;
    const {
      boatLookUps,
      saveBoat,
      boat,
      updateBoat,
      boatTypes,
      history,
      isBoatCreateAndUpdateError,
      errorMessage,
      clearErrorMessageShow,
      clearUpdateBoatFlag,
      clearAddBoatFlag,
      currentUser
    } = this.props;

    const { images, layout } = otherInformation;
    const { attachId, ownershipAttachment } = boatOwnership;
    const {
      address,
      country,
      state,
      city,
      placeName,
      postalCode,
      latitude,
      longitude
    } = location;

    let {
      boatStatus,
      hullMaterial,
      boatParking,
      fuelType,
      engineDrives,
      engineStroke,
      mainSaloon,
      mainSaloonConvertible,
      navigationEq,
      amenitiesItems,
      accessoriesItems,
      manufacturerBoatStatus
    } = boatLookUps || {};

    const roleWiseBoatStatus = currentUser.role && currentUser.role.aliasName === "boat-manufacturer" ? manufacturerBoatStatus: boatStatus

    amenitiesItems = renderSelectOptions(amenitiesItems, "alias", "id", true);
    accessoriesItems = renderSelectOptions(
      accessoriesItems,
      "alias",
      "id",
      true
    );
    navigationEq = renderSelectOptions(navigationEq, "alias", "id", true);

    const initValue =
      isUpdate && boatId
        ? boat.hasOwnProperty("id") && this.prepareBoatValue()
        : {
            ...boatOwnership,
            ...boatInfo,
            ...boatEngine,
            ...mechanicalSystem,
            ...electricalSystem,
            ...dimensions,
            ...otherInformation,
            ...Commercial,
            amenities,
            accessories,
            navigationEquipments,
            ...location
          };

    return (
      <Fragment>
        <DashboardLayout>
          <div className="pl-3 pr-3">
            {isUpdate && !boat.hasOwnProperty("id") ? (
              <Loader></Loader>
            ) : (
              <div>
                <Formik
                  initialValues={initValue}
                  onSubmit={values => {
                    values.price = parseInt(values.price);
                    values.heightInFt = parseInt(values.heightInFt);
                    values.widthInFt = parseInt(values.widthInFt);
                    values.lengthInFt = parseInt(values.lengthInFt);
                    values.weightInKg = parseInt(values.weightInKg);
                    values.address = address;
                    values.country = country;
                    values.state = state;
                    values.city = city;
                    values.placeName = placeName;
                    values.postalCode = postalCode;
                    values.geometricLocation = {
                      coordinates: [longitude, latitude]
                    };
                    delete values.latitude;
                    delete values.longitude;

                    values.adId = adId;

                    const input = {
                      ...values,
                      amenities: getIds(values.amenities),
                      accessories: getIds(values.accessories),
                      navigationEquipments: getIds(values.navigationEquipments)
                    };

                    if (isUpdate) {
                      clearUpdateBoatFlag();
                      updateBoat(input);
                      clearErrorMessageShow();
                    } else {
                      clearAddBoatFlag();
                      saveBoat(input);
                      clearErrorMessageShow();
                    }
                  }}
                  validationSchema={Yup.object().shape({
                    address: Yup.string().required(
                      "Address field is required."
                    ),
                    vehicleIdentificationNumber: Yup.string().required(
                      "Vehicles Identification No. field is required."
                    ),
                    hullSerialNumber: Yup.string().required(
                      "Hull Serial Number field is required."
                    ),
                    yearBuilt: Yup.string().required(
                      "Please Select Boat Build Year"
                    ),
                    boatType: Yup.string().required("Please Select Boat Type"),
                    manufacturedBy: Yup.string().required(
                      "Manufactured By field is required."
                    ),
                    boatName: Yup.string().required(
                      "Boat Name field is required."
                    ),
                    hullMaterial: Yup.string().required(
                      "Hull Material field is required."
                    ),
                    hullColor: Yup.string().required(
                      "Hull Color field is required."
                    ),
                    trailer: Yup.string().required(
                      "Select yes if you have trailer."
                    ),
                    usedHours: Yup.string().required(
                      "Used Hours field is require."
                    ),
                    noOfEngines: Yup.string().required(
                      "Number Of Engines field is require."
                    ),
                    engineManufacturer: Yup.string().required(
                      "Engine Manufacturer field is require."
                    ),
                    modelYear: Yup.string().required(
                      "Model Year field is require."
                    ),
                    engineModel: Yup.string().required(
                      "Engine Model field is require."
                    ),
                    fuelType: Yup.string().required("Select fuel type."),
                    engineHp: Yup.string().required(
                      "Engine HP field is require."
                    ),
                    engineDrives: Yup.string().required(
                      "Please Select Engine Drives."
                    ),
                    boatStatus: Yup.string().required(
                      "Please Select Boat status."
                    ),
                    boatParking: Yup.string().required(
                      "Please Select Boat status."
                    ),
                    engineStroke: Yup.string().required(
                      "Please Select Engine Stroke."
                    ),
                    stabilizerSystem: Yup.string().required(
                      "Stabilizer System field is require."
                    ),
                    steeringSystem: Yup.string().required(
                      "Steering System field is require."
                    ),

                    batteryType: Yup.string().required(
                      "Battery Type field is require."
                    ),
                    generators: Yup.string().required(
                      "Generators field is require."
                    ),
                    batteriesCount: Yup.string().required(
                      "No. of Batteries field is require."
                    ),

                    decks: Yup.string().required("Decks No field is require."),
                    heightInFt: Yup.string().required(
                      "Height field is require."
                    ),
                    lengthInFt: Yup.string().required(
                      "Length field is require."
                    ),
                    widthInFt: Yup.string().required("Width field is require."),
                    weightInKg: Yup.string().required(
                      "Weight field is require."
                    ),
                    beam: Yup.string().required("Beam field is require."),
                    draft: Yup.string().required("Draft field is require."),
                    usage: Yup.string().required("Usage field is require."),
                    boatReview: Yup.string().required(
                      "Boat Review field is require."
                    ),
                    accidentHistory: Yup.string().required(
                      "Accident History field is require."
                    ),
                    navigationEquipments: Yup.array().min(2, "Minimum 2 Navigation Equipments require"),
                    accessories: Yup.array().min(2, "Minimum 2 Accessories require"),
                    amenities: Yup.array().min(2, "Minimum 2 Amenities require"),
                    price: Yup.number()
                      .min(0)
                      .required("Price In USD field is require."),
                    images: Yup.array().min(5, "Minimum 5 photos upload require"),
                    lastMaintenance: Yup.date(),
                    layout: Yup.string().required("Boat layout image is required")
                  })}
                  render={({
                    errors,
                    status,
                    touched,
                    values,
                    setFieldValue,
                    handleSubmit
                  }) => (
                    <Form>
                    
                      <containerFluid>
                        <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                          <Box
                            fontSize={20}
                            letterSpacing={1}
                            fontWeight={600}
                            className="map-title"
                          >
                            {!isUpdate ? `Add Boat` : `Edit Boat`}{" "}
                            <span className="font-weight-400 font-14">
                              Ad ID: {`${adId}`}
                            </span>
                          </Box>
                        </div>
                        <Row className="mb-3 m-0">
                          <Col xs={6} className="pr-0 pl-0">
                            <div className="add-boat-map add-boat-map-div map-div-form h-100">
                              <GoogleMap
                                className="googleMap-position"
                                latLng={latLng}
                                fetch={result =>
                                  this.fetchMapInfo(result, setFieldValue)
                                }
                                height={35}
                                width={100}
                                placeHolder="Boat Address"
                                columnName={"address"}
                                isError={errors.address}
                                value={location}
                                isUpdate
                              ></GoogleMap>
                            </div>
                          </Col>
                          <Col xs={6} className="pl-0 pr-0">
                            <Card
                              className="ml-3 mt-0 dashboard-forms-field
                      border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2 h-100 mb-0"
                            >
                              <CardContent>
                                <Grid container spacing={2}>
                                  <Grid xs={12} item className="pb-1">
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Boat Ownership
                                    </Box>
                                  </Grid>

                                  <Grid item sm={12}>
                                    <Field
                                      label="Compliance Label Number (If Available)"
                                      name="complianceLabelNumber"
                                      type="text"
                                      value={values.complianceLabelNumber}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "complianceLabelNumber",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      component="div"
                                      name="complianceLabelNumber"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={12}>
                                    <Field
                                      label=" Pleasure Craft Licence Number Of
                                    Registration No. (If Already Licensed)"
                                      name="licenceNo"
                                      type="text"
                                      value={values.licenceNo}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "licenceNo",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="licenceNo"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={12}>
                                    <Field
                                      name="hullSerialNumber"
                                      type="text"
                                      required
                                      label="Hull Serial Number(hullSerialNumber)"
                                      value={values.hullSerialNumber}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "hullSerialNumber",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="hullSerialNumber"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={12}>
                                    <Field
                                      name="vehicleIdentificationNumber"
                                      type="text"
                                      label=" Vehicles Identification No.(vehicleIdentificationNumber)"
                                      required
                                      value={values.vehicleIdentificationNumber}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "vehicleIdentificationNumber",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="vehicleIdentificationNumber"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                      className="text-center mb-3"
                                    >
                                      Attach Piece Of ID
                                    </Box>

                                    <div className="d-flex flex-wrap justify-content-center">
                                      <Field
                                        id="attachId"
                                        name="attachId"
                                        type="single-image"
                                        value={values.attachId}
                                        onChangeText={setFieldValue}
                                        
                                      />
                                    </div>
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                      className="text-center mb-3"
                                    >
                                      Boat Ownership Attachment
                                    </Box>
                                    <div className="d-flex flex-wrap justify-content-center">
                                     
                                      <Field
                                        id="ownershipAttachment"
                                        name="ownershipAttachment"
                                        type="single-image"
                                        value={values.ownershipAttachment}
                                        onChangeText={setFieldValue}
                                        
                                      />
                                    </div>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                        </Row>
                        <Row className="mb-3 m-0">
                          <Col xs={6} className="pr-0 pl-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Boat Information
                                    </Box>
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Manufactured By"
                                      name="manufacturedBy"
                                      type="text"
                                      required
                                      value={values.manufacturedBy}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "manufacturedBy",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="manufacturedBy"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Hour Used"
                                      name="usedHours"
                                      type="text"
                                      required
                                      value={values.usedHours}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "usedHours",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="usedHours"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      name="hullColor"
                                      label="Hull Color"
                                      type="text"
                                      required
                                      value={values.hullColor}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "hullColor",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="hullColor"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Boat Name"
                                      name="boatName"
                                      type="text"
                                      required
                                      value={values.boatName}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "boatName",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="boatName"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      name="listedBy"
                                      label="Listed By"
                                      type="text"
                                      value={values.listedBy}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "listedBy",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="listedBy"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Year Built
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      name="yearBuilt"
                                      hinttext="Select a year"
                                      value={values.yearBuilt}
                                      onChange={e =>
                                        setFieldValue(
                                          "yearBuilt",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {this.getYearListing &&
                                        this.getYearListing()}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="yearBuilt"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Boat Type
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      name="boatType"
                                      hinttext="Select a name"
                                      value={values.boatType}
                                      onChange={e =>
                                        setFieldValue(
                                          "boatType",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {boatTypes &&
                                        boatTypes.length > 0 &&
                                        renderBoatTypes(boatTypes)}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="boatType"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Hull Material
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      hinttext="Select a name"
                                      name="hullMaterial"
                                      value={values.hullMaterial}
                                      onChange={e =>
                                        setFieldValue(
                                          "hullMaterial",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {hullMaterial &&
                                        this.menuItems(hullMaterial)}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="hullMaterial"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={4}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info required"
                                        component="legend"
                                      >
                                        Boat Status
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        value={values.boatStatus}
                                        className="radioButton"
                                        onChange={e => {
                                          setFieldValue(
                                            "boatStatus",
                                            e.target.value
                                          );
                                        }}
                                      >
                                        <div className="radio-button-type-font">
                                          {roleWiseBoatStatus &&
                                            roleWiseBoatStatus.map(item => {
                                              return (
                                                <FormControlLabel
                                                  key={item.lookUp.id}
                                                  control={
                                                    <Radio
                                                      value={item.lookUp.id}
                                                    />
                                                  }
                                                  label={item.lookUp.alias}
                                                />
                                              );
                                            })}
                                        </div>
                                      </RadioGroup>
                                    </FormControl>
                                    <ErrorMessage
                                      component="div"
                                      name="boatStatus"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={5}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info required"
                                        component="legend"
                                      >
                                        The Boat Parking
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        value={values.boatParking}
                                        className="radioButton"
                                        onChange={e =>
                                          setFieldValue(
                                            "boatParking",
                                            e.target.value
                                          )
                                        }
                                      >
                                        <div className="radio-button-type-font">
                                          {boatParking &&
                                            boatParking.map(item => {
                                              return (
                                                <FormControlLabel
                                                  key={item.lookUp.id}
                                                  control={
                                                    <Radio
                                                      value={item.lookUp.id}
                                                    />
                                                  }
                                                  label={item.lookUp.alias}
                                                />
                                              );
                                            })}
                                        </div>
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="boatParking"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>

                                  <Grid item sm={3}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info required"
                                        component="legend"
                                      >
                                        Trailer
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        name="trailer"
                                        value={values.trailer}
                                        className="radioButton"
                                        onChange={e => {
                                          setFieldValue(
                                            "trailer",
                                            e.target.value
                                          );
                                        }}
                                      >
                                        <div className="radio-button-type-font">
                                          {this.renderRadioButtons()}
                                        </div>
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="trailer"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>
                                  {values.boatStatus && values.boatStatus !== "" && this.renderStatusType(values.boatStatus, "Under Construction") && <Grid item sm={6}>
                                  <>
                                        <label className="required">Select Date</label>
                                        <div>
                                          <SingleDatePicker
                                            date={expectedCompletion}
                                            onDateChange={date => {
                                              this.setState({ expectedCompletion: date })
                                              // setFieldValue("projectDuration", "")
                                              date && setFieldValue("expectedCompletion", date.toDate())
                                            }}
                                            focused={focused}
                                            onFocusChange={({ focused }) =>
                                              this.setState({ focused })
                                            }
                                            id="startDate"
                                          />
                                        </div>
                                        <ErrorMessage
                                          component="div"
                                          name="startDate"
                                          className="error-message mr-2"
                                        />
                                      </>
                                  </Grid>}
                                  {values.boatStatus && this.renderStatusType(values.boatStatus, "Design Stage") && <Grid item sm={6}>
                                    <Field
                                      label="Project Duration"
                                      name="Project Duration"
                                      type="text"
                                      required
                                      value={values.projectDuration}
                                      className="form-control"
                                      onChangeText={e =>
                                        {
                                          // setFieldValue("expectedCompletion", null)
                                        setFieldValue(
                                          "projectDuration",
                                          e.target.value
                                        )}
                                      }
                                    />
                                  </Grid>}
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                          <Col xs={6} className="pr-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Boat Engine
                                    </Box>
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Number Of Engines"
                                      required
                                      name="noOfEngines"
                                      type="text"
                                      value={values.noOfEngines}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "noOfEngines",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="noOfEngines"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Engine ManuFacturer"
                                      required
                                      name="engineManufacturer"
                                      type="text"
                                      value={values.engineManufacturer}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "engineManufacturer",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="engineManufacturer"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Year Model"
                                      required
                                      name="modelYear"
                                      type="text"
                                      value={values.modelYear}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "modelYear",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="modelYear"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Fuel Capacity Tank"
                                      name="fuelCapacity"
                                      type="text"
                                      value={values.fuelCapacity}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "fuelCapacity",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="fuelCapacity"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Engine Model"
                                      required
                                      name="engineModel"
                                      type="text"
                                      value={values.engineModel}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "engineModel",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="engineModel"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Engine HP"
                                      required
                                      name="engineHp"
                                      type="text"
                                      value={values.engineHp}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "engineHp",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="engineHp"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label=" Fresh Water Tank"
                                      name="freshWater"
                                      type="text"
                                      value={values.freshWater}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "freshWater",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="freshWater"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Holding Capacity Tank"
                                      name="holdingCapacity"
                                      type="text"
                                      value={values.holdingCapacity}
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "holdingCapacity",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="holdingCapacity"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={4}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Engine Drives
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      name="engineDrives"
                                      hinttext="Select a name"
                                      value={values.engineDrives}
                                      onChange={e =>
                                        setFieldValue(
                                          "engineDrives",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {engineDrives &&
                                        this.menuItems(engineDrives)}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="engineDrives"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={4}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Fuel Type
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      name="fuelType"
                                      hinttext="Select a name"
                                      value={values.fuelType}
                                      onChange={e =>
                                        setFieldValue(
                                          "fuelType",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {fuelType && this.menuItems(fuelType)}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="fuelType"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={4}>
                                    <InputLabel
                                      className="required"
                                      htmlFor="outlined-age-simple"
                                    >
                                      Engine Stroke
                                    </InputLabel>
                                    <Select
                                      fullWidth
                                      name="engineStroke"
                                      hinttext="Select a name"
                                      value={values.engineStroke}
                                      onChange={e =>
                                        setFieldValue(
                                          "engineStroke",
                                          e.target.value
                                        )
                                      }
                                    >
                                      {engineStroke &&
                                        this.menuItems(engineStroke)}
                                    </Select>
                                    <ErrorMessage
                                      component="div"
                                      name="engineStroke"
                                      className="error-message"
                                    />
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                        </Row>

                        <Row className="mb-3 m-0">
                          <Col xs={6} className="pr-0 pl-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Mechanical System
                                    </Box>
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Water Marker"
                                      name="waterMarker"
                                      value={values.waterMarker}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "waterMarker",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="waterMarker"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Stabilizer System"
                                      name="stabilizerSystem"
                                      value={values.stabilizerSystem}
                                      required
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "stabilizerSystem",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="stabilizerSystem"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Bow Thruster"
                                      name="bowThruster"
                                      value={values.bowThruster}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "bowThruster",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="bowThruster"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Oil/Water Separator"
                                      name="oilWaterSeparator"
                                      value={values.oilWaterSeparator}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "oilWaterSeparator",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="oilWaterSeparator"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Steering System"
                                      name="steeringSystem"
                                      value={values.steeringSystem}
                                      required
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "steeringSystem",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="steeringSystem"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Fire/Bilge Pump"
                                      name="fireBilgePump"
                                      value={values.fireBilgePump}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "fireBilgePump",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="fireBilgePump"
                                      className="error-message"
                                    />
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                          <Col xs={6} className="pr-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Electrical System
                                    </Box>
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Output"
                                      name="output"
                                      value={values.output}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue("output", e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="output"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="No. Of Batteries"
                                      name="batteriesCount"
                                      value={values.batteriesCount}
                                      required
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "batteriesCount",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="batteriesCount"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Generators"
                                      name="generators"
                                      value={values.generators}
                                      required
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "generators",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="generators"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Type Of Batteries"
                                      name="batteryType"
                                      value={values.batteryType}
                                      required
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "batteryType",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="batteryType"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Emergency Generator"
                                      name="emergencyGenerator"
                                      value={values.emergencyGenerator}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "emergencyGenerator",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="emergencyGenerator"
                                      className="error-message"
                                    />
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                        </Row>

                        <Row className="mb-3 m-0">
                          <Col xs={6} className="pr-0 pl-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field pb-1">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Dimensions
                                    </Box>
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="decks"
                                      label="Decks No"
                                      value={values.decks}
                                      type="text"
                                      required
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue("decks", e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="decks"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="displacement"
                                      label="Displacement"
                                      value={values.displacement}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "displacement",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="displacement"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="heightInFt"
                                      value={values.heightInFt}
                                      type="number"
                                      label="Height ft"
                                      required
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "heightInFt",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="heightInFt"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="lengthInFt"
                                      value={values.lengthInFt}
                                      type="number"
                                      label="Length ft"
                                      required
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "lengthInFt",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="lengthInFt"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="widthInFt"
                                      value={values.widthInFt}
                                      type="number"
                                      required
                                      label="Width ft"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "widthInFt",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="widthInFt"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="numberOfHeads"
                                      value={values.numberOfHeads}
                                      type="number"
                                      label="Number Of Heads"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "numberOfHeads",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="numberOfHeads"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="weightInKg"
                                      value={values.weightInKg}
                                      type="number"
                                      required
                                      label="Weight kg"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "weightInKg",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="weightInKg"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="crewCabinCount"
                                      value={values.crewCabinCount}
                                      type="number"
                                      label="No. Of Crew Cabins"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "crewCabinCount",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="crewCabinCount"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="beam"
                                      value={values.beam}
                                      required
                                      type="text"
                                      className="form-control"
                                      label="Beam"
                                      onChangeText={e =>
                                        setFieldValue("beam", e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="beam"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      name="crewBerthCount"
                                      value={values.crewBerthCount}
                                      type="number"
                                      label="No. Of Crew Berths"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "crewBerthCount",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="crewBerthCount"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      label="Draft"
                                      name="draft"
                                      required
                                      value={values.draft}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue("draft", e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="draft"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <Field
                                      label="No. Of Crew Heads"
                                      name="crewHeadsCount"
                                      value={values.crewHeadsCount}
                                      type="number"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "crewHeadsCount",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="crewHeadsCount"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6} className="pt-1 pb-1">
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info"
                                        component="legend"
                                      >
                                        Main Saloon
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        name="mainSaloon"
                                        value={values.mainSaloon}
                                        className="radioButton"
                                        onChange={e =>
                                          setFieldValue(
                                            "mainSaloon",
                                            e.target.value
                                          )
                                        }
                                      >
                                        {mainSaloon &&
                                          mainSaloon.map(item => {
                                            return (
                                              <FormControlLabel
                                                key={item.lookUp.id}
                                                control={
                                                  <Radio
                                                    value={item.lookUp.id}
                                                  />
                                                }
                                                label={item.lookUp.alias}
                                              />
                                            );
                                          })}
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="mainSaloon"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>

                                  <Grid item sm={6}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info"
                                        component="legend"
                                      >
                                        Main Saloon Convertible
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        name="mainSaloonConvertible"
                                        value={values.mainSaloonConvertible}
                                        className="radioButton"
                                        onChange={e =>
                                          setFieldValue(
                                            "mainSaloonConvertible",
                                            e.target.value
                                          )
                                        }
                                      >
                                        {mainSaloonConvertible &&
                                          mainSaloonConvertible.map(item => {
                                            return (
                                              <FormControlLabel
                                                key={item.lookUp.id}
                                                control={
                                                  <Radio
                                                    value={item.lookUp.id}
                                                  />
                                                }
                                                label={item.lookUp.alias}
                                              />
                                            );
                                          })}
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="mainSaloonConvertible"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                          <Col xs={6} className="pr-0">
                            <Card
                              className="ml-0 mr-0 mt-0 mb-4 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          "
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid xs={12} item>
                                  <Box
                                    fontSize={20}
                                    letterSpacing={1}
                                    fontWeight={600}
                                  >
                                    Amenities
                                  </Box>
                                </Grid>
                                <Grid item sm={12}>
                                  <MultiSelect
                                    selectedOption={values.amenities}
                                    onChangeValue={item =>
                                      setFieldValue("amenities", [...item])
                                    }
                                    options={amenitiesItems}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="amenities"
                                    className="error-message"
                                  />
                                </Grid>
                              </CardContent>
                            </Card>

                            <Card
                              className="ml-0 mr-0 mb-4 mt-2 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          "
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid xs={12} item>
                                  <Box
                                    fontSize={20}
                                    letterSpacing={1}
                                    fontWeight={600}
                                  >
                                    Accessories
                                  </Box>
                                </Grid>
                                <Grid item sm={12}>
                                  <MultiSelect
                                    selectedOption={values.accessories}
                                    onChangeValue={item =>
                                      setFieldValue("accessories", [...item])
                                    }
                                    options={accessoriesItems}
                                  />

                                  <ErrorMessage
                                    component="div"
                                    name="accessories"
                                    className="error-message"
                                  />
                                </Grid>
                              </CardContent>
                            </Card>

                            <Card
                              className="ml-0 mr-0 mt-2 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                         "
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid xs={12} item>
                                  <Box
                                    fontSize={20}
                                    letterSpacing={1}
                                    fontWeight={600}
                                  >
                                    Navigation Equipment
                                  </Box>
                                </Grid>
                                <Grid item sm={12}>
                                  <MultiSelect
                                    selectedOption={values.navigationEquipments}
                                    onChangeValue={item =>
                                      setFieldValue("navigationEquipments", [
                                        ...item
                                      ])
                                    }
                                    options={navigationEq}
                                  />
                                
                                  <ErrorMessage
                                    component="div"
                                    name="navigationEquipments"
                                    className="error-message"
                                  />
                                
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                        </Row>

                        <Row className="m-0">
                          <Col xs={6} className="pr-0 pl-0">
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 
                              border-bottom-right-radius-2 border-bottom-left-radius-2 h-100
                          "
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid container spacing={2}>
                                  <Grid xs={12} item>
                                    <Box
                                      fontSize={20}
                                      letterSpacing={1}
                                      fontWeight={600}
                                    >
                                      Commercial
                                    </Box>
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="The Boat Was Used For"
                                      name="usage"
                                      required
                                      value={values.usage}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue("usage", e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="usage"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Why To Buy This Boat & Reviews"
                                      name="boatReview"
                                      required
                                      value={values.boatReview}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "boatReview",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="boatReview"
                                      className="error-message"
                                    />
                                  </Grid>

                                  <Grid item sm={6}>
                                    <Field
                                      label="Prices US$"
                                      name="price"
                                      required
                                      value={values.price}
                                      type="number"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue("price", +e.target.value)
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="price"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Describe The Damage If Any"
                                      name="accidentDescribe"
                                      value={values.accidentDescribe}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "accidentDescribe",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="accidentDescribe"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <Field
                                      label="Description"
                                      name="description"
                                      value={values.description}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "description",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="description"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    {/* <Field
                                      label="Last Time Maintenance Was"
                                      name="lastMaintenance"
                                      value={values.lastMaintenance}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "lastMaintenance",
                                          e.target.value
                                        )
                                      }
                                    /> */}

                                    <label className="required">
                                      Last Time Maintenance Was
                                    </label>
                                    <Datetime
                                      value={
                                        isUpdate
                                          ? moment(values.lastMaintenance)
                                          : null
                                      }
                                      closeOnSelect
                                      // isValidDate={valid}
                                      onChange={value => {
                                        setFieldValue(
                                          "lastMaintenance",
                                          dateStringFormate(value)
                                        );
                                      }}
                                    />

                                    <ErrorMessage
                                      component="div"
                                      name="lastMaintenance"
                                      className="error-message"
                                    />
                                  </Grid>
                                  <Grid item sm={4}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info required"
                                        component="legend"
                                      >
                                        Any Accident History
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        name="accidentHistory"
                                        value={values.accidentHistory}
                                        className="radioButton"
                                        onChange={e => {
                                          setFieldValue(
                                            "accidentHistory",
                                            e.target.value
                                          );
                                        }}
                                      >
                                        {this.renderRadioButtons()}
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="accidentHistory"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>
                                  <Grid item sm={4}>
                                    <FormControl component="fieldset">
                                      <FormLabel
                                        className="addMarinaStorage-info"
                                        component="legend"
                                      >
                                        Repair History If Any
                                      </FormLabel>
                                      <RadioGroup
                                        row
                                        aria-label="gender"
                                        name="repairHistory"
                                        className="radioButton"
                                        value={values.repairHistory}
                                        onChange={e => {
                                          setFieldValue(
                                            "repairHistory",
                                            e.target.value
                                          );
                                        }}
                                      >
                                        {this.renderRadioButtons()}
                                      </RadioGroup>
                                      <ErrorMessage
                                        component="div"
                                        name="repairHistory"
                                        className="error-message"
                                      />
                                    </FormControl>
                                  </Grid>
                                  <Grid item sm={6}>
                                    <div className="clearfix mt-3">
                                      <div
                                        className="custom-control custom-checkbox float-left mb-none"
                                        onClick={() => setFieldValue("isTaxEnabled", !values.isTaxEnabled)}
                                      >
                                        <input
                                          type="checkbox"
                                          className="custom-control-input"
                                          id="agree"
                                          checked={values.isTaxEnabled}
                                        />
                                        <label
                                          className="custom-control-label font-14 register-custom-control-label mb-0 d-flex cursor-pointer"
                                          for="remember me"
                                        > Enable Tax in original price</label>
                                      </div>
                                    </div>
                                  </Grid>
                                 {values.isTaxEnabled &&  <Grid item sm={6}>
                                    <Field
                                      label="Enter Tax On Boat"
                                      name="tax"
                                      value={values.tax}
                                      type="text"
                                      className="form-control"
                                      onChangeText={e =>
                                        setFieldValue(
                                          "tax",
                                          e.target.value
                                        )
                                      }
                                    />
                                    <ErrorMessage
                                      component="div"
                                      name="tax"
                                      className="error-message"
                                    />
                                  </Grid>}
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                          <Col xs={6}>
                            <Card
                              className="m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2
                          h-100"
                            >
                              <CardContent className="dashboard-forms-field">
                                <Grid xs={12} item>
                                  <Box
                                    fontSize={20}
                                    letterSpacing={1}
                                    fontWeight={600}
                                  >
                                    Other Information
                                  </Box>
                                </Grid>
                                <Grid container>
                                  <Grid item sm={6}>
                                    <div className="d-flex flex-wrap justify-content-center">
                                      {images && images.length
                                        ? images.map((img, index) => (
                                            <>
                                              <div className="addBoat-container mr-2 mb-2">
                                                <img
                                                  src={img}
                                                  key={uuid()}
                                                  alt="upload images"
                                                  height="100px"
                                                  width="100px"
                                                />
                                                <span
                                                  onClick={() =>
                                                    this.handleFileDelete(
                                                      index,
                                                      "images",
                                                      setFieldValue
                                                    )
                                                  }
                                                >
                                                  ×
                                                </span>
                                              </div>
                                            </>
                                          ))
                                        : null}
                                    </div>
                                    <div className="addBoatShow-imgUploader">
                                      <ImageUploader
                                        className="required"
                                        withIcon={true}
                                        buttonText="Upload Images"
                                        onChange={file =>
                                          this.handleFileUpload(
                                            file,
                                            "images",
                                            values.images,
                                            setFieldValue
                                          )
                                        }
                                        maxFileSize={5242880}
                                      />
                                    </div>
                                    <ErrorMessage
                                      component="div"
                                      name="images"
                                      className="error-message text-center"
                                    />
                                  </Grid>
                                  <Grid item sm={6}>
                                    <div className="d-flex flex-wrap justify-content-center">
                                      {layout && layout.length
                                        ? layout.map((img, index) => (
                                            <>
                                              <div className="addBoat-container mr-2 mb-2">
                                                <img
                                                  src={img}
                                                  key={uuid()}
                                                  alt="uploded layout"
                                                  height="100px"
                                                  width="100px"
                                                />
                                                <span
                                                  onClick={() =>
                                                    this.handleFileDelete(
                                                      index,
                                                      "layout",
                                                      setFieldValue
                                                    )
                                                  }
                                                >
                                                  ×
                                                </span>
                                              </div>
                                            </>
                                          ))
                                        : null}
                                    </div>
                                    <div className="addBoatShow-imgUploader">
                                      <ImageUploader
                                        className="required"
                                        withIcon={true}
                                        buttonText="Boat Layout"
                                        onChange={file =>
                                          this.handleFileUpload(
                                            file,
                                            "layout",
                                            values.layout,
                                            setFieldValue
                                          )
                                        }
                                        maxFileSize={5242880}
                                      />
                                    </div>
                                    <ErrorMessage
                                      component="div"
                                      name="layout"
                                      className="error-message text-center"
                                    />
                                  </Grid>
                                </Grid>
                                <Grid item sm={6} className="mt-3 mb-3">
                                  <Field
                                    label="YouTube Video Link"
                                    name="video"
                                    value={values.video}
                                    type="text"
                                    className="form-control"
                                    onChangeText={e =>
                                      setFieldValue("video", e.target.value)
                                    }
                                  />
                                </Grid>
                              </CardContent>
                            </Card>
                          </Col>
                        </Row>

                        <div className="clearfix mt-3">
                          <div
                            className="custom-control custom-checkbox float-left mb-none"
                            onClick={this.onClickAgree}
                          >
                            <input
                              type="checkbox"
                              className="custom-control-input"
                              id="agree"
                              checked={agree}
                            />
                            <label
                              className="custom-control-label font-14 register-custom-control-label mb-0 d-flex cursor-pointer"
                              for="remember me"
                            >
                              I have read and agree to
                              {/* Boats Agreement Fee Service */}
                              <div
                                onClick={this.termsHandler}
                                className="ml-1 darkBlue mr-1"
                              >
                                Boats Agreement Fee Service
                              </div>
                              <Dialog
                                open={termsModal}
                                onClose={this.termsHandler}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                              >
                                <DialogTitle id="alert-dialog-title">
                                  {"Used Boats Agreement Service Fee"}
                                </DialogTitle>
                                <DialogContent>
                                  <DialogContentText
                                    id="alert-dialogquis"
                                    className="d-flex flex-column"
                                  >
                                    <span className="mb-2">
                                      Used Boats Agreement Service Fee
                                    </span>
                                    <span>
                                      Lorem ipsum dolor sit amet, consequis
                                      nostrud exercitation ullamco laboris nisi
                                      ut aliquip ex ea commodo consequat.ctetur
                                      adipiscing elit, sed do eiusmod tquis
                                      nostrud exercitation ullamco
                                    </span>
                                  </DialogContentText>
                                </DialogContent>
                              </Dialog>
                              and understand that this application is to add
                              used boats with AdamSea.Com{" "}
                            </label>
                          </div>
                        </div>
                        {isBoatCreateAndUpdateError && (
                          <ErrorComponent errors={errorMessage} />
                        )}
                        <div className="d-flex justify-content-center">
                          <div
                            className="clearfix mt-3 d-flex justify-content-center"
                            onClick={handleSubmit}
                          >
                            <a
                              type=""
                              className="btn btn-lg btn-primary w-auto primary-button"
                            >
                              {!isUpdate ? "Save" : "Update"}
                            </a>
                          </div>
                          <Button
                            className="profile-button btn-dark mt-3 mb-3 font-weight-400 font13"
                            onClick={() => cancelHandler(history)}
                          >
                            Cancel
                          </Button>
                        </div>
                      </containerFluid>
                    </Form>
                  )}
                ></Formik>
              </div>
            )}
          </div>
        </DashboardLayout>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  success: state.boatReducer.success,
  boatLookUps: state.boatReducer.boatLookups,
  currentUser: state.loginReducer.currentUser,
  boat: state.boatReducer.boat,
  isBoatCreated: state.boatReducer.isBoatCreated,
  isBoatUpdated: state.boatReducer.isBoatUpdated,
  isBoatCreateAndUpdateError: state.boatReducer.isBoatCreateAndUpdateError,
  boatTypes: state.boatReducer.boatTypes,
  errorMessage: state.errorReducer.errorMessage
});

const mapDispatchToProps = dispatch => ({
  getBoatLookUps: () => dispatch(getAllBoatLookUps()),
  saveBoat: data => dispatch(saveBoat(data)),
  updateBoat: data => dispatch(updateBoat(data)),
  getBoatById: data => dispatch(getBoatById(data)),
  getBoatTypeStart: () => dispatch(getBoatTypeStart()),
  clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
  clearAddBoatFlag: () => dispatch(clearAddBoatFlag()),
  clearUpdateBoatFlag: () => dispatch(clearUpdateBoatFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBoat);
