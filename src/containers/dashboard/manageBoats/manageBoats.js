import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ReactTable from "react-table";
import { Switch } from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';

import {
  getAllBoatByUser,
  toggleBoatStatus,
  deleteBoat,
  clearExistingBoat,
  createAuctionRoom,
  clearAuctionFlag
} from "../../../redux/actions";
import { verifiedCheck, readableString } from "../../../helpers/string";
import { viewBoatHandler } from "../../../helpers/boatHelper";
import { SuccessNotify } from "../../../helpers/notification";
import {
  confirmSubmitHandler,
  confirmAuctionHandler,
  createAuctionRoomPopup
} from "../../../helpers/confirmationPopup";

import "react-table/react-table.css";
import "../../../styles/common.scss";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import { pagination } from "../../../util/enums/enums";
import "./AddBoat.scss"
import "../../../styles/manageDashboardTableResponsive.scss"
import { priceFormat } from "../../../util/utilFunctions";

class ManageBoats extends Component {
  state = {};

  static getDerivedStateFromProps(nextProps) {
    const {
      isBoatDeleted,
      auctionSuccess,
      history,
      clearAuctionFlag
    } = nextProps;

    if (isBoatDeleted) {
      SuccessNotify("Boat deleted Successfully");
    }

    if (auctionSuccess) {
      clearAuctionFlag();
      history.push("/dashboard");
    }
    return null;
  }

  async componentDidMount() {
    const { getAllBoatByUser } = this.props;
    await getAllBoatByUser({ page: pagination.PAGE_COUNT, limit: pagination.PAGE_RECORD_LIMIT });
  }

  editBoatHandler = boat => {
    const { boatName, _original } = boat;
    const { history, clearExistingBoat } = this.props;
    clearExistingBoat();
    history.push({
      pathname: "/edit-boat",
      search: `?id=${_original.id}&name=${readableString(boatName)}`
    });
  };

  render() {
    const {
      userBoats,
      history,
      toggleBoatStatus,
      deleteBoat,
      createAuctionRoom
    } = this.props;

    const columns = [
      {
        Header: "Boat Name",
        accessor: "boatName"
      },
      {
        Header: "Ad ID",
        accessor: "adId"
      },
      {
        Header: "Boat Price",
        accessor: "price",
        Cell: data => <span>{priceFormat(data.value)} </span>
      },
      // {
      // 	Header: 'Boat Status',
      // 	accessor: 'status',
      // 	Cell: data => (
      // 		<Switch
      // 			onChange={() => toggleBoatStatus({ id: data.row.id, columnName: 'status', value: data.row.status })}
      // 			value={data.row.status}
      // 			inputProps={{ 'aria-label': 'secondary checkbox' }}
      // 		/>
      // 	),
      // },
      {
        Header: "Ad Status",
        accessor: "adStatus",
        Cell: data => (
          <span className={`bg-green-color font-13 text-capitalize m-auto ${verifiedCheck(data.row.adStatus)}`}>
            {verifiedCheck(data.row.adStatus)}
          </span>
        )
      },
      {
        Header: "Auction",
        accessor: "auction",
        Cell: data =>
          data.row.adStatus && (
            <div className="d-flex flex-row w-100 justify-center auction">
              <button
                type="button"
                onClick={() =>
                  createAuctionRoomPopup(createAuctionRoom, data.original.id)
                }
                className="btn btn-outline-primary "
              >
                Add Auction
              </button>
            </div>
          )
      },
      {
        Header: "Actions",
        Cell: data => (
          <div className="d-flex flex-row justify-content-around action">
            <button
                type="button"
                className="btn btn-outline-success mr-2"
                onClick={() => this.editBoatHandler(data && data.row)}
              >
                Edit
              </button>

            <button
                type="button"
                className="btn btn-outline-info mr-2"
                onClick={() => viewBoatHandler(data.original, history)}
              >
                View
              </button>

            <button
                type="button"
                className="btn btn-outline-danger mr-2"
                onClick={() => confirmSubmitHandler(deleteBoat, data.original.id)}
              >
                Delete
              </button>
          </div>
        )
      }
    ];

    return (
      <DashboardLayout>
        <div className="row pl-3 pr-3 m-0  manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Manage Boats</h4>
                <button
                  type="button"
                  class="btn btn-primary primary-button"
                  onClick={() => {
                    history.push("/add-boat");
                  }}
                >
                  Add Boat
                </button>
                {/* <Link to="" >Add Boat</Link> */}
              </div>
              <div className="card-body mt-0">
                <div className="table-responsive">
                  {userBoats && (
                    <ReactTable
                      columns={columns}
                      data={userBoats}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                      className="-striped -highlight"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  userBoats: state.boatReducer.userBoats,
  isBoatDeleted: state.boatReducer.isBoatDeleted,
  auctionSuccess: state.boatReducer && state.boatReducer.auctionSuccess
});

const mapDispatchToProps = dispatch => ({
  getAllBoatByUser: data => dispatch(getAllBoatByUser(data)),
  toggleBoatStatus: data => dispatch(toggleBoatStatus(data)),
  deleteBoat: data => dispatch(deleteBoat(data)),
  clearExistingBoat: () => dispatch(clearExistingBoat()),
  createAuctionRoom: data => dispatch(createAuctionRoom(data)),
  clearAuctionFlag: () => dispatch(clearAuctionFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageBoats);
