import React, { Component, Fragment } from "react";
import uuid from "uuid/v4";
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  RadioGroup,
  Radio,
  MenuItem,
  Select,
  Chip,
  Button,
  Box
} from "@material-ui/core";
import HelpIcon from '@material-ui/icons/Help';
import { connect } from "react-redux";
import { Formik, ErrorMessage, Form } from "formik";
import * as Yup from "yup";
import ImageUploader from "react-images-upload";
import { Card, Container, Row, Col } from "react-bootstrap";

import {
  clearMarinaFlag,
  createMarinaStorage,
  getAllMarina,
  getSingleMarina,
  updateMarina,
  clearEditMarinaFlag,
  getMoreServices
} from "../../../redux/actions/marinaAndStorageAction";
import { Field, Loader } from "../../../components";
import { uploadToS3 } from "../../../helpers/s3FileUpload";
import GoogleMap from "../../../components/map/map";

import "../../../components/marinaStorage/marinaStorage.scss";
import { randomAdId, renderSelectOptions, getIds } from "../../../helpers/string";
import { getTypeWiseLookup } from "../../../redux/actions/marinaAndStorageAction";
import { InfoNotify, SuccessNotify } from "../../../helpers/notification";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import MultiSelect from "../../../components/helper/multiSelect";

import "../../../styles/common.scss";
import ErrorComponent from "../../../components/error/errorComponent";
import { clearErrorMessageShow } from "../../../redux/actions";

class addMarinaStorage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latLng: { lat: 23.0225, lng: 72.5714 },
      selected: [],
      location: {},
      editMarinaId:
        props.location && props.location.state && props.location.state.id,
      marianAndStorage: {
        address: "",
        city: "",
        country: "",
        latitude: "",
        longitude: "",
        placeName: "",
        postalCode: "",
        route: "",
        state: "",
        provider: "",
        noneMembersRate: "",
        membershipBenefits: "",
        memberEvents: "",
        promotionRunning: "",
        exclusiveFacilities: "",
        serviceDescription: "",
        membersReview: "",
        newsEvents: "",
        berth: "",
        facilities: "",
        storage: "",
        membersRate: "",
        serviceProvide: [],
        salesmanFullName: "",
        salesmanContact: "",
        contactPersonImage: "",
        youtubeLinkVideo: "",
        images: [],
        dockLayout: "",
        storageLayout: "",
      },
      adId: "",
      mediaUpdated: false,
      infoSwp: false,
      infoContact: false,
      infoVideo: false,
    };
  }

  fetchMapInfo = (result, setValue) => {
    const { location } = this.state;

    location.placeName = result.place_name;
    location.country = result.country;
    location.state = result.state;
    location.postalCode = result.postalCode || "";
    location.city = result.city;
    location.address = result.address;
    location.longitude = result.longitude;
    location.latitude = result.latitude;
    location.route = result.route || "";

    setValue("address", result.address);
  };

  componentDidMount() {
    const { getMoreServices, getTypeWiseLookup, getSingleMarina } = this.props;
    const { editMarinaId } = this.state;

    getMoreServices();
    getTypeWiseLookup("You are An");
    editMarinaId && getSingleMarina(editMarinaId);
  }

  static getDerivedStateFromProps(props, state) {
    const {
      createSuccess,
      clearMarinaFlag,
      history,
      singleSuccess,
      updateSuccess,
      clearEditMarinaFlag
    } = props;
    const { adId } = state;

    if (createSuccess) {
      setTimeout(() => SuccessNotify("Marina & Storage is added successfully"), 100);
      history.push("/manage-marina-storage");
    } else if (!adId) {
      return {
        adId: randomAdId("MS")
      };
    }
    if (singleSuccess) {
      clearMarinaFlag();
    }

    if (updateSuccess) {
      clearEditMarinaFlag();
      setTimeout(() => SuccessNotify("Marina & Storage is updated successfully"), 100);
      history.push("/manage-marina-storage");
    }
    return null;
  }

  cancelHandler = () => {
    const { history, clearEditMarinaFlag } = this.props;
    clearEditMarinaFlag();
    history.push("/manage-marina-storage");
  };

  menuItems(values) {
    if (values) {
      return values.map(name => (
        <MenuItem key={uuid()} value={name.alias}>
          {name.alias}
        </MenuItem>
      ));
    }
  }

  handleFileUpload = async (file, name, setValue) => {
    const { marianAndStorage } = this.state;

    if (file.length) {
      let res = await uploadToS3(file[file.length - 1]);
      if (res.location) {
        marianAndStorage[name] = res.location;
        setValue(`${name}`, res.location);
        this.setState({ marianAndStorage });
      }
    }
  };

  handleMultipleFileUpload = async (file, name, value, setValue) => {
    const { marianAndStorage } = this.state;
    let oldValues = marianAndStorage[name];
    let formValue = value || [];

    if (file && file.length) {
      let res = await uploadToS3(file[file.length - 1]);

      if (res && res.location) {
        const newValues = oldValues.concat(res.location);
        setValue(name, formValue.concat(res.location));
        marianAndStorage[name] = newValues;
        this.setState({ marianAndStorage });
      }
    }
  };

  handleFileDelete = async (name, setValue) => {
    const { marianAndStorage } = this.state;
    const oldValues = marianAndStorage[name];

    if (oldValues && oldValues.length) {
      marianAndStorage[name] = "";

      setValue(`${name}`, marianAndStorage[name]);
      this.setState({ marianAndStorage });
    }
  };
  handleMultipleFileDelete = async (index, name, setValue) => {
    const { marianAndStorage } = this.state;
    const oldValues = marianAndStorage[name];

    if (oldValues && oldValues.length) {
      marianAndStorage[name].splice(index, 1);

      setValue(name, marianAndStorage[name]);
      this.setState({ marianAndStorage });
    }
  };

  renderRadioItems = items => {
    return items.map(item => {
      return (
        <FormControlLabel
          key={uuid()}
          label={item.alias}
          control={<Radio value={item.id} />}
        />
      );
    });
  };

  openInfo = (field) => {
    this.setState(prevState => ({
      [field]: !prevState[field]
    }))
  }
  handleChange = e => {
    this.setState({ selected: e.target.value });
  };

  prepareMarinaValues = () => {
    const { editMarina } = this.props;
    const {
      location,
      marianAndStorage,
      adId,
      mediaUpdated,
      editMarinaId
    } = this.state;
    if (editMarinaId && editMarina.hasOwnProperty("id")) {
      if (!mediaUpdated) {
        const {
          address,
          city,
          country,
          geometricLocation,
          placeName,
          postalCode,
          route,
          state,
          provider,
          noneMembersRate,
          membershipBenefits,
          memberEvents,
          promotionRunning,
          exclusiveFacilities,
          serviceDescription,
          membersReview,
          newsEvents,
          berth,
          facilities,
          storage,
          membersRate,
          serviceProvide,
          salesmanFullName,
          salesmanContact,
          contactPersonImage,
          youtubeLinkVideo,
          images,
          adId
        } = editMarina;

        location.address = address;
        location.country = country;
        location.state = state;
        location.city = city;
        location.placeName = placeName;
        location.postalCode = postalCode;
        location.latitude =
          geometricLocation &&
          geometricLocation.coordinates.length > 0 &&
          geometricLocation.coordinates[1];
        location.longitude =
          geometricLocation &&
          geometricLocation.coordinates.length > 0 &&
          geometricLocation.coordinates[0];

        marianAndStorage.adId = adId;
        marianAndStorage.provider = provider;
        marianAndStorage.noneMembersRate = noneMembersRate;
        marianAndStorage.membershipBenefits = membershipBenefits;
        marianAndStorage.memberEvents = memberEvents;
        marianAndStorage.promotionRunning = promotionRunning;
        marianAndStorage.exclusiveFacilities = exclusiveFacilities;
        marianAndStorage.membersReview = membersReview;
        marianAndStorage.serviceDescription = serviceDescription;
        marianAndStorage.newsEvents = newsEvents;
        marianAndStorage.berth = berth;
        marianAndStorage.facilities = facilities;
        marianAndStorage.storage = storage;
        marianAndStorage.membersRate = membersRate;
        marianAndStorage.salesmanFullName = salesmanFullName;
        marianAndStorage.salesmanContact = salesmanContact;
        marianAndStorage.contactPersonImage = contactPersonImage;
        marianAndStorage.youtubeLinkVideo = youtubeLinkVideo;
        marianAndStorage.images = images;

        marianAndStorage.serviceProvide = renderSelectOptions(serviceProvide,"name", "id")

        this.setState({
          marianAndStorage,
          location,
          latLng: { lat: location.latitude, lng: location.longitude },
          adId,
          mediaUpdated: true
        });
      }

      const {
        createdAt,
        __typename,
        userStatus,
        accountStatus,
        reviews,
        userCount,
        user,
        ...filteredEditMarina
      } = editMarina;

      return {
        ...filteredEditMarina,
        serviceProvide: marianAndStorage.serviceProvide
      };
    }
  };

  render() {
    const {
      typeWiseLookup,
      editMarina,
      isLoading,
      createError,
      updateError,
      errorMessage,
      clearErrorMessageShow,
      clearMarinaFlag
    } = this.props;

    let { moreMarinaService } = this.props
    moreMarinaService = renderSelectOptions(moreMarinaService,"name", "id")

    const {
      latLng,
      marianAndStorage,
      adId,
      editMarinaId,
      location,
      infoSwp,
      infoContact,
      infoVideo,
    } = this.state;

    const initValue =
      editMarinaId && editMarina && editMarina.hasOwnProperty("id")
        ? this.prepareMarinaValues()
        : marianAndStorage;

    const { serviceProvide } = marianAndStorage;

    return (
      <Fragment>
        <DashboardLayout>
          {isLoading ? (
            <Loader></Loader>
          ) : (
            <Formik
              initialValues={{...initValue, images: marianAndStorage.images}}
              onSubmit={values => {
                const { createMarinaStorage, updateMarina } = this.props;
                const { editMarinaId, location } = this.state;
                if (!Object.keys(location).length) {
                  InfoNotify("please add location");
                } else {
                  if (location) {
                    delete values.latitude;
                    delete values.longitude;

                    values.serviceProvide = values.serviceProvide.map((item) => item.value)
                    values.address = location.address;
                    values.city = location.city;
                    values.country = location.country;
                    values.geometricLocation = {
                      coordinates: [location.longitude, location.latitude]
                    };
                    values.placeName = location.placeName;
                    values.postalCode = location.postalCode;
                    values.route = location.route;
                    values.state = location.state;
                  }
                  values.images = marianAndStorage.images;
                  values.adId = adId;                  

                  if (editMarinaId) {
                    if (values.provider.hasOwnProperty("id")) {
                      values.provider = values.provider.id;
                    }
                    clearMarinaFlag()
                    clearErrorMessageShow()
                    updateMarina(values);
                  } else {
                    clearMarinaFlag()
                    clearErrorMessageShow()
                    createMarinaStorage(values);
                  }
                }
              }}
              validationSchema={Yup.object().shape({
                provider: Yup.string().required("Provider field is required."),
                membershipBenefits: Yup.string().required(
                  "Membership Benefit field is required."
                ),
                memberEvents: Yup.string().required(
                  "Members Only Events field is required."
                ),
                exclusiveFacilities: Yup.string().required(
                  "Exclusive Facilities field is required."
                ),
                membersReview: Yup.string().required(
                  "What Our Members Telling About Us field is required."
                ),
                serviceDescription: Yup.string().required(
                  "Service Description field is required."
                ),
                berth: Yup.string().required("Our Berth field is required."),
                facilities: Yup.string().required(
                  "Special About Our Facilities is required."
                ),
                storage: Yup.string().required(
                  "Our Storage field is required."
                ),
                serviceProvide: Yup.string().required(
                  "Service we provide field is required."
                ),
                salesmanFullName: Yup.string().required(
                  "SalesMan Name field is required."
                ),
                salesmanContact: Yup.string().required(
                  "Contact No. Of Salesman field is required."
                ),
                contactPersonImage: Yup.string().required(
                  "Contact Person Photo field is required."
                ),
                address: Yup.string().required(
                  "Marina & Storage Location field is required."
                ),
                images: Yup.array().min(5, "Minimum 5 photos upload required")
              })}
              render={({
                errors,
                status,
                touched,
                values,
                setFieldValue,
                handleSubmit
              }) => (
                <div>
                  <Form>
                    <Container fluid={true}>
                      <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                        <Box
                          fontSize={20}
                          letterSpacing={1}
                          fontWeight={600}
                          className="map-title"
                        >
                          {`Marina and Storage Location`}{" "}
                          <span className="font-weight-400 font-14">
                            Ad ID: {`${adId}`}
                          </span>
                        </Box>
                      </div>
                      <Row className="mr-0 ml-0 mb-3">
                        <Col xs={7} className="map-div-form pl-0 pr-0">
                          <GoogleMap
                            latLng={latLng}
                            fetch={result =>
                              this.fetchMapInfo(result, setFieldValue)
                            }
                            height={38}
                            width={100}
                            placeHolder={"Marina and Storage Location"}
                            columnName={"address"}
                            isError={errors.address}
                            value={location}
                            isUpdate
                          ></GoogleMap>
                        </Col>
                        <Col xs={5} className="pr-0 pl-3">
                          <Card
                            className="marina-card mt-0 mb-0 
                          border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0"
                          >
                            <Card.Title className="ml-0">
                              Information
                            </Card.Title>
                            <FormControl
                              component="fieldset"
                              onChange={e => {
                                setFieldValue("provider", e.target.value);
                              }}
                            >
                              <FormLabel
                                className="addMarinaStorage-info"
                                component="legend"
                              >
                                <label className="required">You are An</label>
                              </FormLabel>
                              <RadioGroup
                                row
                                aria-label="gender"
                                className="radioButton radio-button-type-font"
                                defaultValue={values.provider.id}
                              >
                                {typeWiseLookup &&
                                  this.renderRadioItems(typeWiseLookup)}
                              </RadioGroup>
                            </FormControl>
                            <ErrorMessage
                              component="div"
                              name="provider"
                              className="error-message mt-0 mb-4"
                            />
                            <Row className="dashboard-forms-field">
                              <Col>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Membership Benefit"
                                    id={"membershipBenefits"}
                                    name={"membershipBenefits"}
                                    value={values.membershipBenefits}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "membershipBenefits",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="membershipBenefits"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Members Only Events"
                                    id={"memberEvents"}
                                    name={"memberEvents"}
                                    value={values.memberEvents}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "memberEvents",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="memberEvents"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Exclusive Facilities"
                                    id={"exclusiveFacilities"}
                                    name={"exclusiveFacilities"}
                                    value={values.exclusiveFacilities}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "exclusiveFacilities",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="exclusiveFacilities"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Special About Our Facilities"
                                    id={"facilities"}
                                    name={"facilities"}
                                    value={values.facilities}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "facilities",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="facilities"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    label="Our News And Events"
                                    id={"newsEvents"}
                                    name={"newsEvents"}
                                    value={values.newsEvents}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "newsEvents",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="newsEvents"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    label="Members Rates"
                                    id={"membersRate"}
                                    name={"membersRate"}
                                    value={values.membersRate}
                                    type="number"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "membersRate",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="membersRate"
                                    className="error-message"
                                  />
                                </div>
                              </Col>
                              <Col>
                                <div className="mb-2">
                                  <Field
                                    label="None Members Rates"
                                    id={"noneMembersRate"}
                                    value={values.noneMembersRate}
                                    name={"noneMembersRate"}
                                    type="number"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "noneMembersRate",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="noneMembersRate"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Service We Provide"
                                    id={"serviceDescription"}
                                    name={"serviceDescription"}
                                    value={values.serviceDescription}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "serviceDescription",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="serviceDescription"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    label="Running Promotion"
                                    id={"promotionRunning"}
                                    name={"promotionRunning"}
                                    value={values.promotionRunning}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "promotionRunning",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="promotionRunning"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="What Our Members Telling About Us"
                                    id={"membersReview"}
                                    name={"membersReview"}
                                    value={values.membersReview}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue(
                                        "membersReview",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="membersReview"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Our Berth"
                                    id={"berth"}
                                    name={"berth"}
                                    value={values.berth}
                                    type="textarea"
                                    onChangeText={e => {
                                      setFieldValue("berth", e.target.value);
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="berth"
                                    className="error-message"
                                  />
                                </div>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Our Storage"
                                    id={"storage"}
                                    name={"storage"}
                                    type="textarea"
                                    value={values.storage}
                                    onChangeText={e => {
                                      setFieldValue("storage", e.target.value);
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="storage"
                                    className="error-message"
                                  />
                                </div>
                              </Col>
                            </Row>
                          </Card>
                        </Col>
                      </Row>
                      <Row className="ml-0 mr-0">
                        <Col xs={7} className="pl-0 pr-0">
                          <Card
                            className="marina-card marina-field m-0
                           border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2 h-100"
                          >
                            <Card.Title className="ml-0">
                              Information About The Salesman
                            </Card.Title>
                            {/* start */}

                            <Row className="dashboard-forms-field">
                              <Col xs={6}>
                                <div className="mb-2">
                                  <Field
                                    required
                                    label="Full Name Of Salesman To Contact"
                                    id={"salesmanFullName"}
                                    name={"salesmanFullName"}
                                    type="text"
                                    value={values.salesmanFullName}
                                    onChangeText={e => {
                                      setFieldValue(
                                        "salesmanFullName",
                                        e.target.value
                                      );
                                    }}
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="salesmanFullName"
                                    className="error-message"
                                  />
                                </div>
                                {/* </Col>
                                <Col> */}
                                <div className="mb-2">
                                  <Field
                                    type="number"
                                    label="Contact No. Of Salesman"
                                    id={"salesmanContact"}
                                    name={"salesmanContact"}
                                    value={values.salesmanContact}
                                    onChangeText={e => {
                                      setFieldValue(
                                        "salesmanContact",
                                        e.target.value
                                      );
                                    }}
                                    required
                                  />
                                  <ErrorMessage
                                    component="div"
                                    name="salesmanContact"
                                    className="error-message"
                                  />
                                </div>
                                {/* </Col>
                                </Row>
                                <Row>
                                <Col xs={6}> */}
                                 <div className="d-flex justify-content-between">
                                  <label className="required">Youtube Video Link </label> 
                                  <div className="position-relative">
                                    <HelpIcon onMouseEnter={() => { this.openInfo('infoVideo') }} onMouseLeave={() => { this.openInfo('infoVideo') }}  className="hint-info" />
                                      {infoVideo && <span class="info-class">Let other user see your video about your boat yard or storage</span>} 
                                  </div>
                                </div>
                                <Field
                                  id={"youtubeLinkVideo"}
                                  name={"youtubeLinkVideo"}
                                  value={values.youtubeLinkVideo}
                                  type="text"
                                  onChangeText={e => {
                                    setFieldValue(
                                      "youtubeLinkVideo",
                                      e.target.value
                                    );
                                  }}
                                />

                              { values.provider 
                                && typeWiseLookup
                                && typeWiseLookup.length 
                                && typeWiseLookup.find((item) => item.id === values.provider) && (typeWiseLookup.find((item) => item.id === values.provider).alias === 'Storage' ||
                                typeWiseLookup.find((item) => item.id === values.provider).alias === 'Marina & Storage' )?<>
                                <div 
                                className="d-flex 
                                flex-wrap mt-3 
                                flex-direction-column-single 
                                justify-content-center 
                                align-center
                                add-marina-contact-person-image
                                ">
                               
                                <Field
                                      label="Add The Contact Person Photo"
                                      id="storageLayout"
                                      name="storageLayout"
                                      type="single-image"
                                      value={values.storageLayout}
                                      onChangeText={setFieldValue}
                                />
                              </div>
                              <ErrorMessage
                                component="div"
                                name="storageLayout"
                                className="error-message text-center mt-0"
                              />
                                 </>   : <> </>}
                              </Col>
                              <Col xs={6}>
                                <Card.Title className="text-center">
                                <div className="d-flex justify-content-between align-center">
                                  <label className="required">Contact Person Photo </label> 
                                  <div className="position-relative d-flex">
                                    <HelpIcon onMouseEnter={() => { this.openInfo('infoContact') }} onMouseLeave={() => { this.openInfo('infoContact') }}  className="hint-info" />
                                      {infoContact && <span class="info-class">Add Your sales man photo here so any one can contact directly</span>} 
                                  </div>
                                </div>
                                </Card.Title>
                                <div className="d-flex flex-wrap justify-content-center add-marina-contact-person-image">
                                  <Field
                                      id="contactPersonImage"
                                      name="contactPersonImage"
                                      type="single-image"
                                      value={values.contactPersonImage}
                                      onChangeText={setFieldValue}
                                      required
                                    />
                                </div>
                                <ErrorMessage
                                  component="div"
                                  name="contactPersonImage"
                                  className="error-message text-center mt-0"
                                />

                              { values.provider 
                                && typeWiseLookup
                                && typeWiseLookup.length 
                                && typeWiseLookup.find((item) => item.id === values.provider) &&
                                ( typeWiseLookup.find((item) => item.id === values.provider).alias === 'Marina' ||
                                typeWiseLookup.find((item) => item.id === values.provider).alias === 'Marina & Storage' ) ?
                                <>
                                <div className="
                                d-flex 
                                flex-direction-column-single
                                align-center
                                mt-3
                                flex-wrap justify-content-center
                                add-marina-contact-person-image">
                                
                                <Field
                                      label="Add The Contact Person Photo"
                                      id="dockLayout"
                                      name="dockLayout"
                                      type="single-image"
                                      value={values.dockLayout}
                                      onChangeText={setFieldValue}
                                />
                              </div>
                              
                                 </>   :
                                  <> </>}
                              </Col>
                            </Row>

                            {/* end */}
                          </Card>
                        </Col>
                        <Col xs={5} className="pr-0 pl-3">
                          <Row className="h-100">
                            <Col>
                              <Card className="marina-card m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2 h-100">
                                <Card.Title>
                                  <label className="required">
                                    Upload Pictures
                                  </label>
                                </Card.Title>
                                <div className="d-flex flex-wrap justify-content-center">
                                  {marianAndStorage.images &&
                                  marianAndStorage.images.length
                                    ? marianAndStorage.images.map(
                                        (img, index) => (
                                          <>
                                            <div className="addBoat-container mr-2 mb-2">
                                              <img
                                                src={img}
                                                key={uuid()}
                                                alt="uploaded pictures"
                                                height="100px"
                                                width="100px"
                                              />
                                              <span
                                                onClick={() =>
                                                  this.handleMultipleFileDelete(
                                                    index,
                                                    "images",
                                                    setFieldValue
                                                  )
                                                }
                                              >
                                                ×
                                              </span>
                                            </div>
                                          </>
                                        )
                                      )
                                    : null}
                                </div>
                                <div className="addBoatShow-imgUploader ">
                                  <ImageUploader
                                    withIcon={true}
                                    buttonText="Upload Images"
                                    onChange={file =>
                                      this.handleMultipleFileUpload(
                                        file,
                                        "images",
                                        values.images,
                                        setFieldValue
                                      )
                                    }
                                    maxFileSize={5242880}
                                  />
                                </div>
                                <ErrorMessage
                                  component="div"
                                  name="images"
                                  className="error-message text-center mt-0"
                                />
                              </Card>
                            </Col>
                            <Col className="pl-0">
                              <Card className="multi-select-chip-margin marina-card m-0 marina-field ml-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-2 border-bottom-left-radius-2">
                                <Card.Title>
                                <div className="d-flex justify-content-between">
                                  <label className="required">Service We Provide</label> 
                                  <div className="position-relative">
                                    <HelpIcon onMouseEnter={() => { this.openInfo('infoSwp') }} onMouseLeave={() => { this.openInfo('infoSwp') }}  className="hint-info" />
                                      {infoSwp && <span class="info-class">Click in the box and select multiple items</span>} 
                                  </div>
                                </div>
                                </Card.Title>
                                <FormControl className="select-container">
                                  <MultiSelect
                                    selectedOption={values.serviceProvide}
                                    onChangeValue={item => {
                                      setFieldValue("serviceProvide", [...item])}}
                                    options={moreMarinaService}
                                    />
                                </FormControl>
                                <ErrorMessage
                                  component="div"
                                  name="serviceProvide"
                                  className="error-message"
                                />
                              </Card>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      {(createError || updateError) ? <ErrorComponent errors={errorMessage} /> : <> </> }
                      <div className="d-flex justify-content-center">
                        <Button
                          type="button"
                          className="button btn btn-primary w-auto addBoatService-btn primary-button"
                          onClick={handleSubmit}
                        >
                          {editMarinaId ? "Update" : "Save"}
                        </Button>
                        <Button
                          className="profile-button btn-dark"
                          onClick={this.cancelHandler}
                        >
                          Cancel
                        </Button>
                      </div>
                    </Container>
                  </Form>
                </div>
              )}
            ></Formik>
          )}
        </DashboardLayout>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  createSuccess: state.marinaAndStorageReducer.createSuccess,
  createError: state.marinaAndStorageReducer.createError,
  typeWiseLookup:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaTypeWiseLookUps,
  editMarina:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.editMarina,
  isLoading:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.isLoading,
  singleSuccess:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.singleSuccess,
  updateSuccess:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.updateSuccess,
  updateError:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.updateError,
  moreMarinaService: state.marinaAndStorageReducer.moreMarinaService,
  errorMessage: state.errorReducer && state.errorReducer.errorMessage,

});

const mapDispatchToProps = dispatch => ({
  clearMarinaFlag: () => dispatch(clearMarinaFlag()),
  createMarinaStorage: data => dispatch(createMarinaStorage(data)),
  getTypeWiseLookup: data => dispatch(getTypeWiseLookup(data)),
  getSingleMarina: data => dispatch(getSingleMarina(data)),
  updateMarina: data => dispatch(updateMarina(data)),
  clearEditMarinaFlag: () => dispatch(clearEditMarinaFlag()),
  getMoreServices: () => dispatch(getMoreServices()),
  clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
});

export default connect(mapStateToProps, mapDispatchToProps)(addMarinaStorage);
