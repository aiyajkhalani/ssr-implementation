import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch } from "@material-ui/core";
import ReactTable from "react-table";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";

import { pagination } from "../../../util/enums/enums";
import {
  getUserMarinaStorage,
  deleteMarinaStorage,
  clearMarinaFlag
} from "../../../redux/actions/marinaAndStorageAction";
import { confirmSubmitHandler } from "../../../helpers/confirmationPopup";
import { SuccessNotify } from "../../../helpers/notification";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";

import "../../../styles/manageDashboardTableResponsive.scss";

class ManageMarinaStorage extends Component {
  componentDidMount() {
    const { getUserMarinaStorage, createSuccess, clearMarinaFlag } = this.props;

    if (createSuccess) {
      SuccessNotify("Marina Added SuccessFully!");
      clearMarinaFlag();
    }

    getUserMarinaStorage({
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    });
  }

  editMarina = data => {
    const { history } = this.props;
    history.push("add-marina-storage", { id: data });
  };

  static getDerivedStateFromProps(props, state) {
    const {
      getSuccess,
      clearMarinaFlag,
      deleteSuccess,
      getUserMarinaStorage,
      createSuccess,
      history
    } = props;

    if (getSuccess) {
      clearMarinaFlag();
    }

    if (deleteSuccess) {
      clearMarinaFlag();
    }
    return null;
  }

  render() {
    const { marinaStorage, history, deleteMarinaStorage } = this.props;

    const columns = [
      {
        Header: "Ad ID",
        accessor: "adId"
      },
      {
        Header: "Salesman name",
        accessor: "salesmanFullName"
      },
      {
        Header: "Salesman Contact",
        accessor: "salesmanContact"
      },
      {
        Header: "Status",
        accessor: "status",
        Cell: data => (
          <div className="d-flex justify-content-center">
            <Switch
              checked={data.row.status}
              // onChange={() => toggleBoatStatus({ id: data.row.original.id, columnName: 'AdStatus' })}
              value={data.row.AdStatus}
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          </div>
        )
      },
      {
        Header: "Actions",
        Cell: data => (
          <div className="d-flex flex-row justify-content-around action">
            <button
              type="button"
              className="btn btn-outline-success mr-2"
			  onClick={() => this.editMarina(data.original.id)}
            >
              Edit
            </button>

            <button
              type="button"
              className="btn btn-outline-info mr-2"
            >
              View
            </button>

            <button
              type="button"
              className="btn btn-outline-danger mr-2"
              onClick={() =>
                confirmSubmitHandler(deleteMarinaStorage, data.original.id)
              }
            >
              Delete
            </button>

            
          </div>
        )
      }
    ];
    return (
      <DashboardLayout>
        <div className="row mr-0 ml-0 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Manage Marina & Storage</h4>
                <button
                  type="button"
                  className="btn btn-primary  primary-button"
                  onClick={() => {
                    history.push("/add-marina-storage");
                  }}
                >
                  Add Marina & Storage
                </button>
              </div>
              <div className="card-body mt-0">
                <div className="table-responsive">
                  <ReactTable
                    columns={columns}
                    data={marinaStorage}
                    defaultFilterMethod={(filter, row) =>
                      String(row[filter.id]).includes(filter.value)
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  marinaStorage:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.marinaStorage,
  getSuccess:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.getSuccess,
  deleteSuccess:
    state.marinaAndStorageReducer &&
    state.marinaAndStorageReducer.deleteSuccess,
  createSuccess:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.createSuccess
});

const mapDispatchToProps = dispatch => ({
  getUserMarinaStorage: data => dispatch(getUserMarinaStorage(data)),
  deleteMarinaStorage: data => dispatch(deleteMarinaStorage(data)),
  clearMarinaFlag: () => dispatch(clearMarinaFlag())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageMarinaStorage);
