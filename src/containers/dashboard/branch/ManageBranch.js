import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ReactTable from "react-table";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";

import { Layout } from "../../../components";
import {
  getAllBranches,
  deleteBranch,
  changeBranchStatus,
  clearBranchFlag
} from "../../../redux/actions";
import { confirmSubmitHandler } from "../../../helpers/confirmationPopup";
import { SuccessNotify } from "../../../helpers/notification";
import { Switch } from "@material-ui/core";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import "../../../styles/manageDashboardTableResponsive.scss";

class ManageBranch extends Component {
  static getDerivedStateFromProps(nextProps) {
    const { isBranchDeleted, clearBranchFlag, statusChanged } = nextProps;

    if (isBranchDeleted) {
      clearBranchFlag();
      SuccessNotify("Branch deleted Successfully");
    } else if (statusChanged) {
      clearBranchFlag();
      SuccessNotify("Branch status Successfully");
    }
    return null;
  }

  async componentDidMount() {
    const { getAllBranches } = this.props;
    await getAllBranches();
  }

  editBranchHandler = id => {
    const { history } = this.props;
    history && history.push(`/edit-branch/${id}`);
  };

  render() {
    const { branches, history, deleteBranch, changeBranchStatus } = this.props;

    const columns = [
      {
        Header: "Contact Name",
        accessor: "contactName"
      },
      {
        Header: "City",
        accessor: "city"
      },
      {
        Header: "Verification",
        accessor: "branchVerificationStatus",
        Cell: data => (
          <div className="d-flex justify-content-center">
            <Switch
              checked={data.value}
              value={data.value}
              inputProps={{ "aria-label": "secondary checkbox" }}
              onClick={() =>
                changeBranchStatus({
                  id: data.original.id,
                  value: data.value,
                  columnName: "branchVerificationStatus"
                })
              }
            />
          </div>
        )
      },
      {
        Header: "Status",
        accessor: "branchStatus",
        Cell: data => (
          <div className="d-flex justify-content-center">
            <Switch
              checked={data.value}
              value={data.value}
              inputProps={{ "aria-label": "secondary checkbox" }}
              onClick={() =>
                changeBranchStatus({
                  id: data.original.id,
                  value: data.value,
                  columnName: "branchStatus"
                })
              }
            />
          </div>
        )
      },
      {
        Header: "Survey Inquiries"
      },
      {
        Header: "Actions",
        accessor: "id",
        Cell: data => (
          <div className="d-flex flex-row justify-content-around action">
            <button
              type="button"
              className="btn btn-outline-success mr-2"
              onClick={() => this.editBranchHandler(data.value)}
            >
              Edit
            </button>

            <button
              type="button"
              className="btn btn-outline-info mr-2"
            >
              View
            </button>

            <button
              type="button"
              className="btn btn-outline-danger mr-2"
              onClick={() =>
                confirmSubmitHandler(
                  deleteBranch,
                  data &&
                    data.row &&
                    data.row._original &&
                    data.row._original.id
                )
              }
            >
              Delete
            </button>
          </div>
        )
      }
    ];

    return (
      <DashboardLayout>
        <div className="row pl-3 pr-3 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header d-flex justify-content-between">
                <h4 className="card-title">Manage Branches</h4>
                <button
                  type="button"
                  className="btn btn-primary primary-button"
                  onClick={() => {
                    history.push("/add-branch");
                  }}
                >
                  Add Branch
                </button>
              </div>

              <div className="card-body mt-0">
                <div className="table-responsive">
                  {branches && (
                    <ReactTable
                      columns={columns}
                      data={branches}
                      className="-striped -highlight"
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  branches: state.branchReducer.branches,
  isBranchDeleted: state.branchReducer.isBranchDeleted,
  statusChanged: state.branchReducer.statusChanged
});

const mapDispatchToProps = dispatch => ({
  getAllBranches: () => dispatch(getAllBranches()),
  deleteBranch: data => dispatch(deleteBranch(data)),
  changeBranchStatus: data => dispatch(changeBranchStatus(data)),
  clearBranchFlag: () => dispatch(clearBranchFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageBranch);
