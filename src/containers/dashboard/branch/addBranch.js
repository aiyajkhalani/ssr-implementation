import React, { Component } from "react";
import { connect } from "react-redux";
import * as Yup from "yup";
import { ErrorMessage, Formik, Form } from "formik";
import {
  Grid,
  Card,
  Box,
  Container,
  CardContent,
  TextField,
  Button
} from "@material-ui/core";
import HelpIcon from '@material-ui/icons/Help';

import { Layout, Field, Loader } from "../../../components";
import GoogleMap from "../../../components/map/map";
import { addBranch, clearBranchFlag, clearErrorMessageShow, getSingleBranch, updateBranch } from "../../../redux/actions";
import { cancelHandler } from "../../../helpers/routeHelper";
import { SuccessNotify, ErrorNotify } from "../../../helpers/notification";

import "./branch.scss";
import { Row, Col } from "react-bootstrap";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import ErrorComponent from "../../../components/error/errorComponent";

class AddBranch extends Component {
  state = {
    latLng: { lat: 23.0225, lng: 72.5714 },
    infoMobile: false,
    branchInput: {
      contactName: "",
      mobileNumber: "",
      emailAddress: "",
      position: "",
      pricePerFt: "",
    },
    location: {
      placeName: "",
      country: "",
      state: "",
      postalCode: "",
      city: "",
      address: "",
      latitude: 0.0,
      longitude: 0.0
    },
    isUpdate: false,
    editBranchId: "",

    mediaUpdated: false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isAdded, isAddedError, history, clearBranchFlag, match: {params}, isUpdated, isUpdateError } = nextProps;    

    if (isAdded || isUpdated) {
      clearBranchFlag()
      setTimeout(() => SuccessNotify(`Branch is ${isUpdated ? "updated" : "added"} successfully`), 100);
      history.push("/manage-branches");
    } else if (isAddedError || isUpdateError) {
      clearBranchFlag()
      ErrorNotify("Error occurred");
    } else if(params && params.id) {
      return {
        editBranchId: params.id,
        isUpdate: true
      }
    }

    return null;
  }

  componentDidMount() {
    const { editBranchId } = this.state
    const { clearBranchFlag, clearErrorMessageShow, getSingleBranch } = this.props

    clearBranchFlag()
    clearErrorMessageShow()

    editBranchId && getSingleBranch(editBranchId)
  }

  fetchMapInfo = (result, setValue) => {
    const { location } = this.state;
    const {
      address,
      country,
      state,
      city,
      placeName,
      postalCode,
      route,
      latitude,
      longitude
    } = result;

    if (result) {
      location.address = address;
      location.country = country;
      location.state = state;
      location.city = city;
      location.placeName = placeName;
      location.postalCode = postalCode;
      location.route = route;
      location.latitude = latitude;
      location.longitude = longitude;

      setValue("address", address);
      this.setState({ location });
    }
  };

  prepareValue = () => {
    const { branch } = this.props;
    const {
      location,
      mediaUpdated
    } = this.state;

    if (branch.id) {
      if (!mediaUpdated) {
        const {          
          address,
          country,
          state,
          city,
          placeName,
          postalCode,
          officeLocation,
          route,
        } = branch;

        location.address = address;
        location.country = country;
        location.state = state;
        location.city = city;
        location.placeName = placeName;
        location.postalCode = postalCode;
        location.route = route;
        location.latitude =
          officeLocation &&
          officeLocation.coordinates.length > 0 &&
          officeLocation.coordinates[1];
        location.longitude =
          officeLocation &&
          officeLocation.coordinates.length > 0 &&
          officeLocation.coordinates[0];

        this.setState({
          location,
          latLng: { lat: location.latitude, lng: location.longitude },
          mediaUpdated: true
        });
      }

      return {  
        address: branch.address, 
        contactName: branch.contactName, 
         mobileNumber: branch.mobileNumber, 
         emailAddress: branch.emailAddress, 
        position: branch.position,
        pricePerFt: branch.pricePerFt,
      };
    }
  }

  renderBranchInfo = (setFieldValue, values) => {
    const { infoMobile } = this.state

    return (
      <Card className="card-style m-0 border-top-right-radius-0 border-top-left-radius-0 border-bottom-right-radius-0 border-bottom-left-radius-0
      box-shadow-none map-div h-100">
        <CardContent>
          <Grid container spacing={2}>
            
            <Grid item xs={12}>
              <Box fontSize={20} letterSpacing={1} fontWeight={600}>
                Branch Info
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Field
                  label="Contact Name"
                  type="text"
                  name="contactName"
                  value={values.contactName}
                  className="form-control"
                  onChangeText={e => setFieldValue("contactName", e.target.value)}
                  required
                  />
                <ErrorMessage
                  component="div"
                  name="contactName"
                  className="error-message"
                />
            </Grid>

            <Grid item xs={12}>
            <div className="d-flex justify-content-between">
            <label for="mobileNumber" className="required">Mobile Number</label>
                <div className="position-relative">
                  <HelpIcon onMouseEnter={() => { this.setState({ infoMobile: true }) }} onMouseLeave={() => { this.setState({ infoMobile: false }) }}  style={{ height: '15px', width: '15px' }} />
                    {infoMobile && <span class="info-class">Add Your correct mobile No. to be able to receive SMS notifications whenever you receive survey requirement into your branch</span>} 
                </div>
              </div>
              <input
                type="number"
                class="form-control"
                name="mobileNumber"
                onChange={e => setFieldValue("mobileNumber", e.target.value)}
              />
              <ErrorMessage name="mobileNumber" component="div" className="error-message" />
            </Grid>

            <Grid item xs={12}>
              <Field
                  label="Email Address"
                  type="email"
                  name="emailAddress"
                  value={values.emailAddress}
                  className="form-control"
                  onChangeText={e => setFieldValue("emailAddress", e.target.value)}
                  required
                />
                <ErrorMessage
                  component="div"
                  name="emailAddress"
                  className="error-message"
                />
            </Grid>

            <Grid item xs={12}>
              <Field
                  label="Position"
                  type="text"
                  name="position"
                  value={values.position}
                  className="form-control"
                  onChangeText={e => setFieldValue("position", e.target.value)}
                  required
                />
                <ErrorMessage
                  component="div"
                  name="position"
                  className="error-message"
                />
            </Grid>

            <Grid item xs={12}>
              <Field
                  label="Price Per Ft"
                  type="number"
                  name="pricePerFt"
                  value={values.pricePerFt}
                  className="form-control"
                  onChangeText={e => setFieldValue("pricePerFt", +e.target.value)}
                  required
                />
                <ErrorMessage
                  component="div"
                  name="pricePerFt"
                  className="error-message"
                />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  };

  render() {
    const { latLng, branchInput, location, isUpdate, editBranchId } = this.state;
    const { addBranch, history, errorMessage, isAddedError, clearBranchFlag, clearErrorMessageShow, branch, updateBranch } = this.props;

    const initValue = isUpdate && editBranchId ? (branch && branch.id) && this.prepareValue() : branchInput

    return (
      <DashboardLayout>
        {isUpdate && branch && !branch.id ? <Loader /> :
        <div className="pl-3 pr-3">
          <Formik
            initialValues={initValue}
            validationSchema={Yup.object().shape({
              address: Yup.string().required("Branch Address is required"),
              contactName: Yup.string().required("Contact Name is required"),
              mobileNumber: Yup.string().required("Mobile No. is required"),
              emailAddress: Yup.string()
                .email("Email is invalid")
                .required("Email Address is required"),
              position: Yup.string().required("Position is required"),
              pricePerFt: Yup.number().required("Price Per Ft is required"),
            })}
            onSubmit={values => {
              values.address = location.address;
              values.country = location.country;
              values.state = location.state;
              values.city = location.city;
              values.placeName = location.placeName;
              values.postalCode = location.postalCode;
              values.route = location.route;
              values.officeLocation = {
                coordinates: [location.longitude, location.latitude]
              };

              clearBranchFlag();
              clearErrorMessageShow();

              if(isUpdate) {
                updateBranch(values)
              }else {
                addBranch(values)}
              }
            }
            render={({ errors, setFieldValue, values }) => (
              <Form>
                <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                    <Box
                      fontSize={20}
                      letterSpacing={1}
                      fontWeight={600}
                      className="map-title">
                      Branch Location
                    </Box>
                </div>
                
                <Row className="m-0">
                  
                  <Col xs={9} className="p-0 pr-3">
                    <div className="add-branch-map map-div-form">
                      <GoogleMap
                        className="add-branch-map"
                        latLng={latLng}
                        fetch={result =>
                          this.fetchMapInfo(result, setFieldValue)
                        }
                        height={25}
                        width={100}
                        placeHolder="Branch Location"
                        columnName="address"
                        isError={errors.address}
                        value={location}
                        isUpdate
                     />
                    </div>
                  </Col>

                  <Col xs={3} className="p-0 ">
                    {this.renderBranchInfo(setFieldValue, values)}  
                  </Col>

                </Row>

                { isAddedError &&  <ErrorComponent errors={errorMessage} /> }

                <div className="d-flex justify-content-center">
                    <Button type="submit" className="button btn btn-primary w-auto  primary-button">
                      {isUpdate ? "Update" : "Save"}
                    </Button>
                    
                    <Button
                      variant="contained"
                      className="button btn-dark"
                      onClick={() => cancelHandler(history)}>
                      Cancel
                    </Button>
                </div>

              </Form>
            )}
          ></Formik>
        </div>}
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  isAdded: state.branchReducer.isAdded,
  isAddedError: state.branchReducer.isAddedError,
  isUpdated: state.branchReducer.isUpdated,
  isUpdateError: state.branchReducer.isUpdateError,
  branch: state.branchReducer.branch,
  errorMessage: state.errorReducer.errorMessage,
});

const mapDispatchToProps = dispatch => ({
  addBranch: data => dispatch(addBranch(data)),
  clearBranchFlag: () => dispatch(clearBranchFlag()),
  clearErrorMessageShow: () => dispatch(clearErrorMessageShow()),
  getSingleBranch: data => dispatch(getSingleBranch(data)),
  updateBranch: data => dispatch(updateBranch(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddBranch);
