import React, { Component } from "react";
import { connect } from "react-redux";
import * as Yup from "yup";
import { ErrorMessage, Formik, Form } from "formik";
import {
    Box,
    Button,
    Card,
    CardContent,
    Grid,
    Icon,
} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';

import { Layout, Field, Loader } from "../../../components";
import { cancelHandler } from "../../../helpers/routeHelper";
import ImageUploader from "react-images-upload";
import uuid from "uuid/v4";

import "./branch.scss";
import { Row, Col } from "react-bootstrap";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import ErrorComponent from "../../../components/error/errorComponent";
import { uploadToS3, uploadToS3Video } from "../../../helpers/s3FileUpload";
import { createSurveyorReport, submitSurveyorReport, clearReportFlag, getDocumentLinks } from "../../../redux/actions";
import { SuccessNotify } from "../../../helpers/notification";

class CreateSurveyorReport extends Component {
    constructor(props) {
        super(props)

        this.state = {
            invalidVideo: false,
            salesEngineId: props.location && props.location.state && props.location.state.salesEngineId,
            report: "",
            boatVerifications: "",
            otherDocument: "",
            video: "",
            reportData: {
                comment: "",
                report: "",
                boatVerifications: "",
                otherDocument: "",
                video: "",
                images: [],
            }
        };
    }
    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const { createReportSuccess, submitSurveyorReport, currentUser, clearReportFlag, history, getDocumentLinks } = nextProps
        const { salesEngineId } = prevState

        if (createReportSuccess) {
            submitSurveyorReport({ id: salesEngineId, surveyorId: currentUser.id })
            getDocumentLinks({ id: salesEngineId, type: "Surveyor" })
            clearReportFlag()
            setTimeout(() => SuccessNotify("Report Submitted successfully"), 100);
            history.push('/sales-engines')
        }

        return null;
    }


    handleSingleFileUpload = async (file, name, setFieldValue) => {

        if (file.length) {
            this.setState({ [name]: file[0].name })
            let res = await uploadToS3(file[file.length - 1]);
            if (res.location) {
                setFieldValue(name, res.location)
            }
        }
    };

    handleMultipleFileUpload = async (file, values, setValue) => {

        if (file && file.length) {
            let res = await uploadToS3(file[file.length - 1]);
            if (res && res.location) {
                setValue('images', [...values, res.location]);
            }
        }
    };


    handleVideoUpload = async (values, setFieldValue) => {

        if (values.files) {
            const types = /(\.|\/)(mp3|mp4)$/i;

            const video = values.files[0];

            if (types.test(video.type) || types.test(video.name)) {
                this.setState({ invalidVideo: false })
                let res = await uploadToS3Video(video);
                if (res) {
                    setFieldValue('video', res.location)
                    this.setState({ loading: false })
                }
            } else {
                this.setState({ invalidVideo: true })
            }
        }
    };

    handleSingleFileDelete = (name, setFieldValue) => {
        setFieldValue(name, '')
        this.setState({ [name]: '' })
    }

    handleFileDelete = async (images, index, setValue) => {

        if (images && images.length) {
            images.splice(index, 1);
            setValue('images', images);
        }
    };

    render() {

        const { history, createSurveyorReport } = this.props
        const { reportData, salesEngineId, report, boatVerifications, otherDocument, video, loading } = this.state

        return (
            <DashboardLayout>
                {/* {loading && <Loader />} */}
                <div className="pl-3 pr-3">

                    <Formik
                        initialValues={reportData}
                        validationSchema={Yup.object().shape({
                            report: Yup.string().required("Report is required"),
                            boatVerifications: Yup.string().required("Boat Verification is required"),
                            video: Yup.string().required("Video is required"),
                            images: Yup.string().required("Images is required"),
                        })}
                        onSubmit={values => {
                            if (salesEngineId) {
                                values.salesEngineId = salesEngineId
                                createSurveyorReport(values)
                            }
                        }}
                        render={({ errors, setFieldValue, values }) => (
                            <Form>
                                {console.log(errors)}
                                <div className="pl-2 pt-3 pb-3 map-div map-title-bg">
                                    <Box
                                        fontSize={20}
                                        letterSpacing={1}
                                        fontWeight={600}
                                        className="map-title"
                                    >
                                        Inspection Details
                            </Box>
                                </div>
                                <Card className="w-100">
                                    <CardContent>
                                        <Row className="m-0">

                                            <Col className="p-0 ">
                                                <div className="mb-2">
                                                    <Field
                                                        label="Add Your Comment"
                                                        id={"comment"}
                                                        name={"comment"}
                                                        value={values.comment}
                                                        type="textarea"
                                                        onChangeText={e => {
                                                            setFieldValue(
                                                                "comment",
                                                                e.target.value
                                                            );
                                                        }}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="membershipBenefits"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="mb-2">
                                                    <label for="contactName" className="required">Upload Survey Report</label>
                                                    {report && <Box>
                                                        {report}
                                                        <CloseIcon className="remove-doc" onClick={() => this.handleSingleFileDelete('report', setFieldValue)} />
                                                    </Box>}
                                                    <ImageUploader
                                                        withIcon={true}
                                                        buttonText='Upload Survey Report'
                                                        accept="/*"
                                                        imgExtension={['.docx', '.doc', '.pdf', '.txt', '.xlsx']}
                                                        onChange={file =>
                                                            this.handleSingleFileUpload(
                                                                file,
                                                                "report",
                                                                setFieldValue
                                                            )
                                                        }
                                                        maxFileSize={5242880}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="membershipBenefits"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="m-0">
                                            <Col>
                                                <div className="mb-2">
                                                    <label for="contactName" className="required">Upload Boat Verifications </label>
                                                    {boatVerifications && <Box>
                                                        {boatVerifications}
                                                        <CloseIcon className="remove-doc" onClick={() => this.handleSingleFileDelete('boatVerifications', setFieldValue)} />
                                                    </Box>}
                                                    <ImageUploader
                                                        withIcon={true}
                                                        buttonText='Upload Boat Verifications'
                                                        accept="/*"
                                                        imgExtension={['.docx', '.doc', '.pdf', '.txt', '.xlsx']}
                                                        onChange={file =>
                                                            this.handleSingleFileUpload(
                                                                file,
                                                                "boatVerifications",
                                                                setFieldValue
                                                            )
                                                        }
                                                        maxFileSize={5242880}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="membershipBenefits"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="mb-2">
                                                    <label for="contactName">Upload Other Document If Any</label>
                                                    {otherDocument && <Box>
                                                        {otherDocument}
                                                        <CloseIcon className="remove-doc" onClick={() => this.handleSingleFileDelete('otherDocument', setFieldValue)} />
                                                    </Box>}
                                                    <ImageUploader
                                                        withIcon={true}
                                                        buttonText='Upload Other Document'
                                                        accept="/*"
                                                        imgExtension={['.docx', '.doc', '.pdf', '.txt', '.xlsx']}
                                                        onChange={file =>
                                                            this.handleSingleFileUpload(
                                                                file,
                                                                "otherDocument",
                                                                setFieldValue
                                                            )
                                                        }
                                                        maxFileSize={5242880}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="membershipBenefits"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="m-0">
                                            <Col>
                                                <div className="mb-2">
                                                    <label for="contactName" className="required">Upload Video</label>

                                                    {video && <Box>
                                                        {video}
                                                        <CloseIcon className="remove-doc" onClick={() => this.handleSingleFileDelete('video', setFieldValue)} />
                                                    </Box>}

                                                    <div className="video-input">
                                                        <div className="video-url">
                                                            <input type="file" name="video" onChange={e => {
                                                                this.setState({ loading: true })
                                                                this.handleVideoUpload(e.target, setFieldValue)
                                                            }} />
                                                            <button type="button" className="choose-file-button"> <Icon type="upload" /> Video Url</button>
                                                        </div>
                                                        {/* {invalidVideo &&
                                                            <Text type="danger">Invalid file.</Text>
                                                        } */}
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="mb-2">
                                                    <label for="contactName" className="required">Upload Image</label>
                                                    {values.images && values.images.length
                                                        ? values.images.map((img, index) => (
                                                            <>
                                                                <div className="addBoat-container mr-2 mb-2">
                                                                    <img
                                                                        src={img}
                                                                        key={uuid()}
                                                                        alt="upload images"
                                                                        height="100px"
                                                                        width="100px"
                                                                    />
                                                                    <span
                                                                        onClick={() =>
                                                                            this.handleFileDelete(
                                                                                values.images,
                                                                                index,
                                                                                setFieldValue
                                                                            )
                                                                        }
                                                                    >
                                                                        ×
                                                </span>
                                                                </div>
                                                            </>
                                                        ))
                                                        : null}
                                                    <ImageUploader
                                                        withIcon={true}
                                                        buttonText='Choose images'
                                                        accept="/*"
                                                        onChange={file =>
                                                            this.handleMultipleFileUpload(
                                                                file,
                                                                values.images,
                                                                setFieldValue
                                                            )
                                                        }
                                                        maxFileSize={5242880}
                                                    />
                                                    <ErrorMessage
                                                        component="div"
                                                        name="membershipBenefits"
                                                        className="error-message"
                                                    />
                                                </div>
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Card>
                                <div className="d-flex justify-content-center">
                                    <Button type="submit" disabled={loading ? true : false} className="button btn btn-primary w-auto  primary-button">
                                        Save
                        </Button>
                                    <Button
                                        variant="contained"
                                        className="button btn-dark"
                                        onClick={() => cancelHandler(history)}
                                    >
                                        Cancel
                        </Button>
                                </div>
                            </Form>
                        )}
                    ></Formik>
                </div>
            </DashboardLayout>
        );
    }
}

const mapStateToProps = state => ({
    createReportSuccess: state.salesEngineReducer && state.salesEngineReducer.createReportSuccess,
    currentUser: state.loginReducer && state.loginReducer.currentUser
});

const mapDispatchToProps = dispatch => ({
    createSurveyorReport: (data) => dispatch(createSurveyorReport(data)),
    submitSurveyorReport: (data) => dispatch(submitSurveyorReport(data)),
    clearReportFlag: () => dispatch(clearReportFlag()),
    getDocumentLinks: (data) => dispatch(getDocumentLinks(data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateSurveyorReport);
