import React, { Component } from "react";
import { connect } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import { IoIosArrowBack, IoMdHeartEmpty, IoIosImage } from "react-icons/io";
import { TiHomeOutline } from "react-icons/ti";
import { FaRegUserCircle, FaCogs } from "react-icons/fa";
import { MdAddShoppingCart } from "react-icons/md";
import { GoArchive, GoFile } from "react-icons/go";
import { GiGearHammer, GiSailboat } from "react-icons/gi";

import {
  logout,
  sendConfirmationLink,
  clearEmailFlag,
  clearForgotPassword,
  getDashBoardCount,
  clearAuthorizationFlag
} from "../../redux/actions";

import {
  dashboardTabs,
  userRoles,
  userRoleTypes,
  salesEngineAccessibleTypes
} from "../../util/enums/enums";
import { SuccessNotify } from "../../helpers/notification";
import { DashboardLayout } from "../../components/layout/dashboardLayout";
import "./dashboard.scss";
import "../../styles/dashboardResponsive.scss"
import { ConfirmationEmail } from "../../components";
import { getUserProfileCount } from "../../helpers/jsxHelper";
import TableComponent from "../../components/tableComponent/TableComponent";

const dashBoardData = (role) => {
  return [
    {
      name: "Back to Market",
      icon: <IoIosArrowBack />,
      selected: true,
      url: "/#/"
    },
    {
      name: "Dashboard",
      icon: <TiHomeOutline />,
      url: "#"
    },
    {
      name: "User Profile",
      icon: <FaRegUserCircle />,
      url: "#"
    },
    {
      name: "Sales Engine",
      icon: <FaCogs />,
      url: "#"
    },
    {
      name: "Buy it Now",
      icon: <MdAddShoppingCart />,
      url: "#"
    },
    {
      name: "Sales Engine Archive",
      icon: <GoArchive />,
      url: "#"
    },
    {
      name: "Auction Room",
      icon: <GiGearHammer />,
      url: "#"
    },
    {
      name: "Wishlist",
      icon: <IoMdHeartEmpty />,
      url: "#"
    },
    {
      name: "Boat Show",
      icon: <GiSailboat />,
      url: "#"
    },
    {
      name: "Article",
      icon: <GoFile />,
      url: "#"
    },
    // {
    //   name: "Advertisements",
    //   icon: <IoIosImage />,
    //   url: "#"
    // }
  ];
};

class Dashboard extends Component {
  state = {
    openDialog: false
  };

  constructor(props) {
    super(props);
    this.state = { dashBoardData: dashBoardData(props.currentUser && props.currentUser.role) };
  }

  componentDidMount() {
    const { forgotPasswordSuccess, clearForgotPassword, currentUser, getDashBoardCount, clearAuthFlag } = this.props;
    clearAuthFlag()
    if (forgotPasswordSuccess) {
      clearForgotPassword();
      SuccessNotify("Password Changed Successfully");
    }
    if (currentUser && currentUser.role && currentUser.role.aliasName) {
      getDashBoardCount({ role: currentUser.role.aliasName })
    }
  }
  checkCount = (value) => {
    return value ? value : 0
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      isAuthenticated,
      history,
      isMailSent,
      clearEmailFlag,
      forgotPasswordSuccess,
      clearForgotPassword
    } = nextProps;

    if (isMailSent) {
      setTimeout(() => {
        SuccessNotify('Send Confirmation Link Successfully')
      }, 1000);
      clearEmailFlag();
      return {
        openDialog: false
      };
    }

    if (forgotPasswordSuccess) {
      clearForgotPassword();
      SuccessNotify("Password Changed Successfully");
    }
    return null;

  }

  modalHandler = () => {
    this.setState(preState => ({ openDialog: !preState.openDialog }));
  };

  redirectionHandler = url => {
    const { boats, branch, buyItNow, articles, boatRents, services, marinaAndStorage,
      salesEngine,
      salesEngineArchive,
      auctionRooms,
      boatShows } = dashboardTabs;
    const { currentUser, history } = this.props;
    const { isActivated } = currentUser;

    const urls = [boats.url, branch.url, articles.url, buyItNow.url, boatRents.url, services.url, marinaAndStorage.url,
    salesEngine.url, salesEngineArchive.url, auctionRooms.url, boatShows.url];

    if (!isActivated && urls.includes(url)) {
      this.modalHandler();
    } else {
      history.push(`/${url}`);
    }
  };

  onMenuItemClick = selectedIndex => {
    const newData = this.state.dashBoardData.map((data, index) => {
      if (selectedIndex === index) {
        data.selected = true;
        return data;
      } else {
        data.selected = false;
        return data;
      }
    });
    this.setState({
      dashBoardData: newData
    });
  };

  renderRoleWiseCards = roleType => {
    const {
      boats,
      buyItNow,
      branch,
      brokers,
      boatRents,
      services,
      marinaAndStorage,
      auctionRooms,
      boatShows,
      contactLeads,
      agreementsAndFiles,
      salesEngine
    } = dashboardTabs;

    const {
      MEMBER,
      BOAT_OWNER,
      BROKER_AND_DEALER,
      BOAT_MANUFACTURER,
      YACHT_SHIPPER,
      SURVEYOR,
      RENT_AND_CHARTER,
      SERVICE_AND_MAINTENANCE,
      MARINA_AND_STORAGE,
      AGENT
    } = userRoles;
    const { surveyor: branchCount, auctionRoom: auctionCount, broker: brokerCount, boatShow: boatshowCount, salesEngineBuyItNowCount, salesEngineArchiveCount, manageHomeAdvertisement: advertisementsCount, boat: boatsCount, broker: brokersCount, surveyor: surveyorCount, boatRent: boatRentsCount, marina: marinaCount, salesEngine: salesEngineCount } = this.props.dashboardCount;
    switch (roleType) {
      case MEMBER:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(buyItNow.url)}>
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/buy.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(salesEngineBuyItNowCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {buyItNow.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case BOAT_OWNER:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(boats.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/boats.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(boatsCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {boats.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case BROKER_AND_DEALER:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(boats.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/boats.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(boatsCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {boats.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case BOAT_MANUFACTURER:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(boats.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/manage-boat.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(boatsCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {boats.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>

            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div className="d-flex align-items-center dashboard-right-div-content">
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/broker.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(brokerCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {brokers.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case SURVEYOR:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(branch.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/branch.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(branchCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {branch.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case RENT_AND_CHARTER:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(boatRents.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/rent.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(boatRentsCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {boatRents.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case SERVICE_AND_MAINTENANCE:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(services.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/service.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">17</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {services.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      case MARINA_AND_STORAGE:
        return (
          <>
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(marinaAndStorage.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/marina.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(marinaCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {marinaAndStorage.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </>
        );

      default:
        break;
    }
  };

  renderAllCards = () => {
    const { logout, currentUser, dashboardCount } = this.props;
    const { article: articlesCount, auctionRoom: auctionCount, boatShow: boatshowCount, salesEngineBuyItNowCount, salesEngineArchiveCount, manageHomeAdvertisement: advertisementsCount, boat: boatsCount, broker: brokersCount, surveyor: surveyorCount, boatRent: boatRentsCount, marina: marinaCount, salesEngineCount } = dashboardCount;
    const { role } = currentUser;
    const {
      userProfile,
      salesEngine,
      salesEngineArchive,
      auctionRooms,
      contactLeads,
      wishlists,
      articles,
      advertisements,
      boatShows,
      agreementsAndFiles,
      changePassword,
      logOut
    } = dashboardTabs;

    const {
      MEMBER,
      BOAT_OWNER,
      BROKER_AND_DEALER,
      BOAT_MANUFACTURER,
      YACHT_SHIPPER,
      SURVEYOR,
      RENT_AND_CHARTER,
      SERVICE_AND_MAINTENANCE,
      MARINA_AND_STORAGE,
      AGENT
    } = userRoles;
    const auctionRoomAccessibleRoles = [BOAT_OWNER, BROKER_AND_DEALER];
    const boatShowsAccessibleRoles = [
      BOAT_MANUFACTURER,
      MARINA_AND_STORAGE,
      MEMBER
    ];
    const agreementsAndFilesAccessibleRoles = [
      BOAT_MANUFACTURER,
      YACHT_SHIPPER,
      SURVEYOR
    ];

    return (
      <div>
        <Row>
          <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div
                className="d-flex align-items-center dashboard-right-div-content"
                onClick={() => this.redirectionHandler(userProfile.url)}
              >
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/user.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0">{getUserProfileCount(currentUser)}%</h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {userProfile.title}
                  </span>
                </div>
              </div>
            </div>
          </Col>

          {role && this.renderRoleWiseCards(role.aliasName)}

          {role && salesEngineAccessibleTypes.includes(role.type) && (
            <>
              <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
                <div className="dashboard-box-div p-4 bg-white">
                  <div className="d-flex align-items-center dashboard-right-div-content" onClick={() => this.redirectionHandler(salesEngine.url)}>
                    <div className="dashboard-div-icon mr-3 d-flex">
                      <img src={require('./image/sales-engine.png')} alt="Loading..." />
                    </div>
                    <div className="d-flex flex-column dashboard-right-div-data">
                      <h5 className="dashboard-count mb-0">{this.checkCount(dashboardCount && dashboardCount.salesEngine)}</h5>
                      <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                        {salesEngine.title}
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

              <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
                <div className="dashboard-box-div p-4 bg-white">
                  <div className="d-flex align-items-center dashboard-right-div-content" onClick={() => this.redirectionHandler(salesEngineArchive.url)}>
                    <div className="dashboard-div-icon mr-3 d-flex">
                      <img src={require('./image/sales-engine-archieve.png')} alt="Loading..." />
                    </div>
                    <div className="d-flex flex-column dashboard-right-div-data">
                      <h5 className="dashboard-count mb-0">{this.checkCount(salesEngineArchiveCount)}</h5>
                      <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                        {salesEngineArchive.title}
                      </span>
                    </div>
                  </div>
                </div>
              </Col>
            </>
          )}

          {auctionRoomAccessibleRoles.includes(role && role.aliasName) && (
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(auctionRooms.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/auction-room.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(auctionCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {auctionRooms.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          )}

          <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div className="d-flex align-items-center dashboard-right-div-content">
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/whishlist.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0">0</h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {wishlists.title}
                  </span>
                </div>
              </div>
            </div>
          </Col>

          <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div
                className="d-flex align-items-center dashboard-right-div-content"
                onClick={() => this.redirectionHandler(articles.url)}
              >
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/article.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0">{this.checkCount(articlesCount)}</h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {articles.title}
                  </span>
                </div>
              </div>
            </div>
          </Col>

          {/* <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div className="d-flex align-items-center dashboard-right-div-content">
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/advertisement.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0">{this.checkCount(advertisementsCount)}</h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {advertisements.title}
                  </span>
                </div>
              </div>
            </div>
          </Col> */}

          {boatShowsAccessibleRoles.includes(role && role.aliasName) && (
            <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
              <div className="dashboard-box-div p-4 bg-white">
                <div
                  className="d-flex align-items-center dashboard-right-div-content"
                  onClick={() => this.redirectionHandler(boatShows.url)}
                >
                  <div className="dashboard-div-icon mr-3 d-flex">
                    <img src={require('./image/boats.png')} alt="Loading..." />
                  </div>
                  <div className="d-flex flex-column dashboard-right-div-data">
                    <h5 className="dashboard-count mb-0">{this.checkCount(boatshowCount)}</h5>
                    <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                      {boatShows.title}
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          )}

          {agreementsAndFilesAccessibleRoles.includes(
            role && role.aliasName
          ) && (
              <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
                <div className="dashboard-box-div p-4 bg-white">
                  <div className="d-flex align-items-center dashboard-right-div-content">
                    <div className="dashboard-div-icon mr-3 d-flex">
                      <img src={require('./image/agreement.png')} alt="Loading..." />
                    </div>
                    <div className="d-flex flex-column dashboard-right-div-data">
                      <h5 className="dashboard-count mb-0">21</h5>
                      <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                        {agreementsAndFiles.title}
                      </span>
                    </div>
                  </div>
                </div>
              </Col>
            )}

          {/* LATER NEEDED THIS CHANGE */}

          <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div
                className="d-flex align-items-center dashboard-right-div-content"
                onClick={() => this.redirectionHandler(changePassword.url)}
              >
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/password.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0"></h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {changePassword.title}
                  </span>
                </div>
              </div>
            </div>
          </Col>

          <Col sm={4} className="cursor-pointer mb-4 dashboard-box-div-spacing">
            <div className="dashboard-box-div p-4 bg-white">
              <div
                className="d-flex align-items-center dashboard-right-div-content"
                onClick={() => this.redirectionHandler(logOut.url)}
              >
                <div className="dashboard-div-icon mr-3 d-flex">
                  <img src={require('./image/logout.png')} alt="Loading..." />
                </div>
                <div className="d-flex flex-column dashboard-right-div-data">
                  <h5 className="dashboard-count mb-0"></h5>
                  <span className="font-16 font-weight-400 title-text dark-silver dashboard-market-title">
                    {logOut.title}
                  </span>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div >
    );
  };

  render() {
    const { sendConfirmationLink, currentUser, dashboardCount } = this.props;
    const { openDialog } = this.state;
    const { email } = currentUser;

    return (
      <DashboardLayout>
        <>
          {openDialog && (
            <ConfirmationEmail
              open={openDialog}
              close={this.modalHandler}
              email={email}
              sendLink={email => sendConfirmationLink(email)}
            />
          )}

          <div className="width-100">
            <div className="pl-3 dashboard-right-div">
              <div className="pl-5 pb-5 dashboard-right-div-padding">
                <h6 className="title-text font-16 dashboard-main-title pb-4 mb-2">Dashboard</h6>

                {dashboardCount &&
                  this.renderAllCards()
                }
              </div>
            </div>
          </div>
          { /* Do not remove below component
            ask @miraj before

            <TableComponent/>
          */}
        </>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.loginReducer.isAuthenticated,
  currentUser: state.loginReducer.currentUser,
  isMailSent: state.dashboardReducer.isMailSent,
  forgotPasswordSuccess: state.loginReducer.forgotPasswordSuccess,
  dashboardCount: state.dashboardReducer.dashboardCount,
});

const mapDispatchToProps = dispatch => ({
  sendConfirmationLink: data => dispatch(sendConfirmationLink(data)),
  clearEmailFlag: () => dispatch(clearEmailFlag()),
  clearForgotPassword: () => dispatch(clearForgotPassword()),
  clearAuthFlag: () => dispatch(clearAuthorizationFlag()),
  getDashBoardCount: data => dispatch(getDashBoardCount(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
