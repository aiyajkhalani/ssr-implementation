import React, { Component } from "react";
import { connect } from "react-redux";

import { Layout } from "../../components";
import { Container, Row, Col } from "react-bootstrap";
import { IoIosArrowBack } from "react-icons/io";
import { TiHomeOutline } from "react-icons/ti";
import { FaRegUserCircle, FaCogs } from "react-icons/fa";
import { MdAddShoppingCart } from "react-icons/md";
import { GoArchive, GoFile } from "react-icons/go";
import { GiGearHammer, GiSailboat } from "react-icons/gi";
import { Link } from "react-router-dom";
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import {
  logout,
  sendConfirmationLink,
  clearEmailFlag,
  clearForgotPassword
} from "../../redux/actions";

import { dashboardTabs, userRoles } from "../../util/enums/enums";
import { ConfirmationEmail } from "../../components";
import { SuccessNotify } from "../../helpers/notification";
import UserContext from "../../UserContext";
import "./dashboard.scss";
import "../../styles/dashboardResponsive.scss";

// const dashBoardSideBarData = [
const dashBoardSideBarAllData =
  // const dashboardRoleWiseColumn = role && roleWiseColumn(role.aliasName);

  [
    {
      name: "Back to Market",
      imgPath: require("./image/market.png"),
      selected: true,
      url: "",
      role: ["common"]
    },
    {
      name: "Dashboard",
      imgPath: require("./image/dashboard.png"),
      url: "dashboard",
      role: ["common"]
    },
    {
      name: "User Profile",
      imgPath: require("./image/user.png"),
      url: "profile",
      role: ["common"]
    },
    {
      name: "Buy it Now",
      imgPath: require("./image/buy.png"),
      url: "sales-engines?buyItNow=true",
      role: ["member"]
    },
    {
      name: "Branch",
      imgPath: require("./image/branch.png"),
      url: "manage-branches",
      role: ["surveyor"]
    },
    {
      name: "Manage Boat Rent",
      imgPath: require("./image/rent.png"),
      url: "manage-boat-rents",
      role: ["rent-and-charter"]
    },
    {
      name: "Services",
      imgPath: require("./image/service.png"),
      url: "add-boat-service",
      role: ["service-and-maintenance"]
    },
    {
      name: "Marina & Storage",
      imgPath: require("./image/marina.png"),
      url: "manage-marina-storage",
      role: ["marina-and-storage"]
    },
    {
      name: "Manage Boats",
      imgPath: require("./image/manage-boat.png"),
      url: "manage-boats",
      role: ["boat-owner", "broker-and-dealer", "boat-manufacturer"]
    },
    {
      name: "Brokers",
      imgPath: require("./image/broker.png"),
      url: "",
      role: ["boat-manufacturer"]
    },
    {
      name: "Sales Engine",
      imgPath: require("./image/sales-engine.png"),
      url: "sales-engines",
      role: [
        "member",
        "boat-owner",
        "broker-and-dealer",
        "boat-manufacturer",
        "yacht-shipper",
        "surveyor"
      ]
    },
    {
      name: "Sales Engine Archive",
      imgPath: require("./image/sales-engine-archieve.png"),
      url: "sales-engine-archives",
      role: [
        "member",
        "boat-owner",
        "broker-and-dealer",
        "boat-manufacturer",
        "yacht-shipper",
        "surveyor"
      ]
    },
    {
      name: "Manage Auction Rooms",
      imgPath: require("./image/auction-room.png"),
      url: "manage-auction-rooms",
      role: ["boat-owner", "broker-and-dealer"]
    },
    {
      name: "Manage Wishlist",
      imgPath: require("./image/whishlist.png"),
      url: "",
      role: ["common"]
    },
    {
      name: "Manage Articles",
      imgPath: require("./image/article.png"),
      url: "manage-articles",
      role: ["common"]
    },
    // {
    //   name: "Advertisements",
    //   imgPath: require("./image/advertisement.png"),
    //   url: ",
    //   role: ["common"]
    // },
    {
      name: "Boat Shows",
      imgPath: require("./image/boats.png"),
      url: "manage-boat-shows",
      role: ["member", "boat-manufacturer", "marina-and-storage"]
    },
    {
      name: "Agreements & Files",
      imgPath: require("./image/agreement.png"),
      url: "",
      role: ["boat-manufacturer", "yacht-shipper", "surveyor"]
    },
    {
      name: "Change Password",
      imgPath: require("./image/password.png"),
      url: "change-password",
      role: ["common"]
    },
    {
      name: "Logout",
      imgPath: require("./image/logout.png"),
      url: "logout",
      role: ["common"]
    }
  ];

class DashboardSidebar extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      isOpen: true,
      // dashBoardSideBarData: dashBoardSideBarData(
      //   props.currentUser && props.currentUser.role
      // )
      dashBoardSideBarData: dashBoardSideBarAllData
    };
  }
  toggleHandler = () => {
    this.setState(prevState => ({ isOpen: !prevState.isOpen }));
  };

  componentDidMount() {
    const { forgotPasswordSuccess, clearForgotPassword } = this.props;
    if (forgotPasswordSuccess) {
      clearForgotPassword();
      SuccessNotify("Password Changed Successfully");
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      isAuthenticated,
      history,
      isMailSent,
      clearEmailFlag,
      forgotPasswordSuccess,
      clearForgotPassword
    } = nextProps;

    if (isMailSent) {
      clearEmailFlag();
      return {
        openDialog: false
      };
    }

    if (forgotPasswordSuccess) {
      clearForgotPassword();
      SuccessNotify("Password Changed Successfully");
    }
    return null;
  }

  modalHandler = () => {
    this.setState(preState => ({ openDialog: !preState.openDialog }));
  };

  redirectionHandler = url => {
    const { boats, branch, buyItNow, articles, boatRents, services, marinaAndStorage,
      salesEngine,
      salesEngineArchive,
      auctionRooms,
      boatShows } = dashboardTabs;
    const { currentUser } = this.props;
    const { isActivated } = currentUser;
    const { history } = this.context;

    const urls = [boats.url, branch.url, articles.url, buyItNow.url, boatRents.url, services.url, marinaAndStorage.url,
    salesEngine.url, salesEngineArchive.url, auctionRooms.url, boatShows.url];

    if (!isActivated && urls.includes(url)) {
      this.modalHandler();
    } else {
      history.push(`/${url}`);
    }
  };

  onMenuItemClick = (data, selectedIndex) => {

    const newData = this.state.dashBoardSideBarData.map((data, index) => {
      if (selectedIndex === index) {
        data.selected = true;
        return data;
      } else {
        data.selected = false;
        return data;
      }
    });
    this.setState(
      {
        dashBoardSideBarData: newData
      },
      () => {
        this.redirectionHandler(data.url)
      }
    );
  };

  render() {
    const { sendConfirmationLink, currentUser } = this.props;
    const { openDialog, dashBoardSideBarData, isOpen } = this.state;
    const { email } = currentUser;

    return (
      <>
        {openDialog && (
          <ConfirmationEmail
            open={openDialog}
            close={this.modalHandler}
            email={email}
            sendLink={email => sendConfirmationLink(email)}
          />
        )}

        {/* <Container> */}
        <div className="dashboard-sidebar-align">
          <div className="pr-0 dashboard-sidebar">
            <div>
              {dashBoardSideBarData.length &&
                dashBoardSideBarData.map((data, index) => {
                  return (
                    <div
                      onClick={() => this.onMenuItemClick(data, index)}
                      className={
                        data.selected
                          ? "dashboard-selected-div"
                          : "dashboard-left-div-link"
                      }
                    >
                      <div
                        className={
                          data.role.includes(currentUser.role && currentUser.role.aliasName) ||
                            data.role.includes("common")
                            ? "displayBlock dashboard-right-div-option-border"
                            : "displayHide"
                        }
                      >
                        <div className="dashboard-links cursor-pointer">
                          {/* <div to={`${data.url}`} className="dashboard-links"> */}
                          <div className="dashboard-font p-3 pl-4 d-flex align-items-center">
                            <div
                              className={`d-flex align-items-center ${index ===
                                0 &&
                                "pl-2 pr-2 pt-1 pb-1 dashboard-left-div-main-btn"}`}
                            >
                              {/* <span className="mr-2 dashboard-side-icon d-flex">
                                {data.imgPath}
                              </span> */}
                              <img
                                className="mr-2 dashboard-side-icon d-flex dashboard-left-div-icons"
                                src={data.imgPath}
                                alt=""
                              />
                              <span className="title-text dashboard-sidebar-text">
                                {data.name}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* </Link> */}
                    </div>
                  );
                })}
            </div>

            <div className="dashboard-links cursor-pointer">
              <div className="dashboard-font p-3 pl-4 d-flex align-items-center">
                <div className="d-flex width-100">

                  <div className="width-100 d-flex flex-column dashboard-setting-div">

                    <div 
                      className="width-100 d-flex justify-content-between align-items-center"
                      onClick={this.toggleHandler}
                      >
                      <div className="setting-icon">
                        <img
                          className="mr-2 dashboard-side-icon d-flex dashboard-left-div-icons"
                          src={require("./image/setting.png")}
                          alt=""
                        />
                        <span className="title-text dashboard-sidebar-text">
                          Settings
                      </span>
                      </div>
                      {isOpen ? <RemoveIcon/> :  <AddIcon />}
                    </div>
                    {isOpen &&
                    <div className="drop-down-sidebar">
                    <span className="title-text dashboard-sidebar-text light-silver">
                      Manage Payment
                    </span>
                    <Link to='/change-password' className="title-text dashboard-sidebar-text light-silver">
                      Change Password
                    </Link>
                    <span className="title-text dashboard-sidebar-text light-silver">
                      Close Account
                    </span>
                    </div>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.loginReducer.isAuthenticated,
  currentUser: state.loginReducer.currentUser,
  isMailSent: state.dashboardReducer.isMailSent,
  forgotPasswordSuccess: state.loginReducer.forgotPasswordSuccess
});

const mapDispatchToProps = dispatch => ({
  sendConfirmationLink: data => dispatch(sendConfirmationLink(data)),
  clearEmailFlag: () => dispatch(clearEmailFlag()),
  clearForgotPassword: () => dispatch(clearForgotPassword())
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardSidebar);
