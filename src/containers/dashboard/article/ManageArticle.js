import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch } from "@material-ui/core";
import ReactTable from "react-table";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DeleteIcon from "@material-ui/icons/Delete";

import { getUserAllArticle } from "../../../redux/actions/articlesAction";
import { pagination } from "../../../util/enums/enums";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";

import "./article.scss";
import "../../../styles/manageDashboardTableResponsive.scss";

class ManageArticle extends Component {
  componentDidMount() {
    const { getUserAllArticle } = this.props;
    const data = {
      page: pagination.PAGE_COUNT,
      limit: pagination.PAGE_RECORD_LIMIT
    };
    getUserAllArticle(data);
  }

  render() {
    const { articles, history } = this.props;

    const columns = [
      {
        Header: "Title",
        accessor: "title"
      },
      {
        Header: "Ad ID",
        accessor: "id"
      },
      {
        Header: "Article Status",
        accessor: "status",
        Cell: data => (
          <div className="d-flex justify-content-center">
            <Switch
              checked={data.row.status}
              // onChange={() => toggleBoatStatus({ id: data.row.original.id, columnName: 'AdStatus' })}
              value={data.row.AdStatus}
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          </div>
        )
      },
      {
        Header: "Article Image",
        accessor: "file",
        Cell: data => (
          <>
            <div className="d-flex justify-content-center manageArtical-img">
              <img src={data.row.file} alt="article thumbnail"></img>
            </div>
          </>
        )
      },
      {
        Header: "Actions",
        Cell: data => (
          <div className="d-flex flex-row justify-content-around action">
            <button
              type="button"
              className="btn btn-outline-success mr-2"
              
            >
              Edit
            </button>

            <button
              type="button"
              className="btn btn-outline-info mr-2"
              // onClick={() => viewBoatHandler(data.row.original, history)}
            >
              View
            </button>

            <button
              type="button"
              className="btn btn-outline-danger mr-2"
              // onClick={() =>  confirmSubmitHandler(deleteBoatRent, data.row.original.id)}
            >
              Delete
            </button>

          </div>
        )
      }
    ];

    return (
      <DashboardLayout>
        <div className="row pl-3 pr-3 manage-dashboard-table">
          <div className="col-12">
            <div className="card">
              <div className="card-header article-header">
                <h4 className="card-title">Manage Articles</h4>
                <button
                  type="button"
                  className="btn btn-primary primary-button"
                  onClick={() => {
                    history.push("/create-article");
                  }}
                >
                  Add Article
                </button>
              </div>
              <div className="card-body mt-0">
                <div className="table-responsive">
                  {articles && (
                    <ReactTable
                      columns={columns}
                      data={articles}
                      defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]).includes(filter.value)
                      }
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </DashboardLayout>
    );
  }
}

const mapStateToProps = state => ({
  articles: state.articleReducer && state.articleReducer.articles
});

const mapDispatchToProps = dispatch => ({
  getUserAllArticle: data => dispatch(getUserAllArticle(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageArticle);
