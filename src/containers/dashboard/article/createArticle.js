import React, { Component, Fragment } from "react";
import { Formik, ErrorMessage } from "formik";
import { connect } from "react-redux";
import * as Yup from "yup";
import {
  Button,
  Card,
  Row,
  Col,
  Form
} from "react-bootstrap";
import ReactQuill from "react-quill";

import { Field } from "../../../components";
import {
  createArticle,
  clearArticleFlag
} from "../../../redux/actions/articlesAction";
import { randomAdId } from "../../../helpers/string";
import { DashboardLayout } from "../../../components/layout/dashboardLayout";
import ErrorComponent from '../../../components/error/errorComponent';

import "./article.scss";

class CreateArticle extends Component {
  state = {
    article: {
      title: "",
      description: "",
      file: ""
    },
    adId: ""
  };

  componentDidMount() {
    const { clearArticleFlag } = this.props;
    clearArticleFlag();
  }

  static getDerivedStateFromProps(props, state) {
    const { adId } = state;

    const { clearArticleFlag, history, createSuccess } = props;
    if (createSuccess) {
      clearArticleFlag();
      history.push("/manage-articles");
    } else if (!adId) {
      return {
        adId: randomAdId("AR")
      };
    }
  }

  cancelHandler = () => {
    const { history, clearArticleFlag } = this.props;
    clearArticleFlag();
    history.push('/manage-articles');
  };

  render() {
    const { createArticle, createError, errorMessage, user } = this.props;
    const { article, adId } = this.state;
    return (
      <Fragment>
        <DashboardLayout>
          <div className="article-container pt-0">
            <Formik
              initialValues={{ ...article }}
              onSubmit={values => {
                // values.articleApproved = 0;
                // values.articleViewCount = 0;
                values.id = user && user.id;
                values.adId = adId;

                createArticle(values);
              }}
              validationSchema={Yup.object().shape({
                title: Yup.string().required("Title field is required."),
                description: Yup.string().required(
                  "Description field is required."
                ),
                file: Yup.string().required('Article image field is required.'),
              })}
              render={({
                values,
                setFieldValue,
                handleSubmit,
              }) => (
                  <Form>
                    <Card className="p-3">
                      <Card.Title className="card-title d-flex justify-content-center">
                        Add Article
                    </Card.Title>
                      <Row>
                        <Col>
                          <Row className="card-content mt-0 mb-0 pb-0 pt-0">
                            <Col>
                              <Field
                                label="Title"
                                id="title"
                                name="title"
                                type="text"
                                required
                                value={values.title}
                                onChangeText={e => {
                                  setFieldValue("title", e.target.value);
                                }}
                              />
                              <ErrorMessage
                                component="div"
                                name="title"
                                className="error-message"
                              />
                            </Col>
                          </Row>
                          <Row className="card-content m-0">
                            <span class="form-label ml-0">Article Image</span>
                            <Col>
                              <br />

                              <div className="d-flex flex-wrap justify-content-center">
                                <div className="addBoatShow-imgUploader">
                                  <Field
                                    label="Article Images"
                                    name="file"
                                    id="file"
                                    type="single-image"
                                    value={values.file}
                                    onChangeText={setFieldValue}
                                    required
                                  />
                                </div>
                                <ErrorMessage
                                  component="div"
                                  name="file"
                                  className="error-message d-flex justify-content-center w-100"
                                />
                              </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col>
                          <Row className="card-content m-0 pt-0 pb-0">
                            <Col>
                              <span className="form-label required">
                                Article Description
                            </span>
                              <ReactQuill
                                className="editor-height"
                                theme={"snow"}
                                onChange={values =>
                                  setFieldValue("description", values)
                                }
                              />
                              <ErrorMessage
                                component="div"
                                name="description"
                                className="error-message"
                              />
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      {createError && <ErrorComponent errors={errorMessage} />}

                      <div className="d-flex justify-content-center">
                        <Button
                          type="submit"
                          className="button btn btn-primary w-auto primary-button"
                          onClick={handleSubmit}
                        >
                          Save
                      </Button>
                        <Button
                          type="button"
                          className="button btn w-auto btn-dark" onClick={this.cancelHandler}
                        >
                          Cancel
										</Button>
                      </div>
                    </Card>
                  </Form>
                )}
            ></Formik>
          </div>
        </DashboardLayout>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  createSuccess: state.articleReducer && state.articleReducer.createSuccess,
  createError: state.articleReducer && state.articleReducer.createError,
  user: state.loginReducer && state.loginReducer.currentUser,
  errorMessage: state.errorReducer.errorMessage,

});

const mapDispatchToProps = dispatch => ({
  createArticle: data => dispatch(createArticle(data)),
  clearArticleFlag: () => dispatch(clearArticleFlag())
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateArticle);
