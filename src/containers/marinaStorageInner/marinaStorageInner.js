import React, { Component } from "react";
import {
  Layout,
  marinaInnerTrip,
  marinaInnerReview,
  Loader
} from "../../components";
import { Grid, Link, Box } from "@material-ui/core";
import { connect } from "react-redux";
import InnerRating from "../../components/staticComponent/InnerRating";
import RentInnerReviews from "../../components/gridComponents/InnerReviews";
import { RentSharedTrips } from "../../components/gridComponents/rentSharedTrips";
import Rating from "react-rating";
import StarIcon from "@material-ui/icons/Star";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Truncate from "react-truncate";
import Carousel, { Modal, ModalGateway } from "react-images";

import InnerReviews from "../../components/gridComponents/InnerReviews";
import "./marinaStorageInner.scss";
import "./marinaStorageInnerResponsive.scss";
import {
  getSingleMarina,
  increaseMarinaViewCount,
  getMarinaByType
} from "../../redux/actions";
import { getSingleMarinaMarker } from "../../helpers/marinaHelper";
import GoogleMarker from "../../components/map/marker";
import { Player, BigPlayButton } from "video-react";
import { displayDefaultReview, snakeCase } from "../../helpers/string";
import { icons, showAllMarina } from "../../util/enums/enums";
import {
  FirstBannerStyle,
  SecondBannerStyle,
  ThirdBannerStyle
} from "../../components/styleComponent/styleComponent";
import { clearReviewFlag } from "../../redux/actions/ReviewAction";
import moment from "moment";

import { prepareGalleryData } from "../../util/utilFunctions";
import MarinaMostPopular from "../../components/marinaStorage/marinaMostPopular";
import { RecommendedMarinaStorages } from "../../components/gridComponents/recommendedMarinaStorages";
import ReadMore from "../../components/helper/truncate";

class MarinaStorageInner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marinaId: "",
      isGallery: false
    };
  }
  openLightbox = () => {
    this.setState({ isGallery: true });
  };
  closeLightbox = () => {
    this.setState({ isGallery: false });
  };
  static getDerivedStateFromProps(nextProps, prevState) {
    const { isReview, clearReviewFlag, getSingleMarina, match } = nextProps;
    const { params } = match && match;

    const { marinaId } = prevState;
    if (isReview) {
      marinaId && getSingleMarina(marinaId);
      clearReviewFlag();
    }
    if (params && params.id) {
      return {
        marinaId: params.id
      };
    }

    return null;
  }

  async componentDidMount() {
    const { marinaId } = this.state;
    const {
      getSingleMarina,
      increaseMarinaViewCount,
      getMarinaByType
    } = this.props;
    if (marinaId) {
      getSingleMarina(marinaId);
      increaseMarinaViewCount({ marinaId });
      getMarinaByType({ id: marinaId });
    }
  }

  renderBuyNowSection = () => {
    const { editMarina } = this.props;
    const { priceType, priceInUSD, user } = editMarina;
    return (
      <div className="d-flex justify-content-end align-items-center h-100 pl-0 pr-0">
        <div className="d-flex flex-column h-100 justify-content-evenly">
          {editMarina && editMarina.reviews && (
            <div className="d-flex flex-column">
              <div className="d-flex align-items-center justify-content-end">
                <div className="d-flex mr-2 mb-1 margin-bottom-rating">
                  <Rating
                    className="rating-clr rating-small-size"
                    initialRating={
                      editMarina.reviews.ratings &&
                      editMarina.reviews.ratings.averageRating
                    }
                    fullSymbol={<StarIcon />}
                    emptySymbol={<StarBorderIcon />}
                    placeholderSymbol={<StarBorderIcon />}
                    readonly
                  />
                </div>
                <h6 className="m-0 font-14 font-weight-400 dark-silver">
                  {editMarina.reviews.ratings &&
                    editMarina.reviews.ratings.averageRating}
                </h6>
              </div>

              <span className="font13 font-weight-400 gray-light">
                {editMarina.reviews.ratings && editMarina.reviews.ratings.count}
                Customer Reviews
              </span>
            </div>
          )}
          {/* <div className="price-wrap m-0 h5 d-flex mb-1">
            <span className="f-400 font-22">INR {priceInUSD}/hour</span>
            {editMarina.youAreAn && (
              <div className=" light font-11 font-normal">
                Price {editMarina.youAreAn && editMarina.youAreAn.alias}
              </div>
            )}
          </div>
          <Link
            className="btn btn-xs btn-link-info pl-0 font-10 f-500 text-right p-0"
            to="#"
            data-toggle="modal"
            data-target="#currency-modal"
          >
            <u className="text-decoration-none font13 font-weight-400 mb-2">
              USE CURRENCY CONVERTER
            </u>
          </Link>

          <div>
            <Link
              to="#"
              className="btn btn-md btn-primary btn-rounded f-500 w-100 inner-header-button"
            >
              BUY NOW
            </Link>
          </div> */}
        </div>
      </div>
    );
  };

  render() {
    const {
      boat,
      history,
      marinaByType,
      editMarina,
      isLoading,
      currentUser
    } = this.props;
    const { isGallery } = this.state;
    return (
      <Layout className="marina-inner-layout">
        {isLoading && editMarina && !editMarina.hasOwnProperty("id") ? (
          <Loader></Loader>
        ) : (
          <>
            <Grid
              container
              className="container-fluid clr-fluid sticky-on-top inner-sticky-on-top mb-3"
            >
              <div className="m-auto d-flex h-100 w-85vw pt-2 pb-2">
                <Grid item className="h-70 d-flex pr-0 pt-3 pb-3 pl-0" sm={4}>
                  <div className="width-100 align-self-center">
                    <div className="font-weight-500 marina-display-flex">
                      <span className="font-12 font-weight-400 blue-dark">
                        Ad ID:
                        {editMarina && editMarina.adId}
                      </span>
                    </div>
                    <div className="d-flex align-items-center">
                      <span className="font-22 gray-dark">
                        {/* {editMarina.boatName} */}
                        MARINA
                      </span>
                      <h6 className=" ml-3 mb-0 inner-header-badge marina-inner-header-badge dark-silver">
                        {(editMarina &&
                          editMarina.provider &&
                          editMarina.provider.alias) ||
                          "MARINA"}
                      </h6>
                    </div>
                    <span className="font13 gray-light font-weight-400 marina-display-flex">
                      Glasgow, United Kingdom
                    </span>
                    {/* {editMarina && editMarina.reviews && (
                      <div className="d-flex align-items-center">
                        <div className="d-flex mr-2 mb-1">
                          <Rating
                            className="rating-clr rating-small-size"
                            initialRating={
                              editMarina.reviews.ratings &&
                              editMarina.reviews.ratings.averageRating
                            }
                            fullSymbol={<StarIcon />}
                            emptySymbol={<StarBorderIcon />}
                            placeholderSymbol={<StarBorderIcon />}
                            readonly
                          />
                        </div>
                        <h6 className="mr-4 m-0 font-14 font-weight-500">
                          {editMarina.reviews.ratings &&
                            editMarina.reviews.ratings.averageRating}
                        </h6>
                        <span className="font-14 dark-silver font-weight-500">
                          {editMarina.reviews.ratings &&
                            editMarina.reviews.ratings.count}{" "}
                          Customer Reviews
                        </span>
                      </div>
                    )} */}
                  </div>
                </Grid>

                <Grid
                  item
                  className="h-70 d-flex align-items-center pt-3 pb-3 pr-0 pl-0 justify-content-center"
                  sm={4}
                >
                  <div className="widget-media text-center">
                    <ul className="icon-effect icon-effect-1a d-flex">
                      <li>
                        <Link
                          to="/"
                          className="icon d-flex justify-content-center cursor-pointer marina-inner-header-social-icon"
                        >
                          <i className="adam-share gray-light"></i>
                        </Link>
                      </li>
                      <li>
                        <Link
                          to="/"
                          className="icon d-flex justify-content-center cursor-pointer marina-inner-header-social-icon"
                        >
                          <i className="adam-heart1 gray-light"></i>
                        </Link>
                      </li>
                      <li>
                        <Link
                          to="/"
                          className="icon d-flex justify-content-center cursor-pointer marina-inner-header-social-icon"
                        >
                          <i className="adam-flag-3 gray-light"></i>
                        </Link>
                      </li>
                      <li>
                        <Link
                          to="/"
                          className="icon d-flex justify-content-center cursor-pointer marina-inner-header-social-icon"
                        >
                          <i className="adam-envlope gray-light"></i>
                        </Link>
                      </li>
                      <li>
                        <Link
                          to="/"
                          className="icon d-flex justify-content-center cursor-pointer marina-inner-header-social-icon"
                        >
                          <i className="adam-pdf gray-light"></i>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </Grid>

                <Grid item className="h-70" sm={4}>
                  {editMarina && this.renderBuyNowSection()}
                </Grid>
              </div>
            </Grid>

            <div className="row width-100 ml-0">
              <Grid container className="w-85vw m-auto position-relative">
                <div className="position-absolute inner-banner-likes-div d-flex align-items-center font13 dark-silver inner-banner-social-section">
                  {/* <i className="adam-like font13 inner-banner-views-icon-div mr-2 dark-silver"></i> */}
                  <div className="d-flex justify-content-center align-items-center mr-2">
                    <img
                      src={require("../../assets/images/rentInner/like1.png")}
                      className="inner-banner-views-img"
                      alt=""
                    />
                  </div>
                  <span className="banner-count-text"> 0 </span>
                </div>
                <div className="position-absolute inner-banner-top-social-div ">
                  <div className="inner-banner-views-div mr-2 d-flex font13 dark-silver inner-banner-social-section">
                    {/* <i className="adam-view font13 inner-banner-views-icon-div mr-2 dark-silver"></i> */}
                    <div className="d-flex justify-content-center align-items-center mr-2">
                      <img
                        src={require("../../assets/images/rentInner/view.png")}
                        className="inner-banner-views-img"
                        alt=""
                      />
                    </div>
                    <span className="banner-count-text">
                      {" "}
                      {editMarina && editMarina.userCount}
                    </span>
                  </div>
                  <div className="inner-banner-views-div mr-2 d-flex font13 dark-silver inner-banner-social-section">
                    {/* <i className="adam-view font13 inner-banner-views-icon-div mr-2 dark-silver"></i> */}
                    <div className="d-flex justify-content-center align-items-center mr-2">
                      <img
                        src={require("../../assets/images/rentInner/image-count.png")}
                        className="inner-banner-views-img"
                        alt=""
                      />
                    </div>
                    <span className="banner-count-text">
                      {editMarina &&
                        editMarina.images &&
                        editMarina.images.length}
                    </span>
                  </div>
                </div>
                <Grid item sm={6}>
                  <Grid container className="h-100">
                    <Grid item sm={12} className="first-banner-height">
                      <FirstBannerStyle
                        onClick={this.openLightbox}
                        img={
                          editMarina &&
                          editMarina.images &&
                          editMarina.images.length > 0
                            ? encodeURI(editMarina.images[0])
                            : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                        }
                        className="col hidden border-solid-gray pr-0 pl-0 h-100"
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item sm={6}>
                  <Grid container className="h-100">
                    <Grid
                      item
                      xs={12}
                      
                    >
                    <Grid container className="h-100">
                        <Grid item sm={6} className="hidden border-solid-gray h-100 marina-banner-img-padding">
                          <SecondBannerStyle
                            onClick={this.openLightbox}
                            img={
                              editMarina &&
                              editMarina.images &&
                              editMarina.images.length > 1
                                ? encodeURI(editMarina.images[1])
                                : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                            }
                            className="col hidden border-solid-gray pr-0 pl-0 h-100"
                          />
                        </Grid>
                        <Grid item sm={6} className="hidden border-solid-gray h-100 marina-banner-img-padding">
                          <ThirdBannerStyle
                            onClick={this.openLightbox}
                            img={
                              editMarina &&
                              editMarina.images &&
                              editMarina.images.length > 2
                                ? encodeURI(editMarina.images[2])
                                : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                            }
                            className="col hidden border-solid-gray pr-0 pl-0 h-100"
                          />
                        </Grid>
                    </Grid>
                      
                    </Grid>
                    <Grid
                      item
                      xs={12}
                    >
                    <Grid container className="h-100">
                        <Grid item sm={6} className="hidden border-solid-gray h-100 marina-banner-img-padding">
                          <SecondBannerStyle
                            onClick={this.openLightbox}
                            img={
                              editMarina &&
                              editMarina.images &&
                              editMarina.images.length > 3
                                ? encodeURI(editMarina.images[3])
                                : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                            }
                            className="col hidden border-solid-gray pr-0 pl-0 h-100"
                          />
                        </Grid>
                        <Grid item sm={6} className="hidden border-solid-gray h-100 marina-banner-img-padding">
                          <ThirdBannerStyle
                            onClick={this.openLightbox}
                            img={
                              editMarina &&
                              editMarina.images &&
                              editMarina.images.length > 4
                                ? encodeURI(editMarina.images[4])
                                : require("../../assets/images/marinaStorageInner/Light-Grey-Background.jpg")
                            }
                            className="col hidden border-solid-gray pr-0 pl-0 h-100"
                          />
                        </Grid>
                    </Grid>
                      
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <div className="w-85vw m-auto">
                <Grid container spacing={3} className="mt-5">
                  <Grid item sm={9}>
                    <Grid container>
                      <Grid item sm={6} className="pr-3">
                        <div className="">
                          <h4 className=" mb-4 gray-dark">
                            <i className="adam-invoice mr-2 gray-dark"></i>
                            Description of Our Service
                          </h4>
                          <p className="text-justify a-inner-fix font-14">
                            <ReadMore className="text-justify ">
                              {editMarina && editMarina.facilities}
                            </ReadMore>
                          </p>
                        </div>
                      </Grid>
                      <Grid item sm={6}>
                        <div className="">
                          <h4 className=" mb-4 gray-dark d-flex align-items-center">
                            <div className="mr-2 d-flex">
                              <img
                                src={require("../../assets/images/rentInner/star.png")}
                                alt=""
                                className="inner-title-icons"
                              />
                            </div>
                            Membership benefit
                          </h4>
                          <p className="text-justify a-inner-fix font-14">
                            <ReadMore className="text-justify ">
                              {editMarina && editMarina.membershipBenefits}
                            </ReadMore>
                          </p>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid container>
                      <Grid item sm={6}>
                        <div>
                          <h4 className="mt-5 mb-4 gray-dark">
                            <i className="adam-boat-steering-wheel mr-2 gray-dark"></i>
                            Services We Provide
                          </h4>

                          {editMarina &&
                            editMarina.serviceProvide &&
                            editMarina.serviceProvide.length > 0 && (
                              <div className="col-md-12">
                                <Grid container>
                                  <div className="row d-flex">
                                    {editMarina &&
                                      editMarina.serviceProvide &&
                                      editMarina.serviceProvide.length &&
                                      editMarina.serviceProvide.map(item => {
                                        return (
                                          <div className="mb-4 mr-4 service-margin-right-bottom mt-0 ml-0">
                                            <div className="inner-type-div m-auto">
                                              <div className="inner-trip-type-icon-div mb-2 mt-1 dark-silver">
                                                <i
                                                  className={
                                                    item &&
                                                    icons[snakeCase(item.name)]
                                                  }
                                                ></i>
                                              </div>
                                              <div className="inner-trip-type-text-div">
                                                <span className="font-14 trip-type-inner sub-title-sm dark-silver">
                                                  {item.name}
                                                </span>
                                              </div>
                                            </div>
                                          </div>
                                        );
                                      })}
                                  </div>
                                </Grid>
                              </div>
                            )}
                          {/* <div className="d-block">
                              <div className="boat-inner-facilities-div d-flex align-items-center">
                                <ul className="rent pl-0 d-flex width-100 flex-wrap">
                                  {editMarina &&
                                    editMarina.serviceProvide &&
                                    editMarina.serviceProvide.length &&
                                    editMarina.serviceProvide.map(item => {
                                      //need to integrate icons as per service
                                      return <li>{item.name}</li>;
                                    })}
                                </ul>
                              </div>
                            </div> */}
                        </div>
                      </Grid>
                      <Grid item sm={6}>
                        <h4 className="col-12 mt-4 mb-1 pl-0 pt-5 margin-bottom-rating">
                          <i className="adam-coins mr-2 text-black"></i>
                          Payment
                        </h4>
                        <span className="font-14 dark-silver">
                          Seller accept payment of the boat
                        </span>

                        <Grid container>
                          <div className="row width-100 mt-4">
                            <div className="col-6">
                              <div className="inner-payment-div">
                                <i className="adam-creditcard text-black d-flex align-items-center justify-content-center inner-payment-icon mb-2"></i>
                                <h6 className="title-description gray-dark font-14">
                                  Credit Card Payment
                                </h6>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="inner-payment-div">
                                <div className="mb-2">
                                  <div className="d-flex justify-content-center align-items-center mr-3">
                                    <img
                                      src={require("../../assets/images/rentInner/payment.png")}
                                      className="inner-payment-cash-icon"
                                      alt=""
                                    />
                                  </div>
                                </div>
                                <h6 className="title-description gray-dark font-14">
                                  Cash on Arrival
                                </h6>
                              </div>
                            </div>
                          </div>

                          <div className="row width-100 mt-4">
                            <div className="col-6">
                              <div className="inner-payment-div">
                                <div className="mb-2">
                                  <div className="d-flex justify-content-center align-items-center mr-3">
                                    <img
                                      src={require("../../assets/images/rentInner/deliverypayment.png")}
                                      className="inner-payment-cash-icon"
                                      alt=""
                                    />
                                  </div>
                                </div>
                                <h6 className="title-description gray-dark font-14">
                                  Payment after Delivery
                                </h6>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="inner-payment-div">
                                <div className="mb-2">
                                  <div className="d-flex justify-content-center align-items-center mr-3">
                                    <img
                                      src={require("../../assets/images/rentInner/boxcoin.png")}
                                      className="inner-payment-cash-icon"
                                      alt=""
                                    />
                                  </div>
                                </div>
                                <h6 className="title-description gray-dark font-14">
                                  50% Down Payment
                                </h6>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item sm={3}>
                    <div className="col-12 p-0 h-100">
                      <div className="card border user-profile-card m-0 shadow-sm">
                        <div className="card-body mt-0 pt-3 pb-2 pl-3 pr-3">
                          <div className="col-12 brand">
                            <div className="col-12 pl-0 pr-0">
                              <div className="media  mt-3 mb-3 d-flex flex-column h-100 pt-0">
                                <div className="d-flex w-100 justify-content-between flex-wrap">
                                  <div>
                                    <div className="inner-user-info-section user-profile-online-section-marina">
                                      <img
                                        className="rounded-circle cursor-pointer user-img-logo h-100 width-100"
                                        src={
                                          (editMarina &&
                                            editMarina.user &&
                                            editMarina.user.image) ||
                                          require("../../assets/images/rentInner/img-6.jpg")
                                        }
                                        alt="user-img"
                                        onClick={() => {
                                          history.push("/user-profile", {
                                            seller: editMarina.user
                                          });
                                        }}
                                      />
                                      <div className="user-online-marina">
                                        <div className="online-div-user"></div>
                                      </div>
                                    </div>
                                  </div>
                                  <div>
                                    <div className="inner-user-info-section">
                                      <img
                                        className="d-block img-fluid h-100 width-100 marina-inner-company-logo"
                                        src={
                                          (editMarina &&
                                            editMarina.user &&
                                            editMarina.user.companyLogo) ||
                                          require("../../assets/images/rentInner/brand1.jpg")
                                        }
                                        alt="logo"
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="media-body mt-2">
                                <div className="author-info">
                                  <div>
                                    <span className="font-16 font-weight-500 gray-dark">
                                      {editMarina &&
                                        editMarina.user &&
                                        editMarina.user.firstName}
                                    </span>
                                  </div>
                                  <span className="font-14 font-weight-400 dark-silver">
                                    {editMarina &&
                                      editMarina.user &&
                                      editMarina.user.state}{" "}
                                    {editMarina &&
                                      editMarina.user &&
                                      editMarina.user.country}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 mb-3">
                            <ul className="list-group f-14">
                              <li className="list-group-item list-item-color border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                                <div className=" mr-2">
                                  <img
                                    src={require("../../assets/images/rentInner/user.png")}
                                    className="inner-user-info-icon"
                                    alt=""
                                  />
                                </div>
                                <div className="marina-user-card-text">
                                  {editMarina &&
                                    editMarina.user &&
                                    editMarina.user.role.role}
                                </div>
                              </li>
                              <li className="list-group-item list-item-color border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                                <div className=" mr-2">
                                  <img
                                    src={require("../../assets/images/rentInner/place.png")}
                                    className="inner-user-info-icon"
                                    alt=""
                                  />
                                </div>
                                <div className="marina-user-card-text">
                                  On AdamSea since{" "}
                                  {moment(
                                    editMarina &&
                                      editMarina.user &&
                                      editMarina.user.createdAt
                                  ).format("MMM, YYYY")}
                                </div>
                              </li>
                              <li className="list-group-item list-item-color border-0 pl-0 pr-0 sub-title-sm d-flex align-items-center">
                                <div className=" mr-2">
                                  <img
                                    src={require("../../assets/images/rentInner/place.png")}
                                    className="inner-user-info-icon"
                                    alt=""
                                  />
                                </div>
                                <div className="marina-user-card-text">
                                  {" "}
                                  View{" "}
                                  {editMarina &&
                                    editMarina.user &&
                                    editMarina.user.relatedItems &&
                                    editMarina.user.relatedItems.length}{" "}
                                  vehicles
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="mt-3 margin-add-feedback mb-0 text-right width-100 pr-3 d-flex justify-content-end align-items-center">
                        <div className="mr-2">
                          <img
                            src={require("../../assets/images/marinaStorageInner/marinainner-like.png")}
                            alt=""
                            className="feedback-section inner-feedback-img"
                          />
                        </div>
                        <span className="font13 font-weight-500 blue-dark">
                          ADD FEEDBACK
                        </span>
                      </div>
                    </div>
                  </Grid>
                </Grid>
                <hr />
                <Grid container spacing={3} className="pb-5">
                  <Grid item sm={7}>
                    <h4 className="mt-5 mb-3 gray-dark d-flex align-items-center">
                      <div className="mr-2">
                        <img
                          src={require("../../assets/images/rentInner/location.png")}
                          alt=""
                          className="inner-title-icons"
                        />
                      </div>
                      Facility Location
                    </h4>
                    <div className="d-flex">
                      <div className="boat-inner-map-div">

                        {editMarina && (
                          <div className="inner-map">
                            <GoogleMarker
                              // height={25}
                              width={45}
                              markers={getSingleMarinaMarker(editMarina)}
                            />
                          </div>
                        )}
                      </div>
                      <div className="pl-5">
                        <div className="pl-3">
                          <h6 className="mt-4 title-sm title-description mb-2 margin-bottom-rating gray-dark font-14">
                            MEMBERS RATE:
                          </h6>
                          <p className="sub-title-sm font-14 dark-silver">
                            {editMarina && editMarina.membersRate}
                          </p>
                        </div>
                        <div className="pl-3">
                          <h6 className="mt-4 title-sm title-description mb-2 margin-bottom-rating font-14 gray-dark">
                            NON MEMBERS RATES:
                          </h6>
                          <p className="sub-title-sm font-14 dark-silver">
                            200
                          </p>
                        </div>
                      </div>
                    </div>
                  </Grid>
                  { editMarina && editMarina.youtubeLinkVideo && 
                    editMarina.youtubeLinkVideo.length > 0 && (
                  <Grid item sm={4}>
                    <div className="align-items-end justify-content-between">
                      <h4 className="mt-5 mb-4 d-flex align-items-center gray-dark">
                        <div className="mr-2">
                          <img
                            src={require("../../assets/images/rentInner/videos.png")}
                            alt=""
                            className="inner-title-icons"
                          />
                        </div>
                        Related Videos
                      </h4>
                      <div className="video-container h-100 w-100">
                        <Grid
                          xs={12}
                          className="col-12 p-0 align-items-end justify-content-between mt-4 pt-3"
                        >
                          <Player
                            controls={false}
                            playsInline
                            muted
                            poster="/assets/poster.png"
                            src={
                              editMarina &&
                              editMarina.youtubeLinkVideo &&
                              editMarina.youtubeLinkVideo
                            }
                          >
                            <BigPlayButton position="center" />
                          </Player>
                        </Grid>
                      </div>
                    </div>
                  </Grid>
                    )}
                </Grid>
                <hr />
                <div className="pb-5 padding-bottom-for-service">
                  <Grid container spacing={3}>
                    <Grid item sm={6}>
                      <div className="">
                        <h4 className="mt-5 mb-3 margin-bottom-service gray-dark">
                          <i className="adam-storage-1 mr-2 gray-dark"></i> Our
                          Storage
                        </h4>
                        <p className="text-justify a-inner-fix font-14 w-80 margin-bottom-rating">
                            <ReadMore className="text-justify ">
                              {editMarina && editMarina.storage}
                            </ReadMore>
                          </p>
                      </div>
                    </Grid>
                    <Grid item sm={6}>
                      <div className="">
                        <h4 className="mt-5 mb-3 gray-dark margin-bottom-service">
                          <i className="adam-speaker11 mr-2 gray-dark"></i>
                          Running Promotion
                        </h4>
                          <p className="text-justify a-inner-fix font-14 w-80 margin-bottom-rating">
                            <ReadMore className="text-justify ">
                              {editMarina && editMarina.promotionRunning}
                            </ReadMore>
                          </p>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3}>
                    <Grid item sm={6}>
                      <div className="">
                        <h4 className="mt-5 mb-3 margin-bottom-service gray-dark">
                          <i className="adam-berth mr-2 gray-dark"></i> Our
                          Birth
                        </h4>
                        <p className="text-justify a-inner-fix font-14 w-80 margin-bottom-rating">
                          <ReadMore className="text-justify ">
                            {editMarina && editMarina.berth}
                          </ReadMore>
                        </p>
                      </div>
                    </Grid>

                    <Grid item sm={6}>
                      <div className="">
                        <h4 className="mt-5 mb-3 gray-dark margin-bottom-service">
                          <i className="adam-sailor-hat mr-2 gray-dark"></i>
                          Special About Our Facilities
                        </h4>
                        <p className="text-justify a-inner-fix font-14 w-80 margin-bottom-rating">
                          <ReadMore className="text-justify ">
                            {editMarina && editMarina.facilities}
                          </ReadMore>
                        </p>
                      </div>
                    </Grid>
                  </Grid>
                </div>
                <hr />
              </div>
              <div className="w-85vw m-auto">
                <Grid container>
                  {/* start */}
                  <Grid container>
                    <Grid item sm={12} md={12} lg={12}>
                      <div className="container">
                        <div className="row mt-50 mb-5">
                          <div className="col-lg-12 col-md-12 col-sm-12 p-0">
                            <h4 className="gray-dark">
                              <i className="iconColor adam-review mr-2 text-black gray-dark"></i>
                              Review and Rating
                            </h4>
                            {editMarina && editMarina.reviews && (
                              <>
                                <div className="rent-inner-contain inner-rating-section inner-rating-div">
                                  <InnerRating
                                    iconColor="iconColor-rentInner"
                                    btnColor="rentInner-btnBg"
                                    btnBlue="rentInner-btnBlue"
                                    progressColor="rent-progress"
                                    moduleId={editMarina.id}
                                    userId={currentUser.id}
                                    data={displayDefaultReview(
                                      editMarina.reviews.ratings
                                    )}
                                    reviews={editMarina.reviews.reviews}
                                  />
                                </div>
                                <div className="mt-5 user-review-rent inner-review-section">
                                  <InnerReviews
                                    xs={12}
                                    sm={12}
                                    data={editMarina.reviews.reviews}
                                    iconColor="iconColor-rentInner"
                                  />
                                </div>
                              </>
                            )}
                          </div>
                        </div>
                        <hr />
                      </div>
                    </Grid>
                  </Grid>
                  {/* end */}
                  <Grid item sm={12}>
                    {/* Need this in future */}
                    {marinaByType && marinaByType.length > 0 && (
                      <div className="container-fluid marina-storage-layout">
                        <Grid item xs={12} className="mb-5 section-grid">
                          <Box
                            className="section-heading"
                            fontSize={24}
                            fontWeight={500}
                            letterSpacing={0.5}
                          >
                            Recommended Marina & Storage Around You
                          </Box>

                          <RecommendedMarinaStorages
                            xs={12}
                            sm={3}
                            limit={8}
                            recommendedMarinaStorages={marinaByType}
                            textColor="iconColor-marina"
                            showAllText="recommended marina & storage boats"
                          />
                        </Grid>
                      </div>
                    )}
                  </Grid>
                </Grid>
              </div>
            </div>
          </>
        )}
        {editMarina &&
          editMarina.images &&
          editMarina.images.length > 0 &&
          isGallery && (
            <ModalGateway>
              <Modal onClose={this.closeLightbox}>
                <Carousel views={prepareGalleryData(editMarina.images)} />
              </Modal>
            </ModalGateway>
          )}
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  editMarina:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.editMarina,
  isLoading:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.isLoading,
  currentUser: state.loginReducer && state.loginReducer.currentUser,
  isReview: state.reviewReducer && state.reviewReducer.isReview,
  marinaByType:
    state.marinaAndStorageReducer && state.marinaAndStorageReducer.marinaByType
});

const mapDispatchToProps = dispatch => ({
  getSingleMarina: data => dispatch(getSingleMarina(data)),
  increaseMarinaViewCount: data => dispatch(increaseMarinaViewCount(data)),
  clearReviewFlag: () => dispatch(clearReviewFlag()),
  getMarinaByType: data => dispatch(getMarinaByType(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(MarinaStorageInner);
