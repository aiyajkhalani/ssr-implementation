import path from 'path';
import fs from 'fs';

import React from 'react';
import express from 'express';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'


import App from '../src/App';
import rootReducer from '../src/redux/reducers/rootReducer'

import { renderToString } from 'react-dom/server'

function handleRender(req, res) {
  const context = {};
  // Create a new Redux store instance
  const store = createStore(rootReducer)
  // Render the component to a string
  const html = renderToString(
    <StaticRouter location={req.url} context={context}>
    <Provider store={store}>
      <App />
    </Provider>
    </StaticRouter>
  )
  // Grab the initial state from our Redux store
  const preloadedState = store.getState()
  // Send the rendered page back to the client
  res.send(renderFullPage(html, preloadedState))
}

const PORT = process.env.PORT || 3006;
const app = express();

console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

app.use(express.static('./build'));
app.use(handleRender)

app.get('/*', (req, res) => {
  const app = ReactDOMServer.renderToString(<App />);

  const indexFile = path.resolve('./build/index.html');
  fs.readFile(indexFile, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.listen(PORT, () => {
  console.log(`😎 Server is listening on port ${PORT}`);
});

function renderFullPage(html, preloadedState) {
  return `
    <!doctype html>
    <html>
      <head>
        <title>Redux Universal Example</title>
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/recipes/server-rendering/#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
            /</g,
            '\\u003c'
          )}
        </script>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
    `
}